﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace ClearJelly.QBApp
{
    public enum StatusEnum
    {
        [Description("Running")]
        Running = 1,
        [Description("Success")]
        Success = 2,
        [Description("Fail")]
        Fail = 3,
    }

    public enum ModelType
    {
        [Description("Create")]
        Create = 1,
        [Description("Update")]
        Update = 2
    }

    public enum UserStatus
    {
        [Description("Active")]
        Active = 1,
        [Description("Suspended")]
        Suspended = 2,
        [Description("Cancelled")]
        Cancelled = 3,
        [Description("Expired")]
        Expired = 4
    }
    

    public enum EmailTemplates
    {
        [Description("Registration Success Email")]
        RegistrationSuccessEmail = 1,
        [Description("New User Registered Email to Admin")]
        RegistrationSuccessEmailToAdmin = 2,
        [Description("Forgot Password Email")]
        ForgotPasswordEmail = 3,
        [Description("Clearjelly model not built")]
        ClearjellyModelBuiltFailed = 4,
        [Description("Clearjelly model has been built")]
        ClearjellyModelBuiltSuccess = 5,
        [Description("SendMailOn Xerp Api Exception")]
        SendMailOnApiException = 6,
        [Description("Quick Guide")]
        quickGuide = 7,
        [Description("PaymentSuccessEmail")]
        PaymentSuccessEmail = 9,
        [Description("Error Xero Email")]
        ErrorXeroEmail = 10,
        [Description("Database Error")]
        DatabaseError = 11,
        [Description("Jedox Error")]
        JedoxError = 17,
        [Description("Suspend User Max Failure")]
        SuspendUserMaxFailure = 19,

        [Description("Payment Failure")]
        PaymentFailure = 20
    }


    public enum QBErrorCodes
    {
        [Description("The load for your QuickBook data has finished successfully.")]
        Ok = 200,

        [Description("There is some problem while connecting to QuickBook please contact Admin.")]
        BadRequest = 400,

        [Description("Error while connecting QuickBook : Organization is unauthorized or not available please contact Admin.")]
        Unauthorized = 401,

        [Description("Error while connecting QuickBook : Some error occurred please contact Admin.")]
        //[Description("The client SSL certificate was not valid. This indicates the Xero Entrust certificate required for partner API applications is not being supplied in the connection, or has expired")]
        Forbidden = 403,

        [Description("Error while connecting QuickBook : The feature you are trying is not available in QuickBook please contact Admin.")]
        NotFound = 404,

        [Description("Error while connecting QuickBook : Some internal error occurred please contact Admin.")]
        InternalError = 500,

        [Description("Error from QuickBook : The feature you are trying is not available in xero please contact Admin.")]
        NotImplemented = 501,

        [Description("Error from QuickBook : The feature you are trying is not available in xero please contact Admin.")]
        NotAvailable = 503,

        [Description("Error from QuickBook : Token is expired or not active please contact Admin.")]
        GeneralError = 555,

    }
}
