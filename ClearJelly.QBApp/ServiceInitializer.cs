﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.QBApp
{
    public class ServiceInitializer
    {
        private static string accessToken = string.Empty;
        private static string accessTokenSecret = string.Empty;
        private static string consumerKey = string.Empty;
        private static string consumerSecret = string.Empty;
        private static string realmId = string.Empty;

        private static void Initialize()
        {
            //Add keys from oauth2_manager.dictionary in my case
            accessToken = "";
            accessTokenSecret = "";
            consumerKey = "";
            consumerSecret = "";
            realmId = "";
        }

        internal static ServiceContext InitializeQBOServiceContextUsingoAuth()
        {
            Initialize();
            OAuthRequestValidator reqValidator = new OAuthRequestValidator(accessToken, accessTokenSecret, consumerKey, consumerSecret);
            ServiceContext context = new ServiceContext(realmId, IntuitServicesType.QBO, reqValidator);

            //MinorVersion represents the latest features/fields in the xsd supported by the QBO apis.
            //Read more details here- https://developer.intuit.com/docs/0100_quickbooks_online/0200_dev_guides/accounting/querying_data

            context.IppConfiguration.MinorVersion.Qbo = "8";
            return context;
        }
    }
}
