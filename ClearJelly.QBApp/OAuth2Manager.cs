﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using ClearJelly.QBApp.QBService;
using Intuit.Ipp.OAuth2PlatformClient;
using System.Threading.Tasks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.LinqExtender;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;
using Intuit.Ipp.Exception;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using NLog;
using ClearJelly.Configuration;

namespace ClearJelly.QBApp
{
    public class OAuth2Manager
    {
        public string RedirectURI;
        private string _discoveryUrl;
        public string ClientID;
        public string ClientSecret;

        private string _stateVal;
        public string AuthorizationEndpoint;

        private string _userinfoEndPoint;
        private string _revokeEndpoint;
        private string _issuerUrl;

        private string mod;
        private string expo;

        private DiscoveryClient _discoveryClient;
        private DiscoveryResponse _discoveryResponse;
        private Logger _logger;

        private static bool isDbQuickBookCompleted = false;

        //AuthorizeRequest request;
        private IList<JsonWebKey> keys;
        public Dictionary<string, string> Dictionary = new Dictionary<string, string>();

        public string TokenEndpoint { get; set; }
        private string _strConn2;
        private IDbConnection _clearjellyConnecton;

        public OAuth2Manager(string redirectUri, string discoveryUrl, string clientId, string secretKey, string logpath)
        {
            RedirectURI = redirectUri;
            _discoveryUrl = discoveryUrl;
            ClientID = clientId;
            ClientSecret = secretKey;
            _strConn2 = ConfigSection.CompanyDBConnection;

            _clearjellyConnecton = new SqlConnection(ConfigSection.ClearJellyEntities);
            _logger = global::NLog.LogManager.GetCurrentClassLogger();

        }


        public async System.Threading.Tasks.Task ConnectToQuickBooks()
        {
            if (!Dictionary.ContainsKey("accessToken"))
            {
                try
                {
                    await GetDiscoveryData_JWKSkeys();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }
            }
        }

        // For The Future Task
        //public void SaveAccessToken(int userId, string accessToken)
        //{
        //    var query = @"UPDATE [dbo].[User] SET [AccessToken] = @AccessToken WHERE UserId = @UserId";
        //    _clearjellyConnecton.Execute(query, new { AccessToken = accessToken, UserId = userId });
        //}

        //public string GetUserAccessToken(int userId)
        //{
        //    var query = @"SELECT [AccessToken] FROM [dbo].[User]  WHERE UserId = @UserId";
        //    var accessToken = _clearjellyConnecton.Query<string>(query, new { UserId = userId }).FirstOrDefault();
        //    return accessToken;
        //}

        public async System.Threading.Tasks.Task GetDiscoveryData_JWKSkeys()
        {
            _logger.Info("Fetching Discovery Data.");

            //Intialize DiscoverPolicy
            DiscoveryPolicy dpolicy = new DiscoveryPolicy();
            dpolicy.RequireHttps = true;
            dpolicy.ValidateIssuerName = true;

            //Assign the Sandbox Discovery url for the Apps' Dev clientid and clientsecret that you use
            //Or
            //Assign the Production Discovery url for the Apps' Production clientid and clientsecret that you use
            if (_discoveryUrl != null && ClientID != null && ClientSecret != null)
            {
                _discoveryClient = new DiscoveryClient(_discoveryUrl);
            }
            _discoveryResponse = await _discoveryClient.GetAsync();

            if (_discoveryResponse.StatusCode == HttpStatusCode.OK)
            {
                //Authorization endpoint url
                AuthorizationEndpoint = _discoveryResponse.AuthorizeEndpoint;
                //Token endpoint url
                TokenEndpoint = _discoveryResponse.TokenEndpoint;
                //UseInfo endpoint url
                _userinfoEndPoint = _discoveryResponse.UserInfoEndpoint;
                //Revoke endpoint url
                _revokeEndpoint = _discoveryResponse.RevocationEndpoint;
                //Issuer endpoint Url 
                _issuerUrl = _discoveryResponse.Issuer;
                //JWKS Keys
                keys = _discoveryResponse.KeySet.Keys;
                _logger.Info("Discovery Data obtained.");
                isDbQuickBookCompleted = true;
            }
            else
            {
                _logger.Error("Discovery error");
            }
        }


        public async Task<TokenResponse> RefreshToken(string refresh_token)
        {
            _logger.Info("Exchanging refresh token for access token.");//refresh token is valid for 100days and access token for 1hr

            //Request Oauth2 tokens
            var tokenClient = new TokenClient(TokenEndpoint, ClientID, ClientSecret);

            //Handle exception 401 and then make this call
            // Call RefreshToken endpoint to get new access token when you recieve a 401 Status code
            TokenResponse refereshtokenCallResponse = await tokenClient.RequestRefreshTokenAsync(refresh_token);
            _logger.Info("Access token refreshed.");

            Dictionary["accessToken"] = refereshtokenCallResponse.AccessToken;
            Dictionary["refreshToken"] = refereshtokenCallResponse.RefreshToken;

            _logger.Info("Dictionary keys updated.");
            return refereshtokenCallResponse;

        }

        public async System.Threading.Tasks.Task<Company> QBOApiCallAsync(string DbName)
        {
            return await QBOApiCall(DbName);
        }

        public int UpdateQBData(string DbName, DateTime startDate, DateTime endDate, string realmId = null, string selectedCompany = null)
        {
            _strConn2 = ConfigSection.CompanyDBConnection + ";database=QuickBooks_" + DbName + ";";
            QBService.QBService QBService = new QBService.QBService(Dictionary["accessToken"], Dictionary["realmId"], _strConn2);
            var code = QBService.UpdateDataAsync(startDate, endDate, realmId, selectedCompany).Result;
            return code;
        }
        //public async System.Threading.Tasks.Task getDataByCompany(string realmId, string accessToken)
        //{
        //    QBService.QBService QBService = new QBService.QBService(Dictionary["accessToken"], Dictionary["realmId"], _strConn2);
        //    QBService.getDataByCompany(realmId, accessToken);
        //}
        public async System.Threading.Tasks.Task<Company> QBOApiCall(string DbName)
        {
            var companyName = new Company();
            try
            {
                if (Dictionary["realmId"] != "")
                {
                    _strConn2 = ConfigSection.CompanyDBConnection + ";database=QuickBooks_" + DbName + ";";
                    QBService.QBService QBService = new QBService.QBService(Dictionary["accessToken"], Dictionary["realmId"], _strConn2);
                    if (Dictionary.ContainsKey("realmId"))
                    {
                        companyName = await QBService.GetDataAsync(Dictionary["realmId"]);
                    }
                }

            }
            catch (IdsException ex)
            {
                if (ex.Message == "UnAuthorized-401" || ex.Message == "The remote server returned an error: (401) Unauthorized.")
                {
                    //if you get a 401 token expiry then perform token refresh
                    await RefreshToken(Dictionary["refreshToken"]);

                    if ((Dictionary.ContainsKey("accessToken")) && (Dictionary.ContainsKey("accessToken")) && (Dictionary.ContainsKey("realmId")))
                    {
                        await QBOApiCall(DbName);
                    }
                }
                else
                {
                    _logger.Error(ex.Message);
                }

            }
            catch (Exception ex)
            {
                //Check Status Code 401 and then 
                _logger.Error("Invalid/Expired Access Token.\n" + ex.Message);

            }
            return companyName;
        }

        public async Task<UserInfoResponse> GetUserInfo(string access_token, string refresh_token)
        {
            _logger.Info("Making Get User Info Call.");
            //Get UserInfo data when correct scope is set for SIWI and Get App now flows
            var userInfoClient = new UserInfoClient(_userinfoEndPoint);
            UserInfoResponse userInfoResponse = await userInfoClient.GetAsync(access_token);
            _logger.Info("HttpStatusCode: --->" + userInfoResponse.HttpStatusCode.ToString());
            _logger.Info("Get User Info Call completed.");
            return userInfoResponse;
        }

        public RedirectConfig DoOAuth()
        {
            _logger.Info("Intiating OAuth2 call to get code.");
            string authorizationRequest = "";
            string scopeVal = "";

            ////Save the state(CSRF token/Campaign Id/Tracking Id) in dictionary to verify after Oauth2 Callback. This is just for reference. 
            ////Actual CSRF handling should be done as per security standards in some hidden fields or encrypted permanent store
            _stateVal = CryptoRandom.CreateUniqueId();

            if (!Dictionary.ContainsKey("CSRF"))
            {
                Dictionary.Add("CSRF", _stateVal);
            }

            scopeVal = OidcScopes.Accounting.GetStringValue();

            _logger.Info("Setting up Authorize url");
            //Create the OAuth 2.0 authorization request.

            if (AuthorizationEndpoint != "" && AuthorizationEndpoint != null)
            {
                authorizationRequest = string.Format("{0}?client_id={1}&scope={2}&redirect_uri={3}&response_type=code&state={4}",
                    AuthorizationEndpoint,
                    ClientID,
                    scopeVal,
                    Uri.EscapeDataString(RedirectURI), _stateVal);
                _logger.Info("Calling AuthorizeUrl");


                //redirect to authorization request url in pop-up
                var config = new RedirectConfig();
                config.AuthRequest = authorizationRequest;
                config.Target = "_blank";
                config.WindowOptions = "menubar=0,scrollbars=1,width=780,height=900,top=10";
                return config;
            }
            else
            {
                _logger.Error("Missing authorizationEndpoint url!");
            }
            return null;
        }

        public Task<bool> IsIdTokenValid(string id_token)
        {
            _logger.Info("Making IsIdToken Valid Call.");
            string idToken = id_token;
            if (keys != null)
            {
                //Get mod and exponent value
                foreach (var key in keys)
                {
                    if (key.N != null)
                    {
                        //Mod
                        mod = key.N;
                    }
                    if (key.N != null)
                    {
                        //Exponent
                        expo = key.E;
                    }
                }
                //IdentityToken
                if (idToken != null)
                {
                    //Split the identityToken to get Header and Payload
                    string[] splitValues = idToken.Split('.');
                    if (splitValues[0] != null)
                    {
                        //Decode header 
                        var headerJson = Encoding.UTF8.GetString(Base64Url.Decode(splitValues[0].ToString()));
                        //Deserilaize headerData
                        IdTokenHeader headerData = JsonConvert.DeserializeObject<IdTokenHeader>(headerJson);
                        //Verify if the key id of the key used to sign the payload is not null
                        if (headerData.Kid == null)
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }
                        //Verify if the hashing alg used to sign the payload is not null
                        if (headerData.Alg == null)
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }
                    }
                    if (splitValues[1] != null)
                    {
                        //Decode payload
                        var payloadJson = Encoding.UTF8.GetString(Base64Url.Decode(splitValues[1].ToString()));

                        IdTokenJWTClaimTypes payloadData = JsonConvert.DeserializeObject<IdTokenJWTClaimTypes>(payloadJson);
                        //Verify Aud matches ClientId
                        if (payloadData.Aud != null)
                        {
                            if (payloadData.Aud[0].ToString() != ClientID)
                            {
                                return System.Threading.Tasks.Task.FromResult(false);
                            }
                        }
                        else
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }
                        //Verify Authtime matches the time the ID token was authorized.                
                        if (payloadData.Auth_time == null)
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }
                        //Verify exp matches the time the ID token expires, represented in Unix time (integer seconds).                
                        if (payloadData.Exp != null)
                        {
                            long expiration = Convert.ToInt64(payloadData.Exp);
                            long currentEpochTime = EpochTimeExtensions.ToEpochTime(DateTime.UtcNow);
                            //Verify the ID expiration time with what expiry time you have calculated and saved in your application
                            //If they are equal then it means IdToken has expired 

                            if ((expiration - currentEpochTime) <= 0)
                            {
                                return System.Threading.Tasks.Task.FromResult(false);

                            }
                        }
                        //Verify Iat matches the time the ID token was issued, represented in Unix time (integer seconds).            
                        if (payloadData.Iat == null)
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }

                        //verify Iss matches the  issuer identifier for the issuer of the response.     
                        if (payloadData.Iss != null)
                        {
                            if (payloadData.Iss.ToString() != _issuerUrl)
                            {

                                return System.Threading.Tasks.Task.FromResult(false);
                            }
                        }
                        else
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }

                        //Verify sub. Sub is an identifier for the user, unique among all Intuit accounts and never reused. 
                        //An Intuit account can have multiple emails at different points in time, but the sub value is never changed.
                        //Use sub within your application as the unique-identifier key for the user.
                        if (payloadData.Sub == null)
                        {
                            return System.Threading.Tasks.Task.FromResult(false);
                        }
                    }

                    //Use external lib to decode mod and expo value and generte hashes
                    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

                    //Read values of n and e from discovery document.
                    rsa.ImportParameters(
                      new RSAParameters()
                      {
                          //Read values from discovery document
                          Modulus = Base64Url.Decode(mod),
                          Exponent = Base64Url.Decode(expo)
                      });

                    //Verify Siganture hash matches the signed concatenation of the encoded header and the encoded payload with the specified algorithm
                    SHA256 sha256 = SHA256.Create();

                    byte[] hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(splitValues[0] + '.' + splitValues[1]));

                    RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
                    rsaDeformatter.SetHashAlgorithm("SHA256");
                    if (rsaDeformatter.VerifySignature(hash, Base64Url.Decode(splitValues[2])))
                    {
                        //identityToken is valid
                        return System.Threading.Tasks.Task.FromResult(true);
                    }
                    else
                    {
                        //identityToken is not valid
                        return System.Threading.Tasks.Task.FromResult(false);
                    }
                }
                else
                {
                    //identityToken is not valid
                    return System.Threading.Tasks.Task.FromResult(false);
                }
            }
            else
            {
                //Missing mod and expo values
                return System.Threading.Tasks.Task.FromResult(false);
            }

        }

    }

    public class RedirectConfig
    {
        public string AuthRequest { get; set; }
        public string Target { get; set; }
        public string WindowOptions { get; set; }
    }
}
