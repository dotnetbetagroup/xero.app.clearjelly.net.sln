﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using ClearJelly.ViewModels;
using ViewModels;

namespace ClearJelly.QBApp.QBService
{
    public static class QbMappers
    {
        private static DateTime rngMin = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        private static DateTime rngMax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;
        public static string GetJson(object item)
        {
            var res = new JavaScriptSerializer().Serialize(item);
            return res;
        }
        public static bool CheckDate(DateTime item)
        {
            return item < rngMin || item > rngMax;
        }
        public static List<QuickBookItemsViewModel> CompleteItemtModel(List<Item> items, string orgName)
        {
            var itemList = new List<QuickBookItemsViewModel>();
            foreach (var item in items)
            {
                var guid = Guid.NewGuid().ToString();
                var checkDate = item.InvStartDate < rngMin || item.InvStartDate > rngMax;
                var itemCurrent = new QuickBookItemsViewModel
                {
                    Id = guid,
                    QbId = Int32.Parse(item.Id),
                    SyncToken = item.SyncToken,
                    MetaData = GetJson(item.MetaData),
                    CustomField = GetJson(item.CustomField),
                    AttachableRef = GetJson(item.AttachableRef),
                    domain = item.domain,
                    status = item.status,
                    statusSpecified = item.statusSpecified,
                    sparse = item.sparse,
                    sparseSpecified = item.sparseSpecified,
                    NameAndId = item.NameAndId,
                    Overview = item.Overview,
                    HeaderLite = item.HeaderLite,
                    HeaderFull = item.HeaderFull,
                    SalesTaxCodeRef = item.SalesTaxCodeRef != null ? GetJson(item.SalesTaxCodeRef) : string.Empty,
                    DepositToAccountRef = item.DepositToAccountRef != null ? GetJson(item.DepositToAccountRef) : string.Empty,
                    ManPartNum = item.ManPartNum,
                    ReorderPointSpecified = item.ReorderPointSpecified,
                    ReorderPoint = item.ReorderPoint,
                    QtyOnSalesOrderSpecified = item.QtyOnSalesOrderSpecified,
                    PurchaseTaxCodeRef = GetJson(item.PurchaseTaxCodeRef),
                    QtyOnSalesOrder = item.QtyOnSalesOrder,
                    QtyOnPurchaseOrder = item.QtyOnPurchaseOrder,
                    QtyOnHandSpecified = item.QtyOnHandSpecified,
                    QtyOnHand = item.QtyOnHand,
                    TrackQtyOnHandSpecified = item.TrackQtyOnHandSpecified,
                    TrackQtyOnHand = item.TrackQtyOnHand,
                    AvgCostSpecified = item.AvgCostSpecified,
                    QtyOnPurchaseOrderSpecified = item.QtyOnPurchaseOrderSpecified,
                    AvgCost = item.AvgCost,
                    InvStartDate = !checkDate ? item.InvStartDate : DateTime.Now,
                    BuildPoint = item.BuildPoint,
                    ServiceType = item.ServiceType,
                    ReverseChargeRateSpecified = item.ReverseChargeRateSpecified,
                    ReverseChargeRate = item.ReverseChargeRate,
                    AbatementRateSpecified = item.AbatementRateSpecified,
                    AbatementRate = item.AbatementRate,
                    ParentRef = GetJson(item.ParentRef),
                    ItemAssemblyDetail = GetJson(item.ItemAssemblyDetail),
                    InvStartDateSpecified = item.InvStartDateSpecified,
                    ItemGroupDetail = GetJson(item.ItemGroupDetail),
                    SpecialItemType = item.SpecialItemType,
                    SpecialItemSpecified = item.SpecialItemSpecified,
                    SpecialItem = item.SpecialItem,
                    PrintGroupedItemsSpecified = item.PrintGroupedItemsSpecified,
                    AssetAccountRef = GetJson(item.AssetAccountRef),
                    PrintGroupedItems = item.PrintGroupedItems,
                    BuildPointSpecified = item.BuildPointSpecified,
                    SpecialItemTypeSpecified = item.SpecialItemTypeSpecified,
                    ItemCategoryType = item.ItemCategoryType,
                    PrefVendorRef = GetJson(item.PrefVendorRef),
                    COGSAccountRef = GetJson(item.COGSAccountRef),
                    SalesTaxIncluded = item.SalesTaxIncluded,
                    ExpenseAccountRef = GetJson(item.ExpenseAccountRef),
                    TaxableSpecified = item.TaxableSpecified,
                    Taxable = item.Taxable,
                    FullyQualifiedName = item.FullyQualifiedName,
                    LevelSpecified = item.LevelSpecified,
                    Level = 1,
                    SalesTaxIncludedSpecified = item.SalesTaxIncludedSpecified,
                    SubItem = item.SubItem,
                    ActiveSpecified = item.ActiveSpecified,
                    Active = item.Active,
                    IncomeAccountRef = GetJson(item.IncomeAccountRef),
                    Description = item.Description,
                    Sku = item.Sku,
                    Name = item.Name,
                    SubItemSpecified = item.SubItemSpecified,
                    PercentBased = item.PercentBased,
                    UnitPrice = item.UnitPrice,
                    PurchaseCostSpecified = item.PurchaseCostSpecified,
                    PurchaseCost = item.PurchaseCost,
                    PurchaseTaxIncludedSpecified = item.PurchaseTaxIncludedSpecified,
                    PaymentMethodRef = GetJson(item.PaymentMethodRef),
                    PurchaseTaxIncluded = item.PurchaseTaxIncluded,
                    PurchaseDesc = item.PurchaseDesc,
                    PercentBasedSpecified = item.PercentBasedSpecified,
                    TypeSpecified = item.TypeSpecified,
                    Type = item.Type,
                    RatePercentSpecified = item.RatePercentSpecified,
                    RatePercent = item.RatePercent,
                    UnitPriceSpecified = item.UnitPriceSpecified,
                    UOMSetRef = GetJson(item.UOMSetRef),
                    ItemEx = GetJson(item.ItemEx),
                    OrgName = orgName
                };
                itemList.Add(itemCurrent);
            }
            return itemList;
        }
        public static List<QuickBookAccountViewModel> CompleteAccountModel(List<Account> items, string orgName)
        {
            var listAccounts = new List<QuickBookAccountViewModel>();
            foreach (var item in items)
            {
                var guid = Guid.NewGuid().ToString();
                var checkdate = item.OpeningBalanceDate < rngMin || item.OpeningBalanceDate > rngMax;
                var account = new QuickBookAccountViewModel
                {
                    Id = guid,
                    QbId = Int32.Parse(item.Id),
                    SyncToken = item.SyncToken,
                    MetaData = GetJson(item.MetaData),
                    CustomField = GetJson(item.CustomField),
                    AttachableRef = GetJson(item.AttachableRef),
                    domain = item.domain,
                    status = item.status,
                    statusSpecified = item.statusSpecified,
                    sparse = item.sparse,
                    sparseSpecified = item.sparseSpecified,
                    NameAndId = item.NameAndId,
                    Overview = item.Overview,
                    HeaderLite = item.HeaderLite,
                    HeaderFull = item.HeaderFull,
                    OpeningBalanceSpecified = item.OpeningBalanceSpecified,
                    OpeningBalanceDate = !checkdate ? item.OpeningBalanceDate : DateTime.Now,
                    OpeningBalanceDateSpecified = item.OpeningBalanceDateSpecified,
                    CurrentBalance = item.CurrentBalance,
                    JournalCodeRef = item.JournalCodeRef != null ? GetJson(item.JournalCodeRef) : string.Empty,
                    ParentRef = GetJson(item.ParentRef),
                    CurrencyRef = item.CurrencyRef != null ? GetJson(item.CurrencyRef) : string.Empty,
                    TaxCodeRef = GetJson(item.TaxCodeRef),
                    CurrentBalanceSpecified = item.CurrentBalanceSpecified,
                    CurrentBalanceWithSubAccounts = item.CurrentBalanceWithSubAccounts,
                    OpeningBalance = item.OpeningBalance,
                    CurrentBalanceWithSubAccountsSpecified = item.CurrentBalanceWithSubAccountsSpecified,
                    TaxAccount = item.TaxAccount,
                    TaxAccountSpecified = item.TaxAccountSpecified,
                    OnlineBankingEnabled = item.OnlineBankingEnabled,
                    OnlineBankingEnabledSpecified = item.OnlineBankingEnabledSpecified,
                    FIName = item.FIName,
                    BankNum = item.BankNum,
                    AcctNumExtn = item.AcctNumExtn,
                    AcctNum = item.AcctNum,
                    Name = item.Name,
                    SubAccount = item.SubAccount,
                    SubAccountSpecified = item.SubAccountSpecified,
                    Description = item.Description,
                    FullyQualifiedName = item.FullyQualifiedName,
                    AccountAlias = item.AccountAlias,
                    TxnLocationType = item.TxnLocationType,
                    Active = item.Active,
                    ActiveSpecified = item.ActiveSpecified,
                    Classification = item.Classification,
                    ClassificationSpecified = item.ClassificationSpecified,
                    AccountType = item.AccountType,
                    AccountTypeSpecified = item.AccountTypeSpecified,
                    AccountSubType = item.AccountSubType,
                    AccountEx = GetJson(item.AccountEx),
                    OrgName = orgName
                };
                listAccounts.Add(account);
            }
            return listAccounts;

        }
        public static List<QuickBookJournalViewModel> CompleteJouralModel(List<JournalEntry> items, string orgName)
        {
            var journalList = new List<QuickBookJournalViewModel>();
            foreach (var item in items)
            {
                var guid = Guid.NewGuid().ToString();
                var journal = new QuickBookJournalViewModel
                {
                    Id = guid,
                    QbId = Int32.Parse(item.Id),
                    SyncToken = item.SyncToken,
                    MetaData = GetJson(item.MetaData),
                    CustomField = GetJson(item.CustomField),
                    AttachableRef = GetJson(item.AttachableRef),
                    domain = item.domain,
                    status = item.status,
                    statusSpecified = item.statusSpecified,
                    sparse = item.sparse,
                    sparseSpecified = item.sparseSpecified,
                    NameAndId = item.NameAndId,
                    Overview = item.Overview,
                    HeaderLite = item.HeaderLite,
                    HeaderFull = item.HeaderFull,
                    Adjustment = item.Adjustment,
                    AdjustmentSpecified = item.AdjustmentSpecified,
                    HomeCurrencyAdjustment = item.HomeCurrencyAdjustment,
                    HomeCurrencyAdjustmentSpecified = item.HomeCurrencyAdjustmentSpecified,
                    EnteredInHomeCurrency = item.EnteredInHomeCurrency,
                    EnteredInHomeCurrencySpecified = item.EnteredInHomeCurrencySpecified,
                    TotalAmt = item.TotalAmt,
                    TotalAmtSpecified = item.TotalAmtSpecified,
                    HomeTotalAmt = item.HomeTotalAmt,
                    HomeTotalAmtSpecified = item.HomeTotalAmtSpecified,
                    JournalEntryEx = GetJson(item.JournalEntryEx),
                    TaxFormType = item.TaxFormType,
                    TxnSource = item.TxnSource,
                    TxnTaxDetail = GetJson(item.TxnTaxDetail),
                    TxnStatus = item.TxnStatus,
                    TaxFormNum = item.TaxFormNum,
                    PrivateNote = item.PrivateNote,
                    ExchangeRate = item.ExchangeRate,
                    CurrencyRef = GetJson(item.CurrencyRef),
                    DepartmentRef = GetJson(item.DepartmentRef),
                    Line = GetJson(item.Line),
                    LinkedTxn = GetJson(item.LinkedTxn),
                    TxnDateSpecified = item.TxnDateSpecified,
                    TxnDate = item.TxnDate,
                    DocNumber = item.DocNumber,
                    ExchangeRateSpecified = item.ExchangeRateSpecified,
                    TransactionLocationType = item.TransactionLocationType,
                    OrgName = orgName
                };
                journalList.Add(journal);
            }
            return journalList;
        }
        public static List<QuickBookInvoiceViewModel> CompleteInvoiceModel(List<Invoice> items, string orgName)
        { var invoicelList = new List<QuickBookInvoiceViewModel>();
            foreach (var item in items)
            {
                var guid = Guid.NewGuid().ToString();
                var invoice = new QuickBookInvoiceViewModel
                {
                    Id = guid,
                    QbId = Int32.Parse(item.Id),
                    callToAction = item.callToAction,
                    invoiceStatus = item.invoiceStatus,
                    ECloudStatusTimeStampSpecified = item.ECloudStatusTimeStampSpecified,
                    ECloudStatusTimeStamp = !CheckDate(item.ECloudStatusTimeStamp) ? item.ECloudStatusTimeStamp : DateTime.Now,
                    EInvoiceStatusSpecified = item.EInvoiceStatusSpecified,
                    EInvoiceStatus = item.EInvoiceStatus,
                    AllowOnlineACHPaymentSpecified = item.AllowOnlineACHPaymentSpecified,
                    invoiceStatusLog = item.invoiceStatusLog?.ToString(),
                    AllowOnlineACHPayment = item.AllowOnlineACHPayment,
                    AllowOnlineCreditCardPayment = item.AllowOnlineCreditCardPayment,
                    AllowOnlinePaymentSpecified = item.AllowOnlinePaymentSpecified,
                    AllowOnlinePayment = item.AllowOnlinePayment,
                    AllowIPNPaymentSpecified = item.AllowIPNPaymentSpecified,
                    AllowIPNPayment = item.AllowIPNPayment,
                    DepositSpecified = item.DepositSpecified,
                    Deposit = item.Deposit,
                    AllowOnlineCreditCardPaymentSpecified = item.AllowOnlineCreditCardPaymentSpecified,
                    InvoiceEx = item.InvoiceEx?.Any?.ToString(),
                    AnyIntuitObject = item.AnyIntuitObject != null ? GetJson(item.AnyIntuitObject) : string.Empty,
                    ApplyTaxAfterDiscount = item.ApplyTaxAfterDiscount,
                    ApplyTaxAfterDiscountSpecified = item.ApplyTaxAfterDiscountSpecified,
                    ARAccountRef = item.ARAccountRef != null ? GetJson(item.ARAccountRef) : string.Empty,
                    AttachableRef = item.AttachableRef != null ? GetJson(item.AttachableRef) : string.Empty,
                    AutoDocNumber = item.AutoDocNumber,
                    AutoDocNumberSpecified = item.AutoDocNumberSpecified,
                    Balance = item.Balance,
                    BalanceSpecified = item.BalanceSpecified,
                    BillAddr = item.BillAddr != null ? GetJson(item.BillAddr) : string.Empty,
                    BillEmail = item.BillEmail != null ? GetJson(item.BillEmail) : string.Empty,
                    BillEmailBcc = item.BillEmailBcc != null ? GetJson(item.BillEmailBcc) : string.Empty,
                    BillEmailCc = item.BillEmailCc != null ? GetJson(item.BillEmailCc) : string.Empty,
                    ClassRef = item.ClassRef != null ? GetJson(item.ClassRef) : string.Empty,
                    CustomerMemo = item.CustomerMemo != null ? GetJson(item.CustomerMemo) : string.Empty,
                    CustomerRef = item.CustomerRef != null ? GetJson(item.CustomerRef) : string.Empty,
                    CustomField = item.CustomField != null ? GetJson(item.CustomField) : string.Empty,
                    DeliveryInfo = item.DeliveryInfo != null ? GetJson(item.DeliveryInfo) : string.Empty,
                    DepartmentRef = item.DepartmentRef != null ? GetJson(item.DepartmentRef) : string.Empty,
                    DepositToAccountRef = item.DepositToAccountRef != null ? GetJson(item.DepositToAccountRef) : string.Empty,
                    DiscountAmt = item.DiscountAmt,
                    DiscountAmtSpecified = item.DiscountAmtSpecified,
                    DiscountRate = item.DiscountRate,
                    DiscountRateSpecified = item.DiscountRateSpecified,
                    DocNumber = item.DocNumber,
                    domain = item.domain,
                    DueDate = !CheckDate(item.DueDate) ? item.DueDate : DateTime.Now,
                    DueDateSpecified = item.DueDateSpecified,
                    EmailStatus = item.EmailStatus,
                    EmailStatusSpecified = item.EmailStatusSpecified,
                    ExchangeRate = item.ExchangeRate,
                    ExchangeRateSpecified = item.ExchangeRateSpecified,
                    FinanceCharge = item.FinanceCharge,
                    FinanceChargeSpecified = item.FinanceChargeSpecified,
                    FOB = item.FOB,
                    GlobalTaxCalculation = item.GlobalTaxCalculation,
                    GlobalTaxCalculationSpecified = item.GlobalTaxCalculationSpecified,
                    GovtTxnRefIdentifier = item.GovtTxnRefIdentifier,
                    HeaderFull = item.HeaderFull,
                    HeaderLite = item.HeaderLite,
                    HomeBalance = item.HomeBalance,
                    HomeBalanceSpecified = item.HomeBalanceSpecified,
                    HomeTotalAmt = item.HomeTotalAmt,
                    HomeTotalAmtSpecified = item.HomeTotalAmtSpecified,
                    Line = item.Line != null ? GetJson(item.Line) : string.Empty,
                    LinkedTxn = item.LinkedTxn != null ? GetJson(item.LinkedTxn) : string.Empty,
                    MetaData = item.MetaData != null ? GetJson(item.MetaData) : string.Empty,
                    NameAndId = item.NameAndId,
                    Overview = item.Overview,
                    PaymentMethodRef = item.PaymentMethodRef != null ? GetJson(item.PaymentMethodRef) : string.Empty,
                    PaymentRefNum = item.PaymentRefNum,
                    PaymentType = item.PaymentType,
                    PaymentTypeSpecified = item.PaymentTypeSpecified,
                    PONumber = item.PONumber,
                    PrintStatus = item.PrintStatus,
                    PrintStatusSpecified = item.PrintStatusSpecified,
                    PrivateNote = item.PrivateNote,
                    RemitToRef = item.RemitToRef != null ? GetJson(item.RemitToRef) : string.Empty,
                    SalesRepRef = item.SalesRepRef != null ? GetJson(item.SalesRepRef) : string.Empty,
                    SalesTermRef = item.SalesTermRef != null ? GetJson(item.SalesTermRef) : string.Empty,
                    ShipAddr = item.ShipAddr != null ? GetJson(item.ShipAddr) : string.Empty,
                    ShipDate = !CheckDate(item.ShipDate) ? item.ShipDate : DateTime.Now,
                    ShipDateSpecified = item.ShipDateSpecified,
                    ShipMethodRef = item.ShipMethodRef != null ? GetJson(item.ShipMethodRef) : string.Empty,
                    sparse = item.sparse,
                    sparseSpecified = item.sparseSpecified,
                    status = item.status,
                    statusSpecified = item.statusSpecified,
                    SyncToken = item.SyncToken,
                    TaxFormNum = item.TaxFormNum,
                    TaxFormType = item.TaxFormType,
                    TemplateRef = item.TemplateRef != null ? GetJson(item.TemplateRef) : string.Empty,
                    TotalAmt = item.TotalAmt,
                    TotalAmtSpecified = item.TotalAmtSpecified,
                    TrackingNum = item.TrackingNum,
                    TransactionLocationType = item.TransactionLocationType,
                    TxnDate = !CheckDate(item.TxnDate) ? item.TxnDate : DateTime.Now,
                    TxnDateSpecified = item.TxnDateSpecified,
                    TxnSource = item.TxnSource,
                    TxnStatus = item.TxnStatus,
                    TxnTaxDetail = item.TxnTaxDetail != null ? GetJson(item.TxnTaxDetail) : string.Empty,
                    CurrencyRef = item.CurrencyRef != null ? GetJson(item.CurrencyRef) : string.Empty,
                    OrgName = orgName
                };
                invoicelList.Add(invoice);
            }
            return invoicelList;
        }
    }
}
