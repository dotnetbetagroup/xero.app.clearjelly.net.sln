﻿using ClearJelly.Configuration;
using ClearJelly.QBApp.Repositories;
using Dapper;
using Intuit.Ipp.Core.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.QBApp.QBService
{
    public class QBTablesGenerator
    {
        private readonly NLog.Logger _logger;
        private string _connection;
        private string _companyName;

        public QBTablesGenerator(string companyName)
        {
            _companyName = companyName;
            _logger = LogManager.GetCurrentClassLogger();
            _connection = ConfigSection.QBCompanyDbConnection + _companyName;
        }
        public async Task<bool> CreateQBDbAsync(string companyName)
        {
            var _metaDBConnection = ConfigSection.DefaultConnection;
            var QBRepository = new QBRepository(_metaDBConnection);
            var createDB = QBRepository.CreateQBatabaseAsync(companyName);
            await GenerateTables();
            //QBTablesGenerator
            return true;
        }
        public async Task GenerateTables()
        {
            var listTasks = new List<Task>();

            listTasks.Add(Task.Run(() => CreateTables()));           
            await Task.WhenAll(listTasks);
        }

        private void CreateTables()
        {
            try
            {
                _logger.Info("_connection:   " + _connection);
                _logger.Info($"Create QB Account table db for  [QuickBooks_{_companyName}");
                using (SqlConnection connection = new SqlConnection(_connection))
                {

                    var query = "CREATE TABLE [QuickBooks_" + _companyName + @"].[dbo].[Account](
                    [Id][nvarchar](4000) PRIMARY KEY NOT NULL, [QbId] [int] NOT NULL,
                    [SyncToken] [nvarchar] (4000) NULL, [MetaData] [nvarchar] (4000) NULL,
			        [CustomField] [nvarchar] (4000) NULL,[AttachableRef] [nvarchar] (4000) NULL,
			        [domain] [nvarchar] (4000) NULL,[status] [int] NULL,[statusSpecified] [bit] NULL,
			        [sparse] [bit] NULL,[sparseSpecified] [bit] NULL,[NameAndId] [nvarchar] (4000) NULL,
			        [Overview] [nvarchar] (4000) NULL,[HeaderLite] [nvarchar] (4000) NULL,
			        [HeaderFull] [nvarchar] (4000) NULL,[OpeningBalanceSpecified] [bit] NULL,
			        [OpeningBalanceDate] [datetime] NULL,[OpeningBalanceDateSpecified] [bit] NULL,
			        [CurrentBalance] decimal (18, 0) NULL,[JournalCodeRef] [nvarchar] (4000) NULL,
			        [ParentRef] [nvarchar] (4000) NULL,[CurrencyRef] [nvarchar] (4000) NULL,
			        [TaxCodeRef] [nvarchar] (4000) NULL,[CurrentBalanceSpecified] [bit] NULL,
			        [CurrentBalanceWithSubAccounts] [decimal](18, 0) NULL,[OpeningBalance] [decimal](18, 0) NULL,
			        [CurrentBalanceWithSubAccountsSpecified] [bit] NULL,[TaxAccount] [bit] NULL,
			        [TaxAccountSpecified] [bit] NULL,[OnlineBankingEnabled] [bit] NULL,
			        [OnlineBankingEnabledSpecified] [bit] NULL,[FIName] [nvarchar] (4000) NULL,
			        [BankNum] [nvarchar] (4000) NULL,[AcctNumExtn] [nvarchar] (4000) NULL,
			        [AcctNum] [nvarchar] (4000) NULL,[Name] [nvarchar] (4000) NULL,
			        [SubAccount] [bit] NULL,[SubAccountSpecified] [bit] NULL,
			        [Description] [nvarchar] (4000) NULL,[FullyQualifiedName] [nvarchar] (4000) NULL,
			        [AccountAlias] [nvarchar] (4000) NULL,[TxnLocationType] [nvarchar] (4000) NULL,
			        [Active] [bit] NULL,[ActiveSpecified] [bit] NULL,[Classification] [int] NULL,
			        [ClassificationSpecified] [bit] NULL,[AccountType] [int] NULL,
			        [AccountTypeSpecified] [bit] NULL,[AccountSubType] [nvarchar] (4000) NULL,
			        [AccountEx] [nvarchar] (4000) NULL,[OrgName] [nvarchar] (4000) NULL) ON[PRIMARY];

            CREATE TABLE [QuickBooks_" + _companyName + @"].[dbo].[Item](
            [Id][nvarchar](4000) PRIMARY KEY NOT NULL,[QbId] [int] NOT NULL,
           [SyncToken] [nvarchar] (4000) NULL,[MetaData] [nvarchar] (4000) NULL,
			[CustomField] [nvarchar] (4000) NULL,[AttachableRef] [nvarchar] (4000) NULL,
			[domain] [nvarchar] (4000) NULL,[status] [int] NULL,
			[statusSpecified] [bit] NULL,[sparse] [bit] NULL,
			[sparseSpecified] [bit] NULL,[NameAndId] [nvarchar] (4000) NULL,
			[Overview] [nvarchar] (4000) NULL,[HeaderLite] [nvarchar] (4000) NULL,
			[HeaderFull] [nvarchar] (4000) NULL,[SalesTaxCodeRef] [nvarchar] (4000) NULL,
			[DepositToAccountRef] [nvarchar] (4000) NULL,[ManPartNum] [nvarchar] (4000) NULL,
			[ReorderPointSpecified] [bit] NULL,[ReorderPoint] [decimal](18, 0) NULL,
			[QtyOnSalesOrderSpecified] [bit] NULL,[PurchaseTaxCodeRef] [nvarchar] (4000) NULL,
			[QtyOnSalesOrder] [decimal](18, 0) NULL,[QtyOnPurchaseOrder] [decimal](18, 0) NULL,
			[QtyOnHandSpecified] [bit] NULL,[QtyOnHand] [decimal](18, 0) NULL,
			[TrackQtyOnHandSpecified] [bit] NULL,[TrackQtyOnHand] [bit] NULL,
			[AvgCostSpecified] [bit] NULL,[QtyOnPurchaseOrderSpecified] [bit] NULL,
			[AvgCost] [decimal](18, 0) NULL,[InvStartDate] [datetime2] (7) NULL,
			[BuildPoint] [decimal](18, 0) NULL,[ServiceType] [nvarchar] (4000) NULL,
			[ReverseChargeRateSpecified] [bit] NULL,[ReverseChargeRate] [decimal](18, 0) NULL,
			[AbatementRateSpecified] [bit] NULL,[AbatementRate] [decimal](18, 0) NULL,
			[ParentRef] [nvarchar] (4000) NULL,[ItemAssemblyDetail] [nvarchar] (4000) NULL,
			[InvStartDateSpecified] [bit] NULL,[ItemGroupDetail] [nvarchar] (4000) NULL,
			[SpecialItemType] [int] NULL,[SpecialItemSpecified] [bit] NULL,
			[SpecialItem] [bit] NULL,[PrintGroupedItemsSpecified] [bit] NULL,
			[AssetAccountRef] [nvarchar] (4000) NULL,[PrintGroupedItems] [bit] NULL,
			[BuildPointSpecified] [bit] NULL,[SpecialItemTypeSpecified] [bit] NULL,
			[ItemCategoryType] [nvarchar] (4000) NULL,[PrefVendorRef] [nvarchar] (4000) NULL,
			[COGSAccountRef] [nvarchar] (4000) NULL,[SalesTaxIncluded] [bit] NULL,
			[ExpenseAccountRef] [nvarchar] (4000) NULL,[TaxableSpecified] [bit] NULL,
			[Taxable] [bit] NULL,[FullyQualifiedName] [nvarchar] (4000) NULL,
			[LevelSpecified] [bit] NULL,[Level] [int] NULL,[SalesTaxIncludedSpecified] [bit] NULL,
			[SubItem] [bit] NULL,[ActiveSpecified] [bit] NULL,[Active] [bit] NULL,
			[IncomeAccountRef] [nvarchar] (4000) NULL,[Description] [nvarchar] (4000) NULL,
			[Sku] [nvarchar] (200) NULL,[Name] [nvarchar] (200) NULL,
			[SubItemSpecified] [bit] NULL,[PercentBased] [bit] NULL,
			[UnitPrice] [decimal](18, 0) NULL,[PurchaseCostSpecified] [bit] NULL,
			[PurchaseCost] [decimal](18, 0) NULL,[PurchaseTaxIncludedSpecified] [bit] NULL,
			[PaymentMethodRef] [nvarchar] (4000) NULL,[PurchaseTaxIncluded] [bit] NULL,
			[PurchaseDesc] [nvarchar] (4000) NULL,[PercentBasedSpecified] [bit] NULL,
			[TypeSpecified] [bit] NULL,[Type] [int] NULL,[RatePercentSpecified] [bit] NULL,
			[RatePercent] [decimal](18, 0) NULL,[UnitPriceSpecified] [bit] NULL,
			[UOMSetRef] [nvarchar] (4000) NULL,[ItemEx] [nvarchar] (4000) NULL,
			[OrgName] [nvarchar] (4000) NULL) ON[PRIMARY];

            CREATE TABLE [QuickBooks_" + _companyName + @"].[dbo].[QBReport](
            [Id][nvarchar](4000) PRIMARY KEY NOT NULL, [CreationDate] [datetime] NULL,
		    [Date] [nvarchar] (4000) NULL,[TransactionType] [nvarchar] (4000) NULL,
            [Num] [nvarchar] (4000) NULL, [Adj] [nvarchar] (4000) NULL,
            [CreateDate] [nvarchar] (4000) NULL,[CreatedBy] [nvarchar] (4000) NULL,
            [LastModified] [nvarchar] (4000) NULL,[LastModifiedBy] [nvarchar] (4000) NULL,
            [Name] [nvarchar] (4000) NULL, [Customer] [nvarchar] (4000) NULL,
		    [Vendor] [nvarchar] (4000) NULL, [Employee] [nvarchar] (4000) NULL,
           [ProductService] [nvarchar] (4000) NULL, [MemoDescription] [nvarchar] (4000) NULL,
           [Qty] [nvarchar] (4000) NULL,[Rate] [nvarchar] (4000) NULL,
           [Account] [nvarchar] (4000) NULL,[Split] [nvarchar] (4000) NULL,
           [InvoiceDate] [nvarchar] (4000) NULL, [ARPaid] [nvarchar] (4000) NULL,
           [APPaid] [nvarchar] (4000) NULL,[Clr] [nvarchar] (4000) NULL,
           [CheckPrinted] [nvarchar] (4000) NULL,[OpenBalance] [nvarchar] (4000) NULL,
           [Amount] [nvarchar] (4000) NULL,[Balance][nvarchar] (4000) NULL, 
            [TotalAmount][nvarchar] (4000) NULL, [RealmId][nvarchar] (4000) NULL ) ON[PRIMARY];	

            CREATE TABLE [QuickBooks_" + _companyName + @"].[dbo].[Invoice](
            [Id][nvarchar](4000) PRIMARY KEY NOT NULL,[QbId] [int] NOT NULL,
           [callToAction] nvarchar(4000) NULL,[invoiceStatus] [nvarchar] (4000) NULL,
			[ECloudStatusTimeStampSpecified] [bit] NULL,[ECloudStatusTimeStamp] [datetime2] (7) NULL,
			[EInvoiceStatusSpecified] [bit] NULL,[EInvoiceStatus] [int] NULL,
			[AllowOnlineACHPaymentSpecified] [bit] NULL,[invoiceStatusLog] [nvarchar] (4000) NULL,
			[AllowOnlineACHPayment] [bit] NULL,[AllowOnlineCreditCardPayment] [bit] NULL,
			[AllowOnlinePaymentSpecified] [bit] NULL,[AllowOnlinePayment] [bit] NULL,
			[AllowIPNPayment] [bit] NULL,[AllowIPNPaymentSpecified] [bit] NULL,
			[DepositSpecified] [bit] NULL,[Deposit] decimal (18, 0) NULL,
			[AllowOnlineCreditCardPaymentSpecified] [bit] NULL,[InvoiceEx] [nvarchar] (4000) NULL,
			[AnyIntuitObject] [nvarchar] (4000) NULL,[ApplyTaxAfterDiscount] [bit] NULL,
            [ApplyTaxAfterDiscountSpecified] [bit] NULL,[ARAccountRef] [nvarchar] (4000) NULL,
            [AttachableRef] [nvarchar] (4000) NULL,[AutoDocNumber] [bit] NULL,
			[AutoDocNumberSpecified] [bit] NULL,[Balance] decimal (18, 0) NULL,
			[BalanceSpecified] [bit] NULL,[BillAddr] [nvarchar] (4000) NULL,
           	[BillEmail] [nvarchar] (4000) NULL,[BillEmailCc] [nvarchar] (4000) NULL,
			[BillEmailBcc] [nvarchar] (4000) NULL,[ClassRef] [nvarchar] (4000) NULL,
            [CustomerMemo] [nvarchar] (4000) NULL,[CustomerRef] [nvarchar] (4000) NULL,
            [CustomField] [nvarchar] (4000) NULL,[DeliveryInfo] [nvarchar] (4000) NULL,
            [DepartmentRef] [nvarchar] (4000) NULL,[DepositToAccountRef] [nvarchar] (4000) NULL,
            [DiscountAmt] decimal (18, 0) NULL,[DiscountAmtSpecified] [bit] NULL,
            [DiscountRate] decimal (18, 0) NULL,[DiscountRateSpecified] [bit] NULL,
            [DocNumber] [nvarchar] (4000) NULL,[domain] [nvarchar] (4000) NULL,
            [DueDate] [datetime2] (7) NULL,[DueDateSpecified] [bit] NULL,			
			[EmailStatus] [int] NULL,[EmailStatusSpecified] [bit] NULL,
            [ExchangeRate] decimal (18, 0) NULL,[ExchangeRateSpecified] [bit] NULL,
            [FinanceCharge] [bit] NULL,[FinanceChargeSpecified] [bit] NULL,
            [FOB] [nvarchar] (4000) NULL,[GlobalTaxCalculation] [int] NULL,
			[GlobalTaxCalculationSpecified] [bit] NULL,[GovtTxnRefIdentifier] [nvarchar] (4000) NULL,
            [HeaderFull] [nvarchar] (4000) NULL,[HeaderLite] [nvarchar] (4000) NULL,
            [HomeBalance] decimal (18, 0) NULL,[HomeBalanceSpecified] [bit] NULL,
            [HomeTotalAmt] decimal (18, 0) NULL,[HomeTotalAmtSpecified] [bit] NULL,
            [Line] [nvarchar] (4000) NULL,[LinkedTxn] [nvarchar] (4000) NULL,
            [MetaData] [nvarchar] (4000) NULL,[NameAndId] [nvarchar] (4000) NULL,
            [Overview] [nvarchar] (4000) NULL,[PaymentMethodRef] [nvarchar] (4000) NULL,
            [PaymentRefNum] [nvarchar] (4000) NULL,[PaymentType] [int] NULL,
            [PaymentTypeSpecified] [bit] NULL,[PONumber] [nvarchar] (4000) NULL,
			[PrintStatus] [int] NULL,[PrintStatusSpecified] [bit] NULL,             
            [PrivateNote] [nvarchar] (4000) NULL,[RemitToRef] [nvarchar] (4000) NULL,			
            [SalesRepRef] [nvarchar] (4000) NULL,[SalesTermRef] [nvarchar] (4000) NULL,
          	[ShipAddr] [nvarchar] (4000) NULL,[ShipDate] [datetime2] (7) NULL,
			[ShipDateSpecified] [bit] NULL,[ShipMethodRef] [nvarchar] (4000) NULL,
            [sparse] [bit] NULL,[sparseSpecified] [bit] NULL,[status] [int] NULL,
			[statusSpecified] [bit] NULL,[SyncToken] [nvarchar] (4000) NULL,
            [TaxFormNum] [nvarchar] (4000) NULL,[TaxFormType] [nvarchar] (4000) NULL,
            [TemplateRef] [nvarchar] (4000) NULL,[TotalAmt] decimal (18, 0) NULL,
			[TotalAmtSpecified] [bit] NULL,[TrackingNum] [nvarchar] (4000) NULL,	
            [TransactionLocationType] [nvarchar] (4000) NULL,
            [TxnDateSpecified] [bit] NULL,[TxnDate] [datetime2] (7) NULL,	
            [TxnSource] [nvarchar] (4000) NULL,[TxnStatus] [nvarchar] (4000) NULL,
            [TxnTaxDetail] [nvarchar] (4000) NULL,[CurrencyRef] [nvarchar] (4000) NULL,
			[OrgName] [nvarchar] (4000) NULL) ON[PRIMARY];

            CREATE TABLE [QuickBooks_" + _companyName + @"].[dbo].[JournalEntry](
            [Id][nvarchar](4000) PRIMARY KEY NOT NULL,[QbId] [int] NULL,
			[SyncToken] [nvarchar] (4000) NULL,[MetaData] [nvarchar] (4000) NULL,
			[CustomField] [nvarchar] (4000) NULL,[AttachableRef] [nvarchar] (4000) NULL,
			[domain] [nvarchar] (4000) NULL,[status] [int] NULL,
			[statusSpecified] [bit] NULL,[sparse] [bit] NULL,
			[sparseSpecified] [bit] NULL,[NameAndId] [nvarchar] (4000) NULL,
			[Overview] [nvarchar] (4000) NULL,[HeaderLite] [nvarchar] (4000) NULL,
			[HeaderFull] [nvarchar] (4000) NULL,[Adjustment] [bit] NULL,
			[AdjustmentSpecified] [bit] NULL,[HomeCurrencyAdjustment] [bit] NULL,
			[HomeCurrencyAdjustmentSpecified] [bit] NULL,[EnteredInHomeCurrency] [bit] NULL,
			[EnteredInHomeCurrencySpecified] [bit] NULL,[TotalAmt] [decimal](18, 0) NULL,
			[TotalAmtSpecified] [bit] NULL,[HomeTotalAmt] [decimal](18, 0) NULL,
			[HomeTotalAmtSpecified] [bit] NULL,[JournalEntryEx] [nvarchar] (4000) NULL,
			[TaxFormType] [nvarchar] (4000) NULL,[TxnSource] [nvarchar] (4000) NULL,
			[TxnTaxDetail] [nvarchar] (4000) NULL,[TxnStatus] [nvarchar] (4000) NULL,
			[TaxFormNum] [nvarchar] (4000) NULL,[PrivateNote] [nvarchar] (4000) NULL,
			[ExchangeRate] [decimal](18, 0) NULL,[CurrencyRef] [nvarchar] (4000) NULL,
			[DepartmentRef] [nvarchar] (4000) NULL,[Line] [nvarchar] (4000) NULL,
			[LinkedTxn] [nvarchar] (4000) NULL,[TxnDateSpecified] [bit] NULL,
			[TxnDate] [datetime2] (7) NULL,[DocNumber] [nvarchar] (4000) NULL,
			[ExchangeRateSpecified] [bit] NULL,[TransactionLocationType] [nvarchar] (4000) NULL,
			[OrgName] [nvarchar] (4000) NULL) ON[PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create QB  tables db for {_companyName} exception:{ex.Message}");
            }
        }

    }
}
