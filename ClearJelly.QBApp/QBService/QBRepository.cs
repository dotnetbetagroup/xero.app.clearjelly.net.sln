﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Web.Script.Serialization;
using DapperExtensions;
using ClearJelly.ViewModels;
using System.Data;
using NLog;
using System.ComponentModel;
using ClearJelly.Entities;
using ViewModels;

namespace ClearJelly.QBApp.QBService
{
    public class QBRepository
    {
        private string _connection;
        Logger _logger;
        public QBRepository(string connectionString)
        {
            _connection = connectionString;
            _logger = LogManager.GetCurrentClassLogger();
        }
        protected SqlConnection DatabaseConnection
        {
            get
            {
                var cn = new SqlConnection(_connection);
                return cn;
            }
        }

        public async System.Threading.Tasks.Task ClearTable(string table, string orgName)
        {
            _logger.Error("Executing QBRepository.ClearTable");
            try
            {
                using (IDbConnection _dbConnection = new SqlConnection(_connection))
                {
                    string query = $"DELETE FROM {table} WHERE OrgName=@OrgName";
                    _dbConnection.Execute(query, new { OrgName = orgName });
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error while clear tabl at QBRepository.ClearTable: " + ex.Message);
                throw;
            }
        }

        public async System.Threading.Tasks.Task InsertAccount(List<Account> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertAccount.");
            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allAccountList = new List<QuickBookAccountViewModel>();
                    var mappedList = QbMappers.CompleteAccountModel(items, orgName);
                    allAccountList.AddRange(mappedList);
                    SaveAccounts(allAccountList);
                    allAccountList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("QBRepository exception:" + ex.Message);
                }
            }
        }
        public async System.Threading.Tasks.Task InsertItem(List<Item> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertItem.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allItemList = new List<QuickBookItemsViewModel>();
                    var mappedList = QbMappers.CompleteItemtModel(items, orgName);
                    allItemList.AddRange(mappedList);
                    SaveItems(allItemList);
                    allItemList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertItem exception:" + ex.Message);
                }
            }
        }

        //public async System.Threading.Tasks.Task InsertCompany(List<Company> companies, string orgName)
        //{
        //    _logger.Info("Executing QBRepository InsertItem.");

        //    using (var con = new SqlConnection(_connection))
        //    {
        //        try
        //        {
        //            var allItemList = new List<QuickBookItemsViewModel>();
        //            var mappedList = QbMappers.CompleteItemtModel(items, orgName);
        //            allItemList.AddRange(mappedList);
        //            SaveItems(allItemList);
        //            allItemList.Clear();
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error("InsertItem exception:" + ex.Message);
        //        }
        //    }
        //}

        public async System.Threading.Tasks.Task InsertJournalEntry(List<JournalEntry> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertJournalEntry.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allJouralList = new List<QuickBookJournalViewModel>();
                    var mappedList = QbMappers.CompleteJouralModel(items, orgName);
                    allJouralList.AddRange(mappedList);
                    SaveJournas(allJouralList);
                    allJouralList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertJournalEntry exception:" + ex.Message);
                }
            }
        }

        public async System.Threading.Tasks.Task InsertInvoice(List<Invoice> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertInvoice.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allInvoiceList = new List<QuickBookInvoiceViewModel>();
                    var mappedList = QbMappers.CompleteInvoiceModel(items, orgName);
                    allInvoiceList.AddRange(mappedList);
                    SaveInvoices(allInvoiceList);
                    allInvoiceList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertInvoice exception:" + ex.Message);
                }
            }
        }


        public void InsertInvoiceOLD(List<Invoice> items)
        {
            using (SqlConnection cn = new SqlConnection(_connection))
            {
                cn.Open();
                foreach (var item in items)
                {
                    try
                    {
                        cn.Insert(item);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("InsertInvoiceOLD exception:" + ex.Message);
                    }

                }
                cn.Close();
            }
        }
        public void SaveAccounts(List<QuickBookAccountViewModel> accountList)
        {
            _logger.Info("Executing QBRepository SaveAccounts.");

            var dt = ConvertAccountToDataTable(accountList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        sqlBulkCopy.ColumnMappings.Add("QbId", "QbId");
                        sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                        sqlBulkCopy.ColumnMappings.Add("SyncToken", "SyncToken");
                        sqlBulkCopy.ColumnMappings.Add("MetaData", "MetaData");
                        sqlBulkCopy.ColumnMappings.Add("CustomField", "CustomField");
                        sqlBulkCopy.ColumnMappings.Add("AttachableRef", "AttachableRef");
                        sqlBulkCopy.ColumnMappings.Add("domain", "domain");
                        sqlBulkCopy.ColumnMappings.Add("status", "status");
                        sqlBulkCopy.ColumnMappings.Add("statusSpecified", "statusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("sparse", "sparse");
                        sqlBulkCopy.ColumnMappings.Add("sparseSpecified", "sparseSpecified");
                        sqlBulkCopy.ColumnMappings.Add("NameAndId", "NameAndId");
                        sqlBulkCopy.ColumnMappings.Add("Overview", "Overview");
                        sqlBulkCopy.ColumnMappings.Add("HeaderLite", "HeaderLite");
                        sqlBulkCopy.ColumnMappings.Add("HeaderFull", "HeaderFull");
                        sqlBulkCopy.ColumnMappings.Add("OpeningBalanceSpecified", "OpeningBalanceSpecified");
                        sqlBulkCopy.ColumnMappings.Add("OpeningBalanceDate", "OpeningBalanceDate");
                        sqlBulkCopy.ColumnMappings.Add("OpeningBalanceDateSpecified", "OpeningBalanceDateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("CurrentBalance", "CurrentBalance");
                        sqlBulkCopy.ColumnMappings.Add("CurrentBalanceSpecified", "CurrentBalanceSpecified");
                        sqlBulkCopy.ColumnMappings.Add("CurrentBalanceWithSubAccounts", "CurrentBalanceWithSubAccounts");
                        sqlBulkCopy.ColumnMappings.Add("OpeningBalance", "OpeningBalance");
                        sqlBulkCopy.ColumnMappings.Add("CurrentBalanceWithSubAccountsSpecified", "CurrentBalanceWithSubAccountsSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TaxAccount", "TaxAccount");
                        sqlBulkCopy.ColumnMappings.Add("TaxAccountSpecified", "TaxAccountSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TaxCodeRef", "TaxCodeRef");
                        sqlBulkCopy.ColumnMappings.Add("OnlineBankingEnabled", "OnlineBankingEnabled");
                        sqlBulkCopy.ColumnMappings.Add("OnlineBankingEnabledSpecified", "OnlineBankingEnabledSpecified");
                        sqlBulkCopy.ColumnMappings.Add("FIName", "FIName");
                        sqlBulkCopy.ColumnMappings.Add("CurrencyRef", "CurrencyRef");
                        sqlBulkCopy.ColumnMappings.Add("BankNum", "BankNum");
                        sqlBulkCopy.ColumnMappings.Add("AcctNumExtn", "AcctNumExtn");
                        sqlBulkCopy.ColumnMappings.Add("AcctNum", "AcctNum");
                        sqlBulkCopy.ColumnMappings.Add("Name", "Name");
                        sqlBulkCopy.ColumnMappings.Add("SubAccount", "SubAccount");
                        sqlBulkCopy.ColumnMappings.Add("SubAccountSpecified", "SubAccountSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ParentRef", "ParentRef");
                        sqlBulkCopy.ColumnMappings.Add("Description", "Description");
                        sqlBulkCopy.ColumnMappings.Add("FullyQualifiedName", "FullyQualifiedName");
                        sqlBulkCopy.ColumnMappings.Add("AccountAlias", "AccountAlias");
                        sqlBulkCopy.ColumnMappings.Add("TxnLocationType", "TxnLocationType");
                        sqlBulkCopy.ColumnMappings.Add("Active", "Active");
                        sqlBulkCopy.ColumnMappings.Add("ActiveSpecified", "ActiveSpecified");
                        sqlBulkCopy.ColumnMappings.Add("Classification", "Classification");
                        sqlBulkCopy.ColumnMappings.Add("ClassificationSpecified", "ClassificationSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AccountType", "AccountType");
                        sqlBulkCopy.ColumnMappings.Add("AccountTypeSpecified", "AccountTypeSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AccountSubType", "AccountSubType");
                        sqlBulkCopy.ColumnMappings.Add("JournalCodeRef", "JournalCodeRef");
                        sqlBulkCopy.ColumnMappings.Add("AccountEx", "AccountEx");
                        sqlBulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                        sqlBulkCopy.DestinationTableName = "Account";
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveAccounts exception:" + ex.Message);
                }

            }
        }
        public void SaveItems(List<QuickBookItemsViewModel> itemList)
        {
            _logger.Info("Executing QBRepository SaveItems.");

            var dt = ConvertItemToDataTable(itemList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        sqlBulkCopy.ColumnMappings.Add("QbId", "QbId");
                        sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                        sqlBulkCopy.ColumnMappings.Add("SyncToken", "SyncToken");
                        sqlBulkCopy.ColumnMappings.Add("sparseSpecified", "sparseSpecified");
                        sqlBulkCopy.ColumnMappings.Add("MetaData", "MetaData");
                        sqlBulkCopy.ColumnMappings.Add("CustomField", "CustomField");
                        sqlBulkCopy.ColumnMappings.Add("AttachableRef", "AttachableRef");
                        sqlBulkCopy.ColumnMappings.Add("domain", "domain");
                        sqlBulkCopy.ColumnMappings.Add("status", "status");
                        sqlBulkCopy.ColumnMappings.Add("statusSpecified", "statusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("sparse", "sparse");
                        sqlBulkCopy.ColumnMappings.Add("NameAndId", "NameAndId");
                        sqlBulkCopy.ColumnMappings.Add("Overview", "Overview");
                        sqlBulkCopy.ColumnMappings.Add("HeaderLite", "HeaderLite");
                        sqlBulkCopy.ColumnMappings.Add("HeaderFull", "HeaderFull");
                        sqlBulkCopy.ColumnMappings.Add("SalesTaxCodeRef", "SalesTaxCodeRef");
                        sqlBulkCopy.ColumnMappings.Add("DepositToAccountRef", "DepositToAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("ManPartNum", "ManPartNum");
                        sqlBulkCopy.ColumnMappings.Add("ReorderPointSpecified", "ReorderPointSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ReorderPoint", "ReorderPoint");
                        sqlBulkCopy.ColumnMappings.Add("QtyOnSalesOrderSpecified", "QtyOnSalesOrderSpecified");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseTaxCodeRef", "PurchaseTaxCodeRef");
                        sqlBulkCopy.ColumnMappings.Add("QtyOnSalesOrder", "QtyOnSalesOrder");
                        sqlBulkCopy.ColumnMappings.Add("QtyOnPurchaseOrder", "QtyOnPurchaseOrder");
                        sqlBulkCopy.ColumnMappings.Add("QtyOnHandSpecified", "QtyOnHandSpecified");
                        sqlBulkCopy.ColumnMappings.Add("QtyOnHand", "QtyOnHand");
                        sqlBulkCopy.ColumnMappings.Add("TrackQtyOnHandSpecified", "TrackQtyOnHandSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TrackQtyOnHand", "TrackQtyOnHand");
                        sqlBulkCopy.ColumnMappings.Add("AvgCostSpecified", "AvgCostSpecified");
                        sqlBulkCopy.ColumnMappings.Add("QtyOnPurchaseOrderSpecified", "QtyOnPurchaseOrderSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AvgCost", "AvgCost");
                        sqlBulkCopy.ColumnMappings.Add("InvStartDate", "InvStartDate");
                        sqlBulkCopy.ColumnMappings.Add("BuildPoint", "BuildPoint");
                        sqlBulkCopy.ColumnMappings.Add("ServiceType", "ServiceType");
                        sqlBulkCopy.ColumnMappings.Add("ReverseChargeRateSpecified", "ReverseChargeRateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ReverseChargeRate", "ReverseChargeRate");
                        sqlBulkCopy.ColumnMappings.Add("AbatementRateSpecified", "AbatementRateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AbatementRate", "AbatementRate");
                        sqlBulkCopy.ColumnMappings.Add("ItemAssemblyDetail", "ItemAssemblyDetail");
                        sqlBulkCopy.ColumnMappings.Add("InvStartDateSpecified", "InvStartDateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ItemGroupDetail", "ItemGroupDetail");
                        sqlBulkCopy.ColumnMappings.Add("SpecialItemType", "SpecialItemType");
                        sqlBulkCopy.ColumnMappings.Add("SpecialItemSpecified", "SpecialItemSpecified");
                        sqlBulkCopy.ColumnMappings.Add("SpecialItem", "SpecialItem");
                        sqlBulkCopy.ColumnMappings.Add("PrintGroupedItemsSpecified", "PrintGroupedItemsSpecified");
                        sqlBulkCopy.ColumnMappings.Add("PrintGroupedItems", "PrintGroupedItems");
                        sqlBulkCopy.ColumnMappings.Add("BuildPointSpecified", "BuildPointSpecified");
                        sqlBulkCopy.ColumnMappings.Add("SpecialItemTypeSpecified", "SpecialItemTypeSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ItemCategoryType", "ItemCategoryType");
                        sqlBulkCopy.ColumnMappings.Add("PrefVendorRef", "PrefVendorRef");
                        sqlBulkCopy.ColumnMappings.Add("COGSAccountRef", "COGSAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("SalesTaxIncluded", "SalesTaxIncluded");
                        sqlBulkCopy.ColumnMappings.Add("TaxableSpecified", "TaxableSpecified");
                        sqlBulkCopy.ColumnMappings.Add("Taxable", "Taxable");
                        sqlBulkCopy.ColumnMappings.Add("FullyQualifiedName", "FullyQualifiedName");
                        sqlBulkCopy.ColumnMappings.Add("LevelSpecified", "LevelSpecified");
                        sqlBulkCopy.ColumnMappings.Add("Level", "Level");
                        sqlBulkCopy.ColumnMappings.Add("ParentRef", "ParentRef");
                        sqlBulkCopy.ColumnMappings.Add("SalesTaxIncludedSpecified", "SalesTaxIncludedSpecified");
                        sqlBulkCopy.ColumnMappings.Add("SubItem", "SubItem");
                        sqlBulkCopy.ColumnMappings.Add("ActiveSpecified", "ActiveSpecified");
                        sqlBulkCopy.ColumnMappings.Add("Active", "Active");
                        sqlBulkCopy.ColumnMappings.Add("Description", "Description");
                        sqlBulkCopy.ColumnMappings.Add("Sku", "Sku");
                        sqlBulkCopy.ColumnMappings.Add("Name", "Name");
                        sqlBulkCopy.ColumnMappings.Add("SubItemSpecified", "SubItemSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AssetAccountRef", "AssetAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("PercentBased", "PercentBased");
                        sqlBulkCopy.ColumnMappings.Add("UnitPrice", "UnitPrice");
                        sqlBulkCopy.ColumnMappings.Add("ExpenseAccountRef", "ExpenseAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseCostSpecified", "PurchaseCostSpecified");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseCost", "PurchaseCost");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseTaxIncludedSpecified", "PurchaseTaxIncludedSpecified");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseTaxIncluded", "PurchaseTaxIncluded");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseDesc", "PurchaseDesc");
                        sqlBulkCopy.ColumnMappings.Add("PercentBasedSpecified", "PercentBasedSpecified");
                        sqlBulkCopy.ColumnMappings.Add("IncomeAccountRef", "IncomeAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("PaymentMethodRef", "PaymentMethodRef");
                        sqlBulkCopy.ColumnMappings.Add("TypeSpecified", "TypeSpecified");
                        sqlBulkCopy.ColumnMappings.Add("Type", "Type");
                        sqlBulkCopy.ColumnMappings.Add("RatePercentSpecified", "RatePercentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("RatePercent", "RatePercent");
                        sqlBulkCopy.ColumnMappings.Add("UnitPriceSpecified", "UnitPriceSpecified");
                        sqlBulkCopy.ColumnMappings.Add("UOMSetRef", "UOMSetRef");
                        sqlBulkCopy.ColumnMappings.Add("ItemEx", "ItemEx");
                        sqlBulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                        sqlBulkCopy.DestinationTableName = "Item";
                        sqlBulkCopy.WriteToServer(dt);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveAccounts exception:" + ex.Message);
                }

            }
        }
        public void SaveJournas(List<QuickBookJournalViewModel> journalList)
        {
            var dt = ConvertJournalToDataTable(journalList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        sqlBulkCopy.ColumnMappings.Add("QbId", "QbId");
                        sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                        sqlBulkCopy.ColumnMappings.Add("SyncToken", "SyncToken");
                        sqlBulkCopy.ColumnMappings.Add("sparseSpecified", "sparseSpecified");
                        sqlBulkCopy.ColumnMappings.Add("MetaData", "MetaData");
                        sqlBulkCopy.ColumnMappings.Add("CustomField", "CustomField");
                        sqlBulkCopy.ColumnMappings.Add("AttachableRef", "AttachableRef");
                        sqlBulkCopy.ColumnMappings.Add("domain", "domain");
                        sqlBulkCopy.ColumnMappings.Add("status", "status");
                        sqlBulkCopy.ColumnMappings.Add("statusSpecified", "statusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("sparse", "sparse");
                        sqlBulkCopy.ColumnMappings.Add("NameAndId", "NameAndId");
                        sqlBulkCopy.ColumnMappings.Add("Overview", "Overview");
                        sqlBulkCopy.ColumnMappings.Add("HeaderLite", "HeaderLite");
                        sqlBulkCopy.ColumnMappings.Add("HeaderFull", "HeaderFull");
                        sqlBulkCopy.ColumnMappings.Add("Adjustment", "Adjustment");
                        sqlBulkCopy.ColumnMappings.Add("AdjustmentSpecified", "AdjustmentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("HomeCurrencyAdjustment", "HomeCurrencyAdjustment");
                        sqlBulkCopy.ColumnMappings.Add("HomeCurrencyAdjustmentSpecified", "HomeCurrencyAdjustmentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("EnteredInHomeCurrency", "EnteredInHomeCurrency");
                        sqlBulkCopy.ColumnMappings.Add("EnteredInHomeCurrencySpecified", "EnteredInHomeCurrencySpecified");
                        sqlBulkCopy.ColumnMappings.Add("TotalAmt", "TotalAmt");
                        sqlBulkCopy.ColumnMappings.Add("TotalAmtSpecified", "TotalAmtSpecified");
                        sqlBulkCopy.ColumnMappings.Add("HomeTotalAmt", "HomeTotalAmt");
                        sqlBulkCopy.ColumnMappings.Add("HomeTotalAmtSpecified", "HomeTotalAmtSpecified");
                        sqlBulkCopy.ColumnMappings.Add("JournalEntryEx", "JournalEntryEx");
                        sqlBulkCopy.ColumnMappings.Add("TaxFormType", "TaxFormType");
                        sqlBulkCopy.ColumnMappings.Add("CurrencyRef", "CurrencyRef");
                        sqlBulkCopy.ColumnMappings.Add("TxnSource", "TxnSource");
                        sqlBulkCopy.ColumnMappings.Add("TxnTaxDetail", "TxnTaxDetail");
                        sqlBulkCopy.ColumnMappings.Add("Line", "Line");
                        sqlBulkCopy.ColumnMappings.Add("LinkedTxn", "LinkedTxn");
                        sqlBulkCopy.ColumnMappings.Add("TxnStatus", "TxnStatus");
                        sqlBulkCopy.ColumnMappings.Add("TaxFormNum", "TaxFormNum");
                        sqlBulkCopy.ColumnMappings.Add("PrivateNote", "PrivateNote");
                        sqlBulkCopy.ColumnMappings.Add("ExchangeRate", "ExchangeRate");
                        sqlBulkCopy.ColumnMappings.Add("DepartmentRef", "DepartmentRef");
                        sqlBulkCopy.ColumnMappings.Add("TxnDateSpecified", "TxnDateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TxnDate", "TxnDate");
                        sqlBulkCopy.ColumnMappings.Add("DocNumber", "DocNumber");
                        sqlBulkCopy.ColumnMappings.Add("ExchangeRateSpecified", "ExchangeRateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TransactionLocationType", "TransactionLocationType");
                        sqlBulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                        sqlBulkCopy.DestinationTableName = "JournalEntry";
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveJournas exception:" + ex.Message);

                }

            }
        }
        public void SaveInvoices(List<QuickBookInvoiceViewModel> invoiceList)
        {
            var dt = ConvertInvoiceToDataTable(invoiceList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {

                        sqlBulkCopy.ColumnMappings.Add("QbId", "QbId");
                        sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                        sqlBulkCopy.ColumnMappings.Add("SyncToken", "SyncToken");
                        sqlBulkCopy.ColumnMappings.Add("MetaData", "MetaData");
                        sqlBulkCopy.ColumnMappings.Add("CustomField", "CustomField");
                        sqlBulkCopy.ColumnMappings.Add("AttachableRef", "AttachableRef");
                        sqlBulkCopy.ColumnMappings.Add("domain", "domain");
                        sqlBulkCopy.ColumnMappings.Add("status", "status");
                        sqlBulkCopy.ColumnMappings.Add("statusSpecified", "statusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("sparse", "sparse");
                        sqlBulkCopy.ColumnMappings.Add("sparseSpecified", "sparseSpecified");
                        sqlBulkCopy.ColumnMappings.Add("NameAndId", "NameAndId");
                        sqlBulkCopy.ColumnMappings.Add("Overview", "Overview");
                        sqlBulkCopy.ColumnMappings.Add("HeaderLite", "HeaderLite");
                        sqlBulkCopy.ColumnMappings.Add("HeaderFull", "HeaderFull");
                        sqlBulkCopy.ColumnMappings.Add("TaxFormType", "TaxFormType");
                        sqlBulkCopy.ColumnMappings.Add("TxnSource", "TxnSource");
                        sqlBulkCopy.ColumnMappings.Add("TxnTaxDetail", "TxnTaxDetail");
                        sqlBulkCopy.ColumnMappings.Add("Line", "Line");
                        sqlBulkCopy.ColumnMappings.Add("LinkedTxn", "LinkedTxn");
                        sqlBulkCopy.ColumnMappings.Add("TxnStatus", "TxnStatus");
                        sqlBulkCopy.ColumnMappings.Add("TaxFormNum", "TaxFormNum");
                        sqlBulkCopy.ColumnMappings.Add("PrivateNote", "PrivateNote");
                        sqlBulkCopy.ColumnMappings.Add("ExchangeRate", "ExchangeRate");
                        sqlBulkCopy.ColumnMappings.Add("DepartmentRef", "DepartmentRef");
                        sqlBulkCopy.ColumnMappings.Add("TxnDateSpecified", "TxnDateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TxnDate", "TxnDate");
                        sqlBulkCopy.ColumnMappings.Add("DocNumber", "DocNumber");
                        sqlBulkCopy.ColumnMappings.Add("ExchangeRateSpecified", "ExchangeRateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TransactionLocationType", "TransactionLocationType");
                        sqlBulkCopy.ColumnMappings.Add("PrintStatusSpecified", "PrintStatusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("EmailStatus", "EmailStatus");
                        sqlBulkCopy.ColumnMappings.Add("EmailStatusSpecified", "EmailStatusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("BillEmail", "BillEmail");
                        sqlBulkCopy.ColumnMappings.Add("BillEmailCc", "BillEmailCc");
                        sqlBulkCopy.ColumnMappings.Add("BillEmailBcc", "BillEmailBcc");
                        sqlBulkCopy.ColumnMappings.Add("ARAccountRef", "ARAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("Balance", "Balance");
                        sqlBulkCopy.ColumnMappings.Add("BalanceSpecified", "BalanceSpecified");
                        sqlBulkCopy.ColumnMappings.Add("HomeBalance", "HomeBalance");
                        sqlBulkCopy.ColumnMappings.Add("HomeBalanceSpecified", "HomeBalanceSpecified");
                        sqlBulkCopy.ColumnMappings.Add("FinanceCharge", "FinanceCharge");
                        sqlBulkCopy.ColumnMappings.Add("FinanceChargeSpecified", "FinanceChargeSpecified");
                        sqlBulkCopy.ColumnMappings.Add("PaymentMethodRef", "PaymentMethodRef");
                        sqlBulkCopy.ColumnMappings.Add("PaymentRefNum", "PaymentRefNum");
                        sqlBulkCopy.ColumnMappings.Add("PaymentType", "PaymentType");
                        sqlBulkCopy.ColumnMappings.Add("PaymentTypeSpecified", "PaymentTypeSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AnyIntuitObject", "AnyIntuitObject");
                        sqlBulkCopy.ColumnMappings.Add("DepositToAccountRef", "DepositToAccountRef");
                        sqlBulkCopy.ColumnMappings.Add("DeliveryInfo", "DeliveryInfo");
                        sqlBulkCopy.ColumnMappings.Add("DiscountRate", "DiscountRate");
                        sqlBulkCopy.ColumnMappings.Add("DiscountRateSpecified", "DiscountRateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("DiscountAmt", "DiscountAmt");
                        sqlBulkCopy.ColumnMappings.Add("PrintStatus", "PrintStatus");
                        sqlBulkCopy.ColumnMappings.Add("TemplateRef", "TemplateRef");
                        sqlBulkCopy.ColumnMappings.Add("ApplyTaxAfterDiscountSpecified", "ApplyTaxAfterDiscountSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ApplyTaxAfterDiscount", "ApplyTaxAfterDiscount");
                        sqlBulkCopy.ColumnMappings.Add("AutoDocNumber", "AutoDocNumber");
                        sqlBulkCopy.ColumnMappings.Add("AutoDocNumberSpecified", "AutoDocNumberSpecified");
                        sqlBulkCopy.ColumnMappings.Add("CustomerRef", "CustomerRef");
                        sqlBulkCopy.ColumnMappings.Add("CustomerMemo", "CustomerMemo");
                        sqlBulkCopy.ColumnMappings.Add("BillAddr", "BillAddr");
                        sqlBulkCopy.ColumnMappings.Add("ShipAddr", "ShipAddr");
                        sqlBulkCopy.ColumnMappings.Add("RemitToRef", "RemitToRef");
                        sqlBulkCopy.ColumnMappings.Add("ClassRef", "ClassRef");
                        sqlBulkCopy.ColumnMappings.Add("SalesTermRef", "SalesTermRef");
                        sqlBulkCopy.ColumnMappings.Add("DueDate", "DueDate");
                        sqlBulkCopy.ColumnMappings.Add("DueDateSpecified", "DueDateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("DiscountAmtSpecified", "DiscountAmtSpecified");
                        sqlBulkCopy.ColumnMappings.Add("SalesRepRef", "SalesRepRef");
                        sqlBulkCopy.ColumnMappings.Add("FOB", "FOB");
                        sqlBulkCopy.ColumnMappings.Add("ShipMethodRef", "ShipMethodRef");
                        sqlBulkCopy.ColumnMappings.Add("ShipDate", "ShipDate");
                        sqlBulkCopy.ColumnMappings.Add("ShipDateSpecified", "ShipDateSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TrackingNum", "TrackingNum");
                        sqlBulkCopy.ColumnMappings.Add("GlobalTaxCalculation", "GlobalTaxCalculation");
                        sqlBulkCopy.ColumnMappings.Add("GlobalTaxCalculationSpecified", "GlobalTaxCalculationSpecified");
                        sqlBulkCopy.ColumnMappings.Add("TotalAmt", "TotalAmt");
                        sqlBulkCopy.ColumnMappings.Add("TotalAmtSpecified", "TotalAmtSpecified");
                        sqlBulkCopy.ColumnMappings.Add("HomeTotalAmt", "HomeTotalAmt");
                        sqlBulkCopy.ColumnMappings.Add("HomeTotalAmtSpecified", "HomeTotalAmtSpecified");
                        sqlBulkCopy.ColumnMappings.Add("PONumber", "PONumber");
                        sqlBulkCopy.ColumnMappings.Add("GovtTxnRefIdentifier", "GovtTxnRefIdentifier");
                        sqlBulkCopy.ColumnMappings.Add("callToAction", "callToAction");
                        sqlBulkCopy.ColumnMappings.Add("invoiceStatus", "invoiceStatus");
                        sqlBulkCopy.ColumnMappings.Add("ECloudStatusTimeStampSpecified", "ECloudStatusTimeStampSpecified");
                        sqlBulkCopy.ColumnMappings.Add("ECloudStatusTimeStamp", "ECloudStatusTimeStamp");
                        sqlBulkCopy.ColumnMappings.Add("EInvoiceStatusSpecified", "EInvoiceStatusSpecified");
                        sqlBulkCopy.ColumnMappings.Add("EInvoiceStatus", "EInvoiceStatus");
                        sqlBulkCopy.ColumnMappings.Add("AllowOnlineACHPaymentSpecified", "AllowOnlineACHPaymentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("invoiceStatusLog", "invoiceStatusLog");
                        sqlBulkCopy.ColumnMappings.Add("AllowOnlineACHPayment", "AllowOnlineACHPayment");
                        sqlBulkCopy.ColumnMappings.Add("AllowOnlineCreditCardPayment", "AllowOnlineCreditCardPayment");
                        sqlBulkCopy.ColumnMappings.Add("AllowOnlinePaymentSpecified", "AllowOnlinePaymentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AllowOnlinePayment", "AllowOnlinePayment");
                        sqlBulkCopy.ColumnMappings.Add("AllowIPNPaymentSpecified", "AllowIPNPaymentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("AllowIPNPayment", "AllowIPNPayment");
                        sqlBulkCopy.ColumnMappings.Add("DepositSpecified", "DepositSpecified");
                        sqlBulkCopy.ColumnMappings.Add("Deposit", "Deposit");
                        sqlBulkCopy.ColumnMappings.Add("AllowOnlineCreditCardPaymentSpecified", "AllowOnlineCreditCardPaymentSpecified");
                        sqlBulkCopy.ColumnMappings.Add("InvoiceEx", "InvoiceEx");
                        sqlBulkCopy.ColumnMappings.Add("CurrencyRef", "CurrencyRef");
                        sqlBulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                        sqlBulkCopy.DestinationTableName = "Invoice";
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveInvoices exception:" + ex.Message);

                }

            }
        }
        private DataTable ConvertInvoiceToDataTable(List<QuickBookInvoiceViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookInvoiceViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        private DataTable ConvertJournalToDataTable(List<QuickBookJournalViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookJournalViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        private DataTable ConvertItemToDataTable(List<QuickBookItemsViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookItemsViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        private DataTable ConvertAccountToDataTable(List<QuickBookAccountViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookAccountViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

    }
}

