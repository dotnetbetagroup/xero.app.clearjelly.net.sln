﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using NLog;
using System.ComponentModel;
using ClearJelly.Entities;
using ClearJelly.ViewModels;
using ViewModels;
using Intuit.Ipp.Data;
using EntityModel.Entities;
using ClearJelly.QBApp.ReportEntities;
using Dapper;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.Repositories
{
    public class QBRepository
    {
        private string _connection;
        Logger _logger;
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();
        public QBRepository(string connectionString)
        {
            _connection = connectionString;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void UpdateReport(string connection, string dbName, List<QBReport> model)
        {
            using (IDbConnection _dbConnection = new SqlConnection(connection))
            {
                string query = $"DELETE FROM [{dbName}].[dbo].[QBReport] where RealmId = {model.First().RealmId}";
                _dbConnection.Execute(query);
            }
            try
            {
                SaveDataToDb(model, "QBReport", connection);
            }
            catch (Exception ex)
            {
                _logger.Error("QBRepository exception:" + ex.Message);
            }

        }
        public void InsertReport(string connection, List<QBReport> model)
        {
            try
            {
                SaveDataToDb(model, "QBReport");
            }
            catch (Exception ex)
            {
                _logger.Error("QBRepository exception: " + ex.Message);
            }
        }

        public void UpdateRefreshToken(int userId, string rToken, string aToken, string realmId)
        {            
            using (IDbConnection db = new SqlConnection(Configuration.ConfigSection.DefaultConnection))
            {
                try
                {
                    var updateQuery = @"update [ClearJelly].[dbo].[QuickBookRefreshTokens] set RefreshToken = @RefreshToken, AccessToken = @AccessToken, CreationDate = @CreationDate, ExpirationDate = @ExpirationDate where UserId = @UserId and RealmId = @RealmId";
                    var result = db.Execute(updateQuery, new { RefreshToken = rToken, UserId = userId, RealmId = realmId, AccessToken = aToken, CreationDate = DateTime.Now, ExpirationDate = DateTime.Now.AddHours(+1) });
                }
                catch (Exception ex)
                {

                }
            }
        }
        public async System.Threading.Tasks.Task ClearTable(string table, string orgName)
        {
            _logger.Info("Executing QBRepository.ClearTable");
            try
            {
                using (IDbConnection _dbConnection = new SqlConnection(_connection))
                {
                    string query = $"DELETE FROM {table} WHERE OrgName=@OrgName";
                    _dbConnection.Execute(query, new { OrgName = orgName });
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error while clear tabl at QBRepository.ClearTable: " + ex.Message);
                throw;
            }
        }

        public async System.Threading.Tasks.Task InsertAccount(List<Account> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertAccount.");
            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allAccountList = new List<QuickBookAccountViewModel>();
                    var mappedList = QbMappers.CompleteAccountModel(items, orgName);
                    allAccountList.AddRange(mappedList);
                    SaveDataToDb(allAccountList, "Account");
                    allAccountList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("QBRepository exception:" + ex.Message);
                }
            }
        }
        public async System.Threading.Tasks.Task InsertItem(List<Item> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertItem.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allItemList = new List<QuickBookItemsViewModel>();
                    var mappedList = QbMappers.CompleteItemtModel(items, orgName);
                    allItemList.AddRange(mappedList);
                    SaveDataToDb(allItemList, "Item");
                    allItemList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertItem exception:" + ex.Message);
                }
            }
        }

        public async System.Threading.Tasks.Task InsertJournalEntry(List<JournalEntry> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertJournalEntry.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allJouralList = new List<QuickBookJournalViewModel>();
                    var mappedList = QbMappers.CompleteJouralModel(items, orgName);
                    allJouralList.AddRange(mappedList);
                    SaveDataToDb(allJouralList, "JournalEntry");
                    allJouralList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertJournalEntry exception:" + ex.Message);
                }
            }
        }

        public bool CreateQBatabaseAsync(string _companyName)
        {
            var con = "Server = tcp:aptest.database.windows.net,1433; Initial Catalog = master; Persist Security Info = False; User ID = admin@agilityplanningtst.onmicrosoft.com; Password = Managility@2017; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Authentication =\"Active Directory Password\";Connection Timeout=120;";
            _logger.Info($"Creating QuickBooks db for {_companyName}");
            try
            {
                using (SqlConnection connection = new SqlConnection(con))
                {
                    connection.Open();
                    var query = "create database [QuickBooks_" + _companyName + "] (EDITION = 'basic', SERVICE_OBJECTIVE = ELASTIC_POOL(name = aptst_pool));";
                    var res = connection.Execute(query);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create QuickBooks db for {_companyName} exception:{ex.Message}");
                return false;
            }
        }

        public async System.Threading.Tasks.Task InsertInvoice(List<Invoice> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertInvoice.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allInvoiceList = new List<QuickBookInvoiceViewModel>();
                    var mappedList = QbMappers.CompleteInvoiceModel(items, orgName);
                    allInvoiceList.AddRange(mappedList);
                    SaveDataToDb(allInvoiceList, "Invoice");
                    allInvoiceList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertInvoice exception:" + ex.Message);
                }
            }
        }

       
        private List<string> GetUserColumnNames(string tableName)
        {
            var res = new SqlConnection(_connection).Query<string>($"SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('{tableName}')").ToList();
            return res;
        }
       
        public bool CheckDBReportExist(string connection, string tableName)
        {

            try
            {
                using (IDbConnection _dbConnection = new SqlConnection(connection))
                {
                    string query = $"select * from {tableName} ";
                    var result = _dbConnection.Execute(query);
                    return true;
                }
            }
            catch (Exception ex)
            {

                _logger.Error("Error while check table exist: " + ex.Message);
                return false;
            }
        }

        public void SaveDataToDb <T>(List<T> dataList, string columnName, string connectionString = null)
        {
            var dt = ConvertDataToDataTable(dataList);
            using (SqlConnection connection = new SqlConnection(connectionString!= null? connectionString: _connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        var props = typeof(T).GetProperties();
                        foreach (var item in props)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.Name, item.Name);
                        }
                        sqlBulkCopy.DestinationTableName = columnName;
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"Save {columnName} exception:" + ex.Message);

                }

            }
        }
       
        public static DataTable ConvertDataToDataTable<T>(List<T> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public QuickBookRefreshToken checkRefreshTokenByUserName(string userName, string realmId, string _connection)
        {

            int user = 0;
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select UserId from [ClearJelly].[dbo].[User] where Email = @Email";
                    user = db.Query<int>(Query, new { Email = userName }).FirstOrDefault();
                }
                catch (Exception ex)
                {

                }

            }
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    //var Query = @"SELECT QuickBookRefreshTokens.Id, QuickBookRefreshTokens.UserId, RealmId, RefreshToken, AccessToken, CreationDate, ExpirationDate
                    //            FROM QuickBookRefreshTokens 
                    //            INNER JOIN [User] ON [User].Email = @Email 
                    //            AND QuickBookRefreshTokens.UserId = [User].UserId 
                    //            AND QuickBookRefreshTokens.RealmId = @RealmId";
                    var Query = @"select * from [ClearJelly].[dbo].[QuickBookRefreshTokens] where UserId = @UserId and RealmId = @RealmId";
                    var result = db.Query<QuickBookRefreshToken>(Query, new { UserId = user, RealmId = realmId }).FirstOrDefault();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }

    }
}

