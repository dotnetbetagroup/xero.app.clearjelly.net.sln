﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.QBApp.ReportEntities
{
    [Table("Report")]
    public class QBReport
    {
        [Key]
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Date { get; set; }
        public string TransactionType { get; set; }
        public string Num { get; set; }
        public string Adj { get; set; }
        public string CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModified { get; set; }
        public string LastModifiedBy { get; set; }
        public string Name { get; set; }
        public string Customer { get; set; }
        public string Vendor { get; set; }
        public string Employee { get; set; }
        public string ProductService { get; set; }
        public string MemoDescription { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string Account { get; set; }
        public string Split { get; set; }
        public string InvoiceDate { get; set; }
        public string ARPaid { get; set; }
        public string APPaid { get; set; }
        public string Clr { get; set; }
        public string CheckPrinted { get; set; }
        public string OpenBalance { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string TotalAmount { get; set; }
        public string RealmId { get; set; }
    }
}
