﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;


namespace ClearJelly.QBApp.QBMappingModels.TransactionEntities
{
    public class TransferCRUD
    {
        #region Sync Methods

        #region   Add Operations

         
         
        public void TransferAddTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Creating the Transfer for Add
            Transfer transfer = QBOHelper.CreateTransfer(qboContextoAuth);
            //Adding the Transfer
            Transfer added = Helper.Add<Transfer>(qboContextoAuth, transfer);

        }

        #endregion

        #region   FindAll Operations

         
        public void TransferFindAllTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            TransferAddTestUsingoAuth(qboContextoAuth);

            //Retrieving the Transfer using FindAll
            List<Transfer> transfers = Helper.FindAll<Transfer>(qboContextoAuth, new Transfer(), 1, 500);

        }

        #endregion

        #region   FindbyId Operations

         
        public void TransferFindbyIdTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Creating the Transfer for Adding
            Transfer transfer = QBOHelper.CreateTransfer(qboContextoAuth);
            //Adding the Transfer
            Transfer added = Helper.Add<Transfer>(qboContextoAuth, transfer);
            Transfer found = Helper.FindById<Transfer>(qboContextoAuth, added);
          
        }

        #endregion

        #region   Update Operations

         
         
        public void TransferUpdateTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Creating the Transfer for Adding
            Transfer transfer = QBOHelper.CreateTransfer(qboContextoAuth);
            //Adding the Transfer
            Transfer added = Helper.Add<Transfer>(qboContextoAuth, transfer);
            //Change the data of added entity
            Transfer changed = QBOHelper.UpdateTransfer(qboContextoAuth, added);
            //Update the returned entity data
            Transfer updated = Helper.Update<Transfer>(qboContextoAuth, changed);

        }


         
         
        public void TransferSparseUpdateTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Creating the Transfer for Adding
            Transfer transfer = QBOHelper.CreateTransfer(qboContextoAuth);
            //Adding the Transfer
            Transfer added = Helper.Add<Transfer>(qboContextoAuth, transfer);
            //Change the data of added entity
            Transfer changed = QBOHelper.UpdateTransferSparse(qboContextoAuth, added.Id, added.SyncToken);
            //Update the returned entity data
            Transfer updated = Helper.Update<Transfer>(qboContextoAuth, changed);
           
        }

        #endregion

        #region   Delete Operations

         
         
        public void TransferDeleteTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Creating the Transfer for Adding
            Transfer transfer = QBOHelper.CreateTransfer(qboContextoAuth);
            //Adding the Transfer
            Transfer added = Helper.Add<Transfer>(qboContextoAuth, transfer);
            //Delete the returned entity
            try
            {
                Transfer deleted = Helper.Delete<Transfer>(qboContextoAuth, added);
  
            }
            catch (IdsException)
            {
          
            }
        }

        

        #endregion

        #region   CDC Operations

         
         
        public void TransferCDCTestUsingoAuth(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            TransferAddTestUsingoAuth( qboContextoAuth);

            //Retrieving the Transfer using CDC
            List<Transfer> entities = Helper.CDC(qboContextoAuth, new Transfer(), DateTime.Today.AddDays(-1));
            
        }

        #endregion

        #region   Batch

         
         
        public void TransferBatchUsingoAuth(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Transfer existing = Helper.FindOrAdd(qboContextoAuth, new Transfer());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateTransfer(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateTransfer(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from Transfer");

            batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Transfer>(qboContextoAuth, batchEntries);

           
        }

        #endregion

        #region   Query
         
        public void TransferQueryUsingoAuth(ServiceContext qboContextoAuth)
        {
            QueryService<Transfer> entityQuery = new QueryService<Transfer>(qboContextoAuth);
            Transfer existing = Helper.FindOrAdd<Transfer>(qboContextoAuth, new Transfer());
            List<Transfer> tran = entityQuery.ExecuteIdsQuery("SELECT * FROM Transfer where Id='" + existing.Id+"'").ToList<Transfer>();

        }

        #endregion

        #endregion
    }
}