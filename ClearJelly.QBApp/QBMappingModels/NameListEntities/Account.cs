﻿using ClearJelly.QBApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intuit.Ipp.Data;
using Intuit.Ipp.Core;
using Intuit.Ipp.QueryFilter;
using System.Collections.ObjectModel;
using Intuit.Ipp.DataService;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class AccountCRUD
    {
        #region Sync Methods

        #region  Add Operations


        public void AddBankAccount(ServiceContext qboContextoAuth)
        {
            //Creating the Account for Add
            Account account = QBOHelper.CreateAccount(qboContextoAuth, AccountTypeEnum.Bank, AccountClassificationEnum.Asset);
            //Adding the Account
            Account added = Helper.Add<Account>(qboContextoAuth, account);
            //Verify the added Account

        }

        public void AddCreditCardAccount(ServiceContext qboContextoAuth)
        {
            //Creating the Account for Add
            Account account = QBOHelper.CreateAccount(qboContextoAuth, AccountTypeEnum.CreditCard, AccountClassificationEnum.Liability);
            //Adding the Account
            Account added = Helper.Add<Account>(qboContextoAuth, account);


        }

        public void AddExpenseAccount(ServiceContext qboContextoAuth)
        {
            //Creating the Account for Add
            Account account = QBOHelper.CreateAccount(qboContextoAuth, AccountTypeEnum.Expense, AccountClassificationEnum.Expense);
            //Adding the Account
            Account added = Helper.Add<Account>(qboContextoAuth, account);
            //Verify the added Account

        }

        #endregion

        #region  FindAll Operations

        public List<Account> AccountFindAll(ServiceContext qboContextoAuth)
        {
            List<Account> accounts = Helper.FindAll<Account>(qboContextoAuth, new Account(), 1, 500);
            return accounts;
        }

        #endregion

        #region  FindbyId Operations
        public Account AccountFindbyId(ServiceContext qboContextoAuth, Account account)
        {
            Account found = Helper.FindById<Account>(qboContextoAuth, account);
            return found;

        }
        #endregion

        #region  Update Operations

        public void AccountUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the Account for Adding
            Account account = QBOHelper.CreateAccount(qboContextoAuth, AccountTypeEnum.Bank, AccountClassificationEnum.Asset);
            //Adding the Account
            Account added = Helper.Add<Account>(qboContextoAuth, account);
            //Change the data of added entity
            Account changed = QBOHelper.UpdateAccount(qboContextoAuth, added);
            //Update the returned entity data
            Account updated = Helper.Update<Account>(qboContextoAuth, changed);//Verify the updated Account

        }

        public void AccountSparseUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the Account for Adding
            Account account = QBOHelper.CreateAccount(qboContextoAuth, AccountTypeEnum.Bank, AccountClassificationEnum.Asset);
            //Adding the Account
            Account added = Helper.Add<Account>(qboContextoAuth, account);
            //Change the data of added entity
            Account changed = QBOHelper.SparseUpdateAccount(qboContextoAuth, added.Id, added.SyncToken);
            //Update the returned entity data
            Account updated = Helper.Update<Account>(qboContextoAuth, changed);//Verify the updated Account

        }

        #endregion

        #region  Delete Operations
        //TODO:
        #endregion

        #region  CDC Operations

        public void AccountCDC(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            AddBankAccount(qboContextoAuth);

            //Retrieving the Entity using FindAll
            List<Account> entities = Helper.CDC(qboContextoAuth, new Account(), DateTime.Today.AddDays(-1));

        }

        #endregion

        #region  Batch


        public void AccountBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Account existing = Helper.FindOrAdd(qboContextoAuth, new Account());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateAccount(qboContextoAuth, AccountTypeEnum.Bank, AccountClassificationEnum.Asset));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateAccount(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from Account");



            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Account>(qboContextoAuth, batchEntries);


        }

        #endregion

        #region  Query

        public void AccountQuery(ServiceContext qboContextoAuth)
        {
            QueryService<Account> entityQuery = new QueryService<Account>(qboContextoAuth);
            Account existing = Helper.FindOrAddAccount(qboContextoAuth, AccountTypeEnum.Bank, AccountClassificationEnum.Expense);

            List<Account> list = entityQuery.ExecuteIdsQuery("SELECT * FROM Account where Id='" + existing.Id + "'").ToList<Account>();
        }

        #endregion

        #endregion
    }
}
