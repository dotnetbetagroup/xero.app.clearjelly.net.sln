﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class EmployeeCRUD
    {
        #region Sync Methods

        #region  Add Operations

        public void EmployeeAdd(ServiceContext qboContextoAuth)
        {
            //need additional parameters in the method
            //Creating the Employee for Add
            Employee employee = QBOHelper.CreateEmployee(qboContextoAuth);
            //Adding the Employee
            Employee added = Helper.Add<Employee>(qboContextoAuth, employee);


        }

        #endregion

        #region  FindAll Operations

        public List<Employee> EmployeeFindAll(ServiceContext qboContextoAuth, Employee employee)
        {
            List<Employee> employees = Helper.FindAll<Employee>(qboContextoAuth, employee, 1, 500);
            return employees;
        }

        #endregion

        #region  FindbyId Operations


        public Employee EmployeeFindbyId(ServiceContext qboContextoAuth, Employee employee)
        {
            Employee found = Helper.FindById<Employee>(qboContextoAuth, employee);
            return found;
        }

        #endregion

        #region  Update Operations
        public void EmployeeUpdate(ServiceContext qboContextoAuth, Employee e)
        {
            Employee employee = Helper.FindOrAdd(qboContextoAuth, e);
            //Change the data of added entity
            Employee changed = QBOHelper.UpdateEmployee(qboContextoAuth, employee);
            //Update the returned entity data
            Employee updated = Helper.Update<Employee>(qboContextoAuth, changed);//Verify the updated Employee
        }

        public void EmployeeSparseUpdate(ServiceContext qboContextoAuth, Employee e)
        {
            Employee employee = Helper.FindOrAdd(qboContextoAuth, e);
            //Change the data of added entity
            Employee changed = QBOHelper.SparseUpdateEmployee(qboContextoAuth, employee.Id, employee.SyncToken);
            //Update the returned entity data
            Employee updated = Helper.Update<Employee>(qboContextoAuth, changed);//Verify the updated Employee

        }

        #endregion

        #region  CDC Operations


        public List<Employee> EmployeeCDC(ServiceContext qboContextoAuth, Employee e)
        {
            List<Employee> entities = Helper.CDC(qboContextoAuth, e, DateTime.Now.AddDays(-100));
            return entities;
        }

        #endregion

        #region  Batch

        public void EmployeeBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Employee existing = Helper.FindOrAdd(qboContextoAuth, new Employee());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateEmployee(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateEmployee(qboContextoAuth, existing));

            //batchEntries.Add(OperationEnum.query, "select * from Employee");

            //batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Employee>(qboContextoAuth, batchEntries);


        }

        #endregion

        #region  Query

        public void EmployeeQuery(ServiceContext qboContextoAuth, Employee e)
        {
            QueryService<Employee> entityQuery = new QueryService<Employee>(qboContextoAuth);
            Employee existing = Helper.FindOrAdd<Employee>(qboContextoAuth, new Employee());
            List<Employee> list = entityQuery.ExecuteIdsQuery("SELECT * FROM Employee where Id='" + existing.Id + "'").ToList<Employee>();
        }

        #endregion

        #endregion
    }
}
