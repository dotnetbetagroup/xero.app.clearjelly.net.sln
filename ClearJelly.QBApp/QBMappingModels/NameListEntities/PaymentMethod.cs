﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class PaymentMethodCRUD
    {

        #region Sync Methods

        #region  Add Operations

         
        public void PaymentMethodAdd(ServiceContext qboContextoAuth)
        {
            //Creating the PaymentMethod for Add
            PaymentMethod paymentMethod = QBOHelper.CreatePaymentMethod(qboContextoAuth);
            //Adding the PaymentMethod
            PaymentMethod added = Helper.Add<PaymentMethod>(qboContextoAuth, paymentMethod);
     
        }

        #endregion

        #region  FindAll Operations

         
        public void PaymentMethodFindAll(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            PaymentMethodAdd(qboContextoAuth);

            //Retrieving the PaymentMethod using FindAll
            List<PaymentMethod> paymentMethods = Helper.FindAll<PaymentMethod>(qboContextoAuth, new PaymentMethod(), 1, 500);
    
        }

        #endregion

        #region  FindbyId Operations

         
        public void PaymentMethodFindbyId(ServiceContext qboContextoAuth)
        {
            //Creating the PaymentMethod for Adding
            PaymentMethod paymentMethod = QBOHelper.CreatePaymentMethod(qboContextoAuth);
            //Adding the PaymentMethod
            PaymentMethod added = Helper.Add<PaymentMethod>(qboContextoAuth, paymentMethod);
            PaymentMethod found = Helper.FindById<PaymentMethod>(qboContextoAuth, added);

        }

        #endregion

        #region  Update Operations

         
        public void PaymentMethodUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the PaymentMethod for Adding
            PaymentMethod paymentMethod = QBOHelper.CreatePaymentMethod(qboContextoAuth);
            //Adding the PaymentMethod
            PaymentMethod added = Helper.Add<PaymentMethod>(qboContextoAuth, paymentMethod);
            //Change the data of added entity
            PaymentMethod changed = QBOHelper.UpdatePaymentMethod(qboContextoAuth, added);
            //Update the returned entity data
            PaymentMethod updated = Helper.Update<PaymentMethod>(qboContextoAuth, changed); //Verify the updated PaymentMethod
  
        }

         
        public void PaymentMethodSparseUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the PaymentMethod for Adding
            PaymentMethod paymentMethod = QBOHelper.CreatePaymentMethod(qboContextoAuth);
            //Adding the PaymentMethod
            PaymentMethod added = Helper.Add<PaymentMethod>(qboContextoAuth, paymentMethod);
            //Change the data of added entity
            PaymentMethod changed = QBOHelper.SparseUpdatePaymentMethod(qboContextoAuth, added.Id, added.SyncToken);
            //Update the returned entity data
            PaymentMethod updated = Helper.Update<PaymentMethod>(qboContextoAuth, changed); //Verify the updated PaymentMethod
        
        }

        #endregion

        #region  Delete Operations


      

        #endregion

        #region  CDC Operations

         
        public void PaymentMethodCDC(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            PaymentMethodAdd(qboContextoAuth);

            //Retrieving the PaymentMethod using CDC
            List<PaymentMethod> entities = Helper.CDC(qboContextoAuth, new PaymentMethod(), DateTime.Today.AddDays(-1));
           
        }

        #endregion

        #region  Batch

         
        public void PaymentMethodBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            PaymentMethod existing = Helper.FindOrAddPaymentMethod(qboContextoAuth, "CREDIT_CARD");

            batchEntries.Add(OperationEnum.create, QBOHelper.CreatePaymentMethod(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdatePaymentMethod(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from PaymentMethod");

            //  batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<PaymentMethod>(qboContextoAuth, batchEntries);

           
        }

        #endregion

        #region  Query
         
        public void PaymentMethodQuery(ServiceContext qboContextoAuth)
        {
            QueryService<PaymentMethod> entityQuery = new QueryService<PaymentMethod>(qboContextoAuth);
            PaymentMethod existing = Helper.FindOrAdd<PaymentMethod>(qboContextoAuth, new PaymentMethod());
            List<PaymentMethod> tx = entityQuery.ExecuteIdsQuery("SELECT * FROM PaymentMethod where Id='" + existing.Id+"'").ToList<PaymentMethod>();
        }

        #endregion

        #endregion
    }
}