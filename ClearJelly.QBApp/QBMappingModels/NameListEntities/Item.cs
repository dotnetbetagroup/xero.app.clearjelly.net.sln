﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class ItemCRUD
    {
        #region Sync Methods
        #region  Add Operations


        public void ItemAdd(ServiceContext qboContextoAuth)
        {
            //Creating the Item for Add
            Item item = QBOHelper.CreateItem(qboContextoAuth);
            //Adding the Item
            Item added = Helper.Add<Item>(qboContextoAuth, item);

        }

        #endregion

        #region  FindAll Operations


        public void ItemFindAll(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            ItemAdd(qboContextoAuth);

            //Retrieving the Item using FindAll
            List<Item> items = Helper.FindAll<Item>(qboContextoAuth, new Item(), 1, 500);

        }

        #endregion

        #region  FindbyId Operations


        public void ItemFindbyId(ServiceContext qboContextoAuth)
        {
            //Creating the Item for Adding
            Item item = QBOHelper.CreateItem(qboContextoAuth);
            //Adding the Item
            Item added = Helper.Add<Item>(qboContextoAuth, item);
            Item found = Helper.FindById<Item>(qboContextoAuth, added);

        }

        #endregion

        #region  Update Operations


        public void ItemUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the Item for Adding
            Item item = QBOHelper.CreateItem(qboContextoAuth);

            //Adding the Item
            Item added = Helper.Add<Item>(qboContextoAuth, item);
            //Change the data of added entity
            Item changed = QBOHelper.UpdateItem(qboContextoAuth, added);
            //Update the returned entity data
            Item updated = Helper.Update<Item>(qboContextoAuth, changed);//Verify the updated Item

        }





        public void ItemSparseUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the Item for Adding
            Item item = QBOHelper.CreateItem(qboContextoAuth);
            //Adding the Item
            Item added = Helper.Add<Item>(qboContextoAuth, item);
            //Change the data of added entity
            Item changed = QBOHelper.SparseUpdateItem(qboContextoAuth, added.Id, added.SyncToken);
            //Update the returned entity data
            Item updated = Helper.Update<Item>(qboContextoAuth, changed);//Verify the updated Item

        }

        #endregion

        #region  Delete Operations



        //Delete is Soft Delete for Name List entities- Update operation with Active=false


        #endregion

        #region  CDC Operations


        public void ItemCDC(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            ItemAdd(qboContextoAuth);

            //Retrieving the Item using FindAll
            List<Item> items = Helper.CDC(qboContextoAuth, new Item(), DateTime.Today.AddDays(-1));

        }

        #endregion

        #region  Batch


        public void ItemBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Item existing = Helper.FindOrAdd(qboContextoAuth, new Item());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateItem(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateItem(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from Item");

            //batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Item>(qboContextoAuth, batchEntries);

        }

        #endregion

        #region  Query

        public void ItemQuery(ServiceContext qboContextoAuth)
        {
            QueryService<Item> entityQuery = new QueryService<Item>(qboContextoAuth);
            Item existing = Helper.FindOrAdd<Item>(qboContextoAuth, new Item());
            List<Item> inv = entityQuery.ExecuteIdsQuery("SELECT * FROM Item where Id='" + existing.Id + "'").ToList<Item>();

        }

        #endregion
        #endregion
    }
}
