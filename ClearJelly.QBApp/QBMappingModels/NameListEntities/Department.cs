﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class DepartmentCRUD
    {

        #region Sync Methods

        #region  Add Operations


        public void DepartmentAdd(ServiceContext qboContextoAuth, string name)
        {
            var department = QBOHelper.CreateDepartment(qboContextoAuth, name);

            var added = Helper.Add<Department>(qboContextoAuth, department);
        }

        #endregion

        #region  FindAll Operations

        public List<Department> DepartmentFindAll(ServiceContext qboContextoAuth)
        {
            List<Department> departments = Helper.FindAll<Department>(qboContextoAuth, new Department(), 1, 500);
            return departments;
        }

        #endregion

        #region  FindbyId Operations


        public Department DepartmentFindbyId(ServiceContext qboContextoAuth, Department d)
        {
            var found = Helper.FindById<Department>(qboContextoAuth, d);
            return found;
        }

        #endregion

        #region  Update Operations

        public void DepartmentUpdate(ServiceContext qboContextoAuth, Department d)
        {
            Department changed = QBOHelper.UpdateDepartment(qboContextoAuth, d);
            //Update the returned entity data
            Department updated = Helper.Update<Department>(qboContextoAuth, changed);//Verify the updated Department

        }

        public void DepartmentSparseUpdate(ServiceContext qboContextoAuth, Department d)
        {
            Department changed = QBOHelper.UpdateDepartmentSparse(qboContextoAuth, d.Id, d.SyncToken);
            //Update the returned entity data
            Department updated = Helper.Update<Department>(qboContextoAuth, changed);
        }

        #endregion


        #region  CDC Operations

        public List<Department> DepartmentCDC(ServiceContext qboContextoAuth)
        {
            List<Department> entities = Helper.CDC(qboContextoAuth, new Department(), DateTime.Today.AddDays(-1));
            return entities;
        }

        #endregion

        #region Query

        public List<Department> DepartmentQuery(ServiceContext qboContextoAuth)
        {
            QueryService<Department> entityQuery = new QueryService<Department>(qboContextoAuth);
            Department existing = Helper.FindOrAdd<Department>(qboContextoAuth, new Department());
            List<Department> list = entityQuery.ExecuteIdsQuery("SELECT * FROM Department where Id='" + existing.Id + "'").ToList<Department>();
            return list;
        }

        #endregion

        #endregion
    }
}
