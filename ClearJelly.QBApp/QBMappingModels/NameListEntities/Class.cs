﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class ClassCRUD
    {
        #region Sync Methods

        #region  Add Operations

         
        public void ClassAdd(ServiceContext qboContextoAuth)
        {
            //Creating the Class for Add
            Class class1 = QBOHelper.CreateClass(qboContextoAuth);
            //Adding the Class
            Class added = Helper.Add<Class>(qboContextoAuth, class1);
   
        }

        #endregion

        #region  FindAll Operations

         
        public void ClassFindAll(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            ClassAdd(qboContextoAuth);

            //Retrieving the Class using FindAll
            List<Class> classes = Helper.FindAll<Class>(qboContextoAuth, new Class(), 1, 500);
   
        }

        #endregion

        #region  FindbyId Operations

         
        public void ClassFindbyId(ServiceContext qboContextoAuth)
        {
            //Creating the Class for Adding
            Class class1 = QBOHelper.CreateClass(qboContextoAuth);
            //Adding the Class
            Class added = Helper.Add<Class>(qboContextoAuth, class1);
            Class found = Helper.FindById<Class>(qboContextoAuth, added);
          
        }

        #endregion

        #region  Update Operations

         
        public void ClassUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the Class for Adding
            Class class1 = QBOHelper.CreateClass(qboContextoAuth);
            //Adding the Class
            Class added = Helper.Add<Class>(qboContextoAuth, class1);
            //Change the data of added entity
            Class changed = QBOHelper.UpdateClass(qboContextoAuth, added);
            //Update the returned entity data
            Class updated = Helper.Update<Class>(qboContextoAuth, changed);//Verify the updated Class

        }

         
        public void ClassSparseUpdate(ServiceContext qboContextoAuth)
        {
            //Creating the Class for Adding
            Class class1 = QBOHelper.CreateClass(qboContextoAuth);
            //Adding the Class
            Class added = Helper.Add<Class>(qboContextoAuth, class1);
            //Change the data of added entity
            Class changed = QBOHelper.SparseUpdateClass(qboContextoAuth, added.Id, added.SyncToken);
            //Update the returned entity data
            Class updated = Helper.Update<Class>(qboContextoAuth, changed);//Verify the updated Class

        }

        #endregion

        #region  Delete Operations


        //Delete is Soft Delete for Name List entities- Update operation with Active=false


        #endregion

        #region  CDC Operations

         
        public void ClassCDC(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            ClassAdd(qboContextoAuth);

            //Retrieving the Class using FindAll
            List<Class> classes = Helper.CDC(qboContextoAuth, new Class(), DateTime.Today.AddDays(-1));
    
        }

        #endregion

        #region  Batch

         
        public void ClassBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Class existing = Helper.FindOrAdd(qboContextoAuth, new Class());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateClass(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateClass(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from Class");

            // batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Class>(qboContextoAuth, batchEntries);

        }

        #endregion

        #region  Query
         
        public void ClassQuery(ServiceContext qboContextoAuth)
        {
            QueryService<Class> entityQuery = new QueryService<Class>(qboContextoAuth);
            Class existing = Helper.FindOrAdd<Class>(qboContextoAuth, new Class());
            List<Class> cl = entityQuery.ExecuteIdsQuery("SELECT * FROM Class where Id='" + existing.Id+"'").ToList<Class>();

        }

        #endregion

        #endregion
    }
}