﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class VendorCRUD
    {

        #region Sync Methods

        #region  Add Operations

         
        public void VendorAddTest(ServiceContext qboContextoAuth)
        {
            //Creating the Vendor for Add
            Vendor vendor = QBOHelper.CreateVendor(qboContextoAuth);
            //Adding the Vendor
            Vendor added = Helper.Add<Vendor>(qboContextoAuth, vendor);
            
        }

        #endregion

        #region  FindAll Operations

         
        public void VendorFindAllTest(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            VendorAddTest(qboContextoAuth);

            //Retrieving the Vendor using FindAll
            List<Vendor> vendors = Helper.FindAll<Vendor>(qboContextoAuth, new Vendor(), 1, 500);
           
        }

        #endregion

        #region  FindbyId Operations

         
        public void VendorFindbyIdTest(ServiceContext qboContextoAuth)
        {
            //Creating the Vendor for Adding
            Vendor vendor = QBOHelper.CreateVendor(qboContextoAuth);
            //Adding the Vendor
            Vendor added = Helper.Add<Vendor>(qboContextoAuth, vendor);
            Vendor found = Helper.FindById<Vendor>(qboContextoAuth, added);
 
        }

        #endregion

        #region  Update Operations

         
        public void VendorUpdateTest(ServiceContext qboContextoAuth)
        {
            //Creating the Vendor for Adding
            Vendor vendor = QBOHelper.CreateVendor(qboContextoAuth);
            //Adding the Vendor
            Vendor added = Helper.Add<Vendor>(qboContextoAuth, vendor);
            //Change the data of added entity
            Vendor changed = QBOHelper.UpdateVendor(qboContextoAuth, added);
            //Update the returned entity data
            Vendor updated = Helper.Update<Vendor>(qboContextoAuth, changed);//Verify the updated Vendor

        }

         
        public void VendorSparseUpdateTest(ServiceContext qboContextoAuth)
        {
            //Creating the Vendor for Adding
            Vendor vendor = QBOHelper.CreateVendor(qboContextoAuth);
            //Adding the Vendor
            Vendor added = Helper.Add<Vendor>(qboContextoAuth, vendor);
            //Change the data of added entity
            Vendor changed = QBOHelper.SparseUpdateVendor(qboContextoAuth, added.Id, added.SyncToken);
            //Update the returned entity data
            Vendor updated = Helper.Update<Vendor>(qboContextoAuth, changed);//Verify the updated Vendor

        }

        #endregion

        #region  Delete Operations

      

        #endregion

        #region  CDC Operations

         
        public void VendorCDCTest(ServiceContext qboContextoAuth)
        {
            //Making sure that at least one entity is already present
            VendorAddTest(qboContextoAuth);

            //Retrieving the Vendor using FindAll
            List<Vendor> vendors = Helper.CDC(qboContextoAuth, new Vendor(), DateTime.Today.AddDays(-1));

        }

        #endregion

        #region  Batch

         
        public void VendorBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Vendor existing = Helper.FindOrAdd(qboContextoAuth, new Vendor());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateVendor(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateVendor(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from Vendor");

            //   batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Vendor>(qboContextoAuth, batchEntries);

           
        }

        #endregion

        #region  Query
         
        public void VendorQuery(ServiceContext qboContextoAuth)
        {
            QueryService<Vendor> entityQuery = new QueryService<Vendor>(qboContextoAuth);
            Vendor existing = Helper.FindOrAdd<Vendor>(qboContextoAuth, new Vendor());
            List<Vendor> ve = entityQuery.ExecuteIdsQuery("SELECT * FROM Vendor where Id='" + existing.Id+"'").ToList<Vendor>();

        }

        #endregion

        #endregion
    }
}