﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using ClearJelly.QBApp.Helpers;

namespace ClearJelly.QBApp.QBMappingModels.NameListEntities
{
    public class CustomerCRUD
    {
        #region Sync Methods

        #region  Add Operations

        public void CustomerAdd(ServiceContext qboContextoAuth)
        {
            //Creating the Customer for Add
            Customer customer = QBOHelper.CreateCustomer(qboContextoAuth);
            //Adding the Customer
            Customer added = Helper.Add<Customer>(qboContextoAuth, customer);
        }
        #endregion

        #region  FindAll Operations

        public List<Customer> CustomerFindAll(ServiceContext qboContextoAuth)
        {
            List<Customer> customers = Helper.FindAll<Customer>(qboContextoAuth, new Customer(), 1, 500);
            return customers;
        }

        #endregion

        #region  FindbyId Operations

        public Customer CustomerFindbyId(ServiceContext qboContextoAuth, Customer entity)
        {
            Customer found = Helper.FindById<Customer>(qboContextoAuth, entity);
            return found;
        }
        #endregion

        #region  Update Operations

        public void CustomerUpdate(ServiceContext qboContextoAuth, Customer customer)
        {
            Customer updated = QBOHelper.UpdateCustomer(qboContextoAuth, customer);
            //Update the returned entity data
            Customer result = Helper.Update<Customer>(qboContextoAuth, updated);

        }

        public void CustomerSparseUpdate(ServiceContext qboContextoAuth, Customer customer)
        {
            Customer updated = QBOHelper.SparseUpdateCustomer(qboContextoAuth, customer.Id, customer.SyncToken);
            //Update the returned entity data
            Customer result = Helper.Update<Customer>(qboContextoAuth, updated);

        }

        #endregion

        #region  CDC Operations

        public void CustomerCDC(ServiceContext qboContextoAuth)
        {
            CustomerAdd(qboContextoAuth);

            //Retrieving the Customer using FindAll
            List<Customer> customers = Helper.CDC(qboContextoAuth, new Customer(), DateTime.Today.AddDays(-1));

        }

        #endregion

        #region  Batch


        public void CustomerBatch(ServiceContext qboContextoAuth)
        {
            Dictionary<OperationEnum, object> batchEntries = new Dictionary<OperationEnum, object>();

            Customer existing = Helper.FindOrAdd(qboContextoAuth, new Customer());

            batchEntries.Add(OperationEnum.create, QBOHelper.CreateCustomer(qboContextoAuth));

            batchEntries.Add(OperationEnum.update, QBOHelper.UpdateCustomer(qboContextoAuth, existing));

            batchEntries.Add(OperationEnum.query, "select * from Customer");

            //batchEntries.Add(OperationEnum.delete, existing);

            ReadOnlyCollection<IntuitBatchResponse> batchResponses = Helper.Batch<Customer>(qboContextoAuth, batchEntries);


        }

        #endregion

        #region  Query

        public void CustomerQuery(ServiceContext qboContextoAuth)
        {
            QueryService<Customer> entityQuery = new QueryService<Customer>(qboContextoAuth);
            List<Customer> test = entityQuery.ExecuteIdsQuery("SELECT * FROM Customer").ToList<Customer>();

        }


        public void CustomerQueryWithSpecialCharacter(ServiceContext qboContextoAuth)
        {
            QueryService<Customer> entityQuery = new QueryService<Customer>(qboContextoAuth);
            Customer test = entityQuery.ExecuteIdsQuery("SELECT * FROM Customer where DisplayName='Customer\\'s Business'").FirstOrDefault<Customer>();
        }

        #endregion

        #endregion
    }
}
