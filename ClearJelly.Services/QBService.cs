﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using NLog;
using System.Data.SqlClient;
using System.Configuration;
using Intuit.Ipp.ReportService;
using CJ.Api.TokenStores.Dapper;
using ClearJelly.QBApp;
using ClearJelly.DataAccess;
using ClearJelly.Entities.Enums;
using ClearJelly.Configuration;
using System.Threading.Tasks;

namespace ClearJelly.Services
{
    public class QBService
    {
        private static Logger _logger;
        private static string _connection;
        private DataService _commonServiceQBO;
        private static OAuth2Manager _manager;
        private OAuth2RequestValidator _oauthValidator;
        private ServiceContext _serviceContext;
        private Intuit.Ipp.Data.Company _company;
        private const int RowsMaxAmount = 1000;
        private List<Company> _companies;
        QBRepository QBRepository;

        public QBService(string accessToken, string realmId, string connectionString)
        {
            _manager = new OAuth2Manager(
               ConfigSection.RedirectUri,
               ConfigSection.DiscoveryUrl,
               ConfigSection.ClientId,
               ConfigSection.ClientSecret,
               ""
           );
            _company = new Company();
            _logger = LogManager.GetCurrentClassLogger();
            _connection = connectionString;
            QBRepository = new QBRepository(_connection);
            _oauthValidator = new OAuth2RequestValidator(accessToken);
            _serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, _oauthValidator);
            _serviceContext.IppConfiguration.BaseUrl.Qbo = "https://sandbox-quickbooks.api.intuit.com/";
            _serviceContext.IppConfiguration.Message.Request.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Xml;
            _serviceContext.IppConfiguration.Message.Response.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Xml;
            _serviceContext.IppConfiguration.MinorVersion.Qbo = "8";
            _commonServiceQBO = new DataService(_serviceContext);
            _companies = new List<Company>();
        }
        private static DateTime rngMin = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        private static DateTime rngMax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;
        public async System.Threading.Tasks.Task AddAccount(List<Account> item, string orgName)
        {
            await QBRepository.InsertAccount(item, orgName);
        }
        public async System.Threading.Tasks.Task AddItems(List<Item> item, string orgName)
        {
            await QBRepository.InsertItem(item, orgName);
        }
        public async System.Threading.Tasks.Task AddJournal(List<JournalEntry> item, string orgName)
        {
            await QBRepository.InsertJournalEntry(item, orgName);
        }
        public async System.Threading.Tasks.Task AddInvoice(List<Invoice> item, string orgName)
        {
            await QBRepository.InsertInvoice(item, orgName);
        }

        //public bool CreateQBDbAsync(string companyName)
        //{
        //    var createDB = QBRepository.CreateQBatabaseAsync(companyName);
        //    QBTablesGenerator
        //}
        public async System.Threading.Tasks.Task<Company> GetDataAsync(string realmId)
        {
            var company = new Company();
            var tableNames = GetUserTableNames();
            List<Company> companies = _commonServiceQBO.FindAll<Company>(_company, 1, 100).ToList<Company>();

            _logger.Info("Executing QBService.GetDataAsync...");
            company = companies.FirstOrDefault();
            foreach (var item in tableNames)
            {
                if (item == "Account" || item == "Item" || item == "Invoice" || item == "JournalEntry")
                {
                    await QBRepository.ClearTable(item, company.CompanyName);
                }
            }
            try
            {
                var accountsFull = GetFullData<Account>(tableNames.FirstOrDefault(x => x == "Account"), _serviceContext);
                await AddAccount(accountsFull, company.CompanyName);

                var itemFull = GetFullData<Item>(tableNames.FirstOrDefault(x => x == "Item"), _serviceContext);
                await AddItems(itemFull, company.CompanyName);

                var invoiceFull = GetFullData<Invoice>(tableNames.FirstOrDefault(x => x == "Invoice"), _serviceContext);
                await AddInvoice(invoiceFull, company.CompanyName);

                var journalsFull = GetFullData<JournalEntry>(tableNames.FirstOrDefault(x => x == "JournalEntry"), _serviceContext);
                await AddJournal(journalsFull, company.CompanyName);

                return company;
            }
            catch (Exception ex)
            {
                return company;
                throw;
            }

        }

        private int PingToQB()
        {
            try
            {
                _companies = _commonServiceQBO.FindAll<Company>(_company, 1, 100).ToList<Company>();
                return (int)QBErrorCodes.Ok;
            }
            catch (Exception ex)
            {
                _logger.Info(" ~Execution of PingToQB PingToXero method completed with error : " + ex);
                return (int)QBErrorCodes.BadRequest;
            }

        }

        public async System.Threading.Tasks.Task<int> UpdateDataAsync(DateTime startDate, DateTime endDate)
        {

            _logger.Info("Executing QBService.GetDataAsync...");
            var counOfItems = 0;
            try
            {
                //if (ErrorCode == (int)QBErrorCodes.Ok)
                //{
                PingToQB();
                var company = new Company();
                company = _companies.FirstOrDefault();
                var tableNames = GetUserTableNames();

                foreach (var tbName in tableNames)
                {
                    if (company != null && tbName != "QBReport")
                        await QBRepository.ClearTable(tbName, company.CompanyName);
                }

                var accounts = UpdateData<Account>(tableNames.FirstOrDefault(x => x == "Account"), startDate, endDate);
                counOfItems += accounts.Count;
                await AddAccount(accounts, company.CompanyName);

                var item = UpdateData<Item>(tableNames.FirstOrDefault(x => x == "Item"), startDate, endDate);
                counOfItems += item.Count;
                await AddItems(item, company.CompanyName);

                var invoice = UpdateData<Invoice>(tableNames.FirstOrDefault(x => x == "Invoice"), startDate, endDate);
                counOfItems += invoice.Count;
                await AddInvoice(invoice, company.CompanyName);

                var journals = UpdateData<JournalEntry>(tableNames.FirstOrDefault(x => x == "JournalEntry"), startDate, endDate);
                await AddJournal(journals, company.CompanyName);
                counOfItems += journals.Count;
                return counOfItems;
            }
            //}

            catch (Exception ex)
            {
                _logger.Error("Error at QBService.UpdateDataAsync : " + ex.Message);
                return counOfItems;
            }
            //}
            //return (int)status;

        }

        private List<T> GetFullData<T>(string tableName, ServiceContext serviceComtext)
        {
            List<T> resultList = new List<T>();
            try
            {
                QueryService<T> inService = new QueryService<T>(serviceComtext);
                var countRows = inService.ExecuteIdsQuery($"SELECT COUNT(*) FROM {tableName} ORDERBY Metadata.LastUpdatedTime DESC").Count();
                if (countRows > RowsMaxAmount)
                {
                    string query = $"SELECT * FROM {tableName} ORDERBY Metadata.LastUpdatedTime DESC";
                    var result = Pagination(inService, query, countRows);
                    resultList.AddRange(result);
                    _logger.Info("Successfull QBService.GetFullData");
                    return resultList;
                }
                resultList.AddRange(inService.ExecuteIdsQuery($"SELECT * FROM {tableName} ORDERBY Metadata.LastUpdatedTime DESC"));
                _logger.Info("Successfull QBService.GetFullData");

                return resultList;
            }
            catch (Exception ex)
            {
                _logger.Error("Exeption in QBService.GetFullData method :" + ex.Message);
                throw;
            }
        }

        private List<T> UpdateData<T>(string tableName, DateTime startDate, DateTime endDate, string realmId = null)
        {
            _logger.Info("Executing QBService.LoadData...");
            string fromDate = "";
            string toDate = "";


            fromDate = string.Format("{0:s}", startDate);
            toDate = string.Format("{0:s}", endDate);
            if (realmId != null)
            {
                _serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, _oauthValidator);
                _serviceContext.IppConfiguration.BaseUrl.Qbo = "https://sandbox-quickbooks.api.intuit.com/";
                _serviceContext.IppConfiguration.Message.Request.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Xml;
                _serviceContext.IppConfiguration.Message.Response.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Xml;
                _serviceContext.IppConfiguration.MinorVersion.Qbo = "8";
            }
            List<T> resultList = new List<T>();
            try
            {
                QueryService<T> inService = new QueryService<T>(_serviceContext);
                var countRows = inService.ExecuteIdsQuery($"SELECT COUNT(*) FROM {tableName} WHERE Metadata.LastUpdatedTime > '{fromDate}' AND Metadata.LastUpdatedTime <= '{toDate}' ORDERBY Metadata.LastUpdatedTime DESC").Count();
                if (countRows > RowsMaxAmount)
                {
                    string query = $"SELECT * FROM {tableName} WHERE Metadata.LastUpdatedTime > '{fromDate}' AND Metadata.LastUpdatedTime <= '{toDate}' ORDERBY Metadata.LastUpdatedTime DESC";
                    var result = Pagination(inService, query, countRows);
                    resultList.AddRange(result);
                    _logger.Info("Successfull QBService.LoadData");
                    return resultList;
                }
                resultList.AddRange(inService.ExecuteIdsQuery($"SELECT * FROM {tableName} WHERE Metadata.LastUpdatedTime > '{fromDate}' AND Metadata.LastUpdatedTime <= '{toDate}' ORDERBY Metadata.LastUpdatedTime DESC"));
                _logger.Info("Successfull QBService.LoadData");

                return resultList;
            }
            catch (Exception ex)
            {
                _logger.Error("Exeption in QBService.LoadData method :" + ex.Message);
                throw;
            }
        }

        private List<T> Pagination<T>(QueryService<T> inService, string query, int countRows)
        {
            try
            {
                _logger.Info("Executing QBService.Pagination...");

                List<T> resultList = new List<T>();
                double count = countRows / RowsMaxAmount;
                var countSteps = (Math.Ceiling(count) + 1) * RowsMaxAmount;
                for (int i = 1; i < countSteps; i += RowsMaxAmount)
                {
                    resultList.AddRange(inService.ExecuteIdsQuery(query + $" STARTPOSITION {i} MAXRESULTS {RowsMaxAmount}"));
                }
                _logger.Info("Successfull QBService.Pagination");

                return resultList;
            }
            catch (Exception ex)
            {
                _logger.Error("Exeption in QBService.Pagination method :" + ex.Message);
                throw;
            }

        }


        private List<string> GetUserTableNames()
        {
            var res = new SqlConnection(_connection).Query<string>("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES").ToList();
            return res;
        }

        public ServiceContext GetContext()
        {
            return this._serviceContext;
        }

        public string GetReport(string company, string user, DateTime startDate, DateTime endDate, string org)
        {
            //var res = _manager.GetReportXml(company, user, startDate, endDate, org, _serviceContext);
            ReportService reportService = new ReportService(_serviceContext);
            var sd = DateTime.Now.AddMonths(-1);
            reportService.start_date = startDate.ToString("yyyy-MM-dd");
            reportService.end_date = endDate.ToString("yyyy-MM-dd");
            reportService.accounting_method = "Accrual";
            reportService.classid = company;
            try
            {
                var report = reportService.ExecuteReport("ProfitAndLoss");
                var res = _manager.getReportInXML(report);
                //QBRepository.UpdateRefreshToken(Dictionary["Jelly_user"].ToString(), refresh_token.RefreshToken, Dictionary["accessToken"], company);
                _logger.Info("Dictionary keys updated.");
                return res;
            }
            catch (Exception ex)
            {
                return string.Empty;

            }
        }

    }



    //static class SampleCalls
    //{
    //    public static string ReconnectRealm(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
    //    {
    //        HttpWebRequest httpWebRequest = WebRequest.Create("https://appcenter.intuit.com/api/v1/Connection/Reconnect") as HttpWebRequest;
    //        httpWebRequest.Method = "GET";
    //        httpWebRequest.Headers.Add("Authorization", GetDevDefinedOAuthHeader(httpWebRequest, consumerKey, consumerSecret, accessToken, accessTokenSecret));
    //        UTF8Encoding encoding = new UTF8Encoding();
    //        HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse;
    //        using (Stream data = httpWebResponse.GetResponseStream())
    //        {
    //            //return XML response
    //            return new StreamReader(data).ReadToEnd();
    //        }
    //    }

    //    static string GetDevDefinedOAuthHeader(HttpWebRequest webRequest, string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
    //    {

    //        OAuthConsumerContext consumerContext = new OAuthConsumerContext
    //        {
    //            ConsumerKey = consumerKey,
    //            ConsumerSecret = consumerSecret,
    //            SignatureMethod = SignatureMethod.HmacSha1,
    //            UseHeaderForOAuthParameters = true

    //        };

    //        consumerContext.UseHeaderForOAuthParameters = true;

    //        //URIs not used - we already have Oauth tokens
    //        OAuthSession oSession = new OAuthSession(consumerContext, "https://www.example.com",
    //                                "https://www.example.com",
    //                                "https://www.example.com");


    //        oSession.AccessToken = new TokenBase
    //        {
    //            Token = accessToken,
    //            ConsumerKey = consumerKey,
    //            TokenSecret = accessTokenSecret
    //        };

    //        IConsumerRequest consumerRequest = oSession.Request();
    //        consumerRequest = ConsumerRequestExtensions.ForMethod(consumerRequest, webRequest.Method);
    //        consumerRequest = ConsumerRequestExtensions.ForUri(consumerRequest, webRequest.RequestUri);
    //        consumerRequest = consumerRequest.SignWithToken();
    //        return consumerRequest.Context.GenerateOAuthParametersForHeader();
    //    }

    //}
}
