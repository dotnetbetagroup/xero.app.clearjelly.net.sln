﻿using ClearJelly.Configuration;
using ClearJelly.Entities;
using ClearJelly.ViewModels.PayPalViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ClearJelly.XeroApp.CommonXero;

namespace ClearJelly.Services.Helpers
{
    public static class MailHelper
    {

        public static Dictionary<string, string> GetReplacePairNotifyAboutEndWriteToBD(User user, string type)
        {
            var _replacePair = new Dictionary<string, string>();
            _replacePair.Add("{Type}", type);
            return _replacePair;
        }

        public static Dictionary<string, string> GetForgotPasswordSuccessPair(string email, string activationCode)
        {
            var replacePair = new Dictionary<string, string>();
            string currentDomain = ConfigSection.ActivateHostLink;
            replacePair.Add(EmailParameters.ResetPasswordLink, "<a href =" + currentDomain + "Account/ResetPassword.aspx?email=" + email + "&code=" + activationCode + ">" +
            "Click here to change your password </a> ");
            return replacePair;
        }

        public static Dictionary<string, string> GetRegistrationSuccessPair(User registeredUser, string email, string currentDomain)
        {
            var clearJellyLink = currentDomain + "Account/Login.aspx";
            var replacePair = new Dictionary<string, string>();
            var activationCode = registeredUser.ActivationCode;

            var activationLink = currentDomain + "Account/AccountActivation.aspx?email=" + email + "&code=" + activationCode;
            replacePair.Add(EmailParameters.FirstName, registeredUser.FirstName);
            replacePair.Add(EmailParameters.LastName, registeredUser.LastName);
            replacePair.Add(EmailParameters.LoginLink, "<a href = " + clearJellyLink + "> " + clearJellyLink + "</a>");
            replacePair.Add(EmailParameters.ActivationLink, "<a href = " + activationLink + " > " + activationLink + " </a>");

            return replacePair;
        }

        public static Dictionary<string, string> GetRegistrationEmailAdminSuccessPair(User registeredUser, string currentSubscription)
        {
            var siteUrl = SmtpMailSendingService.GetSiteUrl();
            var replacePair = new Dictionary<string, string>();
            var activationCode = registeredUser.ActivationCode;
            string currentDomain = ConfigSection.ActivateHostLink;
            var activationLink = currentDomain + "Account/AccountActivation.aspx?email=" + registeredUser.Email + "&code=" + activationCode;
            replacePair.Add(EmailParameters.UserFullName, registeredUser.Email);
            replacePair.Add(EmailParameters.MobileNumber, registeredUser.Mobile);
            replacePair.Add(EmailParameters.CompanyName, registeredUser.Company.Name);
            replacePair.Add(EmailParameters.RegistrationDate, registeredUser.RegistrationDate.ToShortDateString());
            replacePair.Add(EmailParameters.SubscriptionType, currentSubscription);
            replacePair.Add(EmailParameters.ActivationLink, "<a href = " + activationLink + " > " + activationLink + " </a>");
            return replacePair;
        }

        public static Dictionary<string, string> DatabaseDontMatchErrorPair(string emailId, string companyName, string dbName)
        {
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.CompanyName, companyName);
            replacePair.Add(EmailParameters.EmailId, emailId);
            replacePair.Add(EmailParameters.DbName, dbName);
            return replacePair;
        }

        public static Dictionary<string, string> GetMaxFailsubscriptionpair(PaypalPaymentDetailsViewModel paypalDetailVM)
        {
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.PayerFirstName, paypalDetailVM.FirstName);
            replacePair.Add(EmailParameters.PayerLastName, paypalDetailVM.LastName);
            replacePair.Add(EmailParameters.PayerEmailId, paypalDetailVM.PayerEmail);
            replacePair.Add(EmailParameters.Subscription, paypalDetailVM.Subscription);

            return replacePair;
        }

        public static Dictionary<string, string> GetPaymentErrorPair(PaypalPaymentDetailsViewModel payPalVm)
        {
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.Currency, payPalVm.Currency);
            replacePair.Add(EmailParameters.Amount, payPalVm.Amount.ToString());
            replacePair.Add(EmailParameters.PayerFirstName, payPalVm.FirstName);
            replacePair.Add(EmailParameters.PayerLastName, payPalVm.LastName);
            replacePair.Add(EmailParameters.Subscription, payPalVm.Subscription);
            replacePair.Add(EmailParameters.PaymentDate, payPalVm.PaymentDate.ToString());
            replacePair.Add(EmailParameters.PaypalCompanyName, payPalVm.CompanyName);
            replacePair.Add(EmailParameters.PayerEmailId, payPalVm.PayerId);
            replacePair.Add(EmailParameters.RecurringId, payPalVm.RecurringProfileId);
            return replacePair;
        }

        public static Dictionary<string, string> GetPaymentSuccessPair(PaypalPaymentDetailsViewModel payPalVm, string invoiceNumber)
        {
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.PayerFirstName, payPalVm.FirstName);
            replacePair.Add(EmailParameters.PayerLastName, payPalVm.LastName);
            replacePair.Add(EmailParameters.PaymentDate, payPalVm.PaymentDate.ToString());
            replacePair.Add(EmailParameters.PayerEmail, payPalVm.EmailId);
            replacePair.Add(EmailParameters.Amount, payPalVm.Amount.ToString());
            replacePair.Add(EmailParameters.Currency, payPalVm.Currency);
            replacePair.Add(EmailParameters.Subscription, payPalVm.Subscription);
            replacePair.Add(EmailParameters.InvoiceNumber, invoiceNumber);
            replacePair.Add(EmailParameters.PayerEmailId, payPalVm.PayerEmail);
            replacePair.Add(EmailParameters.PaypalCompanyName, payPalVm.CompanyName);
            return replacePair;
        }

        public static Dictionary<string, string> GetQuickGuidePair(string currentDomain)
        {
            var safeLink = currentDomain + "ManageIPRequest";
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.SafeIPLink, "<input type = 'button' value = 'Add Safe IP' id = 'addSafeIP' class='btn btn-primary btn-default'>");
            return replacePair;
        }

    }
}
