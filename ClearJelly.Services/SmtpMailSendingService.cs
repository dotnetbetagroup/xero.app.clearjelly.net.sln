﻿using ClearJelly.Configuration;
using ClearJelly.Entities;
using ClearJelly.Entities.Enums;
using ClearJelly.Services.Helpers;
using ClearJelly.ViewModels.PayPalViewModels;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using static ClearJelly.XeroApp.CommonXero;

namespace ClearJelly.Services
{
    public static class SmtpMailSendingService
    {
        public static SmtpClient MailClient;
        public static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();


        static SmtpMailSendingService()
        {
            MailClient = new SmtpClient(ConfigSection.SmtpServerName, ConfigSection.SmtpServerPort);
            MailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            MailClient.ServicePoint.MaxIdleTime = 2;
            MailClient.Credentials = new NetworkCredential(ConfigSection.CredentialsMailLogin, ConfigSection.CredentialsMailPassword);
            MailClient.EnableSsl = true;
        }

        public static MailMessage GetMailMessage(EmailTemplates emailTemplate, Dictionary<string, string> contentParameters, string toAddress, string takeAttachmentsFrom = "")
        {
            try
            {
                var template = _clearJellyContext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == (int)emailTemplate);
                var mailMessage = new MailMessage();
                var mailAddressFrom = new MailAddress(ConfigSection.CredentialsMailLogin);
                mailMessage.Subject = template.Subject;
                mailMessage.To.Add(toAddress);
                mailMessage.From = mailAddressFrom;
                var content = template.TemplateContent;
                foreach (var pair in contentParameters)
                {
                    content = content.Replace(pair.Key, pair.Value);
                }
                mailMessage.Body = content;

                if (!string.IsNullOrWhiteSpace(takeAttachmentsFrom))
                {
                    foreach (var attachment in template.EmailAttachments)
                    {
                        var attachmentFileName = attachment.AttachmentName;
                        var attachedFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/" + takeAttachmentsFrom + "/"));
                        mailMessage.Attachments.Add(new System.Net.Mail.Attachment(attachedFile + attachmentFileName));
                    }
                }
                mailMessage.IsBodyHtml = true;
                return mailMessage;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Common GetMailMessage event completed. :" + ex.Message, ex);
                return new MailMessage(ConfigSection.CredentialsMailLogin, toAddress);
            }
        }

        public static void SendForgotPasswordMailMessage(User requestedUser, Guid code)
        {
            try
            {
                var attachmentFrom = GetEmailAttachmentFrom();
                var replacePair = MailHelper.GetForgotPasswordSuccessPair(requestedUser.UserEmail, code.ToString());
                using (MailMessage mailMessage = GetMailMessage(EmailTemplates.ForgotPasswordEmail, replacePair, requestedUser.UserEmail, attachmentFrom))
                {
                    mailMessage.IsBodyHtml = true;
                    if (GetEnableSSLKey())
                    {
                        MailClient.EnableSsl = true;
                    }
                    MailClient.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("SendForgotPasswordMailMessage ", ex.Message);
            }
        }

        public static void SendDbUpdateFinishedMailMessage(User registeredUser, string type)
        {
            try
            {
                var replacePair = MailHelper.GetReplacePairNotifyAboutEndWriteToBD(registeredUser, type);
                using (MailMessage mailMessage = GetMailMessage(EmailTemplates.UpdateDBSuccessEmail, replacePair, registeredUser.UserEmail))
                {
                    mailMessage.IsBodyHtml = true;
                    mailMessage.CC.Add(GetCCEmailId());
                    if (GetEnableSSLKey())
                    {
                        MailClient.EnableSsl = true;
                    }
                    MailClient.Send(mailMessage);
                    _logger.Info("Email Notification send succsessful");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error Send Message", ex.Message);
            }
        }
        public static void SendAbcDbUpdateErrorMailMessage(User registeredUser, string type)
        {
            try
            {
                var replacePair = MailHelper.GetReplacePairNotifyAboutEndWriteToBD(registeredUser, type);
                using (MailMessage mailMessage = GetMailMessage(EmailTemplates.UpdateDBErrorEmail, replacePair, registeredUser.UserEmail))
                {
                    mailMessage.IsBodyHtml = true;
                    mailMessage.CC.Add(GetCCEmailId());
                    if (GetEnableSSLKey())
                    {
                        MailClient.EnableSsl = true;
                    }
                    MailClient.Send(mailMessage);
                    _logger.Info("Email Notification send succsessful");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error Send Message", ex.Message);
            }
        }

        public static void SendRegisteredUserDetailsToClient(string email, User registeredUser, string currentSubscription)
        {
            try
            {
                var replacePair = MailHelper.GetRegistrationEmailAdminSuccessPair(registeredUser, currentSubscription);
                var attachmentFrom = GetEmailAttachmentFrom();
                using (MailMessage mailMessage = GetMailMessage(EmailTemplates.RegistrationSuccessEmailToAdmin,
                                                                              replacePair, email, attachmentFrom))
                {
                    if (GetEnableSSLKey())
                    {
                        MailClient.EnableSsl = true;
                    }
                    MailClient.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("SendRegisteredUserDetailsToClient Error" + ex.Message + "\n" + ex);
            }
        }

        public static bool GetEnableSSLKey()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetEnableSSLKey event started.");
                var configuation = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                MailSettingsSectionGroup settings = (MailSettingsSectionGroup)configuation.GetSectionGroup("system.net/mailSettings");
                if (settings.Smtp.Network.EnableSsl == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetDataBaseName event completed.", ex);
                return false;
            }
        }



        public static void SendActivationLink(string toAddress, User registeredUser, string currentDomain)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Registred_user"] ?? "") + " ~Execution of Register SendActivationLink event started.");

                var replacePair = MailHelper.GetRegistrationSuccessPair(registeredUser, toAddress, currentDomain);
                var attachmentFrom = GetEmailAttachmentFrom();
                var mailMessage = GetMailMessage(EmailTemplates.RegistrationSuccessEmail, replacePair, toAddress, attachmentFrom);
                mailMessage.IsBodyHtml = true;
                mailMessage.CC.Add(GetCCEmailId());
                if (GetEnableSSLKey())
                {
                    MailClient.EnableSsl = true;
                }
                MailClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register SendActivationLink event completed. :" + ex.Message, ex);
            }
        }



        public static void SendMailOnInvoiceCreate(PaypalPaymentDetailsViewModel payPalVm, byte[] invoicePdf, string invoiceNumber)
        {
            try
            {
                _logger.Info((payPalVm.EmailId + " ~Execution of Common SendMailOnInvoiceCreate event started."));
                var replacePair = MailHelper.GetPaymentSuccessPair(payPalVm, invoiceNumber);
                var mailMessage = GetMailMessage(EmailTemplates.PaymentSuccessEmail, replacePair, payPalVm.EmailId);
                mailMessage.CC.Add(GetCCEmailId());
                mailMessage.IsBodyHtml = true;
                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(new MemoryStream(invoicePdf), invoiceNumber + ".pdf");
                mailMessage.Attachments.Add(attachment);
                SmtpClient mailclient = new SmtpClient();
                mailclient.ServicePoint.MaxIdleTime = 2;
                if (GetEnableSSLKey())
                {
                    mailclient.EnableSsl = true;
                }
                mailclient.Send(mailMessage);
                _logger.Info((payPalVm.EmailId + " ~Execution of Common SendMailOnInvoiceCreate event completed."));
            }
            catch (Exception ex)
            {
                _logger.Error(payPalVm.EmailId + "Execution of Common SendPaymentNotificationLink event completed. :" + ex);
            }
        }

        public static void SendPaymentFailureMail(PaypalPaymentDetailsViewModel payPalVm)
        {
            try
            {
                _logger.Info((payPalVm + " ~Execution of Common SendPaymentFailureMail event started."));
                var adminEmail = ConfigurationManager.AppSettings["adminEmailId"];
                if (adminEmail == null)
                {
                    throw new Exception("Admin email is not defined");
                }
                var replacePair = MailHelper.GetPaymentErrorPair(payPalVm);
                var mailMessage = GetMailMessage(EmailTemplates.ErrorXeroEmail, replacePair, adminEmail);
                mailMessage.IsBodyHtml = true;
                SmtpClient mailclient = new SmtpClient();
                mailclient.ServicePoint.MaxIdleTime = 2;
                if (GetEnableSSLKey())
                {
                    mailclient.EnableSsl = true;
                }
                mailclient.Send(mailMessage);
                _logger.Info((payPalVm.EmailId + " ~Execution of Common SendPaymentFailureMail event completed."));
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Common SendPaymentFailureMail event completed. :", ex);
            }
        }

        public static void SendEmail(bool successStatus, string toEmailAddress)
        {
            try
            {
                _logger.Info(toEmailAddress + " ~Execution of SendEmail method started.");
                SmtpClient mailclient = new SmtpClient();

                mailclient.ServicePoint.MaxIdleTime = 2;
                var attachmentFrom = GetEmailAttachmentFrom();
                var replacePair = new Dictionary<string, string>();
                var toAddress = toEmailAddress;
                MailMessage mailMessage = new MailMessage();
                if (successStatus)
                {
                    mailMessage = GetMailMessage(EmailTemplates.ClearjellyModelBuiltSuccess, replacePair, toAddress, attachmentFrom);
                }
                if (!successStatus)
                {
                    mailMessage = GetMailMessage(EmailTemplates.ClearjellyModelBuiltFailed, replacePair, toAddress, attachmentFrom);
                }
                mailMessage.IsBodyHtml = true;
                if (GetEnableSSLKey())
                {
                    mailclient.EnableSsl = true;
                }
                mailclient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                _logger.Error((toEmailAddress ?? "") + " ~Execution of XeroToCJ SendEmail completed:" + ex.Message, ex);
            }
        }

        public static string GetEmailAttachmentFrom()
        {
            try
            {
                var templateAttachmentFolder = ConfigurationManager.AppSettings["EmailTemplateAttachmentFolder"];
                if (templateAttachmentFolder == null)
                {
                    throw new Exception("EmailTemplateAttachment is not defined.");
                }
                return templateAttachmentFolder;
            }
            catch (Exception ex)
            {
                _logger.Error("Execution of Register GetSiteUrl event completed with error :" + ex.Message, ex);
                throw ex;
            }
        }

        public static string GetSiteUrl()
        {
            try
            {
                var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                if (siteUrl == null)
                {
                    new Exception("Invalid Session SiteUrl");
                }
                return siteUrl;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                return "Error SiteUrl ";
            }
        }
    }

}