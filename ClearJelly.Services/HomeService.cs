﻿using ClearJelly.DataAccess;
using ClearJelly.Entities;
using ClearJelly.XeroApp;
using EntityModel.Entities;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using Xero.Api.Core;

public class HomeService
{

    private static Logger _logger;
    private static string _connection;
    HomeRepository HomeRepository;

    public HomeService(string connectionString)
    {
        _logger = LogManager.GetCurrentClassLogger();
        _connection = connectionString;
        HomeRepository = new HomeRepository(_connection);
    }
    public void updateUserChosenService(string data, string UserName)
    {
        HomeRepository.updateUserChosenService(data, UserName);
    }
    public string CheckDbExist(string email)
    {
        return HomeRepository.CheckDbExist(email);
    }
    public bool CheckAbcClub(string clubNumber, int userId)
    {
        return HomeRepository.CheckAbcClub(clubNumber, userId);
    }
    public List<AbcClub> GetAbcClubs(int userId)
    {
        return HomeRepository.GetAbcClubs(userId);
    }
        public string GetUserDBName(String uName)
    {
        return HomeRepository.GetUserDBName(uName, GetDataBaseName());

    }
    public bool CheckModelExists(string orgName)
    {
        return HomeRepository.CheckModelExists(orgName);
    }
    public void ChangeFlagOfModelProcess()
    {
        HomeRepository.ChangeFlagOfModelProcess();
    }
    public async System.Threading.Tasks.Task AddCompany(QuickBookCompany model, int userId)
    {
        await HomeRepository.InsertCompany(model, userId);
    }
    public List<QuickBookCompany> GetUserCompanies(int currentUser)
    {
        return HomeRepository.GetUserCompanies(currentUser);
    }
    public string UpdateQBCompany(int userId, string realmId)
    {
        return HomeRepository.UpdateQBCompany(userId, realmId);
    }
    
    public static string GetDataBaseName()
    {
        try
        {
            _logger.Info(" ~Execution of Common GetDataBaseName event started.");
            var databaseName = ConfigurationManager.AppSettings["DatabaseName"];
            if (databaseName == null)
            {
                throw new Exception("Database Name is not defined");
            }
            return databaseName;
        }
        catch (Exception ex)
        {
            _logger.Error(" ~Execution of Common GetDataBaseName event completed.", ex);
            throw;
        }

    }

    //-------------------------------------------------------------------------------------       
    public Dictionary<string, string> GetUserOrganisations(String uName)
    {
        return HomeRepository.GetUserOrganisations(uName);
    }
    public void InsertModelProcess(int userId, string OrgName, int ModelType, DateTime startDate, DateTime endDate)
    {
        HomeRepository.InsertModelProcess(userId, OrgName, ModelType, startDate, endDate);
    }
    public void UpdateModelStatus(int UserId, int errorCodes)
    {
        HomeRepository.UpdateModelStatus(UserId, errorCodes);

    }

    public string GetRealmId(string company, int userId)
    {
        return HomeRepository.GetRealmId(company, userId);
    }

    public void Work_create(XeroToCJ obj, string org, int userId, DateTime startDate, DateTime endDate)
    {
        bool isSuccess = false;
        int errorCodes = 0;
        try
        {
            SqlAccessTokenStore.orgShortCode = org;

            errorCodes = obj.ReCreateModle(startDate, endDate);
        }
        catch (Exception ex)
        {
            _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Home Work_create event completed.", ex);

        }
        finally
        {
            UpdateModelStatus(userId, errorCodes);
        }

    }
    public void InsertOrganizationDetails(XeroCoreApi api, string orgShortCode, int companyId)
    {
        HomeRepository.InsertOrganizationDetails(api, orgShortCode, companyId);

    }
    public string IsMessageNotSeenByUser(int userId)
    {
        return HomeRepository.IsMessageNotSeenByUser(userId);
    }

    public string getQuickGuideData()
    {
        return HomeRepository.getQuickGuideData();
    }

    public User GetActiveUserByEmail(string email)
    {
        return HomeRepository.GetActiveUserByEmail(email);
    }
    public List<CompanySubscription> GetCurrentSubscriptions(int companyId, string email)
    {
        return HomeRepository.GetCurrentSubscriptions(companyId, email);
    }
    public bool IsXeroCompanyExist(int companyId, string orgShortCode)
    {
        return HomeRepository.IsXeroCompanyExist(companyId, orgShortCode);
    }
    public int GetXeroCompanyCount(int companyId)
    {
        return HomeRepository.GetXeroCompanyCount(companyId);
    }
    public List<int?> GetChildUserAssignedSubscription(int userId)
    {
        return HomeRepository.GetChildUserAssignedSubscription(userId);
    }
    public void AddXeroOrg(Xero_User_Org org)
    {
        HomeRepository.AddXeroOrg(org);
    }
    public bool GetModelProcesses(int userId, StatusEnum runningTypeId)
    {
        return HomeRepository.GetModelProcesses(userId, runningTypeId);
    }
    public bool GetRunningProcesses(int userId, int runningTypeId, string inOrgName)
    {
        return HomeRepository.GetRunningProcesses(userId, runningTypeId, inOrgName);
    }
    //public bool checkUserCompany(string email, string userInList)
    //{
    //    return HomeRepository.CheckUserCompany(email, userInList);
    //}
    public token checkIsTokenExist(string email)
    {
        return HomeRepository.checkIsTokenExist(email);
    }
    public void AddRefreshToken(QuickBookRefreshToken model)
    {
        HomeRepository.AddRefreshToken(model);
    }
    public QuickBookRefreshToken checkRefreshTokenByUserName(int userId, string realmId)
    {
        return HomeRepository.checkRefreshTokenByUserName(userId, realmId); ;
    }
    public void UpdateRefreshToken(string userName, string rToken)
    {
        HomeRepository.UpdateRefreshToken(userName, rToken);
    }
    public string checkAccessTokenByUserName(string userName, string realmId)
    {
        return HomeRepository.CheckAccessTokenByUserName(userName, realmId); ;
    }

    public string checkRefreshTockenByUser(int userId, string refresh_token, string realmId, string aToken)
    {
        //string checkAccessToken = string.Empty;
        //var checkRefreshToken = checkRefreshTokenByUserName(userId, realmId);
        ////if (checkRefreshToken != null)
        ////{
        ////    checkAccessToken = checkAccessTokenByUserName(userName, realmId);
        ////}
        //if (checkRefreshToken == null)
        //{
            var modelNew = new QuickBookRefreshToken();
            modelNew.RefreshToken = refresh_token;
            modelNew.RealmId = realmId;
            modelNew.Id = Guid.NewGuid().ToString();
            modelNew.AccessToken = aToken;
            modelNew.UserId = userId;
            modelNew.CreationDate = DateTime.Now;
            modelNew.ExpirationDate = DateTime.Now.AddHours(+1);
            AddRefreshToken(modelNew);
        //}

        return refresh_token;
    }    
}
