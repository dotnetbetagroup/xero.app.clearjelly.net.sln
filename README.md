### Modeller project (EN description) ###
The modeller project consist of 3 different projects:

* Agility Planning (main project, contains Modeller app - tool to work with data)
* Clear Jelly (project with connection to QuickBooks, Xero and ABC. This project soon will be removed and these service connections will be moved to Agility Planning)
* Excel addin that is currently doing the same job as Modeller app, but in Microsoft Excel Online

### Modeller project (RU description) ###
Проект Modeller состоит из 3 проектов:

* Agility Planning (основной проект, содержит приложение Modeller для работы с данными)
* Clear Jelly (проект с провайдерами для сервисов QuickBooks, Xero and ABC. Проект скоро будет удален, а провайдеры будут перенесены в Agility Planning)
* Excel addin сейчас выполняет те же функции, что и приложение Modeller в онлайн весрии Microsoft Excel

### Common rules ###
* Pull request [EN version](https://bitbucket.org/dotnetbetagroup/xero.app.clearjelly.net.sln/wiki/Pull%20request%20guide%20(EN))
* Pull request [RU version](https://bitbucket.org/dotnetbetagroup/xero.app.clearjelly.net.sln/wiki/Pull%20request%20guide%20(RU))
* [Wiki](https://bitbucket.org/dotnetbetagroup/xero.app.clearjelly.net.sln/wiki/Home)

### Clear Jelly ###

* [Clear Jelly EN Wiki](https://bitbucket.org/dotnetbetagroup/xero.app.clearjelly.net.sln/wiki/Clear%20Jelly%20EN%20wiki)
* [Clear Jelly RU Wiki](https://bitbucket.org/dotnetbetagroup/xero.app.clearjelly.net.sln/wiki/Clear%20Jelly%20RU%20wiki)

### Agility Planning ###

### Excel addin ###

* [ClearJelly Confluence](https://managility.atlassian.net/wiki/spaces/CJT/overview)

* [Agility Confluence](https://managility.atlassian.net/wiki/spaces/APT/overview)

* [Asana](https://app.asana.com/0/181094076979106/434737759347050)

* [Agility Planning documentation](https://bitbucket.org/dotnetbetagroup/xero.app.clearjelly.net.sln/wiki/Agility%20Planning%20documentation)