﻿-- =============================================
-- Author:		Hesam Ziaei
-- Create date: 10/10/2015
-- Description:	This Procedure creates the necessary views to be used by CleayJelly end users on each database
-- =============================================
CREATE PROCEDURE [dbo].[CreateViewsSAASU] 
	-- Add the parameters for the stored procedure here
	@companyname Varchar(600) 

AS
BEGIN
	DECLARE @SQLString NVARCHAR(MAX);
	SET @SQLString  =' use [SAASU_'+@companyname +']';
	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''vwGL'')
									   DROP VIEW vwGL;';
	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''vwPayroll'')
									   DROP VIEW vwPayroll;';
	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''vwSales'')
									   DROP VIEW vwSales;';
	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''vwGL_Summary'')
									   DROP VIEW vwGL_Summary;';
	execute(@SQLString );



	SET @SQLString='EXEC [SAASU_'+@companyname +']..sp_executesql N''	

						Create View [dbo].[vwSales] as
						(
						SELECT 
	  
							  DATENAME(Year,convert(date,TransactionDate,101)) Years
							  ,LEFT(DATENAME(MONTH,convert(date,TransactionDate,101)),3) Months
							  ,convert(datetime2,convert(datetime2,TransactionDate ,101),103)as [Date]
							  ,Case 
								When 
									E.Code	is null
									OR
									LTRIM(RTRIM(E.Code)) =''''''''
								THEN ''''Unknown Item''''
								ELSE E.Code
							  END as Item	   
							  ,Case when 
										[GivenName]  +[FamilyName] is null
										OR 
										LTRIM(RTRIM([GivenName] +[FamilyName]))=''''''''
										THEN
											Case when D.Name is null 
												THEN ''''Unknown Company Default Contact''''
												ELSE D.Name+'''' - Default Contact''''
											END
										ELSE 
											[GivenName] +[FamilyName]
								END Contact							  
							  ,C.[PostalAddress]
							  ,D.Name as Company 
							  ,[Currency]
							  ,''''Actual'''' DataTypes		
							  ,''''Amount'''' Sales_M

							  ,case 
								when 
									LTRIM(RTRIM([LineItemTaxCode])) =''''''''
									OR 
									[LineItemTaxCode] is null
			
								  THEN 0
								  ELSE cast(B.Rate as money)
								END as TaxRate
							  ,[LineItemQuantity]
							  ,[LineItemUnitPrice]
							  ,[LineItemPercentageDiscount]
							  ,[LineItemInventoryId]
							  ,[InvoiceType]
							  ,[IsTaxInc]
							  ,[TransactionDate]
							  ,[BillingContactId]
							  ,[PaymentStatus]
							  ,[LineItemTotalAmount] AS VALUE
						  FROM [SalesInvoiceDetails] A
						  left join TaxCodes B
							  ON
							  A.LineItemTaxCode=B.Code
							  and
							  A.OrgName=B.OrgName
						  left Join Contacts C
							  ON
							  A.BillingContactId=C.Id
	  							  and
							  A.OrgName=C.OrgName
						  Left Join Companies D
							  ON
							  C.CompanyId=D.Id
	  							  and
							  A.OrgName=D.OrgName
						  Left Join Items E
							  ON
							  E.Id=A.LineItemInventoryId
	  							  and
							  A.OrgName=E.OrgName
						  where
						  [InvoiceType]<>''''Quote''''

						';

					SET @SQLString=@SQLString+'	  UNION ALL
  
						  SELECT 
	  
							  DATENAME(Year,convert(date,TransactionDate,101)) Years
							  ,LEFT(DATENAME(MONTH,convert(date,TransactionDate,101)),3) Months
							  ,convert(datetime2,convert(datetime2,TransactionDate ,101),103)as [Date]
							  ,Case 
								When 
									E.Code	is null
									OR
									LTRIM(RTRIM(E.Code)) =''''''''
								THEN ''''Unknown Item''''
								ELSE E.Code
							  END as Item	   
							  ,Case when 
										[GivenName] +[FamilyName] is null
										OR 
										LTRIM(RTRIM([GivenName] +[FamilyName]))=''''''''
										THEN
											Case when D.Name is null 
												THEN ''''Unknown Company Default Contact''''
												ELSE D.Name+'''' - Default Contact''''
											END
										ELSE 
											[GivenName] +[FamilyName]
								END Contact 
							  ,C.[PostalAddress]
							  ,D.Name as Company
							  ,[Currency]
							  ,''''Actual'''' DataTypes
							  ,''''Tax Amount'''' Sales_M
							  ,case 
								when 
									LTRIM(RTRIM([LineItemTaxCode])) =''''''''
									OR 
									[LineItemTaxCode] is null
			
								  THEN 0
								  ELSE cast(B.Rate as money)
								END as TaxRate
							  ,[LineItemQuantity]
							  ,[LineItemUnitPrice]
							  ,[LineItemPercentageDiscount]
							  ,[LineItemInventoryId]
							  ,[InvoiceType]
							  ,[IsTaxInc]
							  ,[TransactionDate]
							  ,[BillingContactId]
							  ,[PaymentStatus]
							  , case
									when IsTaxInc= ''''TRUE''''
									 THEN 
										 case 
											when 
												LTRIM(RTRIM([LineItemTaxCode])) =''''''''
												OR 
												[LineItemTaxCode] is null
			
											  THEN 0
											  ELSE cast(B.Rate as money)*cast([LineItemTotalAmount] as money)/(1+cast(B.Rate as money))
											END 
									 ELSE 0
								END AS VALUE 	  
						  FROM [SalesInvoiceDetails] A
						  left join TaxCodes B
							  ON
							  A.LineItemTaxCode=B.Code
							  and
							  A.OrgName=B.OrgName
						  left Join Contacts C
							  ON
							  A.BillingContactId=C.Id
	  							  and
							  A.OrgName=C.OrgName
						  Left Join Companies D
							  ON
							  C.CompanyId=D.Id
	  							  and
							  A.OrgName=D.OrgName
						  Left Join Items E
							  ON
							  E.Id=A.LineItemInventoryId
	  							  and
							  A.OrgName=E.OrgName
						  where
						  [InvoiceType]<>''''Quote''''
						  )

	 '' ';

	exec(@SQLString);

	SET @SQLString='EXEC [SAASU_'+@companyname +']..sp_executesql N''	
						create view [dbo].[vwPayroll]						
						as
						(
						SELECT
							  cast(DATENAME(Year,convert(date,[Tran Date],101)) as varchar(100)) Years
							  ,LEFT(DATENAME(MONTH,convert(date,[Tran Date],101)),3) Months
							  ,convert(datetime2,convert(datetime2,[Tran Date] ,101),103)as [Date]
							  ,''''Wages and Salaries'''' Accounts
							  ,''''AUD''''  Currencies
							  ,''''Actual'''' Datatype 
							  ,''''Amount'''' GL_M	
							  ,OrgName	 
							  ,''''Payroll'''' JounralType		 
							  ,'''''''' [LineItemId]
							  ,''''Salary for ''''+[First Name]+'''' ''''+[Last Name]+''''  '''' [LineItemDescription]
							  ,'''''''' [LineItemAccountId]
							  ,'''''''' [LineItemTaxCode]
							  ,'''''''' [LineItemTotalAmount]
							  ,'''''''' [LineItemQuantity]
							  ,'''''''' [LineItemUnitPrice]
							  ,'''''''' [LineItemPercentageDiscount]
							  ,'''''''' [LineItemInventoryId]
							  ,'''''''' [LineItemTags]
							  ,'''''''' [LineItemAttributes]
							  ,'''''''' [LineItem_links]
							  ,'''''''' [NotesInternal]
							  ,'''''''' [NotesExternal]
							  ,'''''''' [Attachments]
							  ,'''''''' [TemplateId]
							  ,'''''''' [SendEmailToContact]
							  ,'''''''' [EmailMessage]
							  ,'''''''' [QuickPayment]
							  ,'''''''' [TransactionId]
							  ,'''''''' [LastUpdatedId]
							  ,'''''''' [InvoiceNumber]
							  ,'''''''' [InvoiceType]
							  ,'''''''' [TransactionType]
							  ,'''''''' [Layout]
							  ,''''Salary for ''''+[First Name]+'''' ''''+[Last Name]+''''  '''' [Summary]
							  ,'''''''' [TotalAmount]
							  ,'''''''' [TotalTaxAmount]
							  ,'''''''' [IsTaxInc]
							  ,'''''''' [AmountPaid]
							  ,'''''''' [AmountOwed]
							  ,'''''''' [FxRate]
							  ,'''''''' [AutoPopulateFxRate]
							  ,'''''''' [RequiresFollowUp]
							  ,'''''''' [SentToContact]
							  ,'''''''' [TransactionDate]
							  ,'''''''' [BillingContactId]
							  ,'''''''' [BillingContactFirstName]
							  ,'''''''' [BillingContactLastName]
							  ,'''''''' [BillingContactOrganisationName]
							  ,'''''''' [ShippingContactId]
							  ,'''''''' [ShippingContactFirstName]
							  ,'''''''' [ShippingContactLastName]
							  ,'''''''' [ShippingContactOrganisationName]
							  ,'''''''' [CreatedDateUtc]
							  ,'''''''' [LastModifiedDateUtc]
							  ,'''''''' [PaymentStatus]
							  ,'''''''' [DueDate]
							  ,'''''''' [InvoiceStatus]
							  ,'''''''' [PurchaseOrderNumber]
							  ,'''''''' [PaymentCount]
							  ,'''''''' [Tags]    
							  ,cast([Base Pay Hourly (Amount)] as money) 
							  as [#Value]

						  FROM PayrollSummary A
						  where
						  cast([Base Pay Hourly (Amount)] as money) <>0
						   ';
	
		--select(@SQLString);
		SET @SQLString=@SQLString+'
					
						UNION ALL
						SELECT
							  cast(DATENAME(Year,convert(date,[Tran Date],101)) as varchar(100)) Years
							  ,LEFT(DATENAME(MONTH,convert(date,[Tran Date],101)),3) Months
							  ,convert(datetime2,convert(datetime2,[Tran Date] ,101),103)as [Date]
							  ,''''Annual Leave'''' Accounts
							  ,''''AUD''''  Currencies
							  ,''''Actual'''' Datatype 
							  ,''''Amount'''' GL_M	
							  ,OrgName	 
							  ,''''Payroll'''' JounralType		 
							  ,'''''''' [LineItemId]
							  ,''''Annual Leave for ''''+[First Name]+'''' ''''+[Last Name]+''''  '''' [LineItemDescription]
							  ,'''''''' [LineItemAccountId]
							  ,'''''''' [LineItemTaxCode]
							  ,'''''''' [LineItemTotalAmount]
							  ,'''''''' [LineItemQuantity]
							  ,'''''''' [LineItemUnitPrice]
							  ,'''''''' [LineItemPercentageDiscount]
							  ,'''''''' [LineItemInventoryId]
							  ,'''''''' [LineItemTags]
							  ,'''''''' [LineItemAttributes]
							  ,'''''''' [LineItem_links]
							  ,'''''''' [NotesInternal]
							  ,'''''''' [NotesExternal]
							  ,'''''''' [Attachments]
							  ,'''''''' [TemplateId]
							  ,'''''''' [SendEmailToContact]
							  ,'''''''' [EmailMessage]
							  ,'''''''' [QuickPayment]
							  ,'''''''' [TransactionId]
							  ,'''''''' [LastUpdatedId]
							  ,'''''''' [InvoiceNumber]
							  ,'''''''' [InvoiceType]
							  ,'''''''' [TransactionType]
							  ,'''''''' [Layout]
							  ,''''Annual Leave for ''''+[First Name]+'''' ''''+[Last Name]+''''  '''' [Summary]
							  ,'''''''' [TotalAmount]
							  ,'''''''' [TotalTaxAmount]
							  ,'''''''' [IsTaxInc]
							  ,'''''''' [AmountPaid]
							  ,'''''''' [AmountOwed]
							  ,'''''''' [FxRate]
							  ,'''''''' [AutoPopulateFxRate]
							  ,'''''''' [RequiresFollowUp]
							  ,'''''''' [SentToContact]
							  ,'''''''' [TransactionDate]
							  ,'''''''' [BillingContactId]
							  ,'''''''' [BillingContactFirstName]
							  ,'''''''' [BillingContactLastName]
							  ,'''''''' [BillingContactOrganisationName]
							  ,'''''''' [ShippingContactId]
							  ,'''''''' [ShippingContactFirstName]
							  ,'''''''' [ShippingContactLastName]
							  ,'''''''' [ShippingContactOrganisationName]
							  ,'''''''' [CreatedDateUtc]
							  ,'''''''' [LastModifiedDateUtc]
							  ,'''''''' [PaymentStatus]
							  ,'''''''' [DueDate]
							  ,'''''''' [InvoiceStatus]
							  ,'''''''' [PurchaseOrderNumber]
							  ,'''''''' [PaymentCount]
							  ,'''''''' [Tags]    
							  ,cast([Base Pay Hourly (Amount)] as money) 
							  as [#Value]

						  FROM PayrollSummary A
						  where
						   cast([Annual Leave Pay (Amount)] as money)<> 0

						   ';


						SET @SQLString=@SQLString+'  UNION ALL
						SELECT
							  cast(DATENAME(Year,convert(date,[Tran Date],101)) as varchar(100)) Years
							  ,LEFT(DATENAME(MONTH,convert(date,[Tran Date],101)),3) Months
							  ,convert(datetime2,convert(datetime2,[Tran Date] ,101),103)as [Date]
							  ,''''Superannuation'''' Accounts
							  ,''''AUD''''  Currencies
							  ,''''Actual'''' Datatype 
							  ,''''Amount'''' GL_M	
							  ,OrgName	 
							  ,''''Payroll'''' JounralType		 
							  ,'''''''' [LineItemId]
							  ,''''Superannuation for ''''+[First Name]+'''' ''''+[Last Name]+''''  '''' [LineItemDescription]
							  ,'''''''' [LineItemAccountId]
							  ,'''''''' [LineItemTaxCode]
							  ,'''''''' [LineItemTotalAmount]
							  ,'''''''' [LineItemQuantity]
							  ,'''''''' [LineItemUnitPrice]
							  ,'''''''' [LineItemPercentageDiscount]
							  ,'''''''' [LineItemInventoryId]
							  ,'''''''' [LineItemTags]
							  ,'''''''' [LineItemAttributes]
							  ,'''''''' [LineItem_links]
							  ,'''''''' [NotesInternal]
							  ,'''''''' [NotesExternal]
							  ,'''''''' [Attachments]
							  ,'''''''' [TemplateId]
							  ,'''''''' [SendEmailToContact]
							  ,'''''''' [EmailMessage]
							  ,'''''''' [QuickPayment]
							  ,'''''''' [TransactionId]
							  ,'''''''' [LastUpdatedId]
							  ,'''''''' [InvoiceNumber]
							  ,'''''''' [InvoiceType]
							  ,'''''''' [TransactionType]
							  ,'''''''' [Layout]
							  ,''''Superannuation for ''''+[First Name]+'''' ''''+[Last Name]+''''  '''' [Summary]
							  ,'''''''' [TotalAmount]
							  ,'''''''' [TotalTaxAmount]
							  ,'''''''' [IsTaxInc]
							  ,'''''''' [AmountPaid]
							  ,'''''''' [AmountOwed]
							  ,'''''''' [FxRate]
							  ,'''''''' [AutoPopulateFxRate]
							  ,'''''''' [RequiresFollowUp]
							  ,'''''''' [SentToContact]
							  ,'''''''' [TransactionDate]
							  ,'''''''' [BillingContactId]
							  ,'''''''' [BillingContactFirstName]
							  ,'''''''' [BillingContactLastName]
							  ,'''''''' [BillingContactOrganisationName]
							  ,'''''''' [ShippingContactId]
							  ,'''''''' [ShippingContactFirstName]
							  ,'''''''' [ShippingContactLastName]
							  ,'''''''' [ShippingContactOrganisationName]
							  ,'''''''' [CreatedDateUtc]
							  ,'''''''' [LastModifiedDateUtc]
							  ,'''''''' [PaymentStatus]
							  ,'''''''' [DueDate]
							  ,'''''''' [InvoiceStatus]
							  ,'''''''' [PurchaseOrderNumber]
							  ,'''''''' [PaymentCount]
							  ,'''''''' [Tags]    
							  ,cast([Superannuation Guarantee Contribution (SGC) (Amount)] as money)  
							  as [#Value]

						  FROM PayrollSummary A						  
						   where cast([Superannuation Guarantee Contribution (SGC) (Amount)] as money)<> 0)

	'' ';
			exec(@SQLString);

	SET @SQLString='EXEC [SAASU_'+@companyname +']..sp_executesql N''	
		Create View [dbo].[vwGL] AS
		(
			SELECT 
	  
				  cast(DATENAME(Year,convert(date,TransactionDate,101)) as varchar(100)) Years
				  ,LEFT(DATENAME(MONTH,convert(date,TransactionDate,101)),3) Months
				  ,convert(datetime2,convert(datetime2,[TransactionDate] ,101),103)as [Date]
				  ,C.Name Accounts
				  ,C.[AccountType]
				  ,A.[Currency] as Currencies
				  ,''''Actual'''' Datatype 
				  ,''''Amount'''' GL_M
				  ,A.OrgName
				  ,''''PurchaseInvoice'''' JounralType	  		    
				  ,A.[LineItemId]
				  ,A.[LineItemDescription]
				  ,A.[LineItemAccountId]
				  ,A.[LineItemTaxCode]
				  ,A.[LineItemTotalAmount]
				  ,A.[LineItemQuantity]
				  ,A.[LineItemUnitPrice]
				  ,A.[LineItemPercentageDiscount]
				  ,A.[LineItemInventoryId]
				  ,A.[LineItemTags]
				  ,A.[LineItemAttributes]
				  ,A.[LineItem_links]
				  ,A.[NotesInternal]
				  ,A.[NotesExternal]
				  ,A.[Attachments]
				  ,A.[TemplateId]
				  ,A.[SendEmailToContact]
				  ,A.[EmailMessage]
				  ,A.[QuickPayment]
				  ,A.[TransactionId]
				  ,A.[LastUpdatedId]
				  ,A.[InvoiceNumber]
				  ,A.[InvoiceType]
				  ,A.[TransactionType]
				  ,A.[Layout]
				  ,A.[Summary]
				  ,A.[TotalAmount]
				  ,A.[TotalTaxAmount]
				  ,A.[IsTaxInc]
				  ,A.[AmountPaid]
				  ,A.[AmountOwed]
				  ,A.[FxRate]
				  ,A.[AutoPopulateFxRate]
				  ,A.[RequiresFollowUp]
				  ,A.[SentToContact]
				  ,A.[TransactionDate]
				  ,A.[BillingContactId]
				  ,A.[BillingContactFirstName]
				  ,A.[BillingContactLastName]
				  ,A.[BillingContactOrganisationName]
				  ,A.[ShippingContactId]
				  ,A.[ShippingContactFirstName]
				  ,A.[ShippingContactLastName]
				  ,A.[ShippingContactOrganisationName]
				  ,A.[CreatedDateUtc]
				  ,A.[LastModifiedDateUtc]
				  ,A.[PaymentStatus]
				  ,A.[DueDate]
				  ,A.[InvoiceStatus]
				  ,A.[PurchaseOrderNumber]
				  ,A.[PaymentCount]
				  ,A.[Tags]	  
	  
				  ,[LineItemTotalAmount]
				  -
				  case
						when IsTaxInc= ''''TRUE''''
						 THEN 
							 case 
								when 
									LTRIM(RTRIM([LineItemTaxCode])) =''''''''
									OR 
									[LineItemTaxCode] is null
			
								  THEN 0
								  ELSE cast(B.Rate as money)*cast([LineItemTotalAmount] as money)/(1+cast(B.Rate as money))
								END 
						 ELSE 0
					END 
				  as [#Value]

	  

			  FROM [SAASU_'+@companyname +'].[dbo].[PurchaseInvoiceDetails] A
			  left join [SAASU_'+@companyname +'].[dbo].TaxCodes B
				  ON
				  A.LineItemTaxCode=B.Code
	  	  				  and
				  A.OrgName=B.OrgName
			   Left Join [SAASU_'+@companyname +'].[dbo].ItemsDetails D
				  ON
				  A.LineItemInventoryId=D.Id
	  	  	  				  and
				  A.OrgName=D.OrgName
			  LEFT JOIN [SAASU_'+@companyname +'].[dbo].Accounts C 
				  ON
				  (case 
					when A.LineItemAccountId is null
						OR
						 ltrim(rtrim(A.LineItemAccountId))=''''''''
					 then	D.PurchaseExpenseAccountId
					 else A.LineItemAccountId
					end 
				   )=C.Id
				   and
	   	  	  
				  A.OrgName=C.OrgName

	  		
			  where
			  [InvoiceType]<>''''Quote''''
			 ';
			  --exec( @SQLString);
			SET @SQLString=@SQLString+  '
			  UNION ALL

			  SELECT 
	  
				  DATENAME(Year,convert(date,TransactionDate,101)) Years
				  ,LEFT(DATENAME(MONTH,convert(date,TransactionDate,101)),3) Months
				  ,convert(datetime2,convert(datetime2,[TransactionDate] ,101),103)as [Date]
				  ,C.Name Accounts
				  ,C.[AccountType]
				  ,A.[Currency] Currencies
				  ,''''Actual'''' Datatype
				  ,''''Amount'''' GL_M	  
				  ,A.OrgName
				  ,''''SalesInvoice'''' JounralType
				  ,A.[LineItemId]
				  ,A.[LineItemDescription]
				  ,A.[LineItemAccountId]
				  ,A.[LineItemTaxCode]
				  ,A.[LineItemTotalAmount]
				  ,A.[LineItemQuantity]
				  ,A.[LineItemUnitPrice]
				  ,A.[LineItemPercentageDiscount]
				  ,A.[LineItemInventoryId]
				  ,A.[LineItemTags]
				  ,A.[LineItemAttributes]
				  ,A.[LineItem_links]
				  ,A.[NotesInternal]
				  ,A.[NotesExternal]
				  ,A.[Attachments]
				  ,A.[TemplateId]
				  ,A.[SendEmailToContact]
				  ,A.[EmailMessage]
				  ,A.[QuickPayment]
				  ,A.[TransactionId]
				  ,A.[LastUpdatedId]
				  ,A.[InvoiceNumber]
				  ,A.[InvoiceType]
				  ,A.[TransactionType]
				  ,A.[Layout]
				  ,A.[Summary]
				  ,A.[TotalAmount]
				  ,A.[TotalTaxAmount]
				  ,A.[IsTaxInc]
				  ,A.[AmountPaid]
				  ,A.[AmountOwed]
				  ,A.[FxRate]
				  ,A.[AutoPopulateFxRate]
				  ,A.[RequiresFollowUp]
				  ,A.[SentToContact]
				  ,A.[TransactionDate]
				  ,A.[BillingContactId]
				  ,A.[BillingContactFirstName]
				  ,A.[BillingContactLastName]
				  ,A.[BillingContactOrganisationName]
				  ,A.[ShippingContactId]
				  ,A.[ShippingContactFirstName]
				  ,A.[ShippingContactLastName]
				  ,A.[ShippingContactOrganisationName]
				  ,A.[CreatedDateUtc]
				  ,A.[LastModifiedDateUtc]
				  ,A.[PaymentStatus]
				  ,A.[DueDate]
				  ,A.[InvoiceStatus]
				  ,A.[PurchaseOrderNumber]
				  ,A.[PaymentCount]
				  ,A.[Tags]	  


				  ,[LineItemTotalAmount]
				  -
				  case
						when IsTaxInc= ''''TRUE''''
						 THEN 
							 case 
								when 
									LTRIM(RTRIM([LineItemTaxCode])) =''''''''
									OR 
									[LineItemTaxCode] is null
			
								  THEN 0
								  ELSE cast(B.Rate as money)*cast([LineItemTotalAmount] as money)/(1+cast(B.Rate as money))
								END 
						 ELSE 0
					END 
				  as Value	  

			  FROM [SAASU_'+@companyname +'].[dbo].[SalesInvoiceDetails] A
			  left join [SAASU_'+@companyname +'].[dbo].TaxCodes B
				  ON
				  A.LineItemTaxCode=B.Code
	  				  and
				  A.OrgName=B.OrgName
			   Left Join [SAASU_'+@companyname +'].[dbo].ItemsDetails D
				  ON
				  A.LineItemInventoryId=D.Id
	  	  				  and
				  A.OrgName=D.OrgName
			  LEFT JOIN [SAASU_'+@companyname +'].[dbo].Accounts C 
				  ON
				  (case 
					when A.LineItemAccountId is null
						OR
						 ltrim(rtrim(A.LineItemAccountId))=''''''''
					 then	D.SaleIncomeAccountId 
					 else A.LineItemAccountId
					end 
				   )=C.Id
				   and
	   	  	  
				  A.OrgName=C.OrgName
	
	  		
			  where
			  [InvoiceType]<>''''Quote''''
			  ';
			Set @SQLString=@SQLString+
			' UNION ALL

			select 
			 [Years]
				  ,[Months]
				  ,convert(datetime2,convert(datetime2,[Date] ,101),103)as [Date]
				  ,[Accounts]
				  ,''''Expense'''' [AccountType]
				  ,[Currencies]
				  ,[Datatype]
				  ,[GL_M]
				  ,[OrgName]
				  ,[JounralType]
				  ,[LineItemId]
				  ,[LineItemDescription]
				  ,[LineItemAccountId]
				  ,[LineItemTaxCode]
				  ,[LineItemTotalAmount]
				  ,[LineItemQuantity]
				  ,[LineItemUnitPrice]
				  ,[LineItemPercentageDiscount]
				  ,[LineItemInventoryId]
				  ,[LineItemTags]
				  ,[LineItemAttributes]
				  ,[LineItem_links]
				  ,[NotesInternal]
				  ,[NotesExternal]
				  ,[Attachments]
				  ,[TemplateId]
				  ,[SendEmailToContact]
				  ,[EmailMessage]
				  ,[QuickPayment]
				  ,[TransactionId]
				  ,[LastUpdatedId]
				  ,[InvoiceNumber]
				  ,[InvoiceType]
				  ,[TransactionType]
				  ,[Layout]
				  ,[Summary]
				  ,[TotalAmount]
				  ,[TotalTaxAmount]
				  ,[IsTaxInc]
				  ,[AmountPaid]
				  ,[AmountOwed]
				  ,[FxRate]
				  ,[AutoPopulateFxRate]
				  ,[RequiresFollowUp]
				  ,[SentToContact]
				  ,[TransactionDate]
				  ,[BillingContactId]
				  ,[BillingContactFirstName]
				  ,[BillingContactLastName]
				  ,[BillingContactOrganisationName]
				  ,[ShippingContactId]
				  ,[ShippingContactFirstName]
				  ,[ShippingContactLastName]
				  ,[ShippingContactOrganisationName]
				  ,[CreatedDateUtc]
				  ,[LastModifiedDateUtc]
				  ,[PaymentStatus]
				  ,[DueDate]
				  ,[InvoiceStatus]
				  ,[PurchaseOrderNumber]
				  ,[PaymentCount]
				  ,[Tags]
				  ,[#Value]
				from [SAASU_'+@companyname +'].[dbo].[vwPayroll]
				'
			
		set @SQLString=@SQLString+	' UNION ALL
			SELECT 
				  cast(DATENAME(Year,convert(date,[Date],101)) as varchar(100)) Years
				  ,LEFT(DATENAME(MONTH,convert(date,[Date],101)),3) Months
				  ,convert(datetime2,convert(datetime2,[Date] ,101),103)as [Date]
				  ,[accountUid] ''''Accounts''''
				  ,B.[AccountType]
				  ,[CCY] ''''Currencies''''
				  ,''''Actual'''' [Datatype]
				  ,''''Amount'''' [GL_M]
				  ,A.[OrgName]
				  ,''''Manual Journal'''' [JounralType]
				  ,'''''''' [LineItemId]
				  ,'''''''' [LineItemDescription]
				  ,'''''''' [LineItemAccountId]
				  ,'''''''' [LineItemTaxCode]
				  ,'''''''' [LineItemTotalAmount]
				  ,'''''''' [LineItemQuantity]
				  ,'''''''' [LineItemUnitPrice]
				  ,'''''''' [LineItemPercentageDiscount]
				  ,'''''''' [LineItemInventoryId]
				  ,'''''''' [LineItemTags]
				  ,'''''''' [LineItemAttributes]
				  ,'''''''' [LineItem_links]
				  ,'''''''' [NotesInternal]
				  ,'''''''' [NotesExternal]
				  ,'''''''' [Attachments]
				  ,'''''''' [TemplateId]
				  ,'''''''' [SendEmailToContact]
				  ,'''''''' [EmailMessage]
				  ,'''''''' [QuickPayment]
				  ,'''''''' [TransactionId]
				  ,'''''''' [LastUpdatedId]
				  ,'''''''' [InvoiceNumber]
				  ,'''''''' [InvoiceType]
				  ,[TransactionType]
				  ,'''''''' [Layout]
				  ,A.[Summary]
				  ,''''''''  [TotalAmount]
				  ,''''''''  [TotalTaxAmount]
				  ,'''''''' [IsTaxInc]
				  ,'''''''' [AmountPaid]
				  ,'''''''' [AmountOwed]
				  ,'''''''' [FxRate]
				  ,[AutoPopulateFxRate]
				  ,[RequiresFollowUp]
				  ,'''''''' [SentToContact]
				  ,'''''''' [TransactionDate]
				  ,'''''''' [BillingContactId]
				  ,'''''''' [BillingContactFirstName]
				  ,'''''''' [BillingContactLastName]
				  ,'''''''' [BillingContactOrganisationName]
				  ,'''''''' [ShippingContactId]
				  ,'''''''' [ShippingContactFirstName]
				  ,'''''''' [ShippingContactLastName]
				  ,'''''''' [ShippingContactOrganisationName]
				  ,'''''''' [CreatedDateUtc]
				  ,'''''''' [LastModifiedDateUtc]
				  ,'''''''' [PaymentStatus]
				  ,'''''''' [DueDate]
				  ,'''''''' [InvoiceStatus]
				  ,'''''''' [PurchaseOrderNumber]
				  ,'''''''' [PaymentCount]
				  ,'''''''' [Tags]
				  ,cast([amount] as money) as [#Value]
			  FROM [SAASU_'+@companyname +'].[dbo].[journals]	A
			  Left Join Accounts B
			  On
				A.accountUid=B.Id
			  );'''
			  
	 Exec(@SQLString);

	 	SET @SQLString='EXEC [SAASU_'+@companyname +']..sp_executesql N''	
		Create view vwGL_Summary
				as
				SELECT 
					  A.[OrgName]
					  ,[Years]
					  ,[Months]
					  ,convert(datetime2,convert(datetime2,[TransactionDate] ,101),103)as [Date]
					  ,[Accounts]
					  ,[AccountType]
					  ,[Currencies]
					  ,[Datatype]
					  ,[GL_M]
					  ,cast([#Value] as money) value
					  ,[BillingContactFirstName]
					  ,[BillingContactLastName]
					  ,[BillingContactOrganisationName]
					  ,B.PostalAddress as [Address]
					  ,[Summary]
					  ,[InvoiceNumber]
					  ,[InvoiceType]
					  ,[JounralType]
					  ,[LineItemDescription]
					  ,[LineItemTaxCode]
					  ,[LineItemTotalAmount]
					  ,[LineItemQuantity]
					  ,[LineItemUnitPrice]
					  ,[LineItemPercentageDiscount]
					  ,[LineItemTags]
					  ,[LineItemAttributes]
					  ,[LineItem_links]
					  ,[NotesInternal]
					  ,[NotesExternal]
					  ,[SendEmailToContact]
					  ,[EmailMessage]
					  ,[QuickPayment]
					  ,[TransactionType]

					  ,[TotalAmount]
					  ,[TotalTaxAmount]
					  ,[IsTaxInc]
					  ,[AmountPaid]
					  ,[AmountOwed]
					  ,[FxRate]
					  ,[AutoPopulateFxRate]
					  ,[RequiresFollowUp]
					  ,[SentToContact]
					  ,A.[CreatedDateUtc]
					  ,A.[LastModifiedDateUtc]
					  ,[PaymentStatus]
					  ,[DueDate]
					  ,[InvoiceStatus]
					  ,[PurchaseOrderNumber]
					  ,[PaymentCount]
					  ,A.[Tags]

				  FROM [SAASU_'+@companyname +'].[dbo].[vwGL] A

				  left Join Contacts B
				  on

				  A.BillingContactId=B.ID '' ';
	Exec(@SQLString);
/*	SET @SQLString='EXEC [SAASU_'+@companyname +']..sp_executesql N''	

							create view [dbo].[VW_Journals]
							as


							select 
									A.[OrgName]
									,A.JID
									,A.Date
								    ,A.AccountCode as AccountCode
								   	,A.AccountName
									,B.Code+'''' ''''+B.Name AccountCodeName
									,B.Name+'''' ''''+ B.Code AccountNameCode
									,B.Description as AccountDescription
									,B.Class AccountClass
								   ,LEFT(datename(M,Convert(Date,[Date],101)),3) as Months
								   ,cast(datepart(YYYY,Convert(Date,[Date],101)) as varchar(40)) as Years
								   ,''''Actual'''' Datatype
								   ,TrackingCategory1_option
								   ,TrackingCategory2_option	   
								   ,case 
									When Class=''''REVENUE'''' OR Class=''''EXPENSE''''
									 THEN -NetAmt
									 else NetAmt 
								   end as Value
							from Journals A
									left join
										[dbo].[ChartOfAccounts] B
										ON
										A.AccountCode=B.Code

							where
							 ValidationStatus =''''OK''''

							'''
	execute(@SQLString);	
	*/
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--select @SQLString;
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT @Database
END

