﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CheckDatabase]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @companyname VARCHAR(50)
DECLARE @companyid int;
DECLARE @databasename varchar(100);
DECLARE @Orgname VARCHAR(50)
DECLARE @sqlquery varchar(max);
DECLARE @sqlqueryforinnerloop nvarchar(max)
DECLARE @result int
DECLARE @tablename varchar(100)
Declare @sqlstatement nvarchar(max)
Declare @ErrorPass nvarchar(max) = ''



DECLARE company_cursor CURSOR FOR  

SELECT Name, CompanyId
FROM [ClearJelly].[dbo].[Company]

OPEN company_cursor   
FETCH NEXT FROM company_cursor INTO @companyname , @companyid

WHILE @@FETCH_STATUS = 0   
BEGIN   
      
		set @Orgname = null
		set @databasename = 'Xero_'+ @companyname

		select @Orgname = coalesce( @Orgname + ',''', '''') + OrgName + '''' 	from [ClearJelly].[dbo].[Xero_User_Org] where CompanyId = @companyid and IsActive = 1
		
		IF(@Orgname <> '')
		BEGIN

			set @sqlstatement = 'Declare  checktable CURSOR FOR SELECT name FROM '+  @databasename +'.sys.tables  where  name <> ''Calendar'''
			print(@sqlstatement)
			exec sp_executesql @sqlstatement

			OPEN checktable;

			FETCH NEXT FROM checktable INTO @tablename;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				set @sqlqueryforinnerloop = 'select @result = count(1) from ' + @databasename+'.dbo.'+ @tablename + ' where orgname not in ('+ @Orgname +')' 
				
				exec sp_executesql @sqlqueryforinnerloop, N'@result int out', @result out
				
				if( @result > 0 )
				begin

				Set @ErrorPass =  CASE  WHEN @ErrorPass = '' THEN  @databasename + ',' + @tablename else @ErrorPass + '##'+ @databasename + ',' + @tablename end
					--raiserror('mismath organization',16,1)
				print(@ErrorPass)
				end

			FETCH NEXT FROM checktable INTO @tablename;
			END;

			CLOSE checktable;
			DEALLOCATE checktable;

		END
		
	     FETCH NEXT FROM company_cursor INTO @companyname, @companyid
END  

CLOSE company_cursor   
DEALLOCATE company_cursor

END
