﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Delete_Xero_DB_For_User]
@username varchar(400)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @db_name varchar(600);
	Declare @sql varchar(max);

	select @db_name=(case 
				when name like 'Xero_%'
				THEN  name 
				ELSE 'Not Allowed'
			END )
		from sys.databases 
		where suser_sname(owner_sid) = N''+@username+'';--hesam.ziaei@managility.com.au'
    If @db_name <> 'Not Allowed'
	   AND 
	   ltrim(rtrim(@db_name))<>''
	   AND
	   ltrim(rtrim(@db_name)) like 'Xero_%'
	Begin
		set @sql= 'ALTER DATABASE ['+@db_name+'] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;  Drop database ['+@db_name+'];' 
		execute(@sql);
	End
	print @db_name;
	select @sql;
END

