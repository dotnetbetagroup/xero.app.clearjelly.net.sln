﻿
CREATE PROCEDURE [dbo].[UpdateUserPassword] 
	@user nvarchar(255),
	@oldPassword nvarchar(255),
	@newPassword nvarchar(255)
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @sql NVARCHAR(500)
	SET @sql = 'ALTER LOGIN ' + QuoteName(@user) + 
	' WITH PASSWORD= ' + QuoteName(@newPassword, '''')  +'
	OLD_PASSWORD = ' + QuoteName(@oldPassword, '''')
	execute (@sql)
	select 1 as isPasswordUpdateSuccess
END TRY
BEGIN CATCH  
    select 0 as isPasswordUpdateSuccess 
END CATCH;  