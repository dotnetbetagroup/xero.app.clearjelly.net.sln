﻿

--exec [Create_QuickBooks_DB_Template] 'ze---Test','temp@temp.com','123456'

CREATE PROCEDURE [dbo].[Create_QBReport_Table]
--alter PROCEDURE [dbo].[Create_QBReport_Table]

	-- Add the parameters for the stored procedure here
	@companyname varchar(200)
	--@username varchar(200),
	--@password varchar(200)
AS
BEGIN

	DECLARE @QBReportString VARCHAR(MAX)
	SET @QBReportString = ''	

	SET @QBReportString=@QBReportString+' 										
		CREATE TABLE [QuickBooks_'+@companyname +'].[dbo].[QBReport](
		[Id] [nvarchar](4000) PRIMARY KEY NOT NULL,
		[CreationDate] [datetime] NULL,
		[Date] [nvarchar](4000) NULL,
        [TransactionType] [nvarchar](4000) NULL,
        [Num]  [nvarchar](4000) NULL,
        [Adj]  [nvarchar](4000) NULL,
        [CreateDate] [nvarchar](4000) NULL,
        [CreatedBy]  [nvarchar](4000) NULL,
        [LastModified] [nvarchar](4000) NULL,
        [LastModifiedBy] [nvarchar](4000) NULL,
        [Name] [nvarchar](4000) NULL,
        [Customer] [nvarchar](4000) NULL,
		[Vendor] [nvarchar](4000) NULL,
        [Employee] [nvarchar](4000) NULL,
       [ProductService] [nvarchar](4000) NULL,
       [MemoDescription] [nvarchar](4000) NULL,
       [Qty] [nvarchar](4000) NULL,
       [Rate] [nvarchar](4000) NULL,
       [Account] [nvarchar](4000) NULL,
       [Split] [nvarchar](4000) NULL,
       [InvoiceDate] [nvarchar](4000) NULL,
       [ARPaid] [nvarchar](4000) NULL,
       [APPaid] [nvarchar](4000) NULL,
       [Clr] [nvarchar](4000) NULL,
       [CheckPrinted] [nvarchar](4000) NULL,
       [OpenBalance] [nvarchar](4000) NULL,
       [Amount] [nvarchar](4000) NULL,
       [Balance] [nvarchar](4000) NULL	
		) ON [PRIMARY];		
					 ';
	execute(@QBReportString);

END