﻿

-- ==============================================
-- Author:		Hesam Ziaei
-- Create date: 20/06/2015
-- Description:	This script creates a login
--				 and an empty ClearJelly database
--				 for a new customer 
--				 
-- ==============================================
--exec [Create_SAASU_DB_Template] 'd11 emo-DB_12','trest13@gmail.com','123456'
CREATE PROCEDURE [dbo].[Create_SAASU_DB_Template]
	-- Add the parameters for the stored procedure here
	@companyname varchar(200),
	@username varchar(200),
	@password varchar(200)
AS
BEGIN

	DECLARE @SQLString NVARCHAR(MAX)
	DECLARE @SQLDB NVARCHAR(MAX)
	SET @SQLDB = ''

	SET @SQLString = ''

/*CREATE USER [ETL] FOR LOGIN [ETL] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [ETL]
GO*/
/****** Object:  Table [dbo].[Accnts_PnL_hierarchy]    Script Date: 15/06/2015 6:35:12 PM ******/
	SET @SQLDB =@SQLDB +'create database [SAASU_'+@companyname +']';
	execute(@SQLDB )

	--SET @SQLDB ='Use master;';
	--execute(@SQLDB )

	--SET @SQLDB ='CREATE LOGIN '+@username+'  WITH PASSWORD ='''+@password+'''';
	--execute(@SQLDB )	
	--SET @SQLDB ='CREATE USER '+@username+'  FOR LOGIN '+@username+' ';	
	--execute(@SQLDB );

	SET @SQLDB =+'use [SAASU_'+@companyname +']';
	execute(@SQLDB )

	SET @SQLString=@SQLString+' 
								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[Accounts](
									[Id] [varchar](200) NULL,
									[Name] [varchar](800) NULL,
									[AccountType] [varchar](200) NULL,
									[IsActive] [varchar](200) NULL,
									[IsBuiltIn] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[DefaultTaxCode] [varchar](200) NULL,
									[LedgerCode] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[ExchangeAccountId] [varchar](200) NULL,
									[IsBankAccount] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[IncludeInForecaster] [varchar](200) NULL,
									[BSB] [varchar](200) NULL,
									[Number] [varchar](200) NULL,
									[BankAccountName] [varchar](800) NULL,
									[BankFileCreationEnabled] [varchar](200) NULL,
									[BankCode] [varchar](200) NULL,
									[UserNumber] [varchar](200) NULL,
									[MerchantFeeAccountId] [varchar](200) NULL,
									[IncludePendingTransactions] [varchar](200) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[Companies](
									[Id] [varchar](200) NULL,
									[Name] [varchar](800) NULL,
									[Abn] [varchar](200) NULL,
									[Website] [varchar](400) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[LongDescription] [varchar](200) NULL,
									[LogoUrl] [varchar](200) NULL,
									[TradingName] [varchar](400) NULL,
									[CompanyEmail] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedByUserId] [varchar](200) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[Contacts](
									[Id] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[Salutation] [varchar](200) NULL,
									[GivenName] [varchar](200) NULL,
									[MiddleInitials] [varchar](200) NULL,
									[FamilyName] [varchar](200) NULL,
									[IsActive] [varchar](200) NULL,
									[CompanyId] [varchar](200) NULL,
									[PositionTitle] [varchar](200) NULL,
									[WebsiteUrl] [varchar](200) NULL,
									[PrimaryPhone] [varchar](200) NULL,
									[HomePhone] [varchar](200) NULL,
									[OtherPhone] [varchar](200) NULL,
									[MobilePhone] [varchar](200) NULL,
									[Fax] [varchar](200) NULL,
									[EmailAddress] [varchar](400) NULL,
									[ContactId] [varchar](200) NULL,
									[ContactManagerId] [varchar](200) NULL,
									[DirectDepositDetails] [varchar](400) NULL,
									[ChequeDetails] [varchar](200) NULL,
									[CustomField1] [varchar](600) NULL,
									[CustomField2] [varchar](600) NULL,
									[TwitterId] [varchar](200) NULL,
									[SkypeId] [varchar](200) NULL,
									[LinkedInProfile] [varchar](200) NULL,
									[AutoSendStatement] [varchar](200) NULL,
									[IsPartner] [varchar](200) NULL,
									[IsCustomer] [varchar](200) NULL,
									[IsSupplier] [varchar](200) NULL,
									[IsContractor] [varchar](200) NULL,
									[Tags] [varchar](max) NULL,
									[DefaultSaleDiscount] [varchar](200) NULL,
									[DefaultPurchaseDiscount] [varchar](200) NULL,
									[LastModifiedByUserId] [varchar](200) NULL,
									[BpayDetails] [varchar](400) NULL,
									[PostalAddress] [varchar](max) NULL,
									[OtherAddress] [varchar](max) NULL,
									[SaleTradingTerms] [varchar](max) NULL,
									[PurchaseTradingTerms] [varchar](max) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[Items](
									[Id] [varchar](200) NULL,
									[Code] [varchar](200) NULL,
									[Description] [varchar](max) NULL,
									[Type] [varchar](200) NULL,
									[OnOrder] [varchar](200) NULL,
									[IsActive] [varchar](200) NULL,
									[IsInventoried] [varchar](200) NULL,
									[AssetAccountId] [varchar](200) NULL,
									[IsSold] [varchar](200) NULL,
									[SaleIncomeAccountId] [varchar](200) NULL,
									[SaleTaxCodeId] [varchar](200) NULL,
									[SaleCoSAccountId] [varchar](200) NULL,
									[IsBought] [varchar](200) NULL,
									[PurchaseExpenseAccountId] [varchar](200) NULL,
									[PurchaseTaxCodeId] [varchar](200) NULL,
									[MinimumStockLevel] [varchar](200) NULL,
									[StockOnHand] [varchar](200) NULL,
									[CurrentValue] [varchar](200) NULL,
									[PrimarySupplierContactId] [varchar](200) NULL,
									[PrimarySupplierItemCode] [varchar](200) NULL,
									[DefaultReOrderQuantity] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[IsVisible] [varchar](200) NULL,
									[IsVirtual] [varchar](200) NULL,
									[VType] [varchar](200) NULL,
									[SellingPrice] [varchar](200) NULL,
									[IsSellingPriceIncTax] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[LastModifiedBy] [varchar](200) NULL,
									[BuyingPrice] [varchar](200) NULL,
									[IsBuyingPriceIncTax] [varchar](200) NULL,
									[IsVoucher] [varchar](200) NULL,
									[ValidFrom] [varchar](200) NULL,
									[ValidTo] [varchar](200) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[ItemsDetails](
									[Notes] [varchar](max) NULL,
									[BuildItems] [varchar](max) NULL,
									[Id] [varchar](200) NULL,
									[Code] [varchar](200) NULL,
									[Description] [varchar](max) NULL,
									[Type] [varchar](200) NULL,
									[IsActive] [varchar](200) NULL,
									[IsInventoried] [varchar](200) NULL,
									[AssetAccountId] [varchar](200) NULL,
									[IsSold] [varchar](200) NULL,
									[SaleIncomeAccountId] [varchar](200) NULL,
									[SaleTaxCodeId] [varchar](200) NULL,
									[SaleCoSAccountId] [varchar](200) NULL,
									[IsBought] [varchar](200) NULL,
									[PurchaseExpenseAccountId] [varchar](200) NULL,
									[PurchaseTaxCodeId] [varchar](200) NULL,
									[MinimumStockLevel] [varchar](200) NULL,
									[StockOnHand] [varchar](200) NULL,
									[CurrentValue] [varchar](200) NULL,
									[PrimarySupplierContactId] [varchar](200) NULL,
									[PrimarySupplierItemCode] [varchar](200) NULL,
									[DefaultReOrderQuantity] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[IsVisible] [varchar](200) NULL,
									[IsVirtual] [varchar](200) NULL,
									[VType] [varchar](200) NULL,
									[SellingPrice] [varchar](200) NULL,
									[IsSellingPriceIncTax] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[LastModifiedBy] [varchar](200) NULL,
									[BuyingPrice] [varchar](200) NULL,
									[IsBuyingPriceIncTax] [varchar](200) NULL,
									[IsVoucher] [varchar](200) NULL,
									[ValidFrom] [varchar](200) NULL,
									[ValidTo] [varchar](200) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[journals](
									[JournalID] [varchar](200) NULL,
									[utcFirstCreated] [datetime] NULL,
									[utcLastModified] [datetime] NULL,
									[transactionType] [varchar](200) NULL,
									[Date] [datetime] NULL,
									[Summary] [varchar](max) NULL,
									[requiresFollowUp] [varchar](200) NULL,
									[CCY] [varchar](200) NULL,
									[autoPopulateFxRate] [varchar](200) NULL,
									[fcToBcFxRate] [varchar](200) NULL,
									[accountUid] [varchar](200) NULL,
									[taxCode] [varchar](200) NULL,
									[amount] [varchar](200) NULL,
									[type] [varchar](200) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[PayrollSummary](
									[Last Name] [nvarchar](4000) NULL,
									[First Name] [nvarchar](4000) NULL,
									[Email Address] [nvarchar](4000) NULL,
									[Contact ID] [nvarchar](4000) NULL,
									[Tran Date] [nvarchar](4000) NULL,
									[Annual Leave Pay (Amount)] [nvarchar](4000) NULL,
									[Annual Leave Pay (Hour)] [nvarchar](4000) NULL,
									[Base Pay Hourly (Amount)] [nvarchar](4000) NULL,
									[Base Pay Hourly (Hour)] [nvarchar](4000) NULL,
									[PAYG Withholding (Amount)] [nvarchar](4000) NULL,
									[Superannuation Guarantee Contribution (SGC) (Amount)] [nvarchar](4000) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[PurchaseInvoiceDetails](
									[LineItemId] [varchar](200) NULL,
									[LineItemDescription] [varchar](max) NULL,
									[LineItemAccountId] [varchar](200) NULL,
									[LineItemTaxCode] [varchar](200) NULL,
									[LineItemTotalAmount] [varchar](200) NULL,
									[LineItemQuantity] [varchar](200) NULL,
									[LineItemUnitPrice] [varchar](200) NULL,
									[LineItemPercentageDiscount] [varchar](200) NULL,
									[LineItemInventoryId] [varchar](200) NULL,
									[LineItemTags] [varchar](max) NULL,
									[LineItemAttributes] [varchar](max) NULL,
									[LineItem_links] [varchar](max) NULL,
									[NotesInternal] [varchar](200) NULL,
									[NotesExternal] [varchar](200) NULL,
									[Terms] [varchar](max) NULL,
									[Attachments] [varchar](max) NULL,
									[TemplateId] [varchar](200) NULL,
									[SendEmailToContact] [varchar](max) NULL,
									[EmailMessage] [varchar](max) NULL,
									[QuickPayment] [varchar](200) NULL,
									[TransactionId] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[InvoiceNumber] [varchar](200) NULL,
									[InvoiceType] [varchar](200) NULL,
									[TransactionType] [varchar](200) NULL,
									[Layout] [varchar](200) NULL,
									[Summary] [varchar](max) NULL,
									[TotalAmount] [varchar](200) NULL,
									[TotalTaxAmount] [varchar](200) NULL,
									[IsTaxInc] [varchar](200) NULL,
									[AmountPaid] [varchar](200) NULL,
									[AmountOwed] [varchar](200) NULL,
									[FxRate] [varchar](200) NULL,
									[AutoPopulateFxRate] [varchar](200) NULL,
									[RequiresFollowUp] [varchar](200) NULL,
									[SentToContact] [varchar](200) NULL,
									[TransactionDate] [varchar](200) NULL,
									[BillingContactId] [varchar](200) NULL,
									[BillingContactFirstName] [varchar](400) NULL,
									[BillingContactLastName] [varchar](400) NULL,
									[BillingContactOrganisationName] [varchar](400) NULL,
									[ShippingContactId] [varchar](200) NULL,
									[ShippingContactFirstName] [varchar](400) NULL,
									[ShippingContactLastName] [varchar](400) NULL,
									[ShippingContactOrganisationName] [varchar](400) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[PaymentStatus] [varchar](200) NULL,
									[DueDate] [varchar](200) NULL,
									[InvoiceStatus] [varchar](200) NULL,
									[PurchaseOrderNumber] [varchar](200) NULL,
									[PaymentCount] [varchar](200) NULL,
									[Tags] [varchar](max) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[PurchaseInvoices](
									[TransactionId] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[InvoiceNumber] [varchar](200) NULL,
									[InvoiceType] [varchar](200) NULL,
									[TransactionType] [varchar](200) NULL,
									[Layout] [varchar](200) NULL,
									[Summary] [varchar](max) NULL,
									[TotalAmount] [varchar](200) NULL,
									[TotalTaxAmount] [varchar](200) NULL,
									[IsTaxInc] [varchar](200) NULL,
									[AmountPaid] [varchar](200) NULL,
									[AmountOwed] [varchar](200) NULL,
									[FxRate] [varchar](200) NULL,
									[AutoPopulateFxRate] [varchar](200) NULL,
									[RequiresFollowUp] [varchar](200) NULL,
									[SentToContact] [varchar](200) NULL,
									[TransactionDate] [varchar](200) NULL,
									[BillingContactId] [varchar](200) NULL,
									[BillingContactFirstName] [varchar](200) NULL,
									[BillingContactLastName] [varchar](200) NULL,
									[BillingContactOrganisationName] [varchar](200) NULL,
									[ShippingContactId] [varchar](200) NULL,
									[ShippingContactFirstName] [varchar](200) NULL,
									[ShippingContactLastName] [varchar](200) NULL,
									[ShippingContactOrganisationName] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[PaymentStatus] [varchar](200) NULL,
									[DueDate] [varchar](200) NULL,
									[InvoiceStatus] [varchar](200) NULL,
									[PurchaseOrderNumber] [varchar](200) NULL,
									[PaymentCount] [varchar](200) NULL,
									[Tags] [varchar](max) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[PurchaseInvoices_oldApi](
									[invoiceUid] [varchar](200) NULL,
									[lastUpdatedUid] [varchar](200) NULL,
									[ccy] [varchar](200) NULL,
									[autoPopulateFXRate] [varchar](200) NULL,
									[fcToBcFxRate] [varchar](200) NULL,
									[transactionType] [varchar](200) NULL,
									[invoiceDate] [varchar](200) NULL,
									[utcFirstCreated] [varchar](200) NULL,
									[utcLastModified] [varchar](200) NULL,
									[summary] [varchar](max) NULL,
									[invoiceNumber] [varchar](200) NULL,
									[purchaseOrderNumber] [varchar](200) NULL,
									[dueDate] [varchar](200) NULL,
									[totalAmountInclTax] [varchar](200) NULL,
									[paymentCount] [varchar](200) NULL,
									[totalAmountPaid] [varchar](200) NULL,
									[amountOwed] [varchar](200) NULL,
									[paidStatus] [varchar](200) NULL,
									[requiresFollowUp] [varchar](200) NULL,
									[isSent] [varchar](200) NULL,
									[invoiceLayout] [varchar](200) NULL,
									[invoiceStatus] [varchar](200) NULL,
									[contactUid] [varchar](200) NULL,
									[contactGivenName] [varchar](200) NULL,
									[contactFamilyName] [varchar](200) NULL,
									[contactOrganisationName] [varchar](200) NULL,
									[shipToContactUid] [varchar](200) NULL,
									[shipToContactGivenName] [varchar](200) NULL,
									[shipToContactLastName] [varchar](200) NULL,
									[shipToContactOrganisation] [varchar](200) NULL,
									[tags] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[PurchasePayments](
									[TransactionId] [varchar](200) NULL,
									[TransactionDate] [varchar](200) NULL,
									[TransactionType] [varchar](200) NULL,
									[PaymentAccountId] [varchar](200) NULL,
									[TotalAmount] [varchar](200) NULL,
									[FeeAmount] [varchar](200) NULL,
									[Summary] [varchar](max) NULL,
									[Reference] [varchar](200) NULL,
									[ClearedDate] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[AutoPopulateFxRate] [varchar](200) NULL,
									[FxRate] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[RequiresFollowUp] [varchar](200) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[SalesInvoiceDetails](
									[LineItemId] [varchar](200) NULL,
									[LineItemDescription] [varchar](max) NULL,
									[LineItemAccountId] [varchar](200) NULL,
									[LineItemTaxCode] [varchar](200) NULL,
									[LineItemTotalAmount] [varchar](200) NULL,
									[LineItemQuantity] [varchar](200) NULL,
									[LineItemUnitPrice] [varchar](200) NULL,
									[LineItemPercentageDiscount] [varchar](max) NULL,
									[LineItemInventoryId] [varchar](200) NULL,
									[LineItemTags] [varchar](max) NULL,
									[LineItemAttributes] [varchar](max) NULL,
									[LineItem_links] [varchar](max) NULL,
									[NotesInternal] [varchar](max) NULL,
									[NotesExternal] [varchar](max) NULL,
									[Terms] [varchar](max) NULL,
									[Attachments] [varchar](max) NULL,
									[TemplateId] [varchar](200) NULL,
									[SendEmailToContact] [varchar](max) NULL,
									[EmailMessage] [varchar](max) NULL,
									[QuickPayment] [varchar](200) NULL,
									[TransactionId] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[InvoiceNumber] [varchar](200) NULL,
									[InvoiceType] [varchar](200) NULL,
									[TransactionType] [varchar](200) NULL,
									[Layout] [varchar](200) NULL,
									[Summary] [varchar](max) NULL,
									[TotalAmount] [varchar](200) NULL,
									[TotalTaxAmount] [varchar](200) NULL,
									[IsTaxInc] [varchar](200) NULL,
									[AmountPaid] [varchar](200) NULL,
									[AmountOwed] [varchar](200) NULL,
									[FxRate] [varchar](200) NULL,
									[AutoPopulateFxRate] [varchar](200) NULL,
									[RequiresFollowUp] [varchar](200) NULL,
									[SentToContact] [varchar](200) NULL,
									[TransactionDate] [varchar](200) NULL,
									[BillingContactId] [varchar](200) NULL,
									[BillingContactFirstName] [varchar](400) NULL,
									[BillingContactLastName] [varchar](400) NULL,
									[BillingContactOrganisationName] [varchar](400) NULL,
									[ShippingContactId] [varchar](200) NULL,
									[ShippingContactFirstName] [varchar](400) NULL,
									[ShippingContactLastName] [varchar](400) NULL,
									[ShippingContactOrganisationName] [varchar](400) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[PaymentStatus] [varchar](200) NULL,
									[DueDate] [varchar](200) NULL,
									[InvoiceStatus] [varchar](200) NULL,
									[PurchaseOrderNumber] [varchar](200) NULL,
									[PaymentCount] [varchar](200) NULL,
									[Tags] [varchar](max) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[SalesInvoices](
									[TransactionId] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[InvoiceNumber] [varchar](200) NULL,
									[InvoiceType] [varchar](200) NULL,
									[TransactionType] [varchar](200) NULL,
									[Layout] [varchar](200) NULL,
									[Summary] [varchar](max) NULL,
									[TotalAmount] [varchar](200) NULL,
									[TotalTaxAmount] [varchar](200) NULL,
									[IsTaxInc] [varchar](200) NULL,
									[AmountPaid] [varchar](200) NULL,
									[AmountOwed] [varchar](200) NULL,
									[FxRate] [varchar](200) NULL,
									[AutoPopulateFxRate] [varchar](200) NULL,
									[RequiresFollowUp] [varchar](200) NULL,
									[SentToContact] [varchar](200) NULL,
									[TransactionDate] [varchar](200) NULL,
									[BillingContactId] [varchar](200) NULL,
									[BillingContactFirstName] [varchar](200) NULL,
									[BillingContactLastName] [varchar](200) NULL,
									[BillingContactOrganisationName] [varchar](200) NULL,
									[ShippingContactId] [varchar](200) NULL,
									[ShippingContactFirstName] [varchar](200) NULL,
									[ShippingContactLastName] [varchar](200) NULL,
									[ShippingContactOrganisationName] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[PaymentStatus] [varchar](200) NULL,
									[DueDate] [varchar](200) NULL,
									[InvoiceStatus] [varchar](200) NULL,
									[PurchaseOrderNumber] [varchar](200) NULL,
									[PaymentCount] [varchar](200) NULL,
									[Tags] [varchar](max) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[SalesInvoices_oldApi](
									[invoiceUid] [varchar](200) NULL,
									[lastUpdatedUid] [varchar](200) NULL,
									[ccy] [varchar](200) NULL,
									[autoPopulateFXRate] [varchar](200) NULL,
									[fcToBcFxRate] [varchar](200) NULL,
									[transactionType] [varchar](200) NULL,
									[invoiceDate] [varchar](200) NULL,
									[utcFirstCreated] [varchar](200) NULL,
									[utcLastModified] [varchar](200) NULL,
									[summary] [varchar](max) NULL,
									[invoiceNumber] [varchar](200) NULL,
									[purchaseOrderNumber] [varchar](200) NULL,
									[dueDate] [varchar](200) NULL,
									[totalAmountInclTax] [varchar](200) NULL,
									[paymentCount] [varchar](200) NULL,
									[totalAmountPaid] [varchar](200) NULL,
									[amountOwed] [varchar](200) NULL,
									[paidStatus] [varchar](200) NULL,
									[requiresFollowUp] [varchar](200) NULL,
									[isSent] [varchar](200) NULL,
									[invoiceLayout] [varchar](200) NULL,
									[invoiceStatus] [varchar](200) NULL,
									[contactUid] [varchar](200) NULL,
									[contactGivenName] [varchar](200) NULL,
									[contactFamilyName] [varchar](200) NULL,
									[contactOrganisationName] [varchar](200) NULL,
									[shipToContactUid] [varchar](200) NULL,
									[shipToContactGivenName] [varchar](200) NULL,
									[shipToContactLastName] [varchar](200) NULL,
									[shipToContactOrganisation] [varchar](200) NULL,
									[tags] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[SalesPayments](
									[TransactionId] [varchar](200) NULL,
									[TransactionDate] [varchar](200) NULL,
									[TransactionType] [varchar](200) NULL,
									[PaymentAccountId] [varchar](200) NULL,
									[TotalAmount] [varchar](200) NULL,
									[FeeAmount] [varchar](200) NULL,
									[Summary] [varchar](max) NULL,
									[Reference] [varchar](200) NULL,
									[ClearedDate] [varchar](200) NULL,
									[Currency] [varchar](200) NULL,
									[AutoPopulateFxRate] [varchar](200) NULL,
									[FxRate] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[RequiresFollowUp] [varchar](200) NULL,
									[_links] [varchar](max) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


								CREATE TABLE [SAASU_'+@companyname +'].[dbo].[TaxCodes](
									[Id] [varchar](200) NULL,
									[Code] [varchar](200) NULL,
									[Name] [varchar](200) NULL,
									[Rate] [varchar](200) NULL,
									[PostingAccountId] [varchar](200) NULL,
									[IsSale] [varchar](200) NULL,
									[IsPurchase] [varchar](200) NULL,
									[IsPayroll] [varchar](200) NULL,
									[IsInbuilt] [varchar](200) NULL,
									[IsShared] [varchar](200) NULL,
									[IsActive] [varchar](200) NULL,
									[CreatedDateUtc] [varchar](200) NULL,
									[LastModifiedDateUtc] [varchar](200) NULL,
									[LastModifiedByUserId] [varchar](200) NULL,
									[LastUpdatedId] [varchar](200) NULL,
									[_links] [varchar](200) NULL,
									[OrgName] [varchar](600) NULL,
									[FileId] [varchar](600) NULL
								) ON [PRIMARY]
 
														
			 ';
	execute(@SQLString);	

	
	-- Copying the clear jelly standard calendar to user's database
	Set @SQLString='Select * into [SAASU_'+@companyname +'].dbo.Calendar from ClearJelly.dbo.calendar';
	execute(@SQLString);


	Execute [dbo].[CreateViewsSAASU] @companyname;
	--execute(@SQLString);


	SET @SQLDB ='USE [master] ';
	execute(@SQLDB);
	SET @SQLDB =  ' USE [master];CREATE LOGIN ['+@username+'] WITH PASSWORD=N'''+@password+''', CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;';

	SET @SQLDB = @SQLDB + ' DENY VIEW ANY DATABASE TO ['+@username+'];' ; 
	execute(@SQLDB);

	--SET @SQLDB = ' USE [SAASU_'+@username +']' ;
	--execute(@SQLDB);

	SET @SQLDB = ' USE master';
	execute(@SQLDB);

	SET @SQLDB = ' ALTER AUTHORIZATION ON DATABASE::[SAASU_'+@companyname +'] TO ['+@username +']';
	execute(@SQLDB);
	

END




