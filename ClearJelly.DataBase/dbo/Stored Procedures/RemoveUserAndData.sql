﻿
--exec RemoveUserAndData 'bhavik.kathiriya@zealousys.local'
CREATE PROCEDURE RemoveUserAndData 
	@email nvarchar(255)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @CompanyId int
	declare @DbName nvarchar(200)

    -- Insert statements for procedure here
	SELECT @CompanyId = CompanyId from [User] where Email = @email
	SELECT @DbName = 'Xero_'+Name from Company where CompanyId = @CompanyId

	print 'Deleting company subscription for CompanyId: '+Convert(nvarchar, @CompanyId);
	--delete from CompanySubscription where CompanyId = @CompanyId
	print 'Deleting User for CompanyId: '+Convert(nvarchar, @CompanyId);
	--delete from [User] where CompanyId = @CompanyId
	
	print 'Database: ' + @DbName


END
