﻿
Create PROCEDURE [dbo].[UpdateUserPasswordNew] 
	@user nvarchar(255),
	@oldPassword nvarchar(255),
	@newPassword nvarchar(255),
	 @IsTrue BIT OUTPUT
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @sql NVARCHAR(500)
	SET @sql = 'ALTER LOGIN ' + QuoteName(@user) + 
	' WITH PASSWORD= ' + QuoteName(@newPassword, '''')  +'
	OLD_PASSWORD = ' + QuoteName(@oldPassword, '''')
	execute (@sql)

	SET @IsTrue = 0x1
	return @IsTrue
END TRY
BEGIN CATCH  
    
    SET @IsTrue = 0x0
	return @IsTrue
END CATCH;  



