﻿-- =============================================
-- Author:		Hesam Ziaei
-- Create date: 10/10/2015
-- Description:	This Procedure creates the necessary views to be used by CleayJelly end users on each database
-- =============================================
CREATE PROCEDURE [dbo].[CreateViews] 
	-- Add the parameters for the stored procedure here
	@companyname Varchar(600) 

AS
BEGIN
	DECLARE @SQLString NVARCHAR(MAX);
	SET @SQLString  =' use [Xero_'+@companyname +']';
	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''VW_InvoicesAndCredits'')
									   DROP VIEW VW_InvoicesAndCredits;';
	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''VW_Journals'')
									   DROP VIEW VW_Journals;';

	SET @SQLString=@SQLString + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
											 WHERE TABLE_NAME = ''VW_PurchaseOrders'')
									   DROP VIEW vw_PurchaseOrders;';

	execute(@SQLString );

	SET @SQLString='EXEC [Xero_'+@companyname +']..sp_executesql N''	
		CREATE View [dbo].[VW_InvoicesAndCredits] AS 
( 
SELECT 
A.OrgName, A.[Date]
, A.AccountCode, E.Name as AccountName 
, E.Class 
, ''''Invoice'''' as SalesType 
, left(DATENAME(MM, convert(date, A.Date, 103)), 3) as Month
, cast(datepart(yyyy, convert(date, A.Date, 103)) as varchar(4))YEAR 
, A.Status as payment_status 
, A.Type
, C.ContactName

, CASE 
WHEN A.ItemCode is null 
THEN ''''Unassigned'''' 
ELSE A.ItemCode 
END Item 
,case when LineAmountTypes = ''''Inclusive''''
then A.[LineAmount] - A.[TaxAmount]
else A.[LineAmount] 
end LineAmount_after_tax 
, A.[LineAmount]
, A.[Quantity] as [LineQuantity]
, A.[TaxAmount] as  [LineTaxAmount] 
, A.[TaxType]
, A.[UnitAmount] as  [UnitAmount]
, D.AddressLine1 
, D.City
, D.Country
, D.Region
, D.PostalCode
, A.[type] as LineAmountTypes
, A.[TrackingCategory1]
, A.[TrackingCategory1_Option]
, A.[TrackingCategory2]
, A.[TrackingCategory2_Option]
, A.DueDate
, A.InvoiceNumber
, ''''Actual'''' Scenario FROM [Xero_'+@companyname +'].[dbo].Invoices A

  Left JOIN [Xero_'+@companyname +'].[dbo].Contacts C 
  ON
  A.Contact = C.ContactID 
  and 
  A.OrgName = C.OrgName 
  left JOIN 
  
  (select * from 
  ( select Dense_Rank() over(partition by ContactID order by AddressID) rw, A.* from [Xero_'+@companyname +'].[dbo].[Contact_Address]  A) 
  adrs 
  where adrs.rw = 2
  ) D 
  ON 
  A.Contact = D.ContactID  
  and
  A.OrgName = D.OrgName 
  left join [Xero_'+@companyname +'].[dbo].[ChartOfAccounts] E   
  On  
  A.[AccountCode] = E.Code   
  and     
  A.OrgName = E.OrgName   
  UNION ALL 
  SELECT  A.OrgName,[Date], A.AccountCode, E.Name as AccountName, E.Class,
  ''''Credit Note'''' SalesType     
  , left(DATENAME(MM, convert(date, A.Date, 103)), 3) as Month 
  , cast(datepart(yyyy, convert(date, A.Date, 103)) as varchar(4)) as YEAR
  , A.Status as payment_status 
  , A.Type
  , C.ContactName
  , CASE 
  WHEN A.ItemCode is null 
  THEN ''''Unassigned''''   
  ELSE A.ItemCode  
  END Item   ,case	
  when LineAmountTypes = ''''Inclusive'''' 
  then - A.[LineAmount] + A.[TaxAmount] 
  else -A.[LineAmount] 
  end LineAmount_after_tax
  , -A.[LineAmount] as LineAmount
  , A.[Quantity] as LineQuantity
  , -A.[TaxAmount] as LineTaxAmount
  , A.[TaxType]
  , A.[UnitAmount] as UnitAmount
  , D.AddressLine1
  , D.City
  , D.Country  
  , D.Region  
  , D.PostalCode     
  , LineAmountTypes   
  , A.[TrackingCategory1] 
  , A.[TrackingCategory1_Option]  
  , A.[TrackingCategory2] 
  , A.[TrackingCategory2_Option] 
  , A.DueDate
  , A.CreditNoteNumber as invoiceNumber 
  , ''''Actual'''' Scenario  
  FROM[Xero_'+@companyname +'].[dbo].[CreditNotesDetails] A  
  Left JOIN [Xero_'+@companyname +'].[dbo].Contacts C  
  ON 
  A.ContactID = C.ContactID 
  and A.OrgName = C.OrgName 
  left JOIN 
  (select * from 
  (
  select Dense_Rank() over(partition by ContactID order by AddressID) rw, A.* from [Xero_'+@companyname +'].[dbo].[Contact_Address]  A
  ) adrs 
  where adrs.rw = 2 
  ) D 
  ON 
  A.ContactID = D.ContactID 
  and 
  A.OrgName = D.OrgName 
  left join [Xero_'+@companyname +'].[dbo].[ChartOfAccounts] E 
  On A.AccountCode = E.Code 
  and A.OrgName = E.OrgName 
  )'''
	execute(@SQLString);

	SET @SQLString='EXEC [Xero_'+@companyname +']..sp_executesql N''	

							create view [dbo].[VW_Journals]
							as


							select 
									A.[OrgName]
									,A.JID
									,A.Date
								    ,A.AccountCode as AccountCode
								   	,A.AccountName
									,B.Code+'''' ''''+B.Name AccountCodeName
									,B.Name+'''' ''''+ B.Code AccountNameCode
									,B.Description as AccountDescription
									,B.Class AccountClass
								   ,LEFT(datename(M,Convert(Date,[Date],101)),3) as Months
								   ,cast(datepart(YYYY,Convert(Date,[Date],101)) as varchar(40)) as Years
								   ,''''Actual'''' Datatype
								   ,TrackingCategory1_option
								   ,TrackingCategory2_option	   
								   ,case 
									When Class=''''REVENUE'''' OR Class=''''EXPENSE''''
									 THEN -NetAmt
									 else NetAmt 
								   end as Value
							from Journals A
									left join
										[dbo].[ChartOfAccounts] B
										ON
										A.AccountCode=B.Code
										  and
										 A.OrgShortCode=B.OrgShortCode
							where
							 ValidationStatus =''''OK''''

							'''
		execute(@SQLString);	
	SET @SQLString='EXEC [Xero_'+@companyname +']..sp_executesql N''	

					create view vw_PurchaseOrders as SELECT [PurchaseOrderNumber]
						  ,[DateString]
						  ,[Date]
						  ,[DeliveryDateString]
						  ,[DeliveryDate]
						  ,[DeliveryAddress]
						  ,[AttentionTo]
						  ,[Telephone]
						  ,[DeliveryInstructions]
						  ,[IsDiscounted]
						  ,[Reference]
						  ,[Type]
						  ,[CurrencyRate]
						  ,[CurrencyCode]
						  ,B.ContactName
						  ,[Status]
						  ,[LineAmountTypes]
						  ,[LineItemAccountCode]
						  ,[LineItemDescription]
						  ,[LineItemUnitAmount]
						  ,[LineItemTaxType]
						  ,[LineItemTaxAmount]
						  ,[LineItemLineAmount]
						  ,[LineItemTrackingName1]
						  ,[LineItemTrackingName2]
						  ,[LineItemTrackingOption1]
						  ,[LineItemTrackingOption2]
						  ,[LineItemQuantity]
						  ,[SubTotal]
						  ,[TotalTax]
						  ,[TotalDiscount]
						  ,[Total]
						  ,[UpdatedDateUTC]
						  ,[HasAttachments]
						  ,A.[OrgName]
					  FROM [PurchaseOrders] A
					  Left Outer Join [Contacts]B
					  On
					  A.ContactID=B.ContactID
					    and
          A.OrgShortCode=B.OrgShortCode;

								'''
	execute(@SQLString);	
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--select @SQLString;
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT @Database
END
