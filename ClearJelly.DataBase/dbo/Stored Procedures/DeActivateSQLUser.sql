﻿
CREATE PROCEDURE [dbo].[DeActivateSQLUser] 
	@emailId nvarchar(255)
AS

BEGIN TRY
	Declare @query NVARCHAR(Max)
	set @query = 'ALTER LOGIN ['+@emailId+'] DISABLE'
 exec sp_executesql @query
END TRY

BEGIN CATCH  
    
END CATCH;  