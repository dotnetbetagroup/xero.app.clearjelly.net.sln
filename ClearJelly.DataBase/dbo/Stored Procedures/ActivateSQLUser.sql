﻿
CREATE PROCEDURE [dbo].[ActivateSQLUser] 
	@emailId nvarchar(255)
AS

BEGIN TRY
	Declare @query NVARCHAR(Max)
	set @query = 'ALTER LOGIN ['+@emailId+'] Enable'
 exec sp_executesql @query
END TRY

BEGIN CATCH  
    
END CATCH;  