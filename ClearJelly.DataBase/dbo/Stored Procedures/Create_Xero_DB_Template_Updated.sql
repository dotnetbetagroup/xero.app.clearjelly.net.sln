﻿

-- ==============================================
-- Author:		Hesam Ziaei
-- Create date: 20/06/2015
-- Description:	This script creates a login
--				 and an empty ClearJelly database
--				 for a new customer 
--				 
-- ==============================================
CREATE PROCEDURE [dbo].[Create_Xero_DB_Template_Updated]
	-- Add the parameters for the stored procedure here
	@companyname varchar(200),
	@username varchar(200),
	@password varchar(200)
AS
BEGIN

	DECLARE @SQLString NVARCHAR(MAX)
	DECLARE @SQLDB NVARCHAR(MAX)
	SET @SQLDB = ''

	SET @SQLString = ''

/*CREATE USER [ETL] FOR LOGIN [ETL] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [ETL]
GO*/
/****** Object:  Table [dbo].[Accnts_PnL_hierarchy]    Script Date: 15/06/2015 6:35:12 PM ******/
	SET @SQLDB =@SQLDB +'create database [Xero_'+@companyname +']';
	execute(@SQLDB )

	--SET @SQLDB ='Use master;';
	--execute(@SQLDB )

	--SET @SQLDB ='CREATE LOGIN '+@username+'  WITH PASSWORD ='''+@password+'''';
	--execute(@SQLDB )	
	--SET @SQLDB ='CREATE USER '+@username+'  FOR LOGIN '+@username+' ';	
	--execute(@SQLDB );

	SET @SQLDB =+'use [Xero_'+@companyname +']';
	execute(@SQLDB )

	SET @SQLString=@SQLString+' 
					CREATE TABLE [Xero_'+@companyname +'].[dbo].[Accnts_PnL_hierarchy](
					[Row_Order] [int] NULL,
					[ReportID] [varchar](400) NULL,
					[ReportDate] datetime2 ,
					[UpdatedDateUTC] [varchar](400) NULL,
					[ReportName] [varchar](400) NULL,
					[ReportRowTitle] [varchar](800) NULL,
					[ReportType] [varchar](400) NULL,
					[Type_Level1] [varchar](400) NULL,
					[Type_Level2] [varchar](400) NULL,
					[Name] [varchar](800) NULL,
					[Value] [varchar](800) NULL,
					[ValidationErrors] [varchar](800) NULL,
					[ValidationStatus] [varchar](80) NULL,
					[Warnings] [varchar](800) NULL,
					[OrgShortCode] [varchar](800) Not Null,
					[OrgName] [varchar](800) Not Null
				) ON [PRIMARY];
			CREATE TABLE [Xero_'+@companyname +'].[dbo].[ChartOfAccounts](
					[AccountID] [varchar](200) NULL,
					[BankAccountNumber] [varchar](200) NULL,
					[Class] [varchar](200) NULL,
					[Code] [varchar](200) NULL,
					[CurrencyCode] [varchar](200) NULL,
					[Description] [varchar](max) NULL,
					[EnablePaymentsToAccount] [varchar](200) NULL,
					[Name] [varchar](200) NULL,
					[ReportingCode] [varchar](200) NULL,
					[ReportingCodeName] [varchar](200) NULL,
					[ShowInExpenseClaims] [varchar](200) NULL,
					[Status] [varchar](200) NULL,
					[SystemAccount] [varchar](200) NULL,
					[Type] [varchar](200) NULL,
					[OrgShortCode] [varchar](800) Not Null,
					[OrgName] [varchar](800) Not Null
				) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
			CREATE TABLE [Xero_'+@companyname +'].[dbo].[Contact_Address](
				[AddressID] [varchar](400) NULL,
				[ContactID] [varchar](800) NULL,
				[AddressLine1] [varchar](800) NULL,
				[AddressLine2] [varchar](800) NULL,
				[AddressLine3] [varchar](800) NULL,
				[AddressLine4] [varchar](800) NULL,
				[AttentionTo] [varchar](800) NULL,
				[Country] [varchar](800) NULL,
				[PostalCode] [varchar](50) NULL,
				[City] [varchar](800) NULL,
				[Region] [varchar](800) NULL,
				[OrgShortCode] [varchar](800) Not Null,
				[OrgName] [varchar](800) Not Null
			) ON [PRIMARY];				
				
			CREATE TABLE [Xero_'+@companyname +'].[dbo].[Contact_Group](
				[ContactGroupID] [varchar](800) NULL,
				[GroupID] [varchar](100) NULL,
				[ContactID] [varchar](800) NULL,
				[Name] [varchar](800) NULL,
				[Status] [varchar](800) NULL,
				[OrgShortCode] [varchar](800) Not Null,
				[OrgName] [varchar](800) Not Null
			) ON [PRIMARY];				
				
		CREATE TABLE [Xero_'+@companyname +'].[dbo].[Contacts](
			[ContactID] [varchar](800) NULL,
			[GroupCount] [varchar](800) NULL,
			[ContactName] [varchar](800) NULL,
			[AccountsPayable_Outstanding] [varchar](800) NULL,
			[AccountsPayable_Overdue] [varchar](800) NULL,
			[AccountsReceivable_Outstanding] [varchar](800) NULL,
			[AccountsReceivable_Overdue] [varchar](800) NULL,
			[ContactStatus] [varchar](800) NULL,
			[FirstName] [varchar](800) NULL,
			[LastName] [varchar](800) NULL,
			[IsCustomer] [varchar](800) NULL,
			[IsSupplier] [varchar](800) NULL,
			[PaymentTerms_Bills_Day] [varchar](800) NULL,
			[PaymentTerms_Bills_Type] [varchar](800) NULL,
			[PaymentTerms_Sales_Day] [varchar](800) NULL,
			[PaymentTerms_Sales_Type] [varchar](800) NULL,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY];
		CREATE TABLE [Xero_'+@companyname +'].[dbo].[CreditNotes](
			[Allocations] [varchar](600) NULL,
			[AppliedAmount] [varchar](600) NULL,
			[BrandingThemeID] [varchar](600) NULL,
			[ContactID] [varchar](600) NULL,
			[CreditNoteID] [varchar](600) NULL,
			[CreditNoteNumber] [varchar](600) NULL,
			[CurrencyCode] [varchar](600) NULL,
			[CurrencyRate] DECIMAL(22,4) NULL,
			[Date] datetime2,
			[DueDate] datetime2,
			[FullyPaidOnDate] datetime2,
			[LineAmountTypes] [varchar](600) NULL,
			[UnitAmount] DECIMAL(22,4) NULL,
			[Reference] [varchar](600) NULL,
			[RemainingCredit] DECIMAL(22,4) NULL,
			[SentToContact] [varchar](600) NULL,
			[Status] [varchar](600) NULL,
			[SubTotal] DECIMAL(22,4) NULL,
			[Total] DECIMAL(22,4) NULL,
			[TotalTax] DECIMAL(22,4) NULL,
			[Type] [varchar](600) NULL,
			[UpdatedDateUTC] datetime2,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY];

		CREATE TABLE [Xero_'+@companyname +'].[dbo].[CreditNotesDetails](
			[InvoiceID] [varchar](600) NULL,
			[AccountCode] [varchar](600) NULL,
			[Description] [varchar](600) NULL,
			[DiscountRate] DECIMAL(22,4) NULL,
			[ItemCode] [varchar](600) NULL,
			[LineAmount] DECIMAL(22,4) NULL,
			[Quantity] DECIMAL(22,4) NULL,
			[TaxAmount] DECIMAL(22,4) NULL,
			[TaxType] [varchar](600) NULL,
			[TrackingID] [varchar](600) NULL,
			[TrackingOption] [varchar](600) NULL,
			[Allocations] [varchar](600) NULL,
			[AppliedAmount] [varchar](600) NULL,
			[BrandingThemeID] [varchar](600) NULL,
			[ContactID] [varchar](600) NULL,
			[CreditNoteID] [varchar](600) NULL,
			[CreditNoteNumber] [varchar](600) NULL,
			[CurrencyCode] [varchar](600) NULL,
			[CurrencyRate] DECIMAL(22,4) NULL,
			[Date] datetime2,
			[DueDate] datetime2,
			[FullyPaidOnDate] datetime2,
			[LineAmountTypes] [varchar](600) NULL,
			[UnitAmount] DECIMAL(22,4) NULL,
			[Reference] [varchar](600) NULL,
			[RemainingCredit] DECIMAL(22,4) NULL,
			[SentToContact] [varchar](600) NULL,
			[Status] [varchar](600) NULL,
			[SubTotal] DECIMAL(22,4) NULL,
			[Total] DECIMAL(22,4) NULL,
			[TotalTax] DECIMAL(22,4) NULL,
			[Type] [varchar](600) NULL,
			[UpdatedDateUTC] datetime2,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY];				

		CREATE TABLE [Xero_'+@companyname +'].[dbo].[InvoiceDetails](
			[InvoiceID] [varchar](600) NULL,
			[InvoiceNumber] [varchar](400) NULL,
			[AccountCode] [varchar](600) NULL,
			[Description] [varchar](max) NULL,
			[DiscountRate] DECIMAL(22,4) NULL,
			[ItemCode] [varchar](600) NULL,
			[LineAmount] DECIMAL(22,4) NULL,
			[Quantity] DECIMAL(22,4) NULL,
			[ContactID] [varchar](800) NULL,
			[type] [varchar](400) NULL,
			[status] [varchar](400) NULL,
			[LineAmountTypes] varchar(400) NULL,
			[TaxAmount] DECIMAL(22,4) NULL,
			[TaxType] [varchar](600) NULL,
			[TrackingID] [varchar](600) NULL,
			[TrackingOption] [varchar](600) NULL,
			[UnitAmount] DECIMAL(22,4) NULL,
			[UpdatedDateUTC] datetime2,
            [Date] datetime2,
            [DueDate] datetime2,
 			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
		
		
		CREATE TABLE [Xero_'+@companyname +'].[dbo].[Invoices](
			[AmountCredited] DECIMAL(22,4) NULL,
			[AmountDue] decimal(22, 4),
			[AmountPaid] DECIMAL(22,4) NULL,
			[BrandingThemeID] [varchar](400) NULL,
			[Contact] [varchar](400) NULL,
			[CreditNotes] [varchar](400) NULL,
			[CurrencyCode] [varchar](400) NULL,
			[CurrencyRate] DECIMAL(22,4) NULL,
			[Date] datetime2,
			[DueDate] datetime2,
			[ExpectedPaymentDate] datetime2,
			[ExternalLinkProviderName] [varchar](400) NULL,
			[FullyPaidOnDate] datetime2,
			[HasAttachments] [varchar](400) NULL,
			[InvoiceID] [varchar](400) NULL,
			[InvoiceNumber] [varchar](400) NULL,
			[LineAmountTypes] [varchar](400) NULL,
			[LineItemsCount] [varchar](400) NULL,
			[AccountCode] [varchar](400) NULL,
			[Description] [varchar](max) NULL,
			[DiscountRate] DECIMAL(22,4) NULL,
			[ItemCode] [varchar](400) NULL,
			[LineAmount] DECIMAL(22,4) NULL,
			[Quantity] DECIMAL(22,4) NULL,
			[TaxAmount] DECIMAL(22,4) NULL,
			[TaxType] [varchar](400) NULL,
			[UnitAmount] DECIMAL(22,4) NULL,
			[Payments] [varchar](400) NULL,
			[PlannedPaymentDate] datetime2,
			[Reference] [varchar](400) NULL,
			[SentToContact] [varchar](400) NULL,
			[Status] [varchar](400) NULL,
			[SubTotal] DECIMAL(22,4) NULL,
			[Total] DECIMAL(22,4) NULL,
			[TotalDiscount] DECIMAL(22,4) NULL,
			[TotalTax] DECIMAL(22,4) NULL,
			[Type] [varchar](400) NULL,
			[UpdatedDateUTC] datetime2,
			[Url] [varchar](400) NULL,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];						

		
		CREATE TABLE [Xero_'+@companyname +'].[dbo].[Journals](
			[JID] [varchar](600) NULL,
			[Reference] [varchar](800) NULL,
			[JNumber] [varchar](100) NULL,
			[Date] datetime2,
			[CreatedDateUTC] datetime2,
			[AccountName] [varchar](800) NULL,
			[AccountCode] [varchar](800) NULL,
			[AccountType] [varchar](800) NULL,
			[NetAmt] DECIMAL(22,4) NULL,
			[GrossAmt] DECIMAL(22,4) NULL,
			[TrackingCategory1] [varchar](800) NULL,
			[TrackingCategory1_Option] [varchar](800) NULL,
			[TrackingCategory2] [varchar](800) NULL,
			[TrackingCategory2_Option] [varchar](800) NULL,
			[TaxName] [varchar](200) NULL,
			[TaxType] [varchar](200) NULL,
			[ValidationErrors] [varchar](200) NULL,
			[ValidationStatus] [varchar](200) NULL,
			[Warnings] [varchar](200) NULL,
			[Description] [varchar](max) NULL,
			[JournalLines] [varchar](200) NULL,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

		CREATE TABLE [Xero_'+@companyname +'].[dbo].[ManualJournals](
			[JournalID] [varchar](800) NULL,
			[Narration] [varchar](max) NULL,
			[Date] datetime2 NULL,
			[UpdatedDateUtc] datetime2  NULL,
			[AccountType] [varchar](400) NULL,
			[AccountCode] [varchar](400) NULL,
			[AccountName] [varchar](800) NULL,
			[AccountId] [varchar](400) NULL,
			[LineAmountTypes] [varchar](400) NULL,
			[Amount] DECIMAL(22,4) NULL,
			[NetAmount] DECIMAL(22,4) NULL,
			[GrossAmount] DECIMAL(22,4) NULL,
			[TaxAmount] DECIMAL(22,4) NULL,
			[TaxName] DECIMAL(22,4) NULL,
			[TaxType] [varchar](400) NULL,
			[TrackingCategory1] [varchar](800) NULL,
			[TrackingCategory1_option] [varchar](800) NULL,
			[TrackingCategory2] [varchar](800) NULL,
			[TrackingCategory2_Option] [varchar](800) NULL,
			[Status] [varchar](400) NULL,
			[Url] [varchar](400) NULL,
			[HasAttachments] [varchar](400) NULL,
			[OrgShortCode] [varchar](800) NULL,
			[OrgName] [varchar](800) NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]; 

		CREATE TABLE [Xero_'+@companyname +'].[dbo].[PnL](
			[Row_Order] [int] NULL,
			[ReportID] [varchar](400) NULL,
			[ReportDate] datetime2 NULL,
			[UpdatedDateUTC] [varchar](400) NULL,
			[ReportName] [varchar](400) NULL,
			[ReportRowTitle] [varchar](800) NULL,
			[ReportType] [varchar](400) NULL,
			[Type_Level1] [varchar](400) NULL,
			[Type_Level2] [varchar](400) NULL,
			[Name] [varchar](800) NULL,
			[Value] [varchar](800) NULL,
			[ValidationErrors] [varchar](800) NULL,
			[ValidationStatus] [varchar](80) NULL,
			[Warnings] [varchar](800) NULL,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY];
		
		CREATE TABLE [Xero_'+@companyname +'].[dbo].[TrackingCategories](
			[TrackingCategoryID] [varchar](max) NULL,
			[TrackingOptionID] [varchar](max) NULL,
			[Category] [varchar](600) NULL,
			[Option] [varchar](600) NULL,
			[OrgShortCode] [varchar](800) Not Null,
			[OrgName] [varchar](800) Not Null
		) ON [PRIMARY] ;

		CREATE TABLE [Xero_'+@companyname +'].[dbo].[PurchaseOrders](
			[PurchaseOrderID] [varchar](800) NULL,
			[PurchaseOrderNumber] [varchar](800) NULL,
			[DateString] [varchar](50) NULL,
			[Date] [datetime2](7) NULL,
			[DeliveryDateString] [varchar](50) NULL,
			[DeliveryDate] [datetime2](7) NULL,
			[DeliveryAddress] [varchar](800) NULL,
			[AttentionTo] [varchar](800) NULL,
			[Telephone] [varchar](50) NULL,
			[DeliveryInstructions] [varchar](800) NULL,
			[IsDiscounted] [varchar](100) NULL,
			[Reference] [varchar](800) NULL,
			[Type] [varchar](800) NULL,
			[CurrencyRate] [money] NULL,
			[CurrencyCode] [varchar](800) NULL,
			[ContactID] [varchar](800) NULL,
			[BrandingThemeID] [varchar](800) NULL,
			[Status] [varchar](200) NULL,
			[LineAmountTypes] [varchar](800) NULL,
			[LineItemAccountCode] [varchar](800) NULL,
			[LineItemID] [varchar](800) NULL,
			[LineItemDescription] [varchar](800) NULL,
			[LineItemUnitAmount] [money] NULL,
			[LineItemTaxType] [varchar](800) NULL,
			[LineItemTaxAmount] [money] NULL,
			[LineItemLineAmount] [money] NULL,
			[LineItemTrackingName1] [varchar](800) NULL,
			[LineItemTrackingName2] [varchar](800) NULL,
			[LineItemTrackingOption1] [varchar](800) NULL,
			[LineItemTrackingOption2] [varchar](800) NULL,
			[LineItemQuantity] [money] NULL,
			[SubTotal] [money] NULL,
			[TotalTax] [money] NULL,
			[TotalDiscount] [money] NULL,
			[Total] [money] NULL,
			[UpdatedDateUTC] [datetime2](7) NULL,
			[HasAttachments] [varchar](800) NULL,
			[OrgShortCode] [varchar](800) NULL,
			[OrgName] [varchar](800) NULL
		) ON [PRIMARY]
														
			 ';
	execute(@SQLString);	


	EXEC	[ClearJelly].dbo.[CreateViews] @companyname;
	
	-- Copying the clear jelly standard calendar to user's database
	Set @SQLString='Select * into [Xero_'+@companyname +'].dbo.Calendar from ClearJelly.dbo.calendar';
	execute(@SQLString);

	SET @SQLDB ='USE [master] ';
	execute(@SQLDB);
	SET @SQLDB =  ' USE [master];CREATE LOGIN ['+@username+'] WITH PASSWORD=N'''+@password+''', CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;';

	SET @SQLDB = @SQLDB + ' DENY VIEW ANY DATABASE TO ['+@username+'];' ; 
	execute(@SQLDB);

	--SET @SQLDB = ' USE [Xero_'+@username +']' ;
	--execute(@SQLDB);

	SET @SQLDB = ' USE master';
	execute(@SQLDB);

	SET @SQLDB = ' ALTER AUTHORIZATION ON DATABASE::[Xero_'+@companyname +'] TO ['+@username +']';
	execute(@SQLDB);

END



