﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[change_all_exisitng_databases_tables]
@table_definition varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @MyCursor CURSOR;
	DECLARE @MyField varchar(max);
	DECLARE @sql nvarchar(MAX);
	    SET @MyCursor = CURSOR FOR
		SELECT
			--@sql = ISNULL(@sql, N'') +
				--N'ALTER TABLE ' + QUOTENAME(d.name) + N'.[dbo].[MyTable] ADD C1 int;'
				--@view_definition 
				QUOTENAME(d.name) 
			FROM sys.databases d
			WHERE
				(d.database_id > 4) AND /* No system databases */
				(d.state = 0) AND /* Online only */
				(d.is_distributor = 0) AND /* Not a distribution database */
				(d.is_read_only = 0) AND 
				d.name like 'Xero_%';   

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @MyField

    WHILE @@FETCH_STATUS = 0
    BEGIN
	  --select @MyField ;
	  	BEGIN TRANSACTION;

		 
		set @sql='EXEC '+@MyField+'..sp_executesql N'''+@table_definition+'''';--EXEC @MyField..sp_executesql N''

		EXEC(@sql);
		--select(@sql);
		  --select @view_definition;
		COMMIT TRANSACTION;
      FETCH NEXT FROM @MyCursor 
      INTO @MyField 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;


	
END
