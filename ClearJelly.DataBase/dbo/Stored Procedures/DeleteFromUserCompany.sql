﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteFromUserCompany]
	
	@companyName nvarchar(255),
	@orgShortCode nvarchar(255),
	@orgName nvarchar(255)
AS

BEGIN Try
Begin Tran
BEGIN 
Declare @databasename nvarchar(100)
Declare @sqlstatement nvarchar(Max)
Declare @tablename varchar(100)
Declare @deleteQuery nvarchar(Max)

set @databasename = 'Xero_' + @companyName

set @sqlstatement = 'Declare  checktable cursor for select name from [' + @databasename + '].sys.Tables Where Object_Id IN (select Object_Id FROM [' + @databasename + '].sys.columns where name like ''orgname'' )'
			exec sp_executesql @sqlstatement

			OPEN checktable;

			FETCH NEXT FROM checktable INTO @tablename;

			WHILE @@FETCH_STATUS = 0
			BEGIN
			
			set @deleteQuery = 'Delete from ['+ @databasename + '].dbo.' + @tablename + ' where OrgShortCode ='''+ @orgShortCode + ''' and OrgName  like ''' +'%'+ @orgName +'%'+ ''''
			Exec (@deleteQuery)

			FETCH NEXT FROM checktable INTO @tablename;
			END;

			CLOSE checktable;
			DEALLOCATE checktable;
		select 1 as checksuccess
COMMIT TRAN
END
END Try
BEGIN Catch
select 0 as checksuccess
ROLLBACK

END Catch