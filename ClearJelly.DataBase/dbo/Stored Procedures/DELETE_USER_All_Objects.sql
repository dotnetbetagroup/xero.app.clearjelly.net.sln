﻿-- =============================================
-- Author:		Hesam Ziaei
-- Create date: 23/8/2015
-- Description:	Deletes a user an its 
--				corresponding company detials,
--				its database, and its sql user
-- =============================================
--exec [DELETE_USER_All_Objects] 'test@temp.com'
CREATE PROCEDURE [dbo].[DELETE_USER_All_Objects] 
	@email_address varchar(800)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	Declare @CompanyID as varchar(400);
	Declare @CompanyName as varchar(400);
	Declare @sql as varchar(800);
	Declare @UserID as varchar(400);
	select @CompanyID=[CompanyId],@UserID = [UserId] from [User] where [Email]=@email_address;
	select @CompanyName= Name from [Company] where CompanyId=@CompanyID;
	print('userId: '+@UserID)
	Delete from [DeletedOrganizations] where UserId=@UserID
	Delete from [OrganisationDetails] where CompanyId=@CompanyID
	Delete from [AdditionalUsers] where CompanySubscriptionId in 
	(select CompanySubscriptionId from CompanySubscription where CompanyId=@CompanyID);
	Delete from [RecurringPayments] where CompanySubscriptionId in 
	(select CompanySubscriptionId from CompanySubscription where CompanyId=@CompanyID);
	Delete from [PaypalSyncDataHistory] where CompanySubscriptionId in 
	(select CompanySubscriptionId from CompanySubscription where CompanyId=@CompanyID);
		Delete from [PaypalSyncData] where CompanySubscriptionId in 
	(select CompanySubscriptionId from CompanySubscription where CompanyId=@CompanyID);
	Delete from [SuspendedUser]  where Userid=@UserID;
	Delete from [ModelProcess] where UserId=@UserID
	Delete From [Xero_User_Org] where CompanyId=@CompanyID;
	Delete From [CompanySubscription] where CompanyId= @CompanyID;
	Delete from [UserSafeIP] where UserId=@UserID
	Delete from [SuspendedUser] where UserId=@UserID
	  Delete from [InstantPayments] where AdditionalUserId in 
	(select distinct AdditionalUserId from  AdditionalUsers where
	 CompanySubscriptionId in (select CompanySubscriptionId from CompanySubscription where CompanyId=@CompanyID));
	Delete From [User] where [CompanyId] =@CompanyID;
		Delete From [Company] where CompanyId=@CompanyID;

	
	-- 	if exists(select 1 from DeletedOrganizations where UserId = @UserID)
	--Begin
	-- update DeletedOrganizations set UserId = null where UserId=@UserID
	--End
	 	


	--delete database
	execute [dbo].[Delete_Xero_DB_For_User] @email_address;
	--delete sql user
	execute [dbo].[Delete_Sqluser_For_User] @email_address;
	execute [Delete_UserDB_SQLUser] @email_address;
	print 'query executed successfully.'
END	





--exec [DELETE_USER_All_Objects] 'gdfsg@gmail.com'  
