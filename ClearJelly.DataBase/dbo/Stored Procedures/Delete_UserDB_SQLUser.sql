﻿-- =============================================
-- Author:		Nikin
-- Create date: 02-05-2016
-- Description:	Terminate(inactive) the user,
--				terminate the additional user,
--				delete associate database, delete sql user.
-- =============================================
CREATE PROCEDURE [dbo].[Delete_UserDB_SQLUser] 
	@email_address varchar(800)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	Declare @sql as varchar(800);
	Declare @CompanyID as varchar(400);
	--delete user database
	execute [dbo].[Delete_Xero_DB_For_User] @email_address;
	--delete sql user
	execute [dbo].[Delete_Sqluser_For_User] @email_address;
END
