﻿-- =============================================
-- Author:		Nikin
-- Create date: 02-05-2016
-- Description:	Delete sql user.
-- =============================================
CREATE PROCEDURE [dbo].[[Delete_Sqluser_For_User]]] 
	@email_address varchar(800)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	Declare @sql as varchar(800);
	SET @sql=' IF  EXISTS (SELECT * FROM sys.database_principals 
				WHERE name = N'''+@email_address+''')
				
	            DROP USER ['+@email_address+'] ';
	Execute(@sql);
	

	SET @sql=' IF  EXISTS (SELECT * FROM sys.syslogins  
				WHERE name = N'''+@email_address+''')
				
	            DROP Login ['+@email_address+'] ';
	Execute(@sql);	

	print 'query' + @sql
END
