﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_View_to_exisitng_databases]
@view_definition varchar(max),
@view_name varchar(800)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @MyCursor CURSOR;
	DECLARE @MyField varchar(max);
	DECLARE @sql nvarchar(MAX);
	    SET @MyCursor = CURSOR FOR
		SELECT
			--@sql = ISNULL(@sql, N'') +
				--N'ALTER TABLE ' + QUOTENAME(d.name) + N'.[dbo].[MyTable] ADD C1 int;'
				--@view_definition 
				QUOTENAME(d.name) 
			FROM sys.databases d
			WHERE
				(d.database_id > 4) AND /* No system databases */
				(d.state = 0) AND /* Online only */
				(d.is_distributor = 0) AND /* Not a distribution database */
				(d.is_read_only = 0) AND 
				d.name like 'Xero_%';   

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @MyField

    WHILE @@FETCH_STATUS = 0
    BEGIN
	  --select @MyField ;
	  	BEGIN TRANSACTION;
		 SET @sql  =' use '+@MyField+'; ' ;
   		 SET @sql=@sql + ' IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS 
													WHERE TABLE_NAME = '''+@view_name+''')
											DROP VIEW '+@view_name+';';
		--select @sql;
		execute (@sql);
		 
		set @sql='EXEC '+@MyField+'..sp_executesql N'''+@view_definition+'''';--EXEC @MyField..sp_executesql N''

		EXEC(@sql);
		  --select @view_definition;
		COMMIT TRANSACTION;
      FETCH NEXT FROM @MyCursor 
      INTO @MyField 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;


	
END
