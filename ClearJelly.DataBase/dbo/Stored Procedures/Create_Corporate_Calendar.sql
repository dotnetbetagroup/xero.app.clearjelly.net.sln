﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Create_Corporate_Calendar]
@database varchar(800)
AS
BEGIN

DECLARE @FirstDate DATE
DECLARE @LastDate DATE
DECLARE @FiscalStart INT
SET @FirstDate = '1 Jan 2008' -- Enter first date of calendar as 'dd mmm yyyy'
SET @LastDate = '31 Dec 2030' -- Enter last date of calendar as 'dd mmm yyyy'
SET @FiscalStart = 7 -- First calendar period of Fical Year - 1=Jan, 2=Feb...., 12=Dec

IF @FirstDate IS NULL OR @FirstDate = ''
	BEGIN
		SET @FirstDate = '1 Jan 2000'
	END
	
IF @LastDate IS NULL OR @LastDate = '' 
	OR @LastDate < @FirstDate 
	OR DATEDIFF(DAY, @FirstDate, @LastDate) > 10000
	BEGIN
		SET @LastDate = DATEADD(DAY, 10000, @FirstDate)
	END

	;WITH CTE_DatesTable
	AS
	(
	  SELECT @FirstDate AS [Date]
	 UNION ALL
	 SELECT DATEADD(DAY, 1, [Date])
	 FROM CTE_DatesTable
	 WHERE DATEADD(DAY, 1, [Date]) <= @LastDate
	)

	SELECT 
		Date, 
		DATEPART(DAYOFYEAR, Date) 		AS DayOfYear, 
		DATEPART(DAY, Date) 			AS Day,
		DATEPART(MONTH, Date) 			AS Month,
		DATEPART(YEAR, Date) 			AS Year,
		DATENAME(WEEKDAY, Date) 		AS DayOfWeek,
		LEFT(DATENAME(WEEKDAY, Date),3) 	AS DayOfWeekAbr,
		DATEPART(WEEK, Date) 			AS WeekOfYear,
		DATEPART(QUARTER, Date) 		AS Quarter,
		DATENAME(MONTH, Date) 			AS MonthName,
		LEFT(DATENAME(MM, Date),3) 		AS MonthNameAbr,
		DATEADD(DAY, -1, DATEADD(MONTH, 1, DATEADD(DAY, (DATEPART(DAY, 
DATE) * -1) +1, Date))) 		AS MonthEndDate,
		DATEADD(DAY, -1, DATEADD(MONTH, 1 + 12 - (DATEPART(MONTH, DATE) 
- @FiscalStart + 1 + CASE WHEN @FiscalStart > DATEPART(MONTH, DATE) 
THEN 12 ELSE 0 END), DATEADD(DAY, (DATEPART(DAY, DATE) * -1) +1, 
Date))) 				AS YearEndDate,
		DATEPART(MONTH, DATE) - @FiscalStart + 1 + 
			CASE WHEN @FiscalStart > DATEPART(MONTH, DATE) THEN 12 
ELSE 0 END 			AS FiscalPeriod,
		YEAR(DATEADD(DAY, -1, DATEADD(MONTH, 1 + 12 - (DATEPART(MONTH, 
DATE) - @FiscalStart + 1 + CASE WHEN @FiscalStart > 
DATEPART(MONTH, DATE) THEN 12 ELSE 0 END), DATEADD(DAY, 
(DATEPART(DAY, DATE) * -1) +1, Date)))) 
AS FiscalYearEnd,
		CASE DATEPART(MONTH, DATE) - @FiscalStart + 1 + CASE WHEN 
@FiscalStart > DATEPART(MONTH, DATE) THEN 12 ELSE 0 END
				WHEN 1 THEN 1  
WHEN 2 THEN 1  
WHEN 3 THEN 1
				WHEN 4 THEN 2  
WHEN 5 THEN 2  
WHEN 6 THEN 2
				WHEN 7 THEN 3  
WHEN 8 THEN 3  
WHEN 9 THEN 3
				WHEN 10 THEN 4  
WHEN 11 THEN 4  
WHEN 12 THEN 4
				END 			AS FiscalQuarter
	Into Calendar
	FROM 
	CTE_DatesTable

	OPTION (MAXRECURSION 10000)



	


END
