﻿-- =============================================
-- Author:  	<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Create_ABC_Financial_DB_Template]
-- Add the parameters for the stored procedure here
@companyname varchar(200),
@username varchar(200),
@password varchar(200)
AS
BEGIN
  DECLARE @SQLString nvarchar(max)
  DECLARE @SQLDB nvarchar(max)
  DECLARE @AlterString nvarchar(max)
  DECLARE @MemberPersonalString nvarchar(max)
  DECLARE @AgreementString nvarchar(max)
  DECLARE @AlerString nvarchar(max)
  DECLARE @CheckInCountString nvarchar(max)
  DECLARE @ChildNotesString nvarchar(max)
  DECLARE @CCNamesString nvarchar(max)
  DECLARE @POSTransactionsString nvarchar(max)
  DECLARE @WebPlanListsString nvarchar(max)
  DECLARE @MembersCheckinsSummariesString nvarchar(max)
  DECLARE @AbcBillingIdName nvarchar(max)

  SET @SQLDB = ''
  SET @AlerString = ''
  SET @MemberPersonalString = ''
  SET @AgreementString = ''
  SET @CheckInCountString = ''
  SET @ChildNotesString = ''
  SET @CCNamesString = ''
  SET @POSTransactionsString = ''
  SET @WebPlanListsString = ''
  SET @SQLString = ''
  SET @MembersCheckinsSummariesString = ''
  SET @AbcBillingIdName = ''

  /*CREATE USER [ETL] FOR LOGIN [ETL] WITH DEFAULT_SCHEMA=[dbo]
  GO
  ALTER ROLE [db_owner] ADD MEMBER [ETL]
  GO*/
  /****** Object:  Table [dbo].[Accnts_PnL_hierarchy]    Script Date: 15/06/2015 6:35:12 PM ******/
  SET @SQLDB = @SQLDB + 'create database [ABC_Financial_' + @companyname + ']';
  EXECUTE (@SQLDB)

  --SET @SQLDB ='Use master;';
  --execute(@SQLDB )

  --SET @SQLDB ='CREATE LOGIN '+@username+'  WITH PASSWORD ='''+@password+'''';
  --execute(@SQLDB )	
  --SET @SQLDB ='CREATE USER '+@username+'  FOR LOGIN '+@username+' ';	
  --execute(@SQLDB );

  SET @SQLDB = +'use [ABC_Financial_' + @companyname + ']';
  EXECUTE (@SQLDB)

  SET @MemberPersonalString = @MemberPersonalString + ' 
					CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Member](
					[MemberId ] [varchar](400) NULL,
					[PersonalId] [varchar](400) NULL,
					[AgreementId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Personal](
					[PersonalId] [varchar](400) NOT NULL,
					[FirstName] [varchar](400) NULL,
					[LastName] [varchar](400) NULL,
					[MiddleInitial] [varchar](400) NULL,
					[AddressLine1] [varchar](400) NULL,
					[AddressLine2] [varchar](400) NULL,
					[City] [varchar](400) NULL,
					[State] [varchar](400) NULL,
					[PostalCode] [varchar](400) NULL,
					[HomeClub] [varchar](400) NULL,
					[CountryCode] [varchar](400) NULL,
					[Email] [varchar](400) NULL,
					[PrimaryPhone] [varchar](400) NULL,
					[MobilePhone] [varchar](400) NULL,
					[WorkPhone] [varchar](400) NULL,
					[WorkPhoneExt] [varchar](400) NULL,
					[EmergencyContactName] [varchar](400) NULL,
					[EmergencyPhone] [varchar](400) NULL,
					[EmergencyExt] [varchar](400) NULL,
					[EmergencyAvailability] [varchar](400) NULL,
					[Barcode] [varchar](400) NULL,
					[BirthDate] datetime2 NULL,
					[Gender] [int] NULL,
					[Employer] [varchar](400) NULL,
					[Occupation] [varchar](400) NULL,
					[Group] [varchar](400) NULL,
					[IsActive] [bit] NULL,
					[MemberStatus] [varchar](400) NULL,
					[JoinStatus] [int] NULL,
					[IsConvertedProspect] [bit] NULL,
					[HasPhoto] [bit] NULL,
					[ConvertedDate] datetime2 NULL,
					[MemberStatusReason] [varchar](400) NULL,
					[WellnessProgramId] [varchar](400) NULL,
					[FirstCheckInTimestamp] datetime2 NULL,
					[MemberStatusDate] datetime2 NULL,
					[LastCheckInTimestamp] datetime2 NULL,
					[TotalCheckInCount] [int] NULL,
					[CreateTimestamp] datetime2 NULL,
					[LastModifiedTimestamp] datetime2 NULL,
					[MemberId] [varchar](400) NULL
				) ON [PRIMARY]; ';

  EXECUTE (@MemberPersonalString);

  SET @AgreementString = @AgreementString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Agreement](
					[AgreementId] [varchar](400) NOT NULL,
					[MemberId ] [varchar](400) NULL,
					[AgreementNumber] [varchar](400) NULL,
					[IsPrimaryMember] [bit] NULL,
					[IsNonMember] [bit] NULL,
					[Ordinal] [int] NULL,
					[ReferringMemberId] [varchar](400) NULL,
					[ReferringMemberHomeClub] [varchar](400) NULL,
					[ReferringMemberName] [varchar](400) NULL,
					[SalesPersonId] [varchar](400) NULL,
					[SalesPersonName] [varchar](400) NULL,
					[SalesPersonHomeClub] [varchar](400) NULL,
					[PaymentPlan] [varchar](400) NULL,
					[Term] [varchar](400) NULL,
					[PaymentFrequency] [varchar](400) NULL,
					[MembershipType] [varchar](400) NULL,
					[ManagedType] [varchar](400) NULL,
					[CampaignId] [varchar](400) NULL,
					[CampaignName] [varchar](400) NULL,
					[CampaignGroup] [varchar](400) NULL,
					[IsPastDue] [bit] NULL,
					[DownPaymentPendingPOS] [varchar](400) NULL,
					[RenewalType] [varchar](400) NULL,
					[AgreementPaymentMethod] [varchar](400) NULL,
					[DownPayment] [decimal](22, 4) NULL,
					[NextDueAmount] [decimal](22, 4) NULL,
					[ProjectedDueAmount] [decimal](22, 4) NULL,
					[PastDueBalance] [decimal](22, 4) NULL,
					[LateFeeAmount] [decimal](22, 4) NULL,
					[ServiceFeeAmount] [decimal](22, 4) NULL,
					[TotalPastDueBalance] [decimal](22, 4) NULL,
					[ClubAccountPastDueBalance] [decimal](22, 4) NULL,
					[CurrentQueue] [varchar](400) NULL,
					[QueueTimestamp] datetime2 NULL,
					[StationLocation] [varchar](400) NULL,
					[AgreementEntrySource] [varchar](400) NULL,
					[AgreementEntrySourceReportName] [varchar](400) NULL,
					[SinceDate] datetime2 NULL,
					[BeginDate] datetime2 NULL,
					[ExpirationDate] datetime2 NULL,
					[ConvertedDate] datetime2 NULL,
					[LastRenewalDate] datetime2 NULL,
					[LastRewriteDate] datetime2 NULL,
					[RenewalDate] datetime2 NULL,
					[FirstPaymentDate] datetime2 NULL,
					[SignDate] datetime2 NULL,
					[NextBillingDate] datetime2 NULL,
					[PrimaryBillingAccountHolderId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[PrimaryBillingAccountHolder](
					[PrimaryBillingAccountHolderId] [varchar](400) NOT NULL,
					[FirstName] [varchar](400) NULL,
					[LastName] [varchar](400) NULL
				) ON [PRIMARY]; ';

  EXECUTE (@AgreementString);


  SET @AlerString = @AlerString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[AlertInMember](
					[AlertInMemberId] [varchar](400) NOT NULL,
					[AlertId] [varchar](400) NULL,
					[MemberId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Alert](
					[AlertId] [varchar](400) NOT NULL,
					[Message] [varchar](400) NULL,
					[AbcCode] [varchar](400) NULL,
					[Priority] [varchar](400) NULL,
					[AllowDoorAccess] [varchar](400) NULL,
					[EvaluationDate] [varchar](400) NULL,
					[GracePeriod] [varchar](400) NULL,
					[EvaluationAmount] [decimal](22, 4) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberCheckinDetail](
					[MemberCheckinDetailId] [varchar](400) NOT NULL,
					[MemberId] [varchar](400) NULL,
					[CheckinId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[CheckinDetail](
					[CheckinDetailId] [varchar](400) NOT NULL,
					[CheckInId] [varchar](400) NULL,
					[ClubNumber] [varchar](400) NULL,
					[CheckInTimeStamp] datetime2 NULL
				) ON [PRIMARY];
				';
  EXECUTE (@AlerString);

  SET @MembersCheckinsSummariesString = @MembersCheckinsSummariesString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Payments](
					[PaymentId] [varchar](400) NOT NULL,
					[PaymentType] [varchar](400) NULL,
					[PaymentAmount] [varchar](400) NULL,
					[PaymentTax] [varchar](400) NULL
				) ON [PRIMARY];
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberCheckinSummaries](
					[MemberCheckinSummariesId] [varchar](400) NOT NULL,
					[MemberId] [varchar](400) NULL,
				) ON [PRIMARY];
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Links](
					[LinkId] [varchar](400) NOT NULL,
					[Rel] [varchar](400) NULL,
					[Url] [varchar](400) NULL
				) ON [PRIMARY];
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[LinksInMemberCheckinSummaries](
					[LinksInMemberCheckinSummariesId] [varchar](400) NOT NULL,
					[LinkId] [varchar](400) NULL,
					[MemberCheckinSummariesId] [varchar](400) NULL
				) ON [PRIMARY];
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberInCheckin](
					[MemberInCheckinId] [varchar](400) NOT NULL,
					[CheckInCountId] [varchar](400) NULL,
					[MemberCheckinSummariesId] [varchar](400) NULL
				) ON [PRIMARY];
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[PaymentsInItems](
					[Id] [varchar](400) NOT NULL,
					[PaymentId] [varchar](400) NULL,
					[ItemId] [varchar](400) NULL
				) ON [PRIMARY];  ';

  EXECUTE (@MembersCheckinsSummariesString);

  SET @CheckInCountString = @CheckInCountString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[CheckInCount](
					[CheckInCountId] [varchar](400) NOT NULL,
					[Club] [varchar](400) NULL,
					[Count] [int] NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberChild](
					[MemberChildId] [varchar](400) NULL,
					[MemberChildEmergencyContactId] [varchar](400) NULL,
					[ActiveStatus] [varchar](400) NULL,
					[HomeClub] [varchar](400) NULL,
					[FirstName] [varchar](400) NULL,
					[MiddleInitial] [varchar](400) NULL,
					[LastName] [varchar](400) NULL,
					[Barcode] [varchar](400) NULL,
					[Gender] [varchar](400) NULL,
					[BirthDate] [varchar](400) NULL,
					[PrimaryMemberId] [varchar](400) NULL,
					[AgreementNumber] [varchar](400) NULL,
					[ChildMisc1] [varchar](400) NULL,
					[ChildMisc2] [varchar](400) NULL,
					[ChildMisc3] [varchar](400) NULL,
					[CreateTimestamp] datetime2 NULL,
					[LastModifiedTimestamp] datetime2 NULL,
					[LastCheckInTimestamp] datetime2 NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberChildEmergencyContact](
					[MemberChildEmergencyContactId] [varchar](400) NOT NULL,
					[EmergencyContactFirstName] [varchar](400) NULL,
					[EmergencyContactMiddleInitial] [varchar](400) NULL,
					[EmergencyContactLastName] [varchar](400) NULL,
					[EmergencyContactPhone] [varchar](400) NULL,
					[EmergencyContactExtension] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberChildNote](
					[MemberChildNoteId] [varchar](400) NOT NULL,
					[NoteText] [varchar](400) NULL,
					[NoteCreateTimestamp] datetime2 NULL,
					[EmployeeId] [varchar](400) NULL
				) ON [PRIMARY]; ';

  EXECUTE (@CheckInCountString);

  SET @ChildNotesString = @ChildNotesString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ChildNotes](
					[ChildNotesId] [varchar](400) NOT NULL,
					[MemberChildNoteId] [varchar](400) NULL,
					[MemberChildId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Group](
					[Id] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[Status] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[MemberPersonal](
					[MemberPersonalId] [varchar](400) NOT NULL,
					[MemberId] [varchar](400) NULL,
					[PersonalId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ClubInformation](
					[ClubInformationId] [varchar](400) NOT NULL,
					[Name] [varchar](400) NULL,
					[ShortName] [varchar](400) NULL,
					[TimeZone] [varchar](400) NULL,
					[Address1] [varchar](400) NULL,
					[Address2] [varchar](400) NULL,
					[City] [varchar](400) NULL,
					[State] [varchar](400) NULL,
					[PostalCode] [varchar](400) NULL,
					[Country] [varchar](400) NULL,
					[Email] [varchar](400) NULL,
					[DonationItem] [varchar](400) NULL,
					[OnlineSignupAllowedPaymentMethods] [int] NULL,
					[OnlineId] [varchar](400) NULL,
					[BillingCountry] [varchar](400) NULL					
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Online](
					[OnlineId] [varchar](400) NOT NULL,
					[MinorsId] [varchar](400) NULL,
					[CCNamesId] [varchar](400) NULL,
					[ShowFees] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Minors](
					[MinorsId] [varchar](400) NOT NULL,
					[AllowMinors] [bit] NULL,
					[MinorAge] [varchar](400) NULL,
					[MinorDisclaimer] [varchar](400) NULL
				) ON [PRIMARY]; ';

  EXECUTE (@ChildNotesString);

  SET @CCNamesString = @CCNamesString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[CCNames](
					[CCNamesId] [varchar](400) NOT NULL,
					[RequireCcNameMatch] [bit] NULL,
					[DifferentCcNamesDisclaimer] [varchar](400) NULL,					
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Countries](
					[CountryId] [varchar](400) NOT NULL,
					[Name] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[CountriesInClub](
					[CountriesInClub] [varchar](400) NOT NULL,
					[ClubInformationId] [varchar](400) NULL,
					[CountryId] [varchar](400) NULL
				) ON [PRIMARY];	

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[CreditCardPaymentMethods](
					[PaymentMethodId] [varchar](400) NOT NULL,
					[Name] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ClubPaymentMethods](
					[Id] [varchar](400) NULL,
					[PaymentMethodId] [varchar](400) NULL,
					[ClubInformationId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Checkins](
					[CheckInId] [varchar](400) NULL,
					[CheckInTimestamp] datetime2 NULL,
					[CheckInMessage] [varchar](400) NULL,
					[StationName] [varchar](400) NULL,
					[CheckInStatus] [varchar](400) NULL,
					[MemberId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Members](
					[MemberId] [varchar](400) NULL,
					[HomeClub] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Identities](
					[IdentityId] [varchar](400) NULL,
					[MembershipTypeAbcCode] [varchar](400) NULL,
					[HomeVpdId] [int] NULL,
					[HomeClub] [varchar](400) NULL,
					[HomeCompanyName] [varchar](400) NULL,
					[CorpId] [varchar](400) NULL,
					[MemberId] [varchar](400) NULL,
					[Barcode] [varchar](400) NULL,
					[FirstName] [varchar](400) NULL,
					[LastName] [varchar](400) NULL,
					[IsActive] [bit] NULL
				) ON [PRIMARY]; 
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[POSTransactions](
					[TransactionId] [varchar](400) NULL,
					[TransactionTimestamp] datetime2 NULL,
					[MemberId] [varchar](400) NULL,
					[HomeClub] [varchar](400) NULL,
					[EmployeeId] [varchar](400) NULL,
					[ReceiptNumber] [varchar](400) NULL,
					[StationName] [varchar](400) NULL,
					[Return] [bit] NULL
				) ON [PRIMARY];
				';

  EXECUTE (@CCNamesString);

  SET @WebPlanListsString = @WebPlanListsString + '
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Items](
					[ItemId] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[InventoryType] [int] NULL,
					[Sale] [bit] NULL,
					[Upc] [varchar](400) NULL,
					[ProfitCenter] [varchar](400) NULL,
					[Catalog] [varchar](400) NULL,
					[UnitPrice] [varchar](400) NULL,
					[Quantity] [varchar](400) NULL,
					[PackageQuantity] [varchar](400) NULL,
					[Subtotal] [varchar](400) NULL,
					[Tax] [varchar](400) NULL,
					[RecurringServiceId] [varchar](400) NULL,
					[TransactionId] [varchar](400) NULL
				) ON [PRIMARY];
				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ItemsInPOSTransactions](
					[Id] [varchar](400) NOT NULL,
					[ItemId] [varchar](400) NULL,
					[TransactionId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[WebPlanLists](
					[PlanId] [varchar](400) NULL,
					[PlanName] [varchar](400) NULL,
					[PromoCode] [varchar](400) NULL,
					[PromoName] [varchar](400) NULL,
					[AgreementDescription] [nvarchar](MAX) NULL,
					[LimitedAvailability] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[AnnualFees](
					[Id] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[Amount] [decimal](22, 4) NULL,
					[PreTaxAmount] [decimal](22, 4) NULL,
					[ProfitCenterAbcCode] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ClubNumbersInAnnualFees](
					[Id] [varchar](400) NULL,
					[ClubNumber] [varchar](400) NULL,
					[AnnualFeeId] [varchar](400) NULL
				) ON [PRIMARY];
				';
  EXECUTE (@WebPlanListsString);

  SET @AbcBillingIdName = @AbcBillingIdName + '

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[BillingCatalogItem](
					[Id] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[ProfitCenterAbcCode] [varchar](400) NULL,
					[AnnualFee] [bit] NULL,
					[TotalTaxRate] [decimal](22, 4) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ClubNumbersInBillingCatalogItem](
					[Id] [varchar](400) NULL,
					[ClubNumber] [varchar](400) NULL,
					[BillingCatalogItemId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[TaxResponse](					
					[RateId] [varchar](400) NOT NULL PRIMARY KEY,
					[Amount] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[Percentage] [decimal](22, 4) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[TaxResponsesInBillingCatalogItem](
					[Id] [varchar](400) NULL,
					[TaxResponseId] [varchar](400) NULL,
					[BillingCatalogItemId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[PaymentMethods](
					[Id] [varchar](400) NULL,
					[ClubAccountIdNameId] [varchar](400) NULL,
					[AbcBillingIdNameId] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[ClubAccountIdName](
					[CJId] [varchar](400) NULL,
					[Id] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[AbcCode] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[AbcBillingIdName](
					[CJId] [varchar](400) NULL,
					[Id] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[AbcCode] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[WebPlan](
					[PlanName] [varchar](400) NULL,
					[PlanId] [varchar](400) NULL,
					[PromotionCode] [varchar](400) NULL,
					[PromotionName] [varchar](400) NULL,
					[MembershipType] [varchar](400) NULL,
					[AgreementTerm] [varchar](400) NULL,
					[ScheduleFrequency] [varchar](400) NULL,
					[TermInMonths] [varchar](400) NULL,
					[DueDay] [varchar](400) NULL,
					[FirstDueDate] datetime2 NULL,
					[ActivePresale] [bit] NULL,
					[ExpirationDate] [varchar](400) NULL,
					[OnlineSignupAllowedPaymentMethods] [varchar](400) NULL,
					[PreferredPaymentMethod] [varchar](400) NULL,
					[TotalContractValue] [varchar](400) NULL,
					[DownPaymentName] [varchar](400) NULL,
					[DownPaymentTotalAmount] [varchar](400) NULL,
					[ScheduleTotalAmount] [varchar](400) NULL,
					[AgreementTerms] [varchar](400) NULL,
					[AgreementDescription] [varchar](4000) NULL,
					[AgreementNote] [varchar](400) NULL,
					[EmailGreeting] [varchar](400) NULL,
					[EmailClosing] [varchar](400) NULL,
					[ClubFeeTotalAmount] [varchar](400) NULL,
					[PlanValidation] [int] NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[WebPayment](
					[PlanId] [varchar](400) NULL,
					[SubTotal] [varchar](400) NULL,
					[Tax] [varchar](400) NULL,
					[Total] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[WebSchedule](
					[PlanId] [varchar](400) NULL,
					[ProfitCenter] [varchar](400) NULL,
					[ScheduleDueDate] [varchar](400) NULL,
					[ScheduleAmount] [varchar](400) NULL,
					[NumberOfPayments] [varchar](400) NULL,
					[Recurring] [bit] NULL,
					[Addon] [bit] NULL,
					[DefaultChecked] [bit] NULL,
					[Description] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[WebUserDefinedField](
					[PlanId] [varchar](400) NULL,
					[Field] [varchar](400) NULL,
					[Table] [int] NULL,
					[Index] [int] NULL,
					[Type] [varchar](400) NULL,
					[Required] [bit] NULL,
					[Values] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[WebFee](
					[PlanId] [varchar](400) NULL,
					[FeeDueDate] datetime2 NULL,
					[FeeName] [varchar](400) NULL,
					[FeeAmount] [varchar](400) NULL,
					[FeeApply] [bit] NULL,
					[FeeRecurring] [bit] NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[FieldOption](
					[PlanId] [varchar](400) NULL,
					[FieldOptionCode] [varchar](400) NULL,
					[State] [varchar](400) NULL
				) ON [PRIMARY];

				CREATE TABLE [ABC_Financial_' + @companyname + '].[dbo].[Station](
					[StationId] [varchar](400) NULL,
					[Name] [varchar](400) NULL,
					[Status] [varchar](400) NULL,
					[AbcCode] [varchar](400) NULL
				) ON [PRIMARY];									
			 ';
  EXECUTE (@AbcBillingIdName);


  EXEC [ClearJelly].dbo.[CreateViews] @companyname;

  -- Copying the clear jelly standard calendar to user's database
  --SET @SQLString = 'Select * into [ABC_Financial_' + @companyname + '].dbo.Calendar from ClearJelly.dbo.calendar';
  --EXECUTE (@SQLString);

  --SET @AlterString = 'alter database [ABC_Financial_' + @companyname + '] set recovery simple '
  --EXECUTE (@AlterString);



  SET @SQLDB = 'USE [master] ';
  EXECUTE (@SQLDB);
  SET @SQLDB = ' USE [master];CREATE LOGIN [' + @username + '] WITH PASSWORD=N''' + @password + ''', CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;';

  SET @SQLDB = @SQLDB + ' DENY VIEW ANY DATABASE TO [' + @username + '];';
  EXECUTE (@SQLDB);

  SET @SQLDB = ' USE master';
  EXECUTE (@SQLDB);

  SET @SQLDB = ' ALTER AUTHORIZATION ON DATABASE::[ABC_Financial_' + @companyname + '] TO [' + @username + ']';
  EXECUTE (@SQLDB);

END