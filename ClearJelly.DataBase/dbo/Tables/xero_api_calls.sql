﻿CREATE TABLE [dbo].[xero_api_calls] (
    [OrgShortCode] VARCHAR (100) NULL,
    [CallDate]     DATE          NULL,
    [CallCount]    INT           NULL
);

