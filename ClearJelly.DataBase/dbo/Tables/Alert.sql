﻿CREATE TABLE [dbo].[Alert] (
    [AlertId]          VARCHAR (400)   NOT NULL,
    [Message]          VARCHAR (400)   NULL,
    [AbcCode]          VARCHAR (400)   NULL,
    [Priority]         VARCHAR (400)   NULL,
    [AllowDoorAccess]  VARCHAR (400)   NULL,
    [EvaluationDate]   VARCHAR (400)   NULL,
    [GracePeriod]      VARCHAR (400)   NULL,
    [EvaluationAmount] DECIMAL (22, 4) NULL
);

