﻿CREATE TABLE [dbo].[UploadScenario] (
    [EntityName]            NVARCHAR (MAX) NULL,
    [ClearJellyAccountCode] NVARCHAR (MAX) NULL,
    [SourceAccountCode]     NVARCHAR (MAX) NULL,
    [SourceAccountName]     NVARCHAR (MAX) NULL,
    [Scenario]              NVARCHAR (MAX) NULL,
    [Date]                  DATETIME       NULL,
    [Comment]               NVARCHAR (MAX) NULL,
    [Value]                 NVARCHAR (MAX) NULL,
    [TrackingCode1]         NVARCHAR (MAX) NULL,
    [TrackingCode2]         NVARCHAR (MAX) NULL
);

