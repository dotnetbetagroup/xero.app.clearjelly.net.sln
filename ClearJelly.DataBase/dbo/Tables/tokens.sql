﻿CREATE TABLE [dbo].[tokens] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [UserId]           VARCHAR (800) NOT NULL,
    [OrganisationId]   VARCHAR (800) NOT NULL,
    [ConsumerKey]      VARCHAR (800) NOT NULL,
    [ConsumerSecret]   VARCHAR (800) NOT NULL,
    [TokenKey]         VARCHAR (800) NOT NULL,
    [TokenSecret]      VARCHAR (800) NOT NULL,
    [ExpiresAt]        DATETIME      NULL,
    [Session]          VARCHAR (800) NULL,
    [SessionExpiresAt] DATETIME      NULL,
    CONSTRAINT [PK_tokens] PRIMARY KEY CLUSTERED ([Id] ASC)
);

