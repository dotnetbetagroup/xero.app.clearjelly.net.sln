﻿CREATE TABLE [dbo].[MemberChildNote] (
    [MemberChildNoteId]   VARCHAR (400) NOT NULL,
    [NoteText]            VARCHAR (400) NULL,
    [NoteCreateTimestamp] DATETIME2 (7) NULL,
    [EmployeeId]          VARCHAR (400) NULL
);

