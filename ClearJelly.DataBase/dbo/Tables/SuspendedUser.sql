﻿CREATE TABLE [dbo].[SuspendedUser] (
    [SuspendedUserId] INT            IDENTITY (1, 1) NOT NULL,
    [UserId]          INT            NOT NULL,
    [SuspendedDate]   DATETIME       NULL,
    [Reason]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TerminatedUser] PRIMARY KEY CLUSTERED ([SuspendedUserId] ASC),
    CONSTRAINT [FK_TerminatedUser_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

