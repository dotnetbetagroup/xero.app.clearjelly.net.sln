﻿CREATE TABLE [dbo].[PaypalSyncDataHistory] (
    [HistoryId]             UNIQUEIDENTIFIER NOT NULL,
    [RecurringProfileId]    NVARCHAR (100)   NOT NULL,
    [CompanySubscriptionId] INT              NOT NULL,
    [Status]                INT              NOT NULL,
    [LastPaymentAmount]     DECIMAL (10, 2)  NOT NULL,
    [NoOfCycles]            INT              NOT NULL,
    [CreatedDate]           DATETIME         NOT NULL,
    [UpdatedDate]           DATETIME         NULL,
    [LastSyncOn]            DATETIME         NULL,
    CONSTRAINT [PK_CompanySubscriptionHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC),
    CONSTRAINT [FK_CompanySubscriptionHistory_CompanySubscription] FOREIGN KEY ([CompanySubscriptionId]) REFERENCES [dbo].[CompanySubscription] ([CompanySubscriptionId])
);

