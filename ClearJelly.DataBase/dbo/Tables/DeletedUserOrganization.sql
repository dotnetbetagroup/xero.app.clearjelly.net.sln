﻿CREATE TABLE [dbo].[DeletedUserOrganization] (
    [DeletedUserOrganizationId] INT            IDENTITY (1, 1) NOT NULL,
    [DeletedUserId]             INT            NOT NULL,
    [OrgShortCode]              NVARCHAR (100) NOT NULL,
    [OrgName]                   NVARCHAR (100) NOT NULL,
    [DeletedDate]               DATETIME       NOT NULL,
    CONSTRAINT [PK_DeletedUserOrganization] PRIMARY KEY CLUSTERED ([DeletedUserOrganizationId] ASC),
    CONSTRAINT [FK_DeletedUserOrganization_DeletedUserOrganization] FOREIGN KEY ([DeletedUserId]) REFERENCES [dbo].[DeletedUser] ([DeletedUserId])
);

