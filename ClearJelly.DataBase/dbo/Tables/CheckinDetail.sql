﻿CREATE TABLE [dbo].[CheckinDetail] (
    [CheckinDetailId]  VARCHAR (400) NOT NULL,
    [CheckInId]        VARCHAR (400) NULL,
    [ClubNumber]       VARCHAR (400) NULL,
    [CheckInTimeStamp] DATETIME2 (7) NULL
);

