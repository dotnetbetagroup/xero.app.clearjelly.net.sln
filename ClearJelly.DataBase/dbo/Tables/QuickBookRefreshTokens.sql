﻿CREATE TABLE [dbo].[QuickBookRefreshTokens] (
    [Id]             NVARCHAR (200)  NOT NULL,
    [UserId]         INT             NULL,
    [RealmId]        NVARCHAR (200)  NULL,
    [RefreshToken]   NVARCHAR (200)  NULL,
    [AccessToken]    NVARCHAR (4000) NULL,
    [CreationDate]   DATETIME        NULL,
    [ExpirationDate] DATETIME        NULL
);

