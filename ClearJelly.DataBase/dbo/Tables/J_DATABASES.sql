﻿CREATE TABLE [dbo].[J_DATABASES] (
    [Id]                 INT             NULL,
    [Name]               NVARCHAR (4000) NULL,
    [isNormalDatabase]   NVARCHAR (4000) NULL,
    [isSystemDatabase]   NVARCHAR (4000) NULL,
    [isUserInfoDatabase] NVARCHAR (4000) NULL
);

