﻿CREATE TABLE [dbo].[DeletedCompany] (
    [DeletedCompanyId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (150) NOT NULL,
    [Address]          NVARCHAR (500) NULL,
    [PhoneNumber]      NVARCHAR (15)  NULL,
    [Website]          NVARCHAR (225) NULL,
    [CountryId]        INT            NOT NULL,
    CONSTRAINT [PK_DeletedCompany] PRIMARY KEY CLUSTERED ([DeletedCompanyId] ASC),
    CONSTRAINT [FK_DeletedCompany_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryId])
);

