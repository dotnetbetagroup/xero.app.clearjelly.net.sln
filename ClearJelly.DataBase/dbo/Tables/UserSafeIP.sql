﻿CREATE TABLE [dbo].[UserSafeIP] (
    [UserSafeIPId] INT           IDENTITY (1, 1) NOT NULL,
    [IPAddress]    NVARCHAR (50) NOT NULL,
    [UserId]       INT           NOT NULL,
    CONSTRAINT [PK_UserSafeIP] PRIMARY KEY CLUSTERED ([UserSafeIPId] ASC),
    CONSTRAINT [FK_UserSafeIP_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

