﻿CREATE TABLE [dbo].[PaypalSyncData] (
    [CompanyHistory]        INT            IDENTITY (1, 1) NOT NULL,
    [CompanySubscriptionId] INT            NOT NULL,
    [Email]                 NVARCHAR (100) NOT NULL,
    [IsPaid]                BIT            NOT NULL,
    [LastPaymentDate]       DATETIME       NULL,
    [NoOfCycles]            INT            NOT NULL,
    [Status]                INT            NOT NULL,
    CONSTRAINT [PK_PaidOrTrial] PRIMARY KEY CLUSTERED ([CompanyHistory] ASC),
    CONSTRAINT [FK_PaypalSyncData_CompanySubscription] FOREIGN KEY ([CompanySubscriptionId]) REFERENCES [dbo].[CompanySubscription] ([CompanySubscriptionId])
);

