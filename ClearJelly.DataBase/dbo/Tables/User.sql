﻿CREATE TABLE [dbo].[User] (
    [UserId]           INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]        VARCHAR (50)  NOT NULL,
    [LastName]         VARCHAR (50)  NOT NULL,
    [LoginType]        SMALLINT      NOT NULL,
    [Password]         VARCHAR (100) NULL,
    [Address]          VARCHAR (200) NULL,
    [Email]            VARCHAR (100) NOT NULL,
    [Mobile]           VARCHAR (20)  NULL,
    [Organization]     VARCHAR (100) NULL,
    [ProfilePicture]   VARCHAR (250) NULL,
    [FBUserId]         VARCHAR (100) NULL,
    [LinkedInUserId]   VARCHAR (100) NULL,
    [IsAdmin]          BIT           CONSTRAINT [DF_User_IsAdmin] DEFAULT ((0)) NOT NULL,
    [PaypalPayerId]    VARCHAR (50)  NULL,
    [PaypalToken]      VARCHAR (50)  NULL,
    [RegistrationDate] DATE          NOT NULL,
    [IsDeleted]        BIT           CONSTRAINT [DF_User_IsDeleted] DEFAULT ((0)) NOT NULL,
    [PwdResetCode]     VARCHAR (50)  NULL,
    [ActivationCode]   VARCHAR (50)  NULL,
    [IsActive]         BIT           CONSTRAINT [DF_User_IsActive] DEFAULT ((0)) NOT NULL,
    [CompanyId]        INT           NOT NULL,
    [LastloginFailed]  DATETIME      NULL,
    [ChosenService]    VARCHAR (50)  NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_User_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);

