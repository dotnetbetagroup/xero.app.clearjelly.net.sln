﻿CREATE TABLE [dbo].[EmailTemplate] (
    [EmailTemplateId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [TemplateContent] VARCHAR (MAX) NULL,
    [EmailCategoryId] INT           NULL,
    [IsActive]        BIT           NOT NULL,
    [Subject]         VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Email_Template] PRIMARY KEY CLUSTERED ([EmailTemplateId] ASC),
    CONSTRAINT [FK_Email_Template_Email_Category] FOREIGN KEY ([EmailCategoryId]) REFERENCES [dbo].[EmailCategory] ([EmailCategoryId])
);

