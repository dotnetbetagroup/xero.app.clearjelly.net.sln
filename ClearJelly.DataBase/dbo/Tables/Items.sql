﻿CREATE TABLE [dbo].[Items] (
    [ItemId]             VARCHAR (400) NULL,
    [Name]               VARCHAR (400) NULL,
    [InventoryType]      INT           NULL,
    [Sale]               BIT           NULL,
    [Upc]                VARCHAR (400) NULL,
    [ProfitCenter]       VARCHAR (400) NULL,
    [Catalog]            VARCHAR (400) NULL,
    [UnitPrice]          VARCHAR (400) NULL,
    [Quantity]           VARCHAR (400) NULL,
    [PackageQuantity]    VARCHAR (400) NULL,
    [Subtotal]           VARCHAR (400) NULL,
    [Tax]                VARCHAR (400) NULL,
    [RecurringServiceId] VARCHAR (400) NULL,
    [TransactionId]      VARCHAR (400) NULL
);

