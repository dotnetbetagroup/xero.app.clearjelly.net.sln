﻿CREATE TABLE [dbo].[EmailCategory] (
    [EmailCategoryId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Email_Category] PRIMARY KEY CLUSTERED ([EmailCategoryId] ASC)
);

