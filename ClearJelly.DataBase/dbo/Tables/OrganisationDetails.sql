﻿CREATE TABLE [dbo].[OrganisationDetails] (
    [OrgDetailId]        INT            IDENTITY (1, 1) NOT NULL,
    [APIKey]             NVARCHAR (100) NULL,
    [BaseCurrency]       NVARCHAR (10)  NOT NULL,
    [CreatedDate]        DATETIME       NULL,
    [OrgName]            NVARCHAR (250) NOT NULL,
    [OrgStatus]          BIT            NOT NULL,
    [OrgType]            NVARCHAR (50)  NOT NULL,
    [RegistrationNumber] NVARCHAR (300) NULL,
    [SalesTaxBasisType]  NVARCHAR (50)  NULL,
    [SalestaxPeriod]     NVARCHAR (50)  NULL,
    [OrgShortCode]       NCHAR (50)     NULL,
    [AddressLine1]       NVARCHAR (250) NULL,
    [AddressLine2]       NVARCHAR (250) NULL,
    [AddressLine3]       NVARCHAR (250) NULL,
    [AddressLine4]       NVARCHAR (250) NULL,
    [AddressType]        NVARCHAR (100) NULL,
    [City]               NVARCHAR (50)  NULL,
    [PostalCode]         NVARCHAR (50)  NULL,
    [CompanyId]          INT            NOT NULL,
    CONSTRAINT [PK_OrganisationDetails] PRIMARY KEY CLUSTERED ([OrgDetailId] ASC),
    CONSTRAINT [FK_OrganisationDetails_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);

