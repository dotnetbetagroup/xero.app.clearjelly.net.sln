﻿CREATE TABLE [dbo].[RequestTokens] (
    [UserName]    VARCHAR (400) NOT NULL,
    [AccessToken] VARCHAR (200) NULL,
    [CallbackUrl] VARCHAR (200) NULL,
    [Realm]       VARCHAR (100) NULL,
    [Token]       VARCHAR (200) NULL,
    [TokenSecret] VARCHAR (200) NULL,
    [Verifier]    VARCHAR (200) NULL,
    CONSTRAINT [PK_RequestTokens_UserName] PRIMARY KEY CLUSTERED ([UserName] ASC)
);

