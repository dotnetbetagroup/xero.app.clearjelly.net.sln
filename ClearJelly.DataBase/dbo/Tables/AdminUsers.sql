﻿CREATE TABLE [dbo].[AdminUsers] (
    [AdminUserId] INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]   NVARCHAR (50)  NULL,
    [LastName]    NVARCHAR (50)  NULL,
    [Email]       NVARCHAR (255) NULL,
    [Password]    NVARCHAR (100) NULL,
    [MobileNo]    NVARCHAR (20)  NULL,
    CONSTRAINT [PK_AdminUsers] PRIMARY KEY CLUSTERED ([AdminUserId] ASC)
);

