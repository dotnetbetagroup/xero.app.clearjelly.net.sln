﻿CREATE TABLE [dbo].[AnnualFees] (
    [Id]                  VARCHAR (400)   NULL,
    [Name]                VARCHAR (400)   NULL,
    [Amount]              DECIMAL (22, 4) NULL,
    [PreTaxAmount]        DECIMAL (22, 4) NULL,
    [ProfitCenterAbcCode] VARCHAR (400)   NULL
);

