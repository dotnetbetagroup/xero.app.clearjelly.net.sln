﻿CREATE TABLE [dbo].[CCNames] (
    [CCNamesId]                  VARCHAR (400) NOT NULL,
    [RequireCcNameMatch]         BIT           NULL,
    [DifferentCcNamesDisclaimer] VARCHAR (400) NULL
);

