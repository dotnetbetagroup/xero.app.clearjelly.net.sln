﻿CREATE TABLE [dbo].[DeletedOrganizations] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [UserId]       INT            NULL,
    [OrgShortCode] NVARCHAR (100) NOT NULL,
    [OrgName]      NVARCHAR (100) NOT NULL,
    [DeletedDate]  DATETIME       NOT NULL,
    CONSTRAINT [PK_DeletedOrganizarions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DeletedOrganizations_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

