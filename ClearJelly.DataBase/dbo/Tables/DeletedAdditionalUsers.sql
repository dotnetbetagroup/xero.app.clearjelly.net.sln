﻿CREATE TABLE [dbo].[DeletedAdditionalUsers] (
    [DeletedAdditionalUserId]      INT  IDENTITY (1, 1) NOT NULL,
    [DeletedUserId]                INT  NULL,
    [DeletedCompanySubscriptionId] INT  NULL,
    [JoiningDate]                  DATE NULL,
    CONSTRAINT [PK_DeletedAdditionalUsers] PRIMARY KEY CLUSTERED ([DeletedAdditionalUserId] ASC),
    CONSTRAINT [FK_DeletedAdditionalUsers_DeletedCompanySubscription] FOREIGN KEY ([DeletedCompanySubscriptionId]) REFERENCES [dbo].[DeletedCompanySubscription] ([DeletedCompanySubscriptionId]),
    CONSTRAINT [FK_DeletedAdditionalUsers_DeletedUser] FOREIGN KEY ([DeletedUserId]) REFERENCES [dbo].[DeletedUser] ([DeletedUserId])
);

