﻿CREATE TABLE [dbo].[CompanySubscription] (
    [CompanySubscriptionId] INT          IDENTITY (1, 1) NOT NULL,
    [CompanyId]             INT          NOT NULL,
    [SubscriptionTypeId]    INT          NOT NULL,
    [StartDate]             DATE         NOT NULL,
    [EndDate]               DATE         NULL,
    [RecurringProfileId]    VARCHAR (50) NULL,
    [IsActive]              BIT          NOT NULL,
    [Quantity]              INT          CONSTRAINT [DF_CompanySubscription_Quantity] DEFAULT ((0)) NOT NULL,
    [AdditionalUsers]       INT          CONSTRAINT [DF_CompanySubscription_AdditionalUsers] DEFAULT ((0)) NOT NULL,
    [IsYearlySubscription]  BIT          CONSTRAINT [DF_CompanySubscription_IsSubscriptionYearly] DEFAULT ((0)) NOT NULL,
    [StartedAsTrial]        BIT          NOT NULL,
    CONSTRAINT [PK_CompanySubscription] PRIMARY KEY CLUSTERED ([CompanySubscriptionId] ASC),
    CONSTRAINT [FK_CompanySubscription_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_CompanySubscription_SubscriptionType] FOREIGN KEY ([SubscriptionTypeId]) REFERENCES [dbo].[SubscriptionType] ([SubscriptionTypeId])
);

