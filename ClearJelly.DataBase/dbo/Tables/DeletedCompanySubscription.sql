﻿CREATE TABLE [dbo].[DeletedCompanySubscription] (
    [DeletedCompanySubscriptionId] INT          IDENTITY (1, 1) NOT NULL,
    [DeletedCompanyId]             INT          NOT NULL,
    [SubscriptionTypeId]           INT          NOT NULL,
    [StartDate]                    DATE         NOT NULL,
    [EndDate]                      DATE         NULL,
    [RecurringProfileId]           VARCHAR (50) NULL,
    [IsActive]                     BIT          NOT NULL,
    [Quantity]                     INT          DEFAULT ((0)) NOT NULL,
    [AdditionalUsers]              INT          DEFAULT ((0)) NOT NULL,
    [IsYearlySubscription]         BIT          DEFAULT ((0)) NOT NULL,
    [StartedAsTrial]               BIT          NOT NULL,
    CONSTRAINT [PK_DeletedCompanySubscription] PRIMARY KEY CLUSTERED ([DeletedCompanySubscriptionId] ASC),
    CONSTRAINT [FK_DeletedCompanySubscription_Company] FOREIGN KEY ([DeletedCompanyId]) REFERENCES [dbo].[DeletedCompany] ([DeletedCompanyId]),
    CONSTRAINT [FK_DeletedCompanySubscription_SubscriptionType] FOREIGN KEY ([SubscriptionTypeId]) REFERENCES [dbo].[SubscriptionType] ([SubscriptionTypeId])
);

