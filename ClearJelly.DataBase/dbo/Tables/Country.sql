﻿CREATE TABLE [dbo].[Country] (
    [CountryId] INT             IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (MAX)  NOT NULL,
    [GSTRate]   DECIMAL (30, 2) CONSTRAINT [DF_Country_GSTRate] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC)
);

