﻿CREATE TABLE [dbo].[QuickBookCompanies] (
    [Id]       NVARCHAR (200) NOT NULL,
    [RealmId]  NVARCHAR (200) NULL,
    [OrgName]  NVARCHAR (200) NULL,
    [UserId]   INT            NULL,
    [isActive] BIT            NULL
);

