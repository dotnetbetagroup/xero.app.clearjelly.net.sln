﻿CREATE TABLE [dbo].[Xero_User_Org_new] (
    [CompanyID]    INT           NOT NULL,
    [OrgShortCode] VARCHAR (800) NOT NULL,
    [OrgName]      VARCHAR (800) NULL,
    [isActive]     BIT           NULL,
    PRIMARY KEY CLUSTERED ([CompanyID] ASC, [OrgShortCode] ASC),
    CONSTRAINT [fk_Xero_User_Org_new_userID] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Company] ([CompanyId])
);

