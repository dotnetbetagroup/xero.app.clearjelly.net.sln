﻿CREATE TABLE [dbo].[UploadCustomData] (
    [EntityName]            NVARCHAR (MAX) NULL,
    [ClearJellyAccountCode] NVARCHAR (MAX) NULL,
    [SourceAccountCode]     NVARCHAR (MAX) NULL,
    [Scenario]              NVARCHAR (MAX) NULL,
    [Date]                  DATETIME       NULL,
    [Comment]               NVARCHAR (MAX) NULL,
    [Value]                 NVARCHAR (MAX) NULL
);

