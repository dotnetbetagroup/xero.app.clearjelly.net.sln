﻿CREATE TABLE [dbo].[BillingCatalogItem] (
    [Id]                  VARCHAR (400)   NULL,
    [Name]                VARCHAR (400)   NULL,
    [ProfitCenterAbcCode] VARCHAR (400)   NULL,
    [AnnualFee]           BIT             NULL,
    [TotalTaxRate]        DECIMAL (22, 4) NULL
);

