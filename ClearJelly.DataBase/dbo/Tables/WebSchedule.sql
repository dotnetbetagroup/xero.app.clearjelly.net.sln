﻿CREATE TABLE [dbo].[WebSchedule] (
    [PlanId]           VARCHAR (400) NULL,
    [ProfitCenter]     VARCHAR (400) NULL,
    [ScheduleDueDate]  VARCHAR (400) NULL,
    [ScheduleAmount]   VARCHAR (400) NULL,
    [NumberOfPayments] VARCHAR (400) NULL,
    [Recurring]        BIT           NULL,
    [Addon]            BIT           NULL,
    [DefaultChecked]   BIT           NULL,
    [Description]      VARCHAR (400) NULL
);

