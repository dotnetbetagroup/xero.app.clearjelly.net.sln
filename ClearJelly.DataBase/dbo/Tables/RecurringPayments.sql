﻿CREATE TABLE [dbo].[RecurringPayments] (
    [RecurringPaymentId]    INT             IDENTITY (1, 1) NOT NULL,
    [CompanySubscriptionId] INT             NOT NULL,
    [PaymentStatus]         INT             NOT NULL,
    [Amount]                DECIMAL (18, 2) NOT NULL,
    [PayerEmail]            NVARCHAR (200)  NULL,
    [PaymentCycle]          INT             NOT NULL,
    [PaymentDate]           DATETIME        NULL,
    [NextPaymentDate]       DATETIME        NULL,
    [RecurringCreatedDate]  DATETIME        NOT NULL,
    [OutstandingBalance]    DECIMAL (18, 2) NULL,
    [PayerId]               NVARCHAR (200)  NULL,
    [Currency]              NVARCHAR (50)   NOT NULL,
    [RecurringProfileId]    NVARCHAR (20)   NOT NULL,
    [CreatedDate]           DATETIME        NOT NULL,
    [ModifiedDate]          DATETIME        NULL,
    [RecurringStatus]       INT             NOT NULL,
    CONSTRAINT [PK_RecurringPayments] PRIMARY KEY CLUSTERED ([RecurringPaymentId] ASC),
    CONSTRAINT [FK_PaypalResponseTable_CompanySubscription] FOREIGN KEY ([CompanySubscriptionId]) REFERENCES [dbo].[CompanySubscription] ([CompanySubscriptionId])
);

