﻿CREATE TABLE [dbo].[AdditionalUsers] (
    [AdditionalUserId]      INT  IDENTITY (1, 1) NOT NULL,
    [UserId]                INT  NULL,
    [CompanySubscriptionId] INT  NULL,
    [JoiningDate]           DATE NULL,
    CONSTRAINT [PK_AdditionalUsers] PRIMARY KEY CLUSTERED ([AdditionalUserId] ASC),
    CONSTRAINT [FK_AdditionalUsers_CompanySubscription] FOREIGN KEY ([CompanySubscriptionId]) REFERENCES [dbo].[CompanySubscription] ([CompanySubscriptionId]),
    CONSTRAINT [FK_AdditionalUsers_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

