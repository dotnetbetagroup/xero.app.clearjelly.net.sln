﻿CREATE TABLE [dbo].[SampleLink] (
    [SampleLinkId] INT           IDENTITY (1, 1) NOT NULL,
    [Title]        VARCHAR (200) NOT NULL,
    [DownloadLink] VARCHAR (500) NOT NULL,
    [SortOrder]    INT           NOT NULL,
    CONSTRAINT [PK_SampleLink] PRIMARY KEY CLUSTERED ([SampleLinkId] ASC)
);

