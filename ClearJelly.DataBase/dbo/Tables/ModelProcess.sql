﻿CREATE TABLE [dbo].[ModelProcess] (
    [ModelProcessId]   INT            IDENTITY (1, 1) NOT NULL,
    [UserId]           INT            NULL,
    [StartDate]        DATETIME       NOT NULL,
    [EndDate]          DATETIME       NULL,
    [Status]           INT            NOT NULL,
    [ProcessType]      INT            NOT NULL,
    [OrganizationName] NVARCHAR (150) NULL,
    [IsMessageShown]   BIT            NOT NULL,
    [XeroErrorCode]    INT            NULL,
    [CreatedDate]      DATETIME       NULL,
    [FromDate]         DATETIME       NULL,
    [ToDate]           DATETIME       NULL,
    CONSTRAINT [PK_ModelProcess] PRIMARY KEY CLUSTERED ([ModelProcessId] ASC),
    CONSTRAINT [FK_ModelProcess_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

