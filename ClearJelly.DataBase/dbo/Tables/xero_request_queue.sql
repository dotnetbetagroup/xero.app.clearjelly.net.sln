﻿CREATE TABLE [dbo].[xero_request_queue] (
    [userID]        VARCHAR (800) NULL,
    [OrgShortCode]  VARCHAR (100) NULL,
    [CallDate]      DATE          NULL,
    [RequestStatus] VARCHAR (100) NULL,
    [RequestType]   VARCHAR (800) NULL,
    [ItemID]        VARCHAR (800) NULL,
    [DueDate]       DATETIME      NULL
);

