﻿CREATE TABLE [dbo].[SubscriptionType] (
    [SubscriptionTypeId] INT             IDENTITY (1, 1) NOT NULL,
    [TypeName]           VARCHAR (50)    NOT NULL,
    [Description]        NVARCHAR (1000) NULL,
    [MonthlyFee]         DECIMAL (10, 2) NOT NULL,
    [AdditionalUserFee]  DECIMAL (10, 2) CONSTRAINT [DF_SubscriptionType_AdditionalUserFee] DEFAULT ((0)) NOT NULL,
    [YearlyContractFee]  DECIMAL (10, 2) CONSTRAINT [DF_SubscriptionType_YearlyContractFee] DEFAULT ((0)) NOT NULL,
    [Entities]           INT             CONSTRAINT [DF_SubscriptionType_Entities] DEFAULT ((0)) NOT NULL,
    [IsActive]           BIT             NOT NULL,
    CONSTRAINT [PK_SubscriptionType] PRIMARY KEY CLUSTERED ([SubscriptionTypeId] ASC)
);

