﻿CREATE TABLE [dbo].[Calendar] (
    [Date]          DATE          NULL,
    [DayOfYear]     INT           NULL,
    [Day]           INT           NULL,
    [Month]         INT           NULL,
    [Year]          INT           NULL,
    [DayOfWeek]     VARCHAR (50)  NULL,
    [DayOfWeekAbr]  VARCHAR (50)  NULL,
    [WeekOfYear]    INT           NULL,
    [Quarter]       INT           NULL,
    [MonthName]     NVARCHAR (30) NULL,
    [MonthNameAbr]  NVARCHAR (3)  NULL,
    [MonthEndDate]  DATE          NULL,
    [YearEndDate]   DATE          NULL,
    [FiscalPeriod]  INT           NULL,
    [FiscalYearEnd] INT           NULL,
    [FiscalQuarter] INT           NULL
);

