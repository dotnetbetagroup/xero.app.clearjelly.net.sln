﻿CREATE TABLE [dbo].[MemberChildEmergencyContact] (
    [MemberChildEmergencyContactId] VARCHAR (400) NOT NULL,
    [EmergencyContactFirstName]     VARCHAR (400) NULL,
    [EmergencyContactMiddleInitial] VARCHAR (400) NULL,
    [EmergencyContactLastName]      VARCHAR (400) NULL,
    [EmergencyContactPhone]         VARCHAR (400) NULL,
    [EmergencyContactExtension]     VARCHAR (400) NULL
);

