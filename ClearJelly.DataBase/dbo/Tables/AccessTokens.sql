﻿CREATE TABLE [dbo].[AccessTokens] (
    [UserName]             VARCHAR (400) NOT NULL,
    [ConsumerKey]          VARCHAR (200) NULL,
    [CanRefresh]           VARCHAR (10)  NULL,
    [CreatedDateUtc]       VARCHAR (100) NULL,
    [ExpiresIn]            VARCHAR (100) NULL,
    [ExpiryDateUtc]        VARCHAR (100) NULL,
    [Realm]                VARCHAR (100) NULL,
    [SessionExpiresIn]     VARCHAR (100) NULL,
    [SessionExpiryDateUtc] VARCHAR (100) NULL,
    [SessionHandle]        VARCHAR (400) NULL,
    [SessionTimespan]      VARCHAR (400) NULL,
    [Token]                VARCHAR (400) NULL,
    [TokenSecret]          VARCHAR (400) NULL,
    [TokenTimespan]        VARCHAR (400) NULL,
    [organisation]         VARCHAR (400) NULL,
    CONSTRAINT [PK_UserName] PRIMARY KEY CLUSTERED ([UserName] ASC)
);

