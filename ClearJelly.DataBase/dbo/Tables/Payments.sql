﻿CREATE TABLE [dbo].[Payments] (
    [PaymentId]     VARCHAR (400) NOT NULL,
    [PaymentType]   VARCHAR (400) NULL,
    [PaymentAmount] VARCHAR (400) NULL,
    [PaymentTax]    VARCHAR (400) NULL
);

