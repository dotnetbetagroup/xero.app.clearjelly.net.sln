﻿CREATE TABLE [dbo].[PrimaryBillingAccountHolder] (
    [PrimaryBillingAccountHolderId] VARCHAR (400) NOT NULL,
    [FirstName]                     VARCHAR (400) NULL,
    [LastName]                      VARCHAR (400) NULL
);

