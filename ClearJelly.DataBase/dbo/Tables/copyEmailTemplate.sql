﻿CREATE TABLE [dbo].[copyEmailTemplate] (
    [EmailTemplateId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [TemplateContent] VARCHAR (MAX) NULL,
    [EmailCategoryId] INT           NULL,
    [IsActive]        BIT           NOT NULL,
    [Subject]         VARCHAR (100) NOT NULL
);

