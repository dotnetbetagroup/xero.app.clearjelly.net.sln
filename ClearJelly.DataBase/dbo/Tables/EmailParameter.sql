﻿CREATE TABLE [dbo].[EmailParameter] (
    [EmailParameterId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (50)  NOT NULL,
    [Description]      VARCHAR (250) NOT NULL,
    [EmailTemplateId]  INT           NULL,
    CONSTRAINT [PK_Email_Parameter] PRIMARY KEY CLUSTERED ([EmailParameterId] ASC),
    CONSTRAINT [FK_EmailParameter_EmailTemplate] FOREIGN KEY ([EmailTemplateId]) REFERENCES [dbo].[EmailTemplate] ([EmailTemplateId])
);

