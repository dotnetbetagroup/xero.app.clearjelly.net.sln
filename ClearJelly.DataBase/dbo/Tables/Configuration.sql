﻿CREATE TABLE [dbo].[Configuration] (
    [ConfigurationId] INT            IDENTITY (1, 1) NOT NULL,
    [Key]             NVARCHAR (100) NULL,
    [Value]           NVARCHAR (200) NULL,
    CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED ([ConfigurationId] ASC)
);

