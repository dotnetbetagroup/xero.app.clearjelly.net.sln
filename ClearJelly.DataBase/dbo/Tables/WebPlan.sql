﻿CREATE TABLE [dbo].[WebPlan] (
    [PlanName]                          VARCHAR (400) NULL,
    [PlanId]                            VARCHAR (400) NULL,
    [PromotionCode]                     VARCHAR (400) NULL,
    [PromotionName]                     VARCHAR (400) NULL,
    [MembershipType]                    VARCHAR (400) NULL,
    [AgreementTerm]                     VARCHAR (400) NULL,
    [ScheduleFrequency]                 VARCHAR (400) NULL,
    [TermInMonths]                      VARCHAR (400) NULL,
    [DueDay]                            VARCHAR (400) NULL,
    [FirstDueDate]                      DATETIME2 (7) NULL,
    [ActivePresale]                     BIT           NULL,
    [ExpirationDate]                    VARCHAR (400) NULL,
    [OnlineSignupAllowedPaymentMethods] VARCHAR (400) NULL,
    [PreferredPaymentMethod]            VARCHAR (400) NULL,
    [TotalContractValue]                VARCHAR (400) NULL,
    [DownPaymentName]                   VARCHAR (400) NULL,
    [DownPaymentTotalAmount]            VARCHAR (400) NULL,
    [ScheduleTotalAmount]               VARCHAR (400) NULL,
    [AgreementTerms]                    VARCHAR (400) NULL,
    [AgreementDescription]              VARCHAR (400) NULL,
    [AgreementNote]                     VARCHAR (400) NULL,
    [EmailGreeting]                     VARCHAR (400) NULL,
    [EmailClosing]                      VARCHAR (400) NULL,
    [ClubFeeTotalAmount]                VARCHAR (400) NULL,
    [PlanValidation]                    INT           NULL
);

