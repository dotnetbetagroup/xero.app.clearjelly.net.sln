﻿CREATE TABLE [dbo].[InstantPayments] (
    [InstantPaymentId] INT             IDENTITY (1, 1) NOT NULL,
    [PaymentStatus]    INT             NOT NULL,
    [Amount]           DECIMAL (18, 2) NOT NULL,
    [PayerEmailId]     NVARCHAR (200)  NULL,
    [PaymentDate]      DATETIME        NULL,
    [PayerId]          NVARCHAR (50)   NOT NULL,
    [Currency]         NVARCHAR (50)   NULL,
    [AdditionalUserId] INT             NOT NULL,
    CONSTRAINT [PK_InstantPayments] PRIMARY KEY CLUSTERED ([InstantPaymentId] ASC),
    CONSTRAINT [FK_AddionalUserPayments_AdditionalUsers] FOREIGN KEY ([AdditionalUserId]) REFERENCES [dbo].[AdditionalUsers] ([AdditionalUserId])
);

