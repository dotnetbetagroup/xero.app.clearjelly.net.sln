﻿CREATE TABLE [dbo].[Xero_User_Org] (
    [CompanyID]    INT           NOT NULL,
    [OrgShortCode] VARCHAR (800) NOT NULL,
    [OrgName]      VARCHAR (800) NULL,
    [isActive]     BIT           NULL,
    CONSTRAINT [PK_Xero_User_Org] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [OrgShortCode] ASC)
);

