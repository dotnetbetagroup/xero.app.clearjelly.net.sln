﻿CREATE TABLE [dbo].[WebUserDefinedField] (
    [PlanId]   VARCHAR (400) NULL,
    [Field]    VARCHAR (400) NULL,
    [Table]    INT           NULL,
    [Index]    INT           NULL,
    [Type]     VARCHAR (400) NULL,
    [Required] BIT           NULL,
    [Values]   VARCHAR (400) NULL
);

