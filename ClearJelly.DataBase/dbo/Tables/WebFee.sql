﻿CREATE TABLE [dbo].[WebFee] (
    [PlanId]       VARCHAR (400) NULL,
    [FeeDueDate]   DATETIME2 (7) NULL,
    [FeeName]      VARCHAR (400) NULL,
    [FeeAmount]    VARCHAR (400) NULL,
    [FeeApply]     BIT           NULL,
    [FeeRecurring] BIT           NULL
);

