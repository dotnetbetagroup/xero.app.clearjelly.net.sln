﻿CREATE TABLE [dbo].[Identities] (
    [IdentityId]            VARCHAR (400) NULL,
    [MembershipTypeAbcCode] VARCHAR (400) NULL,
    [HomeVpdId]             INT           NULL,
    [HomeClub]              VARCHAR (400) NULL,
    [HomeCompanyName]       VARCHAR (400) NULL,
    [CorpId]                VARCHAR (400) NULL,
    [MemberId]              VARCHAR (400) NULL,
    [Barcode]               VARCHAR (400) NULL,
    [FirstName]             VARCHAR (400) NULL,
    [LastName]              VARCHAR (400) NULL,
    [IsActive]              BIT           NULL
);

