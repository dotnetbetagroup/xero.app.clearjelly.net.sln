﻿CREATE TABLE [dbo].[Settings] (
    [SettingId]   INT            NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Value]       NVARCHAR (250) NOT NULL,
    [Description] NVARCHAR (500) NULL,
    [UpdatedDate] DATE           NULL,
    CONSTRAINT [PK_Appsetting] PRIMARY KEY CLUSTERED ([SettingId] ASC)
);

