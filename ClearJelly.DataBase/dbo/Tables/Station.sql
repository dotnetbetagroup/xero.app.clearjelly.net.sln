﻿CREATE TABLE [dbo].[Station] (
    [StationId] VARCHAR (400) NULL,
    [Name]      VARCHAR (400) NULL,
    [Status]    VARCHAR (400) NULL,
    [AbcCode]   VARCHAR (400) NULL
);

