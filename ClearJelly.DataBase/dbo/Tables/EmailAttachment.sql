﻿CREATE TABLE [dbo].[EmailAttachment] (
    [EmailAttachmentId] INT           NOT NULL,
    [FileName]          VARCHAR (250) NOT NULL,
    [AttachmentName]    VARCHAR (250) NOT NULL,
    [EmailTemplateId]   INT           NULL,
    CONSTRAINT [PK_Email_Attachment] PRIMARY KEY CLUSTERED ([EmailAttachmentId] ASC),
    CONSTRAINT [FK_EmailAttachment_EmailTemplate] FOREIGN KEY ([EmailTemplateId]) REFERENCES [dbo].[EmailTemplate] ([EmailTemplateId])
);

