﻿CREATE TABLE [dbo].[LogTable] (
    [LogId]      INT             IDENTITY (1, 1) NOT NULL,
    [LogDate]    DATETIME        NOT NULL,
    [EventLevel] NVARCHAR (50)   NOT NULL,
    [LoggerName] NVARCHAR (500)  NOT NULL,
    [Url]        NVARCHAR (1024) NULL,
    [Code]       NVARCHAR (10)   NULL,
    [LogMessage] NVARCHAR (MAX)  NOT NULL,
    PRIMARY KEY CLUSTERED ([LogId] ASC)
);

