﻿CREATE TABLE [dbo].[TaxResponse] (
    [Id]         VARCHAR (400)   NULL,
    [RateId]     VARCHAR (400)   NULL,
    [Amount]     VARCHAR (400)   NULL,
    [Name]       VARCHAR (400)   NULL,
    [Percentage] DECIMAL (22, 4) NULL
);

