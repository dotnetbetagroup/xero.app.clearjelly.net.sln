﻿CREATE TABLE [dbo].[WebPlanLists] (
    [PlanId]               VARCHAR (400) NULL,
    [PlanName]             VARCHAR (400) NULL,
    [PromoCode]            VARCHAR (400) NULL,
    [PromoName]            VARCHAR (400) NULL,
    [AgreementDescription] VARCHAR (400) NULL,
    [LimitedAvailability]  VARCHAR (400) NULL
);

