﻿CREATE TABLE [dbo].[logins] (
    [ID]       INT              IDENTITY (1, 1) NOT NULL,
    [username] VARCHAR (400)    NULL,
    [Company]  VARCHAR (400)    NULL,
    [password] VARCHAR (100)    NULL,
    [email]    VARCHAR (400)    NULL,
    [code]     UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

