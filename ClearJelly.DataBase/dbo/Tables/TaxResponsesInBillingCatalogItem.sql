﻿CREATE TABLE [dbo].[TaxResponsesInBillingCatalogItem] (
    [Id]                   VARCHAR (400) NULL,
    [TaxResponseId]        VARCHAR (400) NULL,
    [BillingCatalogItemId] VARCHAR (400) NULL
);

