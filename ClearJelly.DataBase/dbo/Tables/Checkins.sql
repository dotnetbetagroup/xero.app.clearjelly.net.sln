﻿CREATE TABLE [dbo].[Checkins] (
    [CheckInId]        VARCHAR (400) NULL,
    [CheckInTimestamp] DATETIME2 (7) NULL,
    [CheckInMessage]   VARCHAR (400) NULL,
    [StationName]      VARCHAR (400) NULL,
    [CheckInStatus]    VARCHAR (400) NULL,
    [MemberId]         VARCHAR (400) NULL
);

