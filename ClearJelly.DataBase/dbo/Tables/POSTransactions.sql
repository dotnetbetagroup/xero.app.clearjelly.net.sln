﻿CREATE TABLE [dbo].[POSTransactions] (
    [TransactionId]        VARCHAR (400) NULL,
    [TransactionTimestamp] DATETIME2 (7) NULL,
    [MemberId]             VARCHAR (400) NULL,
    [HomeClub]             VARCHAR (400) NULL,
    [EmployeeId]           VARCHAR (400) NULL,
    [ReceiptNumber]        VARCHAR (400) NULL,
    [StationName]          VARCHAR (400) NULL,
    [Return]               BIT           NULL
);

