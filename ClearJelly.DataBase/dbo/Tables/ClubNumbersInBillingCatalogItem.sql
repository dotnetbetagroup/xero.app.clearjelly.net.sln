﻿CREATE TABLE [dbo].[ClubNumbersInBillingCatalogItem] (
    [Id]                   VARCHAR (400) NULL,
    [ClubNumber]           VARCHAR (400) NULL,
    [BillingCatalogItemId] VARCHAR (400) NULL
);

