﻿CREATE TABLE [dbo].[Minors] (
    [MinorsId]        VARCHAR (400) NOT NULL,
    [AllowMinors]     BIT           NULL,
    [MinorAge]        VARCHAR (400) NULL,
    [MinorDisclaimer] VARCHAR (400) NULL
);

