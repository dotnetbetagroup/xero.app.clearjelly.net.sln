﻿CREATE TABLE [dbo].[MemberChild] (
    [MemberChildId]                 VARCHAR (400) NULL,
    [MemberChildEmergencyContactId] VARCHAR (400) NULL,
    [ActiveStatus]                  VARCHAR (400) NULL,
    [HomeClub]                      VARCHAR (400) NULL,
    [FirstName]                     VARCHAR (400) NULL,
    [MiddleInitial]                 VARCHAR (400) NULL,
    [LastName]                      VARCHAR (400) NULL,
    [Barcode]                       VARCHAR (400) NULL,
    [Gender]                        VARCHAR (400) NULL,
    [BirthDate]                     VARCHAR (400) NULL,
    [PrimaryMemberId]               VARCHAR (400) NULL,
    [AgreementNumber]               VARCHAR (400) NULL,
    [ChildMisc1]                    VARCHAR (400) NULL,
    [ChildMisc2]                    VARCHAR (400) NULL,
    [ChildMisc3]                    VARCHAR (400) NULL,
    [CreateTimestamp]               DATETIME2 (7) NULL,
    [LastModifiedTimestamp]         DATETIME2 (7) NULL,
    [LastCheckInTimestamp]          DATETIME2 (7) NULL
);

