﻿CREATE TABLE [dbo].[ClubInformation] (
    [ClubInformationId]                 VARCHAR (400) NOT NULL,
    [Name]                              VARCHAR (400) NULL,
    [ShortName]                         VARCHAR (400) NULL,
    [TimeZone]                          VARCHAR (400) NULL,
    [Address1]                          VARCHAR (400) NULL,
    [Address2]                          VARCHAR (400) NULL,
    [City]                              VARCHAR (400) NULL,
    [State]                             VARCHAR (400) NULL,
    [PostalCode]                        VARCHAR (400) NULL,
    [Country]                           VARCHAR (400) NULL,
    [Email]                             VARCHAR (400) NULL,
    [DonationItem]                      VARCHAR (400) NULL,
    [OnlineSignupAllowedPaymentMethods] INT           NULL,
    [OnlineId]                          VARCHAR (400) NULL,
    [BillingCountry]                    VARCHAR (400) NULL
);

