﻿CREATE TABLE [dbo].[API_Sessions] (
    [UserID]                         VARCHAR (600) NULL,
    [ConsumerSession]                VARCHAR (MAX) NULL,
    [AccessToken]                    VARCHAR (800) NULL,
    [accessToken_oauth_token]        VARCHAR (800) NULL,
    [accessToken_oauth_token_secret] VARCHAR (800) NULL,
    [CreatedDateUTC]                 DATETIME      NULL,
    [ExperiesIN]                     INT           NULL,
    [ExpiryDateUTC]                  DATETIME      NULL,
    [SessionExpiresIN]               INT           NULL,
    [SessionExpiryDateUTC]           DATETIME      NULL,
    [SessionHandle]                  VARCHAR (400) NULL,
    [SessionTimeSpan]                TIME (7)      NULL,
    [TokenTimeSpan]                  TIME (7)      NULL
);

