﻿CREATE VIEW dbo.vwUserInfo
AS
SELECT        B.Name AS CompanyName, A.Email,
                             (SELECT        STUFF
                                                             ((SELECT        ',"' + [OrgName] + '"'
                                                                 FROM            [ClearJelly].[dbo].[Xero_User_Org]
                                                                 WHERE        CompanyId = A.CompanyId FOR XML PATH('')), 1, 1, '') AS orgs) AS XeroOrgs,
                             (SELECT        STUFF
                                                             ((SELECT        ',"' + dbo.RemoveNonAlphaCharacters([OrgName]) + '"'
                                                                 FROM            [ClearJelly].[dbo].[Xero_User_Org]
                                                                 WHERE        CompanyId = A.CompanyId FOR XML PATH('')), 1, 1, '') AS orgs) AS JedoxDatabaseNames,
                             (SELECT        STUFF
                                                             ((SELECT        ',"' + dbo.RemoveNonAlphaCharacters([OrgName]) + '_group"'
                                                                 FROM            [ClearJelly].[dbo].[Xero_User_Org]
                                                                 WHERE        CompanyId = A.CompanyId FOR XML PATH('')), 1, 1, '') AS orgs) AS JedoxGroups, D .TypeName AS SubscriptionType, D .MonthlyFee, C.IsActive AS isCompanySubscriptionActive, 
                         C.StartDate AS CompanySubscriptionStartDate, C.EndDate AS CompanySubscriptionEndDate, A.UserId, A.FirstName, A.LastName, A.LoginType, A.Password, A.Address, A.Mobile, A.Organization, A.ProfilePicture, 
                         A.FBUserId, A.LinkedInUserId, A.IsAdmin, A.PaypalPayerId, A.PaypalToken, A.RegistrationDate, A.IsDeleted, A.PwdResetCode, A.ActivationCode, A.IsActive, A.CompanyId,
                             (SELECT        TOP 1 Name
                               FROM            country cnt
                               WHERE        cnt.CountryID = B.CountryID) CompanyCountry,
Case WHEN C.RecurringProfileId is not null and C.IsActive = 1 and C.EndDate is null then 1 else 0 end As IsPaid
FROM            dbo.[User] AS A INNER JOIN
                         dbo.Company AS B ON A.CompanyId = B.CompanyId INNER JOIN
                         dbo.CompanySubscription AS C ON A.CompanyId = C.CompanyId INNER JOIN
                         dbo.SubscriptionType AS D ON C.SubscriptionTypeId = D .SubscriptionTypeId

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[15] 4[18] 2[50] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwUserInfo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwUserInfo';

