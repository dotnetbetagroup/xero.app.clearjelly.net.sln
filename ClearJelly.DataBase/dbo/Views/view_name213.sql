﻿CREATE VIEW view_name213 AS
SELECT        B.Name AS CompanyName, A.Email,
                             (SELECT        STUFF
                                                             ((SELECT        ',"' + [OrgName] + '"'
                                                                 FROM            [ClearJelly].[dbo].[Xero_User_Org]
                                                                 WHERE        CompanyId = A.CompanyId FOR XML PATH('')), 1, 1, '') AS orgs) AS XeroOrgs,
                             (SELECT        STUFF
                                                             ((SELECT        ',"' + dbo.RemoveNonAlphaCharacters([OrgName]) + '"'
                                                                 FROM            [ClearJelly].[dbo].[Xero_User_Org]
                                                                 WHERE        CompanyId = A.CompanyId FOR XML PATH('')), 1, 1, '') AS orgs) AS JedoxDatabaseNames,
                             (SELECT        STUFF
                                                             ((SELECT        ',"' + dbo.RemoveNonAlphaCharacters([OrgName]) + '_group"'
                                                                 FROM            [ClearJelly].[dbo].[Xero_User_Org]
                                                                 WHERE        CompanyId = A.CompanyId FOR XML PATH('')), 1, 1, '') AS orgs) AS JedoxGroups, D .TypeName AS SubscriptionType, D .MonthlyFee, C.IsActive AS isCompanySubscriptionActive, 
                         C.StartDate AS CompanySubscriptionStartDate, C.EndDate AS CompanySubscriptionEndDate, A.UserId, A.FirstName, A.LastName, A.LoginType, A.Password, A.Address, A.Mobile, A.Organization, A.ProfilePicture, 
                         A.FBUserId, A.LinkedInUserId, A.IsAdmin, A.PaypalPayerId, A.PaypalToken, A.RegistrationDate, A.IsDeleted, A.PwdResetCode, A.ActivationCode, A.IsActive, A.CompanyId,
                             (SELECT        TOP 1 Name
                               FROM            country cnt
                               WHERE        cnt.CountryID = B.CountryID) CompanyCountry,
Case WHEN C.RecurringProfileId is not null and C.IsActive = 1 and C.EndDate is null then 1 else 0 end As IsPaid
FROM            dbo.[User] AS A INNER JOIN
                         dbo.Company AS B ON A.CompanyId = B.CompanyId INNER JOIN
                         dbo.CompanySubscription AS C ON A.CompanyId = C.CompanyId INNER JOIN
                         dbo.SubscriptionType AS D ON C.SubscriptionTypeId = D .SubscriptionTypeId