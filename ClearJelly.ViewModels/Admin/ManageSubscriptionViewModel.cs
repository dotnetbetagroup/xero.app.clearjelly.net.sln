﻿namespace ClearJelly.ViewModels.Admin
{
    public class ManageSubscriptionViewModel
    {
        public int? SubscriptionTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public decimal MonthlyFee { get; set; }
        public decimal AdditionalUserFee { get; set; }
        public decimal YearlyContractFee { get; set; }
        public int Entities { get; set; }
        public bool IsActive { get; set; }
        public int SubscribedCompanies { get; set; }
    }
}
