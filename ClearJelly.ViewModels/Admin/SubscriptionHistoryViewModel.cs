﻿using System;

namespace ClearJelly.ViewModels.Admin
{
    public class SubscriptionHistoryViewModel
    {
        public Guid HistoryId { get; set; }
        public string RecurringProfileId { get; set; }
        public string Amount { get; set; }
        public int NoOfCycles { get; set; }
        public string Status { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public decimal LastPaymentAmount { get; set; }
        public string LastPaymentDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastUpdatedDate { get; set; }

    }
}
