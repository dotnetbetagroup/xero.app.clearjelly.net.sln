﻿namespace ClearJelly.Web.ViewModels.Admin
{
    public class UserSafeIPListViewModel
    {
        public int UserId { get; set; }
        public string IpAddress { get; set; }

    }
}
