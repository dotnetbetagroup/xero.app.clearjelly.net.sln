﻿using System.Collections.Generic;

namespace ClearJelly.ViewModels.Admin
{
    public class ModelProcessListViewModel
    {
        public List<ModelProcessViewModel> lstModelProcessVm { get; set; }
        public ListXeroOrganization lstXeroOrg { get; set; }
    }
}
