﻿namespace ClearJelly.ViewModels.Admin
{
    public class TemplateParameters
    {
        public int EmailParameterId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }

    }
}
