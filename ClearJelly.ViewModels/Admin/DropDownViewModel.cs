﻿namespace ClearJelly.ViewModels.Admin
{
    public class DropDownViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
