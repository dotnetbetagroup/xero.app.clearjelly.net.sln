﻿namespace ClearJelly.ViewModels.Admin
{
    public class PaidOrTrialViewModel
    {
        public int CompanySubscriptionId { get; set; }
        public string Email { get; set; }
        public bool IsPaid { get; set; }
        public string LastPaymentDate { get; set; }
        public int NoOfCycles { get; set; }
        public string Status { get; set; }
        public string IsTrialOrPaid { get; set; }
        public string SubscriptionName { get; set; }

    }
}
