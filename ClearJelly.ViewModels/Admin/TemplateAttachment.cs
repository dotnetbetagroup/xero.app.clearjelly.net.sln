﻿namespace ClearJelly.ViewModels.Admin
{
    public class TemplateAttachment
    {
        public int EmailAttachmentId { get; set; }
        public string FileName { get; set; }
        public string AttachmentName { get; set; }
        public bool IsSaved { get; set; }
    }
}
