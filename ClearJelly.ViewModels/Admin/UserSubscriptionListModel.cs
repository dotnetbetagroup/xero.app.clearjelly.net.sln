﻿namespace ClearJelly.ViewModels.Admin
{
    public class UserSubscriptionListModel
    {
        public string SubscriptionType { get; set; }
        public bool StartedAsTrial { get; set; }
        public bool IsActive { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
        public int NoOfUsers { get; set; }

        public bool IsYearly { get; set; }
        public int NoOfPacks { get; set; }
    }
}
