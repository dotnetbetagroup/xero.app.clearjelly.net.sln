﻿using ClearJelly.Entities;
using System.Collections.Generic;

namespace ClearJelly.ViewModels.Admin
{
    public class ListXeroOrganization
    {
        public List<Xero_User_Org> lst_Xero_Org { get; set; }
    }
}
