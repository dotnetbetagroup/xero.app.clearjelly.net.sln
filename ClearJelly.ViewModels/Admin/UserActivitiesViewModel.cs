﻿using System;

namespace ClearJelly.ViewModels.Admin
{
    public class UserActivitiesViewModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        public string CompanyName { get; set; }
        public bool IsAdmin { get; set; }

        public bool HasAnyActiveSubscription { get; set; }
        public DateTime RegistrationDate { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsStartedAsTrail { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsTrialEnable { get; set; }
        public bool PaidVersion { get; set; }

        public string SuspendedDueTo { get; set; }
    }
}
