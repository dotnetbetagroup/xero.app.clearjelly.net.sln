﻿namespace ClearJelly.ViewModels.Admin
{
    public class UserSuspendViewModel
    {
        public int UserId { get; set; }
        public string SuspendDate { get; set; }
        public string SuspendReason { get; set; }
        public bool AnyAdditionalUser { get; set; }
    }
}
