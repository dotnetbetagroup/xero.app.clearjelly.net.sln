﻿using System.Collections.Generic;

namespace ClearJelly.ViewModels.Admin
{
    public class UserSubscriptionDetailsViewModel
    {
        public string RecurringProfileId { get; set; }
        public string SubscriptionName { get; set; }

        public Dictionary<string, string> OutStandingBalance { get; set; }
    }
}
