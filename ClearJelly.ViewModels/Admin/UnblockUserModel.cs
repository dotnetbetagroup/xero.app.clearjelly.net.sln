﻿namespace ClearJelly.ViewModels.Admin
{
    public class UnblockUserModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool isBlock { get; set; }
        public string blockTime { get; set; }
    }
}
