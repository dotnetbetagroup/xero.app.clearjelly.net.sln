﻿namespace ClearJelly.ViewModels.Admin
{
    public class ActivateUserModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool isActive { get; set; }
    }
}
