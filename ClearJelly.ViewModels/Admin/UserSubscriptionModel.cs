﻿namespace ClearJelly.ViewModels.Admin
{
    public class UserSubscriptionModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string RegistrationDate { get; set; }
        public string CompanyName { get; set; }
        public bool IsAdmin { get; set; }
        public bool HasAnyActiveSubscription { get; set; }
    }
}
