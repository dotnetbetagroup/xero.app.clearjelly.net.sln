﻿namespace ClearJelly.ViewModels.Admin
{
    public class EmailTemplateViewModel
    {
        public string Category { get; set; }
        public int EmailTemplateId { get; set; }
        public bool IsActive { get; set; }
        public string Subject { get; set; }
        public string Name { get; set; }
        public string TemplateContent { get; set; }
        public int? CategoryId { get; set; }
    }
}
