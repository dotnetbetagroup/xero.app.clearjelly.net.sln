﻿using System;

namespace ClearJelly.ViewModels.Admin
{
    public class ModelProcessViewModel
    {
        public int ModelProcessId { get; set; }
        public string OrganizationName { get; set; }
        public Nullable<int> XeroErrorCode { get; set; }
        public string SuccessOrFail { get; set; }
        public string CreatedDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string UserName { get; set; }
        public int? UserId { get; set; }
        public string Status { get; set; }
        public string ProcessType { get; set; }
    }
}
