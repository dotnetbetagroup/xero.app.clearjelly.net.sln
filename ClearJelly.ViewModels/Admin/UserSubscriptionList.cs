﻿using System.Collections.Generic;

namespace ClearJelly.ViewModels.Admin
{
    public class UserSubscriptionList
    {
        public UserSubscriptionList()
        {
            lstUserSubscriptionDetailsViewModel = new List<UserSubscriptionDetailsViewModel>();
        }
        public List<UserSubscriptionDetailsViewModel> lstUserSubscriptionDetailsViewModel { get; set; }
    }

}
