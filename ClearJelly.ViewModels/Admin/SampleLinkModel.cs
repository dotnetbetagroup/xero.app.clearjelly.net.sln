﻿namespace ClearJelly.ViewModels.Admin
{
    public class SampleLinkModel
    {
        public int? SampleLinkId { get; set; }
        public string Title { get; set; }
        public string DownloadLink { get; set; }
        public int SortOrder { get; set; }
    }
}
