﻿namespace ClearJelly.ViewModels.Account
{
    public class FacebookResponseViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string RedirectUrl { get; set; }
    }
}
