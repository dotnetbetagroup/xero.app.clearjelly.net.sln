﻿using System.Collections.Generic;

namespace ClearJelly.ViewModels.Account
{
    public class UserAndSubscriptionViewModel
    {
        public UserAndSubscriptionViewModel()
        {
            ActiveSubscriptions = new List<ActiveSubscriptions>();
        }

        public List<ActiveSubscriptions> ActiveSubscriptions { get; set; }

        public string UserName { get; set; }

        public string ServerName { get; set; }

        public string DatabaseName { get; set; }

        public string Password { get; set; }
    }
}
