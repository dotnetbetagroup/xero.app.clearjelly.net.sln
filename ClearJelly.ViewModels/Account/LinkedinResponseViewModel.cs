﻿namespace ClearJelly.ViewModels.Account
{
    public class LinkedinResponseViewModel
    {
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureUrl { get; set; }
        public string Location { get; set; }
        public string UserId { get; set; }
        public string RedirectUrl { get; set; }
    }
}
