﻿namespace ClearJelly.ViewModels.Account
{
    public class ActiveSubscriptions
    {
        public string SubscriptionName { get; set; }
        public string EndDate { get; set; }

    }
}
