﻿namespace ClearJelly.ViewModels.Account
{
    public class SubscriptionTypeViewModel
    {
        public int SubscriptionTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public decimal MonthlyFee { get; set; }
        public decimal AdditionalUserFee { get; set; }
        public decimal YearlyContractFee { get; set; }
        public int Entities { get; set; }
        public bool IsCurrentSubscription { get; set; }
        public bool IsYearlySubscription { get; set; }
        public int Quantity { get; set; }
    }

}
