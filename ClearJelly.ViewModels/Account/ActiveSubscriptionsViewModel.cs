﻿namespace ClearJelly.ViewModels.Account
{
    public class ActiveSubscriptionsViewModel
    {
        public int CompanySubscriptionId { get; set; }
        public int SubscriptionTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public bool IsYearly { get; set; }
        public decimal AdditionalUsers { get; set; }
        public string StartDate { get; set; }
        public int Quantity { get; set; }
    }
}