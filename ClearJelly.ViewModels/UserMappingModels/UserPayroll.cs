﻿using System;

namespace ClearJelly.ViewModels.UserMappingModels
{
    public class UserPayroll
    {
        public string Id { get; set; }
        public DateTime? Date { get; set; }
        public string PayItemType { get; set; }
        public string Employee { get; set; }
        public string PayItem { get; set; }
        public string EmployeeGroup { get; set; }
        public object Amount { get; set; }
        public string OrgName { get; set; }
        public string TrackingCategory1 { get; set; }
        public string TrackingCategory2 { get; set; }
        public string DisplayDate { get; set; }
    }
}
