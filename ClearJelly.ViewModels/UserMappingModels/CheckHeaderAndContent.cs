﻿using System.Collections.Generic;

namespace ClearJelly.ViewModels.UserMappingModels
{
    public class CheckHeaderAndContent
    {
        public List<UserPayroll> lstUploadCustomDataViewModel { get; set; }
        public bool IsFileHeaderValid { get; set; }
        public int CountSuccessfull { get; set; }
    }
}
