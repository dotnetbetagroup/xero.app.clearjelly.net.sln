﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels.UserMappingModels
{
    public class Receipt
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string Contact_Id { get; set; }
        public string Reference { get; set; }
        public string LineAmountTypes { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? TotalTax { get; set; }
        public decimal? Total { get; set; }
        public string Status { get; set; }
        public string ReceiptNumber { get; set; }
        public DateTime? UpdatedDateUTC { get; set; }
        public bool? HasAttachments { get; set; }
        public string Url { get; set; }
        
        public string ContactId { get; set; }
        //public virtual Contact Contact { get; set; }
        public string XEROUserId { get; set; }
        // public virtual XEROUser XEROUser { get; set; }
        public string OrgName { get; set; }
        public string OrgShortCode { get; set; }
    }
}
