﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels.UserMappingModels
{
    public class UploadCustomDataViewModel
    {
        public UploadCustomDataViewModel()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string EntityName { get; set; }

        public string ClearJellyAccountCode { get; set; }
        public string SourceAccountCode { get; set; }
        public string SourceAccountName { get; set; }

        public string Scenario { get; set; }
        public DateTime? Date { get; set; }

        public string DisplayDate { get; set; }
        public string Comment { get; set; }
        public string Value { get; set; }

        public string TrackingCode1 { get; set; }
        public string TrackingCode2 { get; set; }
    }
}
