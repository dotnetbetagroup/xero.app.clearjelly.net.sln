﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels.UserMappingModels
{
    public class ReceiptLineItem
    {
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitAmount { get; set; }
        public string AccountCode { get; set; }
        public string ItemCode { get; set; }
        public string TaxType { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? LineAmount { get; set; }
        public decimal? DiscountRate { get; set; }
        //public ItemTracking Tracking { get; set; }
        public string LineItemId { get; set; }
    }
}
