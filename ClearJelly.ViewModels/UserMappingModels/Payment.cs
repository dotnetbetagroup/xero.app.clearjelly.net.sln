﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels.UserMappingModels
{
    public class Payment
    {
        public string Id { get; set; }
        public DateTime? Date { get; set; }
        public decimal? CurrencyRate { get; set; }
        public decimal? Amount { get; set; }
        public string Reference { get; set; }
        public bool? IsReconciled { get; set; }
        public string Status { get; set; }
        public string PaymentType { get; set; }
        public DateTime? UpdatedDateUTC { get; set; }
        public string OverpaymentId { get; set; }
        //public virtual Overpayment Overpayment { get; set; }
        public string XEROAccountId { get; set; }
        //public virtual XEROAccount XEROAccount { get; set; }
        public string InvoiceId { get; set; }
        //public virtual Invoice Invoice { get; set; }
        public string CreditNoteId { get; set; }
        //public virtual CreditNote CreditNote { get; set; }
        public string OrgName { get; set; }
        public string OrgShortCode { get; set; }
    }
}
