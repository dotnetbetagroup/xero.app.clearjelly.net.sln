﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels
{
    public class ReceiptLineitemViewModel
    {
        public string ReceiptId { get; set; }
        public string LineitemId { get; set; }
        public string Id { get; set; }
    }
}
