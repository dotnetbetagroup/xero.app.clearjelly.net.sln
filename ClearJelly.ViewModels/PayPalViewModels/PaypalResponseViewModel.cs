﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels.PayPalViewModels
{
    public class PaypalResponseViewModel
    {
        public string TokenID { get; set; }
        public string PayerId { get; set; }
        public decimal Amount { get; set; }
        public string TransactionReference { get; set; }
        public string FailureMessage { get; set; }
    }
}
