﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels.PayPalViewModels
{
    public class PaypalRequestAmountViewModel
    {
        public decimal OrderAmount { get; set; }
        public decimal OrderGStAmt { get; set; }
        public decimal OrderTotal { get; set; }
    }
}
