﻿using System;
using Xero.Api.Core.Model;

namespace ClearJelly.ViewModels.PayPalViewModels
{
    [Serializable]
    public class PaypalPaymentDetailsViewModel
    {
        public int PaymentId { get; set; }
        public int CompanySubscriptionId { get; set; }
        public int PaymentStatus { get; set; }
        public decimal Amount { get; set; }
        public string PayerEmail { get; set; }
        public int PaymentCycle { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? NextPaymentDate { get; set; }
        public DateTime RecurringCreatedDate { get; set; }
        public decimal? OutstandingBalance { get; set; }
        public string PayerId { get; set; }
        public string Currency { get; set; }
        public string RecurringProfileId { get; set; }

        public int SubscriptionId { get; set; }
        public int UserId { get; set; }
        public int AdditionalUserID { get; set; }
        public string RP_Invoice_Id { get; set; }
        public bool IsRecurringProfile { get; set; }
        public int RecurringStatus { get; set; }

        //invoice
        public string Subscription { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string EmailId { get; set; }
        public string ReceiverEmailId { get; set; }
        public string SaasuOrXero { get; set; }
        public OnlineInvoice OnlineInvoiceLink { get; set; }
    }
}
