﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ViewModels
{
   public class AccountMappingViewModel
    {
        public string OrgName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Mapping1 { get; set; }
        public string Mapping2 { get; set; }
        public string Mapping3 { get; set; }
        public string Mapping4 { get; set; }
        public string Mapping5 { get; set; }
        public string Mapping6 { get; set; }
        public string Mapping7 { get; set; }
        public string Mapping8 { get; set; }
        public string Mapping9 { get; set; }
        public string Mapping10 { get; set; }
    }
}
