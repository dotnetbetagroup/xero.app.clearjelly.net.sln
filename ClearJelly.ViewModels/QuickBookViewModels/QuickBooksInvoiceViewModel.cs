﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ViewModels
{
    public class QuickBookInvoiceViewModel
    {
        public int QbId { get; set; }
        public string Id { get; set; }
        public string SyncToken { get; set; }
        public string MetaData { get; set; }
        public string CustomField { get; set; }
        public string AttachableRef { get; set; }
        public string domain { get; set; }
        public EntityStatusEnum status { get; set; }
        public bool statusSpecified { get; set; }
        public bool sparse { get; set; }
        public bool sparseSpecified { get; set; }
        public string NameAndId { get; set; }
        public string Overview { get; set; }
        public string HeaderLite { get; set; }
        public string HeaderFull { get; set; }
        public string TaxFormType { get; set; }
        public string TxnSource { get; set; }
        public string TxnTaxDetail { get; set; }
        public string Line { get; set; }
        public string LinkedTxn { get; set; }
        public string TxnStatus { get; set; }
        public string TaxFormNum { get; set; }
        public string PrivateNote { get; set; }
        public decimal ExchangeRate { get; set; }
        public string DepartmentRef { get; set; }
        public bool TxnDateSpecified { get; set; }
        public DateTime TxnDate { get; set; }
        public string DocNumber { get; set; }
        public bool ExchangeRateSpecified { get; set; }
        public string TransactionLocationType { get; set; }
        public bool PrintStatusSpecified { get; set; }
        public EmailStatusEnum EmailStatus { get; set; }
        public bool EmailStatusSpecified { get; set; }
        public string BillEmail { get; set; }
        public string BillEmailCc { get; set; }
        public string BillEmailBcc { get; set; }
        public string ARAccountRef { get; set; }
        public decimal Balance { get; set; }
        public bool BalanceSpecified { get; set; }
        public decimal HomeBalance { get; set; }
        public bool HomeBalanceSpecified { get; set; }
        public bool FinanceCharge { get; set; }
        public bool FinanceChargeSpecified { get; set; }
        public string PaymentMethodRef { get; set; }
        public string PaymentRefNum { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }
        public bool PaymentTypeSpecified { get; set; }
        public string AnyIntuitObject { get; set; }
        public string DepositToAccountRef { get; set; }
        public string DeliveryInfo { get; set; }
        public decimal DiscountRate { get; set; }
        public bool DiscountRateSpecified { get; set; }
        public decimal DiscountAmt { get; set; }
        public PrintStatusEnum PrintStatus { get; set; }
        public string TemplateRef { get; set; }
        public bool ApplyTaxAfterDiscountSpecified { get; set; }
        public bool ApplyTaxAfterDiscount { get; set; }
        public bool AutoDocNumber { get; set; }
        public bool AutoDocNumberSpecified { get; set; }
        public string CustomerRef { get; set; }
        public string CustomerMemo { get; set; }
        public string BillAddr { get; set; }
        public string ShipAddr { get; set; }
        public string RemitToRef { get; set; }
        public string ClassRef { get; set; }
        public string SalesTermRef { get; set; }
        public DateTime DueDate { get; set; }
        public bool DueDateSpecified { get; set; }
        public bool DiscountAmtSpecified { get; set; }
        public string SalesRepRef { get; set; }
        public string FOB { get; set; }
        public string ShipMethodRef { get; set; }
        public DateTime ShipDate { get; set; }
        public bool ShipDateSpecified { get; set; }
        public string TrackingNum { get; set; }
        public GlobalTaxCalculationEnum GlobalTaxCalculation { get; set; }
        public bool GlobalTaxCalculationSpecified { get; set; }
        public decimal TotalAmt { get; set; }
        public bool TotalAmtSpecified { get; set; }
        public decimal HomeTotalAmt { get; set; }
        public bool HomeTotalAmtSpecified { get; set; }
        public string PONumber { get; set; }
        public string GovtTxnRefIdentifier { get; set; }
        public string callToAction { get; set; }
        public string invoiceStatus { get; set; }
        public bool ECloudStatusTimeStampSpecified { get; set; }
        public DateTime ECloudStatusTimeStamp { get; set; }
        public bool EInvoiceStatusSpecified { get; set; }
        public ETransactionStatusEnum EInvoiceStatus { get; set; }
        public bool AllowOnlineACHPaymentSpecified { get; set; }
        public string invoiceStatusLog { get; set; }
        public bool AllowOnlineACHPayment { get; set; }
        public bool AllowOnlineCreditCardPayment { get; set; }
        public bool AllowOnlinePaymentSpecified { get; set; }
        public bool AllowOnlinePayment { get; set; }
        public bool AllowIPNPaymentSpecified { get; set; }
        public bool AllowIPNPayment { get; set; }
        public bool DepositSpecified { get; set; }
        public decimal Deposit { get; set; }
        public bool AllowOnlineCreditCardPaymentSpecified { get; set; }
        public string InvoiceEx { get; set; }
        public string CurrencyRef { get; set; }
        public string OrgName { get; set; }
    }
}
