﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ViewModels
{
    public class QuickBookItemsViewModel
    {
        public string Id { get; set; }
        public int QbId { get; set; }
        public string SyncToken { get; set; }
        public string MetaData { get; set; }
        public string CustomField { get; set; }
        public string AttachableRef { get; set; }
        public string domain { get; set; }
        public EntityStatusEnum status { get; set; }
        public bool statusSpecified { get; set; }
        public bool sparse { get; set; }
        public bool sparseSpecified { get; set; }
        public string NameAndId { get; set; }
        public string Overview { get; set; }
        public string HeaderLite { get; set; }
        public string HeaderFull { get; set; }
        public string SalesTaxCodeRef { get; set; }
        public string DepositToAccountRef { get; set; }
        public string ManPartNum { get; set; }
        public bool ReorderPointSpecified { get; set; }
        public decimal ReorderPoint { get; set; }
        public bool QtyOnSalesOrderSpecified { get; set; }
        public string PurchaseTaxCodeRef { get; set; }
        public decimal QtyOnSalesOrder { get; set; }
        public decimal QtyOnPurchaseOrder { get; set; }
        public bool QtyOnHandSpecified { get; set; }
        public decimal QtyOnHand { get; set; }
        public bool TrackQtyOnHandSpecified { get; set; }
        public bool TrackQtyOnHand { get; set; }
        public bool AvgCostSpecified { get; set; }
        public bool QtyOnPurchaseOrderSpecified { get; set; }
        public decimal AvgCost { get; set; }
        public DateTime InvStartDate { get; set; }
        public decimal BuildPoint { get; set; }
        public string ServiceType { get; set; }
        public bool ReverseChargeRateSpecified { get; set; }
        public decimal ReverseChargeRate { get; set; }
        public bool AbatementRateSpecified { get; set; }
        public decimal AbatementRate { get; set; }
        public string ItemAssemblyDetail { get; set; }
        public bool InvStartDateSpecified { get; set; }
        public string ItemGroupDetail { get; set; }
        public SpecialItemTypeEnum SpecialItemType { get; set; }
        public bool SpecialItemSpecified { get; set; }
        public bool SpecialItem { get; set; }
        public bool PrintGroupedItemsSpecified { get; set; }
        public bool PrintGroupedItems { get; set; }
        public bool BuildPointSpecified { get; set; }
        public bool SpecialItemTypeSpecified { get; set; }
        public string ItemCategoryType { get; set; }
        public string PrefVendorRef { get; set; }
        public string COGSAccountRef { get; set; }
        public bool SalesTaxIncluded { get; set; }
        public bool TaxableSpecified { get; set; }
        public bool Taxable { get; set; }
        public string FullyQualifiedName { get; set; }
        public bool LevelSpecified { get; set; }
        public int Level { get; set; }
        public string ParentRef { get; set; }
        public bool SalesTaxIncludedSpecified { get; set; }
        public bool SubItem { get; set; }
        public bool ActiveSpecified { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public bool SubItemSpecified { get; set; }
        public string AssetAccountRef { get; set; }
        public bool PercentBased { get; set; }
        public decimal UnitPrice { get; set; }
        public string ExpenseAccountRef { get; set; }
        public bool PurchaseCostSpecified { get; set; }
        public decimal PurchaseCost { get; set; }
        public bool PurchaseTaxIncludedSpecified { get; set; }
        public bool PurchaseTaxIncluded { get; set; }
        public string PurchaseDesc { get; set; }
        public bool PercentBasedSpecified { get; set; }
        public string IncomeAccountRef { get; set; }
        public string PaymentMethodRef { get; set; }
        public bool TypeSpecified { get; set; }
        public ItemTypeEnum Type { get; set; }
        public bool RatePercentSpecified { get; set; }
        public decimal RatePercent { get; set; }
        public bool UnitPriceSpecified { get; set; }
        public string UOMSetRef { get; set; }
        public string ItemEx { get; set; }
        public string OrgName { get; set; }
    }
}
