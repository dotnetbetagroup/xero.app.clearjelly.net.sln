﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ViewModels
{
    public class QuickBookAccountViewModel
    {
        public int QbId { get; set; }
        public string Id { get; set; }
        public string SyncToken { get; set; }
        public string MetaData { get; set; }
        public string CustomField { get; set; }
        public string AttachableRef { get; set; }
        public string domain { get; set; }
        public EntityStatusEnum status { get; set; }
        public bool statusSpecified { get; set; }
        public bool sparse { get; set; }
        public bool sparseSpecified { get; set; }
        public string NameAndId { get; set; }
        public string Overview { get; set; }
        public string HeaderLite { get; set; }
        public string HeaderFull { get; set; }
        public bool OpeningBalanceSpecified { get; set; }
        public DateTime OpeningBalanceDate { get; set; }
        public bool OpeningBalanceDateSpecified { get; set; }
        public decimal CurrentBalance { get; set; }
        public bool CurrentBalanceSpecified { get; set; }
        public decimal CurrentBalanceWithSubAccounts { get; set; }
        public decimal OpeningBalance { get; set; }
        public bool CurrentBalanceWithSubAccountsSpecified { get; set; }
        public bool TaxAccount { get; set; }
        public bool TaxAccountSpecified { get; set; }
        public string TaxCodeRef { get; set; }
        public bool OnlineBankingEnabled { get; set; }
        public bool OnlineBankingEnabledSpecified { get; set; }
        public string FIName { get; set; }
        public string CurrencyRef { get; set; }
        public string BankNum { get; set; }
        public string AcctNumExtn { get; set; }
        public string AcctNum { get; set; }
        public string Name { get; set; }
        public bool SubAccount { get; set; }
        public bool SubAccountSpecified { get; set; }
        public string ParentRef { get; set; }
        public string Description { get; set; }
        public string FullyQualifiedName { get; set; }
        public string AccountAlias { get; set; }
        public string TxnLocationType { get; set; }
        public bool Active { get; set; }
        public bool ActiveSpecified { get; set; }
        public AccountClassificationEnum Classification { get; set; }
        public bool ClassificationSpecified { get; set; }
        public AccountTypeEnum AccountType { get; set; }
        public bool AccountTypeSpecified { get; set; }
        public string AccountSubType { get; set; }
        public string JournalCodeRef { get; set; }
        public string AccountEx { get; set; }
        public string OrgName { get; set; }
    }
}
