﻿using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ViewModels
{
    public class QuickBookJournalViewModel
    {
        public int QbId { get; set; }
        public bool Adjustment { get; set; }
        public bool AdjustmentSpecified { get; set; }
        public bool HomeCurrencyAdjustment { get; set; }
        public bool HomeCurrencyAdjustmentSpecified { get; set; }
        public bool EnteredInHomeCurrency { get; set; }
        public bool EnteredInHomeCurrencySpecified { get; set; }
        public decimal TotalAmt { get; set; }
        public bool TotalAmtSpecified { get; set; }
        public decimal HomeTotalAmt { get; set; }
        public bool HomeTotalAmtSpecified { get; set; }
        public string JournalEntryEx { get; set; }
        public string TaxFormType { get; set; }
        public string CurrencyRef { get; set; }
        public string TxnSource { get; set; }
        public string TxnTaxDetail { get; set; }
        public string Line { get; set; }
        public string LinkedTxn { get; set; }
        public string TxnStatus { get; set; }
        public string TaxFormNum { get; set; }
        public string PrivateNote { get; set; }
        public decimal ExchangeRate { get; set; }
        public string DepartmentRef { get; set; }
        public bool TxnDateSpecified { get; set; }
        public DateTime TxnDate { get; set; }
        public string DocNumber { get; set; }
        public bool ExchangeRateSpecified { get; set; }
        public string TransactionLocationType { get; set; }
        public string Id { get; set; }
        public string SyncToken { get; set; }
        public string MetaData { get; set; }
        public string CustomField { get; set; }
        public string AttachableRef { get; set; }
        public string domain { get; set; }
        public EntityStatusEnum status { get; set; }
        public bool statusSpecified { get; set; }
        public bool sparse { get; set; }
        public bool sparseSpecified { get; set; }
        public string NameAndId { get; set; }
        public string Overview { get; set; }
        public string HeaderLite { get; set; }
        public string HeaderFull { get; set; }
        public string OrgName { get; set; }
    }
}
