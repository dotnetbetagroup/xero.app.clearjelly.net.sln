﻿namespace ClearJelly.ViewModels
{
    /// <summary>
    /// Summary description for AdditionUserDetailViewModel
    /// </summary>
    public class AdditionUserDetailViewModel
    {

        public decimal SubsctiptionOrderTotal { get; set; }
        public decimal SubsctiptionGstOrderTotal { get; set; }
        public decimal AdditionUserPrice { get; set; }
        public decimal AdditionUserGst { get; set; }
        public decimal SubscriptionGrossAmount { get; set; }
        public decimal AdditionalUserGrossAmount { get; set; }
    }
}