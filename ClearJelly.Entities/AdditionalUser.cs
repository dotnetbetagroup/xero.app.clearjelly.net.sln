﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("AdditionalUsers")]
    public partial class AdditionalUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AdditionalUser()
        {
            this.InstantPayments = new HashSet<InstantPayment>();
        }
        [Key]
        public int AdditionalUserId { get; set; }
        public Nullable<int> UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public Nullable<int> CompanySubscriptionId { get; set; }
        [ForeignKey("CompanySubscriptionId")]
        public virtual CompanySubscription CompanySubscription { get; set; }

        public Nullable<System.DateTime> JoiningDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Required]
        public virtual ICollection<InstantPayment> InstantPayments { get; set; }
    }
}
