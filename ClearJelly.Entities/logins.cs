﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("logins")]

    public partial class logins
    {
        [Key]
        public int ID { get; set; }
        public string username { get; set; }
        public string Company { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public Guid code { get; set; }

        public logins()
        {
            code = new Guid();
        }
    }
}
