﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("OrganisationDetails")]

    public partial class OrganisationDetail
    {
        [Key]
        public int OrgDetailId { get; set; }
        [Required]
        public string APIKey { get; set; }
        public string BaseCurrency { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [Required]
        public string OrgName { get; set; }
        [Required]
        public bool OrgStatus { get; set; }
        [Required]
        public string OrgType { get; set; }
        public string RegistrationNumber { get; set; }
        public string SalesTaxBasisType { get; set; }
        public string SalestaxPeriod { get; set; }
        public string OrgShortCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string AddressType { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public int CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
    }
}
