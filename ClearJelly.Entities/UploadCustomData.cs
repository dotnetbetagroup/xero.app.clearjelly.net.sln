﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("UploadCustomData")]

    public partial class UploadCustomData
    {
        public string EntityName { get; set; }
        public string ClearJellyAccountCode { get; set; }
        public string SourceAccountCode { get; set; }
        public string Scenario { get; set; }
        public DateTime? Date { get; set; }
        public string Comment { get; set; }
        public string Value { get; set; }
    }
}
