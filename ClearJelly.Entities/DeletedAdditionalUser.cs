﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("DeletedAdditionalUsers")]
    public partial class DeletedAdditionalUser
    {
        [Key]
        public int DeletedAdditionalUserId { get; set; }
        public Nullable<int> DeletedUserId { get; set; }
        [ForeignKey("DeletedUserId")]
        public virtual DeletedUser DeletedUser { get; set; }

        public Nullable<int> DeletedCompanySubscriptionId { get; set; }
        [ForeignKey("DeletedCompanySubscriptionId")]
        public virtual DeletedCompanySubscription DeletedCompanySubscription { get; set; }

        public Nullable<System.DateTime> JoiningDate { get; set; }
    }
}
