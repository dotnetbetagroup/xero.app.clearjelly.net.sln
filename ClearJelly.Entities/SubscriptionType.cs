﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("SubscriptionType")]

    public partial class SubscriptionType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubscriptionType()
        {
            this.CompanySubscriptions = new HashSet<CompanySubscription>();
            this.DeletedCompanySubscriptions = new HashSet<DeletedCompanySubscription>();
        }
        [Key]
        public int SubscriptionTypeId { get; set; }
        [Required]
        public string TypeName { get; set; }
        public string Description { get; set; }
        [Required]
        public decimal MonthlyFee { get; set; }
        [Required]
        public decimal AdditionalUserFee { get; set; }
        [Required]
        public decimal YearlyContractFee { get; set; }
        [Required]
        public int Entities { get; set; }
        [Required]
        public bool IsActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanySubscription> CompanySubscriptions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedCompanySubscription> DeletedCompanySubscriptions { get; set; }
    }
}
