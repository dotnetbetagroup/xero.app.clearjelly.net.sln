﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("Calendar")]
    public partial class Calendar
    {
        public DateTime? MyProperty { get; set; }
        public int? DayOfYear { get; set; }
        public int? Day { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string DayOfWeek { get; set; }
        public string DayOfWeekAbr { get; set; }
        public int? WeekOfYear { get; set; }
        public int? Quarter { get; set; }
        public string MonthName { get; set; }
        public string MonthNameAbr { get; set; }
        public DateTime? MonthEndDate { get; set; }
        public DateTime? YearEndDate { get; set; }
        public int? FiscalPeriod { get; set; }
        public int? FiscalYearEnd { get; set; }
        public int? FiscalQuarter { get; set; }

    }
}
