﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("PaypalSyncDataHistory")]

    public partial class PaypalSyncDataHistory
    {
        [Key]
        public System.Guid HistoryId { get; set; }
        [Required]
        public string RecurringProfileId { get; set; }
        [Required]
        public int CompanySubscriptionId { get; set; }
        [ForeignKey("CompanySubscriptionId")]
        public virtual CompanySubscription CompanySubscription { get; set; }

        [Required]
        public int Status { get; set; }
        [Required]
        public decimal LastPaymentAmount { get; set; }
        [Required]
        public int NoOfCycles { get; set; }
        [Required]
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<System.DateTime> LastSyncOn { get; set; }

    }
}
