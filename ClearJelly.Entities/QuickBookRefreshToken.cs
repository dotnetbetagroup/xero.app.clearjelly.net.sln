﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel.Entities
{
    [Table("QuickBookRefreshToken")]
    public class QuickBookRefreshToken
    {
        [Key]
        public string Id { get; set; }
        public string RealmId { get; set; }
        public int UserId { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
