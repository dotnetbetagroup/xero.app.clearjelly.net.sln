﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("Configuration")]
    public partial class Configuration
    {
        [Key]
        public int ConfigurationId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
