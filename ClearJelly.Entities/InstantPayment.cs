﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("InstantPayments")]

    public partial class InstantPayment
    {
        [Key]
        public int InstantPaymentId { get; set; }
        [Required]
        public int PaymentStatus { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public string PayerEmailId { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        [Required]
        public string PayerId { get; set; }
        public string Currency { get; set; }
        [Required]
        public int AdditionalUserId { get; set; }
        [ForeignKey("AdditionalUserId")]
        public virtual AdditionalUser AdditionalUser { get; set; }
    }
}
