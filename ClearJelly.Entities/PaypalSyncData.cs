﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClearJelly.Entities
{
    [Table("PaypalSyncData")]
    public partial class PaypalSyncData
    {
        [Key]
        public int CompanyHistory { get; set; }
        [Required]
        public int CompanySubscriptionId { get; set; }
        [ForeignKey("CompanySubscriptionId")]
        public virtual CompanySubscription CompanySubscription { get; set; }

        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsPaid { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        [Required]
        public int NoOfCycles { get; set; }
        [Required]
        public int Status { get; set; }

    }
}
