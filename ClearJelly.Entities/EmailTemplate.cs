﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("EmailTemplate")]
    public partial class EmailTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EmailTemplate()
        {
            this.EmailAttachments = new HashSet<EmailAttachment>();
            this.EmailParameters = new HashSet<EmailParameter>();
        }
        [Key]
        public int EmailTemplateId { get; set; }
        [Required]
        public string Name { get; set; }
        public string TemplateContent { get; set; }
        public Nullable<int> EmailCategoryId { get; set; }
        [ForeignKey("EmailCategoryId")]
        public virtual EmailCategory EmailCategory { get; set; }

        [Required]
        public bool IsActive { get; set; }
        [Required]
        public string Subject { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmailAttachment> EmailAttachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmailParameter> EmailParameters { get; set; }
    }
}
