﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("DeletedUser")]
    public partial class DeletedUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeletedUser()
        {
            this.DeletedAdditionalUsers = new HashSet<DeletedAdditionalUser>();
            this.DeletedUserOrganizations = new HashSet<DeletedUserOrganization>();
        }
        [Key]
        public int DeletedUserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public short LoginType { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        [Required]
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Organization { get; set; }
        public string ProfilePicture { get; set; }
        public string FBUserId { get; set; }
        public string LinkedInUserId { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        public string PaypalPayerId { get; set; }
        public string PaypalToken { get; set; }
        [Required]
        public System.DateTime RegistrationDate { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public string PwdResetCode { get; set; }
        public string ActivationCode { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public int DeletedCompanyId { get; set; }
        [ForeignKey("DeletedCompanyId")]
        public virtual DeletedCompany DeletedCompany { get; set; }

        public Nullable<System.DateTime> LastloginFailed { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedAdditionalUser> DeletedAdditionalUsers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedUserOrganization> DeletedUserOrganizations { get; set; }
    }
}
