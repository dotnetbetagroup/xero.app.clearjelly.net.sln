﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("xero_request_queue")]

    public partial class xero_request_queue
    {
        [Key]
        public string userID { get; set; }
        public string OrgShortCode { get; set; }
        public DateTime? CallDate { get; set; }
        public string RequestStatus { get; set; }
        public string RequestType { get; set; }
        public string ItemiD { get; set; }
        public DateTime? DueDate { get; set; }
    }
}
