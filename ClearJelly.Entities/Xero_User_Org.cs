﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("Xero_User_Org")]

    public partial class Xero_User_Org
    {
        [Key, Column(Order = 1)]
        public int CompanyID { get; set; }
        [Key, Column(Order = 2)]
        public string OrgShortCode { get; set; }
        public string OrgName { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
