﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("EmailParameter")]
    public partial class EmailParameter
    {
        [Key]
        public int EmailParameterId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public Nullable<int> EmailTemplateId { get; set; }
        [ForeignKey("EmailTemplateId")]
        public virtual EmailTemplate EmailTemplate { get; set; }
    }
}
