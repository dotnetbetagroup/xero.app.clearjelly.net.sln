﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("SuspendedUser")]

    public partial class SuspendedUser
    {
        [Key]
        public int SuspendedUserId { get; set; }
        [Required]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public Nullable<System.DateTime> SuspendedDate { get; set; }
        public string Reason { get; set; }

    }
}
