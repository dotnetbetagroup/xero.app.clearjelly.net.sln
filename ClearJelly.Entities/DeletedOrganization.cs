﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("DeletedOrganizations")]
    public partial class DeletedOrganization
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        [Required]
        public string OrgShortCode { get; set; }
        [Required]
        public string OrgName { get; set; }
        [Required]
        public System.DateTime DeletedDate { get; set; }
    }
}
