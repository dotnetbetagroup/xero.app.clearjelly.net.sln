﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("LogTableArchive")]

    public partial class LogTableArchive
    {
        [Key]
        public int LogId { get; set; }
        [Required]
        public System.DateTime LogDate { get; set; }
        [Required]
        public string EventLevel { get; set; }
        [Required]
        public string LoggerName { get; set; }
        public string Url { get; set; }
        public string Code { get; set; }
        [Required]
        public string LogMessage { get; set; }
    }
}
