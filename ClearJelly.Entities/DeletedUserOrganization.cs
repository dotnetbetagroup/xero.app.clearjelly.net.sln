﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("DeletedUserOrganization")]
    public partial class DeletedUserOrganization
    {
        [Key]
        public int DeletedUserOrganizationId { get; set; }
        [Required]
        public int DeletedUserId { get; set; }
        [ForeignKey("DeletedUserId")]
        public virtual DeletedUser DeletedUser { get; set; }
        [Required]
        public string OrgShortCode { get; set; }
        [Required]
        public string OrgName { get; set; }
        [Required]
        public System.DateTime DeletedDate { get; set; }
    }
}
