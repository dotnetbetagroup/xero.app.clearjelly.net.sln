﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using ClearJelly.Entities;
using EntityModel.Entities;
using ClearJelly.Configuration;

namespace ClearJelly.Entities
{
    public  class ClearJellyEntities : DbContext
    {
        public ClearJellyEntities()
            : base(ConfigSection.ClearJellyEntities)
        {
        }

        public DbSet<AbcClub> AbcClubs { get; set; }
        public DbSet<AccessToken> AccessTokens { get; set; }
        public DbSet<AdditionalUser> AdditionalUsers { get; set; }
        public DbSet<AdminUser> AdminUsers { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanySubscription> CompanySubscriptions { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<DeletedAdditionalUser> DeletedAdditionalUsers { get; set; }
        public DbSet<DeletedCompany> DeletedCompanies { get; set; }
        public DbSet<DeletedCompanySubscription> DeletedCompanySubscriptions { get; set; }
        public DbSet<DeletedOrganization> DeletedOrganizations { get; set; }
        public DbSet<DeletedUser> DeletedUsers { get; set; }
        public DbSet<DeletedUserOrganization> DeletedUserOrganizations { get; set; }
        public DbSet<EmailAttachment> EmailAttachments { get; set; }
        public DbSet<EmailCategory> EmailCategories { get; set; }
        public DbSet<EmailParameter> EmailParameters { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<InstantPayment> InstantPayments { get; set; }
        public DbSet<LogTable> LogTables { get; set; }
        public DbSet<QuickBookRefreshToken> QuickBookRefreshTokens { get; set; }
        public DbSet<QuickBookCompany> QuickBookCompanies { get; set; }        
        public DbSet<RecurringPayment> RecurringPayments { get; set; }
        public DbSet<RequestToken> RequestTokens { get; set; }
        public DbSet<SampleLink> SampleLinks { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<SubscriptionType> SubscriptionTypes { get; set; }
        public DbSet<SuspendedUser> SuspendedUsers { get; set; }
        public DbSet<token> tokens { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserSafeIP> UserSafeIPs { get; set; }
        public DbSet<Xero_User_Org> Xero_User_Org { get; set; }
        public DbSet<Xero_User_Org_new> Xero_User_Org_new { get; set; }
        public DbSet<copyEmailTemplate> copyEmailTemplates { get; set; }
        public DbSet<LogTableArchive> LogTableArchives { get; set; }
        public DbSet<OrganisationDetail> OrganisationDetails { get; set; }
        public DbSet<ModelProcess> ModelProcesses { get; set; }
        public DbSet<PaypalSyncDataHistory> PaypalSyncDataHistories { get; set; }
        public DbSet<PaypalSyncData> PaypalSyncDatas { get; set; }

        public virtual ObjectResult<UpdateUserPassword_Result> UpdateUserPassword(string user, string oldPassword, string newPassword)
        {
            var userParameter = user != null ?
                new ObjectParameter("user", user) :
                new ObjectParameter("user", typeof(string));

            var oldPasswordParameter = oldPassword != null ?
                new ObjectParameter("oldPassword", oldPassword) :
                new ObjectParameter("oldPassword", typeof(string));

            var newPasswordParameter = newPassword != null ?
                new ObjectParameter("newPassword", newPassword) :
                new ObjectParameter("newPassword", typeof(string));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<UpdateUserPassword_Result>("UpdateUserPassword", userParameter, oldPasswordParameter, newPasswordParameter);
        }

        public virtual ObjectResult<DeleteFromUserCompany_Result> DeleteFromUserCompany(string companyName, string orgShortCode, string orgName)
        {
            var companyNameParameter = companyName != null ?
                new ObjectParameter("companyName", companyName) :
                new ObjectParameter("companyName", typeof(string));

            var orgShortCodeParameter = orgShortCode != null ?
                new ObjectParameter("orgShortCode", orgShortCode) :
                new ObjectParameter("orgShortCode", typeof(string));

            var orgNameParameter = orgName != null ?
                new ObjectParameter("orgName", orgName) :
                new ObjectParameter("orgName", typeof(string));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<DeleteFromUserCompany_Result>("DeleteFromUserCompany", companyNameParameter, orgShortCodeParameter, orgNameParameter);
        }
    }
}
