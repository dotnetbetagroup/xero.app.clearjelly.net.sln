﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("ConsumerSessions")]
    public partial class ConsumerSessions
    {
        public string UserID { get; set; }
        public string SessionCode { get; set; }
    }
}
