﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("API_Sessions")]
    public partial class API_Sessions
    {
        [Key]
        public string Userid { get; set; }
        public string ConsumerSession { get; set; }
        public string AccessToken { get; set; }
        public string accessToken_oauth_token { get; set; }
        public string accessToken_oauth_token_secret { get; set; }
        public DateTime? CreatedDateUTC { get; set; }
        public int ExperiesIN { get; set; }
        public DateTime? ExpiryDateUTC { get; set; }
        public int SessionExpiresIN { get; set; }
        public DateTime? SessionExpiryDateUTC { get; set; }
        public string SessionHandle { get; set; }
        public TimeSpan? SessionTimeSpan { get; set; }
        public TimeSpan? TokenTimeSpan { get; set; }
    }
}
