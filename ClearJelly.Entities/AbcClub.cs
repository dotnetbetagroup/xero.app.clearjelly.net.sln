﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("AbcClub")]
    public class AbcClub
    {
            [Key, Column(Order = 1)]
            public string Id { get; set; }
            public int ClubNumber { get; set; }
            public string ClubName { get; set; }
            public int UserId { get; set; }
            public bool isActive { get; set; }
     
    }
}
