﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("xero_api_calls")]

    public partial class xero_api_calls
    {
        [Key]
        public string OrgShortCode { get; set; }
        public DateTime? CallDate { get; set; }
        public int? CallCount { get; set; }
    }
}
