﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("CompanySubscription")]
    public partial class CompanySubscription
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanySubscription()
        {
            this.AdditionalUsers1 = new HashSet<AdditionalUser>();
            this.RecurringPayments = new HashSet<RecurringPayment>();
            this.PaypalSyncDataHistories = new HashSet<PaypalSyncDataHistory>();
            this.PaypalSyncDatas = new HashSet<PaypalSyncData>();
        }
        [Key]
        public int CompanySubscriptionId { get; set; }

        public int CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public int SubscriptionTypeId { get; set; }
        [ForeignKey("SubscriptionTypeId")]
        public virtual SubscriptionType SubscriptionType { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string RecurringProfileId { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int AdditionalUsers { get; set; }
        [Required]
        public bool IsYearlySubscription { get; set; }
        [Required]
        public bool StartedAsTrial { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalUser> AdditionalUsers1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaypalSyncDataHistory> PaypalSyncDataHistories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaypalSyncData> PaypalSyncDatas { get; set; }
    }
}
