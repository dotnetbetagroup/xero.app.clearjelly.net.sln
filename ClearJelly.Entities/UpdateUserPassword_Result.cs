﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    public partial class UpdateUserPassword_Result
    {
        public int isPasswordUpdateSuccess { get; set; }
    }
}
