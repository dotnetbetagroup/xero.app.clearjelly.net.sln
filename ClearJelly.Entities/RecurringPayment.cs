﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("RecurringPayments")]

    public partial class RecurringPayment
    {
        [Key]
        public int RecurringPaymentId { get; set; }
        [Required]
        public int CompanySubscriptionId { get; set; }
        [ForeignKey("CompanySubscriptionId")]
        public virtual CompanySubscription CompanySubscription { get; set; }

        [Required]
        public int PaymentStatus { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public string PayerEmail { get; set; }
        [Required]
        public int PaymentCycle { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<System.DateTime> NextPaymentDate { get; set; }
        [Required]
        public System.DateTime RecurringCreatedDate { get; set; }
        public Nullable<decimal> OutstandingBalance { get; set; }
        public string PayerId { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string RecurringProfileId { get; set; }
        [Required]
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [Required]
        public int RecurringStatus { get; set; }
    }
}
