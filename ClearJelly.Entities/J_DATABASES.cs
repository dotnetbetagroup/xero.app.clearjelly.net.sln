﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("J_DATABASES")]

    public partial class J_DATABASES
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string isNormalDatabase { get; set; }
        public string isSystemDatabase { get; set; }
        public string isUserInfoDatabase { get; set; }
    }
}
