﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("SampleLink")]

    public partial class SampleLink
    {
        [Key]
        public int SampleLinkId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string DownloadLink { get; set; }
        [Required]
        public int SortOrder { get; set; }
    }
}
