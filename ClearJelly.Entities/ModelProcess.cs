﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("ModelProcess")]

    public partial class ModelProcess
    {
        [Key]
        public int ModelProcessId { get; set; }
        public Nullable<int> UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int Status { get; set; }
        [Required]
        public int ProcessType { get; set; }
        public string OrganizationName { get; set; }
        [Required]
        public bool IsMessageShown { get; set; }
        public Nullable<int> XeroErrorCode { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }

    }
}
