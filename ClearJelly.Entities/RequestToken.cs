﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("RequestTokens")]

    public partial class RequestToken
    {
        [Key]
        public string UserName { get; set; }
        public string AccessToken { get; set; }
        public string CallbackUrl { get; set; }
        public string Realm { get; set; }
        public string Token { get; set; }
        public string TokenSecret { get; set; }
        public string Verifier { get; set; }
    }
}
