﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel.Entities
{
    [Table("QuickBookCompany")]
    public partial class QuickBookCompany
    {
        [Key, Column(Order = 1)]
        public string Id { get; set; }
        public string RealmId { get; set; }
        public string OrgName { get; set; }
        public int UserId { get; set; }
        public bool isActive { get; set; }
    }
}
