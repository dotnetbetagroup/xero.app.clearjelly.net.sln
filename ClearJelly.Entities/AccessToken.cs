﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("AccessTokens")]
    public partial class AccessToken
    {
        [Key]
        public string UserName { get; set; }
        public string ConsumerKey { get; set; }
        public string CanRefresh { get; set; }
        public string CreatedDateUtc { get; set; }
        public string ExpiresIn { get; set; }
        public string ExpiryDateUtc { get; set; }
        public string Realm { get; set; }
        public string SessionExpiresIn { get; set; }
        public string SessionExpiryDateUtc { get; set; }
        public string SessionHandle { get; set; }
        public string SessionTimespan { get; set; }
        public string Token { get; set; }
        public string TokenSecret { get; set; }
        public string TokenTimespan { get; set; }
        public string organisation { get; set; }
    }
}
