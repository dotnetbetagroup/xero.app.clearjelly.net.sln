﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            this.AdditionalUsers = new HashSet<AdditionalUser>();
            this.DeletedOrganizations = new HashSet<DeletedOrganization>();
            this.SuspendedUsers = new HashSet<SuspendedUser>();
            this.UserSafeIPs = new HashSet<UserSafeIP>();
            this.ModelProcesses = new HashSet<ModelProcess>();
        }
        [Key]
        public int UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public short LoginType { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        [Required]
        public string Email { get; set; }
        public string UserEmail { get; set; }
        public string Mobile { get; set; }
        public string Organization { get; set; }
        public string ProfilePicture { get; set; }
        public string FBUserId { get; set; }
        public string LinkedInUserId { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        public string PaypalPayerId { get; set; }
        public string PaypalToken { get; set; }
        [Required]
        public System.DateTime RegistrationDate { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public string PwdResetCode { get; set; }
        public string ActivationCode { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public int CompanyId { get; set; }
        public Nullable<System.DateTime> LastloginFailed { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public string ChosenService { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalUser> AdditionalUsers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedOrganization> DeletedOrganizations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SuspendedUser> SuspendedUsers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserSafeIP> UserSafeIPs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModelProcess> ModelProcesses { get; set; }
    }
}
