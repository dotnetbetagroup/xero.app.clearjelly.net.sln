﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("Settings")]

    public partial class Setting
    {
        [Key]
        public int SettingId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    }
}
