﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("CounryData$")]
    public partial class CounryData_
    {
        public string Name { get; set; }
        public float GSTRate { get; set; }
    }
}
