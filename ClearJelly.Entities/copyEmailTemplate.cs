﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("copyEmailTemplate")]
    public partial class copyEmailTemplate
    {
        [Key]
        public int EmailTemplateId { get; set; }
        [Required]
        public string Name { get; set; }
        public string TemplateContent { get; set; }
        public Nullable<int> EmailCategoryId { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public string Subject { get; set; }
    }
}
