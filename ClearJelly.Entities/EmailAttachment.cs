﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("EmailAttachment")]
    public partial class EmailAttachment
    {
        [Key]
        public int EmailAttachmentId { get; set; }
        [Required]
        public string FileName { get; set; }
        [Required]
        public string AttachmentName { get; set; }
        public Nullable<int> EmailTemplateId { get; set; }
        [ForeignKey("EmailTemplateId")]
        public virtual EmailTemplate EmailTemplate { get; set; }
    }
}
