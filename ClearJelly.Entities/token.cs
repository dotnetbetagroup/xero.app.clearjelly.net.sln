﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("tokens")]

    public partial class token
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public string OrganisationId { get; set; }
        [Required]
        public string ConsumerKey { get; set; }
        [Required]
        public string ConsumerSecret { get; set; }
        [Required]
        public string TokenKey { get; set; }
        [Required]
        public string TokenSecret { get; set; }
        public Nullable<System.DateTime> ExpiresAt { get; set; }
        public string Session { get; set; }
        public Nullable<System.DateTime> SessionExpiresAt { get; set; }
    }
}
