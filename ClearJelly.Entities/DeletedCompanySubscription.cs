﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities
{
    [Table("DeletedCompanySubscription")]
    public partial class DeletedCompanySubscription
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeletedCompanySubscription()
        {
            this.DeletedAdditionalUsers = new HashSet<DeletedAdditionalUser>();
        }
        [Key]
        public int DeletedCompanySubscriptionId { get; set; }
        [Required]
        public int DeletedCompanyId { get; set; }
        [ForeignKey("DeletedCompanyId")]
        public virtual DeletedCompany DeletedCompany { get; set; }

        [Required]
        public int SubscriptionTypeId { get; set; }
        [ForeignKey("SubscriptionTypeId")]
        public virtual SubscriptionType SubscriptionType { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string RecurringProfileId { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int AdditionalUsers { get; set; }
        [Required]
        public bool IsYearlySubscription { get; set; }
        [Required]
        public bool StartedAsTrial { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedAdditionalUser> DeletedAdditionalUsers { get; set; }
    }
}
