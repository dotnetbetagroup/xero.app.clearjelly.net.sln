﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities.Enums
{
    public enum ModelType
    {
        [Description("Create")]
        Create = 1,
        [Description("Update")]
        Update = 2
    }
}
