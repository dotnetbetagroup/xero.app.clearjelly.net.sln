﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities.Enums
{
    public enum QBErrorCodes
    {
        [Description("The load for your QuickBook data has finished successfully.")]
        Ok = 200,

        [Description("There is some problem while connecting to QuickBook please contact Admin.")]
        BadRequest = 400,

        [Description("Error while connecting QuickBook : Organization is unauthorized or not available please contact Admin.")]
        Unauthorized = 401,

        [Description("Error while connecting QuickBook : Some error occurred please contact Admin.")]
        //[Description("The client SSL certificate was not valid. This indicates the Xero Entrust certificate required for partner API applications is not being supplied in the connection, or has expired")]
        Forbidden = 403,

        [Description("Error while connecting QuickBook : The feature you are trying is not available in QuickBook please contact Admin.")]
        NotFound = 404,

        [Description("Error while connecting QuickBook : Some internal error occurred please contact Admin.")]
        InternalError = 500,

        [Description("Error from QuickBook : The feature you are trying is not available in xero please contact Admin.")]
        NotImplemented = 501,

        [Description("Error from QuickBook : The feature you are trying is not available in xero please contact Admin.")]
        NotAvailable = 503,

        [Description("Error from QuickBook : Token is expired or not active please contact Admin.")]
        GeneralError = 555,

    }
}
