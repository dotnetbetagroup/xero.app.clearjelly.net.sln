﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities.Enums
{
    public enum StatusEnum
    {
        [Description("Running")]
        Running = 1,
        [Description("Success")]
        Success = 2,
        [Description("Fail")]
        Fail = 3,
    }
}
