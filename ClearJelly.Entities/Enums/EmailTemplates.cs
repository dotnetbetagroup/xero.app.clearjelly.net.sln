﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities.Enums
{
    public enum EmailTemplates
    {
        [Description("Registration Success Email")]
        RegistrationSuccessEmail = 1,
        [Description("New User Registered Email to Admin")]
        RegistrationSuccessEmailToAdmin = 2,
        [Description("Forgot Password Email")]
        ForgotPasswordEmail = 3,
        [Description("Clearjelly model not built")]
        ClearjellyModelBuiltFailed = 4,
        [Description("Clearjelly model has been built")]
        ClearjellyModelBuiltSuccess = 5,
        [Description("SendMailOn Xerp Api Exception")]
        SendMailOnApiException = 6,
        [Description("Quick Guide")]
        quickGuide = 7,
        [Description("PaymentSuccessEmail")]
        PaymentSuccessEmail = 9,
        [Description("Error Xero Email")]
        ErrorXeroEmail = 10,
        [Description("Database Error")]
        DatabaseError = 11,
        [Description("Jedox Error")]
        JedoxError = 17,
        [Description("Suspend User Max Failure")]
        SuspendUserMaxFailure = 19,
        [Description("Payment Failure")]
        PaymentFailure = 20,
        [Description("Update DB successfully")]
        UpdateDBSuccessEmail = 23,
        [Description("Payment Failure")]
        UpdateDBErrorEmail = 24
    }
}
