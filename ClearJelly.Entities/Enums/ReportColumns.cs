﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities.Enums
{
    public enum ReportColumns
    {
        Date = 0,
        TransactionType = 1,
        Num = 2,
        Adj = 3,
        CreateDate = 4,
        CreatedBy = 5,
        LastModified = 6,
        LastModifiedBy = 7,
        Name = 8,
        Customer = 9,
        Vendor = 10,
        Employee = 11,
        ProductService = 12,
        MemoDescription = 13,
        Qty = 14,
        Rate = 15,
        Account = 16,
        Split = 17,
        InvoiceDate = 18,
        ARPaid = 19,
        APPaid = 20,
        Clr = 21,
        CheckPrinted = 22,
        OpenBalance = 23,
        Amount = 24,
        Balance = 25       
    }
}
