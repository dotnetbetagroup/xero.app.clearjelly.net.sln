﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Entities.Enums
{
    public enum UserStatus
    {
        [Description("Active")]
        Active = 1,
        [Description("Suspended")]
        Suspended = 2,
        [Description("Cancelled")]
        Cancelled = 3,
        [Description("Expired")]
        Expired = 4
    }
}
