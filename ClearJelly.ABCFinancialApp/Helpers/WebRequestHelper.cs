﻿using ClearJelly.ABCFinancialApp.Providers;
using System.Net;
using ClearJelly.Configuration;
using ClearJelly.ABCFinancialApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Web.Helpers;
using Newtonsoft.Json.Linq;
using ClearJelly.ABCFinancialApp.Models.Members.Members;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using NLog;

namespace ClearJelly.ABCFinancialApp.Helpers
{
    public static class WebRequestHelper
    {
        private static readonly string _appId = ConfigSection.ABCAppId;
        private static readonly string _appKey = ConfigSection.ABCAppKey;
        private static readonly string _apiBaseUrl = ConfigSection.ABCBaseURL;
        private static readonly string _clubNumber = ConfigSection.ABCClubNumber;
        public static string accessToken;
        public static List<PaginationRequestModel> PaginationRequestModels;

        static WebRequestHelper()
        {

        }


        public static HttpWebRequest GetApiWebRequest(string apiUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Headers["app_id"] = _appId;
            request.Headers["app_key"] = _appKey;
            request.Headers["access_token"] = accessToken;
            request.Accept = "application/json;charset=UTF-8";//xml alternative application/xml;charset=UTF-8
            return request;
        }

        public static string GetRouteBaseUrl(string apiRoute, string clubNumber)
        {
            var url = $"{_apiBaseUrl}{clubNumber}/{apiRoute}";
            return url;
        }

        private static readonly string _apiDateTimeFromat = "yyyy-MM-dd";

        public static async Task<T> GetAbcDataAsync<T>(string url, string dateParam, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                var apiRoute = url;
                var routeBaseUrl = GetRouteBaseUrl(apiRoute, clubNumber);

                if (startDate.HasValue && endDate.HasValue && !String.IsNullOrEmpty(dateParam))
                {
                    routeBaseUrl = routeBaseUrl + $"?" + dateParam + $"={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }

                var request = GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var resultString = streamReader.ReadToEnd();
                        dynamic data = Json.Decode<T>(resultString);
                        return data;
                    }
                }
                return default(T);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
        public static async Task<T> GetAbcDataWithPaginationAsync<T>(string url, string dateParam, string club, int? pageSize, DateTime? startDate = null, DateTime? endDate = null, int? page = 1)
        {
            try
            {
                if (pageSize == null)
                {
                    pageSize = 5000;
                }
                var apiRoute = url;
                var routeBaseUrl = GetRouteBaseUrl(apiRoute, club);
                routeBaseUrl = routeBaseUrl + $"?size={pageSize}&page={page}";
                if (startDate.HasValue && endDate.HasValue && !String.IsNullOrEmpty(dateParam))
                {
                    routeBaseUrl = routeBaseUrl + $"&" + dateParam + $"={startDate.Value.ToString(_apiDateTimeFromat)+" 00:00:00"},{endDate.Value.ToString(_apiDateTimeFromat) + " 23:59:59"}";
                }

                var request = GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)request.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    string resultString;
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        resultString = streamReader.ReadToEnd();
                    }
                    JavaScriptSerializer jsJson = new JavaScriptSerializer();
                    jsJson.MaxJsonLength = Int32.MaxValue;
                    T obj = jsJson.Deserialize<T>(resultString);
                    //var results = JObject.Parse(resultString);
                    //var json = JsonConvert.SerializeObject(results["status"]);
                    //var status = JsonConvert.DeserializeObject<Status>(json);
                    //page = status.NextPage != null ? (int)status.NextPage : 1;
                    return obj;
                }
                return default(T);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
    }
}
