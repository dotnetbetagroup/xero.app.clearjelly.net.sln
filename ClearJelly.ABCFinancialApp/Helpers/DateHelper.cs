﻿using ClearJelly.ABCFinancialApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Helpers
{
    public static class DateHelper
    {
        public static List<DateRangeModel> GetDataByDatePeriod(int maxDays, DateTime startDate, DateTime endDate)
        {
            var dateRange = new List<DateRangeModel>();

            var day = new DateRangeModel();
            var differentDays = (endDate - startDate).Days;
            if (differentDays <= maxDays)
            {
                day = new DateRangeModel { StartDate = startDate, EndDate = endDate };
                dateRange.Add(day);
                return dateRange;
            }
            var count = differentDays / maxDays;
            var startDateTemp = startDate;
            var endDateTemp = startDate.AddDays(maxDays);

            var tmp = differentDays;
            for (int i = 0; i < count; i++)
            {
                tmp -= maxDays;
                if (i == 0)
                {
                    day = new DateRangeModel { StartDate = startDateTemp, EndDate = endDateTemp };
                    dateRange.Add(day);
                    continue;
                }
                startDateTemp = endDateTemp.AddDays(1);
                endDateTemp = endDateTemp.AddDays(maxDays);
                day = new DateRangeModel { StartDate = startDateTemp, EndDate = endDateTemp };
                dateRange.Add(day);
            }
            if(tmp > 0)
            {
                var lastDate = new DateRangeModel { StartDate = endDateTemp.AddDays(1), EndDate = endDate };
                dateRange.Add(lastDate);
            }
            
            return dateRange;
        }
    }
}
