﻿using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Helpers
{
    public static class RepositoryHelper
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public static async Task<int> ClearTable(string table, string clubNumber, string connection, DateTime startDate, DateTime endDate, IDbTransaction transaction = null)
        {
            if (table == "database_firewall_rules")
            {
                return 0;
            }               
            try
            {
                using (IDbConnection _dbConnection = new SqlConnection(connection))
                {
                    _logger.Info("Executing ABC ClearTable for {0} table", table);
                    var query = $"DELETE FROM [{table}] WHERE ClubNumber = @club" ;
                    var result = await _dbConnection.ExecuteAsync(query, new { club = clubNumber });
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error while clearing {0} table from ABC. Exception: {1}", table, ex.Message);
                throw;
            }
        }

       
        public static List<string> GetUserTableNames(string connection)
        {
            var res = new SqlConnection(connection).Query<string>("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES").ToList();
            return res;
        }


        public static async Task BulkInsertList<T>(SqlConnection connection, List<T> clubs, string tableName)
        {
            using (SqlBulkCopy copy = new SqlBulkCopy(connection))
            {
                var table = ConvertItemToDataTable(clubs);
                copy.DestinationTableName = tableName;
                await copy.WriteToServerAsync(table);
            }
        }

        private static DataTable ConvertItemToDataTable<T>(List<T> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
