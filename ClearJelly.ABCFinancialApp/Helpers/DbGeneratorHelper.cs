﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using NLog;

namespace ClearJelly.ABCFinancialApp.Helpers
{
    public class DbGeneratorHelper
    {
        private string _connectionString;
        private readonly Logger _logger;
        private string _companyName;

        public DbGeneratorHelper(string connectionString, string companyName)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _connectionString = connectionString;
            _companyName = companyName;
        }

        public async Task<bool> CreateABCDatabaseAsync()
        {
            var con = "Server = tcp:aptest.database.windows.net,1433; Initial Catalog = master; Persist Security Info = False; User ID = admin@agilityplanningtst.onmicrosoft.com; Password = Managility@2017; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Authentication =\"Active Directory Password\";Connection Timeout=120;";
            _logger.Info($"Creating ABC db for {_companyName}");
            try
            {
                using (SqlConnection connection = new SqlConnection(con))
                {
                    connection.Open();
                    var query = "create database [ABC_Financial_" + _companyName + "] (EDITION = 'basic', SERVICE_OBJECTIVE = ELASTIC_POOL(name = aptst_pool));";
                    var res =  await connection.ExecuteAsync(query);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC db for {_companyName} exception:{ex.Message}");
                return false;
            }
        }
        public async Task<bool> CreateQbBatabaseAsync()
        {
            var con = "Server = tcp:aptest.database.windows.net,1433; Initial Catalog = master; Persist Security Info = False; User ID = admin@agilityplanningtst.onmicrosoft.com; Password = Managility@2017; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Authentication =\"Active Directory Password\";Connection Timeout=120;";
            _logger.Info($"Creating ABC db for {_companyName}");
            try
            {
                using (SqlConnection connection = new SqlConnection(con))
                {
                    connection.Open();
                    var query = "create database [ABC_Financial_" + _companyName + "] (EDITION = 'basic', SERVICE_OBJECTIVE = ELASTIC_POOL(name = aptst_pool));";
                    var res = await connection.ExecuteAsync(query);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC db for {_companyName} exception:{ex.Message}");
                return false;
            }
        }
    }
}
