﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Members;
using ClearJelly.ABCFinancialApp.Models.Members.Members;
using ClearJelly.ABCFinancialApp.Models.Members.Personals;
using System;

namespace ClearJelly.ABCFinancialApp.Mapers
{
    public class MemberMapers
    {
        public static PersonalModel MapToPersonal(Personal memberPersonal, string memberId, string club)
        {
            var PersonalId = Guid.NewGuid().ToString();
            var personal = new PersonalModel
            {
                //PersonalId = PersonalId,
                MemberId = memberId,
                FirstName = memberPersonal.FirstName,
                LastName = memberPersonal.LastName,
                MiddleInitial = memberPersonal.MiddleInitial,
                AddressLine1 = memberPersonal.AddressLine1,
                AddressLine2 = memberPersonal.AddressLine2,
                City = memberPersonal.City,
                State = memberPersonal.State,
                PostalCode = memberPersonal.PostalCode,
                HomeClub = memberPersonal.HomeClub,
                CountryCode = memberPersonal.CountryCode,
                Email = memberPersonal.Email,
                PrimaryPhone = memberPersonal.PrimaryPhone,
                MobilePhone = memberPersonal.MobilePhone,
                WorkPhone = memberPersonal.WorkPhone,
                WorkPhoneExt = memberPersonal.WorkPhoneExt,
                EmergencyContactName = memberPersonal.EmergencyContactName,
                EmergencyPhone = memberPersonal.EmergencyPhone,
                EmergencyExt = memberPersonal.EmergencyExt,
                EmergencyAvailability = memberPersonal.EmergencyAvailability,
                Barcode = memberPersonal.Barcode,
                BirthDate = memberPersonal.BirthDate != null ? memberPersonal.BirthDate : DateTime.Now,
                Gender = memberPersonal.Gender,
                Employer = memberPersonal.Employer,
                Occupation = memberPersonal.Occupation,
                Group = memberPersonal.Group,
                IsActive = memberPersonal.IsActive,
                MemberStatus = memberPersonal.MemberStatus,
                JoinStatus = memberPersonal.JoinStatus,
                IsConvertedProspect = memberPersonal.IsConvertedProspect,
                HasPhoto = memberPersonal.HasPhoto,
                ConvertedDate = memberPersonal.ConvertedDate != null ? memberPersonal.ConvertedDate : DateTime.Now,
                MemberStatusReason = memberPersonal.MemberStatusReason,
                WellnessProgramId = memberPersonal.WellnessProgramId,
                FirstCheckInTimestamp = memberPersonal.FirstCheckInTimestamp != null ? memberPersonal.FirstCheckInTimestamp : DateTime.Now,
                MemberStatusDate = memberPersonal.MemberStatusDate != null ? memberPersonal.MemberStatusDate : DateTime.Now,
                LastCheckInTimestamp = memberPersonal.LastCheckInTimestamp != null ? memberPersonal.LastCheckInTimestamp : DateTime.Now,
                TotalCheckInCount = memberPersonal.TotalCheckInCount,
                CreateTimestamp = memberPersonal.CreateTimestamp != null ? memberPersonal.CreateTimestamp : DateTime.Now,
                LastModifiedTimestamp = memberPersonal.LastModifiedTimestamp != null ? memberPersonal.LastModifiedTimestamp : DateTime.Now,
                ClubNumber = Int32.Parse(club)
            };

            return personal;
        }
        public static MemberAgreementsModel MapToAgreement(MembersMemberModel agreement, string club)
        {
            var agreementId = Guid.NewGuid().ToString();
            MemberAgreementsModel agreementModel = new MemberAgreementsModel
            {
                AgreementId = agreementId,
                MemberId = agreement.MemberId,
                AgreementNumber = agreement.Agreement.AgreementNumber,
                IsPrimaryMember = agreement.Agreement.IsPrimaryMember,
                IsNonMember = agreement.Agreement.IsNonMember,
                Ordinal = agreement.Agreement.Ordinal,
                ReferringMemberId = agreement.Agreement.ReferringMemberId,
                ReferringMemberHomeClub = agreement.Agreement.ReferringMemberHomeClub,
                ReferringMemberName = agreement.Agreement.ReferringMemberName,
                SalesPersonId = agreement.Agreement.SalesPersonId,
                SalesPersonName = agreement.Agreement.SalesPersonName,
                SalesPersonHomeClub = agreement.Agreement.SalesPersonHomeClub,
                PaymentPlan = agreement.Agreement.PaymentPlan,
                Term = agreement.Agreement.Term,
                PaymentFrequency = agreement.Agreement.PaymentFrequency,
                MembershipType = agreement.Agreement.MembershipType,
                ManagedType = agreement.Agreement.ManagedType,
                CampaignId = agreement.Agreement.CampaignId,
                CampaignName = agreement.Agreement.CampaignName,
                CampaignGroup = agreement.Agreement.CampaignGroup,
                IsPastDue = agreement.Agreement.IsPastDue,
                DownPaymentPendingPOS = agreement.Agreement.DownPaymentPendingPos,
                RenewalType = agreement.Agreement.RenewalType,
                AgreementPaymentMethod = agreement.Agreement.AgreementPaymentMethod,
                DownPayment = agreement.Agreement.DownPayment,
                NextDueAmount = agreement.Agreement.NextDueAmount,
                ProjectedDueAmount = agreement.Agreement.ProjectedDueAmount != null ? agreement.Agreement.ProjectedDueAmount : 0,
                PastDueBalance = agreement.Agreement.PastDueBalance != null ? agreement.Agreement.PastDueBalance : 0,
                LateFeeAmount = agreement.Agreement.LateFeeAmount != null ? agreement.Agreement.LateFeeAmount : 0,
                ServiceFeeAmount = agreement.Agreement.ServiceFeeAmount != null ? agreement.Agreement.ServiceFeeAmount : 0,
                TotalPastDueBalance = agreement.Agreement.TotalPastDueBalance != null ? agreement.Agreement.TotalPastDueBalance : 0,
                ClubAccountPastDueBalance = agreement.Agreement.ClubAccountPastDueBalance != null ? agreement.Agreement.ClubAccountPastDueBalance : 0,
                CurrentQueue = agreement.Agreement.CurrentQueue,
                QueueTimestamp = agreement.Agreement.QueueTimestamp != null ? agreement.Agreement.QueueTimestamp : DateTime.Now,
                StationLocation = agreement.Agreement.StationLocation,
                AgreementEntrySource = agreement.Agreement.AgreementEntrySource,
                AgreementEntrySourceReportName = agreement.Agreement.AgreementEntrySourceReportName,
                SinceDate = agreement.Agreement.SinceDate != null ? agreement.Agreement.SinceDate : DateTime.Now,
                BeginDate = agreement.Agreement.BeginDate != null ? agreement.Agreement.BeginDate : DateTime.Now,
                ExpirationDate = agreement.Agreement.ExpirationDate != null ? agreement.Agreement.ExpirationDate : DateTime.Now,
                ConvertedDate = agreement.Agreement.ConvertedDate != null ? agreement.Agreement.ConvertedDate : DateTime.Now,
                LastRenewalDate = agreement.Agreement.LastRenewalDate != null ? agreement.Agreement.LastRenewalDate : DateTime.Now,
                LastRewriteDate = agreement.Agreement.LastRewriteDate != null ? agreement.Agreement.LastRewriteDate : DateTime.Now,
                RenewalDate = agreement.Agreement.RenewalDate != null ? agreement.Agreement.RenewalDate : DateTime.Now,
                FirstPaymentDate = agreement.Agreement.FirstPaymentDate != null ? agreement.Agreement.FirstPaymentDate : DateTime.Now,
                SignDate = agreement.Agreement.SignDate != null ? agreement.Agreement.SignDate : DateTime.Now,
                NextBillingDate = agreement.Agreement.NextBillingDate != null ? agreement.Agreement.NextBillingDate : DateTime.Now,
                PrimaryBillingAccountHolderFirstName = agreement.Agreement?.PrimaryBillingAccountHolder?.FirstName,
                PrimaryBillingAccountHolderLastName = agreement.Agreement?.PrimaryBillingAccountHolder?.LastName,
                ClubNumber = Int32.Parse(club)
            };
            return agreementModel;
        }
    }
}
