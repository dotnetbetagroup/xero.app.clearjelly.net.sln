﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Models.Clubs;
using ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Mapers
{
   public class ClubMapers
    {
        public static List<ClubCheckin> MapToCheckin(List<ClubCheckinModel> clubCheckinModel, string clubNumber)
        {
            var prospects = clubCheckinModel.Select(x => new ClubCheckin
            {
                CheckInId = x.CheckInId,
                CheckInTimestamp = x.CheckInTimestamp,
                StationName = x.StationName,
                CheckInStatus = x.CheckInStatus,
                MemberId = x.Member.MemberId,
                HomeClub = x.Member.HomeClub,
                ClubNumber = clubNumber
            }).ToList();

            return prospects;
        }

        public static POSTransactionsModel MapToPOSTransactions(TransactionModel transaction, string clubNumber)
        {
            var POSTransaction = new POSTransactionsModel
            {
                TransactionId = transaction.TransactionId,
                TransactionTimestamp = transaction.TransactionTimestamp,
                MemberId = transaction.MemberId,
                HomeClub = transaction.HomeClub,
                EmployeeId = transaction.EmployeeId,
                ReceiptNumber = transaction.ReceiptNumber,
                StationName = transaction.StationName,
                Return = transaction.Return,
                ClubNumber = Int32.Parse(clubNumber)
            };

            return POSTransaction;
        }
        public static POS_transItemsModel MapToPOS_transItems(TransactionItemModel item, string clubNumber,string transactionId)
        {
            var prospects = new POS_transItemsModel
            {
                ItemId = item.ItemId,
                Name = item.Name,
                InventoryType = item.InventoryType,
                Sale = item.Sale,
                Upc = item.Upc,
                ProfitCenter = item.ProfitCenter,
                Catalog = item.Catalog,
                UnitPrice = item.UnitPrice,
                Quantity = item.Quantity,
                PackageQuantity = item.PackageQuantity,
                Subtotal = item.Subtotal,
                Tax = item.Tax,
                RecurringServiceId = item.RecurringServiceId,
                TransactionId = transactionId,
                ClubNumber = Int32.Parse(clubNumber)
            };

            return prospects;
        }
        public static POS_transPaymentsModel MapToPOS_transPayments(Payment payment, TransactionItemModel item, string transactionId, string clubNumber)
        {
            var POS_transPayment = new POS_transPaymentsModel
            {
                TransactionId = transactionId,
                ItemId = item.ItemId,
                PaymentType = payment.PaymentType,
                PaymentAmount = payment.PaymentAmount,
                PaymentTax = payment.PaymentTax,
                ClubNumber = Int32.Parse(clubNumber)
            };

            return POS_transPayment;
        }
      
       
    }
}
