﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Models.Members.Prospects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Max
{
    public static class ProspectMaper
    {
        public static List<Prospect> MapToProspect(List<ProspectModel> prospectsModel, int clubNumber)
        {
            var prospects = prospectsModel.Select(x => new Prospect
            {
                Id = Guid.NewGuid().ToString(),
                ProspectId = x.prospectId,
                FirstName = x.personal.firstName,
                LastName = x.personal.lastName,
                City = x.personal.city,
                State = x.personal.state,
                PostalCode = x.personal.postalCode,
                Barcode = x.personal.barcode,
                Gender = x.personal.gender,
                IsActive = x.personal.isActive,
                HasPhoto = x.personal.hasPhoto,
                LastModifiedTimestamp = x.personal.lastModifiedTimestamp,
                AgreementEntrySource = x.agreement.agreementEntrySource,
                AgreementEntrySourceReportName = x.agreement.agreementEntrySourceReportName,
                BeginDate = x.agreement.beginDate,
                ExpirationDate = x.agreement.expirationDate,
                IssueDate = x.agreement.issueDate,
                TourDate = x.agreement.tourDate,
                VisitsAllowed = x.agreement.visitsAllowed,
                VisitsUsed = x.agreement.visitsUsed,
                CountryCode = x.personal.countryCode,
                CreatedTimestamp = x.personal.createdTimestamp,
                ClubNumber = clubNumber
            }).ToList();

            return prospects;
        }
    }
}
