﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models
{
    public class DateRequestModel
    {
        public string ApiUrl { get; set; }
        public string DateParam { get; set; }
        public int MaxDays { get; set; }
        public List<DateRangeModel> Dates { get; set; }
    }
}
