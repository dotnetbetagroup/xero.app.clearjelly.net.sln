﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class ClubCheckinModel
    {
        [JsonProperty("checkInId")]
        public string CheckInId { get; set; }

        [JsonProperty("checkInTimestamp")]
        public string CheckInTimestamp { get; set; }

        [JsonProperty("checkInMessage")]
        public string CheckInMessage { get; set; }

        [JsonProperty("stationName")]
        public string StationName { get; set; }

        [JsonProperty("checkInStatus")]
        public string CheckInStatus { get; set; } //TODO: change to enum

        [JsonProperty("member")]
        public ClubCheckinMemberModel Member { get; set; }
    }
}

//Example
//{
//    "checkInId": "010aee3beb4e4ed5a3682d6276095d15",
//    "clubNumber": "9003",
//    "checkInTimeStamp": "2016-12-28 08:00:00.000000"
//}
