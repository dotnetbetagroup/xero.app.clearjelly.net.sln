﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class ClubCheckinMemberModel
    {
        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("homeClub")]
        public string HomeClub { get; set; }
    }
}
