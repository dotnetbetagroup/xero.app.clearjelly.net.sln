﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Stations
{
    public class ClubsStationsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
