﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubStation
    {
        public string CJId { get; set; }

        [JsonProperty("stationId")]
        public string StationId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public ClubStationStatus? Status { get; set; }

        [JsonProperty("abcCode")]
        public string AbcCode { get; set; }
    }
}

//Example
//{
//    "stationId": "cre848302j5j4022hce84450184j3e3",
//    "name": "Front Desk",
//    "status": "active",
//    "abcCode": "ACCESS_CONTROL_0"
//}