﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Plans
{
    public class PlansRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("onlyPromoPlans")]
        public string OnlyPromoPlans { get; set; }

        [JsonProperty("inClubPlans")]
        public string InClubPlans { get; set; }
    }
}
