﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Plans.Plan
{
    public class PlanResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public PlanRequest Request { get; set; }

        [JsonProperty("paymentPlan")]
        public PaymentPlanModel PaymentPlan { get; set; }
    }
}
