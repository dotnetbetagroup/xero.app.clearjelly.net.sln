﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Plans.PaymentMethods
{
    public class ClubsPlansPaymentMethodsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsPlansPaymentMethodsRequest Request { get; set; }

        [JsonProperty("paymentMethods")]
        public PaymentMethodsModel PaymentMethods { get; set; }
    }
}
