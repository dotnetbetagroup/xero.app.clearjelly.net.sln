﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Plans.PaymentMethods
{
    public class ClubsPlansPaymentMethodsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
