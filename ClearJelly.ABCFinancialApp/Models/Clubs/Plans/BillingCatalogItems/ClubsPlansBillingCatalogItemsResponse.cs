﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Plans.BillingCatalogItems
{
    public class ClubsPlansBillingCatalogItemsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsPlansBillingCatalogItemsRequest Request { get; set; }

        [JsonProperty("billingCatalogItems")]
        public List<BillingCatalogItemModel> BillingCatalogItems { get; set; }
    }
}
