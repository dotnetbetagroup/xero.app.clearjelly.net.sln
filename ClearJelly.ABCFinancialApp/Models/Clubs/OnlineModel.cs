﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class OnlineModel
    {
        [JsonProperty("minors")]
        public ClubOnlineMinors Minors { get; set; }

        [JsonProperty("ccNames")]
        public ClubOnlineCcNames CcNames { get; set; }

        [JsonProperty("showFees")]
        public bool? ShowFees { get; set; }
    }
}
//Example
//{
//    "minors": {
//      "allowMinors": "true",
//      "minorAge": "18",
//      "minorDisclaimer": "string"
//    },
//    "ccNames": {
//      "requireCCNameMatch": "true",
//      "differentCcNamesDisclaimer": "string"
//    },
//    "showFees": "true"
//}