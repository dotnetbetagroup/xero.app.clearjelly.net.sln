﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos
{
   public class POS_transPaymentsModel
    {
        public string TransactionId { get; set; }
        public string ItemId { get; set; }
        public string PaymentType { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentTax { get; set; }
        public int ClubNumber { get; set; }
    }
}
