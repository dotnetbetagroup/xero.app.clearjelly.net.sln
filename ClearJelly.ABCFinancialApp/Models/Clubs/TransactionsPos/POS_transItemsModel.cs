﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos
{
   public class POS_transItemsModel
    {
        public string ItemId { get; set; }
        public string Name  { get; set; }
        public bool? Sale { get; set; }
        public string Upc { get; set; }
        public string ProfitCenter { get; set; }
        public string Catalog { get; set; }
        public string UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public string PackageQuantity { get; set; }
        public string Subtotal { get; set; }
        public string Tax { get; set; }
        public string RecurringServiceId { get; set; }
        public string TransactionId { get; set; }
        public int ClubNumber { get; set; }
        public InventoryType? InventoryType { get; set; }
        

    }
}
