﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos
{
  public  class POSTransactionsModel
    {
        public string TransactionId { get; set; }
        public DateTime? TransactionTimestamp { get; set; }
        public string MemberId { get; set; }
        public int HomeClub { get; set; }
        public string EmployeeId { get; set; }
        public string ReceiptNumber { get; set; }
        public string StationName { get; set; }
        public string Return { get; set; }
        public int ClubNumber { get; set; }
    }
}
