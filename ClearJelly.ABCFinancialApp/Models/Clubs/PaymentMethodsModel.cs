﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class PaymentMethodsModel
    {
        [JsonProperty("clubAccountPaymentMethods")]
        public List<PaymentMethod> ClubAccountPaymentMethods { get; set; }

        [JsonProperty("abcBillingPaymentMethods")]
        public List<PaymentMethod> AbcBillingPaymentMethods { get; set; }
    }
}
