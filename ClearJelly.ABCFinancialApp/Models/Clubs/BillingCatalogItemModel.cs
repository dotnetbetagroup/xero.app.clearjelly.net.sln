﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class BillingCatalogItemModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("profitCenterAbcCode")]
        public string ProfitCenterAbcCode { get; set; }

        [JsonProperty("annualFee")]
        public bool? AnnualFee { get; set; }

        [JsonProperty("totalTaxRate")]
        public double? TotalTaxRate { get; set; }

        [JsonProperty("taxes")]
        public List<Tax> Taxes { get; set; }

        [JsonProperty("assignedClubNumbers")]
        public List<string> AssignedClubNumbers { get; set; }
    }
}
