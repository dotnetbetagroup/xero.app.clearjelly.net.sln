﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Clubs
{
    public class ClubsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsRequest Request { get; set; }

        [JsonProperty("club")]
        public ClubModel Club { get; set; }
    }

}
