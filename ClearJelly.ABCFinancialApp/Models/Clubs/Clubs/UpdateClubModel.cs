﻿using ClearJelly.ABCFinancialApp.Entities;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Clubs
{
    public class UpdateClubModel
    {
        public Club Club { get; set; }
        public ClubOnline ClubOnline { get; set; }
        public ClubOnlineMinors ClubOnlineMinors { get; set; }
        public ClubOnlineCcNames ClubOnlieCcNames { get; set; }
        public List<ClubCountry> ClubCountries { get; set; }
    }
}
