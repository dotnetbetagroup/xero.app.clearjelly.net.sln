﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class ClubTransactionModel
    {
        [JsonProperty("transactions")]
        public List<TransactionModel> Transactions { get; set; }
    }
}
//Example
//{
//"transactions": [
//    {
//    "transactionId": "340tj3g0j30gj02jt20gj20gj2g243524",
//    "transactionTimestamp": "2016-12-28 08:00:00.000000",
//    "memberId": "010aee3beb4e4ed5a3682d6276095d15",
//    "homeClub": "9003",
//    "employeeId": "c41e5df2682a481b8396da9e10272390",
//    "receiptNumber": "5128-66-26970",
//    "stationName": "Front Desk",
//    "return": "true",
//    "items": {
//    "item": [
//        {
//        "itemId": "b53627e336b54dd8a26c994f5ecdcdd3",
//        "name": "1 Hour PT",
//        "inventoryType": "product",
//        "sale": "true",
//        "upc": "70960900122",
//        "profitCenter": "Dues",
//        "catalog": "Company",
//        "unitPrice": "$35.00",
//        "quantity": "1",
//        "packageQuantity": "09",
//        "subtotal": "$40.00",
//        "tax": "$2.67",
//        "recurringServiceId": "264934628624862jjf2jf20k3r2r2352",
//        "payments": [
//            {
//            "paymentType": "Club Account",
//            "paymentAmount": "$35.00",
//            "paymentTax": "$2.67"
//            }
//        ]
//        }
//    ]
//}