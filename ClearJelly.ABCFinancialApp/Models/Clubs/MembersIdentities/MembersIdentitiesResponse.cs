﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.MembersIdentities
{
    public class MembersIdentitiesResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public MembersIdentitiesRequest Request { get; set; }

        [JsonProperty("identities")]
        public List<ClubMemberIdentity> Identities { get; set; }
    }
}
