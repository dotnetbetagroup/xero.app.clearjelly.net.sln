﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.MembersIdentities
{
    public class MembersIdentitiesRequest
    {
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
