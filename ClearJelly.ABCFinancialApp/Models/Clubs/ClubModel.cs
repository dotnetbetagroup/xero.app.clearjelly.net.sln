﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class ClubModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("donationItem")]
        public string DonationItem { get; set; }

        [JsonProperty("onlineSignupAllowedPaymentMethods")]
        public OnlineSignupAllowedPaymentMethods? OnlineSignupAllowedPaymentMethods { get; set; }

        [JsonProperty("supportedCountries")]
        public List<string> SupportedCountries { get; set; }

        [JsonProperty("online")]
        public OnlineModel Online { get; set; }

        [JsonProperty("billingCountry")]
        public string BillingCountry { get; set; }

        [JsonProperty("creditCardPaymentMethods")]
        public List<string> CreditCardPaymentMethods { get; set; } //In doc Array[string]
    }
}
//Example
//{
//"name": "Gym Fitness",
//"shortName": "GF",
//"timeZone": "EDT",
//"address1": "28201 E. Bonanza St.",
//"address2": "#409",
//"city": "South Park",
//"state": "CO",
//"postalCode": "80440",
//"country": "US",
//"email": "mrkitty@gmail.com",
//"donationItem": "string",
//"onlineSignupAllowedPaymentMethods": "EFT",
//"supportedCountries": [
//    "string"
//],
//"online": {
//    "minors": {
//    "allowMinors": "true",
//    "minorAge": "18",
//    "minorDisclaimer": "string"
//    },
//    "ccNames": {
//    "requireCCNameMatch": "true",
//    "differentCcNamesDisclaimer": "string"
//    },
//    "showFees": "true"
//},
//"billingCountry": "US",
//"creditCardPaymentMethods": "visa"
//}