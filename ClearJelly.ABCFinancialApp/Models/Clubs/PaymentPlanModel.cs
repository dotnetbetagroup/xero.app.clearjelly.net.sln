﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public partial class PaymentPlanModel
    {
        [JsonProperty("planName")]
        public string PlanName { get; set; }

        [JsonProperty("planId")]
        public string PlanId { get; set; }

        [JsonProperty("promotionCode")]
        public string PromotionCode { get; set; }

        [JsonProperty("promotionName")]
        public string PromotionName { get; set; }

        [JsonProperty("membershipType")]
        public string MembershipType { get; set; }
        //TODO change to enum
        [JsonProperty("agreementTerm")]
        public string AgreementTerm { get; set; }
        //TODO change to enum
        [JsonProperty("scheduleFrequency")]
        public string ScheduleFrequency { get; set; }

        [JsonProperty("termInMonths")]
        public long TermInMonths { get; set; }

        [JsonProperty("dueDay")]
        public string DueDay { get; set; }

        [JsonProperty("firstDueDate")]
        public DateTime? FirstDueDate { get; set; }

        [JsonProperty("activePresale")]
        public bool ActivePresale { get; set; }

        [JsonProperty("expirationDate")]
        public DateTime? ExpirationDate { get; set; }

        [JsonProperty("onlineSignupAllowedPaymentMethods")]
        public string OnlineSignupAllowedPaymentMethods { get; set; }

        [JsonProperty("preferredPaymentMethod")]
        public string PreferredPaymentMethod { get; set; }

        [JsonProperty("totalContractValue")]
        public string TotalContractValue { get; set; }

        [JsonProperty("downPaymentName")]
        public string DownPaymentName { get; set; }

        [JsonProperty("downPaymentTotalAmount")]
        public string DownPaymentTotalAmount { get; set; }

        [JsonProperty("downPayments")]
        public List<PaymentPlanDownPayment> DownPayments { get; set; }

        [JsonProperty("scheduleTotalAmount")]
        public string ScheduleTotalAmount { get; set; }

        [JsonProperty("schedules")]
        public List<PaymentPlanSchedule> Schedules { get; set; }

        [JsonProperty("agreementTerms")]
        public string AgreementTerms { get; set; }

        [JsonProperty("agreementDescription")]
        public string AgreementDescription { get; set; }

        [JsonProperty("agreementNote")]
        public string AgreementNote { get; set; }

        [JsonProperty("emailGreeting")]
        public string EmailGreeting { get; set; }

        [JsonProperty("emailClosing")]
        public string EmailClosing { get; set; }

        [JsonProperty("userDefinedFields")]
        public List<PaymentPlanUserDefinedField> UserDefinedFields { get; set; }

        [JsonProperty("clubFeeTotalAmount")]
        public string ClubFeeTotalAmount { get; set; }

        [JsonProperty("clubFees")]
        public List<PaymentPlanClubFee> ClubFees { get; set; }

        [JsonProperty("fieldOption")]
        public List<PaymentPlanFieldOption> FieldOption { get; set; }

        [JsonProperty("planValidation")]
        public long PlanValidation { get; set; }
    }
}
