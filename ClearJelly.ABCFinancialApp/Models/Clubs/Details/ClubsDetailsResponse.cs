﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs.Details
{
    public class ClubsCheckinsDetailsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsDetailsRequest Request { get; set; }

        [JsonProperty("checkins")]
        public List<ClubCheckinModel> Checkins { get; set; }
    }
}
