﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Clubs
{
    public class ClubPlanAnnualFeeModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("preTaxAmount")]
        public string PreTaxAmount { get; set; } //api type number 

        [JsonProperty("profitCenterAbcCode")]
        public string ProfitCenterAbcCode { get; set; } //api type number 

        [JsonProperty("assignedClubNumbers")]
        public List<string> AssignedClubNumbers { get; set; }
    }
}
//Example
//{
//    "id": "c41e5df2682a481b8396da9e10272390",
//    "name": "Upgrade fee",
//    "amount": "$35.00",
//    "preTaxAmount": "$40.00",
//    "profitCenterAbcCode": "FEES",
//    "assignedClubNumbers": [
//    "string"
//    ]
//}