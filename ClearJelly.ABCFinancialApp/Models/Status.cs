﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models
{
    public class Status
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("previousPage")]
        public int? PreviousPage { get; set; }

        [JsonProperty("count")]
        public int? Count { get; set; }

        [JsonProperty("nextPage")]
        public int? NextPage { get; set; }

        [JsonProperty("messageCode")]
        public string MessageCode { get; set; }
    }
}
//Example
//{
//"message": "success",
//"previousPage": "1",
//"count": "1",
//"nextPage": "3",
//"messageCode": "API-MEM-MEM-0000"
//}