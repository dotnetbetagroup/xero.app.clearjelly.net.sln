﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using Newtonsoft.Json;
using System;

namespace ClearJelly.ABCFinancialApp.Models.Members
{
    public class AgreementModel
    {
        [JsonProperty("agreementNumber")]
        public string AgreementNumber { get; set; }

        [JsonProperty("isPrimaryMember")]
        public bool? IsPrimaryMember { get; set; }

        [JsonProperty("isNonMember")]
        public bool? IsNonMember { get; set; }

        [JsonProperty("ordinal")]
        public long? Ordinal { get; set; }

        [JsonProperty("referringMemberId")]
        public string ReferringMemberId { get; set; }

        [JsonProperty("referringMemberHomeClub")]
        public string ReferringMemberHomeClub { get; set; }

        [JsonProperty("referringMemberName")]
        public string ReferringMemberName { get; set; }

        [JsonProperty("salesPersonId")]
        public string SalesPersonId { get; set; }

        [JsonProperty("salesPersonName")]
        public string SalesPersonName { get; set; }

        [JsonProperty("salesPersonHomeClub")]
        public string SalesPersonHomeClub { get; set; }

        [JsonProperty("paymentPlan")]
        public string PaymentPlan { get; set; }

        [JsonProperty("term")]
        //[JsonConverter(typeof(StringEnumConverter))]
        public Term? Term { get; set; }

        [JsonProperty("paymentFrequency")]
        public string PaymentFrequency { get; set; }//TODO change to enum

        [JsonProperty("membershipType")]
        public string MembershipType { get; set; }

        [JsonProperty("managedType")]
        public string ManagedType { get; set; }

        [JsonProperty("campaignId")]
        public string CampaignId { get; set; }

        [JsonProperty("campaignName")]
        public string CampaignName { get; set; }

        [JsonProperty("campaignGroup")]
        public string CampaignGroup { get; set; }

        [JsonProperty("isPastDue")]
        public bool? IsPastDue { get; set; }

        [JsonProperty("downPaymentPendingPOS")]
        public string DownPaymentPendingPos { get; set; }

        [JsonProperty("renewalType")]
        public string RenewalType { get; set; }//TODO change to enum

        [JsonProperty("agreementPaymentMethod")]
        public string AgreementPaymentMethod { get; set; }

        [JsonProperty("downPayment")]
        public decimal? DownPayment { get; set; }

        [JsonProperty("nextDueAmount")]
        public decimal? NextDueAmount { get; set; }

        [JsonProperty("projectedDueAmount")]
        public decimal? ProjectedDueAmount { get; set; }

        [JsonProperty("pastDueBalance")]
        public decimal? PastDueBalance { get; set; }

        [JsonProperty("lateFeeAmount")]
        public decimal? LateFeeAmount { get; set; }

        [JsonProperty("serviceFeeAmount")]
        public decimal? ServiceFeeAmount { get; set; }

        [JsonProperty("totalPastDueBalance")]
        public decimal? TotalPastDueBalance { get; set; }

        [JsonProperty("clubAccountPastDueBalance")]
        public decimal? ClubAccountPastDueBalance { get; set; }

        [JsonProperty("currentQueue")]
        public string CurrentQueue { get; set; }

        [JsonProperty("queueTimestamp")]
        public DateTime? QueueTimestamp { get; set; } //YYYY-MM-DD hh:mm:ss:nnnnnn 

        [JsonProperty("stationLocation")]
        public string StationLocation { get; set; }

        [JsonProperty("agreementEntrySource")]
        public string AgreementEntrySource { get; set; }

        [JsonProperty("agreementEntrySourceReportName")]
        public string AgreementEntrySourceReportName { get; set; }

        [JsonProperty("sinceDate")]
        public DateTime? SinceDate { get; set; }

        [JsonProperty("beginDate")]
        public DateTime? BeginDate { get; set; }

        [JsonProperty("expirationDate")]
        public DateTime? ExpirationDate { get; set; }

        [JsonProperty("convertedDate")]
        public DateTime? ConvertedDate { get; set; }

        [JsonProperty("lastRenewalDate")]
        public DateTime? LastRenewalDate { get; set; }

        [JsonProperty("lastRewriteDate")]
        public DateTime? LastRewriteDate { get; set; }

        [JsonProperty("renewalDate")]
        public DateTime? RenewalDate { get; set; }

        [JsonProperty("firstPaymentDate")]
        public DateTime? FirstPaymentDate { get; set; }

        [JsonProperty("signDate")]
        public DateTime? SignDate { get; set; }

        [JsonProperty("nextBillingDate")]
        public DateTime? NextBillingDate { get; set; }

        [JsonProperty("primaryBillingAccountHolder")]
        public PrimaryBillingAccountHolder PrimaryBillingAccountHolder { get; set; }
    }
}
//Example
//{
//    "agreementNumber": "07488",
//    "isPrimaryMember": true,
//    "isNonMember": true,
//    "ordinal": 0,
//    "referringMemberId": "010aee3beb4e4ed5a3682d6276095d15",
//    "referringMemberHomeClub": "9003",
//    "referringMemberName": "Mitch Connor",
//    "salesPersonId": "010aee3beb4e4ed5a3682d6276095d15",
//    "salesPersonName": "Mitch Connor",
//    "salesPersonHomeClub": "9003",
//    "paymentPlan": "DUES",
//    "term": "Cash",
//    "paymentFrequency": "monthly",
//    "membershipType": "Gold",
//    "managedType": "ABC Managed",
//    "campaignId": "010aee3beb4e4ed5a3682d6276095d15",
//    "campaignName": "string",
//    "campaignGroup": "string",
//    "isPastDue": true,
//    "downPaymentPendingPOS": "string",
//    "renewalType": "Auto-Renew Open",
//    "agreementPaymentMethod": "Credit Card",
//    "downPayment": 123.45,
//    "nextDueAmount": 123.45,
//    "projectedDueAmount": 123.45,
//    "pastDueBalance": 123.45,
//    "lateFeeAmount": 123.45,
//    "serviceFeeAmount": 0,
//    "totalPastDueBalance": 0,
//    "clubAccountPastDueBalance": 0,
//    "currentQueue": "string",
//    "queueTimestamp": "2016-12-28 08:00:00.000000",
//    "stationLocation": "Home",
//    "agreementEntrySource": "DataTrak EAE",
//    "agreementEntrySourceReportName": "EAE",
//    "sinceDate": "2017-12-27T09:11:31.517Z",
//    "beginDate": "2017-12-27T09:11:31.517Z",
//    "expirationDate": "2017-12-27T09:11:31.517Z",
//    "convertedDate": "2017-12-27T09:11:31.517Z",
//    "lastRenewalDate": "2017-12-27T09:11:31.517Z",
//    "lastRewriteDate": "2017-12-27T09:11:31.517Z",
//    "renewalDate": "2017-12-27T09:11:31.517Z",
//    "firstPaymentDate": "2017-12-27T09:11:31.517Z",
//    "signDate": "2017-12-27T09:11:31.517Z",
//    "nextBillingDate": "2017-12-27T09:11:31.517Z",
//    "primaryBillingAccountHolder": {
//        "firstName": "Mitch",
//        "lastName": "Conner"
//    }
//}