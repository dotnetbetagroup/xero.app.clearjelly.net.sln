﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Members
{
    public class MemberCheckinSummariesResponceModel
    {
        public string CJId { get; set; }
        public string MemberCheckinSummariesId { get; set; }
        public string MemberId { get; set; }      
    }
}
