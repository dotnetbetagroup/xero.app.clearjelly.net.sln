﻿using ClearJelly.ABCFinancialApp.Models.Members;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members.Checkins.Details
{
    public class DetailsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public DetailsRequest Request { get; set; }

        [JsonProperty("members")]
        public List<MemberCheckinsDetailsMemberModel> Members { get; set; }
    }
}
