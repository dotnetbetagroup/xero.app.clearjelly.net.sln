﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members.Checkins.Summaries
{
    public class SummariesResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public SummariesRequest Request { get; set; }

        [JsonProperty("members")]
        public List<CheckinDetailsSummariesMemberModel> Members { get; set; }
    }
}
