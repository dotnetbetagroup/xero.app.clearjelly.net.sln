﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Members.Groups
{
    public class GroupsRequest
    {
        [JsonProperty("activeStatus")]
        public string ActiveStatus { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
