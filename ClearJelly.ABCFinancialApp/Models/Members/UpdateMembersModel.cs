﻿using ClearJelly.ABCFinancialApp.Entities;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members
{
    public class UpdateMembersModel
    {
        public List<MembersMember> Members { get; set; }
        public List<Personal> Personals { get; set; }
        public List<Agreement> Agreements { get; set; }
        public List<Alert> Alerts { get; set; }

        public UpdateMembersModel()
        {
            Members = new List<MembersMember>();
            Personals = new List<Personal>();
            Alerts = new List<Alert>();
        }
    }
}
