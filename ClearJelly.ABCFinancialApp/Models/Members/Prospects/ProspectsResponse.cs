﻿using ClearJelly.ABCFinancialApp.Models.Members.Personals;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Members.Prospects
{
   public class ProspectsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public MembersPersonalsRequest Request { get; set; }

        [JsonProperty("prospects")]
        public List<ProspectModel> Prospects { get; set; }
    }   
}
