﻿using ClearJelly.ABCFinancialApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Members.Prospects
{
   public class ProspectModel
    {
        public string prospectId { get; set; }
        public PersonalModel personal { get; set; }
        public AgreementModel agreement { get; set; }
    }
}
