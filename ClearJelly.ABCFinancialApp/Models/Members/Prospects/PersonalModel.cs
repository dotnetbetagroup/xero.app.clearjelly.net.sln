﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Members.Prospects
{
    public class PersonalModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleInitial { get; set; }
        public string addressLine1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string countryCode { get; set; }
        public string email { get; set; }
        public string primaryPhone { get; set; }
        public string barcode { get; set; }
        public string birthDate { get; set; }
        public string gender { get; set; }
        public string employer { get; set; }
        public string isActive { get; set; }
        public string hasPhoto { get; set; }
        public string firstCheckInTimestamp { get; set; }
        public string createdTimestamp { get; set; }
        public string lastModifiedTimestamp { get; set; }
        public string misc1 { get; set; }
        public string misc2 { get; set; }
        public string occupation { get; set; }
        public string group { get; set; }
    }

}
