﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Members.Prospects
{
    public class AgreementModel
    {
        public string campaignId { get; set; }
        public string campaignName { get; set; }
        public string campaignGroup { get; set; }
        public string agreementEntrySource { get; set; }
        public string agreementEntrySourceReportName { get; set; }
        public string beginDate { get; set; }
        public string expirationDate { get; set; }
        public string issueDate { get; set; }
        public string tourDate { get; set; }
        public string visitsAllowed { get; set; }
        public string visitsUsed { get; set; }
        public string salesPersonId { get; set; }
        public string salesPersonName { get; set; }
        public string salesPersonHomeClub { get; set; }
        public string leadPriority { get; set; }
        public string referringMemberId { get; set; }
        public string referringMemberHomeClub { get; set; }
        public string referringMemberName { get; set; }
    }
}
