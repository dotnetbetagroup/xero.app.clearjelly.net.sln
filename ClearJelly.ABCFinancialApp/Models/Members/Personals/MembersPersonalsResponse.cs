﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members.Personals
{
    public class MembersPersonalsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public MembersPersonalsRequest Request { get; set; }

        [JsonProperty("members")]
        public List<PersonalsMemberModel> Members { get; set; }
    }
}
