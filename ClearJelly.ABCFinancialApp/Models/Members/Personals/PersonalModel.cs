﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Models.Members.Personals
{
    public class PersonalModel
    {
        //public string PersonalId { get; set; }
        public string MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string HomeClub { get; set; }
        public string CountryCode { get; set; }
        public string Email { get; set; }
        public string PrimaryPhone { get; set; }
        public string MobilePhone { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyPhone { get; set; }
        public string EmergencyExt { get; set; }
        public string EmergencyAvailability { get; set; }
        public string Barcode { get; set; }
        public DateTime? BirthDate { get; set; }
        public Gender? Gender { get; set; }
        public string Employer { get; set; }
        public string Occupation { get; set; }
        public string Group { get; set; }
        public bool? IsActive { get; set; }
        public string MemberStatus { get; set; }
        public JoinStatus? JoinStatus { get; set; }
        public bool? IsConvertedProspect { get; set; }
        public bool? HasPhoto { get; set; }
        public DateTime? ConvertedDate { get; set; }
        public string MemberStatusReason { get; set; }
        public string WellnessProgramId { get; set; }
        public DateTime? FirstCheckInTimestamp { get; set; }
        public DateTime? MemberStatusDate { get; set; }
        public DateTime? LastCheckInTimestamp { get; set; }
        public long? TotalCheckInCount { get; set; }
        public DateTime? CreateTimestamp { get; set; }
        public DateTime? LastModifiedTimestamp { get; set; }
        public int ClubNumber { get; set; }
    }
}
