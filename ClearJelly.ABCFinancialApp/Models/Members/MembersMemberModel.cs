﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members
{
    public class MembersMemberModel
    {

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("personal")]
        public Personal Personal { get; set; }

        [JsonProperty("agreement")]
        public Agreement Agreement { get; set; }

        [JsonProperty("alerts")]
        public List<Alert> Alerts { get; set; }
    }
}
