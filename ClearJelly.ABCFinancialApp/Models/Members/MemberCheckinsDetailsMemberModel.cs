﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members
{
    public class MemberCheckinsDetailsMemberModel
    {
        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("checkins")]
        public List<MemberCheckinsDetailsMemberCheckin> Checkins { get; set; }
    }
}
