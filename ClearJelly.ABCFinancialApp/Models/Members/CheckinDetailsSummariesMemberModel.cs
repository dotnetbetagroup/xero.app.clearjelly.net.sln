﻿using ClearJelly.ABCFinancialApp.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClearJelly.ABCFinancialApp.Models.Members
{
    public class CheckinDetailsSummariesMemberModel
    {
        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("links")]
        public List<LinkModel> Links { get; set; }

        [JsonProperty("checkInCounts")]
        public CheckinCountsModel CheckInCounts { get; set; }
    }

    public class LinkModel
    {
        [JsonProperty("rel")]
        public string Rel { get; set; }

        [JsonProperty("href")]
        public string Url { get; set; }
    }

}
