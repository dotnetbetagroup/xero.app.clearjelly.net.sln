﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models.Members.Children
{
    public class MembersChildrenRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("memberChildId")]
        public string MemberChildId { get; set; }

        [JsonProperty("page")]
        public string Page { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("activeStatus")]
        public string ActiveStatus { get; set; }

        [JsonProperty("agreementNumber")]
        public string AgreementNumber { get; set; }

        [JsonProperty("createdTimestampDateRange")]
        public string CreatedTimestampDateRange { get; set; }
    }
}
