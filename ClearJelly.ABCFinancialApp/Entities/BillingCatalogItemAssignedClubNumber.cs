﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class BillingCatalogItemAssignedClubNumber
    {
        public string BillingCatalogItemId { get; set; }

        public string CJId { get; set; }

        public string Number { get; set; }
    }
}
