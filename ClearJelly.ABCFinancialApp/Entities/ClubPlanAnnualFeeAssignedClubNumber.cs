﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubPlanAnnualFeeAssignedClubNumber
    {
        public string CJId { get; set; }

        public string ClubPlanAnnualFeeId { get; set; }

        public string Number { get; set; }
    }
}
