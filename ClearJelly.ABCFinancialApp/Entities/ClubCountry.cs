﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubCountry
    {
        public string CJId { get; set; }

        public string ClubId { get; set; }

        public string Country { get; set; }
    }
}
