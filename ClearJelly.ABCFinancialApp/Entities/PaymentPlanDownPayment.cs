﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class PaymentPlanDownPayment
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("subTotal")]
        public string SubTotal { get; set; }

        [JsonProperty("tax")]
        public string Tax { get; set; }

        [JsonProperty("total")]
        public string Total { get; set; }
    }
}
//Example
//{
//    "name": "string",
//    "subTotal": "$35.00",
//    "tax": "$2.67",
//    "total": "$40.00"
//}