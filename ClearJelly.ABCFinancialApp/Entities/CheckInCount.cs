﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class CheckinCount
    {
        [JsonProperty("club")]
        public int? Club { get; set; }

        [JsonProperty("count")]
        public int? Count { get; set; }
    }
}

//Example
//{
//  "club": "9003",
//  "count": "3"
//}
