﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class Prospect
    {
        public string Id { get; set; }        
        public string ProspectId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string Barcode { get; set; }
        public string Gender { get; set; }
        public string IsActive { get; set; }
        public string HasPhoto { get; set; }
        public string CreatedTimestamp { get; set; }
        public string LastModifiedTimestamp { get; set; }
        public string AgreementEntrySource { get; set; }
        public string AgreementEntrySourceReportName { get; set; }
        public string BeginDate { get; set; }
        public string ExpirationDate { get; set; }
        public string IssueDate { get; set; }
        public string TourDate { get; set; }
        public string VisitsAllowed { get; set; }
        public string VisitsUsed { get; set; }
        public int ClubNumber { get; set; }

    }
}
