﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class PaymentPlanUserDefinedField
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("table")]
        public long? Table { get; set; }

        [JsonProperty("index")]
        public long? Index { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("required")]
        public bool? Required { get; set; }

        [JsonProperty("values")]
        public string Values { get; set; }
    }
}
//Example
//{
//"field": "string",
//"table": 0,
//"index": 0,
//"type": "string",
//"required": true,
//"values": "string"
//}