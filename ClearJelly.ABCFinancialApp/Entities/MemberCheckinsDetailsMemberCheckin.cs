﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class MemberCheckinsDetailsMemberCheckin
    {
        [JsonProperty("checkInId")]
        public string CheckInId { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("checkInTimeStamp")]
        public string CheckInTimeStamp { get; set; }
    }
}
