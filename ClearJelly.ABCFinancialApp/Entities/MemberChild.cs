﻿using System;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class MemberChild
    {
        public string MemberChildId { get; set; }

        public string ActiveStatus { get; set; }

        public int? HomeClub { get; set; }

        public string FirstName { get; set; }

        public string MiddleInitial { get; set; }

        public string LastName { get; set; }

        public string Barcode { get; set; }

        public string Gender { get; set; }

        public DateTime? BirthDate { get; set; }

        public string PrimaryMemberId { get; set; }
        
        public string AgreementNumber { get; set; }

        public string ChildMisc1 { get; set; }
        
        public string ChildMisc2 { get; set; }
        
        public string ChildMisc3 { get; set; }

        public DateTime? CreateTimestamp { get; set; }

        public DateTime? LastModifiedTimestamp { get; set; }

        public DateTime? LastCheckInTimestamp { get; set; }

        //public MemberChildEmergencyContact MemberChildEmergencyContact { get; set; }

        //public List<Note> Notes { get; set; }
    }
}