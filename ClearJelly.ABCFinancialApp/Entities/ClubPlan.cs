﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubPlan
    {
        public string CJId { get; set; }

        [JsonProperty("planName")]
        public string PlanName { get; set; }

        [JsonProperty("planId")]
        public string PlanId { get; set; }

        [JsonProperty("promoCode")]
        public string PromoCode { get; set; }

        [JsonProperty("promoName")]
        public string PromoName { get; set; }

        [JsonProperty("agreementDescription")]
        public string AgreementDescription { get; set; }

        [JsonProperty("limitedAvailability")]
        public bool? LimitedAvailability { get; set; }
    }
}
//Example
//{
//    "planName": "Silver Plan",
//    "planId": "8b4a1050323746b8bbd684ed2a346b9d",
//    "promoCode": "aug2017",
//    "promoName": "Summer",
//    "agreementDescription": "cmVzcGVjdCBteSBhdXRob3JpdGFhYWFoaGg=",
//    "limitedAvailability": "true"
//}