﻿using System.ComponentModel.DataAnnotations;

namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum MemberStatus
    {
        None = 0,
        [Display(Name = "Active")]
        active = 1,
        inactive = 2,
        all = 3,
        prospect = 4,
        [Display(Name = "Need Address")]
        NeedAddress = 5
    }
}
//Example
//['active', 'inactive', 'all', 'prospect']