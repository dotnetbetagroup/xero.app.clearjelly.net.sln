﻿namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum Term
    {
        None = 0,
        Open = 1,
        Installment = 2,
        Cash = 3
    }
}
//Example
//['Open', 'Installment', 'Cash'] ,