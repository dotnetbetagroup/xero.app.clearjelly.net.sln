﻿namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum OnlineSignupAllowedPaymentMethods
    {
        None = 0,
        ALL = 1,
        EFT = 2,
        CCS = 3
    }
}
//Example
//['ALL', 'EFT', 'CCD']