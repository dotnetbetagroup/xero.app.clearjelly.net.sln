﻿namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum JoinStatus
    {
        None = 0,
        Member = 1,
        Prospect = 2
    }
}
//Example
//['Member', 'Prospect']