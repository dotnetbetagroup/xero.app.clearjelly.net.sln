﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum Clubs
    {
        WorldGymSchererville = 8725,
        WorldGymCedarLake = 8726,
        ABC9003PlusUltra = 9003
    }
}
