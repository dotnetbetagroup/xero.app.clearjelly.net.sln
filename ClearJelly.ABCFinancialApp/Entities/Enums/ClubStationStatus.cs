﻿namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum ClubStationStatus
    {
        None = 0,
        active = 1,
        inactive = 2
    }
}
//Example
//['active', 'inactive'] 