﻿namespace ClearJelly.ABCFinancialApp.Entities.Enums
{
    public enum InventoryType
    {
        None = 0,
        product = 1,
        service = 2
    }
}
//Example
//['product', 'service'] ,
