﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubCheckin
    {
        public string CheckInId { get; set; }
                
        public string CheckInTimestamp { get; set; }        

        public string StationName { get; set; }

        public string CheckInStatus { get; set; } //TODO: change to enum
       
        public string MemberId  { get; set; }
        public string HomeClub { get; set; }
        public string ClubNumber  { get; set; }
        //public ClubCheckinsDetailsMemberModel Member { get; set; }
    }
}