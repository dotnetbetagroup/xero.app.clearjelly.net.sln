﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubOnlineMinors
    {
        public string CJId { get; set; }

        public string ClubOnlineId { get; set; }

        [JsonProperty("allowMinors")]
        public bool AllowMinors { get; set; }

        [JsonProperty("minorAge")]
        public byte MinorAge { get; set; }

        [JsonProperty("minorDisclaimer")]
        public string MinorDisclaimer { get; set; }
    }
}
//Example
//{
//     "allowMinors": "true",
//     "minorAge": "18",
//     "minorDisclaimer": "string"
//    }