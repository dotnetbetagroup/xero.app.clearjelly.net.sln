﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class PaymentMethod
    {
        public string CJId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("abcCode")]
        public string AbcCode { get; set; }
    }
}
