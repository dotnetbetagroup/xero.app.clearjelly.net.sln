﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubCheckinMember
    {
        //public string ClubCheckinMemberId { get; set; }

        //public string ClubCheckinId { get; set; } 

        public string MemberId { get; set; }

        public string HomeClub { get; set; }
    }
}