﻿using ClearJelly.ABCFinancialApp.Entities.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace ClearJelly.ABCFinancialApp.Models
{
    public class Personal
    {
        public string PersonalId { get; set; }

        public string MemberId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("middleInitial")]
        public string MiddleInitial { get; set; }

        [JsonProperty("addressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("addressLine2")]
        public string AddressLine2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("homeClub")]
        public string HomeClub { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("primaryPhone")]
        public string PrimaryPhone { get; set; }

        [JsonProperty("mobilePhone")]
        public string MobilePhone { get; set; }

        [JsonProperty("workPhone")]
        public string WorkPhone { get; set; }

        [JsonProperty("workPhoneExt")]
        public string WorkPhoneExt { get; set; }

        [JsonProperty("emergencyContactName")]
        public string EmergencyContactName { get; set; }

        [JsonProperty("emergencyPhone")]
        public string EmergencyPhone { get; set; }

        [JsonProperty("emergencyExt")]
        public string EmergencyExt { get; set; }

        [JsonProperty("emergencyAvailability")]
        public string EmergencyAvailability { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("birthDate")]
        public DateTime? BirthDate { get; set; } 

        [JsonProperty("gender")]
        public Gender? Gender { get; set; }

        [JsonProperty("employer")]
        public string Employer { get; set; }

        [JsonProperty("occupation")]
        public string Occupation { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }

        [JsonProperty("memberStatus")]
        //[JsonConverter(typeof(StringEnumConverter))]
        public string MemberStatus { get; set; }

        [JsonProperty("joinStatus")]
        //[JsonConverter(typeof(StringEnumConverter))]
        public JoinStatus? JoinStatus { get; set; }

        [JsonProperty("isConvertedProspect")]
        public bool? IsConvertedProspect { get; set; }

        [JsonProperty("hasPhoto")]
        public bool? HasPhoto { get; set; }

        [JsonProperty("convertedDate")]
        public DateTime? ConvertedDate { get; set; }

        [JsonProperty("memberStatusReason")]
        public string MemberStatusReason { get; set; }

        [JsonProperty("wellnessProgramId")]
        public string WellnessProgramId { get; set; }

        [JsonProperty("firstCheckInTimestamp")]
        public DateTime? FirstCheckInTimestamp { get; set; }

        [JsonProperty("memberStatusDate")]
        public DateTime? MemberStatusDate { get; set; }

        [JsonProperty("lastCheckInTimestamp")]       
        public DateTime? LastCheckInTimestamp { get; set; }

        [JsonProperty("totalCheckInCount")]
        public long? TotalCheckInCount { get; set; }

        [JsonProperty("createTimestamp")]
        public DateTime? CreateTimestamp { get; set; }

        [JsonProperty("lastModifiedTimestamp")]
        public DateTime? LastModifiedTimestamp { get; set; }       
    }
}