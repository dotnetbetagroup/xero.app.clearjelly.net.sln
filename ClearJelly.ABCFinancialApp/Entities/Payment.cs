﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public partial class Payment
    {
        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("paymentAmount")]
        public string PaymentAmount { get; set; }

        [JsonProperty("paymentTax")]
        public string PaymentTax { get; set; }
    }
}
