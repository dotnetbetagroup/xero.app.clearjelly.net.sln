﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubOnline
    {
        public string CJId { get; set; }

        public string ClubId { get; set; }        

        //public Minors Minors { get; set; }

        //public CcNames CcNames { get; set; }

        public bool? ShowFees { get; set; }
    }
}