﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Models
{
    public class PrimaryBillingAccountHolder
    {
        public string CJId { get; set; }

        public string AgreementId { get; set; }


        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
