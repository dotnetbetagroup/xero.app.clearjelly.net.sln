﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class MemberChildEmergencyContact
    {
        [JsonProperty("emergencyContactFirstName")]
        public string EmergencyContactFirstName { get; set; }

        [JsonProperty("emergencyContactMiddleInitial")]
        public string EmergencyContactMiddleInitial { get; set; }

        [JsonProperty("emergencyContactLastName")]
        public string EmergencyContactLastName { get; set; }

        [JsonProperty("emergencyContactPhone")]
        public string EmergencyContactPhone { get; set; }

        [JsonProperty("emergencyContactExtension")]
        public string EmergencyContactExtension { get; set; }
    }
}
//Example
//{
//    "emergencyContactFirstName": "Mitch",
//    "emergencyContactMiddleInitial": "H",
//    "emergencyContactLastName": "Conner",
//    "emergencyContactPhone": "9495898283",
//    "emergencyContactExtension": "1234"
//}