﻿namespace ClearJelly.ABCFinancialApp.Entities
{
    public class BillingCatalogItem
    {
        public string CJId { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string ProfitCenterAbcCode { get; set; }

        public bool? AnnualFee { get; set; }

        public double? TotalTaxRate { get; set; }

        //public List<Tax> Taxes { get; set; }

        //public List<string> AssignedClubNumbers { get; set; }
    }
}
