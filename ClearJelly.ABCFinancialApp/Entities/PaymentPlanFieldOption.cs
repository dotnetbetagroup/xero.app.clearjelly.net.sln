﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class PaymentPlanFieldOption
    {
        [JsonProperty("fieldOptionCode")]
        public string FieldOptionCode { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }
}
