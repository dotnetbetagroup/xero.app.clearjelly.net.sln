﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubOnlineCcNames
    {
        public string CJId { get; set; }

        public string ClubOnlineId { get; set; }

        [JsonProperty("requireCCNameMatch")]
        public bool? RequireCcNameMatch { get; set; }

        [JsonProperty("differentCcNamesDisclaimer")]
        public string DifferentCcNamesDisclaimer { get; set; }
    }
}
//Example
//{
//      "requireCCNameMatch": "true",
//      "differentCcNamesDisclaimer": "string"
//}