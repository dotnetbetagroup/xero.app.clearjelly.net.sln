﻿using Newtonsoft.Json;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class ClubMemberIdentity
    {
        [JsonProperty("membershipTypeAbcCode")]
        public string MembershipTypeAbcCode { get; set; }

        [JsonProperty("homeVpdId")]
        public long HomeVpdId { get; set; }

        [JsonProperty("homeClub")]
        public int HomeClub { get; set; }

        [JsonProperty("homeCompanyName")]
        public string HomeCompanyName { get; set; }

        [JsonProperty("corpId")]
        public string CorpId { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
    }
}
//Example
//{
//    "membershipTypeAbcCode": "Gold",
//    "homeVpdId": 17,
//    "homeClub": "9003",
//    "homeCompanyName": "string",
//    "corpId": "string",
//    "memberId": "c41e5df2682a481b8396da9e10272390",
//    "barcode": "barcode1",
//    "firstName": "Mitch",
//    "lastName": "Conner",
//    "isActive": "true"
//}