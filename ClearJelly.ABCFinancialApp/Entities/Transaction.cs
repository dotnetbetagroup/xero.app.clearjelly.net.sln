﻿using System;

namespace ClearJelly.ABCFinancialApp.Entities
{
    public class Transaction
    {
        public string TransactionId { get; set; }

        public DateTime? TransactionTimestamp { get; set; }

        public string MemberId { get; set; }

        public int HomeClub { get; set; }

        public string EmployeeId { get; set; }

        public string ReceiptNumber { get; set; }

        public string StationName { get; set; }

        public string Return { get; set; }

        //public TransactionItems Items { get; set; }
    }
}
