﻿using ClearJelly.ABCFinancialApp.Models.OAuth;
using ClearJelly.Configuration;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsOAuthProvider
    {
        //Quote:Your vendor app_id and app_key will become the Client ID and Client Secret, respectively     
        private readonly string _appId = ConfigSection.ABCAppId;
        private readonly string _appKey = ConfigSection.ABCAppKey;
        private readonly string _redirectUri = ConfigSection.ABCRedirectURI;
        private readonly string _oAuthApiBase = "https://api.abcfinancial.com/uaa/oauth/";
        public string GetLoginUrl()
        {
            return $"https://oua.abcfinancial.com/uaa/oauth/authorize:443?client_id={_appId}&redirect_uri={_redirectUri}";
        }
        //Quote:The access token expires after 24 hours.
        //Quote:The refresh token will not expire
        public async Task<TokenResponse> GetToken(string authorizationCode)
        {
            var url = $"{_oAuthApiBase}token?grant_type=authorization_code&code={authorizationCode}&redirect_uri={_redirectUri}&client_id={_appId}&client_secret={_appKey}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers["app_id"] = _appId;
            request.Headers["app_key"] = _appKey;
            var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
            TokenResponse tokenResponse;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var resultString = streamReader.ReadToEnd();
                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(resultString);
            }
            //TODO:store token
            return tokenResponse;
        }

        public async Task<TokenResponse> RefreshToken(string refreshToken)
        {
            var url = $"{_oAuthApiBase}token?grant_type=refresh_token&refresh_token={refreshToken}&client_id={_appId}&client_secret={_appKey}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers["app_id"] = _appId;
            request.Headers["app_key"] = _appKey;
            var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
            TokenResponse tokenResponse;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var resultString = streamReader.ReadToEnd();
                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(resultString);
            }
            //TODO:store token
            return tokenResponse;
        }

        public async Task<bool> ValidateToken(string accessToken)
        {
            var url = $"{_oAuthApiBase}validateToken?user={_appId}&token={accessToken}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers["app_id"] = _appId;
            request.Headers["app_key"] = _appKey;
            var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
            //Quote: A successful response will return the status message "Success - Access token validated. - 0006" and the oauthMemberId
            var validationSuccessCode = 0006;//check
            return (int)httpResponse.StatusCode == validationSuccessCode ? true : false;
        }
    }
}

