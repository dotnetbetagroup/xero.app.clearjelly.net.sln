﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Clubs;
using ClearJelly.ABCFinancialApp.Models.Clubs.Clubs;
using ClearJelly.ABCFinancialApp.Models.Clubs.Details;
using ClearJelly.ABCFinancialApp.Models.Clubs.MembersIdentities;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.AnnualFees;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.BillingCatalogItems;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.PaymentMethods;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.Plan;
using ClearJelly.ABCFinancialApp.Models.Clubs.Stations;
using ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsClubsApiProvider
    {
        private readonly ABCFinancialsClubsRepository _clubsRepository;
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd";
        private string connectionString;

        public ABCFinancialsClubsApiProvider(string dbName)
        {
            connectionString = ConfigSection.CompanyDBConnection + ";database=ABC_Financial_" + dbName + ";";
            if (ConfigSection.DevelopersType == Developers.AzureDeveloper)
            {
                connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
            }


            _clubsRepository = new ABCFinancialsClubsRepository(connectionString);
        }

        public async Task<bool> GetClub(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsResponse>(requestModel.ApiUrl, "", clubNumber, startDate, endDate);
                Task.Run(() => { _clubsRepository.InsertClub(res.Club); });

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetClubCheckinsDetails(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "clubs/checkins/details";
                requestModel.DateParam = "checkInTimestampRange";
                requestModel.MaxDays = 31;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                foreach (var item in requestModel.Dates)
                {
                    var res = await WebRequestHelper.GetAbcDataAsync<ClubsCheckinsDetailsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, item.StartDate, item.EndDate);
                    if (res.Checkins != null)
                    {
                        Task.Run(() => { _clubsRepository.InsertClubCheckins(res.Checkins, clubNumber); }).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private string GetClubCheckingDetailsDatesString(DateTime? startDate = null, DateTime? endDate = null)
        {
            if (startDate.HasValue && endDate.HasValue)
            {
                return $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
            }
            return $"{DateTime.Now.ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(2).ToString(_apiDateTimeFromat)}";
        }

        public async Task<ClubMemberIdentity> GetClubMemberIdentitieByBarcode2(string barcode, string clubNumber)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qew/clubs/members/identities?barcode=qwe
                var apiRoute = "clubs/members/identities";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}?barcode={barcode}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubMemberIdentity clubMemberIdentity;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<MembersIdentitiesResponse>(resultString);
                    clubMemberIdentity = parsedResonse.Identities.FirstOrDefault();
                }
                return clubMemberIdentity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<ClubMemberIdentity> GetClubMemberIdentitieByMemberId(string memberId, string clubNumber)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/members/qwe/identities
                var apiRoute = $"clubs/members/{memberId}/identities";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubMemberIdentity clubMemberIdentity;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<MembersIdentitiesResponse>(resultString);
                    clubMemberIdentity = parsedResonse.Identities.FirstOrDefault();
                }
                return clubMemberIdentity;
            }
            catch (Exception)
            {
                return null;
            }
        }


        private string GetClubTransactionPosDatesString(DateTime? startDate = null, DateTime? endDate = null)
        {
            if (startDate.HasValue && endDate.HasValue)
            {
                return $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
            }
            return $"{DateTime.Now.AddDays(-179).ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(1).ToString(_apiDateTimeFromat)}";
        }

        public async Task<bool> GetClubTransactionPos(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "clubs/transactions/pos";
                requestModel.DateParam = "transactionTimestampRange";
                requestModel.MaxDays = 10;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                foreach (var item in requestModel.Dates)
                {
                    var res = await WebRequestHelper.GetAbcDataAsync<ClubsTransactionsPosResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, item.StartDate, item.EndDate);
                    if (res.Clubs != null)
                    {
                        Task.Run(() => { _clubsRepository.InsertClubTransaction(res.Clubs, clubNumber); }).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<ClubTransactionModel> GetClubTransactionPosByTransactionId(string transactionId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe&memberId=memb&page=12&size=13
                var apiRoute = "clubs/transactions/pos";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{transactionId}?transactionTimestampRange={GetClubTransactionPosDatesString(startDate, endDate)}";

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubTransactionModel club;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsTransactionsPosResponse>(resultString);
                    club = parsedResonse.Clubs.FirstOrDefault();
                }
                return club;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> GetClubPlans(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<PlansResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                Task.Run(() => { _clubsRepository.InsertClubPlans(res.Plans); });

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetClubPlansAnnualFees(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans/annualfees";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsPlansAnnualFeesResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                Task.Run(() => { _clubsRepository.InsertClubPlansAnnualFees(res.AnnualFees); });

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        //clear full table
        public async Task<bool> GetClubPlansBillingCatalogItems(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans/billingcatalogitems";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsPlansBillingCatalogItemsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                Task.Run(() => { _clubsRepository.InsertBillingCatalogItems(res.BillingCatalogItems); });

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetClubPlansPaymentMethods(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "clubs/plans/paymentmethods";
                requestModel.DateParam = "";
                requestModel.MaxDays = 10;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                foreach (var item in requestModel.Dates)
                {
                    var res = await WebRequestHelper.GetAbcDataAsync<ClubsPlansPaymentMethodsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, item.StartDate, item.EndDate);
                    if (res.PaymentMethods != null)
                    {
                        Task.Run(() => { _clubsRepository.InsertClubPlansPaymentMethods(res.PaymentMethods); }).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        //clear full table
        public async Task<PaymentPlanModel> GetClubPlanById(string planId, string clubNumber)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/plans/Id               
                var apiRoute = "clubs/plans";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{planId}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                PaymentPlanModel clubPaymentPlan;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<PlanResponse>(resultString);
                    clubPaymentPlan = parsedResonse.PaymentPlan;
                }
                return clubPaymentPlan;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> GetClubStations(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/stations";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsStationsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                Task.Run(() => { _clubsRepository.InsertСlubStations(res.Stations); }).Wait();

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}

