﻿using ClearJelly.Configuration;
using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsClubGenerator
    {
        private readonly Logger _logger;
        private string _connection;
        private string _companyName;

        public ABCFinancialsClubGenerator(string companyName)
        {
            _companyName = companyName;
            _logger = LogManager.GetCurrentClassLogger();
            _connection = ConfigSection.AbcCompanyDbConnection + _companyName;
        }

        //public async Task GenerateTables()
        //{
        //    var listTasks = new List<Task>();

        //    listTasks.Add(Task.Run(() => CreateClubInformationTable()));
        //    listTasks.Add(Task.Run(() => CreateOnlineTable()));
        //    listTasks.Add(Task.Run(() => CreateMinorsTable()));
        //    listTasks.Add(Task.Run(() => CreateCCNamesTable()));
        //    listTasks.Add(Task.Run(() => CreateCountriesTable()));
        //    listTasks.Add(Task.Run(() => CreateCountriesInClubTable()));
        //    listTasks.Add(Task.Run(() => CreateCreditCardPaymentMethodsTable()));
        //    listTasks.Add(Task.Run(() => CreateClubPaymentMethodsTable()));
        //    listTasks.Add(Task.Run(() => CreateCheckinsTable()));
        //    listTasks.Add(Task.Run(() => CreateMembersTable()));
        //    listTasks.Add(Task.Run(() => CreateIdentitiesTable()));
        //    listTasks.Add(Task.Run(() => CreatePOSTransactionsTable()));
        //    listTasks.Add(Task.Run(() => CreateItemsTable()));
        //    listTasks.Add(Task.Run(() => CreateItemsInPOSTransactionsTable()));
        //    listTasks.Add(Task.Run(() => CreateWebPlanListsTable()));
        //    listTasks.Add(Task.Run(() => CreateAnnualFeesTable()));
        //    listTasks.Add(Task.Run(() => CreateClubNumbersInAnnualFeesTable()));
        //    listTasks.Add(Task.Run(() => CreateBillingCatalogItemTable()));
        //    listTasks.Add(Task.Run(() => CreateClubNumbersInBillingCatalogItemTable()));
        //    listTasks.Add(Task.Run(() => CreateTaxResponseTable()));
        //    listTasks.Add(Task.Run(() => CreateTaxResponsesInBillingCatalogItemTable()));
        //    listTasks.Add(Task.Run(() => CreatePaymentMethodsTable()));
        //    listTasks.Add(Task.Run(() => CreateClubAccountIdNameTable()));
        //    listTasks.Add(Task.Run(() => CreateAbcBillingIdNameTable()));
        //    listTasks.Add(Task.Run(() => CreateWebPlanTable()));
        //    listTasks.Add(Task.Run(() => CreateWebPaymentTable()));
        //    listTasks.Add(Task.Run(() => CreateWebScheduleTable()));
        //    listTasks.Add(Task.Run(() => CreateWebUserDefinedFieldTable()));
        //    listTasks.Add(Task.Run(() => CreateWebFeeTable()));
        //    listTasks.Add(Task.Run(() => CreateFieldOptionTable()));
        //    listTasks.Add(Task.Run(() => CreateStationTable()));
        //    await Task.WhenAll(listTasks);
        //}

        public async Task GenerateTables()
        {
            //var listTasks = new List<Task>();
            //listTasks.Add(Task.Run(() => CreateNewCheckinsTable()));

            //listTasks.Add(Task.Run(() => CreateNewPOSTransactionsTable()));
            //listTasks.Add(Task.Run(() => CreateNewItemsTable()));
            //listTasks.Add(Task.Run(() => CreateNewPaymentsTable()));
            CreateNewCheckinsTable();
            CreateNewPOSTransactionsTable();
            CreateNewItemsTable();
            CreateNewPaymentsTable();
            //await Task.WhenAll(listTasks);
        }

        #region OldMethods
        private void CreateClubInformationTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ClubInformation](
					            [ClubInformationId] [varchar](400) NOT NULL, [Name] [varchar](400) NULL,
					            [ShortName] [varchar](400) NULL, [TimeZone] [varchar](400) NULL,
					            [Address1] [varchar](400) NULL, [Address2] [varchar](400) NULL,
					            [City] [varchar](400) NULL, [State] [varchar](400) NULL,
					            [PostalCode] [varchar](400) NULL, [Country] [varchar](400) NULL,
					            [Email] [varchar](400) NULL, [DonationItem] [varchar](400) NULL,
					            [OnlineSignupAllowedPaymentMethods] [int] NULL, [OnlineId] [varchar](400) NULL,
					            [BillingCountry] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ClubInformation table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateOnlineTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Online](
					            [OnlineId] [varchar](400) NOT NULL, [MinorsId] [varchar](400) NULL,
					            [CCNamesId] [varchar](400) NULL, [ShowFees] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Online table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMinorsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Minors](
					            [MinorsId] [varchar](400) NOT NULL, [AllowMinors] [bit] NULL,
					            [MinorAge] [varchar](400) NULL, [MinorDisclaimer] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Minors table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateCCNamesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CCNames](
					            [CCNamesId] [varchar](400) NOT NULL, [RequireCcNameMatch] [bit] NULL,
					            [DifferentCcNamesDisclaimer] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC CCNames table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateCountriesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Countries](
					            [CountryId] [varchar](400) NOT NULL, [Name] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Countries table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateCountriesInClubTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CountriesInClub](
					            [CountriesInClub] [varchar](400) NOT NULL, [ClubInformationId] [varchar](400) NULL,
					            [CountryId] [varchar](400) NULL) ON [PRIMARY];	";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC CountriesInClub table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateCreditCardPaymentMethodsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CreditCardPaymentMethods](
					            [PaymentMethodId] [varchar](400) NOT NULL, [Name] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC CreditCardPaymentMethods table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateClubPaymentMethodsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ClubPaymentMethods](
					            [Id] [varchar](400) NULL, [PaymentMethodId] [varchar](400) NULL,
					            [ClubInformationId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ClubPaymentMethods table db for {_companyName} exception:{ex.Message}");
            }
        }

        //private void CreateCheckinsTable()
        //{
        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(_connection))
        //        {
        //            var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Checkins](
					   //         [CheckInId] [varchar](400) NULL, [CheckInTimestamp] datetime2 NULL,
					   //         [CheckInMessage] [varchar](400) NULL, [StationName] [varchar](400) NULL,
					   //         [CheckInStatus] [varchar](400) NULL, [MemberId] [varchar](400) NULL) ON [PRIMARY];";
        //            connection.Execute(query);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error($"Create ABC Checkins table db for {_companyName} exception:{ex.Message}");
        //    }
        //}

        private void CreateMembersTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Members](
					            [MemberId] [varchar](400) NULL, [HomeClub] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Members table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateIdentitiesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Identities](
					            [IdentityId] [varchar](400) NULL, [MembershipTypeAbcCode] [varchar](400) NULL,
					            [HomeVpdId] [int] NULL, [HomeClub] [varchar](400) NULL,
					            [HomeCompanyName] [varchar](400) NULL, [CorpId] [varchar](400) NULL,
					            [MemberId] [varchar](400) NULL, [Barcode] [varchar](400) NULL,
					            [FirstName] [varchar](400) NULL, [LastName] [varchar](400) NULL,
					            [IsActive] [bit] NULL) ON [PRIMARY]; ";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Identities table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreatePOSTransactionsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POSTransactions](
					            [TransactionId] [varchar](400) NULL, [TransactionTimestamp] datetime2 NULL,
					            [MemberId] [varchar](400) NULL, [HomeClub] [varchar](400) NULL,
					            [EmployeeId] [varchar](400) NULL, [ReceiptNumber] [varchar](400) NULL,
					            [StationName] [varchar](400) NULL, [Return] [bit] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC POSTransactions table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateItemsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POS_transItems](
					            [ItemId] [varchar](400) NULL, [Name] [varchar](400) NULL,
					            [InventoryType] [int] NULL, [Sale] [bit] NULL,
					            [Upc] [varchar](400) NULL, [ProfitCenter] [varchar](400) NULL,
					            [Catalog] [varchar](400) NULL, [UnitPrice] [varchar](400) NULL,
					            [Quantity] [varchar](400) NULL, [PackageQuantity] [varchar](400) NULL,
					            [Subtotal] [varchar](400) NULL, [Tax] [varchar](400) NULL,
					            [RecurringServiceId] [varchar](400) NULL, [TransactionId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Items table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateItemsInPOSTransactionsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ItemsInPOSTransactions](
					            [Id] [varchar](400) NOT NULL, [ItemId] [varchar](400) NULL,
					            [TransactionId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ItemsInPOSTransactions table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateWebPlanListsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[WebPlanLists](
					            [PlanId] [varchar](400) NULL, [PlanName] [varchar](400) NULL,
					            [PromoCode] [varchar](400) NULL, [PromoName] [varchar](400) NULL,
					            [AgreementDescription] [nvarchar](MAX) NULL, [LimitedAvailability] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC WebPlanLists table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateAnnualFeesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[AnnualFees](
					            [Id] [varchar](400) NULL, [Name] [varchar](400) NULL,
					            [Amount] [decimal](22, 4) NULL, [PreTaxAmount] [decimal](22, 4) NULL,
					            [ProfitCenterAbcCode] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC AnnualFees table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateClubNumbersInAnnualFeesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ClubNumbersInAnnualFees](
					            [Id] [varchar](400) NULL, [ClubNumber] [varchar](400) NULL,
					            [AnnualFeeId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ClubNumbersInAnnualFees table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateBillingCatalogItemTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[BillingCatalogItem](
					            [Id] [varchar](400) NULL, [Name] [varchar](400) NULL,
					            [ProfitCenterAbcCode] [varchar](400) NULL, [AnnualFee] [bit] NULL,
					            [TotalTaxRate] [decimal](22, 4) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC BillingCatalogItem table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateClubNumbersInBillingCatalogItemTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ClubNumbersInBillingCatalogItem](
					            [Id] [varchar](400) NULL, [ClubNumber] [varchar](400) NULL,
					            [BillingCatalogItemId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ClubNumbersInBillingCatalogItem table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateTaxResponseTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[TaxResponse](					
					            [RateId] [varchar](400) NOT NULL PRIMARY KEY, [Amount] [varchar](400) NULL,
					            [Name] [varchar](400) NULL, [Percentage] [decimal](22, 4) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC TaxResponse table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateTaxResponsesInBillingCatalogItemTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[TaxResponsesInBillingCatalogItem](
					            [Id] [varchar](400) NULL, [TaxResponseId] [varchar](400) NULL,
					            [BillingCatalogItemId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC TaxResponsesInBillingCatalogItem table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreatePaymentMethodsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[PaymentMethods](
					            [Id] [varchar](400) NULL, [ClubAccountIdNameId] [varchar](400) NULL,
					            [AbcBillingIdNameId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC PaymentMethods table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateClubAccountIdNameTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ClubAccountIdName](
					            [CJId] [varchar](400) NULL, [Id] [varchar](400) NULL,
					            [Name] [varchar](400) NULL, [AbcCode] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ClubAccountIdName table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateAbcBillingIdNameTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[AbcBillingIdName](
					            [CJId] [varchar](400) NULL, [Id] [varchar](400) NULL,
					            [Name] [varchar](400) NULL, [AbcCode] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC AbcBillingIdName table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateWebPlanTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[WebPlan](
					            [PlanName] [varchar](400) NULL, [PlanId] [varchar](400) NULL,
					            [PromotionCode] [varchar](400) NULL, [PromotionName] [varchar](400) NULL,
					            [MembershipType] [varchar](400) NULL, [AgreementTerm] [varchar](400) NULL,
					            [ScheduleFrequency] [varchar](400) NULL, [TermInMonths] [varchar](400) NULL,
					            [DueDay] [varchar](400) NULL, [FirstDueDate] datetime2 NULL,
					            [ActivePresale] [bit] NULL, [ExpirationDate] [varchar](400) NULL,
					            [OnlineSignupAllowedPaymentMethods] [varchar](400) NULL, [PreferredPaymentMethod] [varchar](400) NULL,
					            [TotalContractValue] [varchar](400) NULL, [DownPaymentName] [varchar](400) NULL,
					            [DownPaymentTotalAmount] [varchar](400) NULL, [ScheduleTotalAmount] [varchar](400) NULL,
					            [AgreementTerms] [varchar](400) NULL, [AgreementDescription] [varchar](4000) NULL,
					            [AgreementNote] [varchar](400) NULL, [EmailGreeting] [varchar](400) NULL,
					            [EmailClosing] [varchar](400) NULL, [ClubFeeTotalAmount] [varchar](400) NULL,
					            [PlanValidation] [int] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC WebPlan table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateWebPaymentTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[WebPayment](
					            [PlanId] [varchar](400) NULL, [SubTotal] [varchar](400) NULL,
					            [Tax] [varchar](400) NULL, [Total] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC WebPayment table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateWebScheduleTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[WebSchedule](
					            [PlanId] [varchar](400) NULL, [ProfitCenter] [varchar](400) NULL,
					            [ScheduleDueDate] [varchar](400) NULL, [ScheduleAmount] [varchar](400) NULL,
					            [NumberOfPayments] [varchar](400) NULL, [Recurring] [bit] NULL,
					            [Addon] [bit] NULL, [DefaultChecked] [bit] NULL,
					            [Description] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC WebSchedule table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateWebUserDefinedFieldTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[WebUserDefinedField](
					            [PlanId] [varchar](400) NULL, [Field] [varchar](400) NULL,
					            [Table] [int] NULL, [Index] [int] NULL,
					            [Type] [varchar](400) NULL, [Required] [bit] NULL,
					            [Values] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC WebUserDefinedField table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateWebFeeTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[WebFee](
					            [PlanId] [varchar](400) NULL, [FeeDueDate] datetime2 NULL,
					            [FeeName] [varchar](400) NULL, [FeeAmount] [varchar](400) NULL,
					            [FeeApply] [bit] NULL, [FeeRecurring] [bit] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC WebFee table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateFieldOptionTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[FieldOption](
					            [PlanId] [varchar](400) NULL, [FieldOptionCode] [varchar](400) NULL,
					            [State] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC FieldOption table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateStationTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Station](
                                [StationId][varchar](400) NULL, [Name] [varchar] (400) NULL,
					            [Status] [varchar] (400) NULL, [AbcCode] [varchar] (400) NULL) ON[PRIMARY]";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Station table db for {_companyName} exception:{ex.Message}");
            }
        }
        #endregion

        private void CreateNewPOSTransactionsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POSTransactions](
					            [TransactionId] [varchar](400) not NULL,  PRIMARY KEY ([TransactionId]), [TransactionTimestamp] datetime2 NULL,
					            [MemberId] [varchar](400) NULL, [HomeClub] [varchar](400) NULL,
					            [EmployeeId] [varchar](400) NULL, [ReceiptNumber] [varchar](400) NULL,
					            [StationName] [varchar](400) NULL, [Return] [bit] NULL,
                                [ClubNumber] [int] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC POSTransactions table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateNewItemsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POS_transItems](
					            [ItemId] [varchar](400) NOT NULL, [Name] [varchar](400) NULL,
								PRIMARY KEY ([ItemId]),[InventoryType] [int] NULL, [Sale] [bit] NULL,
					            [Upc] [varchar](400) NULL, [ProfitCenter] [varchar](400) NULL,
					            [Catalog] [varchar](400) NULL, [UnitPrice] [varchar](400) NULL,
					            [Quantity] [varchar](400) NULL, [PackageQuantity] [varchar](400) NULL,
					            [Subtotal] [varchar](400) NULL, [Tax] [varchar](400) NULL,
					            [RecurringServiceId] [varchar](400) NULL, [TransactionId] [varchar] (400) NOT NULL ,
                                [ClubNumber] [int] NULL, CONSTRAINT [TransactionId]
								FOREIGN KEY ([TransactionId]) REFERENCES [POSTransactions]([TransactionId])
								ON DELETE CASCADE) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Items table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateNewPaymentsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POS_transPayments](
					            [TransactionId] [varchar](400) NOT NULL, [ItemId] [varchar](400) NOT NULL,
                                [PaymentType] [varchar](400) NULL, [PaymentAmount] [varchar](400) NULL,
                                [PaymentTax] [varchar](400) NULL, [ClubNumber] [int] NULL, CONSTRAINT [ItemId]
								FOREIGN KEY ([ItemId]) REFERENCES [POS_transItems]([ItemId])
								ON DELETE CASCADE) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Payments table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateNewCheckinsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CheckinsDetails](
					            [CheckInId] [varchar](400) NULL, [CheckInTimestamp] datetime2 NULL,
                                [StationName] [varchar](400) NULL, [CheckInStatus] [varchar](400) NULL, 
                                [MemberId] [varchar](400) NULL, [HomeClub] [varchar](400),
                                [ClubNumber] [int] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Checkins table db for {_companyName} exception:{ex.Message}");
            }
        }
    }
}
