﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Members;
using ClearJelly.ABCFinancialApp.Models.Members.Checkins.Details;
using ClearJelly.ABCFinancialApp.Models.Members.Checkins.Summaries;
using ClearJelly.ABCFinancialApp.Models.Members.Children;
using ClearJelly.ABCFinancialApp.Models.Members.Groups;
using ClearJelly.ABCFinancialApp.Models.Members.Members;
using ClearJelly.ABCFinancialApp.Models.Members.Personals;
using ClearJelly.ABCFinancialApp.Models.Members.Prospects;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.Configuration;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsMembersApiProviderWithPaging
    {
        private readonly ABCFinancialsMembersRepository _membersRepository;
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd";
        private string connectionString;
        private static DateTime globalEndDate = new DateTime();
        private readonly Logger _logger;

        public ABCFinancialsMembersApiProviderWithPaging(string dbName)
        {
            connectionString = ConfigSection.CompanyDBConnection + ";database=ABC_Financial_" + dbName + ";";
            
            if (ConfigSection.DevelopersType == Developers.AzureDeveloper)
            {
                connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
            }
            _membersRepository = new ABCFinancialsMembersRepository(connectionString);

        }

        public async Task<bool> GetMembers(string club, DateTime startDate, DateTime endDate, int? nextPage = 1)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "members";
                requestModel.DateParam = "lastModifiedTimestampRange";
                var members = await WebRequestHelper.GetAbcDataWithPaginationAsync<MembersResponse>(requestModel.ApiUrl, requestModel.DateParam, club, null, startDate, endDate, nextPage);
                if (members != null && members.Members != null)
                {
                     _membersRepository.InsertMembers(members.Members, club);
                }
                if (members.Status.NextPage != null)
                {
                    GetMembers(club, startDate, endDate, members.Status.NextPage).Wait();
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"GetMembers exception: {ex.Message}");
                return false;
            }
            return true;
        }
        
        public async Task<bool> GetMembersCheckinsSummaries(string club, DateTime startDate, DateTime endDate, int? nextPage = 1)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "members/checkins/summaries";
                requestModel.DateParam = "checkInTimestampRange";
                var maxDays = 31;
                if(globalEndDate == new DateTime())
                {
                    globalEndDate = endDate;
                }
                var duration = (endDate - startDate).Days;
                if (duration > maxDays)
                {
                    endDate = startDate.AddDays(maxDays);
                }
                var members = await WebRequestHelper.GetAbcDataWithPaginationAsync<SummariesResponse>(requestModel.ApiUrl, requestModel.DateParam, club, null, startDate, endDate, nextPage);
                if (members != null && members.Members != null)
                {
                    _membersRepository.InsertMembersCheckinsSummaries(members.Members);
                }
                if (members.Status.NextPage != null)
                {
                    GetMembersCheckinsSummaries(club, startDate, endDate, members.Status.NextPage).Wait();
                }
                var globalDuration = (globalEndDate - endDate).Days;
                if (globalDuration > 0)
                {
                    startDate = endDate;
                    endDate = endDate.AddDays(maxDays);
                    GetMembersCheckinsSummaries(club, startDate, endDate).Wait();
                }
                globalEndDate = new DateTime();

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        
        public async Task<bool> GetMembersGroups(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "members/groups";
                //requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                var members = await WebRequestHelper.GetAbcDataAsync<GroupsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                if (members != null && members.Groups != null)
                {
                    Task.Run(() => { _membersRepository.InsertMembersGroups(members.Groups); }).Wait();
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetMembersPersonals(string club, DateTime startDate, DateTime endDate, int? nextPage = 1)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "members/personals";
                requestModel.DateParam = "lastModifiedTimestampRange";
                {
                    var members = await WebRequestHelper.GetAbcDataWithPaginationAsync<MembersPersonalsResponse>(requestModel.ApiUrl, requestModel.DateParam, club, null, startDate, endDate, nextPage);
                    if (members != null)
                    {
                        _membersRepository.InsertMembersPersonals(members.Members);
                    }
                    if (members.Status.NextPage != null)
                    {
                        GetMembersPersonals(club, startDate, endDate, members.Status.NextPage).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
       
    }
}
