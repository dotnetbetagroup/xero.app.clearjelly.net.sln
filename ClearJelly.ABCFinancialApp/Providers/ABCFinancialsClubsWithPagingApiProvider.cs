﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Clubs;
using ClearJelly.ABCFinancialApp.Models.Clubs.Clubs;
using ClearJelly.ABCFinancialApp.Models.Clubs.Details;
using ClearJelly.ABCFinancialApp.Models.Clubs.MembersIdentities;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.AnnualFees;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.BillingCatalogItems;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.PaymentMethods;
using ClearJelly.ABCFinancialApp.Models.Clubs.Plans.Plan;
using ClearJelly.ABCFinancialApp.Models.Clubs.Stations;
using ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.Configuration;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsClubsWithPagingApiProvider
    {
        private readonly ABCFinancialsClubsRepository _clubsRepository;
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd";
        private string connectionString;
        private readonly Logger _logger;
        private static DateTime globalEndDate = new DateTime();

        public ABCFinancialsClubsWithPagingApiProvider(string dbName)
        {
            _logger = LogManager.GetCurrentClassLogger();
            connectionString = ConfigSection.CompanyDBConnection + ";database=ABC_Financial_" + dbName + ";";
            if (ConfigSection.DevelopersType == Developers.AzureDeveloper)
            {
                connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
            }


            _clubsRepository = new ABCFinancialsClubsRepository(connectionString);
        }

        public async Task<ClubsResponse> GetClub(string clubNumber, DateTime? startDate = null, DateTime? endDate = null, int? nextPage = 1)
        {
            var club = new ClubsResponse();
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs";
                var res = WebRequestHelper.GetAbcDataWithPaginationAsync<ClubsResponse>(requestModel.ApiUrl, "", clubNumber, 5000, startDate, endDate, nextPage);
                res.Wait();
                club = res.Result;
                //_clubsRepository.InsertClub(res.Club);
            }
            catch (Exception ex)
            {
                return club;
            }
            return club;
        }

        public async Task<bool> GetClubCheckinsDetails(string clubNumber, DateTime startDate, DateTime endDate)
        {
            _logger.Info("GetClubCheckinsDetails started");
            try
            {
                int maxDays = 29;
                if ((endDate - startDate).Days > maxDays)
                {
                    GetClubCheckinsDetailsForMonthsAsync(clubNumber, maxDays, startDate, endDate);
                    _logger.Info("GetClubCheckinsDetails ok");
                    return true;
                }
                if ((endDate - startDate).Days <= maxDays)
                {
                    await GetClubCheckinsDetailsPaging(clubNumber, startDate, endDate);
                    _logger.Info("GetClubCheckinsDetails ok");
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error($"GetClubCheckinsDetails ex: {ex.Message}");
                if (ex.InnerException != null)
                {
                    _logger.Error($"GetClubCheckinsDetails inner ex: {ex.InnerException.Message}");
                }
                return false;
            }

        }

        public async Task<bool> GetClubCheckinsDetailsPaging(string clubNumber, DateTime startDate, DateTime endDate, int? nextPage = 1)
        {
            _logger.Info("GetClubCheckinsDetailsPaging started");
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/checkins/details";
                requestModel.DateParam = "checkInTimestampRange";
                var res = await WebRequestHelper.GetAbcDataWithPaginationAsync<ClubsCheckinsDetailsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, null, startDate, endDate, nextPage);
                if (res != null && res.Checkins != null)
                {
                    _logger.Info($"Get Checkins finished: get {res.Checkins.Count}, next Page {res.Status.NextPage}");
                    _clubsRepository.InsertClubCheckins(res.Checkins, clubNumber);
                }
                if (res.Status.NextPage != null)
                {
                    GetClubCheckinsDetailsPaging(clubNumber, startDate, endDate, res.Status.NextPage).Wait();
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"GetCheckins exception: {ex.Message}");
                return false;
            }
            return true;
        }

        public async void GetClubCheckinsDetailsForMonthsAsync(string clubNumber, int MaxDays, DateTime startDate, DateTime endDate)
        {
            _logger.Info("GetClubCheckinsDetailsForMonths started");
            try
            {
                var dates = DateHelper.GetDataByDatePeriod(MaxDays, startDate, endDate);
                foreach (var date in dates)
                {
                    await GetClubCheckinsDetailsPaging(clubNumber, date.StartDate, date.EndDate);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"GetClubCheckinsDetailsForMonths ex: {ex.Message}");
            }
        }

        private string GetClubCheckingDetailsDatesString(DateTime? startDate = null, DateTime? endDate = null)
        {
            if (startDate.HasValue && endDate.HasValue)
            {
                return $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
            }
            return $"{DateTime.Now.ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(2).ToString(_apiDateTimeFromat)}";
        }

        public async Task<ClubMemberIdentity> GetClubMemberIdentitieByBarcode2(string barcode, string clubNumber)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qew/clubs/members/identities?barcode=qwe
                var apiRoute = "clubs/members/identities";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}?barcode={barcode}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubMemberIdentity clubMemberIdentity;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<MembersIdentitiesResponse>(resultString);
                    clubMemberIdentity = parsedResonse.Identities.FirstOrDefault();
                }
                return clubMemberIdentity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<ClubMemberIdentity> GetClubMemberIdentitieByMemberId(string memberId, string clubNumber)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/members/qwe/identities
                var apiRoute = $"clubs/members/{memberId}/identities";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubMemberIdentity clubMemberIdentity;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<MembersIdentitiesResponse>(resultString);
                    clubMemberIdentity = parsedResonse.Identities.FirstOrDefault();
                }
                return clubMemberIdentity;
            }
            catch (Exception)
            {
                return null;
            }
        }


        private string GetClubTransactionPosDatesString(DateTime? startDate = null, DateTime? endDate = null)
        {
            if (startDate.HasValue && endDate.HasValue)
            {
                return $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
            }
            return $"{DateTime.Now.AddDays(-179).ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(1).ToString(_apiDateTimeFromat)}";
        }

        public async Task<bool> GetClubTransactionPos(string clubNumber, DateTime startDate, DateTime endDate, int? nextPage = 1)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/transactions/pos";
                requestModel.DateParam = "transactionTimestampRange";
                var maxDays = 177;
                int pageSize = 2000;
                if (globalEndDate == new DateTime())
                {
                    globalEndDate = endDate;
                }
                var duration = (endDate - startDate).Days;
                if (duration > maxDays)
                {
                    endDate = startDate.AddDays(maxDays);
                }
                var res = await WebRequestHelper.GetAbcDataWithPaginationAsync<ClubsTransactionsPosResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, pageSize, startDate, endDate, nextPage);
                if (res.Clubs != null)
                {
                     _clubsRepository.InsertClubTransaction(res.Clubs, clubNumber);
                }
                if (res.Status.NextPage != null)
                {
                    GetClubTransactionPos(clubNumber, startDate, endDate, res.Status.NextPage).Wait();
                }
                var globalDuration = (globalEndDate - endDate).Days;
                if (globalDuration > 0)
                {
                    startDate = endDate.AddDays(1);
                    endDate = endDate.AddDays(maxDays);
                    GetClubTransactionPos(clubNumber, startDate, endDate).Wait();
                }
                globalEndDate = new DateTime();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<ClubTransactionModel> GetClubTransactionPosByTransactionId(string transactionId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe&memberId=memb&page=12&size=13
                var apiRoute = "clubs/transactions/pos";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{transactionId}?transactionTimestampRange={GetClubTransactionPosDatesString(startDate, endDate)}";

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubTransactionModel club;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsTransactionsPosResponse>(resultString);
                    club = parsedResonse.Clubs.FirstOrDefault();
                }
                return club;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> GetClubPlans(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<PlansResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                _clubsRepository.InsertClubPlans(res.Plans);

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetClubPlansAnnualFees(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans/annualfees";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsPlansAnnualFeesResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                _clubsRepository.InsertClubPlansAnnualFees(res.AnnualFees);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        //clear full table
        public async Task<bool> GetClubPlansBillingCatalogItems(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans/billingcatalogitems";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsPlansBillingCatalogItemsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                _clubsRepository.InsertBillingCatalogItems(res.BillingCatalogItems);

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetClubPlansPaymentMethods(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/plans/paymentmethods";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsPlansPaymentMethodsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                if (res.PaymentMethods != null)
                {
                    _clubsRepository.InsertClubPlansPaymentMethods(res.PaymentMethods);
                }
                if (res.Status.NextPage != null)
                {
                    GetClubPlansPaymentMethods(clubNumber, startDate, endDate).Wait();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        //clear full table
        public async Task<PaymentPlanModel> GetClubPlanById(string planId, string clubNumber)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/plans/Id               
                var apiRoute = "clubs/plans";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{planId}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                PaymentPlanModel clubPaymentPlan;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<PlanResponse>(resultString);
                    clubPaymentPlan = parsedResonse.PaymentPlan;
                }
                return clubPaymentPlan;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> GetClubStations(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "clubs/stations";
                requestModel.DateParam = "";
                var res = await WebRequestHelper.GetAbcDataAsync<ClubsStationsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                _clubsRepository.InsertСlubStations(res.Stations);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
