﻿using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Members.Prospects;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
   public class ABCFinancialsProspectsApiProvider
    {
        private readonly ABCFinancialsProspectsRepository _prospectsRepository;
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd";
        private string connectionString;
        private static DateTime globalEndDate = new DateTime();
        private readonly Logger _logger;

        public ABCFinancialsProspectsApiProvider(string dbName)
        {
            connectionString = ConfigSection.CompanyDBConnection + ";database=ABC_Financial_" + dbName + ";";

            if (ConfigSection.DevelopersType == Developers.AzureDeveloper)
            {
                connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
            }
            _prospectsRepository = new ABCFinancialsProspectsRepository(connectionString);
        }

        public async Task<bool> GetProspects(string club, DateTime startDate, DateTime endDate, int? nextPage = 1)
        {
            try
            {
                var requestModel = new PaginationRequestModel();
                requestModel.ApiUrl = "prospects";
                requestModel.DateParam = "lastCheckInTimestampRange";
                {
                    var members = await WebRequestHelper.GetAbcDataWithPaginationAsync<ProspectsResponse>(requestModel.ApiUrl, requestModel.DateParam, club, null, startDate, endDate, nextPage);
                    if (members != null)
                    {
                        _prospectsRepository.InsertProspects(members.Prospects, Int32.Parse(club));
                    }
                    if (members.Status.NextPage != null)
                    {
                        GetProspects(club, startDate, endDate, members.Status.NextPage).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
