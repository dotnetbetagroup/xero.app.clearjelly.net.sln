﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Members;
using ClearJelly.ABCFinancialApp.Models.Members.Checkins.Details;
using ClearJelly.ABCFinancialApp.Models.Members.Checkins.Summaries;
using ClearJelly.ABCFinancialApp.Models.Members.Children;
using ClearJelly.ABCFinancialApp.Models.Members.Groups;
using ClearJelly.ABCFinancialApp.Models.Members.Members;
using ClearJelly.ABCFinancialApp.Models.Members.Personals;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.Configuration;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsMembersApiProvider
    {
        private readonly Logger _logger;
        private readonly ABCFinancialsMembersRepository _membersRepository;
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd hh:mm:ss.ffffff";
        private string connectionString;

        public ABCFinancialsMembersApiProvider(string dbName)
        {
            connectionString = ConfigSection.CompanyDBConnection + ";database=ABC_Financial_" + dbName + ";";

            if (ConfigSection.DevelopersType == Developers.AzureDeveloper)
            {
                connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
            }
            _membersRepository = new ABCFinancialsMembersRepository(connectionString);
            _logger = LogManager.GetCurrentClassLogger();
        }

        public async Task<bool> GetMembers(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "members";
                requestModel.DateParam = "lastModifiedTimestampRange";
                requestModel.MaxDays = 2;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                foreach (var item in requestModel.Dates)
                {
                    var members = await WebRequestHelper.GetAbcDataAsync<MembersResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, item.StartDate, item.EndDate);
                    if (members != null)
                    {
                        _logger.Info("members acquired");
                        Task.Run(() => { _membersRepository.InsertMembers(members.Members, clubNumber); }).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetMembersCheckinsSummaries(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "members/checkins/summaries";
                requestModel.DateParam = "checkInTimestampRange";
                requestModel.MaxDays = 30;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                foreach (var item in requestModel.Dates)
                {
                    var members = await WebRequestHelper.GetAbcDataAsync<SummariesResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, item.StartDate, item.EndDate);
                    if (members != null)
                    {
                        Task.Run(() => { _membersRepository.InsertMembersCheckinsSummaries(members.Members); }).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<MembersMemberModel> GetMembersByMemberId(string memberId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe3/members/qwe4
                //full url example
                //https://api.abcfinancial.com/rest/qwe3/members/qwe4?barcode=qe&activeStatus=active&memberStatus=active&joinStatus=prospect&agreementNumber=qwe&lastCheckInTimestampRange=qwe&lastModifiedTimestampRange=qwe&createdTimestampRange=qd&convertedDateRange=qwe&memberSinceDateRange=qwe&page=qwe&size=qwe
                var apiRoute = "members";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{memberId}";

                //check if correct propertie
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl = routeBaseUrl + $"?lastModifiedTimestampRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                MembersMemberModel member;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersResponse>(resultString);
                    member = parsedResponse.Members.FirstOrDefault();
                }
                return member;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MemberCheckinsDetailsMemberModel> GetMembersCheckinsDetailsByMemberId(string memberId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe3/members/checkins/details/qwe4?checkInTimestampRange=242344
                //full url example
                //https://api.abcfinancial.com/rest/qwe3/members/qwe4?barcode=qe&activeStatus=active&memberStatus=active&joinStatus=prospect&agreementNumber=qwe&lastCheckInTimestampRange=qwe&lastModifiedTimestampRange=qwe&createdTimestampRange=qd&convertedDateRange=qwe&memberSinceDateRange=qwe&page=qwe&size=qwe
                var apiRoute = "members/checkins/details";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{memberId}?checkInTimestampRange=";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddYears(-100).ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                MemberCheckinsDetailsMemberModel member;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<DetailsResponse>(resultString);
                    member = parsedResponse.Members.FirstOrDefault();
                }
                return member;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<CheckinDetailsSummariesMemberModel> GetMembersCheckinsSummariesByMemberId(string memberId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qe/members/checkins/summaries/we?checkInTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qe/members/checkins/summaries/we?barcode=qweqe&checkInTimestampRange=qwe&activeStatus=inactive&hasCheckIn=false&page=2&size=12
                var apiRoute = "members/checkins/summaries";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{memberId}?checkInTimestampRange=";//fill timestamp values
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddYears(-100).ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                CheckinDetailsSummariesMemberModel member;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<SummariesResponse>(resultString);
                    member = parsedResponse.Members.FirstOrDefault();
                }
                return member;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<MemberChildModel> GetMembersChildernByMemberChildId(string memberChildId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/33/members/children/44
                //full url example
                //https://api.abcfinancial.com/rest/33/members/children/44?barcode=2&activeStatus=inactive&agreementNumber=1&createdTimestampRange=2&page=2&size=3
                var apiRoute = "members/children";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{memberChildId}";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"?createdTimestampDateRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                MemberChildModel memberChild;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersChildrenResponse>(resultString);
                    memberChild = parsedResponse.MemberChildren.FirstOrDefault();
                }
                return memberChild;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> GetMembersGroups(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "members/groups";
                requestModel.MaxDays = 30;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                var members = await WebRequestHelper.GetAbcDataAsync<GroupsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, startDate, endDate);
                if (members != null)
                {
                    Task.Run(() => { _membersRepository.InsertMembersGroups(members.Groups); }).Wait();
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> GetMembersPersonals(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var requestModel = new DateRequestModel();
                requestModel.ApiUrl = "members/personals";
                requestModel.DateParam = "lastModifiedTimestampRange";
                requestModel.MaxDays = 5;
                requestModel.Dates = DateHelper.GetDataByDatePeriod(requestModel.MaxDays, startDate, endDate);
                foreach (var item in requestModel.Dates)
                {
                    var members = await WebRequestHelper.GetAbcDataAsync<MembersPersonalsResponse>(requestModel.ApiUrl, requestModel.DateParam, clubNumber, item.StartDate, item.EndDate);
                    if (members != null)
                    {
                        Task.Run(() => { _membersRepository.InsertMembersPersonals(members.Members); }).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<PersonalsMemberModel> GetMembersPersonalsByMemberId(string memberId, string clubNumber, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/33/members/personals/23424
                //full url example
                //https://api.abcfinancial.com/rest/33/members/personals/23424?barcode=123&activeStatus=active&memberStatus=problem&joinStatus=member&agreementNumber=123&lastCheckInTimestampRange=123&lastModifiedTimestampRange=123&createdTimestampRange=123&convertedDateRange=123&memberSinceDateRange=123&page=123&size=123
                var apiRoute = "members/personals";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute, clubNumber)}/{memberId}";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"?lastmodifiedTimestampRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                PersonalsMemberModel personalsMember;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersPersonalsResponse>(resultString);
                    personalsMember = parsedResponse.Members.FirstOrDefault();
                }
                return personalsMember;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

