﻿using ClearJelly.Configuration;
using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Providers
{
    public class ABCFinancialsMemberGenerator
    {
        private readonly Logger _logger;
        private string _connection;
        private string _companyName;

        public ABCFinancialsMemberGenerator(string companyName)
        {
            _companyName = companyName;
            _logger = LogManager.GetCurrentClassLogger();
            _connection = ConfigSection.AbcCompanyDbConnection + _companyName;
        }

        //public async Task GenerateTables()
        //{
        //    var listTasks = new List<Task>();

        //    listTasks.Add(Task.Run(() => CreateMemberTable()));
        //    listTasks.Add(Task.Run(() => CreatePersonalTable()));
        //    listTasks.Add(Task.Run(() => CreateAgreementTable()));
        //    listTasks.Add(Task.Run(() => CreatePrimaryBillingAccountHolderTable()));
        //    listTasks.Add(Task.Run(() => CreateAlertInMemberTable()));
        //    listTasks.Add(Task.Run(() => CreateAlertTable()));
        //    listTasks.Add(Task.Run(() => CreateMemberCheckinDetailTable()));
        //    listTasks.Add(Task.Run(() => CreateCheckinDetailTable()));
        //    listTasks.Add(Task.Run(() => CreatePaymentsTable()));
        //    listTasks.Add(Task.Run(() => CreateMemberCheckinSummariesTable()));
        //    listTasks.Add(Task.Run(() => CreateLinksTable()));
        //    listTasks.Add(Task.Run(() => CreatePaymentsInItemsTable()));
        //    listTasks.Add(Task.Run(() => CreateCheckInCountTable()));
        //    listTasks.Add(Task.Run(() => CreateMemberChildTable()));
        //    listTasks.Add(Task.Run(() => CreateMemberChildEmergencyContactTable()));
        //    listTasks.Add(Task.Run(() => CreateMemberChildNoteTable()));
        //    listTasks.Add(Task.Run(() => CreateChildNotesTable()));
        //    listTasks.Add(Task.Run(() => CreateGroupTable()));
        //    listTasks.Add(Task.Run(() => CreateMemberPersonalTable()));
        //    await Task.WhenAll(listTasks);
        //}

        public async Task GenerateTables()
        {
            CreateNewTables();
            //CreateNewMembersTable();
            //CreateNewAgreementTable();
            //CreateProspectsTable();
            //var listTasks = new List<Task>();
            //listTasks.Add(Task.Run(() => CreateNewMembersTable()));
            //listTasks.Add(Task.Run(() => CreateNewAgreementTable()));
            //listTasks.Add(Task.Run(() => CreateProspectsTable()));
            //await Task.WhenAll(listTasks);
        }
        private void CreateNewTables()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Members](
                                [MemberId][varchar](400) NOT NULL, PRIMARY KEY ([MemberId]), [FirstName] [varchar] (400) NULL,
					            [LastName] [varchar] (400) NULL, [MiddleInitial] [varchar] (400) NULL,
					            [AddressLine1] [varchar] (400) NULL, [AddressLine2] [varchar] (400) NULL,
					            [City] [varchar] (400) NULL, [State] [varchar] (400) NULL,
					            [PostalCode] [varchar] (400) NULL, [HomeClub] [varchar] (400) NULL,
					            [CountryCode] [varchar] (400) NULL, [Email] [varchar] (400) NULL,
					            [PrimaryPhone] [varchar] (400) NULL, [MobilePhone] [varchar] (400) NULL,
					            [WorkPhone] [varchar] (400) NULL, [WorkPhoneExt] [varchar] (400) NULL,
					            [EmergencyContactName] [varchar] (400) NULL, [EmergencyPhone] [varchar] (400) NULL,
					            [EmergencyExt] [varchar] (400) NULL, [EmergencyAvailability] [varchar] (400) NULL,
					            [Barcode] [varchar] (400) NULL, [BirthDate] datetime2 NULL,
                                [Gender] [int] NULL, [Employer] [varchar] (400) NULL,
					            [Occupation] [varchar] (400) NULL, [Group] [varchar] (400) NULL,
					            [IsActive] [bit] NULL, [MemberStatus] [varchar] (400) NULL,
					            [JoinStatus] [int] NULL, [IsConvertedProspect] [bit] NULL,
					            [HasPhoto] [bit] NULL, [ConvertedDate] datetime2 NULL,
                                [MemberStatusReason] [varchar] (400) NULL, [WellnessProgramId] [varchar] (400) NULL,
					            [FirstCheckInTimestamp] datetime2 NULL, [MemberStatusDate] datetime2 NULL,
                                [LastCheckInTimestamp] datetime2 NULL, [TotalCheckInCount] [int] NULL,
					            [CreateTimestamp] datetime2 NULL, [LastModifiedTimestamp] datetime2 NULL,
                                [ClubNumber] [int] NULL) ON[PRIMARY];

                                CREATE TABLE[ABC_Financial_" + _companyName + @"].[dbo].[Agreement](
                                [AgreementId][varchar](400) NOT NULL, [MemberId] [varchar] (400) NULL,
					            [AgreementNumber] [varchar] (400) NULL, [IsPrimaryMember] [bit] NULL,
					            [IsNonMember] [bit] NULL, [Ordinal] [int] NULL,
					            [ReferringMemberId] [varchar] (400) NULL, [ReferringMemberHomeClub] [varchar] (400) NULL,
					            [ReferringMemberName] [varchar] (400) NULL, [SalesPersonId] [varchar] (400) NULL,
					            [SalesPersonName] [varchar] (400) NULL, [SalesPersonHomeClub] [varchar] (400) NULL,
					            [PaymentPlan] [varchar] (400) NULL, [Term] [varchar] (400) NULL,
					            [PaymentFrequency] [varchar] (400) NULL, [MembershipType] [varchar] (400) NULL,
					            [ManagedType] [varchar] (400) NULL, [CampaignId] [varchar] (400) NULL,
					            [CampaignName] [varchar] (400) NULL, [CampaignGroup] [varchar] (400) NULL,
					            [IsPastDue] [bit] NULL, [DownPaymentPendingPOS] [varchar] (400) NULL,
					            [RenewalType] [varchar] (400) NULL, [AgreementPaymentMethod] [varchar] (400) NULL,
					            [DownPayment] [decimal](22, 4) NULL, [NextDueAmount] [decimal](22, 4) NULL,
					            [ProjectedDueAmount] [decimal](22, 4) NULL, [PastDueBalance] [decimal](22, 4) NULL,
					            [LateFeeAmount] [decimal](22, 4) NULL, [ServiceFeeAmount] [decimal](22, 4) NULL,
					            [TotalPastDueBalance] [decimal](22, 4) NULL, [ClubAccountPastDueBalance] [decimal](22, 4) NULL,
					            [CurrentQueue] [varchar] (400) NULL, [QueueTimestamp] datetime2 NULL,
                                [StationLocation] [varchar] (400) NULL, [AgreementEntrySource] [varchar] (400) NULL,
					            [AgreementEntrySourceReportName] [varchar] (400) NULL, [SinceDate] datetime2 NULL,
                                [BeginDate] datetime2 NULL, [ExpirationDate] datetime2 NULL,
                                [ConvertedDate] datetime2 NULL, [LastRenewalDate] datetime2 NULL,
                                [LastRewriteDate] datetime2 NULL, [RenewalDate] datetime2 NULL,
                                [FirstPaymentDate] datetime2 NULL, [SignDate] datetime2 NULL,
                                [NextBillingDate] datetime2 NULL, [PrimaryBillingAccountHolderFirstName] [varchar] (200) NULL,
                                [PrimaryBillingAccountHolderLastName] [varchar] (200) NULL, [ClubNumber] [int] NULL, CONSTRAINT [MemberId]
								FOREIGN KEY ([MemberId]) REFERENCES [Members]([MemberId])
								ON DELETE CASCADE  ) ON[PRIMARY];

                                CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Prospects](
                                [Id] [varchar](200) NULL, [ProspectId] [varchar](200) NULL,
					            [FirstName] [varchar](200) NULL, [LastName] [varchar](200) NULL, [City] [varchar](200) NULL, 
                                [State] [varchar](200) NULL, [PostalCode] [varchar](200) NULL,					
					            [CountryCode] [varchar](200) NULL,  [Barcode] [varchar](200) NULL, 
                                [Gender] [varchar](200) NULL, [IsActive] [varchar](200) NULL , 
					            [HasPhoto] [varchar](200) NULL , [CreatedTimestamp] [varchar](200) NULL , 				
                                [LastModifiedTimestamp] [varchar](200) NULL , [AgreementEntrySource] [varchar](200) NULL, 
                                [AgreementEntrySourceReportName] [varchar](200) NULL, [BeginDate] [varchar](200) NULL ,
					            [ExpirationDate] [varchar](200) NULL, [IssueDate] [varchar](200) NULL , 
					            [TourDate] [varchar](200) NULL , [VisitsAllowed] [varchar](200) NULL, 
					            [VisitsUsed] [varchar](200) NULL, [ClubNumber] [int] NULL) ON [PRIMARY];

                                CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CheckinsDetails](
					            [CheckInId] [varchar](400) NULL, [CheckInTimestamp] datetime2 NULL,
                                [StationName] [varchar](400) NULL, [CheckInStatus] [varchar](400) NULL, 
                                [MemberId] [varchar](400) NULL, [HomeClub] [varchar](400),
                                [ClubNumber] [int] NULL) ON [PRIMARY];

                                 CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POSTransactions](
					            [TransactionId] [varchar](400) not NULL,  PRIMARY KEY ([TransactionId]), [TransactionTimestamp] datetime2 NULL,
					            [MemberId] [varchar](400) NULL, [HomeClub] [varchar](400) NULL,
					            [EmployeeId] [varchar](400) NULL, [ReceiptNumber] [varchar](400) NULL,
					            [StationName] [varchar](400) NULL, [Return] [bit] NULL,
                                [ClubNumber] [int] NULL) ON [PRIMARY];      

                                CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POS_transItems](
					            [ItemId] [varchar](400) NOT NULL, [Name] [varchar](400) NULL,
								PRIMARY KEY ([ItemId]),[InventoryType] [int] NULL, [Sale] [bit] NULL,
					            [Upc] [varchar](400) NULL, [ProfitCenter] [varchar](400) NULL,
					            [Catalog] [varchar](400) NULL, [UnitPrice] [varchar](400) NULL,
					            [Quantity] [varchar](400) NULL, [PackageQuantity] [varchar](400) NULL,
					            [Subtotal] [varchar](400) NULL, [Tax] [varchar](400) NULL,
					            [RecurringServiceId] [varchar](400) NULL, [TransactionId] [varchar] (400) NOT NULL ,
                                [ClubNumber] [int] NULL, CONSTRAINT [TransactionId]
								FOREIGN KEY ([TransactionId]) REFERENCES [POSTransactions]([TransactionId])
								ON DELETE CASCADE) ON [PRIMARY];

                                CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[POS_transPayments](
					            [TransactionId] [varchar](400) NOT NULL, [ItemId] [varchar](400) NOT NULL,
                                [PaymentType] [varchar](400) NULL, [PaymentAmount] [varchar](400) NULL,
                                [PaymentTax] [varchar](400) NULL, [ClubNumber] [int] NULL, CONSTRAINT [ItemId]
								FOREIGN KEY ([ItemId]) REFERENCES [POS_transItems]([ItemId])
								ON DELETE CASCADE) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create all tables db for {_companyName} exception:{ex.Message}");
            }
        }
            #region OldMappings
            private void CreateMemberTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Member](
                                [MemberId][varchar](400) NULL, [PersonalId] [varchar] (400) NULL,
					            [AgreementId] [varchar] (400) NULL) ON[PRIMARY]";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Member table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreatePersonalTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE[ABC_Financial_" + _companyName + @"].[dbo].[Personal](
                                [PersonalId][varchar](400) NOT NULL, [FirstName] [varchar] (400) NULL,
					            [LastName] [varchar] (400) NULL, [MiddleInitial] [varchar] (400) NULL,
					            [AddressLine1] [varchar] (400) NULL, [AddressLine2] [varchar] (400) NULL,
					            [City] [varchar] (400) NULL, [State] [varchar] (400) NULL,
					            [PostalCode] [varchar] (400) NULL, [HomeClub] [varchar] (400) NULL,
					            [CountryCode] [varchar] (400) NULL, [Email] [varchar] (400) NULL,
					            [PrimaryPhone] [varchar] (400) NULL, [MobilePhone] [varchar] (400) NULL,
					            [WorkPhone] [varchar] (400) NULL, [WorkPhoneExt] [varchar] (400) NULL,
					            [EmergencyContactName] [varchar] (400) NULL, [EmergencyPhone] [varchar] (400) NULL,
					            [EmergencyExt] [varchar] (400) NULL, [EmergencyAvailability] [varchar] (400) NULL,
					            [Barcode] [varchar] (400) NULL, [BirthDate] datetime2 NULL,
                                [Gender] [int] NULL, [Employer] [varchar] (400) NULL,
					            [Occupation] [varchar] (400) NULL, [Group] [varchar] (400) NULL,
					            [IsActive] [bit] NULL, [MemberStatus] [varchar] (400) NULL,
					            [JoinStatus] [int] NULL, [IsConvertedProspect] [bit] NULL,
					            [HasPhoto] [bit] NULL, [ConvertedDate] datetime2 NULL,
                                [MemberStatusReason] [varchar] (400) NULL, [WellnessProgramId] [varchar] (400) NULL,
					            [FirstCheckInTimestamp] datetime2 NULL, [MemberStatusDate] datetime2 NULL,
                                [LastCheckInTimestamp] datetime2 NULL, [TotalCheckInCount] [int] NULL,
					            [CreateTimestamp] datetime2 NULL,
                                [LastModifiedTimestamp] datetime2 NULL, [MemberId] [varchar] (400) NULL) ON[PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Personal table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateAgreementTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE[ABC_Financial_" + _companyName + @"].[dbo].[Agreement](
                                [AgreementId][varchar](400) NOT NULL, [MemberId] [varchar] (400) NULL,
					            [AgreementNumber] [varchar] (400) NULL, [IsPrimaryMember] [bit] NULL,
					            [IsNonMember] [bit] NULL, [Ordinal] [int] NULL,
					            [ReferringMemberId] [varchar] (400) NULL, [ReferringMemberHomeClub] [varchar] (400) NULL,
					            [ReferringMemberName] [varchar] (400) NULL, [SalesPersonId] [varchar] (400) NULL,
					            [SalesPersonName] [varchar] (400) NULL, [SalesPersonHomeClub] [varchar] (400) NULL,
					            [PaymentPlan] [varchar] (400) NULL, [Term] [varchar] (400) NULL,
					            [PaymentFrequency] [varchar] (400) NULL, [MembershipType] [varchar] (400) NULL,
					            [ManagedType] [varchar] (400) NULL, [CampaignId] [varchar] (400) NULL,
					            [CampaignName] [varchar] (400) NULL, [CampaignGroup] [varchar] (400) NULL,
					            [IsPastDue] [bit] NULL, [DownPaymentPendingPOS] [varchar] (400) NULL,
					            [RenewalType] [varchar] (400) NULL, [AgreementPaymentMethod] [varchar] (400) NULL,
					            [DownPayment] [decimal](22, 4) NULL, [NextDueAmount] [decimal](22, 4) NULL,
					            [ProjectedDueAmount] [decimal](22, 4) NULL, [PastDueBalance] [decimal](22, 4) NULL,
					            [LateFeeAmount] [decimal](22, 4) NULL, [ServiceFeeAmount] [decimal](22, 4) NULL,
					            [TotalPastDueBalance] [decimal](22, 4) NULL, [ClubAccountPastDueBalance] [decimal](22, 4) NULL,
					            [CurrentQueue] [varchar] (400) NULL, [QueueTimestamp] datetime2 NULL,
                                [StationLocation] [varchar] (400) NULL, [AgreementEntrySource] [varchar] (400) NULL,
					            [AgreementEntrySourceReportName] [varchar] (400) NULL, [SinceDate] datetime2 NULL,
                                [BeginDate] datetime2 NULL, [ExpirationDate] datetime2 NULL,
                                [ConvertedDate] datetime2 NULL, [LastRenewalDate] datetime2 NULL,
                                [LastRewriteDate] datetime2 NULL, [RenewalDate] datetime2 NULL,
                                [FirstPaymentDate] datetime2 NULL, [SignDate] datetime2 NULL,
                                [NextBillingDate] datetime2 NULL, [PrimaryBillingAccountHolderId] [varchar] (400) NULL
				            ) ON[PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Agreement table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreatePrimaryBillingAccountHolderTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[PrimaryBillingAccountHolder](
					            [PrimaryBillingAccountHolderId] [varchar](400) NOT NULL, [FirstName] [varchar](400) NULL,
					            [LastName] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC PrimaryBillingAccountHolder table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateAlertInMemberTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[AlertInMember](
					[AlertInMemberId] [varchar](400) NOT NULL,
					[AlertId] [varchar](400) NULL,
					[MemberId] [varchar](400) NULL
				) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC AlertInMember table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateAlertTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Alert](
					            [AlertId] [varchar](400) NOT NULL, [Message] [varchar](400) NULL,
					            [AbcCode] [varchar](400) NULL, [Priority] [varchar](400) NULL,
					            [AllowDoorAccess] [varchar](400) NULL, [EvaluationDate] [varchar](400) NULL,
					            [GracePeriod] [varchar](400) NULL, [EvaluationAmount] [decimal](22, 4) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Alert table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMemberCheckinDetailTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[MemberCheckinDetail](
					            [MemberCheckinDetailId] [varchar](400) NOT NULL, [MemberId] [varchar](400) NULL,
					            [CheckinId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC MemberCheckinDetail table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateCheckinDetailTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CheckinDetail](
					            [CheckinDetailId] [varchar](400) NOT NULL, [CheckInId] [varchar](400) NULL,
					            [ClubNumber] [varchar](400) NULL, [CheckInTimeStamp] datetime2 NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC CheckinDetail table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMemberCheckinSummariesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[MemberCheckinSummaries](
				                [CJId] [varchar](400) NULL, [MemberCheckinSummariesId] [varchar](400) NOT NULL,
					            [MemberId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC MemberCheckinSummaries table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateLinksTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Links](
					            [CJId] [varchar](400) NULL, [LinkId] [varchar](400) NOT NULL,
					            [Rel] [varchar](400) NULL, [Url] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Links table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreatePaymentsInItemsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[PaymentsInItems](
					            [Id] [varchar](400) NOT NULL, [PaymentId] [varchar](400) NULL,
					            [ItemId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC PaymentsInItems table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateCheckInCountTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[CheckInCount](
					            [CJId] [varchar](400) NULL, [CheckInCountId] [varchar](400) NOT NULL,
					            [Club] [varchar](400) NULL, [Count] [int] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC CheckInCount table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMemberChildTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[MemberChild](
					            [MemberChildId] [varchar](400) NULL, [MemberChildEmergencyContactId] [varchar](400) NULL,
					            [ActiveStatus] [varchar](400) NULL, [HomeClub] [varchar](400) NULL,
					            [FirstName] [varchar](400) NULL, [MiddleInitial] [varchar](400) NULL,
					            [LastName] [varchar](400) NULL, [Barcode] [varchar](400) NULL,
					            [Gender] [varchar](400) NULL, [BirthDate] [varchar](400) NULL,
					            [PrimaryMemberId] [varchar](400) NULL, [AgreementNumber] [varchar](400) NULL,
					            [ChildMisc1] [varchar](400) NULL,  [ChildMisc2] [varchar](400) NULL,
					            [ChildMisc3] [varchar](400) NULL, [CreateTimestamp] datetime2 NULL,
					            [LastModifiedTimestamp] datetime2 NULL, [LastCheckInTimestamp] datetime2 NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC MemberChild table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMemberChildEmergencyContactTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[MemberChildEmergencyContact](
					            [MemberChildEmergencyContactId] [varchar](400) NOT NULL, [EmergencyContactFirstName] [varchar](400) NULL,
					            [EmergencyContactMiddleInitial] [varchar](400) NULL, [EmergencyContactLastName] [varchar](400) NULL,
					            [EmergencyContactPhone] [varchar](400) NULL, [EmergencyContactExtension] [varchar](400) NULL
				            ) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC MemberChildEmergencyContact table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMemberChildNoteTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[MemberChildNote](
					            [MemberChildNoteId] [varchar](400) NOT NULL, [NoteText] [varchar](400) NULL,
					            [NoteCreateTimestamp] datetime2 NULL, [EmployeeId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC MemberChildNote table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateChildNotesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[ChildNotes](
					            [ChildNotesId] [varchar](400) NOT NULL, [MemberChildNoteId] [varchar](400) NULL,
					            [MemberChildId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC ChildNotes table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateGroupTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Group](
					            [Id] [varchar](400) NULL, [Name] [varchar](400) NULL,
					            [Status] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Group table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateMemberPersonalTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[MemberPersonal](
					            [MemberPersonalId] [varchar](400) NOT NULL, [MemberId] [varchar](400) NULL,
					            [PersonalId] [varchar](400) NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC MemberPersonal table db for {_companyName} exception:{ex.Message}");
            }
        }
        #endregion

        private void CreateNewMembersTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Members](
                                [MemberId][varchar](400) NOT NULL, PRIMARY KEY ([MemberId]), [FirstName] [varchar] (400) NULL,
					            [LastName] [varchar] (400) NULL, [MiddleInitial] [varchar] (400) NULL,
					            [AddressLine1] [varchar] (400) NULL, [AddressLine2] [varchar] (400) NULL,
					            [City] [varchar] (400) NULL, [State] [varchar] (400) NULL,
					            [PostalCode] [varchar] (400) NULL, [HomeClub] [varchar] (400) NULL,
					            [CountryCode] [varchar] (400) NULL, [Email] [varchar] (400) NULL,
					            [PrimaryPhone] [varchar] (400) NULL, [MobilePhone] [varchar] (400) NULL,
					            [WorkPhone] [varchar] (400) NULL, [WorkPhoneExt] [varchar] (400) NULL,
					            [EmergencyContactName] [varchar] (400) NULL, [EmergencyPhone] [varchar] (400) NULL,
					            [EmergencyExt] [varchar] (400) NULL, [EmergencyAvailability] [varchar] (400) NULL,
					            [Barcode] [varchar] (400) NULL, [BirthDate] datetime2 NULL,
                                [Gender] [int] NULL, [Employer] [varchar] (400) NULL,
					            [Occupation] [varchar] (400) NULL, [Group] [varchar] (400) NULL,
					            [IsActive] [bit] NULL, [MemberStatus] [varchar] (400) NULL,
					            [JoinStatus] [int] NULL, [IsConvertedProspect] [bit] NULL,
					            [HasPhoto] [bit] NULL, [ConvertedDate] datetime2 NULL,
                                [MemberStatusReason] [varchar] (400) NULL, [WellnessProgramId] [varchar] (400) NULL,
					            [FirstCheckInTimestamp] datetime2 NULL, [MemberStatusDate] datetime2 NULL,
                                [LastCheckInTimestamp] datetime2 NULL, [TotalCheckInCount] [int] NULL,
					            [CreateTimestamp] datetime2 NULL, [LastModifiedTimestamp] datetime2 NULL,
                                [ClubNumber] [int] NULL) ON[PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Member table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateProspectsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE [ABC_Financial_" + _companyName + @"].[dbo].[Prospects](
                                [Id] [varchar](200) NULL, [ProspectId] [varchar](200) NULL,
					            [FirstName] [varchar](200) NULL, [LastName] [varchar](200) NULL, [City] [varchar](200) NULL, 
                                [State] [varchar](200) NULL, [PostalCode] [varchar](200) NULL,					
					            [CountryCode] [varchar](200) NULL,  [Barcode] [varchar](200) NULL, 
                                [Gender] [varchar](200) NULL, [IsActive] [varchar](200) NULL , 
					            [HasPhoto] [varchar](200) NULL , [CreatedTimestamp] [varchar](200) NULL , 				
                                [LastModifiedTimestamp] [varchar](200) NULL , [AgreementEntrySource] [varchar](200) NULL, 
                                [AgreementEntrySourceReportName] [varchar](200) NULL, [BeginDate] [varchar](200) NULL ,
					            [ExpirationDate] [varchar](200) NULL, [IssueDate] [varchar](200) NULL , 
					            [TourDate] [varchar](200) NULL , [VisitsAllowed] [varchar](200) NULL, 
					            [VisitsUsed] [varchar](200) NULL, [ClubNumber] [int] NULL) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Member table db for {_companyName} exception:{ex.Message}");
            }
        }

        private void CreateNewAgreementTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    var query = "CREATE TABLE[ABC_Financial_" + _companyName + @"].[dbo].[Agreement](
                                [AgreementId][varchar](400) NOT NULL, [MemberId] [varchar] (400) NULL,
					            [AgreementNumber] [varchar] (400) NULL, [IsPrimaryMember] [bit] NULL,
					            [IsNonMember] [bit] NULL, [Ordinal] [int] NULL,
					            [ReferringMemberId] [varchar] (400) NULL, [ReferringMemberHomeClub] [varchar] (400) NULL,
					            [ReferringMemberName] [varchar] (400) NULL, [SalesPersonId] [varchar] (400) NULL,
					            [SalesPersonName] [varchar] (400) NULL, [SalesPersonHomeClub] [varchar] (400) NULL,
					            [PaymentPlan] [varchar] (400) NULL, [Term] [varchar] (400) NULL,
					            [PaymentFrequency] [varchar] (400) NULL, [MembershipType] [varchar] (400) NULL,
					            [ManagedType] [varchar] (400) NULL, [CampaignId] [varchar] (400) NULL,
					            [CampaignName] [varchar] (400) NULL, [CampaignGroup] [varchar] (400) NULL,
					            [IsPastDue] [bit] NULL, [DownPaymentPendingPOS] [varchar] (400) NULL,
					            [RenewalType] [varchar] (400) NULL, [AgreementPaymentMethod] [varchar] (400) NULL,
					            [DownPayment] [decimal](22, 4) NULL, [NextDueAmount] [decimal](22, 4) NULL,
					            [ProjectedDueAmount] [decimal](22, 4) NULL, [PastDueBalance] [decimal](22, 4) NULL,
					            [LateFeeAmount] [decimal](22, 4) NULL, [ServiceFeeAmount] [decimal](22, 4) NULL,
					            [TotalPastDueBalance] [decimal](22, 4) NULL, [ClubAccountPastDueBalance] [decimal](22, 4) NULL,
					            [CurrentQueue] [varchar] (400) NULL, [QueueTimestamp] datetime2 NULL,
                                [StationLocation] [varchar] (400) NULL, [AgreementEntrySource] [varchar] (400) NULL,
					            [AgreementEntrySourceReportName] [varchar] (400) NULL, [SinceDate] datetime2 NULL,
                                [BeginDate] datetime2 NULL, [ExpirationDate] datetime2 NULL,
                                [ConvertedDate] datetime2 NULL, [LastRenewalDate] datetime2 NULL,
                                [LastRewriteDate] datetime2 NULL, [RenewalDate] datetime2 NULL,
                                [FirstPaymentDate] datetime2 NULL, [SignDate] datetime2 NULL,
                                [NextBillingDate] datetime2 NULL, [PrimaryBillingAccountHolderFirstName] [varchar] (200) NULL,
                                [PrimaryBillingAccountHolderLastName] [varchar] (200) NULL, [ClubNumber] [int] NULL, CONSTRAINT [MemberId]
								FOREIGN KEY ([MemberId]) REFERENCES [Members]([MemberId])
								ON DELETE CASCADE
						    ) ON[PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create ABC Agreement table db for {_companyName} exception:{ex.Message}");
            }
        }

    }
}
