﻿using ClearJelly.ABCFinancialApp.Helpers;
using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.ABCFinancialApp.Providers;
using ClearJelly.ABCFinancialApp.Models.Clubs;
using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Models.Clubs.Clubs;
using ClearJelly.ABCFinancialApp.Models.Members;
using System.Collections.Generic;
using ClearJelly.Configuration;
using ClearJelly.ABCFinancialApp.Models;
using ClearJelly.ABCFinancialApp.Models.Members.Members;

namespace ClearJelly.ABCFinancialApp
{
    //public class ABCFinancialsService
    //{
    //    private readonly ABCFinancialsClubsRepository _clubsRepository;
    //    private readonly ABCFinancialsMembersRepository _membersRepository;
    //    private readonly ABCFinancialsOAuthProvider _oAuthProvider;
    //    private readonly Logger _logger;
    //    private readonly ABCFinancialsMembersApiProvider _membersProvider;
    //    private string _connectionString;
    //    ABCFinancialsClubsApiProvider _clubsProvider;
    //    public ABCFinancialsService(string dbName)
    //    {
    //        _connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
    //        _logger = LogManager.GetCurrentClassLogger();
    //        _clubsRepository = new ABCFinancialsClubsRepository(_connectionString);
    //        _membersRepository = new ABCFinancialsMembersRepository(_connectionString);
    //        _oAuthProvider = new ABCFinancialsOAuthProvider();
    //        _membersProvider = new ABCFinancialsMembersApiProvider(dbName);
    //        _clubsProvider = new ABCFinancialsClubsApiProvider(dbName);
    //    }

    //    public string GetLoginUrl()
    //    {
    //        return _oAuthProvider.GetLoginUrl();
    //    }

    //    public async Task GetToken(string authorizationCode)
    //    {
    //        await _oAuthProvider.GetToken(authorizationCode);
    //    }

    //    public async Task<bool> FetchAllDataAsync(string dbName, string clubNumber, DateTime startDate, DateTime endDate)
    //    {
    //        var isCompletedMembers = Task<bool>.Run(() => GetMembersData(clubNumber, startDate, endDate));
    //        var isCompletedClubs = Task<bool>.Run(() => GetClubsDataAsync(clubNumber, startDate, endDate));
    //        if (isCompletedMembers.IsCompleted && isCompletedClubs.IsCompleted)
    //        {
    //            return true;
    //        }
    //        return false;
    //    }

    //    private async Task<bool> GetMembersData(string clubNumber, DateTime startDate, DateTime endDate)
    //    {
    //        try
    //        {

    //            var result = await GetMembersDataAsync(clubNumber, startDate, endDate);
    //            return result;
    //        }
    //        catch (Exception ex)
    //        {
    //            return false;
    //        }

    //    }

    //    private async Task<bool> GetMembersDataAsync(string clubNumber, DateTime startDate, DateTime endDate)
    //    {
    //        try
    //        {
    //            var listTasks = new List<Task>();

    //            listTasks.Add(Task.Run(() => _membersProvider.GetMembers(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _membersProvider.GetMembersCheckinsSummaries(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _membersProvider.GetMembersGroups(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _membersProvider.GetMembersPersonals(clubNumber, startDate, endDate)));

    //            await Task.WhenAll(listTasks);
    //            return true;
    //        }
    //        catch (Exception)
    //        {
    //            return false;
    //        }
    //    }

    //    private async void GetMembersDataById(string dbName, string clubNumber, DateTime startDate, DateTime endDate, string memberId)
    //    {
    //        //add insert
    //        var memberDataById = await _membersProvider.GetMembersByMemberId(memberId, clubNumber, startDate, endDate);

    //        var membersCheckinsDetailsByMemberId = await _membersProvider.GetMembersCheckinsDetailsByMemberId(memberId, clubNumber, startDate, endDate);
    //        _membersRepository.InsertMembersCheckinsDetails(membersCheckinsDetailsByMemberId);

    //        var membersCheckinsSummariesById = await _membersProvider.GetMembersCheckinsSummariesByMemberId(memberId, clubNumber);

    //        var membersPersonalsById = await _membersProvider.GetMembersPersonalsByMemberId(memberId, clubNumber, startDate, endDate);

    //        var membersChildren = await _membersProvider.GetMembersChildernByMemberChildId(memberId, clubNumber, startDate, endDate);
    //        _membersRepository.InsertMembersChildren(membersChildren);
    //    }

    //    private async Task<bool> GetClubsDataAsync(string clubNumber, DateTime startDate, DateTime endDate)
    //    {
    //        try
    //        {
    //            var listTasks = new List<Task>();

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClub(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubCheckinsDetails(clubNumber, startDate, endDate)));

    //            ////WHERE TO GET BARCODE?
    //            ////Task.Run(() => _clubsProvider.GetClubMemberIdentitieByBarcode(startDate, endDate));
    //            ////var memberIdentitie = await clubsProvider.GetClubMemberIdentitieByBarcode("213");
    //            ////_clubsRepository.InsertClubMemberIdentitie(memberIdentitie);

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubTransactionPos(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlans(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlansAnnualFees(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlansBillingCatalogItems(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlansPaymentMethods(clubNumber, startDate, endDate)));

    //            listTasks.Add(Task.Run(() => _clubsProvider.GetClubStations(clubNumber, startDate, endDate)));

    //            await Task.WhenAll(listTasks);
    //            return true;
    //        }
    //        catch (Exception)
    //        {
    //            return false;
    //        }
    //    }
    //    private async Task<bool> GetClubsData(string clubNumber, DateTime startDate, DateTime endDate)
    //    {
    //        try
    //        {
    //            var result = await GetClubsDataAsync(clubNumber, startDate, endDate);
    //            return result;
    //        }
    //        catch (Exception ex)
    //        {
    //            return false;
    //        }
    //    }

    //    public async Task<bool> UpdateMembers(string dbName, string clubNumber, DateTime startDate, DateTime endDate)
    //    {
    //        var tableNames = RepositoryHelper.GetUserTableNames(_connectionString);
    //        foreach (var tbName in tableNames)
    //        {
    //            await RepositoryHelper.ClearTable(tbName, _connectionString);
    //        }
    //        await FetchAllDataAsync(dbName, clubNumber, startDate, endDate);
    //        return true;
    //    }

    //    public async Task<bool> CreateAbcDbAsync(string companyName)
    //    {
    //        DbGeneratorHelper generator = new DbGeneratorHelper(ConfigSection.ClearJellyEntities, companyName);
    //        var isDbCreated = await generator.CreateABCDatabaseAsync();
    //        if (isDbCreated)
    //        {
    //            ABCFinancialsMemberGenerator memberGenerator = new ABCFinancialsMemberGenerator(companyName);
    //            await memberGenerator.GenerateTables();

    //            ABCFinancialsClubGenerator clubGenerator = new ABCFinancialsClubGenerator(companyName);
    //            await clubGenerator.GenerateTables();
    //            return true;
    //        }
    //        return false;

            
    //    }
    //}
}

