﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp
{
   public static class Common
    {
        public static DataTable ConvertDataToDataTable<T>(List<T> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        public static List<string> GetUserColumnNames(string tableName,string _connectionString)
        {
            var res = new SqlConnection(_connectionString).Query<string>($"SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('{tableName}')").ToList();
            return res;
        }
    }
}
