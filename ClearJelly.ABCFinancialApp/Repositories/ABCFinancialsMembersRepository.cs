﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using ClearJelly.ABCFinancialApp.Models.Members;
using NLog;
using Dapper;
using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Models;
using System.Data;
using ClearJelly.ABCFinancialApp.Mapers;
using ClearJelly.ABCFinancialApp.Models.Members.Personals;
using ClearJelly.ABCFinancialApp.Models.Members.Members;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Repositories
{
    public class ABCFinancialsMembersRepository
    {
        private readonly string _connectionString;
        private readonly Logger _logger;
        private static readonly string _apiDateTimeFromat = "yyyy-MM-dd";

        public ABCFinancialsMembersRepository(string connectionString)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _connectionString = connectionString;
        }

        public void InsertMembers(List<MembersMemberModel> members, string club)
        {
           
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    try
                    {
                        var query = $"select MemberId FROM Members";
                        var ids = connection.Query<string>(query).ToList();
                        foreach (var item in ids)
                        {
                            var idExist = members.Where(x => x.MemberId == item).FirstOrDefault();
                            if (idExist != null)
                            {
                                members.Remove(members.Single(x => x.MemberId == item));
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.Error($"Insert Prospects exception:{ex.Message}");
                    }
                }
                #region currentlyDisabled
                //using (SqlConnection connection = new SqlConnection(_connectionString))
                //{

                //    var queryMember = @"INSERT INTO [dbo].[Member] ([MemberId],[PersonalId],[AgreementId])
                //                      VALUES (@MemberId,@PersonalId,@AgreementId)";

                //    foreach (var member in members)
                //    {
                //        var personalId = InsertMembersPersonal(member.Personal, member.MemberId);
                //        var agreementId = InsertAgreement(connection, member);
                //        connection.Execute(queryMember, new
                //        {
                //            MemberId = member.MemberId,
                //            PersonalId = personalId,
                //            AgreementId = agreementId
                //        });
                //        _logger.Info("members insert ok");
                //        _logger.Info("alert insert start");
                //        if (member.Alerts != null)
                //        {

                //            InsertAlerts(connection, member);
                //        }
                //        _logger.Info("alert insert ok");

                //    }
                //}
                #endregion
                var personals = new List<PersonalModel>();
                var agreements = new List<MemberAgreementsModel>();
                foreach (var member in members)
                {
                    personals.Add(MemberMapers.MapToPersonal(member.Personal, member.MemberId, club));
                    agreements.Add(MemberMapers.MapToAgreement(member, club));
                }
                _logger.Info("members insert start");
                Task.Run(() => SaveDataToDb(personals, "Members"));                
                _logger.Info("members inserted ok. Count: " + personals.Count);
                _logger.Info("Agreement insert start");
                Task.Run(() => SaveDataToDb(agreements, "Agreement"));
                _logger.Info("Agreement inserted ok. Count: " + agreements.Count);
            }
            catch (Exception ex)
            {
                _logger.Error($"Members insert exception:{ex.Message}");
            }
        }        

        //private string InsertAgreement(SqlConnection connection, MembersMemberModel agreement)
        //{
        //    var queryAgreement = @"INSERT INTO [dbo].[Agreement] ([AgreementId],[MemberId],[AgreementNumber],[IsPrimaryMember],[IsNonMember],[Ordinal],[ReferringMemberId],[ReferringMemberHomeClub] ,[ReferringMemberName],
        //                         [SalesPersonId],[SalesPersonName],[SalesPersonHomeClub],[PaymentPlan],[Term],[PaymentFrequency],[MembershipType],[ManagedType],[CampaignId],[CampaignName],[CampaignGroup],[IsPastDue],[DownPaymentPendingPOS],[RenewalType],
        //          [AgreementPaymentMethod],[DownPayment],[NextDueAmount],[ProjectedDueAmount],[PastDueBalance],[LateFeeAmount],[ServiceFeeAmount],[TotalPastDueBalance],[ClubAccountPastDueBalance],[CurrentQueue],[QueueTimestamp],[StationLocation],[AgreementEntrySource],
        //                         [AgreementEntrySourceReportName],[SinceDate],[BeginDate],[ExpirationDate],[ConvertedDate],[LastRenewalDate],[LastRewriteDate],[RenewalDate],[FirstPaymentDate],[SignDate],
        //        	 [NextBillingDate],[PrimaryBillingAccountHolderId])
        //                         VALUES (@AgreementId,@MemberId,@AgreementNumber,@IsPrimaryMember,@IsNonMember,@Ordinal,@ReferringMemberId,@ReferringMemberHomeClub,@ReferringMemberName,
        //                         @SalesPersonId,@SalesPersonName,@SalesPersonHomeClub,@PaymentPlan,@Term,@PaymentFrequency,@MembershipType,@ManagedType,@CampaignId,@CampaignName,@CampaignGroup,@IsPastDue,@DownPaymentPendingPOS,@RenewalType,
        //          @AgreementPaymentMethod,@DownPayment,@NextDueAmount,@ProjectedDueAmount,@PastDueBalance,@LateFeeAmount,@ServiceFeeAmount,@TotalPastDueBalance,@ClubAccountPastDueBalance,@CurrentQueue,@QueueTimestamp,@StationLocation,@AgreementEntrySource,
        //                         @AgreementEntrySourceReportName,@SinceDate,@BeginDate,@ExpirationDate,@ConvertedDate,@LastRenewalDate,@LastRewriteDate,@RenewalDate,@FirstPaymentDate,@SignDate,
        //        	 @NextBillingDate,@PrimaryBillingAccountHolderId)";

        //    string agreementId = Guid.NewGuid().ToString();

        //    try
        //    {
        //        connection.Execute(queryAgreement, new
        //        {
        //            AgreementId = agreementId,
        //            MemberId = agreement.Agreement.MemberId,
        //            AgreementNumber = agreement.Agreement.AgreementNumber,
        //            IsPrimaryMember = agreement.Agreement.IsPrimaryMember,
        //            IsNonMember = agreement.Agreement.IsNonMember,
        //            Ordinal = agreement.Agreement.Ordinal,
        //            ReferringMemberId = agreement.Agreement.ReferringMemberId,
        //            ReferringMemberHomeClub = agreement.Agreement.ReferringMemberHomeClub,
        //            ReferringMemberName = agreement.Agreement.ReferringMemberName,
        //            SalesPersonId = agreement.Agreement.SalesPersonId,
        //            SalesPersonName = agreement.Agreement.SalesPersonName,
        //            SalesPersonHomeClub = agreement.Agreement.SalesPersonHomeClub,
        //            PaymentPlan = agreement.Agreement.PaymentPlan,
        //            Term = agreement.Agreement.Term,
        //            PaymentFrequency = agreement.Agreement.PaymentFrequency,
        //            MembershipType = agreement.Agreement.MembershipType,
        //            ManagedType = agreement.Agreement.ManagedType,
        //            CampaignId = agreement.Agreement.CampaignId,
        //            CampaignName = agreement.Agreement.CampaignName,
        //            CampaignGroup = agreement.Agreement.CampaignGroup,
        //            IsPastDue = agreement.Agreement.IsPastDue,
        //            DownPaymentPendingPOS = agreement.Agreement.DownPaymentPendingPos,
        //            RenewalType = agreement.Agreement.RenewalType,
        //            AgreementPaymentMethod = agreement.Agreement.AgreementPaymentMethod,
        //            DownPayment = agreement.Agreement.DownPayment,
        //            NextDueAmount = agreement.Agreement.NextDueAmount,
        //            ProjectedDueAmount = agreement.Agreement.ProjectedDueAmount,
        //            PastDueBalance = agreement.Agreement.PastDueBalance,
        //            LateFeeAmount = agreement.Agreement.LateFeeAmount,
        //            ServiceFeeAmount = agreement.Agreement.ServiceFeeAmount,
        //            TotalPastDueBalance = agreement.Agreement.TotalPastDueBalance,
        //            ClubAccountPastDueBalance = agreement.Agreement.ClubAccountPastDueBalance,
        //            CurrentQueue = agreement.Agreement.CurrentQueue,
        //            QueueTimestamp = agreement.Agreement.QueueTimestamp,
        //            StationLocation = agreement.Agreement.StationLocation,
        //            AgreementEntrySource = agreement.Agreement.AgreementEntrySource,
        //            AgreementEntrySourceReportName = agreement.Agreement.AgreementEntrySourceReportName,
        //            SinceDate = agreement.Agreement.SinceDate,
        //            BeginDate = agreement.Agreement.BeginDate,
        //            ExpirationDate = agreement.Agreement.ExpirationDate,
        //            ConvertedDate = agreement.Agreement.ConvertedDate,
        //            LastRenewalDate = agreement.Agreement.LastRenewalDate,
        //            LastRewriteDate = agreement.Agreement.LastRewriteDate,
        //            RenewalDate = agreement.Agreement.RenewalDate,
        //            FirstPaymentDate = agreement.Agreement.FirstPaymentDate,
        //            SignDate = agreement.Agreement.SignDate,
        //            NextBillingDate = agreement.Agreement.NextBillingDate,
        //            PrimaryBillingAccountHolderId = " "
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //    return agreementId;
        //}

        private string InsertAgreement(MembersMemberModel agreement, string club)
        {
            var queryAgreement = @"INSERT INTO [dbo].[Agreement] ([AgreementId],[MemberId],[AgreementNumber],[IsPrimaryMember],[IsNonMember],[Ordinal],[ReferringMemberId],[ReferringMemberHomeClub] ,[ReferringMemberName],
                                 [SalesPersonId],[SalesPersonName],[SalesPersonHomeClub],[PaymentPlan],[Term],[PaymentFrequency],[MembershipType],[ManagedType],[CampaignId],[CampaignName],[CampaignGroup],[IsPastDue],[DownPaymentPendingPOS],[RenewalType],
					             [AgreementPaymentMethod],[DownPayment],[NextDueAmount],[ProjectedDueAmount],[PastDueBalance],[LateFeeAmount],[ServiceFeeAmount],[TotalPastDueBalance],[ClubAccountPastDueBalance],[CurrentQueue],[QueueTimestamp],[StationLocation],[AgreementEntrySource],
                                 [AgreementEntrySourceReportName],[SinceDate],[BeginDate],[ExpirationDate],[ConvertedDate],[LastRenewalDate],[LastRewriteDate],[RenewalDate],[FirstPaymentDate],[SignDate],
				            	 [NextBillingDate],[PrimaryBillingAccountHolderFirstName],[PrimaryBillingAccountHolderLastName],[ClubNumber])
                                 VALUES (@AgreementId,@MemberId,@AgreementNumber,@IsPrimaryMember,@IsNonMember,@Ordinal,@ReferringMemberId,@ReferringMemberHomeClub,@ReferringMemberName,
                                 @SalesPersonId,@SalesPersonName,@SalesPersonHomeClub,@PaymentPlan,@Term,@PaymentFrequency,@MembershipType,@ManagedType,@CampaignId,@CampaignName,@CampaignGroup,@IsPastDue,@DownPaymentPendingPOS,@RenewalType,
					             @AgreementPaymentMethod,@DownPayment,@NextDueAmount,@ProjectedDueAmount,@PastDueBalance,@LateFeeAmount,@ServiceFeeAmount,@TotalPastDueBalance,@ClubAccountPastDueBalance,@CurrentQueue,@QueueTimestamp,@StationLocation,@AgreementEntrySource,
                                 @AgreementEntrySourceReportName,@SinceDate,@BeginDate,@ExpirationDate,@ConvertedDate,@LastRenewalDate,@LastRewriteDate,@RenewalDate,@FirstPaymentDate,@SignDate,
				            	 @NextBillingDate,@PrimaryBillingAccountHolderFirstName,@PrimaryBillingAccountHolderLastName,@ClubNumber)";

            string agreementId = Guid.NewGuid().ToString();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Execute(queryAgreement, new
                    {
                        AgreementId = agreementId,
                        MemberId = agreement.MemberId,
                        AgreementNumber = agreement.Agreement.AgreementNumber,
                        IsPrimaryMember = agreement.Agreement.IsPrimaryMember,
                        IsNonMember = agreement.Agreement.IsNonMember,
                        Ordinal = agreement.Agreement.Ordinal,
                        ReferringMemberId = agreement.Agreement.ReferringMemberId,
                        ReferringMemberHomeClub = agreement.Agreement.ReferringMemberHomeClub,
                        ReferringMemberName = agreement.Agreement.ReferringMemberName,
                        SalesPersonId = agreement.Agreement.SalesPersonId,
                        SalesPersonName = agreement.Agreement.SalesPersonName,
                        SalesPersonHomeClub = agreement.Agreement.SalesPersonHomeClub,
                        PaymentPlan = agreement.Agreement.PaymentPlan,
                        Term = agreement.Agreement.Term,
                        PaymentFrequency = agreement.Agreement.PaymentFrequency,
                        MembershipType = agreement.Agreement.MembershipType,
                        ManagedType = agreement.Agreement.ManagedType,
                        CampaignId = agreement.Agreement.CampaignId,
                        CampaignName = agreement.Agreement.CampaignName,
                        CampaignGroup = agreement.Agreement.CampaignGroup,
                        IsPastDue = agreement.Agreement.IsPastDue,
                        DownPaymentPendingPOS = agreement.Agreement.DownPaymentPendingPos,
                        RenewalType = agreement.Agreement.RenewalType,
                        AgreementPaymentMethod = agreement.Agreement.AgreementPaymentMethod,
                        DownPayment = agreement.Agreement.DownPayment,
                        NextDueAmount = agreement.Agreement.NextDueAmount,
                        ProjectedDueAmount = agreement.Agreement.ProjectedDueAmount,
                        PastDueBalance = agreement.Agreement.PastDueBalance,
                        LateFeeAmount = agreement.Agreement.LateFeeAmount,
                        ServiceFeeAmount = agreement.Agreement.ServiceFeeAmount,
                        TotalPastDueBalance = agreement.Agreement.TotalPastDueBalance,
                        ClubAccountPastDueBalance = agreement.Agreement.ClubAccountPastDueBalance,
                        CurrentQueue = agreement.Agreement.CurrentQueue,
                        QueueTimestamp = agreement.Agreement.QueueTimestamp,
                        StationLocation = agreement.Agreement.StationLocation,
                        AgreementEntrySource = agreement.Agreement.AgreementEntrySource,
                        AgreementEntrySourceReportName = agreement.Agreement.AgreementEntrySourceReportName,
                        SinceDate = agreement.Agreement.SinceDate,
                        BeginDate = agreement.Agreement.BeginDate,
                        ExpirationDate = agreement.Agreement.ExpirationDate,
                        ConvertedDate = agreement.Agreement.ConvertedDate,
                        LastRenewalDate = agreement.Agreement.LastRenewalDate,
                        LastRewriteDate = agreement.Agreement.LastRewriteDate,
                        RenewalDate = agreement.Agreement.RenewalDate,
                        FirstPaymentDate = agreement.Agreement.FirstPaymentDate,
                        SignDate = agreement.Agreement.SignDate,
                        NextBillingDate = agreement.Agreement.NextBillingDate,
                        PrimaryBillingAccountHolderFirstName = agreement.Agreement?.PrimaryBillingAccountHolder?.FirstName,
                        PrimaryBillingAccountHolderLastName = agreement.Agreement?.PrimaryBillingAccountHolder?.LastName,
                        ClubNumber = Int32.Parse(club)
                    });
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return agreementId;
        }

        private void InsertAlerts(SqlConnection connection, MembersMemberModel member)
        {
            try
            {
                var queryAlert = @"INSERT INTO [dbo].[Alert] ([AlertId],[Message],[AbcCode],[Priority],[AllowDoorAccess],[EvaluationDate],[GracePeriod],[EvaluationAmount])
                                     VALUES (@AlertId,@Message,@AbcCode,@Priority,@AllowDoorAccess,@EvaluationDate,@GracePeriod,@EvaluationAmount)";
                var queryAlertInMember = @"INSERT INTO [dbo].[AlertInMember] ([AlertInMemberId],[AlertId],[MemberId])
                                     VALUES (@AlertInMemberId,@AlertId,@MemberId)";
                foreach (var alert in member.Alerts)
                {
                    var alertGuid = Guid.NewGuid().ToString();
                    connection.Execute(queryAlert, new
                    {
                        AlertId = alertGuid,
                        Message = alert.Message,
                        AbcCode = alert.AbcCode,
                        Priority = alert.Priority,
                        AllowDoorAccess = alert.AllowDoorAccess,
                        EvaluationDate = alert.EvaluationDate,
                        GracePeriod = alert.GracePeriod,
                        EvaluationAmount = alert.EvaluationAmount
                    });

                    connection.Execute(queryAlertInMember, new
                    {
                        AlertInMemberId = Guid.NewGuid().ToString(),
                        AlertId = alertGuid,
                        MemberId = member.MemberId,
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Error inserting alerts: {ex.Message}");
            }
        }

        public void InsertMembersCheckinsSummaries(List<CheckinDetailsSummariesMemberModel> membersCheckinsSummaries)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var queryMember = @"INSERT INTO [dbo].[MemberCheckinSummaries] ([CJId],[MemberCheckinSummariesId],[MemberId])
                                      VALUES (@CJId,@MemberCheckinSummariesId,@MemberId)";

                    foreach (var member in membersCheckinsSummaries)
                    {
                        var CJId = Guid.NewGuid().ToString();
                        var MemberCheckinSummariesId = Guid.NewGuid().ToString();
                        connection.Execute(queryMember, new
                        {
                            CJId = CJId,
                            MemberCheckinSummariesId = MemberCheckinSummariesId,
                            MemberId = member.MemberId,
                        });

                        InsertCheckInCount(connection, member, CJId);

                        InsertLinks(connection, member, MemberCheckinSummariesId, CJId);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersCheckinsSummaries exception:{ex.Message}");
            }
        }

        private void InsertCheckInCount(SqlConnection connection, CheckinDetailsSummariesMemberModel member, string CJId)
        {
            var queryCheckInCount = @"INSERT INTO [dbo].[CheckInCount] ([CJId],[CheckInCountId],[Club],[Count])
                                            VALUES (@CJId,@CheckInCountId,@Club,@Count)";
            try
            {
                foreach (var checkInCount in member.CheckInCounts.CheckInCount)
                {
                    connection.Execute(queryCheckInCount, new
                    {
                        CJId = CJId,
                        CheckInCountId = Guid.NewGuid().ToString(),
                        Club = checkInCount.Club,
                        Count = checkInCount.Count,
                    });
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void InsertLinks(SqlConnection connection, CheckinDetailsSummariesMemberModel member, string MemberCheckinSummariesId, string CJId)
        {
            var queryLink = @"INSERT INTO [dbo].[Links] ([CJId],[LinkId],[Rel],[Url])
                                            VALUES (@CJId,@LinkId,@Rel,@Url)";

            try
            {
                foreach (var link in member.Links)
                {
                    var LinkId = Guid.NewGuid().ToString();

                    connection.Execute(queryLink, new
                    {
                        CJId = CJId,
                        LinkId = LinkId,
                        Rel = link.Rel,
                        Url = link.Url,
                    });
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public void InsertMembersCheckinsDetails(MemberCheckinsDetailsMemberModel membersCheckinsDetail)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[CheckinDetail] ([CheckinDetailId],[CheckInId],[ClubNumber], [CheckInTimeStamp])
                                      VALUES (@CheckinDetailId,@CheckInId,@ClubNumber,@CheckInTimeStamp)";

                    var queryMemberCheckinDetail = @"INSERT INTO [dbo].[MemberCheckinDetail] ([MemberCheckinDetailId],[MemberId],[CheckinId])
                                      VALUES (@MemberCheckinDetailId,@MemberId,@CheckinId)";

                    foreach (var chekin in membersCheckinsDetail.Checkins)
                    {
                        connection.Execute(query, new
                        {
                            CheckinDetailId = Guid.NewGuid().ToString(),
                            CheckInId = chekin.CheckInId,
                            ClubNumber = chekin.ClubNumber,
                            CheckInTimeStamp = chekin.CheckInTimeStamp
                        });

                        connection.Execute(queryMemberCheckinDetail, new
                        {
                            MemberCheckinDetailId = Guid.NewGuid().ToString(),
                            MemberId = membersCheckinsDetail.MemberId,
                            CheckinId = chekin.CheckInId
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersCheckinsDetails exception:{ex.Message}");
            }
        }

        public void InsertMembersChildren(MemberChildModel memberChild)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[MemberChild] ([MemberChildId],[MemberChildEmergencyContactId],[ActiveStatus],[HomeClub],[FirstName],[MiddleInitial],[LastName],[Barcode],[Gender],[BirthDate],
					            [PrimaryMemberId],[AgreementNumber],[ChildMisc1],[ChildMisc2],[ChildMisc3],[CreateTimestamp],[LastModifiedTimestamp],[LastCheckInTimestamp] )
                                VALUES (@MemberChildId,@MemberChildEmergencyContactId,@ActiveStatus,@HomeClub,@FirstName,@MiddleInitial,@LastName,@Barcode,@Gender,@BirthDate,
					            @PrimaryMemberId,@AgreementNumber,@ChildMisc1,@ChildMisc2,@ChildMisc3,@CreateTimestamp,@LastModifiedTimestamp,@LastCheckInTimestamp)";

                    var queryChildNote = @"INSERT INTO [dbo].[ChildNotes] ([ChildNotesId],[MemberChildNoteId],[MemberChildId])
                                         VALUES (@ChildNotesId,@MemberChildNoteId,@MemberChildId)";

                    var queryMemberChildNote = @"INSERT INTO [dbo].[MemberChildNote] ([MemberChildNoteId],[NoteText],[NoteCreateTimestamp],[EmployeeId])
                                               VALUES (@MemberChildNoteId,@NoteText,@NoteCreateTimestamp,@EmployeeId)";

                    connection.Execute(query, new
                    {
                        MemberChildId = memberChild.MemberChildId,
                        MemberChildEmergencyContactId = memberChild.MemberChildEmergencyContact,
                        ActiveStatus = memberChild.ActiveStatus,
                        HomeClub = memberChild.HomeClub,
                        FirstName = memberChild.FirstName,
                        MiddleInitial = memberChild.MiddleInitial,
                        LastName = memberChild.LastName,
                        Barcode = memberChild.Barcode,
                        Gender = memberChild.Gender,
                        BirthDate = memberChild.BirthDate,
                        PrimaryMemberId = memberChild.PrimaryMemberId,
                        AgreementNumber = memberChild.AgreementNumber,
                        ChildMisc1 = memberChild.ChildMisc1,
                        ChildMisc2 = memberChild.ChildMisc2,
                        ChildMisc3 = memberChild.ChildMisc3,
                        CreateTimestamp = memberChild.CreateTimestamp,
                        LastModifiedTimestamp = memberChild.LastModifiedTimestamp,
                        LastCheckInTimestamp = memberChild.LastCheckInTimestamp
                    });

                    foreach (var note in memberChild.Notes)
                    {
                        var childNoteGuid = Guid.NewGuid().ToString();

                        connection.Execute(queryMemberChildNote, new
                        {
                            MemberChildNoteId = childNoteGuid,
                            NoteText = note.NoteText,
                            NoteCreateTimestamp = note.NoteCreateTimestamp,
                            EmployeeId = note.EmployeeId
                        });

                        connection.Execute(queryChildNote, new
                        {
                            ChildNotesId = Guid.NewGuid().ToString(),
                            MemberChildNoteId = childNoteGuid,
                            MemberChildId = memberChild.MemberChildId
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersChildren exception:{ex.Message}");
            }
        }

        public void InsertMembersGroups(List<ClubGroup> groups)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[Group] ([Id],[Name],[Status])
                                      VALUES (@Id,@Name,@Status)";

                    foreach (var group in groups)
                    {
                        connection.Execute(query, new
                        {
                            Id = group.Id,
                            Name = group.Name,
                            Status = group.Status
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersGroups  exception:{ex.Message}");
            }
        }

        //public string InsertMembersPersonal(Personal memberPersonal, string memberId)
        //{
        //    try
        //    {
        //        var PersonalId = Guid.NewGuid().ToString();
        //        using (SqlConnection connection = new SqlConnection(_connectionString))
        //        {
        //            var query = @"INSERT INTO [dbo].[Personal] ([PersonalId],[FirstName],[LastName],[MiddleInitial],[AddressLine1],[AddressLine2],[City],[State],[PostalCode],[HomeClub],[CountryCode],
        //                        [Email],[PrimaryPhone],[MobilePhone],[WorkPhone],[WorkPhoneExt],[EmergencyContactName],[EmergencyPhone],[EmergencyExt],[EmergencyAvailability],[Barcode],[BirthDate],[Gender],
        //                        [Employer],[Occupation],[Group],[IsActive],[MemberStatus],[JoinStatus],[IsConvertedProspect],[HasPhoto],[ConvertedDate],[MemberStatusReason],[WellnessProgramId],[FirstCheckInTimestamp],
        //                        [MemberStatusDate],[LastCheckInTimestamp],[TotalCheckInCount],[CreateTimestamp],[LastModifiedTimestamp],[MemberId])
        //                        VALUES (@PersonalId,@FirstName,@LastName,@MiddleInitial,@AddressLine1,@AddressLine2,@City,@State,@PostalCode,@HomeClub,@CountryCode,@Email,@PrimaryPhone,@MobilePhone,@WorkPhone, 
        //                        @WorkPhoneExt,@EmergencyContactName,@EmergencyPhone,@EmergencyExt,@EmergencyAvailability,@Barcode,@BirthDate,@Gender,@Employer,@Occupation,@Group,@IsActive,@MemberStatus,@JoinStatus,
        //                        @IsConvertedProspect,@HasPhoto,@ConvertedDate,@MemberStatusReason,@WellnessProgramId,@FirstCheckInTimestamp,@MemberStatusDate,@LastCheckInTimestamp,@TotalCheckInCount,@CreateTimestamp,
        //                        @LastModifiedTimestamp,@MemberId)";
        //            connection.Execute(query, new
        //            {
        //                PersonalId = PersonalId,
        //                FirstName = memberPersonal.FirstName,
        //                LastName = memberPersonal.LastName,
        //                MiddleInitial = memberPersonal.MiddleInitial,
        //                AddressLine1 = memberPersonal.AddressLine1,
        //                AddressLine2 = memberPersonal.AddressLine2,
        //                City = memberPersonal.City,
        //                State = memberPersonal.State,
        //                PostalCode = memberPersonal.PostalCode,
        //                HomeClub = memberPersonal.HomeClub,
        //                CountryCode = memberPersonal.CountryCode,
        //                Email = memberPersonal.Email,
        //                PrimaryPhone = memberPersonal.PrimaryPhone,
        //                MobilePhone = memberPersonal.MobilePhone,
        //                WorkPhone = memberPersonal.WorkPhone,
        //                WorkPhoneExt = memberPersonal.WorkPhoneExt,
        //                EmergencyContactName = memberPersonal.EmergencyContactName,
        //                EmergencyPhone = memberPersonal.EmergencyPhone,
        //                EmergencyExt = memberPersonal.EmergencyExt,
        //                EmergencyAvailability = memberPersonal.EmergencyAvailability,
        //                Barcode = memberPersonal.Barcode,
        //                BirthDate = memberPersonal.BirthDate,
        //                Gender = memberPersonal.Gender,
        //                Employer = memberPersonal.Employer,
        //                Occupation = memberPersonal.Occupation,
        //                Group = memberPersonal.Group,
        //                IsActive = memberPersonal.IsActive,
        //                MemberStatus = memberPersonal.MemberStatus,
        //                JoinStatus = memberPersonal.JoinStatus,
        //                IsConvertedProspect = memberPersonal.IsConvertedProspect,
        //                HasPhoto = memberPersonal.HasPhoto,
        //                ConvertedDate = memberPersonal.ConvertedDate,
        //                MemberStatusReason = memberPersonal.MemberStatusReason,
        //                WellnessProgramId = memberPersonal.WellnessProgramId,
        //                FirstCheckInTimestamp = memberPersonal.FirstCheckInTimestamp,
        //                MemberStatusDate = memberPersonal.MemberStatusDate,
        //                LastCheckInTimestamp = memberPersonal.LastCheckInTimestamp,
        //                TotalCheckInCount = memberPersonal.TotalCheckInCount,
        //                CreateTimestamp = memberPersonal.CreateTimestamp,
        //                LastModifiedTimestamp = memberPersonal.LastModifiedTimestamp,
        //                MemberId = memberId
        //            });
        //        }
        //        return PersonalId;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error($"Insert MembersPersonal exception:{ex.Message}");
        //        return null;
        //    }
        //}

        public void SaveDataToDb<T>(List<T> dataList, string columnName, string connectionString = null)
        {
            var dt = Common.ConvertDataToDataTable(dataList);
            using (SqlConnection connection = new SqlConnection(connectionString != null ? connectionString : _connectionString))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        var props = typeof(T).GetProperties();
                        foreach (var item in props)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.Name, item.Name);
                        }
                        sqlBulkCopy.DestinationTableName = columnName;
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"Save {columnName} exception:" + ex.Message);

                }

            }
        }

        public string InsertMembersPersonal(Personal memberPersonal, string memberId, string club)
        {
            try
            {

                var PersonalId = Guid.NewGuid().ToString();
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[Members] ([MemberId],[FirstName],[LastName],[MiddleInitial],[AddressLine1],[AddressLine2],[City],[State],[PostalCode],[HomeClub],[CountryCode],
                                [Email],[PrimaryPhone],[MobilePhone],[WorkPhone],[WorkPhoneExt],[EmergencyContactName],[EmergencyPhone],[EmergencyExt],[EmergencyAvailability],[Barcode],[BirthDate],[Gender],
                                [Employer],[Occupation],[Group],[IsActive],[MemberStatus],[JoinStatus],[IsConvertedProspect],[HasPhoto],[ConvertedDate],[MemberStatusReason],[WellnessProgramId],[FirstCheckInTimestamp],
                                [MemberStatusDate],[LastCheckInTimestamp],[TotalCheckInCount],[CreateTimestamp],[LastModifiedTimestamp],[ClubNumber])
                                VALUES (@MemberId,@FirstName,@LastName,@MiddleInitial,@AddressLine1,@AddressLine2,@City,@State,@PostalCode,@HomeClub,@CountryCode,@Email,@PrimaryPhone,@MobilePhone,@WorkPhone, 
                                @WorkPhoneExt,@EmergencyContactName,@EmergencyPhone,@EmergencyExt,@EmergencyAvailability,@Barcode,@BirthDate,@Gender,@Employer,@Occupation,@Group,@IsActive,@MemberStatus,@JoinStatus,
                                @IsConvertedProspect,@HasPhoto,@ConvertedDate,@MemberStatusReason,@WellnessProgramId,@FirstCheckInTimestamp,@MemberStatusDate,@LastCheckInTimestamp,@TotalCheckInCount,@CreateTimestamp,
                                @LastModifiedTimestamp,@ClubNumber)";
                    connection.Execute(query, new
                    {
                        MemberId = memberId,
                        FirstName = memberPersonal.FirstName,
                        LastName = memberPersonal.LastName,
                        MiddleInitial = memberPersonal.MiddleInitial,
                        AddressLine1 = memberPersonal.AddressLine1,
                        AddressLine2 = memberPersonal.AddressLine2,
                        City = memberPersonal.City,
                        State = memberPersonal.State,
                        PostalCode = memberPersonal.PostalCode,
                        HomeClub = memberPersonal.HomeClub,
                        CountryCode = memberPersonal.CountryCode,
                        Email = memberPersonal.Email,
                        PrimaryPhone = memberPersonal.PrimaryPhone,
                        MobilePhone = memberPersonal.MobilePhone,
                        WorkPhone = memberPersonal.WorkPhone,
                        WorkPhoneExt = memberPersonal.WorkPhoneExt,
                        EmergencyContactName = memberPersonal.EmergencyContactName,
                        EmergencyPhone = memberPersonal.EmergencyPhone,
                        EmergencyExt = memberPersonal.EmergencyExt,
                        EmergencyAvailability = memberPersonal.EmergencyAvailability,
                        Barcode = memberPersonal.Barcode,
                        BirthDate = memberPersonal.BirthDate,
                        Gender = memberPersonal.Gender,
                        Employer = memberPersonal.Employer,
                        Occupation = memberPersonal.Occupation,
                        Group = memberPersonal.Group,
                        IsActive = memberPersonal.IsActive,
                        MemberStatus = memberPersonal.MemberStatus,
                        JoinStatus = memberPersonal.JoinStatus,
                        IsConvertedProspect = memberPersonal.IsConvertedProspect,
                        HasPhoto = memberPersonal.HasPhoto,
                        ConvertedDate = memberPersonal.ConvertedDate,
                        MemberStatusReason = memberPersonal.MemberStatusReason,
                        WellnessProgramId = memberPersonal.WellnessProgramId,
                        FirstCheckInTimestamp = memberPersonal.FirstCheckInTimestamp,
                        MemberStatusDate = memberPersonal.MemberStatusDate,
                        LastCheckInTimestamp = memberPersonal.LastCheckInTimestamp,
                        TotalCheckInCount = memberPersonal.TotalCheckInCount,
                        CreateTimestamp = memberPersonal.CreateTimestamp,
                        LastModifiedTimestamp = memberPersonal.LastModifiedTimestamp,
                        ClubNumber = Int32.Parse(club)
                    });
                }
                return PersonalId;
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersPersonal exception:{ex.Message}");
                return null;
            }
        }

        public void InsertMembersPersonals(List<PersonalsMemberModel> membersPersonals)
        {

            var personals = MapToPersonal(membersPersonals);
            //var dt = ConvertDataToDataTable(personals);
            //using (SqlConnection connection = new SqlConnection(_connectionString))
            //{
            //    connection.Open();
            //    try
            //    {
            //        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
            //        {
            //            foreach (var item in GetUserColumnNames("Personal"))
            //            {
            //                sqlBulkCopy.ColumnMappings.Add(item, item);
            //            }
            //            sqlBulkCopy.DestinationTableName = "Personal";
            //            sqlBulkCopy.WriteToServer(dt);

            //        }
            //    }
            try
            {

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[Personal] VALUES (@PersonalId,@FirstName,@LastName,@MiddleInitial,@AddressLine1,@AddressLine2,@City,@State,@PostalCode,@HomeClub,@CountryCode,@Email,@PrimaryPhone,@MobilePhone,@WorkPhone, 
                                @WorkPhoneExt,@EmergencyContactName,@EmergencyPhone,@EmergencyExt,@EmergencyAvailability,@Barcode,@BirthDate,@Gender,@Employer,@Occupation,@Group,@IsActive,@MemberStatus,@JoinStatus,
                                @IsConvertedProspect,@HasPhoto,@ConvertedDate,@MemberStatusReason,@WellnessProgramId,@FirstCheckInTimestamp,@MemberStatusDate,@LastCheckInTimestamp,@TotalCheckInCount,@CreateTimestamp,
                                @LastModifiedTimestamp,@MemberId)";

                    connection.Execute(query, personals);
                }
            }

            catch (Exception ex)
            {
                _logger.Error($"Insert MembersPersonals exception:{ex.Message}");
            }
        }




        private List<string> GetUserColumnNames(string tableName)
        {
            var res = new SqlConnection(_connectionString).Query<string>($"SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('{tableName}')").ToList();
            return res;
        }
        public async Task<int?> DeleteMember(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var start = startDate.ToString(_apiDateTimeFromat) + " 00:00:00";
                var end = endDate.ToString(_apiDateTimeFromat) + " 23:59:59";
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    //var getQuery = $"SELECT MemberId FROM Members WHERE LastModifiedTimestamp >= '{start}' AND LastModifiedTimestamp <= '{end}' AND ClubNumber = @club ";
                    //var members = connection.Query<string>(getQuery, new { club = clubNumber });

                    var query = $"DELETE FROM Members WHERE LastModifiedTimestamp >= '{start}' AND LastModifiedTimestamp <= '{end}'  AND ClubNumber = @club";
                    var res = await connection.ExecuteAsync(query, new { club = clubNumber }, null, 400);
                    return res;
                    //foreach (var memberId in members)
                    //{
                    //    var agreementDelete = @"DELETE FROM Agreement WHERE MemberId = @Id";
                    //    connection.Execute(agreementDelete, new { Id = memberId });
                    //}
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersPersonals exception:{ex.Message}");
                return null;
            }
        }

        private List<Personal> MapToPersonal(List<PersonalsMemberModel> membersPersonals)
        {
            var personals = membersPersonals.Select(pers => new Personal
            {
                PersonalId = Guid.NewGuid().ToString(),
                MemberId = pers.MemberId,
                FirstName = pers.Personal.FirstName,
                LastName = pers.Personal.LastName,
                MiddleInitial = pers.Personal.MiddleInitial,
                AddressLine1 = pers.Personal.AddressLine1,
                AddressLine2 = pers.Personal.AddressLine2,
                City = pers.Personal.City,
                State = pers.Personal.State,
                PostalCode = pers.Personal.PostalCode,
                HomeClub = pers.Personal.HomeClub,
                CountryCode = pers.Personal.CountryCode,
                Email = pers.Personal.Email,
                PrimaryPhone = pers.Personal.PrimaryPhone,
                MobilePhone = pers.Personal.MobilePhone,
                WorkPhone = pers.Personal.WorkPhone,
                WorkPhoneExt = pers.Personal.WorkPhoneExt,
                EmergencyContactName = pers.Personal.EmergencyContactName,
                EmergencyPhone = pers.Personal.EmergencyPhone,
                EmergencyExt = pers.Personal.EmergencyExt,
                EmergencyAvailability = pers.Personal.EmergencyAvailability,
                Barcode = pers.Personal.Barcode,
                BirthDate = pers.Personal.BirthDate,
                Gender = pers.Personal.Gender,
                Employer = pers.Personal.Employer,
                Occupation = pers.Personal.Occupation,
                Group = pers.Personal.Group,
                IsActive = pers.Personal.IsActive,
                MemberStatus = pers.Personal.MemberStatus,
                JoinStatus = pers.Personal.JoinStatus,
                IsConvertedProspect = pers.Personal.IsConvertedProspect,
                HasPhoto = pers.Personal.HasPhoto,
                ConvertedDate = pers.Personal.ConvertedDate,
                MemberStatusReason = pers.Personal.MemberStatusReason,
                WellnessProgramId = pers.Personal.WellnessProgramId,
                FirstCheckInTimestamp = pers.Personal.FirstCheckInTimestamp,
                MemberStatusDate = pers.Personal.MemberStatusDate,
                LastCheckInTimestamp = pers.Personal.LastCheckInTimestamp,
                TotalCheckInCount = pers.Personal.TotalCheckInCount,
                CreateTimestamp = pers.Personal.CreateTimestamp,
                LastModifiedTimestamp = pers.Personal.LastModifiedTimestamp
            }).ToList();

            return personals;
        }
    }
}
