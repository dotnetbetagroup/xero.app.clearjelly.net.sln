﻿using NLog;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Data.SqlClient;
using System.Linq;
using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Entities;
using Dapper.Contrib.Extensions;
using ClearJelly.ABCFinancialApp.Models.Clubs;
using ClearJelly.ABCFinancialApp.Models.Clubs.Clubs;
using Dapper;
using System.Data;
using ClearJelly.ABCFinancialApp.Mapers;
using ClearJelly.ABCFinancialApp.Models.Clubs.TransactionsPos;

namespace ClearJelly.ABCFinancialApp.Repositories
{
    public class ABCFinancialsClubsRepository
    {
        private readonly string _connectionString;
        private readonly Logger _logger;
        private static readonly string _apiDateTimeFromat = "yyyy-MM-dd";
        public ABCFinancialsClubsRepository(string connectionString)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _connectionString = connectionString;
        }

        public void InsertClub(ClubModel club)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var minorGuid = Guid.NewGuid().ToString();
                    InsertMinor(connection, minorGuid, club);

                    var CCNameGuid = Guid.NewGuid().ToString();
                    InsertCCName(connection, CCNameGuid, club);

                    var onlineGuid = Guid.NewGuid().ToString();
                    InsertOnline(connection, onlineGuid, CCNameGuid, minorGuid, club);

                    var queryClub = @"INSERT INTO [dbo].[ClubInformation] ([ClubInformationId],[Name],[ShortName],[TimeZone],[Address1],[Address2],[City],[State],[PostalCode],[Country],[Email],[DonationItem],[OnlineSignupAllowedPaymentMethods],[OnlineId],[BillingCountry])
                                    VALUES (@ClubInformationId,@Name,@ShortName,@TimeZone,@Address1,@Address2,@City,@State,@PostalCode,@Country,@Email,@DonationItem,@OnlineSignupAllowedPaymentMethods,@OnlineId,@BillingCountry)";

                    connection.Execute(queryClub, new
                    {
                        ClubInformationId = Guid.NewGuid().ToString(),
                        Name = club.Name,
                        ShortName = club.ShortName,
                        TimeZone = club.TimeZone,
                        Address1 = club.Address1,
                        Address2 = club.Address2,
                        City = club.City,
                        State = club.State,
                        PostalCode = club.PostalCode,
                        Country = club.Country,
                        Email = club.Email,
                        DonationItem = club.DonationItem,
                        OnlineSignupAllowedPaymentMethods = club.OnlineSignupAllowedPaymentMethods,
                        OnlineId = onlineGuid,
                        BillingCountry = club.BillingCountry
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert Club exception:{ex.Message}");
            }
        }
        public async Task<int?> ClearCheckinTableAsync(string clubNumber, DateTime startDate, DateTime endDate)
        {
            _logger.Info($"ClearCheckinTable started");
            var start = startDate.ToString(_apiDateTimeFromat) + " 00:00:00";
            var end = endDate.ToString(_apiDateTimeFromat) + " 23:59:59";
            try
            {
                using (SqlConnection _dbConnection = new SqlConnection(_connectionString))
                {
                    var query = $"DELETE FROM [CheckinsDetails] WHERE ClubNumber = @club and CheckInTimestamp >= '{start }' and CheckInTimestamp <= '{end }'";
                    var result = await _dbConnection.ExecuteAsync(query, new { club = clubNumber }, null, 400);
                    _logger.Info($"ClearCheckinTable finished: removed " + result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"ClearCheckinTable ex: {ex.Message}");
                return null;
            }
        }
        public async Task<int?> ClearPostransactionTableAsync(string clubNumber, DateTime startDate, DateTime endDate)
        {
            _logger.Info($"ClearPostransactionTable started");
            var start = startDate.ToString(_apiDateTimeFromat) + " 00:00:00";
            var end = endDate.ToString(_apiDateTimeFromat) + " 23:59:59";
            try
            {
                using (SqlConnection _dbConnection = new SqlConnection(_connectionString))
                {
                    var query = $"DELETE FROM [POSTransactions] WHERE ClubNumber = @club AND TransactionTimestamp >='{start}' AND TransactionTimestamp <= '{end}'";
                    var res = await _dbConnection.ExecuteAsync(query, new { club = clubNumber }, null, 400);
                    return res;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"ClearPostransactionTable ex: {ex.Message}");
                return null;
            }
        }

        private void InsertCCName(SqlConnection connection, string CCNameGuid, ClubModel club)
        {
            try
            {
                var queryCCNames = @"INSERT INTO [dbo].[CCNames] ([CCNamesId],[RequireCcNameMatch],[DifferentCcNamesDisclaimer])
                                       VALUES (@CCNamesId,@RequireCcNameMatch,@DifferentCcNamesDisclaimer)";

                connection.Execute(queryCCNames, new
                {
                    CCNamesId = CCNameGuid,
                    RequireCcNameMatch = club.Online.CcNames.RequireCcNameMatch,
                    DifferentCcNamesDisclaimer = club.Online.CcNames.DifferentCcNamesDisclaimer
                });
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void InsertMinor(SqlConnection connection, string minorGuid, ClubModel club)
        {
            try
            {
                var queryMinor = @"INSERT INTO [dbo].[Minors] ([MinorsId],[AllowMinors],[MinorAge],[MinorDisclaimer])
                                     VALUES (@MinorsId,@AllowMinors,@MinorAge,@MinorDisclaimer)";

                connection.Execute(queryMinor, new
                {
                    MinorsId = minorGuid,
                    AllowMinors = club.Online.Minors.AllowMinors,
                    MinorAge = club.Online.Minors.MinorAge,
                    MinorDisclaimer = club.Online.Minors.MinorDisclaimer
                });
            }
            catch (Exception ex)
            {
                return;
            }

        }

        private void InsertOnline(SqlConnection connection, string onlineGuid, string CCNameGuid, string minorGuid, ClubModel club)
        {
            try
            {
                var queryOnline = @"INSERT INTO [dbo].[Online] ([OnlineId],[MinorsId],[CCNamesId],[ShowFees])
                                       VALUES (@OnlineId,@MinorsId,@CCNamesId,@ShowFees)";

                connection.Execute(queryOnline, new
                {
                    OnlineId = onlineGuid,
                    MinorsId = minorGuid,
                    CCNamesId = CCNameGuid,
                    ShowFees = club.Online.ShowFees
                });
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public void InsertClubCheckins(List<ClubCheckinModel> clubCheckins, string clubNumber)
        {
            _logger.Info($"Insert ClubCheckins started");
            var checkins = ClubMapers.MapToCheckin(clubCheckins, clubNumber);
            var dt = Common.ConvertDataToDataTable(checkins);
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        var props = typeof(ClubCheckin).GetProperties();
                        foreach (var item in props)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.Name, item.Name);
                        }
                        sqlBulkCopy.DestinationTableName = "CheckinsDetails";
                        sqlBulkCopy.WriteToServer(dt);
                        _logger.Info($"Insert ClubCheckins Finished");

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"Insert MembersPersonals exception:{ex.Message}");
                }
            }

        }

        public void InsertClubMemberIdentitie(ClubMemberIdentity clubMemberIdentity)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var queryClubMemberIdentity = @"INSERT INTO [dbo].[Identities] ([IdentityId],[MembershipTypeAbcCode],[HomeVpdId],[HomeClub],[HomeCompanyName],[CorpId],[MemberId],[Barcode],[FirstName],[LastName],[IsActive])
                                       VALUES (@IdentityId,@MembershipTypeAbcCode,@HomeVpdId,@HomeClub,@HomeCompanyName,@CorpId,@MemberId,@Barcode,@FirstName,@LastName,@IsActive)";

                    connection.Execute(queryClubMemberIdentity, new
                    {
                        IdentityId = Guid.NewGuid().ToString(),
                        MembershipTypeAbcCode = clubMemberIdentity.MembershipTypeAbcCode,
                        HomeVpdId = clubMemberIdentity.HomeVpdId,
                        HomeClub = clubMemberIdentity.HomeClub,
                        HomeCompanyName = clubMemberIdentity.HomeCompanyName,
                        CorpId = clubMemberIdentity.CorpId,
                        MemberId = clubMemberIdentity.MemberId,
                        Barcode = clubMemberIdentity.Barcode,
                        FirstName = clubMemberIdentity.FirstName,
                        LastName = clubMemberIdentity.LastName,
                        IsActive = clubMemberIdentity.IsActive
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert ClubMemberIdentitie exception:{ex.Message}");
            }
        }

        public void InsertClubTransaction(List<ClubTransactionModel> clubTransactions, string club)
        {
            var transactions = clubTransactions.SelectMany(x => x.Transactions).ToList();
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    try
                    {
                        var query = $"select TransactionId FROM POSTransactions";
                        var ids = connection.Query<string>(query).ToList();
                        foreach (var item in ids)
                        {
                            var idExist = transactions.Where(x => x.TransactionId == item).FirstOrDefault();
                            if (idExist != null)
                            {
                                transactions.Remove(transactions.Single(x => x.TransactionId == item));
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.Error($"Insert Prospects exception:{ex.Message}");
                    }
                }
                var POSTransactions = new List<POSTransactionsModel>();
                var POS_transItems = new List<POS_transItemsModel>();
                var POS_transPayments = new List<POS_transPaymentsModel>();
                foreach (var transaction in transactions)
                {
                    POSTransactions.Add(ClubMapers.MapToPOSTransactions(transaction, club));
                    foreach (var item in transaction.Items.Item)
                    {
                        POS_transItems.Add(ClubMapers.MapToPOS_transItems(item, club, transaction.TransactionId));
                        foreach (var payment in item.Payments)
                        {
                            POS_transPayments.Add(ClubMapers.MapToPOS_transPayments(payment, item, transaction.TransactionId, club));
                        }
                    }
                }
                SaveDataToDb(POS_transPayments, "POS_transPayments");
                SaveDataToDb(POS_transItems, "POS_transItems");
                SaveDataToDb(POSTransactions, "POSTransactions");
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert ClubTransaction exception:{ex.Message}");
            }
        }

        public void SaveDataToDb<T>(List<T> dataList, string columnName, string connectionString = null)
        {
            var dt = Common.ConvertDataToDataTable(dataList);
            using (SqlConnection connection = new SqlConnection(connectionString != null ? connectionString : _connectionString))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        var props = typeof(T).GetProperties();
                        foreach (var item in props)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.Name, item.Name);
                        }
                        sqlBulkCopy.DestinationTableName = columnName;
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"Save {columnName} exception:" + ex.Message);

                }

            }
        }

        public void InsertClubPlans(List<ClubPlan> clubPlans)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[WebPlanLists] ([PlanId],[PlanName],[PromoCode],[PromoName],[AgreementDescription],[LimitedAvailability])
                                       VALUES (@PlanId,@PlanName,@PromoCode,@PromoName,@AgreementDescription,@LimitedAvailability)";

                    foreach (var clubPlan in clubPlans)
                    {
                        connection.Execute(query, new
                        {
                            PlanId = clubPlan.PlanId,
                            PlanName = clubPlan.PlanName,
                            PromoCode = clubPlan.PromoCode,
                            PromoName = clubPlan.PromoName,
                            AgreementDescription = clubPlan.AgreementDescription,
                            LimitedAvailability = clubPlan.LimitedAvailability
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert ClubPlans exception:{ex.Message}");
            }
        }

        public void InsertClubPlansAnnualFees(List<ClubPlanAnnualFeeModel> clubPlansAnnualFees)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = @"INSERT INTO [dbo].[AnnualFees] ([Id],[Name],[Amount],[PreTaxAmount],[ProfitCenterAbcCode])
                                VALUES (@Id,@Name,@Amount,@PreTaxAmount,@ProfitCenterAbcCode)";

                    foreach (var annualFee in clubPlansAnnualFees)
                    {
                        connection.Execute(query, new
                        {
                            Id = annualFee.Id,
                            Name = annualFee.Name,
                            Amount = annualFee.Amount,
                            PreTaxAmount = annualFee.PreTaxAmount,
                            ProfitCenterAbcCode = annualFee.ProfitCenterAbcCode
                        });

                        InsertClubNumbersInAnnualFees(connection, annualFee);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert ClubPlansAnnualFees exception:{ex.Message}");
            }
        }

        private void InsertClubNumbersInAnnualFees(SqlConnection connection, ClubPlanAnnualFeeModel annualFee)
        {
            try
            {
                var queryClubNumbersInAnnualFees = @"INSERT INTO [dbo].[ClubNumbersInAnnualFees] ([Id],[ClubNumber],[AnnualFeeId])
                                       VALUES (@Id,@ClubNumber,@AnnualFeeId)";
                foreach (var clubNumber in annualFee.AssignedClubNumbers)
                {
                    connection.Execute(queryClubNumbersInAnnualFees, new
                    {
                        Id = Guid.NewGuid().ToString(),
                        ClubNumber = clubNumber,
                        AnnualFeeId = annualFee.Id,
                    });
                }
            }
            catch (Exception ex)
            {
                return;
            }

        }

        public void InsertBillingCatalogItems(List<BillingCatalogItemModel> billingCatalogItems)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {

                    var query = @"INSERT INTO [dbo].[BillingCatalogItem] ([Id],[Name],[ProfitCenterAbcCode],[AnnualFee],[TotalTaxRate])
                                    VALUES (@Id,@Name,@ProfitCenterAbcCode,@AnnualFee,@TotalTaxRate)";

                    foreach (var billingCatalogItem in billingCatalogItems)
                    {
                        var billingGuid = Guid.NewGuid().ToString();
                        connection.Execute(query, new
                        {
                            Id = billingGuid,
                            Name = billingCatalogItem.Name,
                            ProfitCenterAbcCode = billingCatalogItem.ProfitCenterAbcCode,
                            AnnualFee = billingCatalogItem.AnnualFee,
                            TotalTaxRate = billingCatalogItem.TotalTaxRate
                        });
                        InsertClubNumber(connection, billingGuid, billingCatalogItem.AssignedClubNumbers);
                        InsertTaxes(connection, billingGuid, billingCatalogItem.Taxes);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert Club exception:{ex.Message}");
            }
        }

        private void InsertClubNumber(SqlConnection connection, string billingId, List<string> assignedClubNumbers)
        {
            try
            {
                var query = @"INSERT INTO [dbo].[ClubNumbersInBillingCatalogItem] ([Id],[ClubNumber],[BillingCatalogItemId])
                                    VALUES (@Id,@ClubNumber,@BillingCatalogItemId)";

                foreach (var assignedClubNumber in assignedClubNumbers)
                {
                    var billingGuid = Guid.NewGuid().ToString();
                    connection.Execute(query, new
                    {
                        Id = billingGuid,
                        ClubNumber = assignedClubNumber,
                        BillingCatalogItemId = billingId
                    });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void InsertTaxes(SqlConnection connection, string billingId, List<Tax> taxes)
        {
            var query = @"INSERT INTO [dbo].[TaxResponse] ([RateId],[Amount],[Name],[Percentage])
                                    VALUES (@RateId,@Amount,@Name,@Percentage)";

            var queryTaxResponsesInBillingCatalogItem = @"INSERT INTO [dbo].[TaxResponsesInBillingCatalogItem] ([Id],[TaxResponseId],[BillingCatalogItemId])
                                    VALUES (@Id,@TaxResponseId,@BillingCatalogItemId)";

            try
            {
                foreach (var tax in taxes)
                {
                    connection.Execute(query, new
                    {
                        RateId = tax.RateId,
                        Amount = tax.Amount,
                        Name = tax.Name,
                        Percentage = tax.Percentage
                    });
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                foreach (var tax in taxes)
                {
                    connection.Execute(queryTaxResponsesInBillingCatalogItem, new
                    {
                        Id = Guid.NewGuid().ToString(),
                        TaxResponseId = tax.RateId,
                        BillingCatalogItemId = billingId
                    });
                }
            }
        }

        public void InsertClubPlansPaymentMethods(PaymentMethodsModel clubPlansPaymentMethods)
        {
            string guid = "";
            string clubAccountIdNameId = InsertClubAccountPaymentMethods(clubPlansPaymentMethods.ClubAccountPaymentMethods);
            string abcBillingIdNameId = InsertAbcBillingPaymentMethods(clubPlansPaymentMethods.AbcBillingPaymentMethods);
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {

                    var query = @"INSERT INTO [dbo].[PaymentMethods] ([Id], [ClubAccountIdNameId], [AbcBillingIdNameId])
                                    VALUES (@CJId,@clubAccountIdNameId,@abcBillingIdNameId)";
                    guid = Guid.NewGuid().ToString();
                    connection.Execute(query, new
                    {
                        CJId = guid,
                        clubAccountIdNameId,
                        abcBillingIdNameId
                    });

                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert Club exception:{ex.Message}");
            }
        }

        private string InsertClubAccountPaymentMethods(List<PaymentMethod> clubPlansPaymentMethods)
        {
            string guid = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {

                    var query = @"INSERT INTO [dbo].[ClubAccountIdName] ([CJId], [Id],[Name],[AbcCode])
                                    VALUES (@CJId,@Id,@Name,@AbcCode)";

                    foreach (var item in clubPlansPaymentMethods)
                    {
                        guid = Guid.NewGuid().ToString();
                        connection.Execute(query, new
                        {
                            CJId = guid,
                            item.Id,
                            item.Name,
                            item.AbcCode
                        });
                    }
                }
                return guid;
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert Club exception:{ex.Message}");
                return null;
            }
        }

        private string InsertAbcBillingPaymentMethods(List<PaymentMethod> abcBillingPaymentMethods)
        {
            string guid = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {

                    var query = @"INSERT INTO [dbo].[AbcBillingIdName] ([CJId], [Id],[Name],[AbcCode])
                                    VALUES (@CJId,@Id,@Name,@AbcCode)";
                    foreach (var item in abcBillingPaymentMethods)
                    {
                        guid = Guid.NewGuid().ToString();
                        connection.Execute(query, new
                        {
                            CJId = guid,
                            item.Id,
                            item.Name,
                            item.AbcCode
                        });
                    }
                }
                return guid;
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert Club exception:{ex.Message}");
                return null;
            }
        }

        public void InsertСlubStations(List<ClubStation> clubStations)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {

                    var query = @"INSERT INTO [dbo].[Station] ([StationId],[Name],[Status],[AbcCode])
                                    VALUES (@StationId,@Name,@Status,@AbcCode)";

                    foreach (var item in clubStations)
                    {
                        connection.Execute(query, item);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert СlubStations exception:{ex.Message}");
            }
        }
    }
}
