﻿using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.ABCFinancialApp.Max;
using ClearJelly.ABCFinancialApp.Models.Members.Prospects;
using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.ABCFinancialApp.Repositories
{
   public class ABCFinancialsProspectsRepository
    {
        private readonly string _connectionString;
        private readonly Logger _logger;
        private static readonly string _apiDateTimeFromat = "yyyy-MM-dd";

        public ABCFinancialsProspectsRepository(string connectionString)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _connectionString = connectionString;
        }       

        public void InsertProspects(List<ProspectModel> prospectsModel, int clubNumber)
        {
            var prospects = ProspectMaper.MapToProspect(prospectsModel, clubNumber);
            _logger.Info($"Insert Prospects start");
            
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                try
                {
                    var query = $"select ProspectId FROM Prospects";
                    var ids = connection.Query<string>(query).ToList();
                    foreach (var item in ids)
                    {
                        var idExist = prospects.Where(x => x.ProspectId == item).FirstOrDefault();
                        if (idExist != null)
                        {
                            prospects.Remove(prospects.Single(x => x.ProspectId == item));
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    _logger.Error($"Insert Prospects exception:{ex.Message}");
                }
            }
            var dt = Common.ConvertDataToDataTable(prospects);
            //SqlConnection cn = new SqlConnection(_connectionString);
            //SqlDataAdapter da = new SqlDataAdapter("select * from Prospects", cn);
            //DataSet ds = new DataSet();
            //da.Fill(ds, "Prospects");
            //ds.Tables["Prospects"].Rows[0]["PreospectId"] = "Jack";
            //da.Update(ds, "Prospects");
            //System.Data.SqlClient.SqlBulkCopy bc = new SqlBulkCopy(cn);
            //bc.DestinationTableName = "Prospects"; // copy to self
            //cn.Open();
            //bc.WriteToServer(ds.Tables[0]);
            //cn.Dispose();


            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        var props = typeof(Prospect).GetProperties();
                        foreach (var item in props)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.Name, item.Name);
                        }                       
                        sqlBulkCopy.DestinationTableName = "Prospects";                       
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"Insert Prospects exception:{ex.Message}");
                }
            }
        }
        public async Task<int?> DeleteProspects(string clubNumber, DateTime startDate, DateTime endDate)
        {
           
            try
            {
                var start = startDate.ToString(_apiDateTimeFromat) + " 00:00:00";
                var end = endDate.ToString(_apiDateTimeFromat) + " 23:59:59";
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var query = $"DELETE FROM Prospects WHERE LastModifiedTimestamp >= '{start}' AND LastModifiedTimestamp <= '{end}' AND ClubNumber = @club";
                    var res = await connection.ExecuteAsync(query, new { club = clubNumber }, null, 400);
                    return res;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Insert MembersPersonals exception:{ex.Message}");
                return null;
            }
        }
    }
}
