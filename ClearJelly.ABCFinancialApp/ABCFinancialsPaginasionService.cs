﻿using ClearJelly.ABCFinancialApp.Helpers;
using ClearJelly.ABCFinancialApp.Models.Clubs.Clubs;
using ClearJelly.ABCFinancialApp.Providers;
using ClearJelly.ABCFinancialApp.Repositories;
using ClearJelly.Configuration;
using ClearJelly.DataAccess;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ClearJelly.Services;
using ClearJelly.Configuration.Enums;
using ClearJelly.Entities;
//using ClearJelly.Services;

namespace ClearJelly.ABCFinancialApp
{
    public class ABCFinancialsPaginasionService
    {
        private readonly ABCFinancialsClubsRepository _clubsRepository;
        private readonly ABCFinancialsMembersRepository _membersRepository;
        private readonly ABCFinancialsProspectsRepository _prospectsRepository;
        private readonly ABCFinancialsOAuthProvider _oAuthProvider;
        private readonly Logger _logger;
        private readonly ABCFinancialsMembersApiProviderWithPaging _membersProvider;
        private readonly ABCFinancialsProspectsApiProvider _prospectsProvider;
        
        private string _connectionString;
        ABCFinancialsClubsWithPagingApiProvider _clubsProvider;
        private readonly HomeRepository _homeRepository;

        public ABCFinancialsPaginasionService(string dbName)
        {
            _connectionString = ConfigSection.AbcCompanyDbConnection + dbName;
            _logger = LogManager.GetCurrentClassLogger();
            _clubsRepository = new ABCFinancialsClubsRepository(_connectionString);
            _membersRepository = new ABCFinancialsMembersRepository(_connectionString);
            _prospectsRepository =new ABCFinancialsProspectsRepository(_connectionString);
            _oAuthProvider = new ABCFinancialsOAuthProvider();
            _membersProvider = new ABCFinancialsMembersApiProviderWithPaging(dbName);
            _prospectsProvider = new ABCFinancialsProspectsApiProvider(dbName);
            _clubsProvider = new ABCFinancialsClubsWithPagingApiProvider(dbName);
            _homeRepository = new HomeRepository(ConfigSection.ClearJellyEntities);
        }

        public string GetLoginUrl()
        {
            return _oAuthProvider.GetLoginUrl();
        }

        public async Task GetToken(string authorizationCode)
        {
            await _oAuthProvider.GetToken(authorizationCode);
        }
        public ClubsResponse GetClubByNumber(string clubNumber, int userId)
        {
            var res = _clubsProvider.GetClub(clubNumber);
            res.Wait();
            if (res.Result != null)
            {
                _homeRepository.InsertClub(clubNumber, res.Result.Club.Name, userId);
            }
            return res.Result;
        }

        public async Task<bool> FetchAllDataAsync(string dbName, string club, DateTime startDate, DateTime endDate)
        {
            try
            {
                var listTasks = new List<Task>();
                _logger.Info("FetchAllDataAsync started");
                listTasks.Add(Task<bool>.Run(() => GetProspectsDataAsync(club, startDate, endDate)));
                listTasks.Add(Task<bool>.Run(() => GetMembersDataAsync(club, startDate, endDate)));
                listTasks.Add(Task<bool>.Run(() => GetClubsDataAsync(club, startDate, endDate)));

                await Task.WhenAll(listTasks);

                _logger.Info("FetchAllDataAsync completed");
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("FetchAllDataAsync "+ ex.Message +"\n"+ ex);
                return false;
            }     

        }

        private async Task<bool> GetMembersDataAsync(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var listTasks = new List<Task>();

                listTasks.Add(Task.Run(() => _membersProvider.GetMembers(clubNumber, startDate, endDate)));
                
                //listTasks.Add(Task.Run(() => _membersProvider.GetMembersCheckinsSummaries(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _membersProvider.GetMembersGroups(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _membersProvider.GetMembersPersonals(clubNumber, startDate, endDate)));

                await Task.WhenAll(listTasks);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async Task<bool> GetProspectsDataAsync(string clubNumber, DateTime startDate, DateTime endDate)
        {
            try
            {
                var listTasks = new List<Task>();
                listTasks.Add(Task.Run(() => _prospectsProvider.GetProspects(clubNumber, startDate, endDate)));
                await Task.WhenAll(listTasks);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("-GetProspectsDataAsync - "+ ex.Message + "\n" + ex);
                return false;
            }
        }
        //private async void GetMembersDataById(string dbName, DateTime startDate, DateTime endDate, string memberId)
        //{
        //    //add insert
        //    var memberDataById = await _membersProvider.GetMembersByMemberId(memberId, startDate, endDate);

        //    var membersCheckinsDetailsByMemberId = await _membersProvider.GetMembersCheckinsDetailsByMemberId(memberId, startDate, endDate);
        //    _membersRepository.InsertMembersCheckinsDetails(membersCheckinsDetailsByMemberId);

        //    var membersCheckinsSummariesById = await _membersProvider.GetMembersCheckinsSummariesByMemberId(memberId);

        //    var membersPersonalsById = await _membersProvider.GetMembersPersonalsByMemberId(memberId, startDate, endDate);

        //    var membersChildren = await _membersProvider.GetMembersChildernByMemberChildId(memberId, startDate, endDate);
        //    _membersRepository.InsertMembersChildren(membersChildren);
        //}

        private async Task<bool> GetClubsDataAsync(string clubNumber, DateTime startDate, DateTime endDate)
        {
            _logger.Info("GetClubsDataAsync started");
            try
            {
                var listTasks = new List<Task>();

                //listTasks.Add(Task.Run(() => _clubsProvider.GetClub(clubNumber, startDate, endDate)));

                listTasks.Add(Task.Run(() => _clubsProvider.GetClubCheckinsDetails(clubNumber, startDate, endDate)));

                ////WHERE TO GET BARCODE?
                ////Task.Run(() => _clubsProvider.GetClubMemberIdentitieByBarcode(startDate, endDate));
                ////var memberIdentitie = await clubsProvider.GetClubMemberIdentitieByBarcode("213");
                ////_clubsRepository.InsertClubMemberIdentitie(memberIdentitie);

                listTasks.Add(Task.Run(() => _clubsProvider.GetClubTransactionPos(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlans(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlansAnnualFees(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlansBillingCatalogItems(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _clubsProvider.GetClubPlansPaymentMethods(clubNumber, startDate, endDate)));

                //listTasks.Add(Task.Run(() => _clubsProvider.GetClubStations(clubNumber, startDate, endDate)));

                await Task.WhenAll(listTasks);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"GetClubsDataAsync ex: {ex.Message}");
                return false;
            }
        }

        public async Task<bool> UpdateMembers(string dbName, string club, DateTime startDate, DateTime endDate, User currentUser)
        {
            _logger.Info("Clear tables process started");
            await _clubsRepository.ClearCheckinTableAsync(club, startDate, endDate);
            await _clubsRepository.ClearPostransactionTableAsync(club, startDate, endDate);
            await _membersRepository.DeleteMember(club, startDate, endDate);
           // await _prospectsRepository.DeleteProspects(club, startDate, endDate);
            //var tableNames = RepositoryHelper.GetUserTableNames(_connectionString);
            //foreach (var tbName in tableNames)
            //{
            //    await RepositoryHelper.ClearTable(tbName, club, _connectionString, startDate, endDate);
            //}
            _logger.Info("UpdateMembers started");
           var isCompleted  = await FetchAllDataAsync(dbName, club, startDate, endDate);
            if (isCompleted)
            {
                _logger.Info("Trying send mail about inserted items to DB");
                SmtpMailSendingService.SendDbUpdateFinishedMailMessage(currentUser, ServiceTypes.ABC.ToString());
                _logger.Info("End send mail about inserted items to DB");
                return true;
            }
            return true;
        }

        public async Task<bool> CreateAbcDbAsync(string companyName)
        {
            DbGeneratorHelper generator = new DbGeneratorHelper(ConfigSection.ClearJellyEntities, companyName);
            var isDbCreated = await generator.CreateABCDatabaseAsync();
            //if (isDbCreated)
            //{
                ABCFinancialsMemberGenerator memberGenerator = new ABCFinancialsMemberGenerator(companyName);
                await memberGenerator.GenerateTables();

                //ABCFinancialsClubGenerator clubGenerator = new ABCFinancialsClubGenerator(companyName);
                //await clubGenerator.GenerateTables();
                return true;
            //}
            //return false;
        }

        //public async Task<bool> CreateAbcDbAsync(string companyName, string userName, string password)
        //{
        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(ConfigSection.ClearJellyEntities))
        //        {
        //            var sqlStr = @" Execute [dbo].[Create_Updated_ABC_Financial_DB_Template] '" + companyName + "','" + userName + "','" + password + "'";
        //            connection.Execute(sqlStr);

        //            _logger.Info("ABC DB created ok");
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error($"ABC DB creation failed, ex: {ex.Message}");
        //        return true;
        //    }


        //}
    }
}
