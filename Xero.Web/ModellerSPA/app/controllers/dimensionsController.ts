﻿//import * as Dimension from "../entities/dimension";
//import * as DimensionService from "../services/dimensionService";

module Modeller.Core.Controllers {
    export class DimensionsController {
        static $inject = ["$state", "$stateParams", "dimensionService"];
        dimensions: Array<Modeller.Core.Domain.Dimension>;
        selectedDimensionName: string;
        selectedDimensionElements: Array<any>;
        getCubesModel: IGetCubesViewModel = { username: "xlUser%40zealousys.com", password: "Ze%40lous2012", cube: "TL_summary" };
        selectedDimensionGridOptions: any;
        dimensionsGridOptions: any;
        editDimensionGridOptions: any;
        isGridLoaded: boolean = false;
        excelDataWindow: kendo.ui.Window;
        editDataModel: any;
        //isSaveLoader: boolean = false;
        dataPasteSuccessWindow: kendo.ui.Window;

        constructor(private $state: angular.ui.IStateService,
            private $stateParams: IDimensionStateParams,
            private dimensionService: Modeller.Core.Services.IDimensionService) {
            if ($state.current.name == "dimensions") {
                this.getAllDimensions();
            }
            if ($state.current.name == "dimension") {
                this.getDimentionByName($stateParams.dimension);
            }
            if ($state.current.name == "editDimension") {
                this.getEditDimension($stateParams.dimension);
            }
        }

        getDimentionByName = (name: string) => {
            var self = this;
            var dimensionSchema = [];
            this.dimensionService.getAllDimensions(this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].Name == name) {
                        angular.forEach(response[i].Fields, function (value, key) {
                            angular.forEach(value, function (opt, key) {
                                if (key == "Name") {
                                    dimensionSchema.push({
                                        field: opt.replace(/ /g, ''),
                                        title: opt.replace(/ /g, ''),
                                        width: "200px",
                                        filterable: {
                                            cell: {
                                                operator: "contains",
                                                showOperators: false,
                                                template: function (e) {
                                                    e.element.kendoAutoComplete({
                                                        serverFiltering: false,
                                                        valuePrimitive: true,
                                                        noDataTemplate: ''
                                                    });
                                                }
                                            }
                                        }

                                    });
                                }
                            });
                        });
                    }
                }
                var resources = new kendo.data.DataSource({
                    schema: {
                        data: function (result) {
                            return result.data || result;
                        },
                        total: function (result) {
                            var data = this.data(result);
                            return data ? data.length : 0;
                        }
                    },
                    transport: {
                        read: function (e) {
                            self.dimensionService.getDimensionByName(name, self.getCubesModel.username, self.getCubesModel.password)
                                .then(function (data) {
                                    e.success(data);
                                    var arr = []
                                    for (var i = 0; i < data.length; i++) {
                                        var obj = {};
                                        angular.forEach(data[i], function (value, key) {
                                            key = key.replace(/ /g, '');
                                            obj[key] = value;
                                        });
                                        arr.push(obj);
                                    }
                                    resources.data(arr);
                                    self.selectedDimensionElements = arr;
                                    self.selectedDimensionName = name;
                                    self.isGridLoaded = true;
                                });
                        }
                    },
                    pageSize: 10,
                    sort: ({ field: "ID", dir: "asc" })
                });
                this.selectedDimensionGridOptions = {
                    dataSource: resources,
                    sortable: true,
                    pageable: true,
                    filterable: {
                        mode: "row"
                    },
                    filterMenuInit: function (e) {
                        var firstValueDropDown = e.container.find("select:eq(0)").data("kendoDropDownList");
                        setTimeout(function () {
                            firstValueDropDown.wrapper.hide();
                        })
                    },
                    columns: dimensionSchema
                };
            });

            //this.dimensionService.getDimensionByName(name, this.getCubesModel.username, this.getCubesModel.password).then((response) => {
            //    console.log(response);
            //    var arr = []
            //    for (var i = 0; i < response.length; i++) {
            //        var obj = {};
            //        angular.forEach(response[i], function (value, key) {
            //            key = key.replace(/ /g, '');
            //            obj[key] = value;
            //        });
            //        arr.push(obj);
            //    }
            //    console.log(arr);
            //    this.selectedDimension.Elements = arr;
            //    var schema = this.selectedDimension.Elements[0];
            //    var tableColumns = [];
            //    angular.forEach(schema, function (value, key) {
            //        this.push({
            //            field: key,
            //            title: key,
            //            filterable: {
            //                cell: {
            //                    operator: "contains",
            //                    showOperators: false,
            //                    template: function (e) {
            //                        e.element.kendoAutoComplete({
            //                            serverFiltering: false,
            //                            valuePrimitive: true,
            //                            noDataTemplate: ''
            //                        });
            //                    }
            //                }
            //            }
            //        });
            //    }, tableColumns);
            //    console.log(tableColumns);
            //    var resources = new kendo.data.DataSource({
            //        sort: ({ field: "ID", dir: "asc" })
            //    });
            //    resources.data(this.selectedDimension.Elements);
            //    console.log(resources);
            //    this.selectedDimensionGridOptions = {
            //        dataSource: {
            //            type: "odata",
            //            transport: {
            //                read: this.dimensionService.getDimensionByName(name, this.getCubesModel.username, this.getCubesModel.password)
            //            },
            //            pageSize: 5,
            //            serverPaging: true,
            //            serverSorting: true
            //        },
            //        sortable: true,
            //        pageable: true,
            //        filterable: {
            //            mode: "row"
            //        },
            //        filterMenuInit: function (e) {
            //            //if (e.field === "AccountNumber") {
            //                var firstValueDropDown = e.container.find("select:eq(0)").data("kendoDropDownList");

            //                setTimeout(function () {
            //                    firstValueDropDown.wrapper.hide();
            //                })
            //            //}
            //        },
            //        columns: dimensionSchema
            //    };
            //    console.log(this.selectedDimensionGridOptions);
            //});
        }

        getAllDimensions = () => {
            this.dimensionService.getAllDimensions(this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                for (var i = 0; i < response.length; i++) {
                    var arr = response[i].Fields;
                    var str = "";
                    for (var j = 0; j < arr.length; j++) {
                        angular.forEach(arr[j], function (value, key) {
                            if (key == "Name") {
                                if (j > 0) {
                                    str += ", ";
                                }
                                str = str + value;
                            }
                        });
                    }
                    response[i].Fields = str;
                }
                this.dimensions = response;
                var schema = this.dimensions[0];
                var tableColumns = [];
                angular.forEach(schema, function (value, key) {
                    this.push({
                        field: key,
                        title: key
                    });
                }, tableColumns);
                var resources = new kendo.data.DataSource({
                    sort: ({ field: "ID", dir: "asc" })
                });
                resources.data(this.dimensions);
                this.dimensionsGridOptions = {
                    dataSource: resources,
                    pageSize: 5,
                    serverPaging: true,
                    serverSorting: true,
                    sortable: true,
                    pageable: true,
                    columns: tableColumns
                };
                this.isGridLoaded = true;
            });
        }

        getEditDimension = (name: string) => {
            var self = this;
            var dimensionSchema = [];
            this.dimensionService.getAllDimensions(this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                var dataModel = { fields: {} };
                var editDataMod = [];
                for (var i = 0; i < response.length; i++) {
                    if (response[i].Name == name) {
                        angular.forEach(response[i].Fields, function (value, key) {
                            angular.forEach(value, function (opt, key) {
                                if (key == "Name") {
                                    //var counter = 0;
                                    if (opt == "ID") {
                                        dataModel.fields[opt] = {
                                            type: "number", editable: false
                                        };
                                        //editDataMod.push(opt);
                                        dimensionSchema.push({
                                            field: opt.replace(/ /g, ''),
                                            title: opt.replace(/ /g, ''),
                                            width: "200px",
                                            editable: false,
                                            filterable: {
                                                cell: {
                                                    operator: "contains",
                                                    showOperators: false,
                                                    template: function (e) {
                                                        e.element.kendoAutoComplete({
                                                            serverFiltering: false,
                                                            valuePrimitive: true,
                                                            noDataTemplate: ''
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        dataModel.fields[opt] = {
                                            type: "string"
                                        };
                                        editDataMod.push(opt);
                                        dimensionSchema.push({
                                            field: opt.replace(/ /g, ''),
                                            title: opt.replace(/ /g, ''),
                                            width: "200px",
                                            template: "#=dirtyField(data,'" + opt.replace(/ /g, '') + "')# #:" + opt.replace(/ /g, '') + "#",
                                            filterable: {
                                                cell: {
                                                    operator: "contains",
                                                    showOperators: false,
                                                    template: function (e) {
                                                        e.element.kendoAutoComplete({
                                                            serverFiltering: false,
                                                            valuePrimitive: true,
                                                            noDataTemplate: ''
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                }
                this.editDataModel = editDataMod;
                var resources = new kendo.data.DataSource({
                    schema: {
                        data: function (result) {
                            return result.data || result;
                        },
                        total: function (result) {
                            var data = this.data(result);
                            return data ? data.length : 0;
                        },
                        model: dataModel
                    },
                    transport: {
                        read: function (e) {
                            self.dimensionService.getDimensionByName(name, self.getCubesModel.username, self.getCubesModel.password)
                                .then(function (data) {
                                    e.success(data);
                                    var arr = []
                                    for (var i = 0; i < data.length; i++) {
                                        var obj = {};
                                        angular.forEach(data[i], function (value, key) {
                                            key = key.replace(/ /g, '');
                                            obj[key] = value;
                                        });
                                        arr.push(obj);
                                    }
                                    resources.data(arr);
                                    self.selectedDimensionElements = arr;
                                    self.selectedDimensionName = name;
                                    self.isGridLoaded = true;
                                    self.bindTabFunc();
                                });
                        }
                    },
                    pageSize: 10,
                    sort: ({ field: "ID", dir: "asc" }),
                    change: function (e) {
                        if (e.action == "itemchange") {
                            e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                            e.items[0].dirtyFields[e.field] = true;
                        }
                    }
                });
                this.editDimensionGridOptions = {
                    dataSource: resources,
                    sortable: true,
                    pageable: true,
                    navigatable: true,
                    editable: "incell",
                    toolbar: ["create"],
                    filterable: {
                        mode: "row"
                    },
                    filterMenuInit: function (e) {
                        var firstValueDropDown = e.container.find("select:eq(0)").data("kendoDropDownList");
                        setTimeout(function () {
                            firstValueDropDown.wrapper.hide();
                        })
                    },
                    columns: dimensionSchema
                };

            });


            //this.dimensionService.getDimensionByName(name, this.getCubesModel.username, this.getCubesModel.password).then((response) => {
            //    this.selectedDimension.Name = name;
            //    this.selectedDimension.Elements = response;
            //    var schema = this.selectedDimension.Elements[0];
            //    var tableColumns = [];
            //    console.log(schema);
            //    var readonlyEditor = function (container, options) {
            //        this.closeCell();
            //    };

            //    angular.forEach(schema, function (value, key) {
            //        console.log(key);
            //        if (key == "ID") {
            //            this.push({
            //                field: key,
            //                title: key,
            //                width: "200px",
            //                editable: false,
            //                editor: readonlyEditor,
            //                filterable: {
            //                    cell: {
            //                        operator: "contains",
            //                        showOperators: false,
            //                        template: function (e) {
            //                            e.element.kendoAutoComplete({
            //                                serverFiltering: false,
            //                                valuePrimitive: true,
            //                                noDataTemplate: ''
            //                            });
            //                        }
            //                    }
            //                }
            //            });
            //        }
            //        else {
            //            this.push({
            //                field: key,
            //                title: key,
            //                width: "200px",
            //                template: "#=dirtyField(data,'" + key + "')# #:" + key + "#",
            //                filterable: {
            //                    cell: {
            //                        operator: "contains",
            //                        showOperators: false,
            //                        template: function (e) {
            //                            e.element.kendoAutoComplete({
            //                                serverFiltering: false,
            //                                valuePrimitive: true,
            //                                noDataTemplate: ''
            //                            });
            //                        }
            //                    }
            //                }
            //            });
            //        }
            //    }, tableColumns);
            //    //tableColumns.push({ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" });
            //    var resources = new kendo.data.DataSource({
            //        sort: ({ field: "ID", dir: "asc" }),
            //        change: function (e) {
            //            if (e.action == "itemchange") {
            //                e.items[0].dirtyFields = e.items[0].dirtyFields || {};
            //                e.items[0].dirtyFields[e.field] = true;
            //            }
            //        },
            //        transport: {
            //            //read: {
            //            //    url: crudServiceBaseUrl + "/Products",
            //            //    dataType: "jsonp"
            //            //},
            //            //update: this.updateDimension,
            //            //destroy: {
            //            //    url: crudServiceBaseUrl + "/Products/Destroy",
            //            //    dataType: "jsonp"
            //            //},
            //            //create: {
            //            //    url: crudServiceBaseUrl + "/Products/Create",
            //            //    dataType: "jsonp"
            //            //},
            //            parameterMap: function (options, operation) {
            //                if (operation !== "read" && options.models) {
            //                    return { models: kendo.stringify(options.models) };
            //                }
            //            }
            //        }
            //    });
            //    resources.data(this.selectedDimension.Elements);
            //    this.editDimensionGridOptions = {
            //        dataSource: resources,
            //        pageSize: 5,
            //        serverPaging: true,
            //        serverSorting: true,
            //        sortable: true,
            //        pageable: true,
            //        editable: "incell",
            //        filterable: {
            //            mode: "row"
            //        },
            //        toolbar: ["create"],
            //        filterMenuInit: function (e) {
            //            var firstValueDropDown = e.container.find("select:eq(0)").data("kendoDropDownList");
            //            setTimeout(function () {
            //                firstValueDropDown.wrapper.hide();
            //            })
            //        },
            //        columns: tableColumns
            //    };
            //});
        }

        refresh = () => {
            this.$state.transitionTo(this.$state.current, this.$stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        editDimension = () => {
            this.$state.go("editDimension", { dimension: this.$stateParams.dimension });
        }

        getDirtyElements = () => {
            var grid = angular.element(document.getElementById("dimensionGrid"));
            var gridData = grid.data().kendoGrid.dataSource.view();
            var dirty = [];
            for (var i = 0; i < gridData.length; i++) {
                if (gridData[i].dirtyFields) {
                    var df = [];
                    angular.forEach(gridData[i].dirtyFields, function (value, key) {
                        df.push(key);
                    });
                    var obj = {
                        ID: gridData[i].ID,
                        DataName: gridData[i].Name,
                    };
                    angular.forEach(df, function (value, key) {
                        obj[value] = gridData[i][value];
                    });
                    dirty.push(obj);
                }
            }
            return dirty;
        }

        saveChanged = () => {
            var elemsForSave = this.getDirtyElements();
            var data = [];
            for (var i = 0; i < elemsForSave.length; i++) {
                var fields = [];
                angular.forEach(elemsForSave[i], function (value, key) {
                    if (elemsForSave[i]["ID"] == 0) {
                        if (key != "ID" && key != "DataName" && key != "Name" && key != "dirtyFields") {
                            fields.push({
                                FieldName: key,
                                FieldValue: value
                            });
                        }
                    }
                    if (elemsForSave[i]["ID"] != 0) {
                        if (key != "ID" && key != "DataName" && key != "dirtyFields") {
                            fields.push({
                                FieldName: key,
                                FieldValue: value
                            });
                        }
                    }
                });
                var dimensionName = elemsForSave[i].DataName;
                if (elemsForSave[i].Name) {
                    for (var j = 0; j < this.selectedDimensionElements.length; j++) {
                        if (this.selectedDimensionElements[j].ID == elemsForSave[i].ID) {
                            dimensionName = this.selectedDimensionElements[j].Name;
                        }
                    }
                }
                data.push({
                    "DimensionName": dimensionName,
                    "Fields": fields
                })
            }
            console.log(data);
            this.dimensionService.saveDimensionChanges(this.selectedDimensionName, this.getCubesModel.username, this.getCubesModel.password, data).then((response) => {
                this.$state.reload();
            });
        }

        bindTabFunc = () => {
            var grid = angular.element(document.getElementById("dimensionGrid")).data().kendoGrid;
            grid.table.on('keydown', function (e) {
                if (e.keyCode === kendo.keys.TAB && $($(e.target).closest('.k-edit-cell'))[0]) {
                    e.preventDefault();
                    var currentNumberOfItems = grid.dataSource.view().length;
                    var row = $(e.target).closest('tr').index();
                    var col = grid.cellIndex($(e.target).closest('td'));

                    var dataItem = grid.dataItem($(e.target).closest('tr'));
                    var field = grid.columns[col].field;
                    var value = $(e.target).val();
                    dataItem.set(field, value);

                    if (row >= 0 && row < currentNumberOfItems && col >= 0 && col < grid.columns.length) {
                        var nextCellRow;
                        var nextCellCol = col === 0 ? 2 : 0;
                        if (e.shiftKey) {
                            nextCellRow = nextCellCol === 0 ? row : row - 1;
                        }
                        if (!e.shiftKey) {
                            nextCellRow = nextCellCol === 0 ? row + 1 : row;
                        }

                        if (nextCellRow >= currentNumberOfItems || nextCellRow < 0) {
                            return;
                        }

                        // wait for cell to close and Grid to rebind when changes have been made
                        setTimeout(() => {
                            grid.editCell(grid.tbody.find("tr:eq(" + nextCellRow + ") td:eq(" + nextCellCol + ")"));
                        });
                    }
                }
            });
        }

        openExcelWindow = () => {
            console.log("openExcelWindow");
            this.excelDataWindow.center().open();
        }

        pasteExcelData = (dimensionName: string) => {
            this.excelDataWindow.close();
            this.isGridLoaded = false;
            var pasteData = [];
            var data = $('textarea[name=excelData]').val().trim();
            console.log(data);
            var rows = data.split("\n");

            for (var y in rows) {
                var cells = rows[y].split("\t");
                var fields = [];
                for (var i = 0; i < this.editDataModel.length; i++) {
                    fields.push({
                        FieldName: this.editDataModel[i],
                        FieldValue: cells[i]
                    });
                }
                pasteData.push({
                    "DimensionName": cells[0],
                    "Fields": fields
                })
            }
            console.log(pasteData);

            this.dimensionService.saveDimensionChanges(this.selectedDimensionName, this.getCubesModel.username, this.getCubesModel.password, pasteData).then((response) => {
                console.log(response);
                this.isGridLoaded = true;
                this.dataPasteSuccessWindow.center().open();
            });

        }
    }

    angular.module("app").controller("dimensionsController", DimensionsController);

    interface IDimensionStateParams extends angular.ui.IStateParamsService {
        dimension: string;
    }
}


//var dimensions = "<ROOT>";
//var excelData = $('textarea[name=createDimExcelData]').val();
//var pasteData = [];
//if (this.dimensionsForCubeCreate) {
//    var columnsNames = ["Name"];
//    for (var i = 0; i < this.dimensionsForCubeCreate.length; i++) {
//        dimensions = dimensions + "<dimension dim_name=\"" + this.dimensionsForCubeCreate[i].trim() + "\" dim_order= \"" + (i + 1) + "\" />";
//        if (excelData) {
//            columnsNames.push(this.dimensionsForCubeCreate[i].trim());
//        }
//    }
//    dimensions += "</ROOT>";
//    console.log(columnsNames);
//    if (excelData) {
//        var rows = excelData.split("\n");
//        for (var j = 0; j < rows.length; j++) {
//            var cells = rows[j].split("\t");
//            var fields = [];
//            fields.push({
//                FieldName: "Name",
//                FieldValue: cells[0]
//            });
//            for (var i = 0; i < columnsNames.length; i++) {
//                fields.push({
//                    FieldName: tableRow[i],
//                    FieldValue: cells[i]
//                });
//            }
//            pasteData.push({
//                "DimensionName": cells[0],
//                "Fields": fields
//            })
//        }
//        console.log(pasteData);
//    }

//}
//if (!this.dimensionsForCubeCreate && excelData) {
//    var rows = excelData.split("\n");
//    var tableRow = rows[0].split("\t");
//    for (var i = 1; i < tableRow.length; i++) {
//        dimensions = dimensions + "<dimension dim_name=\"" + tableRow[i].trim() + "\" dim_order= \"" + (i + 1) + "\" />";
//    }
//    dimensions += "</ROOT>";
//    for (var j = 1; j < rows.length; j++) {
//        var cells = rows[j].split("\t");
//        var fields = [];
//        fields.push({
//            FieldName: "Name",
//            FieldValue: cells[0]
//        });
//        for (var i = 1; i < tableRow.length; i++) {
//            fields.push({
//                FieldName: tableRow[i],
//                FieldValue: cells[i]
//            });
//        }
//        pasteData.push({
//            "DimensionName": cells[0],
//            "Fields": fields
//        })
//    }
//}

//console.log(dimensions);
