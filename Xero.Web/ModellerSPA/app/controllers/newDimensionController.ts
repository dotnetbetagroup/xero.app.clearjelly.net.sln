﻿//import * as Dimension from "../entities/dimension";
//import * as DimensionService from "../services/dimensionService";

module Modeller.Core.Controllers {
    export class NewDimensionController {
        static $inject = ["$state", "dimensionService", "uiGridConstants"];

        dimensions: Array<Modeller.Core.Domain.Dimension>;
        selectedDimension: Modeller.Core.Domain.Dimension;
        getCubesModel: IGetCubesViewModel = { username: "xlUser%40zealousys.com", password: "Ze%40lous2012", cube: "TL_summary" };
        selectedDimensionGridOptions: any;
        dimensionsGridOptions: any;
        columns: any;

        newDimensionsGridOptions: any;

        constructor(private $state: angular.ui.IStateService,
            private dimensionService: Modeller.Core.Services.IDimensionService,
            private uiGridConstants: any
        ) {
            //this.columns = [{ field: 'firstName' }, { field: 'lastName' }, { field: 'company' }, { field: 'employed' }];
            this.columns = [{ field: '' }, { field: '' }];
            //this.dimensionsGridOptions = {
            //    data:[
            //        {
            //            "firstName": "Cox",
            //            "lastName": "Carney",
            //            "company": "Enormo",
            //            "employed": true
            //        },
            //        {
            //            "firstName": "Lorraine",
            //            "lastName": "Wise",
            //            "company": "Comveyer",
            //            "employed": false
            //        },
            //        {
            //            "firstName": "Nancy",
            //            "lastName": "Waters",
            //            "company": "Fuelton",
            //            "employed": false
            //        }
            //    ],
            //    columnDefs: this.columns
            //}
            this.dimensionsGridOptions = {
                data: [
                    {
                        "": ""
                    },
                    {
                        "": ""
                    }
                ],
                columnDefs: this.columns
            }
        }

        add = () => {
            this.columns.push({ field: 'newColumn' });
        }
    }
    angular.module("app").controller("newDimensionController", NewDimensionController);
}