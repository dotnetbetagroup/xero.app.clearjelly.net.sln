﻿//import * as UiHelper from "../common/uiHelper";
//import * as Cube from "../entities/cube";
//import * as Dimension from "../entities/dimension";
//import * as CubeService from "../services/cubeService";
//import * as DimensionService from "../services/dimensionService";

module Modeller.Core.Controllers {
    export class MainController {
        static $inject = ["$scope", "$rootScope", "$state", "$stateParams", "$window", "cubeService", "uiService", "dimensionService"];

        allDimensions: Array<Modeller.Core.Domain.Dimension>;
        dimensionsForCubeCreate: any;
        dimensionsTreeData: any;
        cubesTreeData: any;
        selectDimensionsOptions: any;
        selectedDimension: Modeller.Core.Domain.Dimension;
        getCubesModel: IGetCubesViewModel = { username: "xlUser%40zealousys.com", password: "Ze%40lous2012", cube: "TL_summary" };
        createDimensionWindow: kendo.ui.Window;
        deleteDimensionWindow: kendo.ui.Window;
        createCubeWindow: kendo.ui.Window;
        deleteCubeWindow: kendo.ui.Window;
        createDimensionSuccessWindow: kendo.ui.Window;
        createCubeSuccessWindow: kendo.ui.Window;
        deleteDimensionSuccessWindow: kendo.ui.Window;
        deleteCubeSuccessWindow: kendo.ui.Window;
        dimensionForCreate: ICreateViewModel;
        cubeForCreate: ICreateViewModel;
        dimensionForDelete: string;
        cubeForDelete: string;

        constructor(
            private $scope,
            private $rootScope: ng.IScope,
            private $state: angular.ui.IStateService,
            private $stateParams: angular.ui.IStateParamsService,
            private $window,
            private cubeService: Modeller.Core.Services.ICubeService,
            private uiService: Modeller.Core.Services.UiService,
            private dimensionService: Modeller.Core.Services.IDimensionService
        ) {
            this.getPageData();
        }

        getPageData = () => {
            this.dimensionService.getAllDimensions(this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                this.allDimensions = response;
                this.dimensionsTreeData = new kendo.data.HierarchicalDataSource({
                    data: [
                        { text: "Dimensions", items: this.fillTreeItems(response) }
                    ]
                });
                var arr = [];
                for (var i = 0; i < this.allDimensions.length; i++) {
                    arr.push(this.allDimensions[i].Name);
                }
                this.selectDimensionsOptions = {
                    placeholder: "Select dimensions...",
                    valuePrimitive: true,
                    autoBind: false,
                    dataSource: arr
                };
            });
            this.cubeService.getAllCubes(this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                this.cubesTreeData = new kendo.data.HierarchicalDataSource({
                    data: [
                        { text: "Cubes", items: this.fillTreeItems(response) }
                    ]
                });
            });
        }

        fillTreeItems = (items: any) => {
            var result = [];
            for (var i = 0; i < items.length; i++) {
                var item = { "text": items[i].Name };
                result.push(item);
            }
            return result;
        }

        selectCube = (name: string) => {
            if (name == "Cubes") {
                this.$state.go("cubes");
            }
            if (name != "Cubes") {
                this.$state.go("cube", { cube: name });
            }
        }

        selectDimension = (name: string) => {
            if (name == "Dimensions") {
                this.$state.go("dimensions");
            }
            if (name != "Dimensions") {
                this.$state.go("dimension", { dimension: name });
            }
        }

        openNewDimensionWindow = () => {
            //this.$state.go("new_dimension");
            this.createDimensionWindow.center().open();
        }

        openDeleteDimensionWindow = (name: string) => {
            this.dimensionForDelete = name;
            this.deleteDimensionWindow.center().open();
        }

        openNewCubeWindow = () => {
            this.createCubeWindow.center().open();
        }

        openDeleteCubeWindow = (name: string) => {
            this.cubeForDelete = name;
            this.deleteCubeWindow.center().open();
        }

        reload = () => {
            this.$window.location.reload();
        }

        createDimension = () => {
            this.createDimensionWindow.close();
            //var elements = "<ROOT>";
            //var elemsArr = this.dimensionForCreate.Elements.split(",");
            //for (var i = 0; i < elemsArr.length; i++) {
            //    elements = elements + "<column column_name=\"" + elemsArr[i].trim() + "\" column_order= \"" + (i + 1) + "\" />";
            //}
            //elements += "</ROOT>";

            var elements = "<ROOT>";
            var excelData = $('textarea[name=createDimExcelData]').val();
            var pasteData = [];
            if (this.dimensionForCreate.Elements) {
                var elemsArr = this.dimensionForCreate.Elements.split(",");
                var columnsNames = ["Name"];
                for (var i = 0; i < elemsArr.length; i++) {
                    elements = elements + "<column column_name=\"" + elemsArr[i].trim() + "\" column_order= \"" + (i + 1) + "\" />";
                    if (excelData != "") {
                        columnsNames.push(elemsArr[i].trim());
                    }
                }
                elements += "</ROOT>";
                console.log(columnsNames);
                if (excelData != "") {
                    var rows = excelData.split("\n");
                    for (var j = 0; j < rows.length; j++) {
                        var cells = rows[j].split("\t");
                        console.log(cells);
                        var fields = [];
                        fields.push({
                            FieldName: "Name",
                            FieldValue: cells[0]
                        });
                        for (var i = 1; i < columnsNames.length; i++) {
                            fields.push({
                                FieldName: columnsNames[i],
                                FieldValue: cells[i]
                            });
                        }
                        pasteData.push({
                            "DimensionName": cells[0],
                            "Fields": fields
                        })
                    }
                }
            }
            if (!this.dimensionForCreate.Elements && excelData) {
                var rows = excelData.split("\n");
                var tableRow = rows[0].split("\t");
                for (var i = 1; i < tableRow.length; i++) {
                    elements = elements + "<column column_name=\"" + tableRow[i].trim() + "\" column_order= \"" + i + "\" />";
                }
                elements += "</ROOT>";
                for (var j = 1; j < rows.length; j++) {
                    var cells = rows[j].split("\t");
                    var fields = [];
                    fields.push({
                        FieldName: "Name",
                        FieldValue: cells[0]
                    });
                    for (var i = 1; i < tableRow.length; i++) {
                        fields.push({
                            FieldName: tableRow[i],
                            FieldValue: cells[i]
                        });
                    }
                    pasteData.push({
                        "DimensionName": cells[0],
                        "Fields": fields
                    })
                }
            }
            this.dimensionService.create(this.dimensionForCreate.Name, this.getCubesModel.username, this.getCubesModel.password, 0, elements).then((response) => {
                if (response == 201) {
                    this.dimensionService.saveDimensionChanges(this.dimensionForCreate.Name, this.getCubesModel.username, this.getCubesModel.password, pasteData).then((response) => {
                        console.log(response);
                        this.createDimensionSuccessWindow.center().open();
                    });
                }
            });
        }

        deleteDimension = () => {
            this.deleteDimensionWindow.close();
            this.dimensionService.delete(this.dimensionForDelete, this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                this.deleteDimensionSuccessWindow.center().open();
            });
        }

        createCube = () => {
            this.createCubeWindow.close();
            var dimensions = "<ROOT>";
            for (var i = 0; i < this.dimensionsForCubeCreate.length; i++) {
                dimensions = dimensions + "<dimension dim_name=\"" + this.dimensionsForCubeCreate[i].trim() + "\" dim_order= \"" + (i + 1) + "\" />";
            }
            dimensions += "</ROOT>";
            this.cubeService.create(this.cubeForCreate.Name, this.getCubesModel.username, this.getCubesModel.password, dimensions).then((response) => {
                if (response == 201) {
                    this.createCubeSuccessWindow.center().open();
                }
            });
        }

        deleteCube = () => {
            this.deleteCubeWindow.close();
            this.cubeService.delete(this.cubeForDelete, this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                this.deleteCubeSuccessWindow.center().open();
            });
        }
    }
    angular.module("app").controller("mainController", MainController);

    interface ICreateViewModel {
        Name: string;
        Elements: string;
    }
}


//<packages>
//  <package id="AjaxControlToolkit" version="16.1.1.0" targetFramework="net451" />
//  <package id="Antlr" version="3.5.0.2" targetFramework="net45" />
//  <package id="AspNet.ScriptManager.bootstrap" version="3.3.4" targetFramework="net45" />
//  <package id="AspNet.ScriptManager.jQuery" version="2.1.4" targetFramework="net45" />
//  <package id="bootstrap" version="3.3.4" targetFramework="net45" />
//  <package id="CodeAssassin.ConfigTransform" version="1.2" targetFramework="net451" />
//  <package id="Common.Logging" version="3.3.1" targetFramework="net451" />
//  <package id="Common.Logging.Core" version="3.3.1" targetFramework="net451" />
//  <package id="EntityFramework" version="6.1.3" targetFramework="net45" />
//  <package id="Hyak.Common" version="1.0.2" targetFramework="net451" />
//  <package id="jQuery" version="2.1.4" targetFramework="net45" />
//  <package id="jQuery.UI.Combined" version="1.12.1" targetFramework="net451" />
//  <package id="Microsoft.AspNet.FriendlyUrls.Core" version="1.0.2" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Identity.Core" version="2.2.1" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Identity.EntityFramework" version="2.2.1" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Identity.Owin" version="2.2.1" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Providers.Core" version="2.0.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Razor" version="3.2.2" targetFramework="net45" />
//  <package id="Microsoft.AspNet.ScriptManager.MSAjax" version="5.0.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.ScriptManager.WebForms" version="5.0.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.SignalR" version="2.2.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.SignalR.Core" version="2.2.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.SignalR.JS" version="2.2.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.SignalR.Owin" version="1.2.2" targetFramework="net45" />
//  <package id="Microsoft.AspNet.SignalR.SystemWeb" version="2.2.0" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Web.Optimization" version="1.1.3" targetFramework="net45" />
//  <package id="Microsoft.AspNet.Web.Optimization.WebForms" version="1.1.3" targetFramework="net45" />
//  <package id="Microsoft.AspNet.WebApi.Client" version="5.2.3" targetFramework="net451" />
//  <package id="Microsoft.AspNet.WebApi.Core" version="5.2.2" targetFramework="net45" />
//  <package id="Microsoft.AspNet.WebPages" version="3.2.2" targetFramework="net45" />
//  <package id="Microsoft.Azure.Common" version="2.1.0" targetFramework="net451" />
//  <package id="Microsoft.Azure.Common.Authentication" version="1.7.0-preview" targetFramework="net451" />
//  <package id="Microsoft.Azure.Common.Dependencies" version="1.0.0" targetFramework="net451" />
//  <package id="Microsoft.Azure.Management.ResourceManager" version="1.0.0-preview" targetFramework="net451" />
//  <package id="Microsoft.Azure.Management.Resources" version="2.20.1-preview" targetFramework="net451" />
//  <package id="Microsoft.Azure.Management.Sql" version="0.52.0-prerelease" targetFramework="net451" />
//  <package id="Microsoft.Bcl" version="1.1.9" targetFramework="net451" />
//  <package id="Microsoft.Bcl.Async" version="1.0.168" targetFramework="net451" />
//  <package id="Microsoft.Bcl.Build" version="1.0.14" targetFramework="net451" />
//  <package id="Microsoft.IdentityModel.Clients.ActiveDirectory" version="2.18.206251556" targetFramework="net451" />
//  <package id="Microsoft.Net.Http" version="2.2.22" targetFramework="net451" />
//  <package id="Microsoft.Owin" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Host.SystemWeb" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security.Cookies" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security.Facebook" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security.Google" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security.MicrosoftAccount" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security.OAuth" version="3.0.1" targetFramework="net45" />
//  <package id="Microsoft.Owin.Security.Twitter" version="3.0.1" targetFramework="net45" />  
//  <package id="Microsoft.Rest.ClientRuntime" version="2.1.0" targetFramework="net451" />
//  <package id="Microsoft.Rest.ClientRuntime.Azure" version="3.1.0" targetFramework="net451" />
//  <package id="Microsoft.Rest.ClientRuntime.Azure.Authentication" version="2.0.1-preview" targetFramework="net451" />
//  <package id="Microsoft.TeamFoundationServer.Client" version="14.83.0" targetFramework="net45" />
//  <package id="Microsoft.VisualStudio.Services.Client" version="14.83.0" targetFramework="net45" />
//  <package id="Microsoft.Web.Infrastructure" version="1.0.0.0" targetFramework="net45" />
//  <package id="Microsoft.WindowsAzure.Common" version="1.3.0" targetFramework="net451" />
//  <package id="Microsoft.WindowsAzure.Common.Dependencies" version="1.1.0" targetFramework="net451" />
//  <package id="Microsoft.WindowsAzure.Management" version="2.0.0" targetFramework="net451" />
//  <package id="Microsoft.WindowsAzure.Management.Monitoring" version="1.0.0" targetFramework="net451" />
//  <package id="Microsoft.WindowsAzure.Management.Scheduler" version="3.0.0" targetFramework="net451" />
//  <package id="Microsoft.WindowsAzure.Management.Storage" version="3.0.0" targetFramework="net451" />
//  <package id="Modernizr" version="2.8.3" targetFramework="net45" />
//  <package id="Newtonsoft.Json" version="7.0.1" targetFramework="net45" />
//  <package id="NLog" version="4.0.1" targetFramework="net45" />
//  <package id="Owin" version="1.0" targetFramework="net45" />
//  <package id="Respond" version="1.4.2" targetFramework="net45" />
//  <package id="SlowCheetah" version="2.5.48" targetFramework="net451" />
//  <package id="WebGrease" version="1.6.0" targetFramework="net45" />
//  <package id="Xero.API.SDK.Minimal" version="2.1.9.0" targetFramework="net451" />
//  <package id="XeroAPI.Net" version="1.1.0.33" targetFramework="net45" />
//</packages>