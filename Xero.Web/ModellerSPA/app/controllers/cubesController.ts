﻿//import * as Cube from "../entities/cube";
//import * as CubeService from "../services/cubeService";

module Modeller.Core.Controllers {
    export class CubesController {
        static $inject = ["$state", "$stateParams", "cubeService"];
        cubes: Array<Modeller.Core.Domain.Cube>;
        selectedCube: Modeller.Core.Domain.Cube;
        getCubesModel: IGetCubesViewModel = { username: "xlUser%40zealousys.com", password: "Ze%40lous2012", cube: "TL_summary" };
        cubesGridOptions: any;
        cubeDimensionsGridOptions: any;

        constructor(private $state: angular.ui.IStateService,
            private $stateParams: ICubeStateParams,
            private cubeService: Modeller.Core.Services.ICubeService) {
            if (!$stateParams.cube) {
                this.getAllCubes();
            }
            if ($stateParams.cube) {
                this.getCubeByName($stateParams.cube);
            }
        }

        getCubeByName = (name: string) => {
            this.cubeService.getCube(this.getCubesModel.username, this.getCubesModel.password, name).then((response) => {
                this.selectedCube = response[0];
                var schema = this.selectedCube.Dimensions[0];
                var tableColumns = [];
                angular.forEach(schema, function (value, key) {
                    this.push({
                        field: key,
                        title: key,
                        width: "200px"
                    });
                }, tableColumns);

                var resources = new kendo.data.DataSource({
                    sort: ({ field: "ID", dir: "asc" })
                });
                resources.data(this.selectedCube.Dimensions);
                this.cubeDimensionsGridOptions = {
                    dataSource: resources,
                    pageSize: 5,
                    serverPaging: true,
                    serverSorting: true,
                    sortable: true,
                    pageable: true,
                    columns: tableColumns
                };
            });
        }

        getAllCubes = () => {
            this.cubeService.getAllCubes(this.getCubesModel.username, this.getCubesModel.password).then((response) => {
                this.cubes = response;
                //for (var i = 0; i < this.cubes.length; i++) {
                //    var arr = this.cubes[i].Dimensions;
                //    var str = arr.length + " (";
                //    for (var j = 0; j < arr.length; j++) {
                //        if (j + 1 == arr.length) {
                //            str = str + arr[j].Name + ")";
                //        }
                //        else {
                //            str = str + arr[j].Name + ", "
                //        }
                //    }
                //}
                var schema = this.cubes[0];
                var tableColumns = [];
                angular.forEach(schema, function (value, key) {
                    this.push({
                        field: key,
                        title: key,
                        width: "200px"
                    });
                }, tableColumns);

                var resources = new kendo.data.DataSource({
                    sort: ({ field: "ID", dir: "asc" })
                });
                resources.data(this.cubes);
                this.cubesGridOptions = {
                    dataSource: resources,
                    pageSize: 5,
                    serverPaging: true,
                    serverSorting: true,
                    sortable: true,
                    pageable: true,
                    columns: tableColumns
                };
            })
        }

    }
    angular.module("app").controller("cubesController", CubesController);
    interface ICubeStateParams extends angular.ui.IStateParamsService {
        cube: string;
    }
}