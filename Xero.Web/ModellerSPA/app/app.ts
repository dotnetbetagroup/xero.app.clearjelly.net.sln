﻿/// <reference path="../typings/index.d.ts" />
//import * as angular from "angular";
//import * as router from "angular-ui-router";
//import IGrowlProvider = angular.growl.IGrowlProvider;
///* Controllers */
//import { MainController } from "./controllers/mainController";
//import { DimensionsController } from "./controllers/dimensionsController";
//import { CubesController } from "./controllers/cubesController";
//import { NewDimensionController } from "./controllers/newDimensionController";
///* Services */
//import { HttpHelperService } from "./services/common/httpHelperService"
//import { DimensionService } from "./services/dimensionService"
//import { CubeService } from "./services/cubeService"
//import { UiHelper } from "./common/uiHelper"

/*export */var app = angular.module("app", ["ui.router", "kendo.directives", "ngAnimate", "ngTouch", "ui.grid"]);

//angular.module("app")
//    /* Controllers */
//    .controller("mainController", MainController)
//    .controller("dimensionsController", DimensionsController)
//    .controller("cubesController", CubesController)
//    .controller("newDimensionController", NewDimensionController)
//    /* Services */
//    .service("httpHelperService", HttpHelperService)
//    .service("cubeService", CubeService)
//    .service("dimensionService", DimensionService)
//    .service("uiHelper", UiHelper);;

app.config(
    ($stateProvider: ng.ui.IStateProvider,
        $urlRouterProvider: ng.ui.IUrlRouterProvider
        ) => {

        $urlRouterProvider.otherwise("dimensions");
        $stateProvider
            .state("dimensions",
            {
                url: "/dimensions",
                templateUrl: "app/views/dimensions.html",
                controller: "dimensionsController",
                controllerAs: "vm"
            })
            .state("dimension",
            {
                url: "/dimension/:dimension",
                templateUrl: "app/views/dimension.html",
                controller: "dimensionsController",
                controllerAs: "vm"
            })
            .state("editDimension",
            {
                url: "/editDimension/:dimension",
                templateUrl: "app/views/editDimension.html",
                controller: "dimensionsController",
                controllerAs: "vm"
            })
            .state("new_dimension",
            {
                url: "/newDimension",
                templateUrl: "app/views/newDimension.html",
                controller: "newDimensionController",
                controllerAs: "vm"
            })
            .state("cubes",
            {
                url: "/cubes",
                templateUrl: "app/views/cubes.html",
                controller: "cubesController",
                controllerAs: "vm"
            })
            .state("cube",
            {
                url: "/cube/:cube",
                templateUrl: "app/views/cube.html",
                controller: "cubesController",
                controllerAs: "vm"
            });
        
    });

app.run([
    () => {
        console.log("Start");
    }
]);

app.constant("constantService", {
    //apiUrl: "https://sqlvnexttestip.australiaeast.cloudapp.azure.com:9999"
    apiUrl: "https://dev.agilityplanning.com:9999"
});

module Modeller.Core { };
module Modeller.Core.Controllers { };
