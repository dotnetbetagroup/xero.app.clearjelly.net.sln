﻿//import * as Dimension from "../entities/dimension";
module Modeller.Core.Domain {
    export class Cube {
        ID: number;
        Name: string;
        Dimensions: Array<Dimension>;
        ReadOnlyTable: string;
        WriteBackSP: string;
        WriteBackTable: string;
    }
}