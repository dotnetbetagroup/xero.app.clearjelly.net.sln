﻿module Modeller.Core.Domain {
    export class Dimension {
        ID: number;
        Name: string;
        DataType: string;
        Elements: Array<any>;
    }
}