﻿//import * as Cube from "../entities/cube";

module Modeller.Core.Services {
    export interface ICubeService {
        create(name: string, username: string, password: string, dimensions: string): ng.IPromise<any>;
        delete(name: string, username: string, password: string): ng.IPromise<any>;
        getAllCubes(username: string, password: string): ng.IPromise<Array<Modeller.Core.Domain.Cube>>;
        getCube(username: string, password: string, name: string): ng.IPromise<Modeller.Core.Domain.Cube>;
    }

    export class CubeService implements ICubeService {
        static $inject = ["$q", "$http", "constantService", "httpHelperService"];
        constructor(private $q: ng.IQService,
            private $http: ng.IHttpService,
            private constantService,
            private httpHelperService
        ) {

        }

        create(name: string, username: string, password: string, dimensions: string): ng.IPromise<any> {
            var url = this.constantService.apiUrl + "/api/cube?username=" + username + "&password=" + password + "&name=" + name + "&dimensions=" + dimensions;
            var uri = encodeURI(url);
            return this.httpHelperService.http(() => { return this.$http.post(uri, {}); });
        }

        delete(name: string, username: string, password: string): ng.IPromise<any> {
            return this.httpHelperService.http(() => { return this.$http.delete(this.constantService.apiUrl + "/api/cube?username=" + username + "&password=" + password + "&name=" + name) });
        }

        getAllCubes(username: string, password: string): ng.IPromise<Array<Modeller.Core.Domain.Cube>> {
            return this.httpHelperService.http(() => { return this.$http.get(this.constantService.apiUrl + "/api/cube?username=" + username + "&password=" + password) });
        }

        getCube(username: string, password: string, name: string): ng.IPromise<Modeller.Core.Domain.Cube> {
            return this.httpHelperService.http(() => { return this.$http.get(this.constantService.apiUrl + "/api/cube?username=" + username + "&password=" + password + "&cube=" + name) });
        }
    }
    angular.module("app").service("cubeService", CubeService);
}