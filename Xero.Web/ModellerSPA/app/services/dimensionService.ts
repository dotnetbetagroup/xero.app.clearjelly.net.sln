﻿module Modeller.Core.Services {

    export interface IDimensionService {
        create(name: string, username: string, password: string, typeid: number, elements: string): ng.IPromise<any>;
        delete(name: string, username: string, password: string): ng.IPromise<any>;
        getDimensionByName(name: string, username: string, password: string): ng.IPromise<any>;
        getAllDimensions(username: string, password: string): ng.IPromise<any>;
        saveDimensionChanges(dimension: string, username: string, password: string, model: any): ng.IPromise<any>;
    }

    export class DimensionService implements IDimensionService {
        static $inject = ["$q", "$http", "constantService", "httpHelperService"];
        constructor(private $q: ng.IQService,
            private $http: ng.IHttpService,
            private constantService,
            private httpHelperService
        ) {

        }

        create(name: string, username: string, password: string, typeid: number, elements: string): ng.IPromise<any> {
            var url = this.constantService.apiUrl + "/api/dimension?username=" + username + "&password=" + password + "&name=" + name + "&typeid=" + typeid + "&elements=" + elements;
            var uri = encodeURI(url);
            console.log(url);
            console.log(uri);
            return this.httpHelperService.http(() => {
                return this.$http.post(uri, {});
                //return this.$http.post(this.constantService.apiUrl + "api/dimension?username=" + username + "&password=" + password +
                //    "&name=" + name + "&typeid=" + typeid + "&elements=" + elements, {});
                /*(this.constantService.apiUrl + "/api/dimension", { username: username, password: password, name: name, typeid: typeid, elements: elements } )*/
            });
        }

        delete(name: string, username: string, password: string): ng.IPromise<any> {
            return this.httpHelperService.http(() => { return this.$http.delete(this.constantService.apiUrl + "/api/dimension?username=" + username + "&password=" + password + "&name=" + name) });
        }

        getDimensionByName(name: string, username: string, password: string): ng.IPromise<any> {
            return this.httpHelperService.http(() => { return this.$http.get(this.constantService.apiUrl + "/api/dimension/getdata?dimension=" + name + "&username=" + username + "&password=" + password) });
        }

        getAllDimensions(username: string, password: string): ng.IPromise<any> {
            return this.httpHelperService.http(() => { return this.$http.get(this.constantService.apiUrl + "/api/dimension?username=" + username + "&password=" + password) });
        }

        saveDimensionChanges(dimension: string, username: string, password: string, model: any): ng.IPromise<any> {
            return this.httpHelperService.http(() => { return this.$http.post(this.constantService.apiUrl + "/api/dimension/postData?dimension=" + dimension + "&username=" + username + "&password=" + password, model) });
        }
    }

    angular.module("app").service("dimensionService", DimensionService);
}