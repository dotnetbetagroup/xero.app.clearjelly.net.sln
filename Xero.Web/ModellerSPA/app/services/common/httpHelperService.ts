﻿//import GrowlService = angular.growl.IGrowlService;

module Modeller.Core.Services {
    export class HttpHelperService {
        constructor(
            private $q: ng.IQService,
            //private growl: GrowlService,
            private $rootScope: IRootScope) {
        }

        static $inject = ["$q", /*"growl",*/ "$rootScope"];


        http(delegate, shouldHandleOperationResult = false) {
            this.$rootScope.isAjax = true;
            return this.runDelegate(delegate, shouldHandleOperationResult);
        }

        runDelegate(delegate, shouldHandleOperationResult = false): void {
            return delegate()
                .then((result) => {
                    if (shouldHandleOperationResult) {
                        //this.growl.success("Done");
                    }
                    this.$rootScope.isAjax = false;
                    if (result.status == 201) {
                        return 201;
                    }
                    return result.data;

                },
                (result) => {
                    console.log(result.status);
                    if (result.status == 500) {
                        return this.$q.reject();
                    }


                    this.$rootScope.isAjax = false;
                    return this.$q.reject(result);
                });
        }
    }
    angular.module("app").service("httpHelperService", HttpHelperService);
}