﻿//import * as core from "../app";
//import app = core.app;
//import * as angular from "angular";
//import * as router from "angular-ui-router";

module Modeller.Core.Services {
    export class UiService {
        constructor(
        ) {

        }

        toggleSidePanel = (event) => {
            var sidebar = angular.element(document.getElementById("sidebar"));
            var view = angular.element(document.getElementById("view"));
            var icon = angular.element(document.getElementById("toggleSidebarIcon"));
            console.log(sidebar);
            console.log(view);
            console.log(icon);
            var css = sidebar.css("left");
            console.log(css);
            if (css == "0px" || css == "") {
                console.log("open");
                sidebar.css("left", "-300px");
                //view.css({ "margin-left": "", "margin": "0 auto" });
                view.css({ "width": "100%" });
                icon.removeClass("fa-chevron-circle-left");
                icon.addClass("fa-chevron-circle-right");
            }
            if (css == "-300px") {
                console.log("closed");
                sidebar.css("left", "0");
                //view.css({ "margin-left": "auto", "margin": "" });
                view.css({ "width": "calc(100% - 300px)" });
                icon.removeClass("fa-chevron-circle-right");
                icon.addClass("fa-chevron-circle-left");
            }
        }
    }
    angular.module("app").service("uiService", UiService);
}