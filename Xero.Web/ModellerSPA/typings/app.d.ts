﻿
interface IRootScope extends ng.IScope {
    isAjax: boolean;
    refreshTokenAction: any;
    previousState: any;
    previousStateParams: any;
}

interface IAuthViewModel {
    username: string;
    password: string;
}

interface IDeleteViewModel extends IAuthViewModel {
    name: string;
}

interface ICubeCreateViewModel extends IAuthViewModel {
    name: string;
    dimensions: string;
}

interface IDimensionCreateViewModel extends IAuthViewModel {
    name: string;
    typeId: number;
    elements: string;
}

interface IGetCubesViewModel extends IAuthViewModel {
    cube: string;
}

//interface ISocialnetworkLogin {
//    loginProvider: string;
//    providerDisplayName: string;
//}

//interface IProfileFullfilment {
//    percentage: number;
//    tip: string;
//}

//interface IRegisterViewModel {
//    email: string;
//    password: string;
//    confirmPassword: string;
//    firstName: string;
//    lastName: string;
//}

//interface IProfileViewModel {
//    id: string;
//    firstName: string;
//    lastName: string;
//    email: string;
//    phone: string;
//    gender: number;
//    unitsOfMeasure: number;
//    bikertag: string;
//    birthDate: Date;
//    countryId: number;
//    country: string;
//    cityId: number;
//    city: string;
//    weight: number;
//    height: number;
//    district: string;
//    street: string;
//    biography: string;
//}

//interface IDeleteProfileViewModel {
//    reasonId: string;
//    comment: string;
//}

//interface IBikeViewModel {
//    id: string;
//    brand: string;
//    model: string;
//    year: string;
//    isDefault: boolean;
//    notes: string;
//    addedOn: Date;
//    totalDistance: number;
//}

//interface IBikePhotoViewModel {
//    bikeId: string;
//    url: string;
//    isMain: boolean;
//}

//interface IBikeComponentViewModel {
//    componentId: string;
//    bikeId: string;
//    componentType: number;
//    brand: string;
//    model: string;
//    year: string;
//    addedOn: Date;
//    removedOn: Date;
//    totalDistance: number;
//}

//interface IUnitOfMeasureViewModel {
//    id: string;
//    userProfileId: string;
//    distanceUnitId: string;
//    weightUnitId: string;
//    temperatureUnitId: string;
//    firstDayOfWeek: number;
//}

//interface IBioViewModel {
//    id: string;
//    applicationUserId: string;
//    bio: string;
//}

//interface IEmergencyContactViewModel {
//    contactId: string;
//    firstName: string;
//    lastName: string;
//    email: string;
//    phone: string;
//    contactByEmail: Boolean;
//    contactBySms: Boolean;
//}

//interface IBaseEntity {
//    id: string;
//}

//interface INotificationCategory extends IBaseEntity {
//    name: string;
//    order: number;
//}

//interface INotification extends IBaseEntity {
//    name: string;
//    notificationType: NotificationType;
//    order: number;
//    notificationCategory: INotificationCategory;
//}

//interface IRidingStyle {
//    ridingStyleId: string;
//    ridingStyle: string;
//    checked: boolean;
//}

//declare const enum NotificationType {
//    Email = 0,
//    Push = 1
//}

//interface IForgotPasswordViewModel {
//    email: string;
//    token: string;
//    password: string;
//    confirmPassword: string;
//}

//interface IProfilePhotoViewModel {
//    id: string;
//    vkProfileId: string;
//    facebookProfileId: string;
//    googleProfileId: string;
//    twitterProfileId: string;
//    avatarType: number;
//    photoUrl: string;
//}

//declare const enum AwardType {
//    Achievement = 0,
//    Badge = 1,
//    Cup = 2
//}

////Search

//interface ISearchCreteria {
//    id: string;
//    property: string;
//    name: string;
//    controlName: string;
//}

//interface ISearchNode {
//    name: string;
//    property: string;
//    value: string;
//}

//interface ILocationViewModel {
//    userProfileId: string;
//    countryId: number;
//    country: string;
//    cityId: number;
//    city: string;
//    district: string;
//    street: string;
//}

//interface IUserInfoViewModel {
//    firstName: string;
//    lastName: string;
//    photoUrl: string;
//    bikertag: string;
//}

//interface ITrackSimplifiedViewModel {
//    id: string;
//    title: string;
//    description: string;
//    totalDistance: number;
//    duration: number;
//    routeJSON: string;
//    isPublic: boolean;
//    trackTypesArray: number[];
//    trackTypes: number;
//}

//interface ITrackViewModel {
//    id: string;
//    applicationUserId: string;
//    title: string;
//    description: string;
//    startLocation: string;
//    endLocation: string;
//    routeJSON: string;
//    totalDistance: number;
//    duration: number;
//    trackTypes: number;
//    owner: Array<IUserInfoViewModel>;
//}

//interface IPlaceViewModel {
//    id: string;
//    applicationUserId: string;
//    trackId: string;
//    placeTypes: number;
//    placeTypesArray: number[];
//    title: string;
//    isPublic: boolean;
//    description: string;
//    location: string;
//    checkIns: number;
//    checkedInPeople: number;
//}

//interface IPointOfInterestPhoto {
//    id: string;
//    pointOfInterestId: string;
//    url: string;
//    isDefault: boolean;
//}

//interface ITrackPhoto {
//    id: string;
//    trackId: string;
//    url: string;
//    isDefault: boolean;
//}

//interface IBaseSearchViewModel {
//    q: string;
//    skip: number;
//    take: number;
//}

//interface IUserSearchViewModel extends IBaseSearchViewModel {
//    gender: number;
//    rs: number;
//    country: number;
//    city: number;
//    minAge: number;
//    maxAge: number;
//    eventId: string;
//    groupId: string;
//}

//interface IMapObjectSearchViewModel extends IBaseSearchViewModel {
//    latitude: number;
//    longitude: number;
//    radius: number;
//}

//interface ITrackSearchViewModel extends IMapObjectSearchViewModel {
//    rs: number;
//    minDistance: number;
//    maxDistance: number;
//    hasPlacesOfInterest: boolean;
//    hasPhotos: boolean;
//    hasReviews: boolean;
//}

//interface IPlaceSearchViewModel extends IMapObjectSearchViewModel {
//    placeTypes: number;
//    hasPhotos: boolean;
//    hasReviews: boolean;
//    includedInTracks: boolean;
//}

//interface IEventType {
//    id: string;
//    name: string;
//    order: number;
//    color: string;
//}

//interface IEventViewModel {
//    id: string;
//    eventType: number;
//    title: string;
//    description: string;
//    coverPhotoUrl: string;
//    photoUrl: string;
//    trackId: string;
//    recurrence: string;
//    startTimezone: string;
//    endTimezone: string;
//    recurrenceRule: string;
//    recurrenceId: string;
//    recurrenceException: string;
//    isAllDay: boolean;
//    start: Date;
//    end: Date;
//    userId: string;
//    isEditable: boolean;
//    isPublic: boolean;
//    participationStatus: number;
//    accessibilityType: number;
//    country: number;
//    countryName: string;
//    city: number;
//    cityName: string;
//    address: string;
//}

//interface IEventSearchViewModel extends IBaseSearchViewModel {
//    eventType: number;
//    startDate: Date;
//    endDate: Date;
//    recurrenceRule: string;
//    isAllDay: boolean;
//    pastEvents: boolean;
//    hasPhotos: boolean;
//    hasTracks: boolean;
//    isPublic: boolean;
//    country: number;
//    city: number;
//}

//interface IGroupViewModel {
//    id: string;
//    groupType: number;
//    name: string;
//    description: string;
//    headerImage: string;
//    avatarImage: string;
//    country: number;
//    countryName: string;
//    city: number;
//    cityName: string;
//    userId: string;
//    accessibilityType: number;
//    isEditable: boolean;
//    isOwned: boolean;
//    membershipStatus: number;
//}

//interface IChallengeViewModel {
//    id: string;
//    challengeType: number;
//    name: string;
//    description: string;
//    country: number;
//    countryName: string;
//    city: number;
//    cityName: string;
//    userId: string;
//    accessibilityType: number;
//    isEditable: boolean;
//    isOwned: boolean;
//    membershipStatus: number;
//    winner: any;
//    dateCompleted: Date;
//    field_1: string;
//    field_2: string;
//    field_3: string;
//    field_4: string;
//    field_5: string;
//}

//interface IGroupSearchViewModel extends IBaseSearchViewModel {
//    groupType: number;
//    country: number;
//    city: number;
//}

//interface IWidget {
//    id: string;
//    name: string;
//    control: string;
//    x: number;
//    y: number;
//}

//interface IPictureViewModel {
//    parentObjectId: string;
//    contents: string;
//}

//interface IFeedViewModel {
//    id: string;
//    feedTypeName: string;
//    parentObjectId: string;
//    objectId: string;
//}

//interface IBaseFeedDetailsViewModel {
//    id: string;
//    objectId: string;
//    objectName: string;
//    activityType: string;
//    activityDate: Date;
//}

//interface IUserActivityFeedViewModel extends IBaseFeedDetailsViewModel {
//    routeJson: string;
//    distance: number;
//    averageSpeed: number;
//    travelTime: number;
//    photos: Array<string>;
//}

//interface ITrackFeedViewModel extends IBaseFeedDetailsViewModel {
//    routeJson: string;
//}

//interface IDiscussionFeedViewModel extends IBaseFeedDetailsViewModel {
//    discussionId: string;
//    discussionTitle: string;
//    discussionDescription: string;
//}

//interface IPhotosFeedViewModel extends IBaseFeedDetailsViewModel {
//    photoUrls: Array<string>;
//    photosCount: number;
//}

//interface IUserFeedViewModel extends IBaseFeedDetailsViewModel {
//    photoUrl: Array<string>;
//}

//interface IChallengeMemberProgress {
//    userId: string;
//    userName: string;
//    percentCompleted: number;
//}