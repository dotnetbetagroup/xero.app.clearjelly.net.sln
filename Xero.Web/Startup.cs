﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Xero.Web.Startup))]
namespace Xero.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
