﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modeller.aspx.cs" Inherits="Xero.Web.Modeller" %>

<html <%--xmlns="http://www.w3.org/1999/xhtml"--%>>
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0"/>
    <base href="ModellerSPA/">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/var.css" rel="stylesheet" />
    <link href="css/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/kendo/kendo.common-material.css" rel="stylesheet" />
    <link href="css/kendo/kendo.material.css" rel="stylesheet" />
    <link href="css/kendo/kendo.material.mobile.css" rel="stylesheet" />
    <link href="css/kendo/kendo.rtl.css" rel="stylesheet" />
    <link href="css/angular-ui-grid/ui-grid.min.css" rel="stylesheet" />
    <script src="jquery/dist/jquery.js"></script>
    <script src="libs/angular/angular.js"></script>
    <script src="libs/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="libs/kendo/kendo.all.min.js"></script>
    <script src="libs/angular-animate/angular-animate.js"></script>
    <script src="libs/angular-touch/angular-touch.min.js"></script>
    <script src="libs/angular-ui-grid/ui-grid.min.js"></script>
    <script src="app/app.js"></script>
    <script src="app/controllers/newDimensionController.js"></script>
    <script src="app/controllers/mainController.js"></script>
    <script src="app/controllers/dimensionsController.js"></script>
    <script src="app/controllers/cubesController.js"></script>
    <script src="app/services/common/httpHelperService.js"></script>
    <script src="app/services/common/uiService.js"></script>
    <script src="app/services/cubeService.js"></script>
    <script src="app/services/dimensionService.js"></script>
</head>
<body ng-app="app" ng-controller="mainController as vm" ng-cloak>

    <nav class="navbar navbar-default navbar-static-top header-navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="#support">Support</a></li>
                    <li><a href="#subscription">Subscription</a></li>
                    <li><a href="#browser">Ad Hoc Browser</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Martin Kratky <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Log out</a></li>

                        </ul>
                    </li>
                </ul>

            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <div class="main-part">
        <div id="sidebar" class="sidebar-page k-content">

            <nav class="navbar navbar-default">
                <div class="form-group clearfix">
                    <label for="sel1">Connection:</label>
                    <select class="form-control pull-left" id="sel1">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                    <div class="btn-group pull-right">
                        <a href="#"><i class="fa fa-power-off fa-lg" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-database fa-lg" aria-hidden="true"></i></a>
                    </div>

                </div>
                <button type="button" class="navbar-toggle opensidebar pull-right" ng-click="vm.uiService.toggleSidePanel($event)">
                    <span class="sr-only">Toggle navigation</span>
                    <i id="toggleSidebarIcon" class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                </button>
                <div>

                    <div id="navbar" class="navbar-collapse">

                        <div kendo-tree-view="tree"
                             k-data-source="vm.dimensionsTreeData"
                             k-on-change="selectedItem = dataItem">
                            <span k-template>
                                <i class="fa fa-arrows-alt" aria-hidden="true" ng-show="dataItem.text == 'Dimensions'"></i>
                                <i class="fa fa-expand" aria-hidden="true" ng-show="dataItem.text != 'Dimensions'"></i>
                                <span ng-click="vm.selectDimension(dataItem.text);">{{dataItem.text}}</span>

                                <button class='k-button pull-right add-dimension' kendo-tooltip k-content="'Create dimension'" style="padding: 1px 3px;" ng-click="vm.openNewDimensionWindow()" ng-show="dataItem.text == 'Dimensions'">

                                    <i class="fa fa-plus" aria-hidden="true"></i>

                                </button>

                                <button class='k-button pull-right remove-dimension' kendo-tooltip k-content="'Remove dimension'" style="padding: 1px 3px;" ng-click="vm.openDeleteDimensionWindow(dataItem.text)" ng-show="dataItem.text != 'Dimensions'">

                                    <i class="fa fa-minus" aria-hidden="true"></i>

                                </button>
                            </span>

                        </div>
                        <div kendo-tree-view="tree"
                             k-data-source="vm.cubesTreeData"
                             k-on-change="selectedItem = dataItem">
                            <span k-template>
                                <i class="fa fa-cubes" aria-hidden="true" ng-show="dataItem.text == 'Cubes'"></i>
                                <i class="fa fa-cube" aria-hidden="true" ng-show="dataItem.text != 'Cubes'"></i>
                                <span ng-click="vm.selectCube(dataItem.text);">{{dataItem.text}}</span>
                                <button class='k-button pull-right add-cube' kendo-tooltip k-content="'Create cube'" style="padding: 1px 3px;" ng-click="vm.openNewCubeWindow()" ng-show="dataItem.text == 'Cubes'">

                                    <i class="fa fa-plus" aria-hidden="true"></i>

                                </button>
                                <button class='k-button pull-right remove-cube' kendo-tooltip k-content="'Remove cube'" style="padding: 1px 3px;" ng-click="vm.openDeleteCubeWindow(dataItem.text)" ng-show="dataItem.text != 'Cubes'">

                                    <i class="fa fa-minus" aria-hidden="true"></i>

                                </button>
                            </span>
                        </div>

                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </nav>
        </div>


        <div id="view" class="wrapper clearfix" ui-view></div>
    </div>
    <div kendo-window="vm.createDimensionWindow"
         k-title="'Create New Dimension'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div style="margin-bottom: 15px;">
            <div class="form-group">
                <label>Name: </label>
                <input type="text" class="form-control" ng-model="vm.dimensionForCreate.Name" />
            </div>

            <div class="form-group">
                <label>Elements: </label>
                <input type="text" class="form-control" ng-model="vm.dimensionForCreate.Elements" />
            </div>
            <div>*please put in one or more element's name in comma</div>
            <div>OR</div>
            <div>just paste data from Excel</div>
            <textarea name="createDimExcelData" style="width:360px; height: 200px;"></textarea>
        </div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.createDimension()" class="btn btn-default btn-add">Create</button>
            <button type="reset" ng-click="vm.createDimensionWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
        </div>
    </div>

    <div kendo-window="vm.createCubeWindow"
         k-title="'Create New Cube'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div style="margin-bottom: 15px;">
            <div class="form-group clearfix">
                <label class="col-form-label"> Name:</label>
                <input type="text" class="form-control" ng-model="vm.cubeForCreate.Name" />
            </div>
            <div class="form-group clearfix">
                <label>Dimensions:</label>
                <select kendo-multi-select k-options="vm.selectDimensionsOptions" k-ng-model="vm.dimensionsForCubeCreate" id="multiselect"></select>
            </div>
        </div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.createCube()" class="btn btn-default btn-add">Create</button>
            <button type="reset" ng-click="vm.createCubeWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
        </div>
    </div>

    <div kendo-window="vm.deleteDimensionWindow"
         k-title="'Delete Dimension?'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div>Name: {{vm.dimensionForDelete}}</div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.deleteDimension()" class="btn btn-default btn-delete">Delete</button>
            <button type="reset" ng-click="vm.deleteDimensionWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
        </div>
    </div>

    <div kendo-window="vm.deleteCubeWindow"
         k-title="'Delete Cube?'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div>Name: {{vm.cubeForDelete}}</div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.deleteCube()" class="btn btn-default btn-delete">Delete</button>
            <button type="reset" ng-click="vm.deleteCubeWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
        </div>
    </div>

    <div kendo-window="vm.createDimensionSuccessWindow"
         k-title="'Dimension was created successfully'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div>Name: {{vm.dimensionForCreate.Name}}</div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.reload()" class="btn btn-default btn-add">Ok</button>
        </div>
    </div>

    <div kendo-window="vm.deleteDimensionSuccessWindow"
         k-title="'Dimension was deleted successfully'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div>Name: {{vm.dimensionForDelete}}</div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.$state.go('dimensions');vm.reload()" class="btn btn-default btn-add">Ok</button>
        </div>
    </div>

    <div kendo-window="vm.createCubeSuccessWindow"
         k-title="'Cube was created successfully'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div>Name: {{vm.cubeForCreate.Name}}</div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.reload()" class="btn btn-default btn-add">Ok</button>
        </div>
    </div>

    <div kendo-window="vm.deleteCubeSuccessWindow"
         k-title="'Cube was deleted successfully'"
         k-width="400"
         k-modal="true"
         k-visible="false">
        <div>Name: {{vm.cubeForDelete}}</div>
        <div class="btn-holder" style="text-align: center;">
            <button type="submit" ng-click="vm.reload()" class="btn btn-default btn-add">Ok</button>
        </div>
    </div>

    <!--<script>
        $(document).ready(function () {
            // create MultiSelect from select HTML element
            var required1 = $("#multiselect").kendoMultiSelect().data().kendoMultiSelect;
            console.log(required1);

            required1.tagList.kendoSortable({
                hint: function (element) {
                    console.log(element);
                    return element.clone().addClass("hint");
                },
                placeholder: function (element) {
                    console.log(element);
                    return element.clone().addClass("placeholder").text("drop here");
                }
            });
        });
    </script>-->

</body>
</html>
