﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Repositories
{
    public class HomeRepository
    {
        private string _connection;
        Logger _logger;
        public HomeRepository(string connectionString)
        {
            _connection = connectionString;
            _logger = LogManager.GetCurrentClassLogger();
        }
        protected SqlConnection DatabaseConnection
        {
            get
            {
                var cn = new SqlConnection(_connection);
                return cn;
            }
        }

    }
}