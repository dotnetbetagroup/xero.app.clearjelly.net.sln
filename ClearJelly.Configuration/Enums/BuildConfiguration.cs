﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Configuration.Enums
{
    public enum BuildConfiguration
    {
        Local = 0,
        Development = 1,
        TestRelease = 2,
        Release = 3
    }

    
}
