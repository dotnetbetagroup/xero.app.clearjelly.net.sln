﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Configuration.Enums
{
    public enum ServiceTypes
    {
        ABC = 0,
        QuickBooks = 1,
        Xero = 2,
        Saasu = 3
    }
}
