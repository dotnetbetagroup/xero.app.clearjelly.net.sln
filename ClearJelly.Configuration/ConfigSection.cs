﻿using ClearJelly.Configuration.Enums;

namespace ClearJelly.Configuration
{
    public enum Developers
    {
        Developer1 = 0,
        Developer2 = 1,
        AzureDeveloper = 2
    }

    public static class ConfigSection
    {
        public static string ApiEndpoint { get; set; }
        public static string ApiDBList { get; set; }

        public static BuildConfiguration Configuration { get; set; }
        public static Developers DevelopersType { get; set; }

        #region ConnectionStrings
        public static string DefaultConnection { get; private set; }
        public static string Tenant { get; set; }
        public static string ClearJellyEntities { get; set; }
        public static string CompanyDBConnection { get; private set; }
        public static string AzureUserDatabase { get; private set; }
        #endregion
        #region Azure
        public static string IdaClientId { get; set; }
        public static string IdaPostLogoutRedirectUri { get; private set; }
        public static string IdaResourceUrl { get; private set; }
        public static string IdaTenantId { get; private set; }
        public static string ClientSecretAzure { get; private set; }
        public static string AuthString { get; private set; }
        public static string ServiceRootURL { get; private set; }
        public static string IdaTenant { get; private set; }
        public static string IdaAADInstance { get; private set; }
        public static string AbcCompanyDbConnection { get; private set; }
        public static string QBCompanyDbConnection { get; private set; }
        public static string XeroCompanyDbConnection { get; private set; }
        #endregion
        #region QuickBooks
        public static string RedirectUri { get; set; }
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }
        public static string DiscoveryUrl { get; set; }
        #endregion
        #region ABC
        public static string ABCClubNumber { get; set; }
        public static string ABCAppId { get; private set; }
        public static string ABCAppKey { get; private set; }
        public static string ABCRedirectURI { get; private set; }
        public static string ABCBaseURL { get; private set; }
        #endregion

        #region Xero
        public static string CallbackUrl { get; private set; }
        public static string SigningCertificatePath { get; private set; }
        public static string SigningCertificatePassword { get; private set; }
        public static string PartnerConsumerKey { get; private set; }
        public static string PartnerConsumerSecret { get; private set; }
        public static string BaseApiUrl { get; private set; }

        #endregion

        public static string AADInstance { get; private set; }

        #region Mail

        public static string SmtpServerName { get; set; }
        public static ushort SmtpServerPort { get; set; }
        public static string CredentialsMailLogin { get; set; }
        public static string CredentialsMailPassword { get; set; }

        public static string ActivateHostLink { get; set; }


        #endregion




        static ConfigSection()
        {
            DefaultConnection = @"server=13.70.93.206;user id=Dev_User;password=dev@2017;Initial Catalog=ClearJelly;MultipleActiveResultSets=true";
            ClearJellyEntities = @"server=13.70.93.206;user id=Dev_User;password=dev@2017;Initial Catalog=ClearJelly;MultipleActiveResultSets=true";
            CompanyDBConnection = @"server=13.70.93.206;user id=Dev_User;password=dev@2017;Connection Timeout=20;Integrated Security=False; MultipleActiveResultSets=true";
            AzureUserDatabase = "Server=tcp:democlearjellyserver.database.windows.net,1433;Persist Security Info=False;User ID=Nikin;Password=ze@lous2016;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";

            DefaultConnection = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;";
            CompanyDBConnection = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;";
            AzureUserDatabase = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;";
            ClearJellyEntities = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;";
            AbcCompanyDbConnection = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;Initial Catalog=ABC_Financial_";
            QBCompanyDbConnection = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;Initial Catalog=QuickBooks_";
            XeroCompanyDbConnection = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=28800;Initial Catalog=Xero_";

            AADInstance = "https://login.microsoftonline.com/{0}";
            Tenant = "AgilityPlanningTST.onmicrosoft.com";
            IdaTenantId = "1265953a-956a-474e-b818-e38b55da0a96";
            IdaResourceUrl = "https://graph.windows.net";
            IdaClientId = "6a2f18ea-89db-4c1c-9623-45cafcd4412c";
            IdaPostLogoutRedirectUri = "https://dev.agilityplanning.com:4363/";
            ClientSecretAzure = "oH320JBBOPASa+do1DP4NlElFKHwU4FCPzktL8IyySc=";
            AuthString = "https://login.windows.net/" + IdaTenantId;
            IdaTenant = "AgilityPlanningTST.onmicrosoft.com";
            IdaAADInstance = "https://login.microsoftonline.com/{0}";
            // The Azure AD Graph API for my directory is available at this URL.
            //ServiceRootURL = "https://graph.windows.net/" + IdaTenantId;
            AzureConfig();
            ABCConfiguration();
            XeroConfig();
            ServiceRootURL = "https://graph.windows.net/" + Tenant;

            SmtpServerName = "smtp.gmail.com";
            SmtpServerPort = 587;
            CredentialsMailLogin = "lolwner2@gmail.com";
            CredentialsMailPassword = "top.ptf05";
            ActivateHostLink = "https://dev.agilityplanning.com:4363/";

#if DEBUG
      Configuration = BuildConfiguration.Local;
      DevelopersType = Developers.AzureDeveloper;
#endif
#if !DEBUG
            Configuration = BuildConfiguration.TestRelease;
            DevelopersType = Developers.AzureDeveloper;
#endif

            if (Configuration == BuildConfiguration.Local)
            {
                LocalConfiguration();
            }
            if (Configuration == BuildConfiguration.TestRelease)
            {
                DevelopmentConfiguration();
            }
            if (Configuration == BuildConfiguration.Release)
            {
                ReleaseConfiguration();
            }
        }

        public static void LocalConfiguration()
        {
            ActivateHostLink = "http://localhost:30875/";
            ApiEndpoint = "https://dev.agilityplanning.com:9999";
            ApiDBList = "https://dev.agilityplanning.com:90";
            RedirectUri = "http://localhost:30875/Home.aspx";
            DiscoveryUrl = "https://developer.api.intuit.com/.well-known/openid_sandbox_configuration/";

            IdaPostLogoutRedirectUri = "http://localhost:30875/";
            CallbackUrl = "http://localhost:30875/Home.aspx";
            ABCRedirectURI = "http://localhost:30875/Home.aspx";
            SigningCertificatePath = @"C:\Users\Anuitex-3\Source\Repos\xero.app.clearjelly.net.sln\ClearJelly.Web\Certs\public_privatekey.pfx";
            //SigningCertificatePath = @"C:\Users\Anuitex-3\Source\Repos\xero.app.clearjelly.net.sln\ClearJelly.Web\CertsNew\public_privatekey.pfx";
            ClientId = "Q0VdWo8mea0jqOnSJE4g1s6R4tSvcSa5JgcbFhcDNkQpLMDIJ4";
            ClientSecret = "Z6c5FVzpYTu2JkeE6VnVFJBcJjenhTgRGhAN0f67";
            if (DevelopersType == Developers.Developer1)
            {
                DefaultConnection = @"Data Source=DESKTOP-LQN8ALT\SQLEXPRESS;Initial Catalog=ClearJelly;Integrated Security=False";
                ClearJellyEntities = @"Data Source=DESKTOP-LQN8ALT\SQLEXPRESS;Initial Catalog=ClearJelly;Integrated Security=False;MultipleActiveResultSets=true";
                CompanyDBConnection = @"Data Source=DESKTOP-LQN8ALT\SQLEXPRESS;Connection Timeout=20;Integrated Security=False";
                AzureUserDatabase = "Server=tcp:democlearjellyserver.database.windows.net,1433;Persist Security Info=False;User ID=Nikin;Password=ze@lous2016;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";
            }
            if (DevelopersType == Developers.Developer2)
            {
                DefaultConnection = "server=13.70.93.206;user id=Dev_User;password=dev@2017;Initial Catalog=ClearJelly;MultipleActiveResultSets=true";
                ClearJellyEntities = "server=13.70.93.206;user id=Dev_User;password=dev@2017;Initial Catalog=ClearJelly;MultipleActiveResultSets=true";
                CompanyDBConnection = "server=13.70.93.206;user id=Dev_User;password=dev@2017;Connection Timeout=20;Integrated Security=False; MultipleActiveResultSets=true";
                AzureUserDatabase = "Server=tcp:democlearjellyserver.database.windows.net,1433;Persist Security Info=False;User ID=Nikin;Password=ze@lous2016;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";
                AbcCompanyDbConnection = "server=13.70.93.206;user id=Dev_User;password=dev@2017;MultipleActiveResultSets=true;Initial Catalog=ABC_Financial_";
            }
            if (DevelopersType == Developers.AzureDeveloper)
            {
                IdaPostLogoutRedirectUri = "http://localhost:30875/";

            }

        }

        public static void DevelopmentConfiguration()
        {
            ActivateHostLink = "https://dev.agilityplanning.com:4363/";
            ApiEndpoint = "https://dev.agilityplanning.com:9999";
            ApiDBList = "https://dev.agilityplanning.com:90";
            RedirectUri = "https://dev.agilityplanning.com:4363/Home.aspx";
            DiscoveryUrl = "https://developer.api.intuit.com/.well-known/openid_sandbox_configuration/";
            ClientId = "Q0VdWo8mea0jqOnSJE4g1s6R4tSvcSa5JgcbFhcDNkQpLMDIJ4";
            ClientSecret = "Z6c5FVzpYTu2JkeE6VnVFJBcJjenhTgRGhAN0f67";
            IdaPostLogoutRedirectUri = "https://dev.agilityplanning.com:4363/";
            ABCRedirectURI = "https://dev.agilityplanning.com:4363/Home.aspx";

            if (DevelopersType == Developers.AzureDeveloper)
            {
                //DefaultConnection = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;";
                //CompanyDBConnection = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;";
                //AzureUserDatabase = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;";
                //ClearJellyEntities = "Server=tcp:aptest.database.windows.net,1433;Initial Catalog=ClearJelly;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;";
                //QBCompanyDbConnection = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;Initial Catalog=QuickBooks_";
                //AbcCompanyDbConnection = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;Initial Catalog=ABC_Financial_";
                //XeroCompanyDbConnection = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=120;Initial Catalog=Xero_";
            }
            if (DevelopersType == Developers.Developer2)
            {
                DefaultConnection = "server=13.70.93.206;user id=Dev_User;password=dev@2017;Initial Catalog=ClearJelly;MultipleActiveResultSets=true";
                ClearJellyEntities = "server=13.70.93.206;user id=Dev_User;password=dev@2017;Initial Catalog=ClearJelly;MultipleActiveResultSets=true";
                CompanyDBConnection = "server=13.70.93.206;user id=Dev_User;password=dev@2017;Connection Timeout=20;Integrated Security=False; MultipleActiveResultSets=true";
                AzureUserDatabase = "Server=tcp:democlearjellyserver.database.windows.net,1433;Persist Security Info=False;User ID=Nikin;Password=ze@lous2016;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";
                AbcCompanyDbConnection = "server=13.70.93.206;user id=Dev_User;password=dev@2017;MultipleActiveResultSets=true;Initial Catalog=ABC_Financial_";
            }
        }

        public static void ReleaseConfiguration()
        {
            ApiEndpoint = "https://tst.agilityplanning.com:9999";
            ApiDBList = "https://tst.agilityplanning.com:90";
            RedirectUri = "https://dev.agilityplanning.com:4363/Home.aspx";
            DiscoveryUrl = "https://developer.api.intuit.com/.well-known/openid_configuration";
        }

        private static void ABCConfiguration()
        {
            ABCAppId = "b87718fe";
            ABCAppKey = "b7a04acc3c573f9748f42295561b942e";
            ABCRedirectURI = "https://dev.agilityplanning.com:4363/Home.aspx";
            ABCBaseURL = "https://api.abcfinancial.com/rest/";
        }

        private static void AzureConfig()
        {
            IdaTenantId = "1265953a-956a-474e-b818-e38b55da0a96";
            IdaResourceUrl = "https://graph.windows.net";
            IdaClientId = "6a2f18ea-89db-4c1c-9623-45cafcd4412c";
            //IdaPostLogoutRedirectUri = "https://dev.agilityplanning.com:4363/";
        }

        private static void XeroConfig()
        {
            CallbackUrl = "https://dev.agilityplanning.com:4363/Home.aspx";
            SigningCertificatePath = @"C:\Certs\public_privatekey.pfx";
            //SigningCertificatePath = @"C:\CertsNew\public_privatekey.pfx";
            SigningCertificatePassword = "mana@2010";
            //PartnerConsumerKey = "9G3UX8BNHXTRWLJINO6SYLUIU57QCK";
            //PartnerConsumerSecret = "UX7ZZGVDOYEAGKFGQLFE12RAWJ0C2O";
            PartnerConsumerKey = "S8PNXVUYUUAQJWO1BVXD9IQ51RNE8H";
            PartnerConsumerSecret = "KL9OZP2VD4TFFFM9WF78GK539HY6CQ";
            BaseApiUrl = "https://api.xero.com";
        }
    }

}
