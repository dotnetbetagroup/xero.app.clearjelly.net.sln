﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.Configuration
{
    public enum ChosenService
    {
        Xero = 1,
        Quickbooks = 2,
        ABC = 3
    }
}
