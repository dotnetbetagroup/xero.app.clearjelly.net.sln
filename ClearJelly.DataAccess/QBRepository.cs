﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using NLog;
using System.ComponentModel;
using ClearJelly.Entities;
using CJ.Api.TokenStores.Dapper;
using ClearJelly.ViewModels;
using ViewModels;
using Intuit.Ipp.Data;
using EntityModel.Entities;
using ClearJelly.QBApp.ReportEntities;

namespace ClearJelly.DataAccess
{
    public class QBRepository
    {
        private string _connection;
        Logger _logger;
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();
        public QBRepository(string connectionString)
        {
            _connection = connectionString;
            _logger = LogManager.GetCurrentClassLogger();
        }
        protected SqlConnection DatabaseConnection
        {
            get
            {
                var cn = new SqlConnection(_connection);
                return cn;
            }
        }
        public void CreateTableQBReport(string companyName)
        {
            var sqlStr = @" Execute [dbo].[Create_QBReport_Table] '" + companyName + "'";
            _clearJellyContext.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
        }

        public void UpdateReport(string connection, string dbName, List<QBReport> model)
        {
            using (IDbConnection _dbConnection = new SqlConnection(connection))
            {
                string query = $"DELETE FROM [{dbName}].[dbo].[QBReport]";
                _dbConnection.Execute(query);
            }
            try
            {
                SaveQBReport(connection, model);
            }
            catch (Exception ex)
            {
                _logger.Error("QBRepository exception:" + ex.Message);
            }

        }
        public void InsertReport(string connection, List<QBReport> model)
        {
            try
            {
                SaveQBReport(connection, model);
            }
            catch (Exception ex)
            {
                _logger.Error("QBRepository exception:" + ex.Message);
            }
        }

        public void UpdateRefreshToken(string userName, string rToken, string aToken, string realmId)
        {
            int user = 0;

            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select UserId from [ClearJelly].[dbo].[User] where Email = @Email";
                    user = db.Query<int>(Query, new { Email = userName }).FirstOrDefault();
                }
                catch (Exception ex)
                {

                }
            }
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var updateQuery = @"update [ClearJelly].[dbo].[QuickBookRefreshTokens] set RefreshToken = @RefreshToken, AccessToken = @AccessToken, CreationDate = @CreationDate, ExpirationDate = @ExpirationDate where UserId = @UserId and RealmId = @RealmId";
                    var result = db.Execute(updateQuery, new { RefreshToken = rToken, UserId = user, RealmId = realmId, AccessToken = aToken, CreationDate = DateTime.Now, ExpirationDate = DateTime.Now.AddHours(+1) });
                }
                catch (Exception ex)
                {

                }
            }
        }
        public async System.Threading.Tasks.Task ClearTable(string table, string orgName)
        {
            _logger.Error("Executing QBRepository.ClearTable");
            try
            {
                using (IDbConnection _dbConnection = new SqlConnection(_connection))
                {
                    string query = $"DELETE FROM {table} WHERE OrgName=@OrgName";
                    _dbConnection.Execute(query, new { OrgName = orgName });
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error while clear tabl at QBRepository.ClearTable: " + ex.Message);
                throw;
            }
        }

        public async System.Threading.Tasks.Task InsertAccount(List<Account> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertAccount.");
            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allAccountList = new List<QuickBookAccountViewModel>();
                    var mappedList = QbMappers.CompleteAccountModel(items, orgName);
                    allAccountList.AddRange(mappedList);
                    SaveAccounts(allAccountList);
                    allAccountList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("QBRepository exception:" + ex.Message);
                }
            }
        }
        public async System.Threading.Tasks.Task InsertItem(List<Item> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertItem.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allItemList = new List<QuickBookItemsViewModel>();
                    var mappedList = QbMappers.CompleteItemtModel(items, orgName);
                    allItemList.AddRange(mappedList);
                    SaveItems(allItemList);
                    allItemList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertItem exception:" + ex.Message);
                }
            }
        }

        public async System.Threading.Tasks.Task InsertJournalEntry(List<JournalEntry> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertJournalEntry.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allJouralList = new List<QuickBookJournalViewModel>();
                    var mappedList = QbMappers.CompleteJouralModel(items, orgName);
                    allJouralList.AddRange(mappedList);
                    SaveJournas(allJouralList);
                    allJouralList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertJournalEntry exception:" + ex.Message);
                }
            }
        }

        public bool CreateQBatabaseAsync(string _companyName)
        {
            var con = "Server = tcp:aptest.database.windows.net,1433; Initial Catalog = master; Persist Security Info = False; User ID = admin@agilityplanningtst.onmicrosoft.com; Password = Managility@2017; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Authentication =\"Active Directory Password\";Connection Timeout=120;";
            _logger.Info($"Creating QuickBooks db for {_companyName}");
            try
            {
                using (SqlConnection connection = new SqlConnection(con))
                {
                    connection.Open();
                    var query = "create database [QuickBooks_" + _companyName + "] (EDITION = 'basic', SERVICE_OBJECTIVE = ELASTIC_POOL(name = aptst_pool));";
                    var res = connection.Execute(query);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create QuickBooks db for {_companyName} exception:{ex.Message}");
                return false;
            }
        }

        public async System.Threading.Tasks.Task InsertInvoice(List<Invoice> items, string orgName)
        {
            _logger.Info("Executing QBRepository InsertInvoice.");

            using (var con = new SqlConnection(_connection))
            {
                try
                {
                    var allInvoiceList = new List<QuickBookInvoiceViewModel>();
                    var mappedList = QbMappers.CompleteInvoiceModel(items, orgName);
                    allInvoiceList.AddRange(mappedList);
                    SaveInvoices(allInvoiceList);
                    allInvoiceList.Clear();
                }
                catch (Exception ex)
                {
                    _logger.Error("InsertInvoice exception:" + ex.Message);
                }
            }
        }

        public void SaveQBReport(string connectionstr, List<QBReport> model)
        {
            var dt = ConvertQBReportToDataTable(model);
            using (SqlConnection connection = new SqlConnection(connectionstr))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        var props = typeof(QBReport).GetProperties();
                        foreach (var item in props)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.Name, item.Name);
                        }                        
                        sqlBulkCopy.DestinationTableName = "QBReport";
                        sqlBulkCopy.WriteToServer(dt);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveJournas exception:" + ex.Message);

                }

            }
        }
        public void SaveAccounts(List<QuickBookAccountViewModel> accountList)
        {
            _logger.Info("Executing QBRepository SaveAccounts.");

            var dt = ConvertAccountToDataTable(accountList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        foreach (var item in GetUserColumnNames("Account"))
                        {
                            sqlBulkCopy.ColumnMappings.Add(item, item);
                        }
                        sqlBulkCopy.DestinationTableName = "Account";
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveAccounts exception:" + ex.Message);
                }

            }
        }

        public void SaveItems(List<QuickBookItemsViewModel> itemList)
        {
            _logger.Info("Executing QBRepository SaveItems.");

            var dt = ConvertItemToDataTable(itemList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        foreach (var item in GetUserColumnNames("Item"))
                        {
                            sqlBulkCopy.ColumnMappings.Add(item, item);
                        }
                        sqlBulkCopy.DestinationTableName = "Item";
                        sqlBulkCopy.WriteToServer(dt);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveAccounts exception:" + ex.Message);
                }

            }
        }
        public void SaveJournas(List<QuickBookJournalViewModel> journalList)
        {
            var dt = ConvertJournalToDataTable(journalList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        foreach (var item in GetUserColumnNames("JournalEntry"))
                        {
                            sqlBulkCopy.ColumnMappings.Add(item, item);
                        }
                        sqlBulkCopy.DestinationTableName = "JournalEntry";
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveJournas exception:" + ex.Message);

                }

            }
        }
        private List<string> GetUserColumnNames(string tableName)
        {
            var res = new SqlConnection(_connection).Query<string>($"SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('{tableName}')").ToList();
            return res;
        }

        public void SaveInvoices(List<QuickBookInvoiceViewModel> invoiceList)
        {
            var dt = ConvertInvoiceToDataTable(invoiceList);
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                try
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        foreach (var item in GetUserColumnNames("Invoice"))
                        {
                            sqlBulkCopy.ColumnMappings.Add(item, item);
                        }
                        sqlBulkCopy.DestinationTableName = "Invoice";
                        sqlBulkCopy.WriteToServer(dt);

                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("SaveInvoices exception:" + ex.Message);

                }

            }
        }
        public bool CheckDBReportExist(string connection, string tableName)
        {

            try
            {
                using (IDbConnection _dbConnection = new SqlConnection(connection))
                {
                    string query = $"select * from {tableName} ";
                    var result = _dbConnection.Execute(query);
                    return true;
                }
            }
            catch (Exception ex)
            {

                _logger.Error("Error while check table exist: " + ex.Message);
                return false;
            }

        }
        private DataTable ConvertInvoiceToDataTable(List<QuickBookInvoiceViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookInvoiceViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        private DataTable ConvertJournalToDataTable(List<QuickBookJournalViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookJournalViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        private DataTable ConvertItemToDataTable(List<QuickBookItemsViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookItemsViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        private DataTable ConvertQBReportToDataTable(List<QBReport> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QBReport));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        private DataTable ConvertAccountToDataTable(List<QuickBookAccountViewModel> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(QuickBookAccountViewModel));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public QuickBookRefreshToken checkRefreshTokenByUserName(string userName, string realmId, string _connection)
        {

            int user = 0;
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select UserId from [ClearJelly].[dbo].[User] where Email = @Email";
                    user = db.Query<int>(Query, new { Email = userName }).FirstOrDefault();
                }
                catch (Exception ex)
                {

                }
            }
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    //var Query = @"SELECT QuickBookRefreshTokens.Id, QuickBookRefreshTokens.UserId, RealmId, RefreshToken, AccessToken, CreationDate, ExpirationDate
                    //            FROM QuickBookRefreshTokens 
                    //            INNER JOIN [User] ON [User].Email = @Email 
                    //            AND QuickBookRefreshTokens.UserId = [User].UserId 
                    //            AND QuickBookRefreshTokens.RealmId = @RealmId";
                    var Query = @"select * from [ClearJelly].[dbo].[QuickBookRefreshTokens] where UserId = @UserId and RealmId = @RealmId";
                    var result = db.Query<QuickBookRefreshToken>(Query, new { UserId = user, RealmId = realmId }).FirstOrDefault();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }

    }
}

