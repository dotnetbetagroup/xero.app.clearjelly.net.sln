﻿using CJ.Api.TokenStores.Dapper;
using ClearJelly.Configuration;
using ClearJelly.Entities;
using ClearJelly.XeroApp;
using EntityModel.Entities;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Xero.Api.Core;
using Xero.Api.Core.Model.Types;
using static ClearJelly.XeroApp.CommonXero;

namespace ClearJelly.DataAccess
{
    public class HomeRepository
    {
        private string _connection;
        Logger _logger;
        ClearJellyEntities _clearJellyContext;
        
        public HomeRepository(string connectionString)
        {
            _clearJellyContext = new ClearJellyEntities();
            _connection = connectionString;
            _logger = LogManager.GetCurrentClassLogger();
        }
        protected SqlConnection DatabaseConnection
        {
            get
            {
                var cn = new SqlConnection(_connection);
                return cn;
            }
        }
        public string GetUserDBName(string uName, string databaseName)
        {
            String tmpConn = _connection + ";database=" + databaseName + ";";
            using (IDbConnection db = new SqlConnection(tmpConn))
            {
                try
                {
                    var sqlStr = @"SELECT Name FROM  Company Inner JOIN[ClearJelly].[dbo].[User] on Company.CompanyId = [ClearJelly].[dbo].[User].CompanyId where email ='" + uName + "'";
                    var result = db.Query<string>(sqlStr).FirstOrDefault();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        
        public bool CheckAbcClub(string clumNumber, int userId)
        {            
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var sqlStr = @"SELECT * From [ClearJelly].[dbo].[AbcClubs] where ClubNumber ='" + clumNumber + "' and UserId = '" + userId + "'";
                    var result = db.Query<AbcClub>(sqlStr).FirstOrDefault();
                    return result != null;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public List<AbcClub> GetAbcClubs(int userId)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var sqlStr = @"SELECT * From [ClearJelly].[dbo].[AbcClubs] where UserId = '" + userId + "'";
                    var result = db.Query<AbcClub>(sqlStr).ToList();
                    return result ;
                }
                catch (Exception ex)
                {
                    return new List<AbcClub>();
                }
            }
        }

        public bool InsertClub(string clumNumber, string clubName, int UserId)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var sqlStr = @"INSERT INTO [ClearJelly].[dbo].[AbcClubs] (Id, ClubNumber, isActive,ClubName,UserId) Values (@Id, @ClubNumber, @isActive,@ClubName,@UserId)";
                    var result = db.Execute(sqlStr, new {
                        Id = Guid.NewGuid(),
                        ClubNumber = clumNumber,
                        isActive = true,
                        ClubName = clubName,
                        UserId = UserId
                    });
                    return result != 1;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public string CheckDbExist(string email)
        {
            var databaseName = ConfigurationManager.AppSettings["DatabaseName"];
            var dbName = GetUserDBName(email, databaseName);
            SqlConnection MyConnection = null;
            String tmpConn = _connection + ";database= 'QuickBooks_" + dbName + "';";
            String tmpConnXero = _connection + ";database= 'Xero_" + dbName + "';";
            try
            {
                MyConnection = new SqlConnection(tmpConn);
                MyConnection.Open();
                MyConnection.Close();
                updateUserChosenService("Quickbooks", email);
                return "Quickbooks";
            }
            catch (SqlException ex)
            {
                try
                {
                    MyConnection = new SqlConnection(tmpConnXero);
                    MyConnection.Open();
                    MyConnection.Close();
                    updateUserChosenService("Xero", email);
                    _logger.Fatal(email + " ~An SQL error occurred during execution of GetUserDBName method. Error details:", ex.Message);
                    return "Xero";
                }
                catch (SqlException ex2)
                {
                    return "ABC";
                }
            }
        }
        public void updateUserChosenService(string data, string UserName)
        {
            var databaseName = ConfigurationManager.AppSettings["DatabaseName"];
            String tmpConn = _connection + ";database=" + databaseName + ";";
            using (IDbConnection db = new SqlConnection(tmpConn))
            {
                try
                {
                    var updateQuery = @"update[ClearJelly].[dbo].[User] set ChosenService = '" + data + "' where Email = '" + UserName + "'";

                    var result = db.Execute(updateQuery);
                }
                catch (Exception ex)
                {
                    _logger.Fatal(UserName + " ~An SQL error occurred during execution of GetUserDBName method. Error details:", ex.Message);
                }
            }
        }
        public Dictionary<string, string> GetUserOrganisations(String uName)
        {
            try
            {
                var companies = new Dictionary<string, string>();

                SqlConnection MyConnection = null;
                SqlDataReader myReader = null;
                String UserName = uName;               

                String sqlStr = @"SELECT  " +
                                         "    distinct LEFT([OrgName],25)  [OrgName], [OrgShortCode] " +
                                         "     " +
                                         " FROM [" + ConfigurationManager.AppSettings["DatabaseName"] + "].[dbo].[Xero_User_Org] A " +
                                         "   inner join [User] B " +
                                         "   ON " +
                                         "   A.CompanyID=B.CompanyId " +
                                         "   where " +
                                         "   UPPER(Ltrim(RTRIM(B.Email)))=UPPER('" + UserName.Trim() + "') ";
                try
                {
                    MyConnection = new SqlConnection(ConfigSection.ClearJellyEntities);
                    MyConnection.Open();

                    SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        companies.Add(myReader.GetString(1), myReader.GetString(0));
                    }
                    return companies;
                }
                catch (SqlException ex)
                {

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (MyConnection != null)
                    {
                        MyConnection.Close();
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                    }
                }
                return companies;

            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Home GetUserOrganisations event completed.", ex);

                throw;
            }
        }
        public async System.Threading.Tasks.Task InsertCompany(QuickBookCompany model, int userId)
        {            
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var query = $@"SELECT *  FROM[ClearJelly].[dbo].[QuickBookCompanies] q
                         JOIN[ClearJelly].[dbo].[User] u ON q.UserId = u.UserId
                        WHERE q.UserId = {userId} and RealmId = {model.RealmId}";                    
                    var compExist = db.Query<QuickBookCompany>(query).FirstOrDefault();
                    if (compExist != null && compExist.OrgName != null)
                    {
                        return;
                    }                   
                }
                catch (Exception ex)
                {

                }
            }
            model.UserId = userId;

            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var updateQuery = @"delete [ClearJelly].[dbo].[QuickBookCompanies] where RealmId = @RealmId and UserId = @UserId
                                        INSERT  INTO [ClearJelly].[dbo].[QuickBookCompanies](Id, OrgName, isActive,RealmId,UserId) 
                                        Values (@Id, @OrgName, @isActive,@RealmId,@UserId)";
                    var result = db.Execute(updateQuery, model);
                }
                catch (Exception ex)
                {

                }
            }
        }
        public string UpdateQBCompany(int userId, string realmId)
        {
            var companyName = string.Empty;
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select * from [QuickBookCompanies] where RealmId = @RealmId and OrgName is not null";
                    companyName = db.Query<QuickBookCompany>(Query, new { RealmId = realmId }).FirstOrDefault().OrgName;
                }
                catch (Exception ex)
                {
                    _logger.Error("UpdateQBCompany exeption select " + ex.Message);
                }
            }
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var updateQuery = @"update [QuickBookCompanies] set OrgName = @OrgName where RealmId = @RealmId and UserId = @UserId";
                    var result = db.Execute(updateQuery, new { RealmId = realmId, UserId = userId, OrgName = companyName });
                }
                catch (Exception ex)
                {
                    _logger.Error("UpdateQBCompany exeption update " + ex.Message);
                }
            }
            return companyName;
        }
        public void InsertModelProcess(int userId, string OrgName, int ModelType, DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of InsertModelProcess method started.");
                ModelProcess model = new ModelProcess()
                {
                    UserId = userId,
                    StartDate = DateTime.Now,
                    ProcessType = ModelType,
                    Status = (int)StatusEnum.Running,
                    OrganizationName = OrgName,
                    FromDate = startDate,
                    ToDate = endDate
                    // CreatedDate = DateTime.Now
                };
                _clearJellyContext.ModelProcesses.Add(model);
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Home InsertModelProcess event completed.", ex);
                throw ex;
            }
        }
        public void UpdateModelStatus(int UserId, int errorCodes)
        {
            try
            {
                _logger.Info("userId=" + UserId + " ~Execution of Home UpdateModelStatus method started.");
                var runningStatusId = (int)StatusEnum.Running;
                var modelProcess = _clearJellyContext.ModelProcesses.Where(a => a.UserId == UserId && a.Status == runningStatusId).OrderByDescending(a => a.StartDate).FirstOrDefault();
                modelProcess.Status = errorCodes == (int)System.Net.HttpStatusCode.OK ? (int)StatusEnum.Success : (int)StatusEnum.Fail;
                modelProcess.XeroErrorCode = errorCodes;
                modelProcess.EndDate = DateTime.Now;
                modelProcess.CreatedDate = DateTime.Now;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error("userId=" + UserId + " ~Execution of Home UpdateModelStatus event completed.", ex);


                throw ex;
            }
        }
        public void InsertOrganizationDetails(XeroCoreApi api, string orgShortCode, int companyId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "").ToString() + " ~Execution of Home InsertOrganizationDetails method started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    OrganisationDetail orgDetails = new OrganisationDetail();
                    orgDetails.APIKey = api.Organisation.ApiKey;
                    orgDetails.BaseCurrency = api.Organisation.BaseCurrency;
                    orgDetails.CreatedDate = api.Organisation.CreatedDateUtc;
                    orgDetails.OrgName = api.Organisation.Name;
                    orgDetails.OrgStatus = api.Organisation.OrganisationStatus == Xero.Api.Core.Model.Status.OrganisationStatus.Active ? true : false;
                    orgDetails.OrgType = Enum.GetName(typeof(OrganisationType), api.Organisation.OrganisationType);
                    orgDetails.RegistrationNumber = api.Organisation.RegistrationNumber;
                    orgDetails.SalesTaxBasisType = Enum.GetName(typeof(SalesTaxBasisType), api.Organisation.SalesTaxBasisType);
                    orgDetails.SalestaxPeriod = Enum.GetName(typeof(SalesTaxPeriodType), api.Organisation.SalesTaxPeriod);
                    orgDetails.OrgShortCode = orgShortCode;
                    orgDetails.CompanyId = companyId;
                    if (api.Organisation.Addresses.Count() > 0)
                    {
                        orgDetails.AddressLine1 = api.Organisation.Addresses[0].AddressLine1;
                        orgDetails.AddressLine2 = api.Organisation.Addresses[0].AddressLine2;
                        orgDetails.AddressLine3 = api.Organisation.Addresses[0].AddressLine3;
                        orgDetails.AddressLine4 = api.Organisation.Addresses[0].AddressLine4;
                        orgDetails.City = api.Organisation.Addresses[0].City;
                        orgDetails.AddressType = Enum.GetName(typeof(AddressType), api.Organisation.Addresses[0].AddressType);
                        orgDetails.PostalCode = api.Organisation.Addresses[0].PostalCode;
                    }
                    dbContext.OrganisationDetails.Add(orgDetails);
                    dbContext.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "").ToString() + " ~Execution of Home InsertOrganizationDetails method completed.", ex);
            }
        }
        public string IsMessageNotSeenByUser(int userId)
        {
            try
            {
                var errorMessage = string.Empty;
                string ErrorMessage = string.Empty;
                if (_clearJellyContext.ModelProcesses.Any(a => a.UserId == userId && !a.IsMessageShown))
                {
                    int? errorCode = 0;
                    var modelProcessData = _clearJellyContext.ModelProcesses.FirstOrDefault(x => x.UserId == userId && !x.IsMessageShown);
                    string fromDate = string.Empty;
                    string toDate = string.Empty;
                    if (modelProcessData != null)
                    {
                        errorCode = modelProcessData.XeroErrorCode;
                        fromDate = modelProcessData.FromDate.HasValue ? modelProcessData.FromDate.Value.ToShortDateString() : null;
                        toDate = modelProcessData.ToDate.HasValue ? modelProcessData.ToDate.Value.ToShortDateString() : null;
                    }

                    if (errorCode == (int)XeroErrorCodes.Ok)
                    {
                        ErrorMessage = "The load for " + fromDate + " to " + toDate + " of your Xero data has finished successfully.";
                    }
                    else
                    {
                        ErrorMessage = GetEnumDescription((XeroErrorCodes)errorCode);
                    }
                }
                return ErrorMessage;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "").ToString() + " ~Execution of isAnyProcessRunning method completed.", ex);
                throw;
            }
        }
        public static string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public void ChangeFlagOfModelProcess()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + "~Execution of ChangeFlagOfModelProcess method started.");
                var loggedinUser = HttpContext.Current.Session["Jelly_UserId"].ToString();
                int intLoginUser = int.Parse(loggedinUser);
                var runningTypeId = (int)StatusEnum.Running;
                var userWithFalseFlag = _clearJellyContext.ModelProcesses.Where(x => x.UserId == intLoginUser && x.Status != runningTypeId && x.IsMessageShown == false).ToList();
                if (userWithFalseFlag.Count > 0)
                {
                    foreach (var process in userWithFalseFlag)
                    {
                        process.IsMessageShown = true;
                    }
                    _clearJellyContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + "~Execution of ChangeFlagOfModelProcess method completed.", ex);
                throw;
            }
        }
        public string getQuickGuideData()
        {
            try
            {
                var emailTemplate = EmailTemplates.quickGuide;
                string currentDomain = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
                var template = _clearJellyContext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == (int)emailTemplate);
                var quickGuidePair = GetQuickGuidePair(currentDomain);
                foreach (var pair in quickGuidePair)
                {
                    template.TemplateContent = template.TemplateContent.Replace(pair.Key, pair.Value);
                }
                return template.TemplateContent;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Home getQuickGuideData event completed.", ex);

                throw ex;
            }
        }
        public static Dictionary<string, string> GetQuickGuidePair(string currentDomain)
        {
            var safeLink = currentDomain + "ManageIPRequest";
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.SafeIPLink, "<input type = 'button' value = 'Add Safe IP' id = 'addSafeIP' class='btn btn-primary btn-default'>");
            return replacePair;
        }
        public bool CheckModelExists(string orgName)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + "~Execution of CheckModelExists method started.");
                if (HttpContext.Current.Session["Jelly_UserId"] != null)
                {
                    var loggedinUserID = int.Parse(HttpContext.Current.Session["Jelly_UserId"].ToString());
                    var currentUser = _clearJellyContext.Users.SingleOrDefault(x => x.UserId == loggedinUserID && !x.IsDeleted);
                    var userWithFalseFlag = _clearJellyContext.Xero_User_Org.Any(x => x.CompanyID == currentUser.CompanyId && x.OrgName == orgName);
                    if (userWithFalseFlag)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    throw new Exception("session Jelly_UserId is not defined.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + "~Execution of CheckModelExists method started.", ex);
                throw;
            }
        }
        public User GetActiveUserByEmail(string email)
        {
            return _clearJellyContext.Users.SingleOrDefault(x => /*x.IsActive &&*/ x.Email == email && !x.IsDeleted);
        }
        public List<CompanySubscription> GetCurrentSubscriptions(int companyId, string email)
        {

            var currentDate = DateTime.Now.Date;
            return _clearJellyContext.CompanySubscriptions.Where(x => x.CompanyId == companyId /*&& x.IsActive*/ &&
                        (!x.EndDate.HasValue || x.EndDate.Value >= currentDate)).ToList();
        }
        public bool IsXeroCompanyExist(int companyId, string orgShortCode)
        {
            return _clearJellyContext.Xero_User_Org.Any(a => a.CompanyID == companyId && a.OrgShortCode == orgShortCode);
        }

        public int GetXeroCompanyCount(int companyId)
        {
            return _clearJellyContext.Xero_User_Org.Count(a => a.CompanyID == companyId);
        }
        public List<int?> GetChildUserAssignedSubscription(int userId)
        {
            return _clearJellyContext.AdditionalUsers.Where(a => a.UserId == userId)
                             .Select(a => a.CompanySubscriptionId).ToList();
        }
        public void AddXeroOrg(Xero_User_Org org)
        {
            _clearJellyContext.Xero_User_Org.Add(org);
            _clearJellyContext.SaveChanges();
        }
        public bool GetModelProcesses(int userId, StatusEnum runningTypeId)
        {
            var t = _clearJellyContext.ModelProcesses.Any(a => a.UserId == userId && a.Status == (int)runningTypeId);
            return t;
        }
        public bool GetRunningProcesses(int userId, int runningTypeId, string inOrgName)
        {
            return _clearJellyContext.ModelProcesses.Any(a => a.UserId == userId
                   && a.OrganizationName == inOrgName
                   && a.Status == runningTypeId);
        }
        public token checkIsTokenExist(string email)
        {
            return _clearJellyContext.tokens.FirstOrDefault(a => a.UserId == email);
        }
        public void AddRefreshToken(QuickBookRefreshToken model)
        {            
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var updateQuery = @"delete  [ClearJelly].[dbo].[QuickBookRefreshTokens] where UserId = @UserId and RealmId = @RealmId
                                        INSERT  INTO [ClearJelly].[dbo].[QuickBookRefreshTokens](Id, UserId, RefreshToken,RealmId, AccessToken, CreationDate, ExpirationDate) 
                                        Values (@Id, @UserId, @RefreshToken, @RealmId,@AccessToken, @CreationDate, @ExpirationDate)";

                    var result = db.Execute(updateQuery, model);
                }
                catch (Exception ex)
                {

                }
            }
        }
        public string CheckAccessTokenByUserName(string userName, string realmId)
        {
            int user = 0;

            //using (IDbConnection db = new SqlConnection(_connection))
            //{
            //    try
            //    {
            //        var Query = @"select UserId from [ClearJelly].[dbo].[User] where Email = @Email";
            //        user = db.Query<int>(Query, new { Email = userName }).FirstOrDefault();
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var query = $@"SELECT *  FROM[ClearJelly].[dbo].[QuickBookRefreshTokens] q
                         JOIN[ClearJelly].[dbo].[User] u ON q.UserId = u.UserId
                        WHERE q.Email = {userName} and RealmId = {realmId}";
                    //var test = _clearJellyContext.QuickBookRefreshTokens.Join(_clearJellyContext.Users, x => x.UserId, y => y.UserId, (x, y) => new { x, y }).Where(x => x.x.RealmId == realmId).FirstOrDefault();
                    //var Query = @"select * from [ClearJelly].[dbo].[QuickBookRefreshTokens] where UserId = @UserId and RealmId = @RealmId and ExpirationDate <= @ExpirationDate";
                    var result = db.Query<QuickBookRefreshToken>(query, new { UserId = user, RealmId = realmId, ExpirationDate = DateTime.Now }).FirstOrDefault();
                    return result != null ? result.RefreshToken : string.Empty;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }
        public QuickBookRefreshToken checkRefreshTokenByUserName(int userId, string realmId)
        {
            //int user = 0;
            //using (IDbConnection db = new SqlConnection(_connection))
            //{
            //    try
            //    {
            //        var Query = @"select UserId from [ClearJelly].[dbo].[User] where Email = @Email";
            //        user = db.Query<int>(Query, new { Email = userName }).FirstOrDefault();
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = "select QuickBookRefreshTokens.UserId ,RealmId ,RefreshToken ,AccessToken ,CreationDate ,ExpirationDate " +
                        "from QuickBookRefreshTokens INNER join [User] ON [User].UserId = @UserId and  QuickBookRefreshTokens.UserId = [User].UserId " +
                        "and QuickBookRefreshTokens.RealmId = @RealmId ";
                    //var Query = @"select * from [ClearJelly].[dbo].[QuickBookRefreshTokens] where UserId = @UserId and RealmId = @RealmId";
                    var result = db.Query<QuickBookRefreshToken>(Query, new { UserId = userId, RealmId = realmId }).FirstOrDefault();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }
        public void UpdateRefreshToken(string userName, string rToken)
        {
            int user = 0;

            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select UserId from [ClearJelly].[dbo].[User] where Email = @Email";
                    user = db.Query<int>(Query, new { Email = userName }).FirstOrDefault();
                }
                catch (Exception ex)
                {

                }
            }
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = "select QuickBookRefreshTokens.UserId ,RealmId ,RefreshToken ,AccessToken ,CreationDate ,ExpirationDate from QuickBookRefreshTokens INNER join [User] ON [User].Email = @email and  QuickBookRefreshTokens.UserId = [User].UserId and QuickBookRefreshTokens.RealmId = @RealmId ";

                    var updateQuery = @"update [ClearJelly].[dbo].[QuickBookRefreshTokens] set RefreshToken = @RefreshToken where UserId = @UserId";

                    var result = db.Execute(updateQuery, new { RefreshToken = rToken, UserId = user });
                }
                catch (Exception ex)
                {

                }
            }
        }

        public List<QuickBookCompany> GetUserCompanies(int currentUser)
        {
            List<QuickBookCompany> users = new List<QuickBookCompany>();       
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select * from [ClearJelly].[dbo].[QuickBookCompanies] where UserId = @UserId";
                    var t = db.Query<QuickBookCompany>(Query, new { UserId = currentUser }).ToList();
                    return t;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }

        public string GetRealmId(string company, int userId)
        {
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {
                    var Query = @"select RealmId from [ClearJelly].[dbo].[QuickBookCompanies] where OrgName = @OrgName and UserId = @UserId";
                    var res = db.Query<string>(Query, new { OrgName = company, UserId = userId }).FirstOrDefault();
                    return res;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


    }
}