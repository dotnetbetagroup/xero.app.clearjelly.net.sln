﻿using NLog;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Data.SqlClient;
using ABCFinancialsLibrary.Helpers;
using ABCFinancialsLibrary.Models.Clubs;
using Dapper;
using Dapper.Contrib.Extensions;
using ABCFinancialsLibrary.Entities;
using System.Linq;

namespace ABCFinancialsLibrary.Repositories
{
    public class ABCFinancialsClubsRepository
    {
        private readonly string _connectionString;
        private readonly Logger _logger;

        public ABCFinancialsClubsRepository(string connectionString)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _connectionString = connectionString;
        }

        public async Task BulkInsertList<T>(List<T> clubs, string tableName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await RepositoryHelper.BulkInsertList(connection, clubs, tableName);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"{tableName} bulk insert exception:{ex.Message}");
            }
        }

        public async Task UpdateClub(Club club, ClubOnline clubOnline, ClubOnlineMinors clubOnlineMinors, ClubOnlineCcNames clubOnlieCCNames, List<ClubCountry> clubCountries)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();//check
                    using (var transaction = connection.BeginTransaction())
                    {
                        var clearTasks = new List<Task<int>>
                        {
                            RepositoryHelper.ClearTable("ClubOnlineCcNames", connection,transaction),
                            RepositoryHelper.ClearTable("ClubOnlineMinors", connection,transaction),
                            RepositoryHelper.ClearTable("ClubOnlines", connection,transaction),
                            RepositoryHelper.ClearTable("ClubCountries", connection,transaction),
                            RepositoryHelper.ClearTable("Clubs", connection,transaction)
                        };
                        await Task.WhenAll(clearTasks);

                        var clubId = await connection.InsertAsync(club, transaction);
                        clubOnline.ClubId = clubId;

                        var clubOnlineId = await connection.InsertAsync(clubOnline, transaction);
                        clubOnlineMinors.ClubOnlineId = clubOnlineId;
                        clubOnlieCCNames.ClubOnlineId = clubOnlineId;
                        await connection.InsertAsync(clubOnlineMinors, transaction);
                        await connection.InsertAsync(clubOnlieCCNames, transaction);

                        clubCountries.ForEach(cc => cc.ClubId = clubId);
                        await connection.InsertAsync(clubCountries, transaction);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Clubs update exception:{ex.Message}");
            }
        }

        public async Task UpdatePlans(List<ClubPlan> clubPlans)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();//check
                    using (var transaction = connection.BeginTransaction())
                    {
                        await RepositoryHelper.ClearTable("ClubPlans", connection, transaction);
                        await connection.InsertAsync(clubPlans, transaction);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"ClubPlans update exception:{ex.Message}");
            }
        }

        public async Task UpdatePlansAnnualFees(List<ClubPlanAnnualFeeModel> clubPlanAnnualFeesModels)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();//check
                    using (var transaction = connection.BeginTransaction())
                    {
                        var clearTasks = new List<Task<int>>
                        {
                            RepositoryHelper.ClearTable("ClubPlanAnnualFeeAssignedClubNumbers", connection,transaction),
                            RepositoryHelper.ClearTable("ClubPlanAnnualFees", connection,transaction)
                        };
                        await Task.WhenAll(clearTasks);
                        var clubAnnualFees = clubPlanAnnualFeesModels
                            .Select(cafm => new ClubPlanAnnualFee
                            {
                                Amount = cafm.Amount,
                                Id = cafm.Id,
                                Name = cafm.Name,
                                PreTaxAmount = cafm.PreTaxAmount,
                                ProfitCenterAbcCode = cafm.ProfitCenterAbcCode
                            }).ToList();
                        //OR set ids Manually instead of database
                        foreach (var item in clubAnnualFees)
                        {
                            await InsertAnnualFee(clubPlanAnnualFeesModels, item, connection, transaction);
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"ClubPlansAnnualFees update exception:{ex.Message}");
            }
        }

        private async Task InsertAnnualFee(List<ClubPlanAnnualFeeModel> clubPlanAnnualFeesModels, ClubPlanAnnualFee item, SqlConnection connection, SqlTransaction transaction)
        {
            var clubAnnualFeeId = await connection.InsertAsync(item, transaction);
            var clubPlanAnnualFeeAssignedClubNumbersModels = clubPlanAnnualFeesModels.Where(cafm => cafm.Id == item.Id).FirstOrDefault()?.AssignedClubNumbers;
            if (clubPlanAnnualFeeAssignedClubNumbersModels != null && clubPlanAnnualFeeAssignedClubNumbersModels.Count > 0)
            {
                var clubPlanAnnualFeeAssignedClubNumbers = clubPlanAnnualFeeAssignedClubNumbersModels
                    .Select(m => new ClubPlanAnnualFeeAssignedClubNumber
                    {
                        ClubPlanAnnualFeeId = clubAnnualFeeId,
                        Number = m
                    })
                    .ToList();
                await connection.InsertAsync(clubPlanAnnualFeeAssignedClubNumbers, transaction);
            }
        }

        public async Task UpdatePlansBillingCatalogItems(List<BillingCatalogItemModel> billingCatalogItemsModels)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();//check
                    using (var transaction = connection.BeginTransaction())
                    {
                        var clearTasks = new List<Task<int>>
                        {
                            RepositoryHelper.ClearTable("Tax", connection,transaction),
                            RepositoryHelper.ClearTable("BillingCatalogItems", connection,transaction)                            
                        };
                        await Task.WhenAll(clearTasks);
                        var billingCatalogItems = billingCatalogItemsModels
                            .Select(bci => new BillingCatalogItem
                            {
                                Id = bci.Id,
                                Name = bci.Name,
                                AnnualFee = bci.AnnualFee,
                                TotalTaxRate = bci.TotalTaxRate,
                                ProfitCenterAbcCode = bci.ProfitCenterAbcCode
                            }).ToList();
                        //OR set ids Manually instead of database
                        foreach (var item in billingCatalogItems)
                        {
                            await InsertBillingCatalogItem(billingCatalogItemsModels, item, connection, transaction);
                        }

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"ClubPlansAnnualFees update exception:{ex.Message}");
            }
        }

        private async Task InsertBillingCatalogItem(List<BillingCatalogItemModel> billingCatalogItemsModels, BillingCatalogItem item, SqlConnection connection, SqlTransaction transaction)
        {
            var billingCatalogItemId = await connection.InsertAsync(item, transaction);
            var taxModels = billingCatalogItemsModels.Where(bci => bci.Id == item.Id).FirstOrDefault()?.Taxes;
            if (taxModels != null && taxModels.Count > 0)
            {
                var taxes = taxModels
                    .Select(tm => new Tax
                    {
                        Amount = tm.Amount,
                        Id = tm.Id,
                        Name = tm.Name,
                        Percentage = tm.Percentage,
                        RateId = tm.RateId
                    })
                    .ToList();
                await connection.InsertAsync(taxes, transaction);
            }
            var assignedClubNumbersModels = billingCatalogItemsModels.Where(bci => bci.Id == item.Id).FirstOrDefault()?.AssignedClubNumbers;
            if (assignedClubNumbersModels != null && assignedClubNumbersModels.Count > 0)
            {
                var clubPlanAnnualFeeAssignedClubNumbers = assignedClubNumbersModels
                    .Select(m => new BillingCatalogItemAssignedClubNumber
                    {
                        BillingCatalogItemId = billingCatalogItemId,
                        Number = m
                    })
                    .ToList();
                await connection.InsertAsync(clubPlanAnnualFeeAssignedClubNumbers, transaction);
            }
        }

        public async Task UpdatePlansPaymentMethods(List<PaymentMethod> clubAccountPaymentMethods, List<PaymentMethod> abcBillingPaymentMethods)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();//check
                    using (var transaction = connection.BeginTransaction())
                    {
                        var clearTasks = new List<Task<int>>
                        {
                            RepositoryHelper.ClearTable("ClubAccountPaymentMethods", connection,transaction),
                            RepositoryHelper.ClearTable("AbcBillingPaymentMethods", connection,transaction)
                        };
                        await Task.WhenAll(clearTasks);

                        await connection.InsertAsync(clubAccountPaymentMethods, transaction);
                        await connection.InsertAsync(abcBillingPaymentMethods, transaction);

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Clubs update exception:{ex.Message}");
            }
        }


        //private async Task<int> ClearTable(string table, IDbConnection connection, IDbTransaction transaction = null)
        //{
        //    var query = $"DELETE FROM {table}";
        //    var result = await connection.ExecuteAsync(query, transaction);
        //    return result;
        //}


        //public async Task BulkInsertList<T>(List<T> clubs, string tableName)
        //{
        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(_connectionString))
        //        {
        //            using (SqlBulkCopy copy = new SqlBulkCopy(connection))
        //            {
        //                var table = ConvertItemToDataTable(clubs);
        //                copy.DestinationTableName = tableName;
        //                await copy.WriteToServerAsync(table);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error($"{tableName} bulk insert exception:{ex.Message}");
        //    }
        //}

        //private DataTable ConvertItemToDataTable<T>(List<T> types)
        //{
        //    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        //    var table = new DataTable();
        //    foreach (PropertyDescriptor prop in properties)
        //    {
        //        var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
        //        table.Columns.Add(prop.Name, type);
        //    }

        //    foreach (var item in types)
        //    {
        //        DataRow row = table.NewRow();
        //        foreach (PropertyDescriptor prop in properties)
        //        {
        //            var value = prop.GetValue(item);
        //            row[prop.Name] = value;
        //        }
        //        table.Rows.Add(row);
        //    }
        //    return table;
        //}
    }
}
