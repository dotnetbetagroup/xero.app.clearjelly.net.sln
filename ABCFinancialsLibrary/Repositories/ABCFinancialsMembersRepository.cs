﻿using NLog;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Data.SqlClient;
using ABCFinancialsLibrary.Helpers;

namespace ABCFinancialsLibrary.Repositories
{
    public class ABCFinancialsMembersRepository
    {
        private readonly string _connectionString;
        private readonly Logger _logger;

        public ABCFinancialsMembersRepository(string connectionString)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _connectionString = connectionString;
        }
        
        public async Task BulkInsertList<T>(List<T> clubs, string tableName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await RepositoryHelper.BulkInsertList(connection, clubs, tableName);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"{tableName} bulk insert exception:{ex.Message}");
            }
        }
    }
}
