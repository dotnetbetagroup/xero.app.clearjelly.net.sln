﻿namespace ABCFinancialsLibrary.Entities
{
    public class CheckinDetailsSummariesMember
    {
        public string MemberId { get; set; }

        public string Links { get; set; }

        //public CheckinCountsModel CheckInCounts { get; set; }
    }
}
