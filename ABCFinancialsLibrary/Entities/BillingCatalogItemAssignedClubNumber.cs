﻿namespace ABCFinancialsLibrary.Entities
{
    public class BillingCatalogItemAssignedClubNumber
    {
        public int BillingCatalogItemId { get; set; }

        public string Number { get; set; }
    }
}
