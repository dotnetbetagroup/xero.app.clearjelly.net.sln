﻿using ABCFinancialsLibrary.Entities.Enums;

namespace ABCFinancialsLibrary.Entities
{
    public class Club
    {
        public string Name { get; set; }
        
        public string ShortName { get; set; }
        
        public string TimeZone { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Email { get; set; }

        public string DonationItem { get; set; }

        public OnlineSignupAllowedPaymentMethods? OnlineSignupAllowedPaymentMethods { get; set; }

        public string BillingCountry { get; set; }

        public string CreditCardPaymentMethods { get; set; } //In doc Array[string]
    }
}