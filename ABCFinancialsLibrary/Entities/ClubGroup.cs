﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Entities
{
    public class ClubGroup
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
//Example
//{
//    "id": "010aee3beb4e4ed5a3682d6276095d15",
//    "name": "string",
//    "status": "string"
//}