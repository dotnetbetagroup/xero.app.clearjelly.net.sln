﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Entities
{
    public partial class Payment
    {
        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("paymentAmount")]
        public string PaymentAmount { get; set; }

        [JsonProperty("paymentTax")]
        public string PaymentTax { get; set; }
    }
}
