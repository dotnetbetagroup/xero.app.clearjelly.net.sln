﻿using System;

namespace ABCFinancialsLibrary.Entities
{
    public class ClubCheckin
    {
        public string CheckInId { get; set; }

        public int ClubNumber { get; set; }

        public DateTime? CheckInTimeStamp { get; set; }

        public string CheckInMessage { get; set; }

        public string StationName { get; set; }

        public string CheckInStatus { get; set; }//TODO: change to enum

        //public CheckinsDetailsMember Member { get; set; }
    }
}