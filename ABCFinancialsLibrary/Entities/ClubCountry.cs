﻿namespace ABCFinancialsLibrary.Entities
{
    public class ClubCountry
    {
        public int ClubId { get; set; }

        public string Country { get; set; }
    }
}
