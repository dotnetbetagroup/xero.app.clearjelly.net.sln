﻿using Newtonsoft.Json;
using System;

namespace ABCFinancialsLibrary.Entities
{
    public class Note
    {
        [JsonProperty("noteText")]
        public string NoteText { get; set; }

        [JsonProperty("noteCreateTimestamp")]
        public DateTime? NoteCreateTimestamp { get; set; }

        [JsonProperty("employeeId")]
        public string EmployeeId { get; set; }
    }
}
//Example
//{
//    "noteText": "Peanut allergy",
//    "noteCreateTimestamp": "2016-12-28 08:00:00.000000",
//    "employeeId": "010aee3beb4e4ed5a3682d6276095d15"
//}