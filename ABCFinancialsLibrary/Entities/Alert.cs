﻿using Newtonsoft.Json;
using System;

namespace ABCFinancialsLibrary.Entities
{
    public class Alert
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("abcCode")]
        public string AbcCode { get; set; }

        [JsonProperty("priority")]
        public int? Priority { get; set; }

        [JsonProperty("allowDoorAccess")]
        public bool? AllowDoorAccess { get; set; }

        [JsonProperty("evaluationDate")]
        public DateTime? EvaluationDate { get; set; }

        [JsonProperty("gracePeriod")]
        public int? GracePeriod { get; set; }

        [JsonProperty("evaluationAmount")]
        public decimal? EvaluationAmount { get; set; }
    }
}

//Example
//{
//    "message": "PAYMENT OVERDUE 1196 DAYS",
//    "abcCode": "Payment Overdue Alert",
//    "priority": "1",
//    "allowDoorAccess": "true",
//    "evaluationDate": "1997-08-13",
//    "gracePeriod": "60",
//    "evaluationAmount": 123.45
//}
