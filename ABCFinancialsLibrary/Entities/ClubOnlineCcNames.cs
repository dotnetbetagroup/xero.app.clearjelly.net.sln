﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Entities
{
    public class ClubOnlineCcNames
    {
        public int ClubOnlineId { get; set; }

        [JsonProperty("requireCCNameMatch")]
        public bool? RequireCcNameMatch { get; set; }

        [JsonProperty("differentCcNamesDisclaimer")]
        public string DifferentCcNamesDisclaimer { get; set; }
    }
}
//Example
//{
//      "requireCCNameMatch": "true",
//      "differentCcNamesDisclaimer": "string"
//}