﻿namespace ABCFinancialsLibrary.Entities
{
    public class ClubPlanAnnualFeeAssignedClubNumber
    {
        public int ClubPlanAnnualFeeId { get; set; }

        public string Number { get; set; }
    }
}
