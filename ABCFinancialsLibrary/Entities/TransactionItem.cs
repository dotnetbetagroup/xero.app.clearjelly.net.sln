﻿using ABCFinancialsLibrary.Entities.Enums;

namespace ABCFinancialsLibrary.Entities
{
    public class TransactionItem
    {
        public string ItemId { get; set; }

        public string Name { get; set; }

        public InventoryType? InventoryType { get; set; }

        public bool? Sale { get; set; }

        public string Upc { get; set; }

        public string ProfitCenter { get; set; }

        public CatalogType? Catalog { get; set; }

        public string UnitPrice { get; set; }

        public int? Quantity { get; set; }

        public string PackageQuantity { get; set; }

        public string Subtotal { get; set; }

        public string Tax { get; set; }

        public string RecurringServiceId { get; set; }

        //public List<Payment> Payments { get; set; }
    }
}
