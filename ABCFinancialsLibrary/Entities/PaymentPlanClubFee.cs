﻿using Newtonsoft.Json;
using System;

namespace ABCFinancialsLibrary.Entities
{
    public class PaymentPlanClubFee
    {
        [JsonProperty("feeDueDate")]
        public DateTime? FeeDueDate { get; set; }

        [JsonProperty("feeName")]
        public string FeeName { get; set; }

        [JsonProperty("feeAmount")]
        public string FeeAmount { get; set; }

        [JsonProperty("feeApply")]
        public bool? FeeApply { get; set; }

        [JsonProperty("feeRecurring")]
        public bool? FeeRecurring { get; set; }
    }
}
//Example
//{
//"feeDueDate": "1997-08-13",
//"feeName": "string",
//"feeAmount": "$35.00",
//"feeApply": true,
//"feeRecurring": true
//}