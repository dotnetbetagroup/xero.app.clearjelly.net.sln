﻿using ABCFinancialsLibrary.Entities.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace ABCFinancialsLibrary.Models
{
    public class Personal
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("middleInitial")]
        public string MiddleInitial { get; set; }

        [JsonProperty("addressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("addressLine2")]
        public string AddressLine2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("homeClub")]
        public string HomeClub { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("primaryPhone")]
        public string PrimaryPhone { get; set; }

        [JsonProperty("mobilePhone")]
        public string MobilePhone { get; set; }

        [JsonProperty("workPhone")]
        public string WorkPhone { get; set; }

        [JsonProperty("workPhoneExt")]
        public string WorkPhoneExt { get; set; }

        [JsonProperty("emergencyContactName")]
        public string EmergencyContactName { get; set; }

        [JsonProperty("emergencyPhone")]
        public string EmergencyPhone { get; set; }

        [JsonProperty("emergencyExt")]
        public string EmergencyExt { get; set; }

        [JsonProperty("emergencyAvailability")]
        public string EmergencyAvailability { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("birthDate")]
        public DateTime? BirthDate { get; set; } 

        [JsonProperty("gender")]
        public Gender? Gender { get; set; }

        [JsonProperty("employer")]
        public string Employer { get; set; }

        [JsonProperty("occupation")]
        public string Occupation { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }

        [JsonProperty("memberStatus")]
        //[JsonConverter(typeof(StringEnumConverter))]
        public MemberStatus? MemberStatus { get; set; }

        [JsonProperty("joinStatus")]
        //[JsonConverter(typeof(StringEnumConverter))]
        public JoinStatus? JoinStatus { get; set; }

        [JsonProperty("isConvertedProspect")]
        public bool? IsConvertedProspect { get; set; }

        [JsonProperty("hasPhoto")]
        public bool? HasPhoto { get; set; }

        [JsonProperty("convertedDate")]
        public DateTime? ConvertedDate { get; set; }

        [JsonProperty("memberStatusReason")]
        public string MemberStatusReason { get; set; }

        [JsonProperty("wellnessProgramId")]
        public string WellnessProgramId { get; set; }

        [JsonProperty("firstCheckInTimestamp")]
        public DateTime? FirstCheckInTimestamp { get; set; }

        [JsonProperty("memberStatusDate")]
        public DateTime? MemberStatusDate { get; set; }

        [JsonProperty("lastCheckInTimestamp")]       
        public DateTime? LastCheckInTimestamp { get; set; }

        [JsonProperty("totalCheckInCount")]
        public long? TotalCheckInCount { get; set; }

        [JsonProperty("createTimestamp")]
        public DateTime? CreateTimestamp { get; set; }

        [JsonProperty("lastModifiedTimestamp")]
        public DateTime? LastModifiedTimestamp { get; set; }
    }
}
//Example
//{
//    "firstName": "Mitch",
//    "lastName": "Conner",
//    "middleInitial": "H",
//    "addressLine1": "28201 E. Bonanza St.",
//    "addressLine2": "#409",
//    "city": "South Park",
//    "state": "CO",
//    "postalCode": "80440",
//    "homeClub": "9003",
//    "countryCode": "US",
//    "email": "mrkitty@gmail.com",
//    "primaryPhone": "9495898283",
//    "mobilePhone": "9495898283",
//    "workPhone": "9495898283",
//    "workPhoneExt": "1234",
//    "emergencyContactName": "Mitch Connor",
//    "emergencyPhone": "9495898283",
//    "emergencyExt": "1234",
//    "emergencyAvailability": "string",
//    "barcode": "barcode1",
//    "birthDate": "1997-08-13",
//    "gender": "Male",
//    "employer": "string",
//    "occupation": "Employee",
//    "group": "string",
//    "isActive": true,
//    "memberStatus": "active",
//    "joinStatus": "member",
//    "isConvertedProspect": true,
//    "hasPhoto": true,
//    "convertedDate": "2016-12-28",
//    "memberStatusReason": "active",
//    "wellnessProgramId": "010aee3beb4e4ed5a3682d6276095d15",
//    "firstCheckInTimestamp": "2016-12-28 08:00:00.000000",
//    "memberStatusDate": "1997-08-13",
//    "lastCheckInTimestamp": "2016-12-28 08:00:00.000000",
//    "totalCheckInCount": 100,
//    "createTimestamp": "2016-12-28 08:00:00.000000",
//    "lastModifiedTimestamp": "2016-12-28 08:00:00.000000"
//}