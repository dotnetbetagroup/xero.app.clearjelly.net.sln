﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Entities
{
    public class PaymentMethod
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("abcCode")]
        public string AbcCode { get; set; }
    }
}
