﻿using Newtonsoft.Json;
using System;

namespace ABCFinancialsLibrary.Entities
{
    public class PaymentPlanSchedule
    {
        [JsonProperty("profitCenter")]
        public string ProfitCenter { get; set; }

        [JsonProperty("scheduleDueDate")]
        public DateTime? ScheduleDueDate { get; set; }

        [JsonProperty("scheduleAmount")]
        public string ScheduleAmount { get; set; }

        [JsonProperty("numberOfPayments")]
        public string NumberOfPayments { get; set; }

        [JsonProperty("recurring")]
        public bool? Recurring { get; set; }

        [JsonProperty("addon")]
        public bool? Addon { get; set; }

        [JsonProperty("defaultChecked")]
        public bool? DefaultChecked { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}

//Example
//{
//"profitCenter": "string",
//"scheduleDueDate": "1997-08-13",
//"scheduleAmount": "$35.00",
//"numberOfPayments": "09",
//"recurring": true,
//"addon": true,
//"defaultChecked": true,
//"description": "string"
//}
