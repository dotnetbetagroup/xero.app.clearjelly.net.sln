﻿using System.ComponentModel;

namespace ABCFinancialsLibrary.Entities.Enums
{
    public enum RenewalType
    {
        [Description("None")]
        None = 0,
        [Description("Auto-Renew Open")]
        AutoRenewOpen = 1,
        [Description("Term Renewal")]
        TermRenewal = 2
    }
}
//Example
//['Auto-Renew Open', 'Term Renewal', 'None'] ,