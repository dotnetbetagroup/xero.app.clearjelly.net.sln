﻿namespace ABCFinancialsLibrary.Entities.Enums
{
    public enum MemberStatus
    {
        None = 0,
        active = 1,
        inactive = 2,
        all = 3,
        prospect = 4
    }
}
//Example
//['active', 'inactive', 'all', 'prospect']