﻿using System.ComponentModel;

namespace ABCFinancialsLibrary.Entities.Enums
{
    public enum CheckInStatus
    {
        None = 0,
        [Description("Normal Entry")]
        NormalEntry = 1,
        [Description("Entry Allowed")]
        EntryAllowed = 2,
        [Description("Entry Denied")]
        EntryDenied = 3,
        [Description("Entry Not Set")]
        EntryNotSet = 4
    }
}
//Example
// ['Normal Entry', 'Entry Allowed', 'Entry Denied', 'Entry Not Set'] 