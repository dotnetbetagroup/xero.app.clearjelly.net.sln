﻿using System.ComponentModel;

namespace ABCFinancialsLibrary.Entities.Enums
{
    public enum PaymentFrequency
    {
        None = 0,
        [Description("Weekly")]
        Weekly = 1,
        [Description("Bi-Weekly")]
        BiWeekly = 2,
        [Description("Monthly")]
        Monthly = 3,
        [Description("Semi-Annuall")]
        SemiAnnually = 4,
        [Description("Annually")]
        Annually = 5
    }
}
//Example
//['Weekly', 'Bi-Weekly', 'Monthly', 'Semi-Annually', 'Annually'] ,