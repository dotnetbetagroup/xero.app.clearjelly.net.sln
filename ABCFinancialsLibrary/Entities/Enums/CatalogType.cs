﻿namespace ABCFinancialsLibrary.Entities.Enums
{
    public enum CatalogType
    {
        None = 0,
        Company = 1,
        Club = 2
    }
}
//Example
// ['Company', 'Club']