﻿namespace ABCFinancialsLibrary.Entities.Enums
{
    public enum Gender
    {
        None = 0,
        Unknown = 1,
        Male = 2,
        Female = 3
    }
}
//Example
//['Male', 'Female', 'Unknown'] 