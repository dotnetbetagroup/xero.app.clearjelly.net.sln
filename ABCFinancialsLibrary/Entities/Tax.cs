﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Entities
{
    public class Tax
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("rateId")]
        public string RateId { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("percentage")]
        public byte? Percentage { get; set; }
    }
}
//Example
//{
//      "id": "c41e5df2682a481b8396da9e10272390",
//      "rateId": "010aee3beb4e4ed5a3682d6276095d15",
//      "amount": "$35.00",
//      "name": "string",
//      "percentage": 0
//}