﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models
{
    public class PrimaryBillingAccountHolder
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
