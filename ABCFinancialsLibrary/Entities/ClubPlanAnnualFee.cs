﻿namespace ABCFinancialsLibrary.Entities
{
    public class ClubPlanAnnualFee
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Amount { get; set; }

        public string PreTaxAmount { get; set; }

        public string ProfitCenterAbcCode { get; set; }

        //public List<string> AssignedClubNumbers { get; set; }
    }
}
