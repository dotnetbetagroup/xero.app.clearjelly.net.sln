﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Entities
{
    public class ClubOnlineMinors
    {
        public int ClubOnlineId { get; set; }

        [JsonProperty("allowMinors")]
        public bool AllowMinors { get; set; }

        [JsonProperty("minorAge")]
        public byte MinorAge { get; set; }

        [JsonProperty("minorDisclaimer")]
        public string MinorDisclaimer { get; set; }
    }
}
//Example
//{
//     "allowMinors": "true",
//     "minorAge": "18",
//     "minorDisclaimer": "string"
//    }