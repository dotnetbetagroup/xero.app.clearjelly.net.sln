﻿using System;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Entities
{
    public class PaymentPlan
    {
        public string PlanName { get; set; }

        public string PlanId { get; set; }

        public string PromotionCode { get; set; }

        public string PromotionName { get; set; }

        public string MembershipType { get; set; }

        //TODO change to enum
        public string AgreementTerm { get; set; }

        //TODO change to enum    
        public string ScheduleFrequency { get; set; }

        public long TermInMonths { get; set; }

        public string DueDay { get; set; }

        public DateTime? FirstDueDate { get; set; }

        public bool ActivePresale { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string OnlineSignupAllowedPaymentMethods { get; set; }

        public string PreferredPaymentMethod { get; set; }

        public string TotalContractValue { get; set; }

        public string DownPaymentName { get; set; }

        public string DownPaymentTotalAmount { get; set; }

        public string ScheduleTotalAmount { get; set; }        

        public string AgreementTerms { get; set; }

        public string AgreementDescription { get; set; }

        public string AgreementNote { get; set; }

        public string EmailGreeting { get; set; }

        public string EmailClosing { get; set; }

        public string ClubFeeTotalAmount { get; set; }

        public long PlanValidation { get; set; }

        //public List<PaymentPlanUserDefinedField> UserDefinedFields { get; set; }

        //public List<PaymentPlanClubFee> ClubFees { get; set; }

        //public List<PaymentPlanFieldOption> FieldOption { get; set; }

        //public List<PaymentPlanDownPayment> DownPayments { get; set; }

        //public List<PaymentPlanSchedule> Schedules { get; set; }
    }
}
