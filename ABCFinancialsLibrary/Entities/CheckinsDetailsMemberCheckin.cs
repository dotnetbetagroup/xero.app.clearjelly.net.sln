﻿using Newtonsoft.Json;
using System;

namespace ABCFinancialsLibrary.Entities
{
    public class CheckinsDetailsMemberCheckin
    {
        [JsonProperty("checkInId")]
        public string CheckInId { get; set; }

        [JsonProperty("clubNumber")]
        public int? ClubNumber { get; set; }

        [JsonProperty("checkInTimeStamp")]
        public DateTime? CheckInTimeStamp { get; set; }
    }
}
//Example
//{
//    "checkInId": "010aee3beb4e4ed5a3682d6276095d15",
//    "clubNumber": "9003",
//    "checkInTimeStamp": "2016-12-28 08:00:00.000000"
//}