﻿using ABCFinancialsLibrary.Entities.Enums;
using System;

namespace ABCFinancialsLibrary.Entities
{
    public class Agreement
    {
        public string AgreementNumber { get; set; }

        public bool? IsPrimaryMember { get; set; }

        public bool? IsNonMember { get; set; }
        
        public long? Ordinal { get; set; }

        public string ReferringMemberId { get; set; }

        public string ReferringMemberHomeClub { get; set; }

        public string ReferringMemberName { get; set; }

        public string SalesPersonId { get; set; }

        public string SalesPersonName { get; set; }

        public string SalesPersonHomeClub { get; set; }

        public string PaymentPlan { get; set; }
        
        public Term? Term { get; set; }
        
        public string PaymentFrequency { get; set; }//TODO change to enum

        public string MembershipType { get; set; }

        public string ManagedType { get; set; }        

        public string CampaignId { get; set; }

        public string CampaignName { get; set; }

        public string CampaignGroup { get; set; }

        public bool? IsPastDue { get; set; }

        public string DownPaymentPendingPos { get; set; }

        public string RenewalType { get; set; }//TODO change to enum

        public string AgreementPaymentMethod { get; set; }

        public decimal? DownPayment { get; set; }

        public decimal? NextDueAmount { get; set; }

        public decimal? ProjectedDueAmount { get; set; }

        public decimal? PastDueBalance { get; set; }

        public decimal? LateFeeAmount { get; set; }

        public decimal? ServiceFeeAmount { get; set; }

        public decimal? TotalPastDueBalance { get; set; }

        public decimal? ClubAccountPastDueBalance { get; set; }

        public string CurrentQueue { get; set; }

        public DateTime? QueueTimestamp { get; set; } 

        public string StationLocation { get; set; }

        public string AgreementEntrySource { get; set; }

        public string AgreementEntrySourceReportName { get; set; }

        public DateTime? SinceDate { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public DateTime? ConvertedDate { get; set; }

        public DateTime? LastRenewalDate { get; set; }

        public DateTime? LastRewriteDate { get; set; }
        
        public DateTime? RenewalDate { get; set; }

        public DateTime? FirstPaymentDate { get; set; }

        public DateTime? SignDate { get; set; }

        public DateTime? NextBillingDate { get; set; }

        //public PrimaryBillingAccountHolder PrimaryBillingAccountHolder { get; set; }
    }
}