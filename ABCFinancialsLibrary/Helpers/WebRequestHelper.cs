﻿using ABCFinancialsLibrary.Providers;
using System.Net;

namespace ABCFinancialsLibrary.Helpers
{
    public static class WebRequestHelper
    {
        private static readonly string _appId = ABCFinancialsConfigProvider.AppId;
        private static readonly string _appKey = ABCFinancialsConfigProvider.AppKey;
        private static readonly string _apiBaseUrl = "https://api.abcfinancial.com/rest/";
        private static readonly string _clubNumber = "12345";//Replace with real value
        public static string accessToken;

        public static HttpWebRequest GetApiWebRequest(string apiUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Headers["app_id"] = _appId;
            request.Headers["app_key"] = _appKey;
            request.Headers["access_token"] = accessToken;
            request.Accept = "application/json;charset=UTF-8";//xml alternative application/xml;charset=UTF-8
            return request;
        }

        public static string GetRouteBaseUrl(string apiRoute)
        {
            var url = $"{_apiBaseUrl}{_clubNumber}/{apiRoute}";
            return url;
        }
    }
}
