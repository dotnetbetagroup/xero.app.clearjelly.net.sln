﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ABCFinancialsLibrary.Helpers
{
    public static class RepositoryHelper
    {
        public static async Task<int> ClearTable(string table, IDbConnection connection, IDbTransaction transaction = null)
        {
            var query = $"DELETE FROM {table}";
            var result = await connection.ExecuteAsync(query, transaction);
            return result;
        }


        public static async Task BulkInsertList<T>(SqlConnection connection, List<T> clubs, string tableName)
        {
            using (SqlBulkCopy copy = new SqlBulkCopy(connection))
            {
                var table = ConvertItemToDataTable(clubs);
                copy.DestinationTableName = tableName;
                await copy.WriteToServerAsync(table);
            }
        }

        private static DataTable ConvertItemToDataTable<T>(List<T> types)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                table.Columns.Add(prop.Name, type);
            }

            foreach (var item in types)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var value = prop.GetValue(item);
                    row[prop.Name] = value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
