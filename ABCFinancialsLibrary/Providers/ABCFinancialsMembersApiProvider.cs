﻿using ABCFinancialsLibrary.Entities;
using ABCFinancialsLibrary.Helpers;
using ABCFinancialsLibrary.Models.Clubs;
using ABCFinancialsLibrary.Models.Members;
using ABCFinancialsLibrary.Models.Members.Checkins.Details;
using ABCFinancialsLibrary.Models.Members.Checkins.Summaries;
using ABCFinancialsLibrary.Models.Members.Children;
using ABCFinancialsLibrary.Models.Members.Groups;
using ABCFinancialsLibrary.Models.Members.Members;
using ABCFinancialsLibrary.Models.Members.Personals;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ABCFinancialsLibrary.Providers
{
    public class ABCFinancialsMembersApiProvider
    {
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd hh:mm:ss.ffffff";


        public async Task<List<MembersMemberModel>> GetMembers(DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/asd/members
                //full url example
                //https://api.abcfinancial.com/rest/asd/members?barcode=asd&activeStatus=active&memberStatus=cancelled&joinStatus=member&agreementNumber=asdasd&lastCheckInTimestampRange=adad&lastModifiedTimestampRange=adasd&createdTimestampRange=asdsad&convertedDateRange=asdad&memberSinceDateRange=asdasd&page=1&size=12
                var apiRoute = "members";
                var routeBaseUrl = WebRequestHelper.GetRouteBaseUrl(apiRoute);

                //check if correct propertie
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl = routeBaseUrl + $"?lastModifiedTimestampRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<MembersMemberModel> members;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersResponse>(resultString);
                    members = parsedResponse.Members;
                }
                return members;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MembersMemberModel> GetMembersByMemberId(string memberId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe3/members/qwe4
                //full url example
                //https://api.abcfinancial.com/rest/qwe3/members/qwe4?barcode=qe&activeStatus=active&memberStatus=active&joinStatus=prospect&agreementNumber=qwe&lastCheckInTimestampRange=qwe&lastModifiedTimestampRange=qwe&createdTimestampRange=qd&convertedDateRange=qwe&memberSinceDateRange=qwe&page=qwe&size=qwe
                var apiRoute = "members";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{memberId}";

                //check if correct propertie
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl = routeBaseUrl + $"?lastModifiedTimestampRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                MembersMemberModel member;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersResponse>(resultString);
                    member = parsedResponse.Members.FirstOrDefault();
                }
                return member;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<CheckinsDetailsMemberModel> GetMembersCheckinsDetailsByMemberId(string memberId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe3/members/checkins/details/qwe4?checkInTimestampRange=242344
                //full url example
                //https://api.abcfinancial.com/rest/qwe3/members/qwe4?barcode=qe&activeStatus=active&memberStatus=active&joinStatus=prospect&agreementNumber=qwe&lastCheckInTimestampRange=qwe&lastModifiedTimestampRange=qwe&createdTimestampRange=qd&convertedDateRange=qwe&memberSinceDateRange=qwe&page=qwe&size=qwe
                var apiRoute = "members/checkins/details";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{memberId}?checkInTimestampRange=";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddYears(-100).ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                CheckinsDetailsMemberModel member;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<DetailsResponse>(resultString);
                    member = parsedResponse.Members.FirstOrDefault();
                }
                return member;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<List<CheckinDetailsSummariesMemberModel>> GetMembersCheckinsSummaries(DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/members/checkins/summaries?checkInTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qwe/members/checkins/summaries?barcode=qe&checkInTimestampRange=qwe&activeStatus=active&hasCheckIn=true&page=qwe&size=qwe
                var apiRoute = "members/checkins/summaries";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}?checkInTimestampRange=";//fill timestamp values
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddYears(-100).ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                List<CheckinDetailsSummariesMemberModel> members;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<SummariesResponse>(resultString);
                    members = parsedResponse.Members;
                }
                return members;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<CheckinDetailsSummariesMemberModel> GetMembersCheckinsSummariesByMemberId(string memberId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qe/members/checkins/summaries/we?checkInTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qe/members/checkins/summaries/we?barcode=qweqe&checkInTimestampRange=qwe&activeStatus=inactive&hasCheckIn=false&page=2&size=12
                var apiRoute = "members/checkins/summaries";                
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{memberId}?checkInTimestampRange=";//fill timestamp values
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddYears(-100).ToString(_apiDateTimeFromat)}";
                }

                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                CheckinDetailsSummariesMemberModel member;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<SummariesResponse>(resultString);
                    member = parsedResponse.Members.FirstOrDefault();
                }
                return member;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MemberChildModel> GetMembersChildernByMemberChildId(string memberChildId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/33/members/children/44
                //full url example
                //https://api.abcfinancial.com/rest/33/members/children/44?barcode=2&activeStatus=inactive&agreementNumber=1&createdTimestampRange=2&page=2&size=3
                var apiRoute = "members/children";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{memberChildId}";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"?createdTimestampDateRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                MemberChildModel memberChild;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersChildrenResponse>(resultString);
                    memberChild = parsedResponse.MemberChildren.FirstOrDefault();
                }
                return memberChild;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        //full clear of table?
        public async Task<List<ClubGroup>> GetMembersGroups()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/members/groups
                //full url example
                //https://api.abcfinancial.com/rest/qwe/members/groups?activeStatus=inactive
                var apiRoute = "members/groups";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                List<ClubGroup> clubGroups;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<GroupsResponse>(resultString);
                    clubGroups = parsedResponse.Groups;
                }
                return clubGroups;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<List<PersonalsMemberModel>> GetMembersPersonals(DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/33/members/personals
                //full url example
                //https://api.abcfinancial.com/rest/33/members/personals?barcode=12&activeStatus=all&memberStatus=cancelled&joinStatus=prospect&agreementNumber=12&lastCheckInTimestampRange=12&lastModifiedTimestampRange=12&createdTimestampRange=12&convertedDateRange=12&memberSinceDateRange=12&page=12&size=12
                var apiRoute = "members/personals";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"?lastmodifiedTimestampRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                List<PersonalsMemberModel> personalsMembers;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersPersonalsResponse>(resultString);
                    personalsMembers = parsedResponse.Members;
                }
                return personalsMembers;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<PersonalsMemberModel> GetMembersPersonalsByMemberId(string memberId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/33/members/personals/23424
                //full url example
                //https://api.abcfinancial.com/rest/33/members/personals/23424?barcode=123&activeStatus=active&memberStatus=problem&joinStatus=member&agreementNumber=123&lastCheckInTimestampRange=123&lastModifiedTimestampRange=123&createdTimestampRange=123&convertedDateRange=123&memberSinceDateRange=123&page=123&size=123
                var apiRoute = "members/personals";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{memberId}";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"?lastmodifiedTimestampRange={startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();//check
                PersonalsMemberModel personalsMember;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResponse = JsonConvert.DeserializeObject<MembersPersonalsResponse>(resultString);
                    personalsMember = parsedResponse.Members.FirstOrDefault();
                }
                return personalsMember;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

