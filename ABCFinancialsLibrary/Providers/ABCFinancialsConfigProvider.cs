﻿using System.Configuration;

namespace ABCFinancialsLibrary.Providers
{
    public static class ABCFinancialsConfigProvider
    {
        public static string RedirectUri
        {
            get { return ConfigurationManager.AppSettings["redirectURI"]; }
        }
        public static string AppId
        {
            get { return ConfigurationManager.AppSettings["abc:AppId"]; }
        }
        public static string AppKey
        {
            get { return ConfigurationManager.AppSettings["abc:AppKey"]; }
        }
    }
}
