﻿using ABCFinancialsLibrary.Entities;
using ABCFinancialsLibrary.Helpers;
using ABCFinancialsLibrary.Models.Clubs;
using ABCFinancialsLibrary.Models.Clubs.Details;
using ABCFinancialsLibrary.Models.Clubs.MembersIdentities;
using ABCFinancialsLibrary.Models.Clubs.Plans;
using ABCFinancialsLibrary.Models.Clubs.Plans.AnnualFees;
using ABCFinancialsLibrary.Models.Clubs.Plans.BillingCatalogItems;
using ABCFinancialsLibrary.Models.Clubs.Plans.PaymentMethods;
using ABCFinancialsLibrary.Models.Clubs.Plans.Plan;
using ABCFinancialsLibrary.Models.Clubs.Stations;
using ABCFinancialsLibrary.Models.Clubs.TransactionsPos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ABCFinancialsLibrary.Providers
{
    public class ABCFinancialsClubsApiProvider
    {
        private readonly string _apiDateTimeFromat = "yyyy-MM-dd hh:mm:ss.ffffff";

        public ABCFinancialsClubsApiProvider()
        {
            
        }
        //clear full table 
        public async Task<ClubModel> GetClub()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/3/clubs
                var apiRoute = "clubs";
                var routeBaseUrl = WebRequestHelper.GetRouteBaseUrl(apiRoute);
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubModel club;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsResponse>(resultString);
                    club = parsedResonse.Club;
                }
                return club;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public async Task<List<ClubCheckinModel>> GetClubCheckinsDetails(DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qweq/clubs/checkins/details?checkInTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qweq/clubs/checkins/details?checkInTimestampRange=qwe&page=1&size=12
                var apiRoute = "clubs/checkins/details";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}?checkInTimestampRange=";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddYears(-100).ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(2).ToString(_apiDateTimeFromat)}";
                }
                //checkInTimestampRange
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<ClubCheckinModel> clubCheckins;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsCheckinsDetailsResponse>(resultString);
                    clubCheckins = parsedResonse.Checkins;
                }
                return clubCheckins;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<ClubMemberIdentity> GetClubMemberIdentitieByBarcode(string barcode)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qew/clubs/members/identities?barcode=qwe
                var apiRoute = "clubs/members/identities";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}?barcode={barcode}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubMemberIdentity clubMemberIdentity;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<MembersIdentitiesResponse>(resultString);
                    clubMemberIdentity = parsedResonse.Identities.FirstOrDefault();
                }
                return clubMemberIdentity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<ClubMemberIdentity> GetClubMemberIdentitieByMemberId(string memberId)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/members/qwe/identities
                var apiRoute = $"clubs/members/{memberId}/identities";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubMemberIdentity clubMemberIdentity;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<MembersIdentitiesResponse>(resultString);
                    clubMemberIdentity = parsedResonse.Identities.FirstOrDefault();
                }
                return clubMemberIdentity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //Quote: The time range being returned for point - of - sale transactions, limited to 180 days.If no end date is specified, a range of 180 days will be assumed.
        //TODO add check if dates range is more than 180 days if so split dates for multuple requests
        public async Task<List<ClubTransactionModel>> GetClubTransactionPos(string memberId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/wqe/clubs/transactions/pos?transactionTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/wqe/clubs/transactions/pos?transactionTimestampRange=qwe&memberId=qwe&page=qew&size=qwe
                var apiRoute = "clubs/transactions/pos";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}?transactionTimestampRange=";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddDays(-179).ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(1).ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<ClubTransactionModel> clubs;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsTransactionsPosResponse>(resultString);
                    clubs = parsedResonse.Clubs;
                }
                return clubs;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<ClubTransactionModel> GetClubTransactionPosByTransactionId(string transactionId, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe
                //full url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe&memberId=memb&page=12&size=13
                var apiRoute = "clubs/transactions/pos";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{transactionId}?transactionTimestampRange=";
                if (startDate.HasValue && endDate.HasValue)
                {
                    routeBaseUrl += $"{startDate.Value.ToString(_apiDateTimeFromat)},{endDate.Value.ToString(_apiDateTimeFromat)}";
                }
                else
                {
                    //check value
                    routeBaseUrl += $"{DateTime.Now.AddDays(-179).ToString(_apiDateTimeFromat)},{DateTime.Now.AddDays(1).ToString(_apiDateTimeFromat)}";
                }
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                ClubTransactionModel club;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsTransactionsPosResponse>(resultString);
                    club = parsedResonse.Clubs.FirstOrDefault();
                }
                return club;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<List<ClubPlan>> GetClubPlans()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/plans
                //full url example
                //https://api.abcfinancial.com/rest/qwe/clubs/transactions/pos/ID?transactionTimestampRange=qwe&memberId=memb&page=12&size=13
                var apiRoute = "clubs/plans";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<ClubPlan> clubPlans;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<PlansResponse>(resultString);
                    clubPlans = parsedResonse.Plans;
                }
                return clubPlans;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<List<ClubPlanAnnualFeeModel>> GetClubPlansAnnualFees()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qew/clubs/plans/annualfees
                var apiRoute = "clubs/plans/annualfees";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<ClubPlanAnnualFeeModel> clubPlansAnnualFees;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsPlansAnnualFeesResponse>(resultString);
                    clubPlansAnnualFees = parsedResonse.AnnualFees;
                }
                return clubPlansAnnualFees;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<List<BillingCatalogItemModel>> GetClubPlansBillingCatalogItems()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/plans/billingcatalogitems
                var apiRoute = "clubs/plans/billingcatalogitems";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<BillingCatalogItemModel> clubPlansAnnualFees;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsPlansBillingCatalogItemsResponse>(resultString);
                    clubPlansAnnualFees = parsedResonse.BillingCatalogItems;
                }
                return clubPlansAnnualFees;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //clear full table
        public async Task<PaymentMethodsModel> GetClubPlansPaymentMethods()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/plans/paymentmethods
                var apiRoute = "clubs/plans/paymentmethods";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                PaymentMethodsModel clubsPlansPaymentMethods;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsPlansPaymentMethodsResponse>(resultString);
                    clubsPlansPaymentMethods = parsedResonse.PaymentMethods;
                }
                return clubsPlansPaymentMethods;
            }
            catch (Exception)
            {
                return null;
            }
        }

        //clear full table
        public async Task<PaymentPlanModel> GetClubPlanById(string planId)
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/plans/Id               
                var apiRoute = "clubs/plans";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}/{planId}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                PaymentPlanModel clubPaymentPlan;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<PlanResponse>(resultString);
                    clubPaymentPlan = parsedResonse.PaymentPlan;
                }
                return clubPaymentPlan;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<List<ClubStation>> GetClubStations()
        {
            try
            {
                //minimal url example
                //https://api.abcfinancial.com/rest/qwe/clubs/stations              
                var apiRoute = "clubs/stations";
                var routeBaseUrl = $"{WebRequestHelper.GetRouteBaseUrl(apiRoute)}";
                var request = WebRequestHelper.GetApiWebRequest(routeBaseUrl);
                var httpResponse = (HttpWebResponse)await request.GetResponseAsync();
                List<ClubStation> clubPaymentPlan;
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO Update
                    return null;
                }
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var resultString = streamReader.ReadToEnd();
                    var parsedResonse = JsonConvert.DeserializeObject<ClubsStationsResponse>(resultString);
                    clubPaymentPlan = parsedResonse.Stations;
                }
                return clubPaymentPlan;
            }
            catch (Exception )
            {
                return null;
            }
        }
    }
}

