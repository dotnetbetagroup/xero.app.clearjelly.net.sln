﻿using ABCFinancialsLibrary.Helpers;
using ABCFinancialsLibrary.Providers;
using ABCFinancialsLibrary.Repositories;
using NLog;
using System;
using System.Threading.Tasks;

namespace ABCFinancialsLibrary
{
    public class ABCFinancialsService
    {
        private readonly ABCFinancialsClubsRepository _clubsRepository;
        private readonly ABCFinancialsMembersRepository _membersRepository;
        //private readonly ABCFinancialsClubsApiProvider _clubsApiProvider;
        //private readonly ABCFinancialsMembersApiProvider _membersApiProvider;
        private readonly ABCFinancialsOAuthProvider _oAuthProvider;
        private readonly Logger _logger;
        private readonly string _dbName;
        //token to constructor?
        public ABCFinancialsService(string dbName)
        {
            _dbName = dbName;
            var connectionString = $"{System.Configuration.ConfigurationManager.ConnectionStrings["CompanyDBConnection"].ConnectionString};database=ABC_Financial{dbName};";
            _logger = LogManager.GetCurrentClassLogger();
            _clubsRepository = new ABCFinancialsClubsRepository(connectionString);
            _membersRepository = new ABCFinancialsMembersRepository(connectionString);
            //_clubsApiProvider = new ABCFinancialsClubsApiProvider();
            //_membersApiProvider = new ABCFinancialsMembersApiProvider();
            _oAuthProvider = new ABCFinancialsOAuthProvider();
        }

        public string GetLoginUrl()
        {
            return _oAuthProvider.GetLoginUrl();
        }

        public async Task GetToken(string authorizationCode)
        {
            await _oAuthProvider.GetToken(authorizationCode);
        }

        public async Task UpdateData(DateTime startDate, DateTime endDate)
        {
            var refreshToken = "Todo";
            var tokenResponse =  await _oAuthProvider.RefreshToken(refreshToken);
            //todo response check
            WebRequestHelper.accessToken = tokenResponse.access_token;
            var updateClubsTask = UpdateClubsData(startDate, endDate);
            var updateMembersTask = UpdateMembersData(startDate, endDate);
            await updateClubsTask;
            await updateMembersTask;
        }

        private async Task UpdateClubsData(DateTime startDate, DateTime endDate)
        {

            var _clubsApiProvider = new ABCFinancialsClubsApiProvider();
            var clubResult = _clubsApiProvider.GetClub();
            var clubCheckinDetailsResult = _clubsApiProvider.GetClubCheckinsDetails(startDate, endDate);
            //_clubsApiProvider.GetClubMemberIdentitieByBarcode(); missing barcode
            //_clubsApiProvider.GetClubMemberIdentitieByMemberId(); missing id
            //_clubsApiProvider.GetClubPlanById(); missing id
            var clubPlansResult = _clubsApiProvider.GetClubPlans();
            var clubAnnualFeesResult = _clubsApiProvider.GetClubPlansAnnualFees();
            var clubPlansBillingCatalogItemsResult = _clubsApiProvider.GetClubPlansBillingCatalogItems();
            var clubPlansPaymentMethodsResult = _clubsApiProvider.GetClubPlansPaymentMethods();
            var clubStationsResult = _clubsApiProvider.GetClubStations();
            //_clubsApiProvider.GetClubTransactionPos(); missing member id
            //_clubsApiProvider.GetClubTransactionPosByTransactionId(); Missing transaction id
            //var club = await clubResult;
            //if (club != null) {
            //    await _clubsRepository.UpdateClub(club);
            //}
            //var checkinDetails = await clubCheckinDetailsResult;
            //if (checkinDetails != null && checkinDetails.Count > 0) {
            //    await _clubsRepository.UpdateCheckinDetails(checkinDetails);
            //}

            //todo get values
            //todo clear table
            //todo add values
            //transaction?
        }

        private async Task UpdateMembersData(DateTime startDate, DateTime endDate)
        {
            //todo get values
            //todo clear table
            //todo add values
            //transaction?
        }
    }
}

