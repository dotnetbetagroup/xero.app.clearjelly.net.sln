﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Members.Children
{
    public class MembersChildrenResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public MembersChildrenRequest Request { get; set; }

        [JsonProperty("memberChildren")]
        public List<MemberChildModel> MemberChildren { get; set; }
    }
}
