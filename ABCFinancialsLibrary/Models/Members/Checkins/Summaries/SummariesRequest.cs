﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Members.Checkins.Summaries
{
    public class SummariesRequest
    {
        [JsonProperty("checkInTimestampRange")]
        public string CheckInTimestampRange { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("page")]
        public string Page { get; set; }

        [JsonProperty("activeStatus")]
        public string ActiveStatus { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("hasCheckIn")]
        public string HasCheckIn { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }
    }
}
