﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Members.Checkins.Details
{
    public class DetailsRequest
    {
        [JsonProperty("checkInTimestampRange")]
        public string CheckInTimestampRange { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("page")]
        public string Page { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }
    }
}
