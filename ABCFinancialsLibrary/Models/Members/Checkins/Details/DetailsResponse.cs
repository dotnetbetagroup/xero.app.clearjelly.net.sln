﻿using ABCFinancialsLibrary.Entities;
using ABCFinancialsLibrary.Models.Clubs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Members.Checkins.Details
{
    public class DetailsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public DetailsRequest Request { get; set; }

        [JsonProperty("members")]
        public List<CheckinsDetailsMemberModel> Members { get; set; }
    }
}
