﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Members
{
    public class MemberChildModel
    {
        [JsonProperty("memberChildId")]
        public string MemberChildId { get; set; }

        [JsonProperty("activeStatus")]
        public string ActiveStatus { get; set; }

        [JsonProperty("homeClub")]
        public int? HomeClub { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("middleInitial")]
        public string MiddleInitial { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("birthDate")]
        public DateTime? BirthDate { get; set; }

        [JsonProperty("primaryMemberId")]
        public string PrimaryMemberId { get; set; }

        [JsonProperty("agreementNumber")]
        public string AgreementNumber { get; set; }

        [JsonProperty("memberChildEmergencyContact")]
        public MemberChildEmergencyContact MemberChildEmergencyContact { get; set; }

        [JsonProperty("childMisc1")]
        public string ChildMisc1 { get; set; }

        [JsonProperty("childMisc2")]
        public string ChildMisc2 { get; set; }

        [JsonProperty("childMisc3")]
        public string ChildMisc3 { get; set; }

        [JsonProperty("notes")]
        public List<Note> Notes { get; set; }

        [JsonProperty("createTimestamp")]
        public DateTime? CreateTimestamp { get; set; }

        [JsonProperty("lastModifiedTimestamp")]
        public DateTime? LastModifiedTimestamp { get; set; }

        [JsonProperty("lastCheckInTimestamp")]
        public DateTime? LastCheckInTimestamp { get; set; }
    }
}
//{
//      "memberChildId": "010aee3beb4e4ed5a3682d6276095d15",
//      "activeStatus": "active",
//      "homeClub": "9003",
//      "firstName": "Mitch",
//      "middleInitial": "H",
//      "lastName": "Conner",
//      "barcode": "barcode1",
//      "gender": "Male",
//      "birthDate": "1997-08-13",
//      "primaryMemberId": "010aee3beb4e4ed5a3682d6276095d15",
//      "agreementNumber": "07488",
//      "memberChildEmergencyContact"://nested,
//      "childMisc1": "Miscellaneous information",
//      "childMisc2": "Miscellaneous information",
//      "childMisc3": "Miscellaneous information",
//      "notes"://nested,
//      "createTimestamp": "2016-12-28 08:00:00.000000",
//      "lastModifiedTimestamp": "2016-12-28 08:00:00.000000",
//      "lastCheckInTimestamp": "2016-12-28 08:00:00.000000"
//    }