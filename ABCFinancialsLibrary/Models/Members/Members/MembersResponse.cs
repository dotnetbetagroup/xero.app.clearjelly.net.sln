﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Members.Members
{
    public class MembersResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public MembersRequest Request { get; set; }

        [JsonProperty("members")]
        public List<MembersMemberModel> Members { get; set; }
    }
}
