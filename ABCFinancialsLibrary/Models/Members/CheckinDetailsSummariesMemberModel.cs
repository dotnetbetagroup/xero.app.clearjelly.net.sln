﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Members
{
    public class CheckinDetailsSummariesMemberModel
    {
        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("links")]
        public string Links { get; set; }

        [JsonProperty("checkInCounts")]
        public CheckinCountsModel CheckInCounts { get; set; }
    }
}
