﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Members.Groups
{
    public class GroupsRequest
    {
        [JsonProperty("activeStatus")]
        public string ActiveStatus { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
