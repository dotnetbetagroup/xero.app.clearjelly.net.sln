﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Members.Groups
{
    public class GroupsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public GroupsRequest Request { get; set; }

        [JsonProperty("groups")]
        public List<ClubGroup> Groups { get; set; }
    }
}
