﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Members
{
    public class PersonalsMemberModel
    {
        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("personal")]
        public Personal Personal { get; set; }
    }
}
