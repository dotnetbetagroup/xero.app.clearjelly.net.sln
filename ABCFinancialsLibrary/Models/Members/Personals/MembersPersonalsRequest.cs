﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Members.Personals
{
    public class MembersPersonalsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("page")]
        public string Page { get; set; }

        [JsonProperty("joinStatus")]
        public string JoinStatus { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("activeStatus")]
        public string ActiveStatus { get; set; }

        [JsonProperty("agreementNumber")]
        public string AgreementNumber { get; set; }

        [JsonProperty("memberStatus")]
        public string MemberStatus { get; set; }

        [JsonProperty("lastCheckInTimestampRange")]
        public string LastCheckInTimestampRange { get; set; }

        [JsonProperty("lastmodifiedTimestampRange")]
        public string LastmodifiedTimestampRange { get; set; }

        [JsonProperty("createdTimestampDateRange")]
        public string CreatedTimestampDateRange { get; set; }

        [JsonProperty("convertedDateRange")]
        public string ConvertedDateRange { get; set; }

        [JsonProperty("memberSinceDateRange")]
        public string MemberSinceDateRange { get; set; }
    }
}
