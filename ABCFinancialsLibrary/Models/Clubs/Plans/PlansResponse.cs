﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs.Plans
{
    public class PlansResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public PlansRequest Request { get; set; }

        [JsonProperty("plans")]
        public List<ClubPlan> Plans { get; set; }
    }
}
