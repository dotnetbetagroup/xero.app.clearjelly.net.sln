﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.AnnualFees
{
    public class ClubsPlansAnnualFeesRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
