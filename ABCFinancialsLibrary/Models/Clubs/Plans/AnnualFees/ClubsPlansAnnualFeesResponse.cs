﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.AnnualFees
{
    public class ClubsPlansAnnualFeesResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsPlansAnnualFeesRequest Request { get; set; }

        [JsonProperty("annualFees")]
        public List<ClubPlanAnnualFeeModel> AnnualFees { get; set; }

    }
}
