﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans
{
    public class PlansRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("onlyPromoPlans")]
        public string OnlyPromoPlans { get; set; }

        [JsonProperty("inClubPlans")]
        public string InClubPlans { get; set; }
    }
}
