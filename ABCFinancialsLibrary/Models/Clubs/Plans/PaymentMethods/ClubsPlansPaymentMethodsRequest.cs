﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.PaymentMethods
{
    public class ClubsPlansPaymentMethodsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
