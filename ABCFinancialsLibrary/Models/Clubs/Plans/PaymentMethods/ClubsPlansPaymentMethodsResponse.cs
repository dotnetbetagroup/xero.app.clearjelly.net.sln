﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.PaymentMethods
{
    public class ClubsPlansPaymentMethodsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsPlansPaymentMethodsRequest Request { get; set; }

        [JsonProperty("paymentMethods")]
        public PaymentMethodsModel PaymentMethods { get; set; }
    }
}
