﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.BillingCatalogItems
{
    public class ClubsPlansBillingCatalogItemsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
