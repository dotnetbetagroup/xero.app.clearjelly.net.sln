﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.Plan
{
    public class PlanRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("planId")]
        public string PlanId { get; set; }
    }
}
