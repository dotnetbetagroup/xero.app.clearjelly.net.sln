﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Plans.Plan
{
    public class PlanResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public PlanRequest Request { get; set; }

        [JsonProperty("paymentPlan")]
        public PaymentPlanModel PaymentPlan { get; set; }
    }
}
