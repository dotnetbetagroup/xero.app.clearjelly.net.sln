﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public partial class ClubsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsRequest Request { get; set; }

        [JsonProperty("club")]
        public ClubModel Club { get; set; }
    }

}
