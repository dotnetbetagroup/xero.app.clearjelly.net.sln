﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public class ClubsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
