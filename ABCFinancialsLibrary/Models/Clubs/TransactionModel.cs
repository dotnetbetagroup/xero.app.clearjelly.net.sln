﻿using Newtonsoft.Json;
using System;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public class TransactionModel
    {
        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }

        [JsonProperty("transactionTimestamp")]
        public DateTime? TransactionTimestamp { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("homeClub")]
        public int HomeClub { get; set; }

        [JsonProperty("employeeId")]
        public string EmployeeId { get; set; }

        [JsonProperty("receiptNumber")]
        public string ReceiptNumber { get; set; }

        [JsonProperty("stationName")]
        public string StationName { get; set; }

        [JsonProperty("return")]
        public string Return { get; set; }

        [JsonProperty("items")]
        public TransactionItems Items { get; set; }
    }
}
