﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public class PaymentMethodsModel
    {
        [JsonProperty("clubAccountPaymentMethods")]
        public List<PaymentMethod> ClubAccountPaymentMethods { get; set; }

        [JsonProperty("abcBillingPaymentMethods")]
        public List<PaymentMethod> AbcBillingPaymentMethods { get; set; }
    }
}
