﻿using Newtonsoft.Json;

namespace ABCFinancialApp.Models.Clubs.Clubs
{
    public partial class ClubsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsRequest Request { get; set; }

        [JsonProperty("club")]
        public ClubModel Club { get; set; }
    }

}
