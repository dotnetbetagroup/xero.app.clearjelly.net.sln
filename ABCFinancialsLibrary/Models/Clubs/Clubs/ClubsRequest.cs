﻿using Newtonsoft.Json;

namespace ABCFinancialApp.Models.Clubs.Clubs
{
    public class ClubsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
