﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public class CheckinsDetailsMemberModel
    {
        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("checkins")]
        public List<CheckinsDetailsMemberCheckin> Checkins { get; set; }
    }
}
