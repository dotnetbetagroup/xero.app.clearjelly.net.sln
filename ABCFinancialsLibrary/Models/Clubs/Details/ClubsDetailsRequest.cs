﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Details
{
    public class ClubsDetailsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("page")]
        public string Page { get; set; }

        [JsonProperty("checkInTimestampRange")]
        public string CheckInTimestampRange { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }
    }
}
