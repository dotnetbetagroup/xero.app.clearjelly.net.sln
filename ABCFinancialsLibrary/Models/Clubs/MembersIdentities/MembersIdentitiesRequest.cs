﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.MembersIdentities
{
    public class MembersIdentitiesRequest
    {
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
