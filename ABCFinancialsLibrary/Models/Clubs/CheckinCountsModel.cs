﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Entities
{
    public class CheckinCountsModel
    {
        [JsonProperty("checkInCount")]
        public List<CheckinCount> CheckInCount { get; set; }
    }
}