﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public class TransactionItems
    {
        [JsonProperty("item")]
        public List<TransactionItemModel> Item { get; set; }
    }
}
