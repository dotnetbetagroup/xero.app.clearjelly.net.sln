﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.Stations
{
    public class ClubsStationsRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }
    }
}
