﻿using ABCFinancialsLibrary.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs.Stations
{
    public class ClubsStationsResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsStationsRequest Request { get; set; }

        [JsonProperty("stations")]
        public List<ClubStation> Stations { get; set; }
    }
}
