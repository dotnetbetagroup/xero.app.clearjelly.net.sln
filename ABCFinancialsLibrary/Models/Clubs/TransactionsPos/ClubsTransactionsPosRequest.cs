﻿using Newtonsoft.Json;

namespace ABCFinancialsLibrary.Models.Clubs.TransactionsPos
{
    public class ClubsTransactionsPosRequest
    {
        [JsonProperty("clubNumber")]
        public string ClubNumber { get; set; }

        [JsonProperty("page")]
        public string Page { get; set; }

        [JsonProperty("transactionTimestampRange")]
        public string TransactionTimestampRange { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }
    }
}
