﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs.TransactionsPos
{
    public partial class ClubsTransactionsPosResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("request")]
        public ClubsTransactionsPosRequest Request { get; set; }

        [JsonProperty("clubs")]
        public List<ClubTransactionModel> Clubs { get; set; }
    }
}
