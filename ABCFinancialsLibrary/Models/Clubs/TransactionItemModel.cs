﻿using ABCFinancialsLibrary.Entities;
using ABCFinancialsLibrary.Entities.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ABCFinancialsLibrary.Models.Clubs
{
    public class TransactionItemModel
    {
        [JsonProperty("itemId")]
        public string ItemId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("inventoryType")]
        public InventoryType? InventoryType { get; set; }

        [JsonProperty("sale")]
        public bool? Sale { get; set; }

        [JsonProperty("upc")]
        public string Upc { get; set; }

        [JsonProperty("profitCenter")]
        public string ProfitCenter { get; set; }

        [JsonProperty("catalog")]
        public CatalogType? Catalog { get; set; }

        [JsonProperty("unitPrice")]
        public string UnitPrice { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("packageQuantity")]
        public string PackageQuantity { get; set; }

        [JsonProperty("subtotal")]
        public string Subtotal { get; set; }

        [JsonProperty("tax")]
        public string Tax { get; set; }

        [JsonProperty("recurringServiceId")]
        public string RecurringServiceId { get; set; }

        [JsonProperty("payments")]
        public List<Payment> Payments { get; set; }
    }
}
