﻿using System.ComponentModel;

namespace ClearJelly.XeroApp
{
    public enum StatusEnum
    {
        [Description("Running")]
        Running = 1,
        [Description("Success")]
        Success = 2,
        [Description("Fail")]
        Fail = 3,
    }

    public enum ModelType
    {
        [Description("Create")]
        Create = 1,
        [Description("Update")]
        Update = 2
    }

    public enum UserFilter
    {
        [Description("Active users")]
        ActiveUsers = 1,

        [Description("Active subscription")]
        ActiveSubscriptions = 2,

        [Description("Non-active users")]
        NonActiveUsers = 3,

        [Description("blocked users")]
        blockedUsers = 4,

        [Description("Suspended users")]
        Suspend = 5
    }


    public enum ModelFilter
    {
        [Description("User")]
        User = 1,

        [Description("Organisation")]
        Organisation = 2,
    }

    public enum UserStatus
    {
        [Description("Active")]
        Active = 1,
        [Description("Suspended")]
        Suspended = 2,
        [Description("Cancelled")]
        Cancelled = 3,
        [Description("Expired")]
        Expired = 4
    }

    public enum PaidOrTial
    {
        [Description("Paid")]
        Paid = 1,
        [Description("Trial")]
        Trial = 2,
    }


    public enum SettingEnum
    {
        [Description("Certificate")]
        Certificate = 1,
        [Description("Maintenance")]
        Maintenance = 2
    }

    public enum SaasuOrXero
    {
        [Description("Saasu")]
        Saasu = 1,
        [Description("Xero")]
        Xero = 2
    }


    public enum EmailTemplates
    {
        [Description("Registration Success Email")]
        RegistrationSuccessEmail = 1,
        [Description("New User Registered Email to Admin")]
        RegistrationSuccessEmailToAdmin = 2,
        [Description("Forgot Password Email")]
        ForgotPasswordEmail = 3,
        [Description("Clearjelly model not built")]
        ClearjellyModelBuiltFailed = 4,
        [Description("Clearjelly model has been built")]
        ClearjellyModelBuiltSuccess = 5,
        [Description("SendMailOn Xerp Api Exception")]
        SendMailOnApiException = 6,
        [Description("Quick Guide")]
        quickGuide = 7,
        [Description("PaymentSuccessEmail")]
        PaymentSuccessEmail = 9,
        [Description("Error Xero Email")]
        ErrorXeroEmail = 10,
        [Description("Database Error")]
        DatabaseError = 11,
        [Description("Jedox Error")]
        JedoxError = 17,
        [Description("Suspend User Max Failure")]
        SuspendUserMaxFailure = 19,

        [Description("Payment Failure")]
        PaymentFailure = 20
    }


    public enum XeroErrorCodes
    {
        [Description("The load for the {0} to {1} of your Xero data has finished successfully.")]
        Ok = 200,

        [Description("There is some problem while connecting to xero please contact Admin.")]
        BadRequest = 400,

        [Description("Error while connecting Xero : Organization is unauthorized or not available please contact Admin.")]
        Unauthorized = 401,

        [Description("Error while connecting Xero : Some error occurred please contact Admin.")]
        //[Description("The client SSL certificate was not valid. This indicates the Xero Entrust certificate required for partner API applications is not being supplied in the connection, or has expired")]
        Forbidden = 403,

        [Description("Error while connecting Xero : The feature you are trying is not available in xero please contact Admin.")]
        NotFound = 404,

        [Description("Error while connecting Xero : Some internal error occurred please contact Admin.")]
        InternalError = 500,

        [Description("Error from Xero : The feature you are trying is not available in xero please contact Admin.")]
        NotImplemented = 501,

        [Description("Error from Xero : The feature you are trying is not available in xero please contact Admin.")]
        NotAvailable = 503,

        [Description("Error from Xero : Token is expired or not active please contact Admin.")]
        GeneralError = 555,

    }


    public enum PaypalPaymentStatus
    {
        canceled = 1,
        created = 2,
        completed = 3,
        incomplete = 4,
        error = 5,
        reversalerror = 6,
        processing = 7,
        pending = 8,
        failed = 9,
        declined = 10,
        canceled_reversal = 11,
        expired = 12,
        partially_refunded = 14,
        processed = 15,
        refunded = 16,
        reversed = 17,
        voided = 18,
    }


    public enum PaymentCycle
    {
        Daily = 1,
        Weekly = 2,
        SemiMonthly = 3,
        Monthly = 4,
        Yearly = 5
    }


    public enum PaymentType
    {
        Recurring = 1,
        Instant = 2
    }

    public enum EnumAccountMapping
    {
        CompositeKey = 0,
        Mapping1 = 1,
        Mapping2 = 2,
        Mapping3 = 3,
        Mapping4 = 4,
        Mapping5 = 5,
        Mapping6 = 6,
        Mapping7 = 7,
        Mapping8 = 8,
        Mapping9 = 9,
        Mapping10 = 10,
    }



    public enum PaypalTxnCode
    {
        [Description("Failed")]
        recurring_payment_failed = 1,

        [Description("Maximum Failed")]
        recurring_payment_suspended_due_to_max_failed_payment = 2,

        [Description("Recurring Profile Created")]
        recurring_payment_profile_created = 3,

        [Description("Recurring Payment Cancel")]
        recurring_payment_profile_cancel = 4,

        [Description("Recurring Payment Suspended")]
        recurring_payment_suspended = 5,

        [Description("Recurring Payment Expired")]
        recurring_payment_expired = 6,
    }

    public enum StatusCodes
    {
        ACTIVE = 1,
        ARCHIVED = 2
    }

    public enum XEROUserRoles
    {
        READONLY = 0,
        INVOICEONLY = 1,
        STANDARD = 2,
        FINANCIALADVISER = 3,
        MANAGEDCLIENT = 4,
        CASHBOOKCLIENT = 5
    }

    public enum LineAmountTypes
    {
        Exclusive = 0,
        Inclusive = 1,
        NoTax = 2
    }

    public enum ReceiptStatusCodes
    {
        DRAFT = 0,
        SUBMITTED = 1,
        AUTHORISED = 2,
        DECLINED = 3
    }

    public enum PaymentStatusCodes
    {
        AUTHORISED = 0,
        DELETED = 1
    }

    public enum PaymentTypes
    {
        ACCRECPAYMENT = 0,
        ACCPAYPAYMENT = 1,
        ARCREDITPAYMENT = 2,
        APCREDITPAYMENT = 3,
        AROVERPAYMENTPAYMENT = 4,
        ARPREPAYMENTPAYMENT = 5,
        APPREPAYMENTPAYMENT = 6,
        APOVERPAYMENTPAYMENT = 7
    }

    public enum AccountTypes
    {
        BANK = 0,
        CURRENT = 1,
        CURRLIAB = 2,
        DEPRECIATN = 3,
        DIRECTCOSTS = 4,
        EQUITY = 5,
        EXPENSE = 6,
        FIXED = 7,
        INVENTORY = 8,
        LIABILITY = 9,
        NONCURRENT = 10,
        OTHERINCOME = 11,
        OVERHEADS = 12,
        PREPAYMENT = 13,
        REVENUE = 14,
        SALES = 15,
        TERMLIAB = 16,
        PAYGLIABILITY = 17,
        SUPERANNUATIONEXPENSE = 18,
        SUPERANNUATIONLIABILITY = 19,
        WAGESEXPENSE = 20
    }

    public enum BankAccountTypes
    {
        BANK = 0,
        CREDITCARD = 1,
        PAYPAL = 2
    }

    public enum TaxTypes
    {
        INPUT2 = 0,
        NONE = 1,
        ZERORATED = 2,
        OUTPUT2 = 3,
        GSTONIMPORTS = 4
    }

    public enum AccountClassTypes
    {
        ASSET = 0,
        EQUITY = 1,
        EXPENSE = 2,
        LIABILITY = 3,
        REVENUE = 4
    }

    public enum InvoiceTypes
    {
        ACCPAY = 0,
        ACCREC = 1
    }

    public enum InvoiceStatusCodes
    {
        DRAFT = 0,
        SUBMITTED = 1,
        AUTHORISED = 2
    }

    public enum CreditNoteTypes
    {
        ACCPAYCREDIT = 0,
        ACCRECCREDIT = 1
    }

    public enum ExpenseClaimStatusCodes
    {
        SUBMITTED = 0,
        AUTHORISED = 1,
        PAID = 2
    }

    public enum PrepaymentTypes
    {
        RECEIVE_PREPAYMENT = 0,
        SPEND_PREPAYMENT = 1
    }
}
