﻿using Xero.Api.Infrastructure.Interfaces;

namespace ClearJelly.XeroApp
{
    public interface IMvcAuthenticator
    {
        string GetRequestTokenAuthorizeUrl(string userId);
        IToken RetrieveAndStoreAccessToken(string userId, string tokenKey, string verfier, string organisationShortCode);
    }
}
