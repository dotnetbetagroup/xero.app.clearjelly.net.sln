﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
using DapperExtensions;
using ClearJelly.Entities;
using System.Threading.Tasks;
using Xero.Api.Core.Model;

namespace ClearJelly.XeroApp
{
    public class BaseRepository<T> where T : class
    {
        private string _connection;

        public BaseRepository(string connectionString)
        {
            _connection = connectionString;
        }


        protected SqlConnection DatabaseConnection
        {
            get
            {
                var cn = new SqlConnection(_connection);
                return cn;
            }
        }

        public void InsertReceiptLineItem(LineItem Lineitem)
        {
            try
            {
                var query = @"INSERT INTO Lineitems (Id, AccountCode, Description, DiscountRate, LineAmount,Quantity,TaxType,UnitAmount)
                        VALUES (@Id, @AccountCode, @Description,@DiscountRate,@LineAmount,@Quantity,@TaxType,@UnitAmount);";
                using (var con = new SqlConnection(_connection))
                {
                    var parameters = new
                    {
                        Id = Guid.NewGuid().ToString(),
                        AccountCode = Lineitem.AccountCode,
                        Description = Lineitem.Description,
                        DiscountRate = Lineitem.DiscountRate,
                        LineAmount = Lineitem.LineAmount,
                        Quantity = Lineitem.Quantity,
                        TaxType = Lineitem.TaxType,
                        UnitAmount = Lineitem.UnitAmount,
                    };
                    var result = con.ExecuteAsync(query, parameters);
                }
            }
            catch (Exception)
            {

            }
        }

        public OrganisationDetail GetUserOrg(string mail)
        {
            try
            {
                var query = @"Select * From [dbo].[OrganisationDetails]
                            LEFT JOIN [dbo].[User] ON [dbo].[OrganisationDetails].CompanyId = [dbo].[User].CompanyId 
                            Where [dbo].[User].Email = @mail";
                using (var con = new SqlConnection(_connection))
                {
                    var parameters = new
                    {
                        mail = mail
                    };
                    var result = con.Query<OrganisationDetail, ClearJelly.Entities.User, OrganisationDetail>(query,
                        (od, u) =>
                        {
                            od.CompanyId = u.CompanyId;
                            return od;
                        }, parameters, splitOn: "CompanyId").FirstOrDefault();
                    return result;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }

        public bool Update(T model)
        {
            using (var connection = DatabaseConnection)
            {

                return connection.Update(model);
            }
        }

        public T RetrieveSingle(Expression<Func<T, object>> filter, object value)
        {
            using (var connection = DatabaseConnection)
            {
                var predicate = Predicates.Field(filter, Operator.Eq, value);
                return connection.GetList<T>(predicate).SingleOrDefault();
            }
        }
        public T Get(string id)
        {
            using (var connection = DatabaseConnection)
            {
                return connection.Get<T>(id);

            }
        }

        public bool IsExist(string id)
        {
            using (var connection = DatabaseConnection)
            {
                return connection.Get<T>(id) != null;
            }
        }

        public IEnumerable<T> GetList()
        {
            using (var connection = DatabaseConnection)
            {
                return connection.GetList<T>();

            }
        }

        public bool Delete(string id)
        {
            using (var connection = DatabaseConnection)
            {
                var model = connection.Get<T>(id);
                return connection.Delete(model);
            }
        }

        protected bool Delete(T model)
        {
            using (var connection = DatabaseConnection)
            {
                return connection.Delete(model);
            }
        }

    }
}
