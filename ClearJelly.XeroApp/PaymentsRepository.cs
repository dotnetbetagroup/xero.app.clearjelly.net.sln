﻿using ClearJelly.ViewModels.UserMappingModels;
using Dapper;
using System;
using System.Data.SqlClient;

namespace ClearJelly.XeroApp
{
    public class PaymentsRepository
    {
        private string _connection;

        public PaymentsRepository(string connectionString)
        {
            _connection = connectionString;
        }

        public void InsertPayment(Payment payment)
        {
            try
            {
                var query = @"INSERT INTO Payments (Id, Date, CurrencyRate, Amount, Reference, IsReconciled, Status, PaymentType,
                            UpdatedDateUTC, OverpaymentId, XEROAccount_Id, Invoice_Id, CreditNote_Id, OrgShortCode, OrgName)
                        VALUES (@Id, @Date, @CurrencyRate,@Amount,@Reference,@IsReconciled,@Status,@PaymentType,@UpdatedDateUTC,
                        @OverpaymentId, @XEROAccount_Id, @Invoice_Id, @CreditNote_Id, @OrgShortCode, @OrgName);";
                using (var con = new SqlConnection(_connection))
                {
                    var parameters = new
                    {
                        Id = Guid.NewGuid().ToString(),
                        Date = payment.Date,
                        CurrencyRate = payment.CurrencyRate,
                        Amount = payment.Amount,
                        Reference = payment.Reference,
                        IsReconciled = payment.IsReconciled,
                        Status = payment.Status,
                        PaymentType = payment.PaymentType,
                        UpdatedDateUTC = payment.UpdatedDateUTC,
                        OverpaymentId = payment.OverpaymentId,
                        XEROAccount_Id = payment.XEROAccountId,
                        Invoice_Id = payment.InvoiceId,
                        CreditNote_Id = payment.CreditNoteId,
                        OrgShortCode = payment.OrgShortCode,
                        OrgName = payment.OrgName
                    };
                    var result = con.Execute(query, parameters);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
