﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using NLog;
using ClearJelly.Entities;

namespace ClearJelly.XeroApp
{
    public static class XeroService
    {
        private static Logger _logger = LogManager.GetLogger("CommonXero");
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();

        public static List<int> GetSubscriptionIds(string currentUser)
        {
            try
            {
                _logger.Info(currentUser + " ~Execution of Common GetSubscriptionIds method started.");
                var businessPremium = "business user premium";
                var AdvisorPremium = "advisor premium";
                var subscriptionIds = _clearJellyContext.SubscriptionTypes.Where(x => x.TypeName.Trim().ToLower().Contains(businessPremium) || x.TypeName.Trim().ToLower().Contains(AdvisorPremium)).Select(x => x.SubscriptionTypeId).ToList();
                var newids = string.Join(" ,", subscriptionIds.ToArray());
                _logger.Info(currentUser + " ~Execution of Common GetSubscriptionIds method started.ids " + newids);
                return subscriptionIds;
            }
            catch (Exception ex)
            {
                _logger.Error(currentUser + " ~Execution of Common GetSubscriptionIds method completed.", ex);
                return null;
            }
        }

        public static bool CheckPremium(string email, List<int> subscriptionIds)
        {
            var userdetails = GetCurrentLoggedInUser(email);
            var res = _clearJellyContext.CompanySubscriptions.Any(x => x.CompanyId == userdetails.CompanyId && x.IsActive == true && (x.EndDate > DateTime.Now || x.EndDate == null) && subscriptionIds.Contains(x.SubscriptionTypeId));
            return res;
        }

        public static User GetCurrentLoggedInUser(string loggedInUser)
        {
            try
            {
                _logger.Info(loggedInUser + " ~Execution of Common SaveSubscription method started.");
                var currentUser = loggedInUser;
                var user = _clearJellyContext.Users.First(a => a.Email == currentUser && a.IsActive);
                return user;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedInUser + " ~Execution of Common SaveSubscription method completed.", ex);
                return null;
            }
        }

        public static string GetCompanyNameByUserId(string email)
        {
            try
            {
                _logger.Info(email + "Execution of Common GetCompanyNameByUserId event started.");
                var companyName = string.Empty;
                var userDetail = _clearJellyContext.Users.FirstOrDefault(a => a.Email == email);
                if (userDetail != null)
                {
                    companyName = userDetail.Company.Name;
                }
                return companyName;
            }
            catch (Exception ex)
            {
                _logger.Error(email + "Execution of Common GetCompanyNameByUserId event completed. :", ex);
                throw;
            }
        }

        public static string GetEmailAttachmentFrom()
        {
            var templateAttachmentFolder = ConfigurationManager.AppSettings["EmailTemplateAttachmentFolder"];
            if (templateAttachmentFolder == null)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :EmailTemplateAttachment is not defined.");
                return null;
            }
            return templateAttachmentFolder;
        }

    }
}
