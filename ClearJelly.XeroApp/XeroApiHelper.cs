﻿using ClearJelly.Configuration;
using System;
using Xero.Api.Core;
using Xero.Api.Core.Model;
using Xero.Api.Example.Applications.Public;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;
using Xero.Api.Example.TokenStores;
using System.Linq;

namespace ClearJelly.XeroApp
{
    public class ApplicationSettings
    {
        public string BaseApiUrl { get; set; }
        public Consumer Consumer { get; set; }
        public object Authenticator { get; set; }
    }

    public static class XeroApiHelper
    {
        private static ApplicationSettings _applicationSettings;
        private static String uName;
        private static NLog.Logger _logger;

        static XeroApiHelper()
        {
            GetAutentification();
        }

        public static void GetAutentification()
        {
            try
            {
                _logger = NLog.LogManager.GetCurrentClassLogger();
                _logger.Info(uName + " ~Execution of XeroApiHelper started.");                
                var sqlStore = new SqlAccessTokenStore();
                var requestTokenStore = new MemoryRequestTokenStore();
                //var requestTokenStore = new SqlRequestTokenStore();
                //var basePartnerApiUrl = "https://api-partner.network.xero.com";
                
                // Partner Application Settings
                var partnerConsumer = new Consumer(ConfigSection.PartnerConsumerKey, ConfigSection.PartnerConsumerSecret);
                try
                {
                    var partnerAuthenticator = new PartnerMvcAuthenticator(ConfigSection.BaseApiUrl, ConfigSection.BaseApiUrl, ConfigSection.CallbackUrl,
                        sqlStore, ConfigSection.SigningCertificatePath, partnerConsumer, requestTokenStore, ConfigSection.SigningCertificatePassword);
                    var partnerApplicationSettings = new ApplicationSettings
                    {
                        BaseApiUrl = ConfigSection.BaseApiUrl,
                        Consumer = partnerConsumer,
                        Authenticator = partnerAuthenticator
                    };
                    // Pick one
                    // Choose what sort of application is appropriate. Comment out the above code (Partner Application Settings/Public Application Settings) that are not used.              
                    _applicationSettings = partnerApplicationSettings;
                }
                catch (Exception ex)
                {

                }                           
            }
            catch (Exception ex)
            {
                _logger.Error((uName ?? "") + "Execution of  XeroApiHelper Completed." + ex.Message, ex);
            }
        }

        public static ApiUser User()
        {
            try
            {
                _logger.Info(uName + " ~Execution of XeroApiHelper User started.");
                var user = new ApiUser { Name = uName };
                return user;
            }
            catch (Exception ex)
            {
                _logger.Error((uName ?? "") + "Execution of  XeroApiHelper User Completed." + ex.Message, ex);
                throw;
            }
        }

        public static IConsumer Consumer()
        {
            try
            {
                _logger.Info(uName + " ~Execution of XeroApiHelper Consumer started.");
                return _applicationSettings.Consumer;
            }
            catch (Exception ex)
            {
                _logger.Error((uName ?? "") + "Execution of  XeroApiHelper Consumer Completed." + ex.Message, ex);
                throw;
            }

        }

        public static PartnerMvcAuthenticator MvcAuthenticator(String inUserName)
        {
            try
            {
                //GetAutentification();
                _logger.Info(uName + " ~Execution of XeroApiHelper MvcAuthenticator started.");
                uName = inUserName;
                return (PartnerMvcAuthenticator)_applicationSettings.Authenticator;
            }

            catch (Exception ex)
            {
                _logger.Error((uName ?? "") + "Execution of  XeroApiHelper MvcAuthenticator Completed." + ex.Message, ex);
                throw;
            }

        }

        public static XeroCoreApi CoreApi()
        {
            try
            {
                _logger.Info(uName + " ~Execution of XeroApiHelper CoreApi started.");

                if (_applicationSettings.Authenticator is IAuthenticator)
                {                   

                    var xero = new XeroCoreApi(_applicationSettings.BaseApiUrl, _applicationSettings.Authenticator as IAuthenticator,
                    _applicationSettings.Consumer, User(), new DefaultMapper(), new DefaultMapper());
                    return xero;
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error((uName ?? "") + "Execution of  XeroApiHelper CoreApi Completed." + ex.Message, ex);

                throw;
            }
        }
    }
}
