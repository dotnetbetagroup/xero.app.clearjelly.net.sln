﻿using ClearJelly.Configuration;
using ClearJelly.XeroApp.Repositories;
using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.XeroApp.Helpers
{
    public class XeroTablesGenerator
    {
        private readonly NLog.Logger _logger;
        private string _connection;
        private string _companyName;

        public XeroTablesGenerator(string companyName)
        {
            _companyName = companyName;
            _logger = LogManager.GetCurrentClassLogger();
            _connection = ConfigSection.XeroCompanyDbConnection + _companyName;
        }
        public async Task<bool> CreateQBDbAsync(string companyName)
        {
            var _metaDBConnection = ConfigSection.DefaultConnection;
            XeroRepository xeroRepository = new XeroRepository(_metaDBConnection);
            var createDB = xeroRepository.CreateXeroDatabaseAsync(companyName);
            await GenerateTables();
            //QBTablesGenerator
            return true;
        }
        public async Task GenerateTables()
        {
            var listTasks = new List<Task>();

            listTasks.Add(Task.Run(() => CreateAccnts_PnL_hierarchyTable()));
            listTasks.Add(Task.Run(() => CreateChartOfAccountsTable()));
            listTasks.Add(Task.Run(() => CreateContactsTable()));
            listTasks.Add(Task.Run(() => CreateContact_AddressTable()));
            listTasks.Add(Task.Run(() => CreateContact_GroupTable()));
            listTasks.Add(Task.Run(() => CreateCreditNotesDetailsTable()));
            listTasks.Add(Task.Run(() => CreateCreditNotesTable()));
            listTasks.Add(Task.Run(() => CreateInvoiceDetailsTable()));
            listTasks.Add(Task.Run(() => CreateInvoicesTable()));
            listTasks.Add(Task.Run(() => CreateJournalsTable()));
            listTasks.Add(Task.Run(() => CreateLineItemInReceiptTable()));
            listTasks.Add(Task.Run(() => CreateManualJournalsTable()));
            listTasks.Add(Task.Run(() => CreatePaymentsTable()));
            listTasks.Add(Task.Run(() => CreatePnLTable()));
            listTasks.Add(Task.Run(() => CreatePurchaseOrdersTable()));
            listTasks.Add(Task.Run(() => CreateReceiptLineItemsTable()));
            listTasks.Add(Task.Run(() => CreateReceiptsTable()));
            listTasks.Add(Task.Run(() => CreateTrackingCategoriesTable()));
            listTasks.Add(Task.Run(() => CreateUserPayrollsTable()));
            await Task.WhenAll(listTasks);
        }

        private void CreateAccnts_PnL_hierarchyTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info("_connection:   " + _connection);
                    _logger.Info($"Create QB Accnts_PnL_hierarchy table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Accnts_PnL_hierarchy](
                    [Row_Order][int] NULL, [ReportID] [varchar] (400) NULL,
 		        [ReportDate] datetime2 ,[UpdatedDateUTC] [varchar] (400) NULL,
 		        [ReportName] [varchar] (400) NULL,[ReportRowTitle] [varchar] (800) NULL,
 		        [ReportType] [varchar] (400) NULL,[Type_Level1] [varchar] (400) NULL,
 		        [Type_Level2] [varchar] (400) NULL,	[Name] [varchar] (800) NULL,
 		        [Value] [varchar] (800) NULL,[ValidationErrors] [varchar] (800) NULL,
 		        [ValidationStatus] [varchar] (80) NULL,[Warnings] [varchar] (800) NULL,
 		        [OrgShortCode] [varchar] (800) Not Null,[OrgName] [varchar] (800) Not Null) ON[PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Accnts_PnL_hierarchy table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateChartOfAccountsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero ChartOfAccounts table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[ChartOfAccounts](
                    [AccountID] [varchar](200) NULL,[BankAccountNumber] [varchar](200) NULL,
 		        [Class] [varchar](200) NULL, [Code] [varchar](200) NULL,
 		        [CurrencyCode] [varchar](200) NULL, [Description] [varchar](max) NULL,
 		        [EnablePaymentsToAccount] [varchar](200) NULL,[Name] [varchar](200) NULL,
 		        [ReportingCode] [varchar](200) NULL, [ReportingCodeName] [varchar](200) NULL,
 		        [ShowInExpenseClaims] [varchar](200) NULL, [Status] [varchar](200) NULL,
 		        [SystemAccount] [varchar](200) NULL, [Type] [varchar](200) NULL,
 		        [OrgShortCode] [varchar](800) Not Null, [OrgName] [varchar](800) Not Null
 	             ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero ChartOfAccounts table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateContact_AddressTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero Contact_Address table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Contact_Address](
                    [AddressID] [varchar](400) NULL,
 	            [ContactID] [varchar](800) NULL, [AddressLine1] [varchar](800) NULL,
 	            [AddressLine2] [varchar](800) NULL,[AddressLine3] [varchar](800) NULL,
 	            [AddressLine4] [varchar](800) NULL, [AttentionTo] [varchar](800) NULL,
 	            [Country] [varchar](800) NULL,[PostalCode] [varchar](50) NULL,
 	            [City] [varchar](800) NULL,	[Region] [varchar](800) NULL,
 	            [OrgShortCode] [varchar](800) Not Null, [OrgName] [varchar](800) Not Null ) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Contact_Address table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateContact_GroupTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create xero Contact_Group table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Contact_Group](
                  [ContactGroupID] [varchar](800) NULL,
 	            [GroupID] [varchar](100) NULL, 	[ContactID] [varchar](800) NULL,
 	            [Name] [varchar](800) NULL, 	[Status] [varchar](800) NULL,
 	            [OrgShortCode] [varchar](800) Not Null,	[OrgName] [varchar](800) Not Null ) ON [PRIMARY];	";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Contact_Group table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateContactsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {

                    _logger.Info($"Create Xero Contacts table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Contacts](
                   [ContactID] [varchar](800) NULL, [GroupCount] [varchar](800) NULL,
                 [ContactName] [varchar](800) NULL, [AccountsPayable_Outstanding] [varchar](800) NULL,
                 [AccountsPayable_Overdue] [varchar](800) NULL, [AccountsReceivable_Outstanding] [varchar](800) NULL,
                 [AccountsReceivable_Overdue] [varchar](800) NULL, [ContactStatus] [varchar](800) NULL,
                 [FirstName] [varchar](800) NULL, [LastName] [varchar](800) NULL,
                 [IsCustomer] [varchar](800) NULL, [IsSupplier] [varchar](800) NULL,
                 [PaymentTerms_Bills_Day] [varchar](800) NULL, [PaymentTerms_Bills_Type] [varchar](800) NULL,
                 [PaymentTerms_Sales_Day] [varchar](800) NULL, [PaymentTerms_Saяes_Type] [varchar](800) NULL,
                 [OrgShortCode] [varchar](800) Not Null, [OrgName] [varchar](800) Not Null) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Contacts table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateCreditNotesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero CreditNotes table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[CreditNotes](
                    [Allocations] [varchar](600) NULL,
                 [AppliedAmount] [varchar](600) NULL, [BrandingThemeID] [varchar](600) NULL,
                 [ContactID] [varchar](600) NULL, [CreditNoteID] [varchar](600) NULL,
                 [CreditNoteNumber] [varchar](600) NULL, [CurrencyCode] [varchar](600) NULL,
                 [CurrencyRate] DECIMAL(22,4) NULL, [Date] datetime2,
                 [DueDate] datetime2, [FullyPaidOnDate] datetime2,
                 [LineAmountTypes] [varchar](600) NULL, [UnitAmount] DECIMAL(22,4) NULL,
                 [Reference] [varchar](600) NULL, [RemainingCredit] DECIMAL(22,4) NULL,
                 [SentToContact] [varchar](600) NULL, [Status] [varchar](600) NULL,
                 [SubTotal] DECIMAL(22,4) NULL, [Total] DECIMAL(22,4) NULL,
                 [TotalTax] DECIMAL(22,4) NULL, [Type] [varchar](600) NULL,
                 [UpdatedDateUTC] datetime2, [OrgShortCode] [varchar](800) Not Null,
                 [OrgName] [varchar](800) Not Null) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero CreditNotes table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateCreditNotesDetailsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero CreditNotesDetails table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[CreditNotesDetails](
                    [InvoiceID] [varchar](600) NULL, [AccountCode] [varchar](600) NULL,
                 [Description] [varchar](600) NULL, [DiscountRate] DECIMAL(22,4) NULL,
                 [ItemCode] [varchar](600) NULL, [LineAmount] DECIMAL(22,4) NULL,
                 [Quantity] DECIMAL(22,4) NULL, [TaxAmount] DECIMAL(22,4) NULL,
                 [TaxType] [varchar](600) NULL, [TrackingID] [varchar](600) NULL,
                 [TrackingOption] [varchar](600) NULL, [Allocations] [varchar](600) NULL,
                 [AppliedAmount] [varchar](600) NULL, [BrandingThemeID] [varchar](600) NULL,
                 [ContactID] [varchar](600) NULL, [CreditNoteID] [varchar](600) NULL,
                 [CreditNoteNumber] [varchar](600) NULL, [CurrencyCode] [varchar](600) NULL,
                 [CurrencyRate] DECIMAL(22,4) NULL, [Date] datetime2,
                 [DueDate] datetime2, [FullyPaidOnDate] datetime2,
                 [LineAmountTypes] [varchar](600) NULL, [UnitAmount] DECIMAL(22,4) NULL,
                 [Reference] [varchar](600) NULL, [RemainingCredit] DECIMAL(22,4) NULL,
                 [SentToContact] [varchar](600) NULL, [Status] [varchar](600) NULL,
                 [SubTotal] DECIMAL(22,4) NULL, [Total] DECIMAL(22,4) NULL,
                 [TotalTax] DECIMAL(22,4) NULL, [Type] [varchar](600) NULL,
                 [UpdatedDateUTC] datetime2, [TrackingCategory1] varchar(800),
                 [TrackingCategory1_Option] varchar(800), [TrackingCategory2] varchar(800),
                 [TrackingCategory2_Option] varchar(800), [OrgShortCode] [varchar](800) Not Null,
                 [OrgName] [varchar](800) Not Null) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero CreditNotesDetails table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateInvoiceDetailsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero InvoiceDetails table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[InvoiceDetails](
                    [InvoiceID] [varchar](600) NULL, [InvoiceNumber] [varchar](400) NULL,
                 [AccountCode] [varchar](600) NULL, [Description] [varchar](max) NULL,
                 [DiscountRate] DECIMAL(22,4) NULL, [ItemCode] [varchar](600) NULL,
                 [LineAmount] DECIMAL(22,4) NULL, [Quantity] DECIMAL(22,4) NULL,
                 [ContactID] [varchar](800) NULL, [type] [varchar](400) NULL,
                 [status] [varchar](400) NULL, [LineAmountTypes] varchar(400) NULL,
                 [TaxAmount] DECIMAL(22,4) NULL, [TaxType] [varchar](600) NULL,
                 [TrackingID] [varchar](600) NULL, [TrackingOption] [varchar](600) NULL,
                 [UnitAmount] DECIMAL(22,4) NULL, [UpdatedDateUTC] datetime2,
                 [Date] datetime2, [DueDate] datetime2, [TrackingCategory1] varchar(800),
                 [TrackingCategory1_Option] varchar(800), [TrackingCategory2] varchar(800),
                 [TrackingCategory2_Option] varchar(800),  [OrgShortCode] [varchar](800) Not Null,
                 [OrgName] [varchar](800) Not Null) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero InvoiceDetails table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateInvoicesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero Invoices table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Invoices](
                    [AmountCredited] DECIMAL(22,4) NULL, [AmountDue] decimal(22, 4),
                 [AmountPaid] DECIMAL(22,4) NULL, [BrandingThemeID] [varchar](400) NULL,
                 [Contact] [varchar](400) NULL, [CreditNotes] [varchar](400) NULL,
                 [CurrencyCode] [varchar](400) NULL, [CurrencyRate] DECIMAL(22,4) NULL,
                 [Date] datetime2, [DueDate] datetime2, [ExpectedPaymentDate] datetime2,
                 [ExternalLinkProviderName] [varchar](400) NULL, [FullyPaidOnDate] datetime2,
                 [HasAttachments] [varchar](400) NULL, [InvoiceID] [varchar](400) NULL,
                 [InvoiceNumber] [varchar](400) NULL, [LineAmountTypes] [varchar](400) NULL,
                 [LineItemsCount] [varchar](400) NULL, [AccountCode] [varchar](400) NULL,
                 [Description] [varchar](max) NULL, [DiscountRate] DECIMAL(22,4) NULL,
                 [ItemCode] [varchar](400) NULL, [LineAmount] DECIMAL(22,4) NULL,
                 [Quantity] DECIMAL(22,4) NULL, [TaxAmount] DECIMAL(22,4) NULL,
                 [TaxType] [varchar](400) NULL, [UnitAmount] DECIMAL(22,4) NULL,
                 [Payments] [varchar](400) NULL, [PlannedPaymentDate] datetime2,
                 [Reference] [varchar](400) NULL, [SentToContact] [varchar](400) NULL,
                 [Status] [varchar](400) NULL, [SubTotal] DECIMAL(22,4) NULL,
                 [Total] DECIMAL(22,4) NULL, [TotalDiscount] DECIMAL(22,4) NULL,
                 [TotalTax] DECIMAL(22,4) NULL, [Type] [varchar](400) NULL,
                 [UpdatedDateUTC] datetime2, [Url] [varchar](400) NULL,
                 [TrackingCategory1] varchar(800), [TrackingCategory1_Option] varchar(800),
                 [TrackingCategory2] varchar(800), [TrackingCategory2_Option] varchar(800),
                 [OrgShortCode] [varchar](800) Not Null, [OrgName] [varchar](800) Not Null) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Invoices table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateJournalsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero Journals table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Journals](
                    [JID] [varchar](600) NULL, [Reference] [varchar](800) NULL,
                 [JNumber] [varchar](100) NULL, [Date] datetime2,
                 [CreatedDateUTC] datetime2, [AccountName] [varchar](800) NULL,
                 [AccountCode] [varchar](800) NULL, [AccountType] [varchar](800) NULL,
                 [NetAmt] DECIMAL(22,4) NULL, [GrossAmt] DECIMAL(22,4) NULL,
                 [TrackingCategory1] [varchar](800) NULL, [TrackingCategory1_Option] [varchar](800) NULL,
                 [TrackingCategory2] [varchar](800) NULL, [TrackingCategory2_Option] [varchar](800) NULL,
                 [TaxName] [varchar](200) NULL, [TaxType] [varchar](200) NULL,
                 [ValidationErrors] [varchar](200) NULL, [ValidationStatus] [varchar](200) NULL,
                 [Warnings] [varchar](200) NULL, [Description] [varchar](max) NULL,
                 [JournalLines] [varchar](200) NULL, [OrgShortCode] [varchar](800) Not Null,
                 [OrgName] [varchar](800) Not Null) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Journals table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateManualJournalsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero ManualJournals table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[ManualJournals](
                   [JournalID] [varchar](800) NULL, [Narration] [varchar](max) NULL,
                 [Date] datetime2 NULL, [UpdatedDateUtc] datetime2  NULL,
                 [AccountType] [varchar](400) NULL, [AccountCode] [varchar](400) NULL,
                 [AccountName] [varchar](800) NULL, [AccountId] [varchar](400) NULL,
                 [LineAmountTypes] [varchar](400) NULL, [Amount] DECIMAL(22,4) NULL,
                 [NetAmount] DECIMAL(22,4) NULL, [GrossAmount] DECIMAL(22,4) NULL,
                 [TaxAmount] DECIMAL(22,4) NULL, [TaxName] DECIMAL(22,4) NULL,
                 [TaxType] [varchar](400) NULL, [TrackingCategory1] [varchar](800) NULL,
                 [TrackingCategory1_option] [varchar](800) NULL, [TrackingCategory2] [varchar](800) NULL,
                 [TrackingCategory2_Option] [varchar](800) NULL, [Status] [varchar](400) NULL,
                 [Url] [varchar](400) NULL, [HasAttachments] [varchar](400) NULL,
                 [OrgShortCode] [varchar](800) NULL, [OrgName] [varchar](800) NULL) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero ManualJournals table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreatePnLTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero PnL table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[PnL](
                    [Row_Order] [int] NULL, [ReportID] [varchar](400) NULL,
                 [ReportDate] datetime2 NULL, [UpdatedDateUTC] [varchar](400) NULL,
                 [ReportName] [varchar](400) NULL, [ReportRowTitle] [varchar](800) NULL,
                 [ReportType] [varchar](400) NULL, [Type_Level1] [varchar](400) NULL,
                 [Type_Level2] [varchar](400) NULL, [Name] [varchar](800) NULL,
                 [Value] [varchar](800) NULL, [ValidationErrors] [varchar](800) NULL,
                 [ValidationStatus] [varchar](80) NULL, [Warnings] [varchar](800) NULL,
                 [OrgShortCode] [varchar](800) Not Null, [OrgName] [varchar](800) Not Null) ON [PRIMARY];";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero PnL table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateTrackingCategoriesTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero TrackingCategories table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[TrackingCategories](
                   [TrackingCategoryID] [varchar](max) NULL, [Category] [varchar](600) NULL,
                 [Option] [varchar](600) NULL, [OrgShortCode] [varchar](800) Not Null,
                 [OrgName] [varchar](800) Not Null, [TrackingOptionID] [varchar](max) NULL, ) ON [PRIMARY] ;";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero TrackingCategories table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreatePurchaseOrdersTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero PurchaseOrders table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[PurchaseOrders](
                    [PurchaseOrderID] [varchar](800) NULL, [PurchaseOrderNumber] [varchar](800) NULL,
                 [DateString] [varchar](50) NULL, [Date] [datetime2](7) NULL,
                 [DeliveryDateString] [varchar](50) NULL, [DeliveryDate] [datetime2](7) NULL,
                 [DeliveryAddress] [varchar](800) NULL, [AttentionTo] [varchar](800) NULL,
                 [Telephone] [varchar](50) NULL, [DeliveryInstructions] [varchar](800) NULL,
                 [IsDiscounted] [varchar](100) NULL, [Reference] [varchar](800) NULL,
                 [Type] [varchar](800) NULL, [CurrencyRate] [money] NULL,
                 [CurrencyCode] [varchar](800) NULL, [ContactID] [varchar](800) NULL,
                 [BrandingThemeID] [varchar](800) NULL, [Status] [varchar](200) NULL,
                 [LineAmountTypes] [varchar](800) NULL, [LineItemAccountCode] [varchar](800) NULL,
                 [LineItemID] [varchar](800) NULL, [LineItemDescription] [varchar](800) NULL,
                 [LineItemUnitAmount] [money] NULL, [LineItemTaxType] [varchar](800) NULL,
                 [LineItemTaxAmount] [money] NULL, [LineItemLineAmount] [money] NULL,
                 [LineItemTrackingName1] [varchar](800) NULL, [LineItemTrackingName2] [varchar](800) NULL,
                 [LineItemTrackingOption1] [varchar](800) NULL, [LineItemTrackingOption2] [varchar](800) NULL,
                 [LineItemQuantity] [money] NULL, [SubTotal] [money] NULL,
                 [TotalTax] [money] NULL, [TotalDiscount] [money] NULL,
                 [Total] [money] NULL, [UpdatedDateUTC] [datetime2](7) NULL,
                 [HasAttachments] [varchar](800) NULL, [OrgShortCode] [varchar](800) NULL,
                 [OrgName] [varchar](800) NULL) ON [PRIMARY]";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero PurchaseOrders table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreatePaymentsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero Payments table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Payments](
                    [Id] [nvarchar](50)  NOT NULL, [Date] [datetime2](7)  NULL,
                 [CurrencyRate] DECIMAL(18,0)  NULL, [Amount] DECIMAL(18,0)  NULL,
                 [Reference] [nvarchar](max)  NULL, [IsReconciled] [bit]  NULL,
                 [Status] [nvarchar](max)  NULL, [PaymentType] [nvarchar](max)  NULL,
                 [UpdatedDateUTC] [datetime2](7)  NULL, [OverpaymentId] [nvarchar](max)  NULL,
                 [XEROAccount_Id] [nvarchar](max)  NULL, [Invoice_Id] [nvarchar](max)  NULL,
                 [CreditNote_Id] [nvarchar](max)  NULL, [OrgShortCode] [nvarchar](max)  NULL,
                 [OrgName] [nvarchar](max)  NULL ) ON [PRIMARY] ;";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Payments table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateReceiptsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero Receipts table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[Receipts](
                   [Id] [nvarchar](max)  NOT NULL, [Date] [datetime2](7)  NOT NULL,
                 [Contact_Id] [nvarchar](max)  NULL, [Reference] [nvarchar](max)  NULL,
                 [LineAmountTypes] [nvarchar](max)  NULL, [SubTotal] DECIMAL(18,0)  NULL,
                 [TotalTax] DECIMAL(18,0)  NULL, [Total] [decimal](18,0)  NULL,
                 [ReceiptID] [nvarchar](max)  NULL, [Status] [nvarchar](max)  NULL,
                 [ReceiptNumber] [nvarchar](max)  NULL, [UpdatedDateUTC] [datetime2](7)  NULL,
                 [HasAttachments] [bit]  NULL, [Url] [nvarchar](max)  NULL,
                 [XEROUser_Id] [nvarchar](max)  NULL, [OrgShortCode] [nvarchar](max)  NULL,
                 [OrgName] [nvarchar](max)  NULL ) ON [PRIMARY] ;";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero Receipts table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateReceiptLineItemsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero ReceiptLineItems table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[ReceiptLineItems](
                    [LineItemId] [nvarchar](max)  NOT NULL, [Description] [nvarchar](max) NOT NULL,
                     [Quantity] DECIMAL(18,0) NULL, [UnitAmount] DECIMAL(18,0) NULL,
                     [AccountCode] [nvarchar](max) NOT NULL, [ItemCode] [nvarchar](max) NOT NULL,
                     [TaxType] [nvarchar](max) NOT NULL, [TaxAmount] DECIMAL(18,0) NULL,	
                    [LineAmount] DECIMAL(18,0) NULL, [DiscountRate] DECIMAL(18,0) NULL ) ON [PRIMARY] ;";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero ReceiptLineItems table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateLineItemInReceiptTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero LineItemInReceipt table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[LineItemInReceipt](
                  [Id] [nvarchar](max)  NOT NULL, [LineItemId] [nvarchar](max)  NOT NULL,
                    [ReceiptId] [nvarchar](max)  NOT NULL ) ON [PRIMARY] ;";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero LineItemInReceipt table db for {_companyName} exception:{ex.Message}");
            }
        }
        private void CreateUserPayrollsTable()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    _logger.Info($"Create Xero UserPayrolls table db for  [QuickBooks_{_companyName}");
                    var query = "CREATE TABLE [Xero_" + _companyName + @"].[dbo].[UserPayrolls](
                 [Id] [nvarchar](max)  NOT NULL, [Date] [datetime2](7)  NOT NULL,
                 [PayItemType] [nvarchar](max)  NOT NULL, [Employee] [nvarchar](max)  NOT NULL,
                 [PayItem] [nvarchar](max)  NOT NULL, [OrgName] [nvarchar](max)  NOT NULL,
                 [TrackingCategory1] [nvarchar](max)  NOT NULL, [TrackingCategory2] [nvarchar](max)  NOT NULL
                 ) ON [PRIMARY] ;";
                    connection.Execute(query);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero UserPayrolls table db for {_companyName} exception:{ex.Message}");
            }
        }
    }
}
