﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;

namespace ClearJelly.XeroApp
{
    public class JedoxEtl
    {
        private const String URL = "http://127.0.0.1:7775/etlserver/services/ETL-Server/";

        public JedoxEtl()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private static string jedoxLogin()
        {
            string myParameters = "user=Hesam&password=mana@2010";
            string HtmlResult;
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                HtmlResult = wc.UploadString(URL + "login", myParameters);
                int y1 = 0;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(HtmlResult);
            XmlNode node = xmlDoc.DocumentElement.SelectSingleNode("/").FirstChild.FirstChild.ChildNodes[1];
            String etl_session = node.InnerText;
            return (etl_session);
        }

        public string jedoxExecuteJob(Dictionary<String, String> Variables, String locator)
        {
            String etl_session = jedoxLogin();
            String jobID = "";
            System.Net.WebRequest req = null;
            System.Net.WebResponse rsp = null;
            string result = "";
            try
            {
                String message = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://ns.jedox.com/ETL-Server"" xmlns:ns2=""http://service.etl.jedox.com/xsd"" xmlns:ns3=""http://ns.jedox.com/ETL-Server/""><SOAP-ENV:Header>";
                message += "<ns3:etlsession>" + etl_session + "</ns3:etlsession></SOAP-ENV:Header>";
                message += "<SOAP-ENV:Body><ns1:execute><ns1:locator>" + locator + "</ns1:locator>";

                //"<ns2:name>CompanyName</ns2:name><ns2:value>UUU</ns2:value></ns1:variables><ns1:variables><ns2:name>UserGroup</ns2:name><ns2:value>UUU_group</ns2:value></ns1:variables><ns1:variables><ns2:name>FilePath</ns2:name><ns2:value>C:\test</ns2:value></ns1:variables></ns1:execute></SOAP-ENV:Body></SOAP-ENV:Envelope>"


                foreach (KeyValuePair<string, string> kvp in Variables)
                {
                    message += "<ns1:variables>";
                    //Console.WriteLine("Key = {0}, Value = {1}",
                    //    kvp.Key, kvp.Value);
                    message += "<ns2:name>" + kvp.Key + "</ns2:name><ns2:value>" + kvp.Value + "</ns2:value>";

                    message += "</ns1:variables>";
                }

                message += "</ns1:execute></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                req = System.Net.WebRequest.Create(URL);
                req.Method = "POST";
                req.ContentType = "text/xml";
                System.IO.StreamWriter writer =
            new System.IO.StreamWriter(req.GetRequestStream());
                string mess = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://ns.jedox.com/ETL-Server"" xmlns:ns2=""http://service.etl.jedox.com/xsd"" xmlns:ns3=""http://ns.jedox.com/ETL-Server/""><SOAP-ENV:Header><ns3:etlsession>" + etl_session + "</ns3:etlsession></SOAP-ENV:Header><SOAP-ENV:Body><ns1:execute><ns1:locator>FolderGeneration.jobs.default</ns1:locator><ns1:variables><ns2:name>CompanyName</ns2:name><ns2:value>UUU</ns2:value></ns1:variables><ns1:variables><ns2:name>UserGroup</ns2:name><ns2:value>UUU_group</ns2:value></ns1:variables><ns1:variables><ns2:name>FilePath</ns2:name><ns2:value>C:\test</ns2:value></ns1:variables></ns1:execute></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                //writer.WriteLine(mess);
                writer.WriteLine(message);
                writer.Close();
                rsp = req.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(rsp.GetResponseStream());
                String xmlData = reader.ReadToEnd();

                var x = rsp.GetResponseStream();
                int pos1 = xmlData.IndexOf("<ax21:id>");
                int pos2 = xmlData.IndexOf("</ax21:id>");
                jobID = xmlData.Substring(pos1 + 9, pos2 - pos1 - 9).Trim();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (req != null) req.GetRequestStream().Close();
                if (rsp != null) rsp.GetResponseStream().Close();
                if (etl_session != null) jedoxLogout(etl_session);
            }

            return (jobID);
        }

        public string jedoxExecuteSystemOpsJob(Dictionary<String, String> Variables, String locator)
        {
            String etl_session = jedoxLogin();
            String jobID = "";
            System.Net.WebRequest req = null;
            System.Net.WebResponse rsp = null;
            string result = "";
            try
            {
                String message = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://ns.jedox.com/ETL-Server"" xmlns:ns2=""http://service.etl.jedox.com/xsd"" xmlns:ns3=""http://ns.jedox.com/ETL-Server/""><SOAP-ENV:Header>";
                message += "<ns3:etlsession>" + etl_session + "</ns3:etlsession></SOAP-ENV:Header>";
                message += "<SOAP-ENV:Body><ns1:execute><ns1:locator>" + locator + "</ns1:locator>";

                //"<ns2:name>CompanyName</ns2:name><ns2:value>UUU</ns2:value></ns1:variables><ns1:variables><ns2:name>UserGroup</ns2:name><ns2:value>UUU_group</ns2:value></ns1:variables><ns1:variables><ns2:name>FilePath</ns2:name><ns2:value>C:\test</ns2:value></ns1:variables></ns1:execute></SOAP-ENV:Body></SOAP-ENV:Envelope>"


                foreach (KeyValuePair<string, string> kvp in Variables)
                {
                    message += "<ns1:variables>";
                    //Console.WriteLine("Key = {0}, Value = {1}",
                    //    kvp.Key, kvp.Value);
                    message += "<ns2:name>" + kvp.Key + "</ns2:name><ns2:value>" + kvp.Value + "</ns2:value>";

                    message += "</ns1:variables>";
                }

                message += "</ns1:execute></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                req = System.Net.WebRequest.Create(URL);
                req.Method = "POST";
                req.ContentType = "text/xml";
                System.IO.StreamWriter writer =
            new System.IO.StreamWriter(req.GetRequestStream());
                string mess = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://ns.jedox.com/ETL-Server"" xmlns:ns2=""http://service.etl.jedox.com/xsd"" xmlns:ns3=""http://ns.jedox.com/ETL-Server/""><SOAP-ENV:Header><ns3:etlsession>" + etl_session + "</ns3:etlsession></SOAP-ENV:Header><SOAP-ENV:Body><ns1:execute><ns1:locator>FolderGeneration.jobs.default</ns1:locator><ns1:variables><ns2:name>CompanyName</ns2:name><ns2:value>UUU</ns2:value></ns1:variables><ns1:variables><ns2:name>UserGroup</ns2:name><ns2:value>UUU_group</ns2:value></ns1:variables><ns1:variables><ns2:name>FilePath</ns2:name><ns2:value>C:\test</ns2:value></ns1:variables></ns1:execute></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                //writer.WriteLine(mess);
                writer.WriteLine(message);
                writer.Close();
                rsp = req.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(rsp.GetResponseStream());
                String xmlData = reader.ReadToEnd();

                var x = rsp.GetResponseStream();
                int pos1 = xmlData.IndexOf("<ax21:id>");
                int pos2 = xmlData.IndexOf("</ax21:id>");
                jobID = xmlData.Substring(pos1 + 9, pos2 - pos1 - 9).Trim();

            }
            catch
            {
                throw;
            }
            finally
            {
                if (req != null) req.GetRequestStream().Close();
                if (rsp != null) rsp.GetResponseStream().Close();
                if (etl_session != null) jedoxLogout(etl_session);
            }

            return (jobID);
        }

        public string jedoxExecuteStatus(String ID)
        {
            String etl_session = jedoxLogin();
            String jobID = "";
            System.Net.WebRequest req = null;
            System.Net.WebResponse rsp = null;
            string result = "";
            try
            {
                String message = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://ns.jedox.com/ETL-Server"" xmlns:ns2=""http://ns.jedox.com/ETL-Server/""><SOAP-ENV:Header>";//<ns2:etlsession>ncWFLx/VVzRm26uVGYfgJiVx78EcBU8KlP3qWDjg2wg1u7IEOzXpoNIQudK0eViUBiozsXVC7S0LmDmdw6iIhQ==</ns2:etlsession></SOAP-ENV:Header><SOAP-ENV:Body><ns1:getExecutionStatus><ns1:id>1</ns1:id><ns1:waitForTermination>true</ns1:waitForTermination></ns1:getExecutionStatus></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                message += "<ns2:etlsession>" + etl_session + "</ns2:etlsession></SOAP-ENV:Header>";
                message += "<SOAP-ENV:Body><ns1:getExecutionStatus><ns1:id>" + ID + "</ns1:id>";
                message += "<ns1:waitForTermination>true</ns1:waitForTermination></ns1:getExecutionStatus>";
                message += "</SOAP-ENV:Body></SOAP-ENV:Envelope>";

                req = System.Net.WebRequest.Create(URL);
                req.Method = "POST";
                req.ContentType = "text/xml";
                System.IO.StreamWriter writer =
                new System.IO.StreamWriter(req.GetRequestStream());

                writer.WriteLine(message);
                writer.Close();
                rsp = req.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(rsp.GetResponseStream());
                String xmlData = reader.ReadToEnd();

                var x = rsp.GetResponseStream();
                int pos1 = xmlData.IndexOf("<ax21:status>");
                int pos2 = xmlData.IndexOf("</ax21:status>");
                result = xmlData.Substring(pos1 + 13, pos2 - pos1 - 13).Trim();

                //string t = "";

            }
            catch
            {
                throw;
            }
            finally
            {
                if (req != null) req.GetRequestStream().Close();
                if (rsp != null) rsp.GetResponseStream().Close();
                if (etl_session != null) jedoxLogout(etl_session);
            }

            return (result);
        }

        private void jedoxLogout(string etlSession)
        {
            System.Net.WebRequest req = null;
            System.Net.WebResponse rsp = null;
            string result = "";
            try
            {
                String message = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://ns.jedox.com/ETL-Server"" xmlns:ns2=""http://ns.jedox.com/ETL-Server/""><SOAP-ENV:Header>";//<ns2:etlsession>ncWFLx/VVzRm26uVGYfgJiVx78EcBU8KlP3qWDjg2wg1u7IEOzXpoNIQudK0eViUBiozsXVC7S0LmDmdw6iIhQ==</ns2:etlsession></SOAP-ENV:Header><SOAP-ENV:Body><ns1:getExecutionStatus><ns1:id>1</ns1:id><ns1:waitForTermination>true</ns1:waitForTermination></ns1:getExecutionStatus></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                message += "</SOAP-ENV:Header>";
                message += "<SOAP-ENV:Body><ns1:logout><ns1:etlsession>" + etlSession + "</ns1:etlsession></ns1:logout>";

                message += "</SOAP-ENV:Body></SOAP-ENV:Envelope>";

                req = System.Net.WebRequest.Create(URL);
                req.Method = "POST";
                req.ContentType = "text/xml";
                System.IO.StreamWriter writer =
                new System.IO.StreamWriter(req.GetRequestStream());

                writer.WriteLine(message);
                writer.Close();
                rsp = req.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(rsp.GetResponseStream());
                String xmlData = reader.ReadToEnd();

            }
            catch
            {
                throw;
            }
            finally
            {
                if (req != null) req.GetRequestStream().Close();
                if (rsp != null) rsp.GetResponseStream().Close();
            }
        }
    }
}