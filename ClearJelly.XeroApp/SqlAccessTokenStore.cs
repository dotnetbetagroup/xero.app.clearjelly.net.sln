﻿using System.Data.SqlClient;
using System.Linq;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using System.Web.Configuration;
using System;
using Dapper;

namespace ClearJelly.XeroApp
{
    public class SqlAccessTokenStore : ITokenStore
    {
        [ThreadStatic] public static string orgShortCode;
        SqlConnection connection = new SqlConnection(
                    WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        public SqlAccessTokenStore()
        {
            connection = new SqlConnection(
                    WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }


        //public IToken Find(string userId,string org)
        public IToken Find(string userId)
        {

            if (string.IsNullOrWhiteSpace(userId))
                return null;

            connection = new SqlConnection(
                                WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            // the following code should be used in the sql statement if the application doe not use multi threading
            // in that case a session variable can be used.
            // if this function is called from a worker thread then a ThreadStatic variable should be used for OrgShortCode
            //WHERE UserId = @UserId AND OrganisationId='" + HttpContext.Current.Session["tmpShortCode"] + "' ",
            using (connection)
            {
                connection.Open();

                var token = connection.Query<Token>(
                        @"SELECT UserId, OrganisationId, ConsumerKey, ConsumerSecret, TokenKey, TokenSecret, ExpiresAt, Session, SessionExpiresAt
                FROM tokens
                WHERE UserId = @UserId AND OrganisationId='" + orgShortCode + "' ",
                        new
                        {
                            userId
                        }).FirstOrDefault();

                if (null != token && token.ExpiresAt.HasValue)
                {
                    // This is done because SQLite seems to be storing it as local time.
                    // token.ExpiresAt = token.ExpiresAt.Value.ToUniversalTime();
                }
                return token;
            }


        }


        public IToken FindByOrg(string userId, string org)
        {
            orgShortCode = org;
            //HttpContext.Current.Session["tmpShortCode"] = org;

            if (string.IsNullOrWhiteSpace(userId))
                return null;
            connection = new SqlConnection(
                                WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            using (connection)
            {
                connection.Open();

                var token = connection.Query<Token>(
                    @"SELECT UserId, OrganisationId, ConsumerKey, ConsumerSecret, TokenKey, TokenSecret, ExpiresAt, Session, SessionExpiresAt
                FROM tokens
                WHERE UserId = @UserId AND OrganisationId='" + org + "' ",
                    new
                    {
                        userId
                    }).FirstOrDefault();

                if (null != token && token.ExpiresAt.HasValue)
                {
                    // This is done because SQLite seems to be storing it as local time.
                    //token.ExpiresAt = token.ExpiresAt.Value.ToUniversalTime();
                }

                return token;
            }
        }

        public void Add(IToken token)
        {
            connection = new SqlConnection(
                                            WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            using (connection)
            {
                connection.Open();

                connection.Execute("DELETE FROM tokens WHERE OrganisationId = @OrganisationId", new
                {
                    token.OrganisationId
                });
                orgShortCode = token.OrganisationId;
                connection.Execute(@"INSERT INTO tokens (UserId, OrganisationId, ConsumerKey, ConsumerSecret, TokenKey, TokenSecret, ExpiresAt, Session, SessionExpiresAt)
                VALUES (@UserId, @OrganisationID, @ConsumerKey, @ConsumerSecret, @TokenKey, @TokenSecret, @ExpiresAt, @Session, @SessionExpiresAt)", token);
            }
        }

        public void Delete(IToken token)
        {
            connection = new SqlConnection(
                                WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            using (connection)
            {
                connection.Open();

                connection.Execute("DELETE FROM tokens WHERE OrganisationId = @OrganisationId", new
                {
                    token.OrganisationId
                });
            }
        }

        public void DeletePrev(string orgCode)
        {
            connection = new SqlConnection(
                                WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            using (connection)
            {
                connection.Open();

                connection.Execute("DELETE FROM tokens WHERE OrganisationId = '" + orgCode + "' ", null);
            }
        }
    }
}
