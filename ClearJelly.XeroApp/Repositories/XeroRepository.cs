﻿using ClearJelly.Entities;
using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.XeroApp.Repositories
{
   public class XeroRepository
    {
        private string _connection;
        Logger _logger;
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();
        public XeroRepository(string connectionString)
        {
            _connection = connectionString;
            _logger = LogManager.GetCurrentClassLogger();
        }
        public bool CreateXeroDatabaseAsync(string _companyName)
        {
            var con = "Server = tcp:aptest.database.windows.net,1433; Initial Catalog = master; Persist Security Info = False; User ID = admin@agilityplanningtst.onmicrosoft.com; Password = Managility@2017; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Authentication =\"Active Directory Password\";Connection Timeout=120;";
            _logger.Info($"Creating QuickBooks db for {_companyName}");
            try
            {
                using (SqlConnection connection = new SqlConnection(con))
                {
                    connection.Open();
                    var query = "create database [Xero_" + _companyName + "] (EDITION = 'basic', SERVICE_OBJECTIVE = ELASTIC_POOL(name = aptst_pool));";
                    var res = connection.Execute(query);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Create Xero db for {_companyName} exception:{ex.Message}");
                return false;
            }
        }
    }
}
