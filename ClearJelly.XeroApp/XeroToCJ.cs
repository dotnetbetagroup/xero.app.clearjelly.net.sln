﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using Xero.Api.Core;
using Xero.Api.Core.Model;
using Xero.Api.Infrastructure.Exceptions;
using ClearJelly.Entities;
using static ClearJelly.XeroApp.CommonXero;
using ClearJelly.Configuration;
/// <summary>
/// Coded By:   Hesam Ziaei (hesamz@managility.com.au)
/// Start Date: 25/09/2015
/// Summary description for XeroToCJ
///              This class centrlises to logic for 
///              communication between Xero and CJ model.
///              It uses the new Xero .Net library to 
///              connect to Xero and retrieve client's 
///              data. The class caters for both 
///              creation and update of the models.
/// 
/// </summary>
namespace ClearJelly.XeroApp
{
    public class XeroToCJ
    {
        //  private static readonly X509Certificate2 OAuthCertificate = new X509Certificate2(System.Web.HttpContext.Current.Server.MapPath("/Certs/public_privatekey.pfx"), "mana@2010");
        //  private static readonly X509Certificate2 ClientSslCertificate = new X509Certificate2(System.Web.HttpContext.Current.Server.MapPath("/Certs/entrust-client.p12"), "mana@2010");
        private string user_conn;
        private string azure_user_conn;
        private string system_conn;
        private static NLog.Logger _logger;
        private XeroCoreApi api;
        private String inUserName;
        private String inOrgShortCode;
        private String inOrgName;
        private string inPassword;

        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1);
        private List<string> exceptionMethodNameList { get; set; }
        private readonly ClearJellyEntities _clearJellyContext;
        public XeroToCJ(XeroCoreApi api, String inUserName, string inPassword, String inOrgName, String inOrgShortCode)
        {
            this.api = api;
            this.inOrgName = inOrgName;
            this.inOrgShortCode = inOrgShortCode;
            this.inUserName = inUserName;
            this.inPassword = inPassword;
            this.exceptionMethodNameList = new List<string>();
            //this.exceptionXeroErrorMessage = new string();
            _clearJellyContext = new ClearJellyEntities();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-AU");


            _logger = NLog.LogManager.GetCurrentClassLogger();
            user_conn = ConfigSection.XeroCompanyDbConnection;

            azure_user_conn = ConfigSection.AzureUserDatabase;
            system_conn = ConfigSection.DefaultConnection;

            String DbName = GetUserDBName(inUserName);
            user_conn += DbName + ";";

            azure_user_conn += "database=Xero_" + DbName + ";";
        }
        //--------------------------------------------------------------------------------------

        public int PingToXero()
        {
            try
            {
                if (api != null)
                {
                    var organization = api.Organisation;
                }
                return (int)XeroErrorCodes.Ok;
            }
            catch (XeroApiException ex)
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ PingToXero method completed.", ex);
                return (int)ex.Code;
            }
            catch (Exception ex)
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ PingToXero  GeneralError method completed.", ex);
                return (int)XeroErrorCodes.GeneralError;
            }
        }

        public int UpdateModel(DateTime inStartDate, DateTime inEndDate)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-AU");
            DateTime startDate = inStartDate;
            DateTime endDate = inEndDate;
            exceptionMethodNameList.Clear();
            //GetAccounts();
            string methodNamesList = string.Empty;
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ UpdateModel method started.");


                int ErrorCode = PingToXero();

                if (ErrorCode == (int)XeroErrorCodes.Ok)
                {
                    GetAllPayments();
                    System.Threading.Thread.Sleep(5000);

                    GetAllReceipts();

                    System.Threading.Thread.Sleep(5000);

                    GetAllJournalsWithPagination(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAllInvoicesWithPagination(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetManualJournalsIDs(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAllCreditNotesWithPagination(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAccounts();
                    System.Threading.Thread.Sleep(5000);

                    getTrackingCategories(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAllContacts(startDate, endDate);

                    System.Threading.Thread.Sleep(5000);

                    GetPurchaseOrders(startDate, endDate);

                    var CheckPremiumflag = CommonXero.CheckPremiumSubscription(inUserName);
                    _logger.Info(inUserName + " ~Execution of XeroToCJ UpdateModel CheckPremiumflag " + CheckPremiumflag);
                    if (CheckPremiumflag == true)
                    {
                        _logger.Info(inUserName + " ~Execution of XeroToCJ UpdateModel CheckPremiumflag true" + CheckPremiumflag);
                        //runJedoxETL("Update");
                        _logger.Info(inUserName + " ~Execution of XeroToCJ UpdateModel CheckPremiumflag runjedox complete." + CheckPremiumflag);
                    }
                    //send email to user if limit exceeds.
                    if (exceptionMethodNameList.Count() > 0)
                    {
                        var methodNames = string.Join(" ,", exceptionMethodNameList.ToArray());
                        SendMailOnApiException(methodNames, startDate, endDate);
                    }

                    SendEmail(true);
                }
                //send email to user if limit exceeds.
                if (ErrorCode == (int)System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    var methodNames = string.Join(" ,", exceptionMethodNameList.ToArray());
                    SendMailOnApiException(methodNames, startDate, endDate);
                }
                return ErrorCode;
            }
            catch (Exception ex)
            {
                int p = 0;
                SendEmail(false);
                _logger.Error(inUserName + " ~Execution of XeroToCJ UpdateModel method completed." + ex.Message, ex);
                return (int)XeroErrorCodes.GeneralError;
            }

        }
        //--------------------------------------------------------------------------------------

        public int ReCreateModle(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ ReCreateModle method started.");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-AU");


                exceptionMethodNameList.Clear();
                int ErrorCode = PingToXero();
                if (ErrorCode == (int)XeroErrorCodes.Ok)
                {
                    //GetAccounts();
                    GetAllPayments();
                    System.Threading.Thread.Sleep(5000);

                    GetAllReceipts();
                    System.Threading.Thread.Sleep(5000);

                    GetAllJournalsWithPagination(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAllInvoicesWithPagination(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetManualJournalsIDs(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAllCreditNotesWithPagination(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAccounts();
                    System.Threading.Thread.Sleep(5000);

                    getTrackingCategories(startDate, endDate);
                    System.Threading.Thread.Sleep(5000);

                    GetAllContacts(startDate, endDate);

                    System.Threading.Thread.Sleep(5000);

                    GetPurchaseOrders(startDate, endDate);
                    var CheckPremiumflag = CommonXero.CheckPremiumSubscription(inUserName);
                    _logger.Info(inUserName + " ~Execution of XeroToCJ ReCreateModle " + CheckPremiumflag);
                    if (CheckPremiumflag == true)
                    {
                        _logger.Info(inUserName + " ~Execution of XeroToCJ ReCreateModle CheckPremiumflag true" + CheckPremiumflag);
                        // runJedoxETL("Create");
                        _logger.Info(inUserName + " ~Execution of XeroToCJ ReCreateModle CheckPremiumflag true runJedoxETL completed." + CheckPremiumflag);
                    }
                    //send email to user if limit exceeds.
                    if (exceptionMethodNameList.Count() > 0)
                    {
                        var methodNames = string.Join(" ,", exceptionMethodNameList.ToArray());
                        SendMailOnApiException(methodNames, startDate, endDate);
                    }
                    SendEmail(true);
                }

                else if (ErrorCode == (int)System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    var methodNames = string.Join(" ,", exceptionMethodNameList.ToArray());
                    SendMailOnApiException(methodNames, startDate, endDate);
                }

                return ErrorCode;
            }
            catch (Exception ex)
            {
                int p = 0;
                SendEmail(false);
                _logger.Error(inUserName + " ~Execution of XeroToCJ UpdateModel method completed." + ex.Message, ex);
                return (int)System.Net.HttpStatusCode.Found;
            }
        }
        //--------------------------------------------------------------------------------------

        protected void GetAccounts()
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAccounts method started.");


                // API v2.15 Get a list of accounts that can be used when creating expense claims
                var Accounts = api.Accounts;

                DataTable entries = new DataTable("ChartOfAccounts");


                DataColumn AccountID = new DataColumn();
                AccountID.DataType = System.Type.GetType("System.String");
                AccountID.ColumnName = "AccountID";
                entries.Columns.Add(AccountID);

                DataColumn BankAccountNumber = new DataColumn();
                BankAccountNumber.DataType = System.Type.GetType("System.String");
                BankAccountNumber.ColumnName = "BankAccountNumber";
                entries.Columns.Add(BankAccountNumber);

                DataColumn Class = new DataColumn();
                Class.DataType = System.Type.GetType("System.String");
                Class.ColumnName = "Class";
                entries.Columns.Add(Class);

                DataColumn Code = new DataColumn();
                Code.DataType = System.Type.GetType("System.String");
                Code.ColumnName = "Code";
                entries.Columns.Add(Code);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                entries.Columns.Add(CurrencyCode);

                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn EnablePaymentsToAccount = new DataColumn();
                EnablePaymentsToAccount.DataType = System.Type.GetType("System.String");
                EnablePaymentsToAccount.ColumnName = "EnablePaymentsToAccount";
                entries.Columns.Add(EnablePaymentsToAccount);

                DataColumn Name = new DataColumn();
                Name.DataType = System.Type.GetType("System.String");
                Name.ColumnName = "Name";
                entries.Columns.Add(Name);

                DataColumn ReportingCode = new DataColumn();
                ReportingCode.DataType = System.Type.GetType("System.String");
                ReportingCode.ColumnName = "ReportingCode";
                entries.Columns.Add(ReportingCode);

                DataColumn ReportingCodeName = new DataColumn();
                ReportingCodeName.DataType = System.Type.GetType("System.String");
                ReportingCodeName.ColumnName = "ReportingCodeName";
                entries.Columns.Add(ReportingCodeName);

                DataColumn ShowInExpenseClaims = new DataColumn();
                ShowInExpenseClaims.DataType = System.Type.GetType("System.String");
                ShowInExpenseClaims.ColumnName = "ShowInExpenseClaims";
                entries.Columns.Add(ShowInExpenseClaims);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn SystemAccount = new DataColumn();
                SystemAccount.DataType = System.Type.GetType("System.String");
                SystemAccount.ColumnName = "SystemAccount";
                entries.Columns.Add(SystemAccount);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                entries.Columns.Add(Type);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);


                foreach (var account in Accounts.Find())
                {
                    DataRow row = entries.NewRow();
                    row["AccountID"] = account.Id;
                    row["BankAccountNumber"] = account.BankAccountNumber;
                    row["Class"] = account.Class;
                    row["Code"] = account.Code;
                    row["CurrencyCode"] = account.CurrencyCode;
                    row["Description"] = account.Description;
                    row["EnablePaymentsToAccount"] = account.EnablePaymentsToAccount;
                    row["Name"] = account.Name;
                    row["ReportingCode"] = account.ReportingCode;
                    row["ReportingCodeName"] = account.ReportingCodeName;
                    row["ShowInExpenseClaims"] = account.ShowInExpenseClaims;
                    row["Status"] = account.Status;
                    row["SystemAccount"] = account.SystemAccount;
                    row["Type"] = account.Type;
                    row["OrgShortCode"] = inOrgShortCode;
                    row["OrgName"] = inOrgName;

                    entries.Rows.Add(row);
                }

                // insert into sql table

                if (CheckValidDatabase())
                {
                    clear_table("ChartOfAccounts");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        bulkInsertGetAccounts(bulkCopy, entries);
                    }
                }


                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table("ChartOfAccounts");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            bulkInsertGetAccounts(bulkCopy, entries);
                        }
                    }
                }

            }
            catch (XeroApiException ex)
            {

                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAccounts XeroApiException. Organization :" + (inOrgName ?? "") + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetAccounts");
                }
            }
            catch (Exception ex)
            {

                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAccounts completed :" + ex.Message, ex);
            }
        }

        protected void GetAllPayments()
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllPayments method started.");

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                DataTable entries = new DataTable("Payments");
                List<String> IDs = new List<String>();

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn CurrencyRate = new DataColumn();
                CurrencyRate.DataType = System.Type.GetType("System.Decimal");
                CurrencyRate.ColumnName = "CurrencyRate";
                entries.Columns.Add(CurrencyRate);

                DataColumn Amount = new DataColumn();
                Amount.DataType = System.Type.GetType("System.Decimal");
                Amount.ColumnName = "Amount";
                entries.Columns.Add(Amount);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn IsReconciled = new DataColumn("IsReconciled", typeof(bool));
                entries.Columns.Add(IsReconciled);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn PaymentType = new DataColumn();
                PaymentType.DataType = System.Type.GetType("System.String");
                PaymentType.ColumnName = "PaymentType";
                entries.Columns.Add(PaymentType);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.DateTime");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                entries.Columns.Add(UpdatedDateUTC);

                DataColumn XEROAccount_Id = new DataColumn();
                XEROAccount_Id.DataType = System.Type.GetType("System.String");
                XEROAccount_Id.ColumnName = "XEROAccount_Id";
                entries.Columns.Add(XEROAccount_Id);

                DataColumn Invoice_Id = new DataColumn();
                Invoice_Id.DataType = System.Type.GetType("System.String");
                Invoice_Id.ColumnName = "Invoice_Id";
                entries.Columns.Add(Invoice_Id);

                DataColumn CreditNote_Id = new DataColumn();
                CreditNote_Id.DataType = System.Type.GetType("System.String");
                CreditNote_Id.ColumnName = "CreditNote_Id";
                entries.Columns.Add(CreditNote_Id);

                DataColumn OverpaymentId = new DataColumn();
                OverpaymentId.DataType = System.Type.GetType("System.String");
                OverpaymentId.ColumnName = "OverpaymentId";
                entries.Columns.Add(OverpaymentId);

                DataColumn OrgShortCode = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                List<Xero.Api.Core.Model.Payment> all_notes = new List<Xero.Api.Core.Model.Payment>();
                all_notes = api.Payments
                                .Find()
                                .ToList();
                var connection = new SqlConnectionStringBuilder(user_conn);
                var defaultConnection = new SqlConnectionStringBuilder(system_conn);
                var baseRepository = new BaseRepository<LineItem>(defaultConnection.ConnectionString);
                var org = baseRepository.GetUserOrg(inUserName);

                if (CheckValidDatabase())
                {
                    clear_table("Payments");
                }

                foreach (Payment payment in all_notes)
                {

                    var pay = new ClearJelly.ViewModels.UserMappingModels.Payment();
                    pay.Id = payment?.Id.ToString();
                    pay.Amount = payment.Amount;
                    pay.CreditNoteId = payment.CreditNote?.Id.ToString();
                    pay.CurrencyRate = payment.CurrencyRate;
                    pay.Date = payment.Date;
                    pay.InvoiceId = payment.Invoice?.Id.ToString();
                    pay.IsReconciled = payment.IsReconciled;
                    pay.OverpaymentId = payment.Overpayment?.Id.ToString();
                    pay.PaymentType = payment.Type.ToString();
                    pay.Reference = payment.Reference;
                    pay.Status = payment.Status.ToString();
                    pay.UpdatedDateUTC = payment.UpdatedDateUtc;
                    pay.XEROAccountId = payment.Account?.Id.ToString();
                    pay.OrgName = org.OrgName;
                    pay.OrgShortCode = org.OrgShortCode;


                    var repository = new PaymentsRepository(connection.ConnectionString);
                    repository.InsertPayment(pay);
                }

                if (CheckValidDatabase())
                {
                    // clear_table("CreditNotes");

                    //else
                    //{
                    //    clear_table_section("CreditNotes", startDate, endDate, "Date");
                    //}

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertPayments(bulkCopy, entries);
                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table("Payments");

                        //else
                        //{
                        //    azure_clear_table_section("CreditNotes", startDate, endDate, "Date");

                        //}

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertPayments(bulkCopy, entries);
                        }
                    }
                }


                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllPayments method completed.");
            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllPayments XeroApiException completed. Organization :" + (inOrgName ?? "") + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetAllPayments");
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllPayments completed :" + ex.Message, ex);
            }

        }


        protected void GetAllReceipts()
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllReceipts method started.");


                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                DataTable entries = new DataTable("Receipts");
                List<String> IDs = new List<String>();

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn Contact_Id = new DataColumn();
                Contact_Id.DataType = System.Type.GetType("System.String");
                Contact_Id.ColumnName = "Contact_Id";
                entries.Columns.Add(Contact_Id);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.String");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                entries.Columns.Add(LineAmountTypes);

                DataColumn SubTotal = new DataColumn();
                SubTotal.DataType = System.Type.GetType("System.Decimal");
                SubTotal.ColumnName = "SubTotal";
                entries.Columns.Add(SubTotal);

                DataColumn TotalTax = new DataColumn();
                TotalTax.DataType = System.Type.GetType("System.Decimal");
                TotalTax.ColumnName = "TotalTax";
                entries.Columns.Add(TotalTax);

                DataColumn Total = new DataColumn();
                Total.DataType = System.Type.GetType("System.Decimal");
                Total.ColumnName = "Total";
                entries.Columns.Add(Total);

                DataColumn ReceiptID = new DataColumn();
                ReceiptID.DataType = System.Type.GetType("System.String");
                ReceiptID.ColumnName = "ReceiptID";
                entries.Columns.Add(ReceiptID);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn ReceiptNumber = new DataColumn();
                ReceiptNumber.DataType = System.Type.GetType("System.String");
                ReceiptNumber.ColumnName = "ReceiptNumber";
                entries.Columns.Add(ReceiptNumber);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.DateTime");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                entries.Columns.Add(UpdatedDateUTC);

                DataColumn HasAttachments = new DataColumn();
                HasAttachments.DataType = System.Type.GetType("System.Boolean");
                HasAttachments.ColumnName = "HasAttachments";
                entries.Columns.Add(HasAttachments);

                DataColumn Url = new DataColumn();
                Url.DataType = System.Type.GetType("System.String");
                Url.ColumnName = "Url";
                entries.Columns.Add(Url);

                DataColumn XEROUser_Id = new DataColumn();
                XEROUser_Id.DataType = System.Type.GetType("System.String");
                XEROUser_Id.ColumnName = "XEROUser_Id";
                entries.Columns.Add(XEROUser_Id);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                List<Xero.Api.Core.Model.Receipt> all_receipts = new List<Xero.Api.Core.Model.Receipt>();
                all_receipts = api.Receipts
                                .Find()
                                .ToList();
                var connection = new SqlConnectionStringBuilder(user_conn);
                var defaultConnection = new SqlConnectionStringBuilder(system_conn);
                var baseRepository = new BaseRepository<LineItem>(defaultConnection.ConnectionString);
                var org = baseRepository.GetUserOrg(inUserName);

                if (CheckValidDatabase())
                {
                    clear_table("Receipts");
                }
                foreach (Xero.Api.Core.Model.Receipt receipt in all_receipts)
                {
                    var rec = new ClearJelly.ViewModels.UserMappingModels.Receipt();
                    rec.Id = receipt.Id.ToString();
                    rec.Contact_Id = receipt.Contact?.Id.ToString();
                    rec.Date = receipt.Date;
                    rec.HasAttachments = receipt.HasAttachments;
                    rec.LineAmountTypes = receipt.LineAmountTypes.ToString();
                    rec.ReceiptNumber = receipt.ReceiptNumber;
                    rec.Reference = receipt.Reference;
                    rec.Status = receipt.Status.ToString();
                    rec.SubTotal = receipt.SubTotal;
                    rec.Total = receipt.Total;
                    rec.TotalTax = receipt.TotalTax;
                    rec.UpdatedDateUTC = receipt.UpdatedDateUtc;
                    rec.Url = receipt.Url;
                    rec.XEROUserId = receipt.User.Id.ToString();
                    rec.OrgName = org.OrgName;
                    rec.OrgShortCode = org.OrgShortCode;


                    var repository = new ReceiptRepository(connection.ConnectionString);
                    repository.InsertReceipt(rec);

                    foreach (var lineitem in receipt.LineItems)
                    {
                        repository.InsertLineItem(lineitem, receipt.Id.ToString());
                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table("Receipts");

                        //else
                        //{
                        //    azure_clear_table_section("CreditNotes", startDate, endDate, "Date");

                        //}

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertReceipts(bulkCopy, entries);
                        }
                    }
                }
                int x = 1;
                foreach (String id in IDs)
                {
                    System.Threading.Thread.Sleep(1000);

                    Guid receipt_id = new Guid(id);
                    Xero.Api.Core.Model.Receipt receipt = api.Receipts.Find(receipt_id);
                    var lineItemList = new List<LineItem>();
                    //var connection = new SqlConnectionStringBuilder(user_conn);
                    var repository = new BaseRepository<LineItem>(connection.ConnectionString);
                    foreach (LineItem line in receipt.LineItems)
                    {
                        //DataRow row = entries.NewRow();
                        var lineItem = new LineItem();
                        lineItem.AccountCode = line.AccountCode;
                        lineItem.Description = line.Description;
                        lineItem.DiscountRate = line.DiscountRate;
                        lineItem.LineAmount = line.LineAmount;
                        lineItem.Quantity = Decimal.ToInt32(line.Quantity.Value);
                        lineItem.TaxType = line.TaxType;
                        lineItem.UnitAmount = line.UnitAmount;

                        lineItemList.Add(lineItem);

                        repository.InsertReceiptLineItem(lineItem);

                    }
                }

                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllReceipts method completed.");
            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllReceipts XeroApiException completed. Organization :" + (inOrgName ?? "") + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetAllReceipts");
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllReceipts completed :" + ex.Message, ex);
            }

        }


        public void BulkInsertPayments(SqlBulkCopy bulkCopy, DataTable entries)
        {
            bulkCopy.DestinationTableName =
                            "Payments";

            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("CurrencyRate", "CurrencyRate");
            bulkCopy.ColumnMappings.Add("Amount", "Amount");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("PaymentType", "PaymentType");
            bulkCopy.ColumnMappings.Add("XEROAccount_Id", "XEROAccount_Id");
            bulkCopy.ColumnMappings.Add("Invoice_Id", "Invoice_Id");
            bulkCopy.ColumnMappings.Add("CreditNote_Id", "CreditNote_Id");
            bulkCopy.ColumnMappings.Add("OverpaymentId", "OverpaymentId");
            bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
            bulkCopy.ColumnMappings.Add("IsReconciled", "IsReconciled");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            try
            {
                Console.WriteLine("Copying to the relational table...");
                _logger.Info(inUserName + " ~Copying to the relational table...");
                bulkCopy.WriteToServer(entries);
            }
            catch (Exception ex)
            {
                _logger.Fatal(inUserName + " ~Execution of XeroToCJ copying to the relational table. Error details:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }

        public void BulkInsertReceipts(SqlBulkCopy bulkCopy, DataTable entries)
        {
            bulkCopy.DestinationTableName = "Receipts";

            bulkCopy.ColumnMappings.Add("ReceiptID", "ReceiptID");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("Contact_Id", "Contact_Id");
            bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
            bulkCopy.ColumnMappings.Add("ReceiptID", "ReceiptID");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("ReceiptNumber", "ReceiptNumber");
            bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
            bulkCopy.ColumnMappings.Add("HasAttachments", "HasAttachments");
            bulkCopy.ColumnMappings.Add("Url", "Url");
            bulkCopy.ColumnMappings.Add("XEROUser_Id", "XEROUser_Id");
            bulkCopy.ColumnMappings.Add("Reference", "Reference");
            //bulkCopy.ColumnMappings.Add("UnitAmount", "UnitAmount");

            bulkCopy.ColumnMappings.Add("SubTotal", "SubTotal");
            bulkCopy.ColumnMappings.Add("TotalTax", "TotalTax");
            bulkCopy.ColumnMappings.Add("Total", "Total");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            try
            {
                Console.WriteLine("Copying to the relational table...");
                _logger.Info(inUserName + " ~Copying to the relational table...");
                bulkCopy.WriteToServer(entries);
            }
            catch (Exception ex)
            {
                _logger.Fatal(inUserName + " ~Execution of XeroToCJ copying to the relational table. Error details:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }

        public void bulkInsertGetAccounts(SqlBulkCopy bulkCopy, DataTable entries)
        {
            bulkCopy.DestinationTableName =
                           "ChartOfAccounts";
            try
            {
                //Console.WriteLine("Copying to the relational table...");
                _logger.Info(inUserName + " ~Copying to the relational table...");
                bulkCopy.WriteToServer(entries);
                _logger.Info(inUserName + " ~Execution of GetAccounts method completed.");
            }
            catch (SqlException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAccounts  bulk Copy SqlException:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAccounts  bulk Copy :" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }

        //--------------------------------------------------------------------------------------
        protected long ToUnixTime(DateTime dateTime)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ ToUnixTime method started.");
                return (dateTime - UnixEpoch).Ticks / TimeSpan.TicksPerMillisecond;
            }
            catch (Exception ex)
            {

                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ ToUnixTime completed :" + ex.Message, ex);
                throw ex;
            }
        }

        public void GetAllJournalsWithPagination(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllJournalsWithPagination method started.");
                //api.Journals.Find();
                // Get all journals from the general ledger using the ?offset=xxx parameter
                List<Journal> allJournals = new List<Journal>();
                List<Journal> batchOfJournals = new List<Journal>();
                int skip = 290;
                string sqlStr = "";
                DataTable entries = new DataTable("Journals");

                DataColumn JID = new DataColumn();
                JID.DataType = System.Type.GetType("System.String");
                JID.ColumnName = "JID";
                entries.Columns.Add(JID);

                DataColumn JNumber = new DataColumn();
                JNumber.DataType = System.Type.GetType("System.String");
                JNumber.ColumnName = "JNumber";
                entries.Columns.Add(JNumber);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn CreatedDateUTC = new DataColumn();
                CreatedDateUTC.DataType = System.Type.GetType("System.DateTime");
                CreatedDateUTC.ColumnName = "CreatedDateUTC";
                entries.Columns.Add(CreatedDateUTC);


                DataColumn AccountName = new DataColumn();
                AccountName.DataType = System.Type.GetType("System.String");
                AccountName.ColumnName = "AccountName";
                entries.Columns.Add(AccountName);

                DataColumn AccountCode = new DataColumn();
                AccountCode.DataType = System.Type.GetType("System.String");
                AccountCode.ColumnName = "AccountCode";
                entries.Columns.Add(AccountCode);

                DataColumn AccountType = new DataColumn();
                AccountType.DataType = System.Type.GetType("System.String");
                AccountType.ColumnName = "AccountType";
                entries.Columns.Add(AccountType);

                DataColumn NetAmt = new DataColumn();
                NetAmt.DataType = System.Type.GetType("System.Decimal");
                NetAmt.ColumnName = "NetAmt";
                entries.Columns.Add(NetAmt);

                DataColumn GrossAmt = new DataColumn();
                GrossAmt.DataType = System.Type.GetType("System.Decimal");
                GrossAmt.ColumnName = "GrossAmt";
                entries.Columns.Add(GrossAmt);

                DataColumn ValidationErrors = new DataColumn();
                ValidationErrors.DataType = System.Type.GetType("System.String");
                ValidationErrors.ColumnName = "ValidationErrors";
                entries.Columns.Add(ValidationErrors);

                DataColumn ValidationStatus = new DataColumn();
                ValidationStatus.DataType = System.Type.GetType("System.String");
                ValidationStatus.ColumnName = "ValidationStatus";
                entries.Columns.Add(ValidationStatus);

                DataColumn Warnings = new DataColumn();
                Warnings.DataType = System.Type.GetType("System.String");
                Warnings.ColumnName = "Warnings";
                entries.Columns.Add(Warnings);

                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn TrackingCategory1 = new DataColumn();
                TrackingCategory1.DataType = System.Type.GetType("System.String");
                TrackingCategory1.ColumnName = "TrackingCategory1";
                entries.Columns.Add(TrackingCategory1);

                DataColumn TrackingCategory1_Option = new DataColumn();
                TrackingCategory1_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory1_Option.ColumnName = "TrackingCategory1_Option";
                entries.Columns.Add(TrackingCategory1_Option);

                DataColumn TrackingCategory2 = new DataColumn();
                TrackingCategory2.DataType = System.Type.GetType("System.String");
                TrackingCategory2.ColumnName = "TrackingCategory2";
                entries.Columns.Add(TrackingCategory2);

                DataColumn TrackingCategory2_Option = new DataColumn();
                TrackingCategory2_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory2_Option.ColumnName = "TrackingCategory2_Option";
                entries.Columns.Add(TrackingCategory2_Option);

                DataColumn JournalLines = new DataColumn();
                JournalLines.DataType = System.Type.GetType("System.String");
                JournalLines.ColumnName = "JournalLines";
                entries.Columns.Add(JournalLines);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                allJournals.Clear();
                DateTime start = DateTime.Now;

                int lastJournalNumber = 0;
                System.Threading.Thread.Sleep(5000);
                try
                {
                    //var J = api.Journals.ModifiedSince(startDate);
                    //Journal jTmp = api.Journals
                    //              .Where(string.Format("JournalDate > DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day)).OrderBy("JournalNumber").Find().First();    

                    if (api != null)
                    {
                        if (api.Journals != null)
                        {
                            Journal jTmp = api.Journals.ModifiedSince(startDate).OrderBy("JournalNumber").Find().FirstOrDefault();

                            //.Where(string.Format("CreatedDateUTC > DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day))
                            //.Where("JournalDate > "+startDate)
                            //.Where("JournalDate > " + startDate)
                            //  .Where("JournalDate > Date(" + ToUnixTime(startDate) + ")")



                            skip = 0;

                            if (jTmp != null)
                            {
                                lastJournalNumber = jTmp.Number - 1;
                            }


                            int page = 0;

                            //while ((batchOfJournals = api.Journals.Where(string.Format("CreatedDateUTC > DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day)).OrderBy("JournalNumber").Offset(lastJournalNumber).Find().ToList()).Count() > 0)
                            //while ((batchOfJournals = api.Journals.Where("JournalDate > " + startDate).OrderBy("JournalNumber").Offset(lastJournalNumber).Find().ToList()).Count() > 0)
                            while ((batchOfJournals = api.Journals.ModifiedSince(startDate).OrderBy("JournalNumber").Offset(lastJournalNumber).Find().ToList()).Count() > 0)
                            {
                                //Console.WriteLine("Fetched {0} journals from API using skip={1}", batchOfJournals.Count, skip)
                                allJournals.AddRange(batchOfJournals);
                                skip += (batchOfJournals.Count);
                                lastJournalNumber = allJournals.Last().Number;
                                batchOfJournals.Clear();
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                    }
                }
                catch (XeroApiException ex)
                {

                    int y = 0;
                    //writeJournalsToDB(entries);
                    //queue_xero_request("Journals", lastJournalNumber);
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException GetAllJournalsWithPagination reading journals. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                    if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                    {
                        exceptionMethodNameList.Add("GetAllJournalsWithPagination");
                    }

                }
                catch (Exception ex)
                {
                    int y = 0;

                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllJournalsWithPagination reading journals:" + ex.Message, ex);
                }

                //_logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Number of Journals processed:" + allJournals.Count());

                int x = 0;

                entries.Clear();
                try
                {
                    if (allJournals.Any())
                    {

                        foreach (Journal item in allJournals)
                        {

                            //item.JournalLines.Find()
                            foreach (Line line in item.Lines)
                            {
                                //System.Threading.Thread.Sleep(1500);
                                DataRow row = entries.NewRow();
                                row["JID"] = item.Id;
                                row["Reference"] = item.Reference;
                                row["JNumber"] = item.Number;
                                row["Date"] = (item.Date == null) ? DateTime.MinValue : item.Date;
                                row["CreatedDateUTC"] = (item.CreatedDateUtc == null) ? DateTime.MinValue : item.CreatedDateUtc;
                                row["AccountName"] = line.AccountName;
                                row["AccountCode"] = line.AccountCode;
                                row["AccountType"] = line.AccountType;
                                row["NetAmt"] = line.NetAmount;
                                row["GrossAmt"] = line.GrossAmount;


                                row["ValidationStatus"] = line.ValidationStatus;

                                //row["Description"] = line.ToString();

                                if (line.TrackingCategories.Count == 1)
                                {

                                    row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                    row["TrackingCategory1_Option"] = line.TrackingCategories[0].Option;
                                }
                                else
                                {
                                    row["TrackingCategory1"] = null;
                                    row["TrackingCategory1_Option"] = null;
                                    row["TrackingCategory2"] = null;
                                    row["TrackingCategory2_Option"] = null;
                                }

                                if (line.TrackingCategories.Count == 2)
                                {
                                    row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                    row["TrackingCategory1_Option"] = line.TrackingCategories[0].Option;
                                    row["TrackingCategory2"] = line.TrackingCategories[1].Name;
                                    row["TrackingCategory2_Option"] = line.TrackingCategories[1].Option;
                                }
                                row["JournalLines"] = item.Lines.Count;

                                row["OrgShortCode"] = inOrgShortCode;
                                row["OrgName"] = inOrgName;

                                entries.Rows.Add(row);
                                x++;
                            }
                        }
                        _logger.Info(inUserName + " ~There are " + allJournals.Count + " journals in the general ledger, starting with " + allJournals.First().Number + " and ending with " + allJournals.Last().Number);

                    }
                    else
                    {
                        Console.WriteLine("There are no journals in the general ledger.");
                    }

                    _logger.Info("api.Journals.batchOfJournals completed");
                }
                catch (Exception ex)
                {
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ journal lines:" + ex.Message, ex);
                }

                //setApiCallCount(xeroApiCallsCount);

                writeJournalsToDB(entries, startDate, endDate);
            }
            catch (XeroApiException ex)
            {
                int y = 0;
                //writeJournalsToDB(entries);
                //queue_xero_request("Journals", lastJournalNumber);
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException GetAllJournalsWithPagination reading journals.Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetAllJournalsWithPagination");
                }

            }
            catch (Exception ex)
            {

                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllJournalsWithPagination completed :" + ex.Message, ex);
            }
        }
        //--------------------------------------------------------------------------------------
        protected void writeJournalsToDB(DataTable entries, DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ writeJournalsToDB method started.");

                if (CheckValidDatabase())
                {
                    clear_table_modified_after("Journals", startDate, "CreatedDateUTC");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertJournal(bulkCopy, startDate, endDate, entries);
                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table_modified_after("Journals", startDate, "CreatedDateUTC");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {

                            BulkInsertJournal(bulkCopy, startDate, endDate, entries);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ writeJournalsToDB:" + ex.Message, ex);
            }
        }

        public void BulkInsertJournal(SqlBulkCopy bulkCopy, DateTime startDate, DateTime endDate, DataTable entries)
        {
            bulkCopy.DestinationTableName =
                           "Journals";

            // Number of records to be processed in one go
            //sbc.BatchSize = batchSize;

            // Add your column mappings here
            bulkCopy.ColumnMappings.Add("JID", "JID");
            bulkCopy.ColumnMappings.Add("Reference", "Reference");
            bulkCopy.ColumnMappings.Add("JNumber", "JNumber");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("CreatedDateUTC", "CreatedDateUTC");
            bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
            bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
            bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
            bulkCopy.ColumnMappings.Add("NetAmt", "NetAmt");
            bulkCopy.ColumnMappings.Add("GrossAmt", "GrossAmt");
            bulkCopy.ColumnMappings.Add("ValidationErrors", "ValidationErrors");
            bulkCopy.ColumnMappings.Add("ValidationStatus", "ValidationStatus");
            bulkCopy.ColumnMappings.Add("Warnings", "Warnings");
            bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
            bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
            bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
            bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
            bulkCopy.ColumnMappings.Add("JournalLines", "JournalLines");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            try
            {
                //Console.WriteLine("Copying to the relational table...");
                // _logger.Info((Session["Jelly_user"] ?? "").ToString() + " Copying to the relational table...");
                // Write from the source to the destination.

                bulkCopy.WriteToServer(entries);
                //_logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllJournalsWithPagination method completed.");
            }
            catch (SqlException ex)
            {
                _logger.Error(inUserName + " Execution of XeroToCJ relational table:" + ex.Message, ex);
            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ relational table:" + ex.Message, ex);
            }
        }

        //--------------------------------------------------------------------------------------
        protected void GetManualJournalsIDs(DateTime startDate, DateTime endDate)
        {
            List<Guid> IDs = new List<Guid>();
            try
            {
                System.Threading.Thread.Sleep(5000);
                _logger.Info(inUserName + " ~Execution XeroToCJ of GetManualJournalsIDs method started.");
                List<ManualJournal> journals = api.ManualJournals
                    .Where(string.Format("Date >= DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day)).Find().ToList();


                int page = 0;

                foreach (ManualJournal manualJoural in journals)
                {
                    IDs.Add(manualJoural.Id);
                }

            }
            catch (XeroApiException ex)
            {
                int y = 0;
                //writeJournalsToDB(entries);
                //queue_xero_request("Journals", lastJournalNumber);
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException GetManualJournalsIDs method completed. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetManualJournalsIDs");
                }
            }
            catch (Exception ex)
            {
                int y = 0;
                _logger.Error(inUserName + " ~Execution XeroToCJ of GetManualJournalsIDs method completed.:" + ex.Message, ex);
            }

            GetManualJournalsDetails(IDs, startDate, endDate);
            //_logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Number of Journals processed:" + allJournals.Count());
        }
        //--------------------------------------------------------------------------------------
        protected void GetManualJournalsDetails(List<Guid> IDs, DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetManualJournalsDetails method started.");

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                DataTable entries = new DataTable("ManualJournals");

                DataColumn JournalID = new DataColumn();
                JournalID.DataType = System.Type.GetType("System.String");
                JournalID.ColumnName = "JournalID";
                entries.Columns.Add(JournalID);

                DataColumn Narration = new DataColumn();
                Narration.DataType = System.Type.GetType("System.String");
                Narration.ColumnName = "Narration";
                entries.Columns.Add(Narration);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.String");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn AccountType = new DataColumn();
                AccountType.DataType = System.Type.GetType("System.String");
                AccountType.ColumnName = "AccountType";
                entries.Columns.Add(AccountType);

                DataColumn AccountCode = new DataColumn();
                AccountCode.DataType = System.Type.GetType("System.String");
                AccountCode.ColumnName = "AccountCode";
                entries.Columns.Add(AccountCode);

                DataColumn AccountId = new DataColumn();
                AccountId.DataType = System.Type.GetType("System.String");
                AccountId.ColumnName = "AccountId";
                entries.Columns.Add(AccountId);

                DataColumn AccountName = new DataColumn();
                AccountName.DataType = System.Type.GetType("System.String");
                AccountName.ColumnName = "AccountName";
                entries.Columns.Add(AccountName);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.String");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                entries.Columns.Add(LineAmountTypes);

                DataColumn Amount = new DataColumn();
                Amount.DataType = System.Type.GetType("System.String");
                Amount.ColumnName = "Amount";
                entries.Columns.Add(Amount);

                DataColumn NetAmount = new DataColumn();
                NetAmount.DataType = System.Type.GetType("System.String");
                NetAmount.ColumnName = "NetAmount";
                entries.Columns.Add(NetAmount);

                DataColumn GrossAmount = new DataColumn();
                GrossAmount.DataType = System.Type.GetType("System.String");
                GrossAmount.ColumnName = "GrossAmount";
                entries.Columns.Add(GrossAmount);

                DataColumn TaxAmount = new DataColumn();
                TaxAmount.DataType = System.Type.GetType("System.String");
                TaxAmount.ColumnName = "TaxAmount";
                entries.Columns.Add(TaxAmount);

                DataColumn TaxName = new DataColumn();
                TaxName.DataType = System.Type.GetType("System.String");
                TaxName.ColumnName = "TaxName";
                entries.Columns.Add(TaxName);

                DataColumn TaxType = new DataColumn();
                TaxType.DataType = System.Type.GetType("System.String");
                TaxType.ColumnName = "TaxType";
                entries.Columns.Add(TaxType);

                DataColumn ValidationErrors = new DataColumn();
                ValidationErrors.DataType = System.Type.GetType("System.String");
                ValidationErrors.ColumnName = "ValidationErrors";
                entries.Columns.Add(ValidationErrors);

                DataColumn Url = new DataColumn();
                Url.DataType = System.Type.GetType("System.String");
                Url.ColumnName = "Url";
                entries.Columns.Add(Url);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn HasAttachments = new DataColumn();
                HasAttachments.DataType = System.Type.GetType("System.String");
                HasAttachments.ColumnName = "HasAttachments";
                entries.Columns.Add(HasAttachments);

                DataColumn TrackingCategory1 = new DataColumn();
                TrackingCategory1.DataType = System.Type.GetType("System.String");
                TrackingCategory1.ColumnName = "TrackingCategory1";
                entries.Columns.Add(TrackingCategory1);

                DataColumn TrackingCategory1_Option = new DataColumn();
                TrackingCategory1_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory1_Option.ColumnName = "TrackingCategory1_Option";
                entries.Columns.Add(TrackingCategory1_Option);

                DataColumn TrackingCategory2 = new DataColumn();
                TrackingCategory2.DataType = System.Type.GetType("System.String");
                TrackingCategory2.ColumnName = "TrackingCategory2";
                entries.Columns.Add(TrackingCategory2);

                DataColumn TrackingCategory2_Option = new DataColumn();
                TrackingCategory2_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory2_Option.ColumnName = "TrackingCategory2_Option";
                entries.Columns.Add(TrackingCategory2_Option);

                DataColumn UpdatedDateUtc = new DataColumn();
                UpdatedDateUtc.DataType = System.Type.GetType("System.String");
                UpdatedDateUtc.ColumnName = "UpdatedDateUtc";
                entries.Columns.Add(UpdatedDateUtc);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                try
                {
                    foreach (Guid jID in IDs)
                    {
                        System.Threading.Thread.Sleep(1000);
                        ManualJournal journal = api.ManualJournals
                            .Where(string.Format("Date >= DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day)).Find(jID.ToString());
                        foreach (Line line in journal.Lines)
                        {
                            DataRow row = entries.NewRow();

                            row["JournalID"] = journal.Id;
                            row["Narration"] = journal.Narration;
                            row["AccountCode"] = line.AccountCode;
                            if (journal.Date == null)
                            {
                                row["Date"] = null;
                            }
                            else
                            {
                                row["Date"] = journal.Date.ToString("dd/MMM/yyyy");
                            }
                            row["AccountType"] = line.AccountType;
                            row["AccountCode"] = line.AccountCode;
                            row["AccountName"] = line.AccountName;
                            row["AccountId"] = line.AccountId;
                            row["LineAmountTypes"] = journal.LineAmountTypes;
                            row["Amount"] = line.Amount;
                            row["NetAmount"] = line.NetAmount;
                            row["GrossAmount"] = line.GrossAmount;

                            row["TaxAmount"] = line.TaxAmount;
                            row["TaxName"] = line.TaxName;
                            row["TaxType"] = line.TaxType;
                            if (line.TrackingCategories != null)
                            {
                                if (line.TrackingCategories.Count == 1)
                                {
                                    row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                    row["TrackingCategory1_option"] = line.TrackingCategories[0].Option;
                                }
                                else
                                {
                                    row["TrackingCategory1"] = null;
                                    row["TrackingCategory1_option"] = null;
                                    row["TrackingCategory2"] = null;
                                    row["TrackingCategory2_Option"] = null;
                                }

                                if (line.TrackingCategories.Count == 2)
                                {
                                    row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                    row["TrackingCategory1_option"] = line.TrackingCategories[0].Option;
                                    row["TrackingCategory2"] = line.TrackingCategories[1].Name;
                                    row["TrackingCategory2_Option"] = line.TrackingCategories[1].Option;
                                }
                            }


                            row["LineAmountTypes"] = journal.LineAmountTypes;
                            row["Status"] = journal.Status;
                            if (journal.UpdatedDateUtc == null)
                            {
                                row["UpdatedDateUtc"] = null;
                            }
                            else
                            {
                                row["UpdatedDateUtc"] = journal.UpdatedDateUtc.Value.ToString("dd/MMM/yyyy");
                            }
                            row["Url"] = journal.Url;
                            row["HasAttachments"] = journal.HasAttachments;
                            row["OrgShortCode"] = selectedOrgShortCode;
                            row["OrgName"] = selectedOrgName;


                            entries.Rows.Add(row);
                        }

                    }
                }
                catch (XeroApiException ex)
                {
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException reading manual journals details. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                    if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                    {
                        exceptionMethodNameList.Add("GetManualJournalsDetails");
                    }
                    //_logger.Fatal((inUserName ?? "").ToString() + " ~xero api error reading manual journals details:" + ex.Message);
                    //_logger.Fatal((inUserName ?? "").ToString() + " ~xero api error reading manual journals details:" + ex.StackTrace.ToString());
                }
                catch (Exception ex)
                {
                    //_logger.Fatal((inUserName ?? "").ToString() + " ~general error reading journals details:" + ex.Message);
                    //_logger.Fatal((inUserName ?? "").ToString() + " ~general error reading journals details:" + ex.StackTrace.ToString());
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ reading manual journals details:" + ex.Message, ex);
                }

                if (CheckValidDatabase())
                {
                    clear_table_section("ManualJournals", startDate, endDate, "Date");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {

                        BulkInsertManualJournals(bulkCopy, startDate, endDate, entries);

                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table_section("ManualJournals", startDate, endDate, "Date");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {

                            BulkInsertManualJournals(bulkCopy, startDate, endDate, entries);


                        }
                    }
                }
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetManualJournalsDetails method completed.");
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetManualJournalsDetails completed.:" + ex.Message, ex);
                // throw;
            }
        }


        public void BulkInsertManualJournals(SqlBulkCopy bulkCopy, DateTime startDate, DateTime endDate, DataTable entries)
        {
            bulkCopy.DestinationTableName =
                               "ManualJournals";
            bulkCopy.ColumnMappings.Add("JournalID", "JournalID");
            bulkCopy.ColumnMappings.Add("Narration", "Narration");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("UpdatedDateUtc", "UpdatedDateUtc");
            bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
            bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
            bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
            bulkCopy.ColumnMappings.Add("AccountId", "AccountId");
            bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
            bulkCopy.ColumnMappings.Add("Amount", "Amount");
            bulkCopy.ColumnMappings.Add("NetAmount", "NetAmount");
            bulkCopy.ColumnMappings.Add("GrossAmount", "GrossAmount");
            bulkCopy.ColumnMappings.Add("TaxAmount", "TaxAmount");
            bulkCopy.ColumnMappings.Add("TaxName", "TaxName");
            bulkCopy.ColumnMappings.Add("TaxType", "TaxType");
            bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
            bulkCopy.ColumnMappings.Add("TrackingCategory1_option", "TrackingCategory1_option");
            bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
            bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("Url", "Url");
            bulkCopy.ColumnMappings.Add("HasAttachments", "HasAttachments");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            try
            {
                Console.WriteLine("Execution of XeroToCJ Copying manual journals to the relational table.");
                // Write from the source to the destination.

                bulkCopy.WriteToServer(entries);
                _logger.Info(inUserName + " ~Execution of XeroToCJ Manual Journals method completed.");
            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ copying to the relational table:" + ex.Message, ex);

            }
        }

        //--------------------------------------------------------------------------------------
        protected void GetAllCreditNotesWithPagination(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllCreditNotesWithPagination method started.");


                int fullUpdateFlag = 1;

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                DataTable entries = new DataTable("CreditNotes");
                List<String> IDs = new List<String>();

                DataColumn Allocations = new DataColumn();
                Allocations.DataType = System.Type.GetType("System.String");
                Allocations.ColumnName = "Allocations";
                entries.Columns.Add(Allocations);

                DataColumn AppliedAmount = new DataColumn();
                AppliedAmount.DataType = System.Type.GetType("System.String");
                AppliedAmount.ColumnName = "AppliedAmount";
                entries.Columns.Add(AppliedAmount);

                DataColumn BrandingThemeID = new DataColumn();
                BrandingThemeID.DataType = System.Type.GetType("System.String");
                BrandingThemeID.ColumnName = "BrandingThemeID";
                entries.Columns.Add(BrandingThemeID);

                DataColumn ContactID = new DataColumn();
                ContactID.DataType = System.Type.GetType("System.String");
                ContactID.ColumnName = "ContactID";
                entries.Columns.Add(ContactID);

                DataColumn CreditNoteID = new DataColumn();
                CreditNoteID.DataType = System.Type.GetType("System.String");
                CreditNoteID.ColumnName = "CreditNoteID";
                entries.Columns.Add(CreditNoteID);

                DataColumn CreditNoteNumber = new DataColumn();
                CreditNoteNumber.DataType = System.Type.GetType("System.String");
                CreditNoteNumber.ColumnName = "CreditNoteNumber";
                entries.Columns.Add(CreditNoteNumber);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                entries.Columns.Add(CurrencyCode);

                DataColumn CurrencyRate = new DataColumn();
                CurrencyRate.DataType = System.Type.GetType("System.Decimal");
                CurrencyRate.ColumnName = "CurrencyRate";
                entries.Columns.Add(CurrencyRate);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn DueDate = new DataColumn();
                DueDate.DataType = System.Type.GetType("System.DateTime");
                DueDate.ColumnName = "DueDate";
                entries.Columns.Add(DueDate);

                DataColumn FullyPaidOnDate = new DataColumn();
                FullyPaidOnDate.DataType = System.Type.GetType("System.DateTime");
                FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
                entries.Columns.Add(FullyPaidOnDate);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.Decimal");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                entries.Columns.Add(LineAmountTypes);

                DataColumn UnitAmount = new DataColumn();
                UnitAmount.DataType = System.Type.GetType("System.Decimal");
                UnitAmount.ColumnName = "UnitAmount";
                entries.Columns.Add(UnitAmount);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn RemainingCredit = new DataColumn();
                RemainingCredit.DataType = System.Type.GetType("System.String");
                RemainingCredit.ColumnName = "RemainingCredit";
                entries.Columns.Add(RemainingCredit);

                DataColumn SentToContact = new DataColumn();
                SentToContact.DataType = System.Type.GetType("System.String");
                SentToContact.ColumnName = "SentToContact";
                entries.Columns.Add(SentToContact);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn SubTotal = new DataColumn();
                SubTotal.DataType = System.Type.GetType("System.Decimal");
                SubTotal.ColumnName = "SubTotal";
                entries.Columns.Add(SubTotal);

                DataColumn Total = new DataColumn();
                Total.DataType = System.Type.GetType("System.Decimal");
                Total.ColumnName = "Total";
                entries.Columns.Add(Total);

                DataColumn TotalTax = new DataColumn();
                TotalTax.DataType = System.Type.GetType("System.Decimal");
                TotalTax.ColumnName = "TotalTax";
                entries.Columns.Add(TotalTax);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                entries.Columns.Add(Type);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.DateTime");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                entries.Columns.Add(UpdatedDateUTC);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                int x = 1;

                List<CreditNote> all_notes = new List<CreditNote>();
                //CreditNotes.Where(c => c.Date >= selectedDate).ToList()
                all_notes = api.CreditNotes
                                .Where(string.Format("Date >= DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day))
                                .And(string.Format("Date <= DateTime({0},{1},{2})", endDate.Year, endDate.Month, endDate.Day))
                                .Find()
                                .ToList();

                ;

                foreach (CreditNote credit in all_notes)
                {
                    DataRow row = entries.NewRow();
                    IDs.Add(credit.Id.ToString());
                    row["Allocations"] = credit.Allocations.Count;
                    row["AppliedAmount"] = credit.AppliedAmount;
                    //row["BrandingThemeID"] = credit.;
                    row["ContactID"] = credit.Contact.Id;
                    row["CreditNoteID"] = credit.Id;
                    row["CreditNoteNumber"] = credit.Number;
                    row["CurrencyCode"] = credit.CurrencyCode;
                    row["CurrencyRate"] = credit.CurrencyRate;
                    if (credit.Date == null)
                    {
                        //row["Date"] = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue; 
                    }
                    else
                    {
                        row["Date"] = credit.Date;//.ToString("dd/MMM/yyyy");
                    }

                    if (credit.DueDate == null)
                    {
                        //row["Date"] = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue; 
                    }
                    else
                    {
                        row["DueDate"] = credit.DueDate;//.ToString("dd/MMM/yyyy");
                    }

                    if (credit.FullyPaidOnDate == null)
                    {
                        //row["FullyPaidOnDate"] = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue; 
                    }
                    else
                    {
                        row["FullyPaidOnDate"] = credit.FullyPaidOnDate;//.ToString("dd/MMM/yyyy");
                    }


                    row["LineAmountTypes"] = credit.LineAmountTypes;
                    row["Reference"] = credit.Reference;
                    row["RemainingCredit"] = credit.RemainingCredit;
                    row["SentToContact"] = credit.SentToContact;
                    row["Status"] = credit.Status;
                    row["SubTotal"] = credit.SubTotal;
                    row["Total"] = credit.Total;
                    row["TotalTax"] = credit.TotalTax;
                    row["Type"] = credit.Type;
                    row["UpdatedDateUTC"] = credit.UpdatedDateUtc ?? DateTime.MinValue;//.Value.ToString("dd/MMM/yyyy");            
                    row["OrgShortCode"] = selectedOrgShortCode;
                    row["OrgName"] = selectedOrgName;

                    entries.Rows.Add(row);
                }



                if (CheckValidDatabase())
                {
                    if (fullUpdateFlag == 1)
                    {
                        clear_table("CreditNotes");
                    }
                    else
                    {
                        clear_table_section("CreditNotes", startDate, endDate, "Date");

                    }

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertCreditNotes(bulkCopy, startDate, endDate, entries, fullUpdateFlag);


                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {

                        if (fullUpdateFlag == 1)
                        {
                            azure_clear_table("CreditNotes");
                        }
                        else
                        {
                            azure_clear_table_section("CreditNotes", startDate, endDate, "Date");

                        }

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertCreditNotes(bulkCopy, startDate, endDate, entries, fullUpdateFlag);


                        }
                    }
                }
                GetCreditNotesByDetail(IDs, startDate, endDate);

                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllCreditNotesWithPagination method completed.");
            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllCreditNotesWithPagination XeroApiException completed. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetAllCreditNotesWithPagination");
                }
            }
            catch (Exception ex)
            {

                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllCreditNotesWithPagination completed :" + ex.Message, ex);
            }
        }


        public void BulkInsertCreditNotes(SqlBulkCopy bulkCopy, DateTime startDate, DateTime endDate, DataTable entries, int fullUpdateFlag)
        {
            bulkCopy.DestinationTableName =
                            "CreditNotes";

            // Number of records to be processed in one go
            //sbc.BatchSize = batchSize;

            // Add your column mappings here
            bulkCopy.ColumnMappings.Add("Allocations", "Allocations");
            bulkCopy.ColumnMappings.Add("AppliedAmount", "AppliedAmount");
            bulkCopy.ColumnMappings.Add("BrandingThemeID", "BrandingThemeID");
            bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
            bulkCopy.ColumnMappings.Add("CreditNoteID", "CreditNoteID");
            bulkCopy.ColumnMappings.Add("CreditNoteNumber", "CreditNoteNumber");
            bulkCopy.ColumnMappings.Add("CurrencyCode", "CurrencyCode");
            bulkCopy.ColumnMappings.Add("CurrencyRate", "CurrencyRate");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("DueDate", "DueDate");
            bulkCopy.ColumnMappings.Add("FullyPaidOnDate", "FullyPaidOnDate");
            bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
            bulkCopy.ColumnMappings.Add("UnitAmount", "UnitAmount");
            bulkCopy.ColumnMappings.Add("Reference", "Reference");
            bulkCopy.ColumnMappings.Add("RemainingCredit", "RemainingCredit");
            bulkCopy.ColumnMappings.Add("SentToContact", "SentToContact");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("SubTotal", "SubTotal");
            bulkCopy.ColumnMappings.Add("Total", "Total");
            bulkCopy.ColumnMappings.Add("TotalTax", "TotalTax");
            bulkCopy.ColumnMappings.Add("Type", "Type");
            bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            try
            {
                Console.WriteLine("Copying to the relational table...");
                _logger.Info(inUserName + " ~Copying to the relational table...");
                // Write from the source to the destination.

                bulkCopy.WriteToServer(entries);

            }
            catch (Exception ex)
            {
                _logger.Fatal(inUserName + " ~Execution of XeroToCJ copying to the relational table. Error details:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }


        public bool CheckValidDatabase()
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ CheckValidDatabase method started.");
                var companyName = CommonXero.GetCompanyNameByUserId(inUserName);
                var finalCompanyName = "Xero_" + companyName;
                var connection = new SqlConnectionStringBuilder(user_conn);
                string dbName = connection.InitialCatalog;
                if (finalCompanyName.ToLower() == dbName.ToLower())
                {
                    return true;
                }
                else
                {
                    CommonXero.SendMailOnDatabaseDontMatch(inUserName, companyName, dbName);
                    return false;
                }

            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ CheckValidDatabase method completed.", ex);
                throw;
            }
        }

        public bool CheckAzureValidDatabase()
        {
            try
            {

                _logger.Info(inUserName + " ~Execution of XeroToCJ CheckAzureValidDatabase method started.");
                var companyName = CommonXero.GetCompanyNameByUserId(inUserName);
                var finalCompanyName = "Xero_" + companyName;

                if (CommonXero.CheckDatabaseExists(finalCompanyName))
                {
                    var connection = new SqlConnectionStringBuilder(azure_user_conn);
                    string dbName = connection.InitialCatalog;
                    if (finalCompanyName.ToLower() == dbName.ToLower())
                    {
                        return true;
                    }
                    else
                    {
                        CommonXero.SendMailOnDatabaseDontMatch(inUserName, companyName, dbName);
                        return false;
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ CheckAzureValidDatabase method completed.", ex);
                throw;
            }
        }

        protected void GetCreditNotesByDetail(List<String> IDs, DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetCreditNotesByDetail method started.");



                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;


                DataTable entries = new DataTable("CreditNotesDetails");


                DataColumn InvoiceID = new DataColumn();
                InvoiceID.DataType = System.Type.GetType("System.String");
                InvoiceID.ColumnName = "InvoiceID";
                entries.Columns.Add(InvoiceID);

                DataColumn AccountCode = new DataColumn();
                AccountCode.DataType = System.Type.GetType("System.String");
                AccountCode.ColumnName = "AccountCode";
                entries.Columns.Add(AccountCode);


                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn DiscountRate = new DataColumn();
                DiscountRate.DataType = System.Type.GetType("System.String");
                DiscountRate.ColumnName = "DiscountRate";
                entries.Columns.Add(DiscountRate);


                DataColumn ItemCode = new DataColumn();
                ItemCode.DataType = System.Type.GetType("System.String");
                ItemCode.ColumnName = "ItemCode";
                entries.Columns.Add(ItemCode);


                DataColumn LineAmount = new DataColumn();
                LineAmount.DataType = System.Type.GetType("System.String");
                LineAmount.ColumnName = "LineAmount";
                entries.Columns.Add(LineAmount);

                DataColumn Quantity = new DataColumn();
                Quantity.DataType = System.Type.GetType("System.String");
                Quantity.ColumnName = "Quantity";
                entries.Columns.Add(Quantity);

                DataColumn TaxAmount = new DataColumn();
                TaxAmount.DataType = System.Type.GetType("System.String");
                TaxAmount.ColumnName = "TaxAmount";
                entries.Columns.Add(TaxAmount);


                DataColumn TaxType = new DataColumn();
                TaxType.DataType = System.Type.GetType("System.String");
                TaxType.ColumnName = "TaxType";
                entries.Columns.Add(TaxType);

                /*DataColumn TrackingID = new DataColumn();
                TrackingID.DataType = System.Type.GetType("System.String");
                TrackingID.ColumnName = "TrackingID";
                entries.Columns.Add(TrackingID);

                DataColumn TrackingOption = new DataColumn();
                TrackingOption.DataType = System.Type.GetType("System.String");
                TrackingOption.ColumnName = "TrackingOption";
                entries.Columns.Add(TrackingOption);*/

                DataColumn TrackingCategory1 = new DataColumn();
                TrackingCategory1.DataType = System.Type.GetType("System.String");
                TrackingCategory1.ColumnName = "TrackingCategory1";
                entries.Columns.Add(TrackingCategory1);

                DataColumn TrackingCategory1_Option = new DataColumn();
                TrackingCategory1_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory1_Option.ColumnName = "TrackingCategory1_Option";
                entries.Columns.Add(TrackingCategory1_Option);

                DataColumn TrackingCategory2 = new DataColumn();
                TrackingCategory2.DataType = System.Type.GetType("System.String");
                TrackingCategory2.ColumnName = "TrackingCategory2";
                entries.Columns.Add(TrackingCategory2);

                DataColumn TrackingCategory2_Option = new DataColumn();
                TrackingCategory2_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory2_Option.ColumnName = "TrackingCategory2_Option";
                entries.Columns.Add(TrackingCategory2_Option);


                DataColumn Allocations = new DataColumn();
                Allocations.DataType = System.Type.GetType("System.String");
                Allocations.ColumnName = "Allocations";
                entries.Columns.Add(Allocations);

                DataColumn AppliedAmount = new DataColumn();
                AppliedAmount.DataType = System.Type.GetType("System.String");
                AppliedAmount.ColumnName = "AppliedAmount";
                entries.Columns.Add(AppliedAmount);

                DataColumn BrandingThemeID = new DataColumn();
                BrandingThemeID.DataType = System.Type.GetType("System.String");
                BrandingThemeID.ColumnName = "BrandingThemeID";
                entries.Columns.Add(BrandingThemeID);

                DataColumn ContactID = new DataColumn();
                ContactID.DataType = System.Type.GetType("System.String");
                ContactID.ColumnName = "ContactID";
                entries.Columns.Add(ContactID);

                DataColumn CreditNoteID = new DataColumn();
                CreditNoteID.DataType = System.Type.GetType("System.String");
                CreditNoteID.ColumnName = "CreditNoteID";
                entries.Columns.Add(CreditNoteID);

                DataColumn CreditNoteNumber = new DataColumn();
                CreditNoteNumber.DataType = System.Type.GetType("System.String");
                CreditNoteNumber.ColumnName = "CreditNoteNumber";
                entries.Columns.Add(CreditNoteNumber);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                entries.Columns.Add(CurrencyCode);

                DataColumn CurrencyRate = new DataColumn();
                CurrencyRate.DataType = System.Type.GetType("System.String");
                CurrencyRate.ColumnName = "CurrencyRate";
                entries.Columns.Add(CurrencyRate);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.String");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn DueDate = new DataColumn();
                DueDate.DataType = System.Type.GetType("System.String");
                DueDate.ColumnName = "DueDate";
                entries.Columns.Add(DueDate);

                DataColumn FullyPaidOnDate = new DataColumn();
                FullyPaidOnDate.DataType = System.Type.GetType("System.String");
                FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
                entries.Columns.Add(FullyPaidOnDate);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.String");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                entries.Columns.Add(LineAmountTypes);

                DataColumn UnitAmount = new DataColumn();
                UnitAmount.DataType = System.Type.GetType("System.String");
                UnitAmount.ColumnName = "UnitAmount";
                entries.Columns.Add(UnitAmount);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn RemainingCredit = new DataColumn();
                RemainingCredit.DataType = System.Type.GetType("System.String");
                RemainingCredit.ColumnName = "RemainingCredit";
                entries.Columns.Add(RemainingCredit);

                DataColumn SentToContact = new DataColumn();
                SentToContact.DataType = System.Type.GetType("System.String");
                SentToContact.ColumnName = "SentToContact";
                entries.Columns.Add(SentToContact);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn SubTotal = new DataColumn();
                SubTotal.DataType = System.Type.GetType("System.String");
                SubTotal.ColumnName = "SubTotal";
                entries.Columns.Add(SubTotal);

                DataColumn Total = new DataColumn();
                Total.DataType = System.Type.GetType("System.String");
                Total.ColumnName = "Total";
                entries.Columns.Add(Total);

                DataColumn TotalTax = new DataColumn();
                TotalTax.DataType = System.Type.GetType("System.String");
                TotalTax.ColumnName = "TotalTax";
                entries.Columns.Add(TotalTax);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                entries.Columns.Add(Type);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.String");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                entries.Columns.Add(UpdatedDateUTC);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                int x = 1;
                foreach (String id in IDs)
                {
                    System.Threading.Thread.Sleep(1000);

                    Guid note_id = new Guid(id);
                    CreditNote note = api.CreditNotes.Find(note_id);
                    foreach (LineItem line in note.LineItems)
                    {
                        DataRow row = entries.NewRow();

                        row["InvoiceID"] = null;
                        row["AccountCode"] = line.AccountCode;
                        row["Description"] = line.Description;
                        row["DiscountRate"] = line.DiscountRate;
                        row["ItemCode"] = line.ItemCode;
                        row["LineAmount"] = line.LineAmount;
                        row["Quantity"] = line.Quantity;
                        row["TaxAmount"] = line.TaxAmount;
                        row["TaxType"] = line.TaxType;

                        row["Allocations"] = note.Allocations.Count;
                        row["AppliedAmount"] = note.AppliedAmount;
                        row["BrandingThemeID"] = note.BrandingThemeId;
                        row["ContactID"] = note.Contact.Id;
                        row["CreditNoteID"] = note.Id;
                        row["CreditNoteNumber"] = note.Number;
                        row["CurrencyCode"] = note.CurrencyCode;
                        row["CurrencyRate"] = note.CurrencyRate;
                        if (note.Date == null)
                        {
                            row["Date"] = null;
                        }
                        else
                        {
                            row["Date"] = note.Date.Value.ToString("dd/MMM/yyyy");
                            //row["Date"] = note.Date.ToString("dd/MMM/yyyy");
                        }

                        if (note.DueDate == null)
                        {
                            row["DueDate"] = null;
                        }
                        else
                        {
                            row["DueDate"] = note.DueDate.Value.ToString("dd/MMM/yyyy");
                            //row["DueDate"] = note.DueDate.ToString();
                        }
                        if (note.FullyPaidOnDate == null)
                        {
                            row["FullyPaidOnDate"] = null;
                        }
                        else
                        {
                            row["FullyPaidOnDate"] = note.FullyPaidOnDate.ToString("dd/MMM/yyyy");
                        }


                        if (line.Tracking.Count == 2)
                        {
                            row["TrackingCategory1"] = line.Tracking[0].Name;
                            row["TrackingCategory1_Option"] = line.Tracking[0].Option;
                            row["TrackingCategory2"] = line.Tracking[1].Name;
                            row["TrackingCategory2_Option"] = line.Tracking[1].Option;
                        }
                        else if (line.Tracking.Count == 1)
                        {

                            row["TrackingCategory1"] = line.Tracking[0].Name;
                            row["TrackingCategory1_Option"] = line.Tracking[0].Option;
                        }
                        else
                        {
                            row["TrackingCategory1"] = null;
                            row["TrackingCategory1_Option"] = null;
                            row["TrackingCategory2"] = null;
                            row["TrackingCategory2_Option"] = null;
                        }
                        row["LineAmountTypes"] = note.LineAmountTypes;
                        row["UnitAmount"] = line.UnitAmount;
                        row["Reference"] = note.Reference;
                        row["RemainingCredit"] = note.RemainingCredit;
                        row["SentToContact"] = note.SentToContact;
                        row["Status"] = note.Status;
                        row["SubTotal"] = note.SubTotal;
                        row["Total"] = note.Total;
                        row["TotalTax"] = note.TotalTax;
                        row["Type"] = note.Type;
                        if (note.UpdatedDateUtc == null)
                        {
                            row["UpdatedDateUTC"] = null;
                        }
                        else
                        {
                            row["UpdatedDateUTC"] = note.UpdatedDateUtc.Value.ToString("dd/MMM/yyyy");
                        }

                        row["OrgShortCode"] = selectedOrgShortCode;
                        row["OrgName"] = selectedOrgName;


                        entries.Rows.Add(row);
                    }
                }



                if (CheckValidDatabase())
                {
                    clear_table_section("CreditNotesDetails", startDate, endDate, "Date");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertCreditNotesDetails(bulkCopy, startDate, endDate, entries);
                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table_section("CreditNotesDetails", startDate, endDate, "Date");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertCreditNotesDetails(bulkCopy, startDate, endDate, entries);
                        }
                    }
                }
            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetAllCreditNotesWithPagination XeroApiException GetCreditNotesByDetail completed. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetCreditNotesByDetail");
                }
            }

            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetCreditNotesByDetail completed:" + ex.Message, ex);
            }
        }

        public void BulkInsertCreditNotesDetails(SqlBulkCopy bulkCopy, DateTime startDate, DateTime endDate, DataTable entries)
        {
            bulkCopy.DestinationTableName =
                            "CreditNotesDetails";

            // Number of records to be processed in one go
            //sbc.BatchSize = batchSize;

            // Add your column mappings here
            bulkCopy.ColumnMappings.Add("InvoiceID", "InvoiceID");
            bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
            bulkCopy.ColumnMappings.Add("Description", "Description");
            bulkCopy.ColumnMappings.Add("DiscountRate", "DiscountRate");
            bulkCopy.ColumnMappings.Add("ItemCode", "ItemCode");
            bulkCopy.ColumnMappings.Add("LineAmount", "LineAmount");
            bulkCopy.ColumnMappings.Add("Quantity", "Quantity");
            bulkCopy.ColumnMappings.Add("TaxAmount", "TaxAmount");
            bulkCopy.ColumnMappings.Add("TaxType", "TaxType");
            bulkCopy.ColumnMappings.Add("Allocations", "Allocations");
            bulkCopy.ColumnMappings.Add("AppliedAmount", "AppliedAmount");
            bulkCopy.ColumnMappings.Add("BrandingThemeID", "BrandingThemeID");
            bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
            bulkCopy.ColumnMappings.Add("CreditNoteID", "CreditNoteID");
            bulkCopy.ColumnMappings.Add("CreditNoteNumber", "CreditNoteNumber");
            bulkCopy.ColumnMappings.Add("CurrencyCode", "CurrencyCode");
            bulkCopy.ColumnMappings.Add("CurrencyRate", "CurrencyRate");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("DueDate", "DueDate");
            bulkCopy.ColumnMappings.Add("FullyPaidOnDate", "FullyPaidOnDate");
            bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
            bulkCopy.ColumnMappings.Add("UnitAmount", "UnitAmount");
            bulkCopy.ColumnMappings.Add("Reference", "Reference");
            bulkCopy.ColumnMappings.Add("RemainingCredit", "RemainingCredit");
            bulkCopy.ColumnMappings.Add("SentToContact", "SentToContact");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("SubTotal", "SubTotal");
            bulkCopy.ColumnMappings.Add("Total", "Total");
            bulkCopy.ColumnMappings.Add("TotalTax", "TotalTax");
            bulkCopy.ColumnMappings.Add("Type", "Type");
            bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
            bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
            bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
            bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");

            try
            {
                Console.WriteLine("Copying to the relational table...");
                _logger.Info(inUserName + " ~Copying to the relational table...");





                bulkCopy.WriteToServer(entries);
                _logger.Info(inUserName + " ~Execution of GetCreditNotesByDetail method completed.");

            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ copying to the relational table." + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }

        //--------------------------------------------------------------------------------------

        protected void GetAllInvoicesWithPagination(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllInvoicesWithPagination method started.");

                int fullUpdateFlag = 1;

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                // Get all journals from the general ledger using the ?offset=xxx parameter
                List<Invoice> allInvoices = new List<Invoice>();
                List<Invoice> batchOfInvoices;
                List<String> IDs = new List<String>();
                int skip = 0;
                string sqlStr = "";
                DataTable entries = new DataTable("Invoices");


                DataColumn AmountCredited = new DataColumn();
                AmountCredited.DataType = System.Type.GetType("System.Decimal");
                AmountCredited.ColumnName = "AmountCredited";
                entries.Columns.Add(AmountCredited);

                DataColumn AmountDue = new DataColumn();
                AmountDue.DataType = System.Type.GetType("System.Decimal");
                AmountDue.ColumnName = "AmountDue";
                entries.Columns.Add(AmountDue);

                DataColumn AmountPaid = new DataColumn();
                AmountPaid.DataType = System.Type.GetType("System.Decimal");
                AmountPaid.ColumnName = "AmountPaid";
                entries.Columns.Add(AmountPaid);

                DataColumn BrandingThemeID = new DataColumn();
                BrandingThemeID.DataType = System.Type.GetType("System.String");
                BrandingThemeID.ColumnName = "BrandingThemeID";
                entries.Columns.Add(BrandingThemeID);

                DataColumn Contact = new DataColumn();
                Contact.DataType = System.Type.GetType("System.String");
                Contact.ColumnName = "Contact";
                entries.Columns.Add(Contact);

                DataColumn CreditNotes = new DataColumn();
                CreditNotes.DataType = System.Type.GetType("System.String");
                CreditNotes.ColumnName = "CreditNotes";
                entries.Columns.Add(CreditNotes);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                entries.Columns.Add(CurrencyCode);

                DataColumn CurrencyRate = new DataColumn();
                CurrencyRate.DataType = System.Type.GetType("System.Decimal");
                CurrencyRate.ColumnName = "CurrencyRate";
                entries.Columns.Add(CurrencyRate);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn DueDate = new DataColumn();
                DueDate.DataType = System.Type.GetType("System.DateTime");
                DueDate.ColumnName = "DueDate";
                entries.Columns.Add(DueDate);

                DataColumn ExpectedPaymentDate = new DataColumn();
                ExpectedPaymentDate.DataType = System.Type.GetType("System.DateTime");
                ExpectedPaymentDate.ColumnName = "ExpectedPaymentDate";
                entries.Columns.Add(ExpectedPaymentDate);

                DataColumn ExternalLinkProviderName = new DataColumn();
                ExternalLinkProviderName.DataType = System.Type.GetType("System.String");
                ExternalLinkProviderName.ColumnName = "ExternalLinkProviderName";
                entries.Columns.Add(ExternalLinkProviderName);

                DataColumn FullyPaidOnDate = new DataColumn();
                FullyPaidOnDate.DataType = System.Type.GetType("System.DateTime");
                FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
                entries.Columns.Add(FullyPaidOnDate);

                DataColumn HasAttachments = new DataColumn();
                HasAttachments.DataType = System.Type.GetType("System.String");
                HasAttachments.ColumnName = "HasAttachments";
                entries.Columns.Add(HasAttachments);

                DataColumn InvoiceID = new DataColumn();
                InvoiceID.DataType = System.Type.GetType("System.String");
                InvoiceID.ColumnName = "InvoiceID";
                entries.Columns.Add(InvoiceID);

                DataColumn InvoiceNumber = new DataColumn();
                InvoiceNumber.DataType = System.Type.GetType("System.String");
                InvoiceNumber.ColumnName = "InvoiceNumber";
                entries.Columns.Add(InvoiceNumber);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.String");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                entries.Columns.Add(LineAmountTypes);

                DataColumn LineItemsCount = new DataColumn();
                LineItemsCount.DataType = System.Type.GetType("System.String");
                LineItemsCount.ColumnName = "LineItemsCount";
                entries.Columns.Add(LineItemsCount);

                DataColumn AccountCode = new DataColumn();
                AccountCode.DataType = System.Type.GetType("System.String");
                AccountCode.ColumnName = "AccountCode";
                entries.Columns.Add(AccountCode);

                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn TrackingCategory1 = new DataColumn();
                TrackingCategory1.DataType = System.Type.GetType("System.String");
                TrackingCategory1.ColumnName = "TrackingCategory1";
                entries.Columns.Add(TrackingCategory1);

                DataColumn TrackingCategory1_Option = new DataColumn();
                TrackingCategory1_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory1_Option.ColumnName = "TrackingCategory1_Option";
                entries.Columns.Add(TrackingCategory1_Option);

                DataColumn TrackingCategory2 = new DataColumn();
                TrackingCategory2.DataType = System.Type.GetType("System.String");
                TrackingCategory2.ColumnName = "TrackingCategory2";
                entries.Columns.Add(TrackingCategory2);

                DataColumn TrackingCategory2_Option = new DataColumn();
                TrackingCategory2_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory2_Option.ColumnName = "TrackingCategory2_Option";
                entries.Columns.Add(TrackingCategory2_Option);

                DataColumn DiscountRate = new DataColumn();
                DiscountRate.DataType = System.Type.GetType("System.Decimal");
                DiscountRate.ColumnName = "DiscountRate";
                entries.Columns.Add(DiscountRate);


                DataColumn ItemCode = new DataColumn();
                ItemCode.DataType = System.Type.GetType("System.String");
                ItemCode.ColumnName = "ItemCode";
                entries.Columns.Add(ItemCode);

                DataColumn LineAmount = new DataColumn();
                LineAmount.DataType = System.Type.GetType("System.Decimal");
                LineAmount.ColumnName = "LineAmount";
                entries.Columns.Add(LineAmount);

                DataColumn Quantity = new DataColumn();
                Quantity.DataType = System.Type.GetType("System.Decimal");
                Quantity.ColumnName = "Quantity";
                entries.Columns.Add(Quantity);

                DataColumn TaxAmount = new DataColumn();
                TaxAmount.DataType = System.Type.GetType("System.Decimal");
                TaxAmount.ColumnName = "TaxAmount";
                entries.Columns.Add(TaxAmount);

                DataColumn TaxType = new DataColumn();
                TaxType.DataType = System.Type.GetType("System.String");
                TaxType.ColumnName = "TaxType";
                entries.Columns.Add(TaxType);

                DataColumn UnitAmount = new DataColumn();
                UnitAmount.DataType = System.Type.GetType("System.Decimal");
                UnitAmount.ColumnName = "UnitAmount";
                entries.Columns.Add(UnitAmount);

                DataColumn Payments = new DataColumn();
                Payments.DataType = System.Type.GetType("System.String");
                Payments.ColumnName = "Payments";
                entries.Columns.Add(Payments);

                DataColumn PlannedPaymentDate = new DataColumn();
                PlannedPaymentDate.DataType = System.Type.GetType("System.DateTime");
                PlannedPaymentDate.ColumnName = "PlannedPaymentDate";
                entries.Columns.Add(PlannedPaymentDate);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn SentToContact = new DataColumn();
                SentToContact.DataType = System.Type.GetType("System.String");
                SentToContact.ColumnName = "SentToContact";
                entries.Columns.Add(SentToContact);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn SubTotal = new DataColumn();
                SubTotal.DataType = System.Type.GetType("System.Decimal");
                SubTotal.ColumnName = "SubTotal";
                entries.Columns.Add(SubTotal);

                DataColumn Total = new DataColumn();
                Total.DataType = System.Type.GetType("System.Decimal");
                Total.ColumnName = "Total";
                entries.Columns.Add(Total);

                DataColumn TotalDiscount = new DataColumn();
                TotalDiscount.DataType = System.Type.GetType("System.Decimal");
                TotalDiscount.ColumnName = "TotalDiscount";
                entries.Columns.Add(TotalDiscount);

                DataColumn TotalTax = new DataColumn();
                TotalTax.DataType = System.Type.GetType("System.String");
                TotalTax.ColumnName = "TotalTax";
                entries.Columns.Add(TotalTax);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                entries.Columns.Add(Type);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.DateTime");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                entries.Columns.Add(UpdatedDateUTC);

                DataColumn Url = new DataColumn();
                Url.DataType = System.Type.GetType("System.String");
                Url.ColumnName = "Url";
                entries.Columns.Add(Url);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);


                allInvoices.Clear();
                // repository.Invoices.OrderBy
                // Invoice jTmp = repository.Invoices.ToList().First();
                // skip = Convert.ToInt32(jTmp.InvoiceNumber) - 1;
                skip = 200;
                List<Guid> guids;
                int x = 0;
                CultureInfo Au_date = new CultureInfo("en-AU");


                try
                {
                    int page = 1;
                    batchOfInvoices = api.Invoices
                                        .Where(string.Format("Date >= DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day))
                                        .And(string.Format("Date <= DateTime({0},{1},{2})", endDate.Year, endDate.Month, endDate.Day))
                                        .Page(page)
                                        .Find()
                                        .ToList();

                    _logger.Info(inUserName + " for Org " + OrgName +
                            "::Invoices::Debug:: ~" + batchOfInvoices.Count.ToString());
                    while (batchOfInvoices.Any())
                    {

                        System.Threading.Thread.Sleep(1000);
                        allInvoices.AddRange(batchOfInvoices);
                        page++;
                        batchOfInvoices.Clear();
                        batchOfInvoices = api.Invoices
                            .Where(string.Format("Date >= DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day))
                            .And(string.Format("Date <= DateTime({0},{1},{2})", endDate.Year, endDate.Month, endDate.Day))
                            .Page(page)
                            .Find()
                            .ToList();
                    }
                }
                catch (XeroApi.Exceptions.ApiResponseException ex)
                {

                    _logger.Error(inUserName + " ~Execution of XeroToCJ GetAllInvoicesWithPagination reading invoices:" + ex.Message, ex);
                }
                finally
                {
                    entries.Clear();
                    if (allInvoices.Any())
                    {

                        foreach (Invoice item in allInvoices)
                        {
                            foreach (LineItem line in item.Items)
                            {
                                IDs.Add(item.Id.ToString());
                                DataRow row = entries.NewRow();
                                row["AmountCredited"] = item.AmountCredited ?? 0;
                                row["AmountDue"] = item.AmountDue ?? 0;
                                row["AmountPaid"] = item.AmountPaid ?? 0;
                                row["BrandingThemeID"] = item.BrandingThemeId;
                                row["Contact"] = item.Contact.Id;
                                row["CreditNotes"] = item.CreditNotes.Count.ToString();
                                row["CurrencyCode"] = item.CurrencyCode;
                                row["CurrencyRate"] = item.CurrencyRate ?? 0;
                                //var y = line.Tracking;
                                row["Date"] = item.Date ?? DateTime.MinValue;//.Value.ToString("dd/MMM/yyyy");                        
                                row["DueDate"] = item.DueDate ?? DateTime.MinValue;//.Value.ToString("dd/MMM/yyyy");


                                row["ExpectedPaymentDate"] = item.ExpectedPaymentDate ?? DateTime.MinValue;
                                //row["ExternalLinkProviderName"] = item.ExternalLinkProviderName;
                                row["FullyPaidOnDate"] = item.FullyPaidOnDate ?? DateTime.MinValue;
                                row["HasAttachments"] = item.HasAttachments;
                                row["InvoiceID"] = item.Id;
                                row["InvoiceNumber"] = item.Number;
                                row["LineAmountTypes"] = item.LineAmountTypes;
                                row["LineItemsCount"] = item.Items.Count.ToString();
                                row["AccountCode"] = line.AccountCode;
                                row["Description"] = line.Description;


                                if (line.Tracking.Count == 2)
                                {
                                    row["TrackingCategory1"] = line.Tracking[0].Name;
                                    row["TrackingCategory1_Option"] = line.Tracking[0].Option;
                                    row["TrackingCategory2"] = line.Tracking[1].Name;
                                    row["TrackingCategory2_Option"] = line.Tracking[1].Option;
                                }
                                else if (line.Tracking.Count == 1)
                                {

                                    row["TrackingCategory1"] = line.Tracking[0].Name;
                                    row["TrackingCategory1_Option"] = line.Tracking[0].Option;
                                }
                                else
                                {
                                    row["TrackingCategory1"] = null;
                                    row["TrackingCategory1_Option"] = null;
                                    row["TrackingCategory2"] = null;
                                    row["TrackingCategory2_Option"] = null;
                                }



                                row["DiscountRate"] = line.DiscountRate ?? 0;
                                row["ItemCode"] = line.ItemCode;
                                row["LineAmount"] = line.LineAmount ?? 0;
                                row["Quantity"] = line.Quantity ?? 0;
                                row["TaxAmount"] = line.TaxAmount ?? 0;
                                row["TaxType"] = line.TaxType;
                                row["UnitAmount"] = line.UnitAmount ?? 0;
                                row["Payments"] = item.Prepayments.Count.ToString();
                                row["PlannedPaymentDate"] = item.PlannedPaymentDate ?? DateTime.MinValue;
                                row["Reference"] = item.Reference;
                                row["SentToContact"] = item.SentToContact;
                                row["Status"] = item.Status;
                                row["SubTotal"] = item.SubTotal ?? 0;
                                row["Total"] = item.Total ?? 0;
                                //row["TotalDiscount"] = item.TotalDiscount;
                                row["TotalTax"] = item.TotalTax ?? 0;
                                row["Type"] = item.Type;
                                row["UpdatedDateUTC"] = item.UpdatedDateUtc ?? DateTime.MinValue;//.Value.ToString("dd/MMM/yyyy");
                                row["Url"] = item.Url;
                                row["OrgShortCode"] = selectedOrgShortCode;
                                row["OrgName"] = selectedOrgName;


                                entries.Rows.Add(row);
                                x++;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("There are no invoices available!");
                        _logger.Info(inUserName + " for Org " + OrgName + "::Invoices:: ~There are no invoices available!");
                    }


                    if (CheckValidDatabase())
                    {
                        clear_table_section("Invoices", startDate, endDate, "Date");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                        {
                            BulkInsertInvoices(bulkCopy, startDate, endDate, entries, OrgName);
                        }
                    }

                    if (bool.Parse(CommonXero.GetIncludeAzure()))
                    {
                        if (CheckAzureValidDatabase())
                        {
                            azure_clear_table_section("Invoices", startDate, endDate, "Date");
                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                            {
                                BulkInsertInvoices(bulkCopy, startDate, endDate, entries, OrgName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.Fatal(inUserName + " ~Execution of XeroToCJ GetAllInvoicesWithPagination completed. Error message:" + ex.Message, ex);
            }

        }


        public void BulkInsertInvoices(SqlBulkCopy bulkCopy, DateTime startDate, DateTime endDate, DataTable entries, DataColumn OrgName)
        {
            bulkCopy.DestinationTableName =
                                "Invoices";

            // Number of records to be processed in one go
            //sbc.BatchSize = batchSize;
            bulkCopy.ColumnMappings.Add("AmountCredited", "AmountCredited");
            bulkCopy.ColumnMappings.Add("AmountDue", "AmountDue");
            bulkCopy.ColumnMappings.Add("AmountPaid", "AmountPaid");
            bulkCopy.ColumnMappings.Add("BrandingThemeID", "BrandingThemeID");
            bulkCopy.ColumnMappings.Add("Contact", "Contact");
            bulkCopy.ColumnMappings.Add("CreditNotes", "CreditNotes");
            bulkCopy.ColumnMappings.Add("CurrencyCode", "CurrencyCode");
            bulkCopy.ColumnMappings.Add("CurrencyRate", "CurrencyRate");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("DueDate", "DueDate");
            bulkCopy.ColumnMappings.Add("ExpectedPaymentDate", "ExpectedPaymentDate");
            bulkCopy.ColumnMappings.Add("ExternalLinkProviderName", "ExternalLinkProviderName");
            bulkCopy.ColumnMappings.Add("FullyPaidOnDate", "FullyPaidOnDate");
            bulkCopy.ColumnMappings.Add("HasAttachments", "HasAttachments");
            bulkCopy.ColumnMappings.Add("InvoiceID", "InvoiceID");
            bulkCopy.ColumnMappings.Add("InvoiceNumber", "InvoiceNumber");
            bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
            bulkCopy.ColumnMappings.Add("LineItemsCount", "LineItemsCount");
            bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
            bulkCopy.ColumnMappings.Add("Description", "Description");
            bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
            bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
            bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
            bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
            bulkCopy.ColumnMappings.Add("DiscountRate", "DiscountRate");
            bulkCopy.ColumnMappings.Add("ItemCode", "ItemCode");
            bulkCopy.ColumnMappings.Add("LineAmount", "LineAmount");
            bulkCopy.ColumnMappings.Add("Quantity", "Quantity");
            bulkCopy.ColumnMappings.Add("TaxAmount", "TaxAmount");
            bulkCopy.ColumnMappings.Add("TaxType", "TaxType");
            bulkCopy.ColumnMappings.Add("UnitAmount", "UnitAmount");
            bulkCopy.ColumnMappings.Add("Payments", "Payments");
            bulkCopy.ColumnMappings.Add("PlannedPaymentDate", "PlannedPaymentDate");
            bulkCopy.ColumnMappings.Add("Reference", "Reference");
            bulkCopy.ColumnMappings.Add("SentToContact", "SentToContact");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("SubTotal", "SubTotal");
            bulkCopy.ColumnMappings.Add("Total", "Total");
            bulkCopy.ColumnMappings.Add("TotalDiscount", "TotalDiscount");
            bulkCopy.ColumnMappings.Add("TotalTax", "TotalTax");
            bulkCopy.ColumnMappings.Add("Type", "Type");
            bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
            bulkCopy.ColumnMappings.Add("Url", "Url");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");


            _logger.Info(inUserName + " for Org " + OrgName + "::Invoices:: ~" + entries.Rows.Count.ToString() + " ivoices were retrieved from Xero");
            try
            {
                Console.WriteLine("Copying to the relational table...");
                _logger.Info(inUserName + " ~Copying to the relational table...");
                // Write from the source to the destination.

                bulkCopy.WriteToServer(entries);
                _logger.Info(inUserName + " ~Execution of GetAllInvoicesWithPagination relational table method completed.");
            }
            catch (Exception ex)
            {
                _logger.Fatal(inUserName + " ~Execution of XeroToCJ GetAllInvoicesWithPagination relational table completed. Error message:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
                Debug.WriteLine(ex.Message.ToString());
            }
        }


        //--------------------------------------------------------------------------------------

        private void clear_table_section(String teble_name, DateTime startDate, DateTime endDate, String DateColumn)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ clear_table_section method started.");


                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");
                SqlConnection MyConnection = null;
                SqlDataReader myReader = null;
                SqlConnection MyAzureConnection = null;
                endDate = endDate.AddDays(1).AddSeconds(-1);
                SqlDataReader myAzureReader = null;
                String sqlStr = @" Delete FROM  " + teble_name + " Where OrgShortCode='" + inOrgShortCode + "'" +
                                 " AND  [" + DateColumn + "] Between convert(DateTime,'" + startDate + "',103) " +
                                 " AND convert(DateTime,'" + endDate + "',103) ";
                try
                {
                    MyConnection = new SqlConnection(user_conn);
                    MyConnection.Open();

                    SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {

                    }
                    int i = 0;

                    if (bool.Parse(CommonXero.GetIncludeAzure()))
                    {
                        if (CheckAzureValidDatabase())
                        {
                            MyAzureConnection = new SqlConnection(azure_user_conn);
                            MyAzureConnection.Open();

                            SqlCommand myAzureCommand = new SqlCommand(sqlStr, MyAzureConnection);
                            myAzureReader = myAzureCommand.ExecuteReader();

                            while (myAzureReader.Read())
                            {

                            }
                        }
                    }
                    //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method completed.");
                }
                catch (SqlException ex)
                {
                    _logger.Fatal(inUserName + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                catch (Exception ex)
                {
                    _logger.Fatal(inUserName + " ~An error occurred during execution of clear_table_section method. Error details:" + ex.Message, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    if (MyConnection != null)
                    {
                        MyConnection.Close();
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                    }

                    if (MyAzureConnection != null)
                    {
                        MyAzureConnection.Close();
                    }
                    if (myAzureReader != null)
                    {
                        myAzureReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table_section completed.:" + ex.Message, ex);
            }
        }

        private void azure_clear_table_section(String teble_name, DateTime startDate, DateTime endDate, String DateColumn)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ azure_clear_table_section method started.");

                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");
                SqlConnection MyAzureConnection = null;
                endDate = endDate.AddDays(1).AddSeconds(-1);
                SqlDataReader myAzureReader = null;
                String sqlStr = @" Delete FROM  " + teble_name + " Where OrgShortCode='" + inOrgShortCode + "'" +
                                 " AND  [" + DateColumn + "] Between convert(DateTime,'" + startDate + "',103) " +
                                 " AND convert(DateTime,'" + endDate + "',103) ";
                try
                {
                    if (bool.Parse(CommonXero.GetIncludeAzure()))
                    {
                        if (CheckAzureValidDatabase())
                        {
                            MyAzureConnection = new SqlConnection(azure_user_conn);
                            MyAzureConnection.Open();

                            SqlCommand myAzureCommand = new SqlCommand(sqlStr, MyAzureConnection);
                            myAzureReader = myAzureCommand.ExecuteReader();

                            while (myAzureReader.Read())
                            {

                            }
                        }
                    }
                    //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method completed.");
                }
                catch (SqlException ex)
                {
                    _logger.Fatal(inUserName + " ~An SQL error occurred during execution of azure_clear_table_section method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                catch (Exception ex)
                {
                    _logger.Fatal(inUserName + " ~An error occurred during execution of azure_clear_table_section method. Error details:" + ex.Message, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    if (MyAzureConnection != null)
                    {
                        MyAzureConnection.Close();
                    }
                    if (myAzureReader != null)
                    {
                        myAzureReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table_section completed.:" + ex.Message, ex);
            }
        }



        private void azure_clear_table_modified_after(String teble_name, DateTime startDate, String DateColumn)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ azure_clear_table_modified_after method started.");

                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");

                SqlConnection MyAzureConnection = null;
                SqlDataReader myAzureReader = null;
                String sqlStr = @" Delete FROM  " + teble_name + " Where OrgShortCode='" + inOrgShortCode + "'" +
                                 " AND  [" + DateColumn + "] >= convert(DateTime,'" + startDate + "',103) ";

                try
                {
                    if (bool.Parse(CommonXero.GetIncludeAzure()))
                    {
                        if (CheckAzureValidDatabase())
                        {
                            MyAzureConnection = new SqlConnection(azure_user_conn);
                            MyAzureConnection.Open();

                            SqlCommand myAzureCommand = new SqlCommand(sqlStr, MyAzureConnection);
                            myAzureReader = myAzureCommand.ExecuteReader();

                            while (myAzureReader.Read())
                            {

                            }
                        }
                    }


                    //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method completed.");
                }
                catch (SqlException ex)
                {
                    _logger.Error(inUserName + " ~An SQL error occurred during execution of azure_clear_table_modified_after method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                    Debug.WriteLine(ex.Message.ToString());

                }
                catch (Exception ex)
                {
                    _logger.Error(inUserName + " ~Execution of XeroToCJ azure_clear_table_modified_after method. Error details:" + ex.Message, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    if (MyAzureConnection != null)
                    {
                        MyAzureConnection.Close();
                    }
                    if (myAzureReader != null)
                    {
                        myAzureReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table_modified_after:" + ex.Message, ex);
            }
        }



        private void clear_table_modified_after(String teble_name, DateTime startDate, String DateColumn)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ clear_table_modified_after method started.");

                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");
                SqlConnection MyConnection = null;
                SqlDataReader myReader = null;
                String sqlStr = @" Delete FROM  " + teble_name + " Where OrgShortCode='" + inOrgShortCode + "'" +
                                 " AND  [" + DateColumn + "] >= convert(DateTime,'" + startDate + "',103) ";

                try
                {
                    MyConnection = new SqlConnection(user_conn);
                    MyConnection.Open();

                    SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {

                    }
                    int i = 0;

                    //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method completed.");
                }
                catch (SqlException ex)
                {
                    _logger.Error(inUserName + " ~An SQL error occurred during execution of clear_table_modified_after method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                    Debug.WriteLine(ex.Message.ToString());

                }
                catch (Exception ex)
                {
                    _logger.Error(inUserName + " ~Execution of XeroToCJ clear_table_modified_after method. Error details:" + ex.Message, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    if (MyConnection != null)
                    {
                        MyConnection.Close();
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table_modified_after:" + ex.Message, ex);
            }
        }



        //--------------------------------------------------------------------------------------
        // This function starts relevant jedox etl jobs for the current users
        // type:
        // Inputs: username , organisation name
        // Outputs: 
        // Operation: Triggers Jedox Jobs
        //--------------------------------------------------------------------------------------
        //private void runJedoxETL_v5(string ETLMode)
        //{
        //    try
        //    {
        //        _logger.Info((inUserName ?? "") + " ~Execution of runJedoxETL method started. Job Mode:" + ETLMode);

        //        string username = inUserName;
        //        string password = inPassword;

        //        JedoxETL.ServerStatus x = new JedoxETL.ServerStatus();
        //        JedoxETL.ETLServer s = new JedoxETL.ETLServer();

        //        JedoxETL.Variable username_variable = new JedoxETL.Variable();
        //        username_variable.name = "username";
        //        username_variable.value = username;

        //        JedoxETL.Variable password_variable = new JedoxETL.Variable();
        //        password_variable.name = "password";
        //        password_variable.value = password;

        //        JedoxETL.Variable database_variable = new JedoxETL.Variable();
        //        database_variable.name = "database";
        //        database_variable.value = Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", "");

        //        JedoxETL.Variable OrgName = new JedoxETL.Variable();
        //        OrgName.name = "OrgName";
        //        OrgName.value = inOrgName.ToString();

        //        JedoxETL.Variable Jedox_DB = new JedoxETL.Variable();
        //        Jedox_DB.name = "Jedox_DB";
        //        Jedox_DB.value = Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", "");

        //        JedoxETL.Variable UpdateFlag = new JedoxETL.Variable();
        //        UpdateFlag.name = "UpdateFlag";
        //        UpdateFlag.value = "update";


        //        JedoxETL.Variable CompanyName = new JedoxETL.Variable();
        //        CompanyName.name = "CompanyName";
        //        CompanyName.value = Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", "");

        //        JedoxETL.Variable UserGroup = new JedoxETL.Variable();
        //        UserGroup.name = "UserGroup";
        //        UserGroup.value = Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", "") + "_group";


        //        if (ETLMode == "Create")
        //        {
        //            var status = s.execute("ClearJellySystemOps.jobs.default", new JedoxETL.Variable[] { username_variable, password_variable, Jedox_DB });

        //            while (s.getExecutionStatus(status.id.Value, true, true, true).status == "Running")
        //            {
        //                System.Threading.Thread.Sleep(1000);
        //            }

        //            database_variable.value = GetUserDBName(inUserName);
        //            var status2 = s.execute("Xero.jobs.default", new JedoxETL.Variable[] { database_variable, OrgName, Jedox_DB, UpdateFlag });


        //            while (s.getExecutionStatus(status2.id.Value, true, true, true).status == "Running")
        //            {
        //                System.Threading.Thread.Sleep(1000);
        //            }

        //            var status3 = s.execute("FolderGeneration.jobs.default", new JedoxETL.Variable[] { CompanyName, UserGroup });


        //            while (s.getExecutionStatus(status3.id.Value, true, true, true).status == "Running")
        //            {
        //                System.Threading.Thread.Sleep(1000);
        //            }

        //            var status4 = s.execute("ReportGeneration.jobs.default", new JedoxETL.Variable[] { CompanyName, UserGroup });


        //            while (s.getExecutionStatus(status4.id.Value, true, true, true).status == "Running")
        //            {
        //                System.Threading.Thread.Sleep(1000);
        //            }
        //        }
        //        else
        //        {
        //            database_variable.value = GetUserDBName(inUserName);
        //            var status2 = s.execute("Xero_Updates.jobs.default", new JedoxETL.Variable[] { database_variable, OrgName, Jedox_DB, UpdateFlag });


        //            while (s.getExecutionStatus(status2.id.Value, true, true, true).status == "Running")
        //            {
        //                System.Threading.Thread.Sleep(1000);
        //            }
        //        }
        //        _logger.Info((inUserName ?? "").ToString() + " ~Execution of createJedoxModel method completed.");
        //    }
        //    catch (Exception ex)
        //    {

        //        _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ runJedoxETL completed.:" + ex.Message, ex);
        //    }
        //}

        private void runJedoxETL(string ETLMode)
        {

            try
            {
                _logger.Info((inUserName ?? "") + " ~Execution of runJedoxETL method started. Job Mode:" + ETLMode);

                string username = inUserName;
                string password = inPassword;


                if (ETLMode == "Create")
                {
                    //--------------------------------------------------------------------------------//
                    // Running SysOps ETL Job. This job should be moved to where xero org addition operation happens
                    //--------------------------------------------------------------------------------//
                    Dictionary<String, String> varsSysOps = new Dictionary<string, string>();
                    varsSysOps.Add("username", inUserName);
                    varsSysOps.Add("Jedox_DB", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", ""));
                    varsSysOps.Add("password", inPassword);
                    String SysOpsLocator = "ClearJellySystemOps.jobs.default";

                    JedoxEtl jdxSysOps = new JedoxEtl();
                    String ID_SysOps = jdxSysOps.jedoxExecuteJob(varsSysOps, SysOpsLocator);
                    String statusSysOps = jdxSysOps.jedoxExecuteStatus(ID_SysOps);

                    while (statusSysOps == "Running")
                    {
                        System.Threading.Thread.Sleep(1000);
                        statusSysOps = jdxSysOps.jedoxExecuteStatus(ID_SysOps);
                    }
                    //--------------------------------------------------------------------------------//
                    // Running the Jedox ETL default job: xero.jobs.default
                    // This job creates the Jedox cubes and dimensions
                    // It overwrites the current cubes and dimenision in the relevant Jedox database
                    //--------------------------------------------------------------------------------//
                    Dictionary<String, String> varsCreate = new Dictionary<string, string>();
                    varsCreate.Add("database", GetUserDBName(inUserName));
                    varsCreate.Add("OrgName", inOrgName.ToString());
                    varsCreate.Add("Jedox_DB", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", ""));
                    varsCreate.Add("UpdateFlag", "update");

                    String CreateLocator = "Xero.jobs.default";

                    JedoxEtl jdxCreate = new JedoxEtl();
                    String ID_Create = jdxCreate.jedoxExecuteJob(varsCreate, CreateLocator);
                    String statusCreate = jdxCreate.jedoxExecuteStatus(ID_Create);


                    while (statusCreate == "Running")
                    {
                        System.Threading.Thread.Sleep(1000);
                        statusCreate = jdxCreate.jedoxExecuteStatus(ID_Create);
                    }


                    //--------------------------------------------------------------------------------//
                    // Runs the ETL job which create a new folder for the user in Jedox's Folder Manager
                    // and sets the rights so the user can only see his/her own folder
                    //--------------------------------------------------------------------------------//				
                    Dictionary<String, String> varsFolder = new Dictionary<string, string>();
                    varsFolder.Add("CompanyName", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", ""));
                    varsFolder.Add("UserGroup", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", "") + "_group");

                    String FolderLocator = "FolderGeneration.jobs.default";

                    JedoxEtl jdxFolder = new JedoxEtl();
                    String ID_Folder = jdxFolder.jedoxExecuteJob(varsFolder, FolderLocator);
                    String statusFolder = jdxFolder.jedoxExecuteStatus(ID_Folder);

                    while (statusFolder == "Running")
                    {
                        System.Threading.Thread.Sleep(1000);
                        statusFolder = jdxFolder.jedoxExecuteStatus(ID_Folder);
                    }


                    //--------------------------------------------------------------------------------//
                    // Runs the ETL job which create a new report folder for the user in Jedox's 
                    // Report Manager and sets the rights so the user can only see his/her own folder
                    //--------------------------------------------------------------------------------//
                    Dictionary<String, String> varsReport = new Dictionary<string, string>();
                    varsReport.Add("CompanyName", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", ""));
                    varsReport.Add("UserGroup", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", "") + "_group");

                    String ReportLocator = "ReportGeneration.jobs.default";

                    JedoxEtl jdxReport = new JedoxEtl();
                    String ID_Report = jdxReport.jedoxExecuteJob(varsReport, ReportLocator);
                    String statusReport = jdxReport.jedoxExecuteStatus(ID_Report);

                    while (statusReport == "Running")
                    {
                        System.Threading.Thread.Sleep(1000);
                        statusReport = jdxReport.jedoxExecuteStatus(ID_Report);
                    }

                }
                else
                {
                    //--------------------------------------------------------------------------------//
                    // Running the Jedox ETL default Update job: Xero_Updates.jobs.default
                    // This job creates the Jedox cubes and dimensions
                    // It overwrites the current cubes and dimenision in the relevant Jedox database
                    //--------------------------------------------------------------------------------//
                    Dictionary<String, String> varsUpdate = new Dictionary<string, string>();
                    varsUpdate.Add("database", GetUserDBName(inUserName));
                    varsUpdate.Add("OrgName", inOrgName.ToString());
                    varsUpdate.Add("Jedox_DB", Regex.Replace(inOrgName.ToString(), @"[^0-9a-zA-Z]+", ""));
                    varsUpdate.Add("UpdateFlag", "update");

                    String UpdateLocator = "Xero_Updates.jobs.default";

                    JedoxEtl jdxUpdate = new JedoxEtl();
                    String ID_Update = jdxUpdate.jedoxExecuteJob(varsUpdate, UpdateLocator);
                    String statusUpdate = jdxUpdate.jedoxExecuteStatus(ID_Update);


                    while (statusUpdate == "Running")
                    {
                        System.Threading.Thread.Sleep(1000);
                        statusUpdate = jdxUpdate.jedoxExecuteStatus(ID_Update);
                    }
                }
                _logger.Info((inUserName ?? "").ToString() + " ~Execution of createJedoxModel method completed.");
            }
            catch (Exception ex)
            {

                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ runJedoxETL completed.:" + ex.Message, ex);
            }
        }



        //--------------------------------------------------------------------------------------
        // Retrieves the name of the sql server database associated with current user
        //--------------------------------------------------------------------------------------
        protected String GetUserDBName(String inUserName)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetUserDBName method started.");
                string databaseName = CommonXero.GetDataBaseName();
                SqlConnection MyConnection = null;
                SqlDataReader myReader = null;
                String tmpConn = system_conn + ";database=" + databaseName + ";";
                String UserName = inUserName;
                String email = "";
                String sqlStr = @" SELECT B.Name " +
                                "	  FROM [" + databaseName + "].[dbo].[User] A " +
                                "	  Inner JOIN [" + databaseName + "].[dbo].[Company] B " +
                                "	  ON" +
                                "	  A.CompanyId=B.CompanyId " +
                                "	  where email= '" + UserName + "'";
                try
                {
                    MyConnection = new SqlConnection(tmpConn);
                    MyConnection.Open();

                    SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        email = myReader.GetString(0);
                    }
                }
                catch (SqlException ex)
                {
                    _logger.Error(inUserName + " ~Execution of XeroToCJ GetUserDBName SqlException Error details:" + ex.Message, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                catch (Exception ex)
                {
                    _logger.Error(inUserName + " Execution of XeroToCJ GetUserDBName :" + ex.Message, ex);
                    Debug.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    if (MyConnection != null)
                    {
                        MyConnection.Close();
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                    }
                }
                _logger.Info(inUserName + " ~Execution of GetUserDBName method completed.");
                return (email);
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ GetUserDBName completed:" + ex.Message, ex);
                throw ex;
            }
        }
        //--------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------
        // This function starts relevant jedox etl jobs for the current users
        // type: SQL operation
        // Inputs: table name 
        // Outputs: 
        // Operation: Clears a table
        //--------------------------------------------------------------------------------------
        private void clear_table(String table_name)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ clear_table method started.");

                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");
                SqlConnection MyConnection = null;
                SqlDataReader myReader = null;
                String sqlStr = @" Delete FROM  " + table_name + " Where OrgShortCode='" + inOrgShortCode + "'";
                try
                {
                    MyConnection = new SqlConnection(user_conn);
                    MyConnection.Open();

                    SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {

                    }

                    //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method completed.");
                }
                catch (SqlException ex)
                {
                    //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table method. Error details:" + ex.Message);            
                    Debug.WriteLine(ex.Message.ToString());
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table SqlException  completed:" + ex.Message, ex);

                }
                catch (Exception ex)
                {
                    //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An error occurred during execution of clear_table method. Error details:" + ex.Message);
                    Debug.WriteLine(ex.Message.ToString());
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table completed:" + ex.Message, ex);
                }
                finally
                {
                    if (MyConnection != null)
                    {
                        MyConnection.Close();
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ clear_table completed:" + ex.Message, ex);
            }
        }

        private void azure_clear_table(String table_name)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ azure_clear_table method started.");

                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");

                SqlConnection MyAzureConnection = null;
                SqlDataReader myAzureReader = null;
                String sqlStr = @" Delete FROM  " + table_name + " Where OrgShortCode='" + inOrgShortCode + "'";
                try
                {

                    MyAzureConnection = new SqlConnection(azure_user_conn);
                    MyAzureConnection.Open();

                    SqlCommand myAzureCommand = new SqlCommand(sqlStr, MyAzureConnection);
                    myAzureReader = myAzureCommand.ExecuteReader();

                    while (myAzureReader.Read())
                    {

                    }

                    //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method completed.");
                }
                catch (SqlException ex)
                {
                    //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table method. Error details:" + ex.Message);            
                    Debug.WriteLine(ex.Message.ToString());
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ azure_clear_table SqlException  completed:" + ex.Message, ex);

                }
                catch (Exception ex)
                {
                    //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An error occurred during execution of clear_table method. Error details:" + ex.Message);
                    Debug.WriteLine(ex.Message.ToString());
                    _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ azure_clear_table completed:" + ex.Message, ex);
                }
                finally
                {

                    if (MyAzureConnection != null)
                    {
                        MyAzureConnection.Close();
                    }
                    if (myAzureReader != null)
                    {
                        myAzureReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ azure_clear_table completed:" + ex.Message, ex);
            }
        }
        //--------------------------------------------------------------------------------------


        //--------------------------------------------------------------------------------------
        // This function starts relevant jedox etl jobs for the current users
        // type: SQL operation
        // Inputs: table name and date 
        // Outputs: 
        // Operation: Clears a table's section based on the date paramter 
        //            Deletes all the rows with date column >= input date  
        //--------------------------------------------------------------------------------------

        protected void getTrackingCategories(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ getTrackingCategories method started.");

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                // Get the tracking categories in this org
                List<TrackingCategory> trackingCategories = api.TrackingCategories.Find().ToList();

                DataTable entries = new DataTable("TrackingCategories");

                DataColumn trackingCategoryID = new DataColumn();
                trackingCategoryID.DataType = System.Type.GetType("System.String");
                trackingCategoryID.ColumnName = "trackingCategoryID";
                entries.Columns.Add(trackingCategoryID);

                DataColumn Category = new DataColumn();
                Category.DataType = System.Type.GetType("System.String");
                Category.ColumnName = "Category";
                entries.Columns.Add(Category);

                DataColumn Option = new DataColumn();
                Option.DataType = System.Type.GetType("System.String");
                Option.ColumnName = "Option";
                entries.Columns.Add(Option);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                entries.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                entries.Columns.Add(OrgName);

                DataColumn trackingOptionID = new DataColumn();
                trackingOptionID.DataType = System.Type.GetType("System.String");
                trackingOptionID.ColumnName = "TrackingOptionID";
                entries.Columns.Add(trackingOptionID);

                foreach (var trackingCategory in trackingCategories)
                {

                    Console.WriteLine(string.Format("Tracking Category: {0}", trackingCategory.Name));

                    foreach (var trackingOption in trackingCategory.Options)
                    {
                        DataRow row = entries.NewRow();
                        row["trackingCategoryID"] = trackingCategory.Id;
                        row["Category"] = trackingCategory.Name;
                        row["Option"] = trackingOption.Name;
                        row["OrgShortCode"] = selectedOrgShortCode;
                        row["OrgName"] = selectedOrgName;
                        row["TrackingOptionID"] = trackingOption.Id;

                        entries.Rows.Add(row);
                    }
                }


                // insert into sql table

                if (CheckValidDatabase())
                {
                    clear_table("TrackingCategories");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertTrackingCategories(bulkCopy, entries);
                    }
                }
                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table("TrackingCategories");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertTrackingCategories(bulkCopy, entries);
                        }
                    }
                }
                Console.WriteLine("Tracking categories have been imported!");
            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException getTrackingCategories completed. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("getTrackingCategories");
                }
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ getTrackingCategories completed:" + ex.Message, ex);
            }
        }

        public void BulkInsertTrackingCategories(SqlBulkCopy bulkCopy, DataTable entries)
        {
            bulkCopy.DestinationTableName =
                           "TrackingCategories";
            try
            {
                _logger.Info(inUserName + " ~Copying to the relational table...");
                bulkCopy.WriteToServer(entries);
                _logger.Info(inUserName + " ~Execution of XeroToCJ getTrackingCategories method completed.");
            }
            catch (SqlException ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ getTrackingCategories copying to the relational table SqlException" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~Execution of XeroToCJ getTrackingCategories copying to the relational table" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }


        //--------------------------------------------------------------------------------------
        protected void GetAllContacts(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetAllContacts method started.");

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                //------------------------------------------------------------//
                //    Addresses: creating the relevant data table and columns
                //------------------------------------------------------------//            

                DataTable Contact_Address = new DataTable("Contact_Address");

                DataColumn AddressID = new DataColumn();
                AddressID.DataType = System.Type.GetType("System.String");
                AddressID.ColumnName = "AddressID";
                Contact_Address.Columns.Add(AddressID);

                DataColumn adressContactID = new DataColumn();
                adressContactID.DataType = System.Type.GetType("System.String");
                adressContactID.ColumnName = "adressContactID";
                Contact_Address.Columns.Add(adressContactID);

                DataColumn AddressLine1 = new DataColumn();
                AddressLine1.DataType = System.Type.GetType("System.String");
                AddressLine1.ColumnName = "AddressLine1";
                Contact_Address.Columns.Add(AddressLine1);

                DataColumn AddressLine2 = new DataColumn();
                AddressLine2.DataType = System.Type.GetType("System.String");
                AddressLine2.ColumnName = "AddressLine2";
                Contact_Address.Columns.Add(AddressLine2);

                DataColumn AddressLine3 = new DataColumn();
                AddressLine3.DataType = System.Type.GetType("System.String");
                AddressLine3.ColumnName = "AddressLine3";
                Contact_Address.Columns.Add(AddressLine3);


                DataColumn AddressLine4 = new DataColumn();
                AddressLine4.DataType = System.Type.GetType("System.String");
                AddressLine4.ColumnName = "AddressLine4";
                Contact_Address.Columns.Add(AddressLine4);

                DataColumn AttentionTo = new DataColumn();
                AttentionTo.DataType = System.Type.GetType("System.String");
                AttentionTo.ColumnName = "AttentionTo";
                Contact_Address.Columns.Add(AttentionTo);

                DataColumn City = new DataColumn();
                City.DataType = System.Type.GetType("System.String");
                City.ColumnName = "City";
                Contact_Address.Columns.Add(City);

                DataColumn Country = new DataColumn();
                Country.DataType = System.Type.GetType("System.String");
                Country.ColumnName = "Country";
                Contact_Address.Columns.Add(Country);

                DataColumn PostalCode = new DataColumn();
                PostalCode.DataType = System.Type.GetType("System.String");
                PostalCode.ColumnName = "PostalCode";
                Contact_Address.Columns.Add(PostalCode);

                DataColumn Region = new DataColumn();
                Region.DataType = System.Type.GetType("System.String");
                Region.ColumnName = "Region";
                Contact_Address.Columns.Add(Region);

                DataColumn Contact_Address_OrgShortCode = new DataColumn();
                Contact_Address_OrgShortCode.DataType = System.Type.GetType("System.String");
                Contact_Address_OrgShortCode.ColumnName = "OrgShortCode";
                Contact_Address.Columns.Add(Contact_Address_OrgShortCode);

                DataColumn Contact_Address_OrgShortCode_OrgName = new DataColumn();
                Contact_Address_OrgShortCode_OrgName.DataType = System.Type.GetType("System.String");
                Contact_Address_OrgShortCode_OrgName.ColumnName = "OrgName";
                Contact_Address.Columns.Add(Contact_Address_OrgShortCode_OrgName);


                //------------------------------------------------------------//
                //    Contacts: creating the relevant data table and columns
                //------------------------------------------------------------//
                DataTable Contacts_Table = new DataTable("Contacts_Table");

                DataColumn ContactID = new DataColumn();
                ContactID.DataType = System.Type.GetType("System.String");
                ContactID.ColumnName = "ContactID";
                Contacts_Table.Columns.Add(ContactID);

                DataColumn GroupCount = new DataColumn();
                GroupCount.DataType = System.Type.GetType("System.String");
                GroupCount.ColumnName = "GroupCount";
                Contacts_Table.Columns.Add(GroupCount);

                DataColumn ContactName = new DataColumn();
                ContactName.DataType = System.Type.GetType("System.String");
                ContactName.ColumnName = "ContactName";
                Contacts_Table.Columns.Add(ContactName);

                DataColumn AccountsPayable_Outstanding = new DataColumn();
                AccountsPayable_Outstanding.DataType = System.Type.GetType("System.String");
                AccountsPayable_Outstanding.ColumnName = "AccountsPayable_Outstanding";
                Contacts_Table.Columns.Add(AccountsPayable_Outstanding);

                DataColumn AccountsPayable_Overdue = new DataColumn();
                AccountsPayable_Overdue.DataType = System.Type.GetType("System.String");
                AccountsPayable_Overdue.ColumnName = "AccountsPayable_Overdue";
                Contacts_Table.Columns.Add(AccountsPayable_Overdue);

                DataColumn AccountsReceivable_Outstanding = new DataColumn();
                AccountsReceivable_Outstanding.DataType = System.Type.GetType("System.String");
                AccountsReceivable_Outstanding.ColumnName = "AccountsReceivable_Outstanding";
                Contacts_Table.Columns.Add(AccountsReceivable_Outstanding);

                DataColumn AccountsReceivable_Overdue = new DataColumn();
                AccountsReceivable_Overdue.DataType = System.Type.GetType("System.String");
                AccountsReceivable_Overdue.ColumnName = "AccountsReceivable_Overdue";
                Contacts_Table.Columns.Add(AccountsReceivable_Overdue);

                DataColumn ContactStatus = new DataColumn();
                ContactStatus.DataType = System.Type.GetType("System.String");
                ContactStatus.ColumnName = "ContactStatus";
                Contacts_Table.Columns.Add(ContactStatus);

                DataColumn FirstName = new DataColumn();
                FirstName.DataType = System.Type.GetType("System.String");
                FirstName.ColumnName = "FirstName";
                Contacts_Table.Columns.Add(FirstName);

                DataColumn LastName = new DataColumn();
                LastName.DataType = System.Type.GetType("System.String");
                LastName.ColumnName = "LastName";
                Contacts_Table.Columns.Add(LastName);

                DataColumn IsCustomer = new DataColumn();
                IsCustomer.DataType = System.Type.GetType("System.String");
                IsCustomer.ColumnName = "IsCustomer";
                Contacts_Table.Columns.Add(IsCustomer);

                DataColumn IsSupplier = new DataColumn();
                IsSupplier.DataType = System.Type.GetType("System.String");
                IsSupplier.ColumnName = "IsSupplier";
                Contacts_Table.Columns.Add(IsSupplier);

                DataColumn PaymentTerms_Bills_Day = new DataColumn();
                PaymentTerms_Bills_Day.DataType = System.Type.GetType("System.String");
                PaymentTerms_Bills_Day.ColumnName = "PaymentTerms_Bills_Day";
                Contacts_Table.Columns.Add(PaymentTerms_Bills_Day);

                DataColumn PaymentTerms_Bills_Type = new DataColumn();
                PaymentTerms_Bills_Type.DataType = System.Type.GetType("System.String");
                PaymentTerms_Bills_Type.ColumnName = "PaymentTerms_Bills_Type";
                Contacts_Table.Columns.Add(PaymentTerms_Bills_Type);

                DataColumn PaymentTerms_Sales_Day = new DataColumn();
                PaymentTerms_Sales_Day.DataType = System.Type.GetType("System.String");
                PaymentTerms_Sales_Day.ColumnName = "PaymentTerms_Sales_Day";
                Contacts_Table.Columns.Add(PaymentTerms_Sales_Day);

                DataColumn PaymentTerms_Sales_Type = new DataColumn();
                PaymentTerms_Sales_Type.DataType = System.Type.GetType("System.String");
                PaymentTerms_Sales_Type.ColumnName = "PaymentTerms_Sales_Type";
                Contacts_Table.Columns.Add(PaymentTerms_Sales_Type);

                DataColumn Contacts_Table_OrgShortCode = new DataColumn();
                Contacts_Table_OrgShortCode.DataType = System.Type.GetType("System.String");
                Contacts_Table_OrgShortCode.ColumnName = "OrgShortCode";
                Contacts_Table.Columns.Add(Contacts_Table_OrgShortCode);

                DataColumn Contacts_Table_OrgName = new DataColumn();
                Contacts_Table_OrgName.DataType = System.Type.GetType("System.String");
                Contacts_Table_OrgName.ColumnName = "OrgName";
                Contacts_Table.Columns.Add(Contacts_Table_OrgName);
                int page = 1;

                List<Contact> batchOfContacts = new List<Contact>();// api.Contacts.Page(page).OrderBy("JournalNumber").Find().ToList();
                List<Contact> contacts = new List<Contact>();// api.Contacts.Find().ToList();

                while ((batchOfContacts = api.Contacts.Page(page).OrderBy("ContactID").Find().ToList()).Count() > 0)
                {
                    contacts.AddRange(batchOfContacts);
                    page++; ;

                    batchOfContacts.Clear();
                    System.Threading.Thread.Sleep(1000);
                }
                foreach (Contact contact in contacts)
                {

                    DataRow row1 = Contacts_Table.NewRow();
                    row1["ContactID"] = contact.Id;
                    row1["GroupCount"] = contact.ContactGroups.Count.ToString();
                    row1["ContactName"] = contact.Name;
                    if (contact.Balances != null)
                    {
                        if (contact.Balances.AccountsPayable != null)
                        {
                            if (contact.Balances.AccountsPayable.Outstanding != null)
                            {
                                row1["AccountsPayable_Outstanding"] = contact.Balances.AccountsPayable.Outstanding.ToString();
                            }
                        }
                    }
                    if (contact.Balances != null)
                    {
                        if (contact.Balances.AccountsPayable != null)
                        {
                            if (contact.Balances.AccountsPayable.Overdue != null)
                            {
                                row1["AccountsPayable_Overdue"] = contact.Balances.AccountsPayable.Overdue.ToString();
                            }
                        }
                    }
                    if (contact.Balances != null)
                    {
                        if (contact.Balances.AccountsReceivable != null)
                        {
                            if (contact.Balances.AccountsReceivable.Outstanding != null)
                            {
                                row1["AccountsReceivable_Outstanding"] = contact.Balances.AccountsReceivable.Outstanding.ToString();
                            }
                        }
                    }
                    if (contact.Balances != null)
                    {
                        if (contact.Balances.AccountsReceivable != null)
                        {
                            if (contact.Balances.AccountsReceivable.Overdue != null)
                            {
                                row1["AccountsReceivable_Overdue"] = contact.Balances.AccountsReceivable.Overdue.ToString();
                            }
                        }
                    }
                    row1["ContactStatus"] = contact.ContactStatus;
                    row1["FirstName"] = contact.FirstName;
                    row1["LastName"] = contact.LastName;
                    row1["IsCustomer"] = contact.IsCustomer.ToString();
                    row1["IsSupplier"] = contact.IsSupplier.ToString();
                    if (contact.PaymentTerms != null)
                    {

                        if (contact.PaymentTerms.Bills != null)
                        {
                            row1["PaymentTerms_Bills_Day"] = contact.PaymentTerms.Bills.Day.ToString();
                            row1["PaymentTerms_Bills_Type"] = contact.PaymentTerms.Bills.TermType.ToString();
                        }
                        if (contact.PaymentTerms.Sales != null)
                        {
                            row1["PaymentTerms_Sales_Day"] = contact.PaymentTerms.Sales.Day.ToString();
                            row1["PaymentTerms_Sales_Type"] = contact.PaymentTerms.Sales.TermType.ToString();
                        }
                    }

                    row1["OrgShortCode"] = selectedOrgShortCode;
                    row1["OrgName"] = selectedOrgName;

                    Contacts_Table.Rows.Add(row1);
                    int address_id = 0;

                    foreach (Address address in contact.Addresses)
                    {
                        DataRow row = Contact_Address.NewRow();
                        row["AddressID"] = address_id.ToString();
                        row["adressContactID"] = contact.Id.ToString();
                        row["AddressLine1"] = address.AddressLine1;
                        row["AddressLine2"] = address.AddressLine2;
                        row["AddressLine3"] = address.AddressLine3;
                        row["AddressLine4"] = address.AddressLine4;
                        row["AttentionTo"] = address.AttentionTo;
                        row["Country"] = address.Country;
                        row["PostalCode"] = address.PostalCode;
                        row["City"] = address.City;
                        row["Region"] = address.Region;
                        row["OrgShortCode"] = selectedOrgShortCode;
                        row["OrgName"] = selectedOrgName;
                        Contact_Address.Rows.Add(row);
                        address_id++;
                    }
                }


                if (CheckValidDatabase())
                {
                    clear_table("Contact_Address");
                    clear_table("Contacts");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertContact_Address(bulkCopy, Contact_Address);
                    }

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {
                        BulkInsertContact(bulkCopy, Contacts_Table);
                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table("Contact_Address");
                        azure_clear_table("Contacts");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertContact_Address(bulkCopy, Contact_Address);
                        }

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {
                            BulkInsertContact(bulkCopy, Contacts_Table);
                        }
                    }
                }


            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException GetAllContacts completed. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetAllContacts");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~An Execution of XeroToCJ GetAllContacts completed. Error details:" + ex.Message, ex);
            }
        }

        public void BulkInsertContact_Address(SqlBulkCopy bulkCopy, DataTable Contact_Address)
        {
            bulkCopy.DestinationTableName =
                            "Contact_Address";
            bulkCopy.ColumnMappings.Add("AddressID", "AddressID");
            bulkCopy.ColumnMappings.Add("adressContactID", "ContactID");
            bulkCopy.ColumnMappings.Add("AddressLine1", "AddressLine1");
            bulkCopy.ColumnMappings.Add("AddressLine2", "AddressLine2");
            bulkCopy.ColumnMappings.Add("AddressLine3", "AddressLine3");
            bulkCopy.ColumnMappings.Add("AddressLine4", "AddressLine4");
            bulkCopy.ColumnMappings.Add("AttentionTo", "AttentionTo");
            bulkCopy.ColumnMappings.Add("Country", "Country");
            bulkCopy.ColumnMappings.Add("PostalCode", "PostalCode");
            bulkCopy.ColumnMappings.Add("City", "City");
            bulkCopy.ColumnMappings.Add("Region", "Region");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
            try
            {
                _logger.Info(inUserName + " ~Copying to the relational table...");

                // Write from the source to the destination.
                bulkCopy.WriteToServer(Contact_Address);
            }
            catch (Exception ex)
            {
                //
                _logger.Error(inUserName + " ~AnExecution of XeroToCJ copying to the relational table. Error details:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }

        public void BulkInsertContact(SqlBulkCopy bulkCopy, DataTable Contacts_Table)
        {
            bulkCopy.DestinationTableName =
                          "Contacts";
            bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
            bulkCopy.ColumnMappings.Add("GroupCount", "GroupCount");
            bulkCopy.ColumnMappings.Add("ContactName", "ContactName");
            bulkCopy.ColumnMappings.Add("AccountsPayable_Outstanding", "AccountsPayable_Outstanding");
            bulkCopy.ColumnMappings.Add("AccountsPayable_Overdue", "AccountsPayable_Overdue");
            bulkCopy.ColumnMappings.Add("AccountsReceivable_Outstanding", "AccountsReceivable_Outstanding");
            bulkCopy.ColumnMappings.Add("AccountsReceivable_Overdue", "AccountsReceivable_Overdue");
            bulkCopy.ColumnMappings.Add("ContactStatus", "ContactStatus");
            bulkCopy.ColumnMappings.Add("FirstName", "FirstName");
            bulkCopy.ColumnMappings.Add("LastName", "LastName");
            bulkCopy.ColumnMappings.Add("IsCustomer", "IsCustomer");
            bulkCopy.ColumnMappings.Add("IsSupplier", "IsSupplier");
            bulkCopy.ColumnMappings.Add("PaymentTerms_Bills_Day", "PaymentTerms_Bills_Day");
            bulkCopy.ColumnMappings.Add("PaymentTerms_Bills_Type", "PaymentTerms_Bills_Type");
            bulkCopy.ColumnMappings.Add("PaymentTerms_Sales_Day", "PaymentTerms_Sales_Day");
            bulkCopy.ColumnMappings.Add("PaymentTerms_Sales_Type", "PaymentTerms_Sales_Type");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");

            try
            {
                // Write from the source to the destination.
                _logger.Info(inUserName + " ~Copying to the relational table...");

                bulkCopy.WriteToServer(Contacts_Table);
                _logger.Info(inUserName + " ~Execution of GetAllContacts method completed.");
            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~An Execution of XeroToCJ Copying to the relational table completed. Error details:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }


        //--------------------------------------------------------------------------------------
        protected void GetPurchaseOrders(DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of XeroToCJ GetPurchaseOrders method started.");

                String selectedOrgShortCode = inOrgShortCode;
                String selectedOrgName = inOrgName;

                //------------------------------------------------------------//
                //    Addresses: creating the relevant data table and columns
                //------------------------------------------------------------//            

                DataTable PurchaseOrders = new DataTable("PurchaseOrders");

                DataColumn PurchaseOrderID = new DataColumn();
                PurchaseOrderID.DataType = System.Type.GetType("System.String");
                PurchaseOrderID.ColumnName = "PurchaseOrderID";
                PurchaseOrders.Columns.Add(PurchaseOrderID);


                DataColumn PurchaseOrderNumber = new DataColumn();
                PurchaseOrderNumber.DataType = System.Type.GetType("System.String");
                PurchaseOrderNumber.ColumnName = "PurchaseOrderNumber";
                PurchaseOrders.Columns.Add(PurchaseOrderNumber);

                DataColumn DateString = new DataColumn();
                DateString.DataType = System.Type.GetType("System.String");
                DateString.ColumnName = "DateString";
                PurchaseOrders.Columns.Add(DateString);


                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                PurchaseOrders.Columns.Add(Date);

                DataColumn DeliveryDateString = new DataColumn();
                DeliveryDateString.DataType = System.Type.GetType("System.String");
                DeliveryDateString.ColumnName = "DeliveryDateString";
                PurchaseOrders.Columns.Add(DeliveryDateString);

                DataColumn DeliveryDate = new DataColumn();
                DeliveryDate.DataType = System.Type.GetType("System.DateTime");
                DeliveryDate.ColumnName = "DeliveryDate";
                PurchaseOrders.Columns.Add(DeliveryDate);

                DataColumn DeliveryAddress = new DataColumn();
                DeliveryAddress.DataType = System.Type.GetType("System.String");
                DeliveryAddress.ColumnName = "DeliveryAddress";
                PurchaseOrders.Columns.Add(DeliveryAddress);

                DataColumn AttentionTo = new DataColumn();
                AttentionTo.DataType = System.Type.GetType("System.String");
                AttentionTo.ColumnName = "AttentionTo";
                PurchaseOrders.Columns.Add(AttentionTo);

                DataColumn Telephone = new DataColumn();
                Telephone.DataType = System.Type.GetType("System.String");
                Telephone.ColumnName = "Telephone";
                PurchaseOrders.Columns.Add(Telephone);

                DataColumn DeliveryInstructions = new DataColumn();
                DeliveryInstructions.DataType = System.Type.GetType("System.String");
                DeliveryInstructions.ColumnName = "DeliveryInstructions";
                PurchaseOrders.Columns.Add(DeliveryInstructions);

                DataColumn IsDiscounted = new DataColumn();
                IsDiscounted.DataType = System.Type.GetType("System.String");
                IsDiscounted.ColumnName = "IsDiscounted";
                PurchaseOrders.Columns.Add(IsDiscounted);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                PurchaseOrders.Columns.Add(Reference);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                PurchaseOrders.Columns.Add(Type);

                DataColumn CurrencyRate = new DataColumn();
                CurrencyRate.DataType = System.Type.GetType("System.Decimal");
                CurrencyRate.ColumnName = "CurrencyRate";
                PurchaseOrders.Columns.Add(CurrencyRate);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                PurchaseOrders.Columns.Add(CurrencyCode);

                DataColumn ContactID = new DataColumn();
                ContactID.DataType = System.Type.GetType("System.String");
                ContactID.ColumnName = "ContactID";
                PurchaseOrders.Columns.Add(ContactID);

                DataColumn BrandingThemeID = new DataColumn();
                BrandingThemeID.DataType = System.Type.GetType("System.String");
                BrandingThemeID.ColumnName = "BrandingThemeID";
                PurchaseOrders.Columns.Add(BrandingThemeID);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                PurchaseOrders.Columns.Add(Status);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.String");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                PurchaseOrders.Columns.Add(LineAmountTypes);

                DataColumn LineItemAccountCode = new DataColumn();
                LineItemAccountCode.DataType = System.Type.GetType("System.String");
                LineItemAccountCode.ColumnName = "LineItemAccountCode";
                PurchaseOrders.Columns.Add(LineItemAccountCode);

                DataColumn LineItemID = new DataColumn();
                LineItemID.DataType = System.Type.GetType("System.String");
                LineItemID.ColumnName = "LineItemID";
                PurchaseOrders.Columns.Add(LineItemID);

                DataColumn LineItemDescription = new DataColumn();
                LineItemDescription.DataType = System.Type.GetType("System.String");
                LineItemDescription.ColumnName = "LineItemDescription";
                PurchaseOrders.Columns.Add(LineItemDescription);

                DataColumn LineItemUnitAmount = new DataColumn();
                LineItemUnitAmount.DataType = System.Type.GetType("System.Decimal");
                LineItemUnitAmount.ColumnName = "LineItemUnitAmount";
                PurchaseOrders.Columns.Add(LineItemUnitAmount);

                DataColumn LineItemTaxType = new DataColumn();
                LineItemTaxType.DataType = System.Type.GetType("System.String");
                LineItemTaxType.ColumnName = "LineItemTaxType";
                PurchaseOrders.Columns.Add(LineItemTaxType);

                DataColumn LineItemTaxAmount = new DataColumn();
                LineItemTaxAmount.DataType = System.Type.GetType("System.Decimal");
                LineItemTaxAmount.ColumnName = "LineItemTaxAmount";
                PurchaseOrders.Columns.Add(LineItemTaxAmount);

                DataColumn LineItemLineAmount = new DataColumn();
                LineItemLineAmount.DataType = System.Type.GetType("System.Decimal");
                LineItemLineAmount.ColumnName = "LineItemLineAmount";
                PurchaseOrders.Columns.Add(LineItemLineAmount);

                DataColumn LineItemTrackingName1 = new DataColumn();
                LineItemTrackingName1.DataType = System.Type.GetType("System.String");
                LineItemTrackingName1.ColumnName = "LineItemTrackingName1";
                PurchaseOrders.Columns.Add(LineItemTrackingName1);

                DataColumn LineItemTrackingName2 = new DataColumn();
                LineItemTrackingName2.DataType = System.Type.GetType("System.String");
                LineItemTrackingName2.ColumnName = "LineItemTrackingName2";
                PurchaseOrders.Columns.Add(LineItemTrackingName2);

                DataColumn LineItemTrackingOption1 = new DataColumn();
                LineItemTrackingOption1.DataType = System.Type.GetType("System.String");
                LineItemTrackingOption1.ColumnName = "LineItemTrackingOption1";
                PurchaseOrders.Columns.Add(LineItemTrackingOption1);

                DataColumn LineItemTrackingOption2 = new DataColumn();
                LineItemTrackingOption2.DataType = System.Type.GetType("System.String");
                LineItemTrackingOption2.ColumnName = "LineItemTrackingOption2";
                PurchaseOrders.Columns.Add(LineItemTrackingOption2);

                DataColumn LineItemQuantity = new DataColumn();
                LineItemQuantity.DataType = System.Type.GetType("System.Decimal");
                LineItemQuantity.ColumnName = "LineItemQuantity";
                PurchaseOrders.Columns.Add(LineItemQuantity);

                DataColumn SubTotal = new DataColumn();
                SubTotal.DataType = System.Type.GetType("System.Decimal");
                SubTotal.ColumnName = "SubTotal";
                PurchaseOrders.Columns.Add(SubTotal);

                DataColumn TotalTax = new DataColumn();
                TotalTax.DataType = System.Type.GetType("System.Decimal");
                TotalTax.ColumnName = "TotalTax";
                PurchaseOrders.Columns.Add(TotalTax);

                DataColumn TotalDiscount = new DataColumn();
                TotalDiscount.DataType = System.Type.GetType("System.Decimal");
                TotalDiscount.ColumnName = "TotalDiscount";
                PurchaseOrders.Columns.Add(TotalDiscount);

                DataColumn Total = new DataColumn();
                Total.DataType = System.Type.GetType("System.Decimal");
                Total.ColumnName = "Total";
                PurchaseOrders.Columns.Add(Total);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.DateTime");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                PurchaseOrders.Columns.Add(UpdatedDateUTC);

                DataColumn HasAttachments = new DataColumn();
                HasAttachments.DataType = System.Type.GetType("System.String");
                HasAttachments.ColumnName = "HasAttachments";
                PurchaseOrders.Columns.Add(HasAttachments);

                DataColumn OrgShortCode = new DataColumn();
                OrgShortCode.DataType = System.Type.GetType("System.String");
                OrgShortCode.ColumnName = "OrgShortCode";
                PurchaseOrders.Columns.Add(OrgShortCode);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                PurchaseOrders.Columns.Add(OrgName);

                int page = 1;

                _logger.Info(inUserName + " ~In Progress GetPurchaseOrders method. Step1 started");

                List<PurchaseOrder> batchOfPOs = new List<PurchaseOrder>();// api.Contacts.Page(page).OrderBy("JournalNumber").Find().ToList();
                List<PurchaseOrder> POs = new List<PurchaseOrder>();// api.Contacts.Find().ToList();
                try
                {

                    while ((batchOfPOs = api.PurchaseOrders.Where(string.Format("Date >= DateTime({0},{1},{2})", startDate.Year, startDate.Month, startDate.Day))
                                    .And(string.Format("Date <= DateTime({0},{1},{2})", endDate.Year, endDate.Month, endDate.Day))
                                                 .Page(page).OrderBy("PurchaseOrderID").Find().ToList()).Count() > 0)
                    {
                        POs.AddRange(batchOfPOs);
                        page++; ;

                        batchOfPOs.Clear();
                        _logger.Info(inUserName + " ~In Progress GetPurchaseOrders method. Step2 started, while loop - Count:" + POs.Count + " Page:" + page);
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                catch (XeroApiException ex)
                {
                    _logger.Error((inUserName ?? "") + " ~XeroError: In Porgress GetPurchaseOrders in while loop ,step 2. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message + " StackTrace:" + ex.StackTrace, ex);
                    if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                    {
                        exceptionMethodNameList.Add("GetPurchaseOrders");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(inUserName + " ~Error: In Porgress GetPurchaseOrders in while loop ,step 2. Error details:" + ex.Message + " StackTrace:" + ex.StackTrace, ex);
                }

                int j = 0;
                try
                {
                    foreach (PurchaseOrder PO in POs)
                    {
                        //PO.AttentionTo.ToString();

                        foreach (LineItem item in PO.LineItems)
                        {
                            DataRow row = PurchaseOrders.NewRow();

                            row["PurchaseOrderID"] = PO.Id;
                            row["PurchaseOrderNumber"] = PO.Number;
                            row["DateString"] = PO.Date;
                            row["Date"] = PO.Date ?? DateTime.MinValue;
                            row["DeliveryDateString"] = PO.DeliveryDate;
                            row["DeliveryDate"] = PO.DeliveryDate ?? DateTime.MinValue;
                            row["DeliveryAddress"] = PO.DeliveryAddress;
                            row["AttentionTo"] = PO.AttentionTo;
                            row["Telephone"] = PO.Telephone;
                            row["DeliveryInstructions"] = PO.DeliveryInstructions;
                            //row["IsDiscounted"] = "";
                            row["Reference"] = PO.Reference;
                            //row["Type"] = item.t
                            row["CurrencyRate"] = PO.CurrencyRate ?? 0;
                            row["CurrencyCode"] = PO.CurrencyCode;
                            row["ContactID"] = PO.Contact.Id;
                            row["BrandingThemeID"] = PO.BrandingThemeID;
                            row["Status"] = PO.Status;
                            row["LineAmountTypes"] = PO.LineAmountTypes.ToString();
                            row["LineItemAccountCode"] = item.AccountCode;
                            row["LineItemID"] = item.LineItemId;
                            row["LineItemDescription"] = item.Description;
                            row["LineItemUnitAmount"] = item.UnitAmount ?? 0;
                            row["LineItemTaxType"] = item.TaxType;
                            row["LineItemTaxAmount"] = item.TaxAmount ?? 0;
                            row["LineItemLineAmount"] = item.LineAmount ?? 0;
                            if (item.Tracking.Count == 0)
                            {
                                row["LineItemTrackingName1"] = "";
                                row["LineItemTrackingName2"] = "";
                                row["LineItemTrackingOption1"] = "";
                                row["LineItemTrackingOption2"] = "";
                            }
                            else if (item.Tracking.Count == 1)
                            {
                                row["LineItemTrackingName1"] = item.Tracking[0].Name;
                                row["LineItemTrackingOption1"] = item.Tracking[0].Option;
                            }
                            else if (item.Tracking.Count == 2)
                            {
                                row["LineItemTrackingName1"] = item.Tracking[0].Name;
                                row["LineItemTrackingOption1"] = item.Tracking[0].Option;
                                row["LineItemTrackingName2"] = item.Tracking[1].Name;
                                row["LineItemTrackingOption2"] = item.Tracking[1].Option;
                            }
                            else
                            {
                                row["LineItemTrackingName1"] = "";
                                row["LineItemTrackingName2"] = "";
                                row["LineItemTrackingOption1"] = "";
                                row["LineItemTrackingOption2"] = "";
                            }

                            row["LineItemQuantity"] = item.Quantity;
                            row["SubTotal"] = PO.SubTotal ?? 0;
                            row["TotalTax"] = PO.TotalTax ?? 0;
                            row["TotalDiscount"] = PO.TotalDiscount ?? 0;
                            row["Total"] = PO.Total ?? 0;
                            row["UpdatedDateUTC"] = PO.UpdatedDateUtc ?? DateTime.MinValue;
                            row["HasAttachments"] = PO.HasAttachments;
                            row["OrgShortCode"] = selectedOrgShortCode;
                            row["OrgName"] = selectedOrgName;

                            PurchaseOrders.Rows.Add(row);

                        }

                    }
                }
                catch (XeroApiException ex)
                {
                    _logger.Error((inUserName ?? "") + " ~XeroError: In Porgress GetPurchaseOrders in for loop ,step 2. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message + " StackTrace:" + ex.StackTrace, ex);
                    if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                    {
                        exceptionMethodNameList.Add("GetPurchaseOrders");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(inUserName + " ~Error: In Porgress GetPurchaseOrders in for loop ,step 2. Error details:" + ex.Message + " StackTrace:" + ex.StackTrace, ex);
                }

                if (CheckValidDatabase())
                {
                    clear_table("PurchaseOrders");
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(user_conn))
                    {

                        BulkInsertPurchaseOrders(bulkCopy, PurchaseOrders);

                    }
                }

                if (bool.Parse(CommonXero.GetIncludeAzure()))
                {
                    if (CheckAzureValidDatabase())
                    {
                        azure_clear_table("PurchaseOrders");
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(azure_user_conn))
                        {

                            BulkInsertPurchaseOrders(bulkCopy, PurchaseOrders);

                        }
                    }
                }
            }
            catch (XeroApiException ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ XeroApiException GetPurchaseOrders completed. Organization :" + (inOrgName ?? "") + " Start date : " + startDate + "End date :" + endDate + " " + ex.Message, ex);
                if (ex.Code == System.Net.HttpStatusCode.ServiceUnavailable)
                {
                    exceptionMethodNameList.Add("GetPurchaseOrders");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(inUserName + " ~An Execution of XeroToCJ GetPurchaseOrders completed. Error details:" + ex.Message, ex);
            }
        }

        public void BulkInsertPurchaseOrders(SqlBulkCopy bulkCopy, DataTable PurchaseOrders)
        {
            bulkCopy.DestinationTableName =
                            "PurchaseOrders";
            bulkCopy.ColumnMappings.Add("PurchaseOrderID", "PurchaseOrderID");
            bulkCopy.ColumnMappings.Add("PurchaseOrderNumber", "PurchaseOrderNumber");
            bulkCopy.ColumnMappings.Add("DateString", "DateString");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("DeliveryDateString", "DeliveryDateString");
            bulkCopy.ColumnMappings.Add("DeliveryDate", "DeliveryDate");
            bulkCopy.ColumnMappings.Add("DeliveryAddress", "DeliveryAddress");
            bulkCopy.ColumnMappings.Add("AttentionTo", "AttentionTo");
            bulkCopy.ColumnMappings.Add("Telephone", "Telephone");
            bulkCopy.ColumnMappings.Add("DeliveryInstructions", "DeliveryInstructions");

            bulkCopy.ColumnMappings.Add("Reference", "Reference");

            bulkCopy.ColumnMappings.Add("CurrencyRate", "CurrencyRate");
            bulkCopy.ColumnMappings.Add("CurrencyCode", "CurrencyCode");
            bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
            bulkCopy.ColumnMappings.Add("BrandingThemeID", "BrandingThemeID");
            bulkCopy.ColumnMappings.Add("Status", "Status");
            bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
            bulkCopy.ColumnMappings.Add("LineItemAccountCode", "LineItemAccountCode");
            bulkCopy.ColumnMappings.Add("LineItemID", "LineItemID");
            bulkCopy.ColumnMappings.Add("LineItemDescription", "LineItemDescription");
            bulkCopy.ColumnMappings.Add("LineItemUnitAmount", "LineItemUnitAmount");
            bulkCopy.ColumnMappings.Add("LineItemTaxType", "LineItemTaxType");
            bulkCopy.ColumnMappings.Add("LineItemTaxAmount", "LineItemTaxAmount");
            bulkCopy.ColumnMappings.Add("LineItemLineAmount", "LineItemLineAmount");
            bulkCopy.ColumnMappings.Add("LineItemTrackingName1", "LineItemTrackingName1");
            bulkCopy.ColumnMappings.Add("LineItemTrackingName2", "LineItemTrackingName2");
            bulkCopy.ColumnMappings.Add("LineItemTrackingOption1", "LineItemTrackingOption1");
            bulkCopy.ColumnMappings.Add("LineItemTrackingOption2", "LineItemTrackingOption2");
            bulkCopy.ColumnMappings.Add("LineItemQuantity", "LineItemQuantity");
            bulkCopy.ColumnMappings.Add("SubTotal", "SubTotal");
            bulkCopy.ColumnMappings.Add("TotalTax", "TotalTax");
            bulkCopy.ColumnMappings.Add("TotalDiscount", "TotalDiscount");
            bulkCopy.ColumnMappings.Add("Total", "Total");
            bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
            bulkCopy.ColumnMappings.Add("HasAttachments", "HasAttachments");
            bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
            bulkCopy.ColumnMappings.Add("OrgName", "OrgName");

            try
            {
                _logger.Info(inUserName + " ~Copying to the relational table PurchaseOrders ...");

                // Write from the source to the destination.
                bulkCopy.WriteToServer(PurchaseOrders);
            }
            catch (Exception ex)
            {
                //
                _logger.Error(inUserName + " ~AnExecution of XeroToCJ copying to the relational table PurchaseOrders . Error details:" + ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
        }


        private void SendEmail(bool successStatus)
        {
            try
            {
                _logger.Info(inUserName + " ~Execution of SendEmail method started.");
                CommonXero.SendEmail(successStatus, inUserName);
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + " ~Execution of XeroToCJ SendEmail completed:" + ex.Message, ex);
            }
        }


        private Dictionary<string, string> GetPairOfMailOnApiException(string functionName, DateTime? startDate, DateTime? endDate)
        {

            var replacePair = new Dictionary<string, string>();
            var startDateStr = startDate != null ? startDate.Value.ToShortDateString() : "N/A";
            var endsDateStr = endDate != null ? endDate.Value.ToShortDateString() : "N/A";
            replacePair.Add(EmailParameters.OrgName, inOrgName);
            replacePair.Add(EmailParameters.StartDate, startDateStr);
            replacePair.Add(EmailParameters.EndDate, endsDateStr);
            replacePair.Add(EmailParameters.FunctionName, functionName);
            return replacePair;
        }


        private void SendMailOnApiException(string functionName, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                _logger.Info((inUserName ?? "") + " ~Execution of XeroToCJ SendMailOnApiException event started.");
                SmtpClient mailclient = new SmtpClient();
                mailclient.ServicePoint.MaxIdleTime = 2;
                var attachmentFrom = CommonXero.GetEmailAttachmentFrom();
                var replacePair = GetPairOfMailOnApiException(functionName, startDate, endDate);
                var toAddress = inUserName;
                MailMessage mailMessage = new MailMessage();
                mailMessage = CommonXero.GetMailMessage(EmailTemplates.SendMailOnApiException, replacePair, toAddress, attachmentFrom);
                mailMessage.IsBodyHtml = true;
                if (CommonXero.GetEnableSSLKey() == true)
                {
                    mailclient.EnableSsl = true;
                }
                mailclient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                _logger.Error((inUserName ?? "") + "~Execution of XeroToCJ SendMailOnApiException event completed. :" + ex.Message, ex);
                throw;
            }
        }

    }
}