﻿using ClearJelly.ViewModels.UserMappingModels;
using Dapper;
using System;
using System.Data.SqlClient;

namespace ClearJelly.XeroApp
{
    public class ReceiptRepository
    {
        private string _connection;

        public ReceiptRepository(string connectionString)
        {
            _connection = connectionString;
        }

        public void InsertReceipt(Receipt receipt)
        {
            try
            {
                var query = @"INSERT INTO Receipts (Id, Date, Contact_Id, Reference, LineAmountTypes, SubTotal, TotalTax, Total,
                            Status, ReceiptNumber, UpdatedDateUTC, HasAttachments, Url, XEROUser_Id, OrgShortCode, OrgName)
                            VALUES (@Id, @Date, @Contact_Id,@Reference,@LineAmountTypes,@SubTotal,@TotalTax,@Total,
                            @Status, @ReceiptNumber, @UpdatedDateUTC, @HasAttachments, @Url, @XEROUser_Id, @OrgShortCode, @OrgName);";
                using (var con = new SqlConnection(_connection))
                {
                    var parameters = new
                    {
                        Id = Guid.NewGuid().ToString(),
                        Date = receipt.Date,
                        Contact_Id = receipt.Contact_Id,
                        Reference = receipt.Reference,
                        LineAmountTypes = receipt.LineAmountTypes,
                        SubTotal = receipt.SubTotal,
                        TotalTax = receipt.TotalTax,
                        Total = receipt.Total,
                        Status = receipt.Status,
                        ReceiptNumber = receipt.ReceiptNumber,
                        UpdatedDateUTC = receipt.UpdatedDateUTC,
                        HasAttachments = receipt.HasAttachments,
                        Url = receipt.Url,
                        XEROUser_Id = receipt.XEROUserId,
                        OrgShortCode = receipt.OrgShortCode,
                        OrgName = receipt.OrgName

                    };
                    var result = con.Execute(query, parameters);
                }
            }
            catch (Exception)
            {

            }
        }

        public void InsertLineItem(Xero.Api.Core.Model.LineItem lineItem, string receiptId)
        {
            try
            {
                var query = @"INSERT INTO ReceiptLineItems (LineItemId, Description, Quantity, UnitAmount, AccountCode, ItemCode, TaxType, TaxAmount,
                            LineAmount)
                            VALUES (@LineItemId, @Description, @Quantity,@UnitAmount,@AccountCode,@ItemCode,@TaxType,@TaxAmount,
                            @LineAmount);";
                using (var con = new SqlConnection(_connection))
                {
                    var parameters = new
                    {
                        LineItemId = lineItem.LineItemId.ToString(),
                        Description = lineItem.Description,
                        Quantity = lineItem.Quantity,
                        UnitAmount = lineItem.UnitAmount,
                        AccountCode = lineItem.AccountCode,
                        ItemCode = lineItem.ItemCode,
                        TaxType = lineItem.TaxType,
                        TaxAmount = lineItem.TaxAmount,
                        LineAmount = lineItem.LineAmount,
                        DiscountRate = lineItem.DiscountRate
                    };
                    var result = con.Execute(query, parameters);
                }
                InsertItemInReceipt(lineItem.LineItemId.ToString(), receiptId);
            }
            catch (Exception)
            {

            }
        }

        public void InsertItemInReceipt(string lineItemId, string receiptId)
        {
            var query = @"INSERT INTO LineItemInReceipt (Id, LineItemId, ReceiptId)
                        VALUES (@Id, @LineItemId, @ReceiptId)";

            using (var con = new SqlConnection(_connection))
            {
                var parameters = new
                {
                    Id = Guid.NewGuid().ToString(),
                    LineItemId = lineItemId,
                    ReceiptId = receiptId,
                };
                var result = con.Execute(query, parameters);
            }
        }
    }
}

