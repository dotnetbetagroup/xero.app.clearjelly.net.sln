﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearJelly.XeroApp
{
    public class BaseEntity<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : class
    {
        private Guid id;
        protected BaseEntity()
        {

            DateTime now = DateTime.Now;
            CreationDate = new DateTime(now.Ticks / 100000 * 100000, now.Kind);
        }


        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id
        {
            get
            {
                if (id == null)
                {
                    id = Guid.NewGuid();
                }
                return id;
            }
            set { id = value; }
        }

        //[Required]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public Guid Id { get; set; }

        [Required]
        public int OldId { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
