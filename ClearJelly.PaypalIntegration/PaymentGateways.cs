﻿using System.Collections.Generic;
using PaypalIntegration.PayPallHelper;

namespace PaypalIntegration
{
  
    public interface IPayPalExpressCheckout
    {
        /// <summary>
        /// Starts a PayPal express checkout session by providing a token and URL
        /// for the user to be redirected to.
        /// </summary>
        /// <param name="amount">Transaction amount.</param>
        /// <param name="cancelUrl">URL to return to if the customer cancels the checkout process.</param>
        /// <param name="confirmationUrl">URL to return to for the customer to confirm payment and place the order.</param>
        /// <returns>
        /// A PayPal token for the express checkout and URL to redirect the customer to.
        /// When the customer navigates to the confirmationUrl, you should then call
        /// <see cref="GetExpressCheckoutDetails"/> to retrieve details about the express checkout.
        /// </returns>
        PayPalExpressCheckoutToken SetExpressCheckout(decimal amount, PayPalEnums.CurrencyCodeType currencyCodeType, string cancelUrl, string confirmationUrl);

        /// <summary>
        /// Starts a PayPal express checkout session by providing a token and URL
        /// for the user to be redirected to.
        /// </summary>
        /// <param name="orderDetails">Detailed information on the order this checkout is for.</param>
        /// <param name="cancelUrl">URL to return to if the customer cancels the checkout process.</param>
        /// <param name="confirmationUrl">URL to return to for the customer to confirm payment and place the order.</param>
        /// <returns>
        /// A PayPal token for the express checkout and URL to redirect the customer to.
        /// When the customer navigates to the confirmationUrl, you should then call
        /// <see cref="GetExpressCheckoutDetails"/> to retrieve details about the express checkout.
        /// </returns>
        PayPalExpressCheckoutToken SetExpressCheckout(OrderDetails orderDetails, string cancelUrl, string confirmationUrl);

        /// <summary>
        /// Retrieves information about the express checkout required to carry out authorisation of payment.
        /// </summary>
        /// <param name="payPalToken">The PayPal token returned in the initial <see cref="SetExpressCheckout"/> call.</param>
        /// <returns>
        /// Details about the express checkout, such as the customer details, and PayPal PayerId, which 
        /// must be passed to <see cref="DoExpressCheckoutPayment"/> to perform the payment.
        /// </returns>
        PayPalExpressCheckoutDetails GetExpressCheckoutDetails(string payPalToken);

        /// <summary>
        /// Performs the payment.
        /// </summary>
        /// <param name="amount">Transaction amount.  You may have adjusted the amount depending on the delivery options the customer specified in PayPal.</param>
        /// <param name="payPalToken">The PayPal token returned in the initial <see cref="SetExpressCheckout"/> call.</param>
        /// <param name="payPalPayerId">The PayPal PayerID returned in the <see cref="GetExpressCheckoutDetails"/> call.</param>
        IPaymentResponse DoExpressCheckoutPayment(decimal amount, PayPalEnums.CurrencyCodeType currencyCodeType, string payPalToken, string payPalPayerId);

        /// <summary>
        /// Performs the payment.
        /// </summary>
        /// <param name="orderDetails">Detailed information on the order this checkout is for.</param>
        /// <param name="payPalToken">The PayPal token returned in the initial <see cref="SetExpressCheckout"/> call.</param>
        /// <param name="payPalPayerId">The PayPal PayerID returned in the <see cref="GetExpressCheckoutDetails"/> call.</param>        
        IPaymentResponse DoExpressCheckoutPayment(OrderDetails orderDetails, string payPalToken, string payPalPayerId);

        /// <summary>
        /// Performs a full transaction refund.
        /// </summary>
        /// <param name="transactionId">The PayPal transaction id to be refunded.</param>
        /// <returns>The status of the refund.</returns>
        IPayPalRefundResponse RefundFullTransaction(string transactionId);

        /// <summary>
        /// Performs a partial transaction refund.
        /// </summary>
        /// <param name="transactionId">The transaction Id received from the DoExpressCheckoutPayment call.</param>
        /// <param name="amount">The amount to refund.</param>
        /// <param name="currencyCodeType">The Currency Code for the refund.</param>
        /// <param name="description">A note about the refund. Optional.</param>
        /// <returns>The status of the refund.</returns>
        IPayPalRefundResponse RefundPartialTransaction(string transactionId, decimal amount, PayPalEnums.CurrencyCodeType currencyCodeType, string description = null);
    }

    
}
