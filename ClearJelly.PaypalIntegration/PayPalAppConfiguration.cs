﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using PaypalIntegration.PayPallHelper;

namespace PaypalIntegration
{
    public class PayPalAppConfiguration : ConfigurationSection
    {
        /// <summary>
        /// The current configuration from an application config file.
        /// </summary>
        public static PayPalAppConfiguration Current
        {
            get { return (PayPalAppConfiguration)ConfigurationManager.GetSection(SectionName); }
        }

        public const string SectionName = "PayPalWebApp";

        static class Elements
        {
            public const string DataCashMoTo = "DataCashMoTo";
            public const string DataCash3DSecure = "DataCash3DSecure";
            public const string PayPal = "PayPal";
        }

        private const string XmlNamespaceConfigurationPropertyName = "xmlns";
        [ConfigurationProperty(XmlNamespaceConfigurationPropertyName)]
        public string XmlNamespace
        {
            get { return (string)this[XmlNamespaceConfigurationPropertyName]; }
            set { this[XmlNamespaceConfigurationPropertyName] = value; }
        }


        [ConfigurationProperty(Elements.PayPal)]
        public PayPalConfiguration PayPal
        {
            get { return (PayPalConfiguration)this[Elements.PayPal]; }
            set { this[Elements.PayPal] = value; }
        }
    }
}