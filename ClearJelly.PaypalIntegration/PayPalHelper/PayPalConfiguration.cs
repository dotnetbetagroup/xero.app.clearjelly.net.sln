﻿using System;
using System.Configuration;

namespace PaypalIntegration.PayPallHelper
{
    public class PayPalConfiguration : ConfigurationElement
    {
        const string TestHost = "https://api-3t.sandbox.paypal.com/nvp";
        const string TestCheckoutUrlFormat = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token={0}";

        // Sandbox Credentials provided in Integration Guide
        // https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_ECGettingStarted
        const string TestUser = "dump-facilitator_api1.zealousys.com";
        const string TestPassword = "92LG2796ZUUKYE6F";
        const string TestSignature = "A6Q5gIh1WYiWWWZ9eEvQAtPI9Y5jAnrkUpMd10PptLB4SUo6r-CfOFRS";

        const string LiveHost = "https://api-3t.paypal.com/nvp";
        const string LiveCheckoutUrlFormat = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token={0}";

        internal PayPalConfiguration()
            : this(PayPalEnums.PaymentEnvironment.Test)
        {
        }

        /// <summary>
        /// Used for test environment, where PayPal sandbox credentials are static.
        /// </summary>
        public PayPalConfiguration(PayPalEnums.PaymentEnvironment environment)
            : this(environment, TestUser, TestPassword, TestSignature)
        {
            if (environment == PayPalEnums.PaymentEnvironment.Live)
                throw new ArgumentException("PayPal UserId, Password and Signature must be provided for the live environment.");
        }

        public PayPalConfiguration(PayPalEnums.PaymentEnvironment environment, string userId = null, string password = null, string signature = null)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentNullException("userId");
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException("password");
            if (string.IsNullOrEmpty(signature)) throw new ArgumentNullException("signature");

            //var paypalConfig = (PayPalConfiguration)(System.Configuration.ConfigurationManager.GetSection("paypal"));
            environment = (ConfigurationManager.AppSettings["mode"] != null ? (ConfigurationManager.AppSettings["mode"].ToString() == "sandbox" ? PayPalEnums.PaymentEnvironment.Test : PayPalEnums.PaymentEnvironment.Live) : environment);
            userId = ConfigurationManager.AppSettings["Username"] != null ? ConfigurationManager.AppSettings["Username"] : TestUser;
            password = ConfigurationManager.AppSettings["Password"] != null ? ConfigurationManager.AppSettings["Password"] : TestPassword;
            signature = ConfigurationManager.AppSettings["Signature"] != null ? ConfigurationManager.AppSettings["Signature"] : TestSignature;

            Environment = environment;
            UserId = userId;
            Password = password;
            Signature = signature;


            //var abc = System.Configuration.ConfigurationManager.GetSection("paypal");
        }

        static class Attributes
        {
            public const string Environment = "environment";
            public const string UserId = "userId";
            public const string Password = "password";
            public const string Signature = "signature";
            public const string LocaleCode = "localeCode";
            public const string UseLocaleFromCurrentCulture = "useLocaleFromCurrentCulture";
        }

        [ConfigurationProperty(Attributes.Environment)]
        public PayPalEnums.PaymentEnvironment Environment
        {
            get { return (PayPalEnums.PaymentEnvironment)this[Attributes.Environment]; }
            set { this[Attributes.Environment] = value; }
        }

        [ConfigurationProperty(Attributes.UserId)]
        public string UserId
        {
            get { return (string)this[Attributes.UserId]; }
            set { this[Attributes.UserId] = value; }
        }

        [ConfigurationProperty(Attributes.Password)]
        public string Password
        {
            get { return (string)this[Attributes.Password]; }
            set { this[Attributes.Password] = value; }
        }

        [ConfigurationProperty(Attributes.Signature)]
        public string Signature
        {
            get { return (string)this[Attributes.Signature]; }
            set { this[Attributes.Signature] = value; }
        }

        /// <summary>
        /// PayPal Locale Code to use (optional).
        /// If <seealso cref="UseLocaleFromCurrentCulture"/> is specified, then this is ignored.
        /// </summary>
        [ConfigurationProperty(Attributes.LocaleCode)]
        public string LocaleCode
        {
            get { return (string)this[Attributes.LocaleCode]; }
            set { this[Attributes.LocaleCode] = value; }
        }

        /// <summary>
        /// If PayPal supports it, the current culture is used for the PayPal locale.
        /// If it isn't supported, no locale code is specified (defaults to US).
        /// </summary>
        [ConfigurationProperty(Attributes.UseLocaleFromCurrentCulture)]
        public bool UseLocaleFromCurrentCulture
        {
            get { return (bool)this[Attributes.UseLocaleFromCurrentCulture]; }
            set { this[Attributes.UseLocaleFromCurrentCulture] = value; }
        }

        public string Host
        {
            get { return Environment == PayPalEnums.PaymentEnvironment.Live ? LiveHost : TestHost; }
        }

        public string CheckoutUrlFormat
        {
            get { return Environment == PayPalEnums.PaymentEnvironment.Live ? LiveCheckoutUrlFormat : TestCheckoutUrlFormat; }
        }
    }
}