﻿using System.Collections.Specialized;
using PaypalIntegration;

namespace PaypalIntegration.PayPallHelper
{
    public class PayPalExpressCheckoutToken
    {
        public NameValueCollection PayPalResponse { get; set; }

        public string PayPalToken { get; set; }

        public string RedirectUrl { get; set; }

        public PayPalEnums.PaymentStatus Status { get; set; }

        public bool IsSystemFailure { get; set; }

        public string FailureMessage { get; set; }
    }
}