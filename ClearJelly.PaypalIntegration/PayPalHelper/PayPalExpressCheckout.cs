﻿using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Web;

namespace PaypalIntegration.PayPallHelper
{
    public class PayPalExpressCheckout
    {

        private readonly PayPalConfiguration _configuration;
        private readonly IHttpClient _httpClient;
        private readonly PayPalRequestBuilder _requestBuilder;
        private readonly PayPalResponseParser _responseParser;

        /// <summary>
        /// For testing.
        /// </summary>
        public PayPalExpressCheckout()
        {
            //if (configuration == null) throw new ArgumentNullException("configuration");
            //if (httpClient == null) throw new ArgumentNullException("httpClient");
            //if (requestBuilder == null) throw new ArgumentNullException("requestBuilder");
            //if (responseParser == null) throw new ArgumentNullException("responseParser");
            //_configuration = configuration;
            //_httpClient = httpClient;
            //_requestBuilder = requestBuilder;
            //_responseParser = responseParser;

            _configuration = new PayPalConfiguration();
            _httpClient = new HttpClient(); ;
            _requestBuilder = new PayPalRequestBuilder(_configuration);
            _responseParser = new PayPalResponseParser(_configuration);
        }

        public PayPalExpressCheckoutToken SetExpressCheckout(decimal amount, PayPalEnums.CurrencyCodeType currencyCodeType, string cancelUrl, string confirmationUrl)
        {
            if (amount <= 0) throw new ArgumentOutOfRangeException("amount", "Amount must be greater than zero.");
            if (string.IsNullOrWhiteSpace(cancelUrl)) throw new ArgumentNullException("cancelUrl");
            if (string.IsNullOrWhiteSpace(confirmationUrl)) throw new ArgumentNullException("confirmationUrl");

            //_logger.Log("SetExpressCheckout.Request", new { Amount = amount, CancelUrl = cancelUrl, ConfirmationUrl = confirmationUrl });

            var request = _requestBuilder.SetExpressCheckout(amount, currencyCodeType, cancelUrl, confirmationUrl);
            return setExpressCheckoutRequestFor(request);
        }

        public PayPalExpressCheckoutToken SetExpressCheckout(OrderDetails orderDetails, string cancelUrl, string confirmationUrl)
        {
            if (orderDetails == null) throw new ArgumentNullException("orderDetails");
            if (string.IsNullOrWhiteSpace(cancelUrl)) throw new ArgumentNullException("cancelUrl");
            if (string.IsNullOrWhiteSpace(confirmationUrl)) throw new ArgumentNullException("confirmationUrl");

            //_logger.Log("SetExpressCheckout.Request", orderDetails);

            var request = _requestBuilder.SetExpressCheckout(orderDetails, cancelUrl, confirmationUrl);
            return setExpressCheckoutRequestFor(request);
        }

        public PayPalExpressCheckoutToken SetExpressCheckout(OrderDetails orderDetails, string cancelUrl, string confirmationUrl, NameValueCollection optionalFields)
        {
            if (orderDetails == null) throw new ArgumentNullException("orderDetails");
            if (string.IsNullOrWhiteSpace(cancelUrl)) throw new ArgumentNullException("cancelUrl");
            if (string.IsNullOrWhiteSpace(confirmationUrl)) throw new ArgumentNullException("confirmationUrl");

            //_logger.Log("SetExpressCheckout.Request", orderDetails);

            var request = _requestBuilder.SetExpressCheckout(orderDetails, cancelUrl, confirmationUrl, optionalFields);
            return setExpressCheckoutRequestFor(request);
        }

        PayPalExpressCheckoutToken setExpressCheckoutRequestFor(NameValueCollection request)
        {
            var response = sendToPayPal(request);

            //_logger.Log("SetExpressCheckout.Response", response);

            return _responseParser.SetExpressCheckout(response);
        }

        public PayPalExpressCheckoutDetails GetExpressCheckoutDetails(string payPalToken)
        {
            if (string.IsNullOrWhiteSpace(payPalToken)) throw new ArgumentNullException("payPalToken");

            var request = _requestBuilder.GetExpressCheckoutDetails(payPalToken);
            var response = sendToPayPal(request);
            return _responseParser.GetExpressCheckoutDetails(response);
        }

        public IPaymentResponse DoExpressCheckoutPayment(decimal amount, PayPalEnums.CurrencyCodeType currencyCodeType, string payPalToken, string payPalPayerId)
        {
            if (amount <= 0) throw new ArgumentOutOfRangeException("amount", "Amount must be greater than zero.");
            if (string.IsNullOrWhiteSpace(payPalToken)) throw new ArgumentNullException("payPalToken");
            if (string.IsNullOrWhiteSpace(payPalPayerId)) throw new ArgumentNullException("payPalPayerId");

            var request = _requestBuilder.DoExpressCheckoutPayment(amount, currencyCodeType, payPalToken, payPalPayerId);
            return doExpressCheckoutPaymentFor(request);
        }

        public IPaymentResponse GetRecurringPaymentsProfileDetails(string recurringProfileId) {
            var request = _requestBuilder.GetRecurringPaymentsProfileDetails(recurringProfileId);
            return GetRecurringPaymentsProfileResponse(request);
        }



        public IPaymentResponse BillOutstandingAmount(string recurringProfileId)
        {
            //if (amount <= 0) throw new ArgumentOutOfRangeException("amount", "Amount must be greater than zero.");
          
            var request = _requestBuilder.BillOutstandingAmount(recurringProfileId);
            return billOutstandingAmount(request);
        }



        public IPaymentResponse DoExpressCheckoutPayment(OrderDetails orderDetails, string payPalToken, string payPalPayerId)
        {
            if (orderDetails == null) throw new ArgumentNullException("orderDetails");
            if (string.IsNullOrWhiteSpace(payPalToken)) throw new ArgumentNullException("payPalToken");
            if (string.IsNullOrWhiteSpace(payPalPayerId)) throw new ArgumentNullException("payPalPayerId");

            var request = _requestBuilder.DoExpressCheckoutPayment(orderDetails, payPalToken, payPalPayerId);
            return doExpressCheckoutPaymentFor(request);
        }

        IPaymentResponse doExpressCheckoutPaymentFor(NameValueCollection request)
        {
            var response = sendToPayPal(request);
            return _responseParser.DoExpressCheckoutPayment(response);
        }

        IPaymentResponse billOutstandingAmount(NameValueCollection request)
        {
            var response = sendToPayPal(request);
            return _responseParser.BillOutstandingAmountResponse(response);
        }

        IPaymentResponse GetRecurringPaymentsProfileResponse(NameValueCollection request)
        {
            var response = sendToPayPal(request);
            return _responseParser.BillOutstandingAmountResponse(response);
        }


        private NameValueCollection sendToPayPal(NameValueCollection queryString)
        {
            return HttpUtility.ParseQueryString(_httpClient.Post(_configuration.Host, queryString.ToString()));
        }

        public IPayPalRefundResponse RefundFullTransaction(string transactionId)
        {
            if (String.IsNullOrWhiteSpace(transactionId)) throw new ArgumentNullException("transactionId", "Transaction id must be specified");

            var request = _requestBuilder.RefundFullTransaction(transactionId);
            var response = sendToPayPal(request);
            return _responseParser.RefundTransaction(response);
        }

        public IPayPalRefundResponse RefundPartialTransaction(string transactionId, decimal amount, PayPalEnums.CurrencyCodeType currencyCodeType, string description = null)
        {
            if (String.IsNullOrWhiteSpace(transactionId)) throw new ArgumentNullException("transactionId", "Transaction id must be specified");
            if (amount <= 0) throw new ArgumentOutOfRangeException("amount", "Amount must be greater than zero.");

            var request = _requestBuilder.RefundPartialTransaction(transactionId, amount, currencyCodeType, description);
            var response = sendToPayPal(request);
            return _responseParser.RefundTransaction(response);
        }

        public IPayPalRecurringResponse CreateRecurringPaymentsProfile(RecurringProfile profile, string payPalToken)
        {
            if (profile == null) throw new ArgumentNullException("orderDetails");
            if (string.IsNullOrWhiteSpace(payPalToken)) throw new ArgumentNullException("payPalToken");

            var request = _requestBuilder.CreateRecurringPaymentsProfile(profile, payPalToken);
            return doCreateRecurringPaymentsProfile(request);
        }

        public IPayPalRecurringResponse UpdateRecurringPaymentsProfile(RecurringProfile profile, string profileId)
        {
            if (profile == null)
                throw new ArgumentNullException("orderDetails");
            if (string.IsNullOrEmpty(profileId))
                throw new ArgumentNullException("profileId");
            return this.doCreateRecurringPaymentsProfile(this._requestBuilder.UpdateRecurringPaymentsProfile(profile, profileId));
        }

        IPayPalRecurringResponse doCreateRecurringPaymentsProfile(NameValueCollection request)
        {
            var response = sendToPayPal(request);
            return _responseParser.CreateRecurringProfile(response);
        }

        public IPayPalRecurringResponse ManageRecurringPaymentsProfileStatus(string profileId, PayPalEnums.RecurringProfileAction action)
        {
            if (action == null) throw new ArgumentNullException("action");
            if (string.IsNullOrWhiteSpace(profileId)) throw new ArgumentNullException("profileId");

            var request = _requestBuilder.ManageRecurringPaymentsProfileStatus(profileId, action);
            return doManageRecurringPaymentsProfileStatus(request);
        }

        IPayPalRecurringResponse doManageRecurringPaymentsProfileStatus(NameValueCollection request)
        {
            var response = sendToPayPal(request);
            return _responseParser.CreateRecurringProfile(response);
        }

    }
}