﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Enum
/// </summary>
public enum StatusEnum
{
    [Description("Running")]
    Running = 1,
    [Description("Success")]
    Success = 2,
    [Description("Fail")]
    Fail = 3,
}

public enum ModelType
{
    [Description("Create")]
    Create = 1,
    [Description("Update")]
    Update = 2
}

public enum UserFilter
{
    [Description("Active users")]
    ActiveUsers = 1,

    [Description("Active subscription")]
    ActiveSubscriptions = 2,

    [Description("Non-active users")]
    NonActiveUsers = 3,

    [Description("blocked users")]
    blockedUsers = 4,

    [Description("Suspended users")]
    Suspend = 5
}

public enum SettingEnum
{
    [Description("Certificate")]
    Certificate = 1,
    [Description("Maintenance")]
    Maintenance = 2
}

public enum SaasuOrXero
{
    [Description("Saasu")]
    Saasu = 1,
    [Description("Xero")]
    Xero = 2
}


public enum EmailTemplates
{
    [Description("Registration Success Email")]
    RegistrationSuccessEmail = 1,
    [Description("New User Registered Email to Admin")]
    RegistrationSuccessEmailToAdmin = 2,
    [Description("Forgot Password Email")]
    ForgotPasswordEmail = 3,
    [Description("Clearjelly model not built")]
    ClearjellyModelBuiltFailed = 4,
    [Description("Clearjelly model has been built")]
    ClearjellyModelBuiltSuccess = 5,
    [Description("SendMailOn Xerp Api Exception")]
    SendMailOnApiException = 6,
    [Description("Quick Guide")]
    quickGuide = 7,
    [Description("PaymentSuccessEmail")]
    PaymentSuccessEmail = 9,
    [Description("Error Xero Email")]
    ErrorXeroEmail = 10,
    [Description("Database Error")]
    DatabaseError = 11,
    [Description("Jedox Error")]
    JedoxError = 17
}


public enum XeroErrorCodes
{
    [Description("The xero model process is finished successfully.")]
    Ok = 200,

    [Description("There is some problem while connecting to xero please contact Admin.")]
    BadRequest = 400,

    [Description("Error while connecting Xero : User is UnAuthorized or not available please contact Admin.")]
    Unauthorized = 401,

    [Description("Error while connecting Xero : Some error occurred please contact Admin.")]
    //[Description("The client SSL certificate was not valid. This indicates the Xero Entrust certificate required for partner API applications is not being supplied in the connection, or has expired")]
    Forbidden = 403,

    [Description("Error while connecting Xero : The feature you are trying is not available in xero please contact Admin.")]
    NotFound = 404,

    [Description("Error while connecting Xero : Some internal error occurred please contact Admin.")]
    InternalError = 500,

    [Description("Error from Xero : The feature you are trying is not available in xero please contact Admin.")]
    NotImplemented = 501,

    [Description("Error from Xero : The feature you are trying is not available in xero please contact Admin.")]
    NotAvailable = 503,

    [Description("Error from Xero : Token is expired or not active please contact Admin.")]
    GeneralError = 555,

}


public enum PaypalPaymentStatus
{
    canceled = 1,
    created = 2,
    completed = 3,
    incomplete = 4,
    error = 5,
    reversalerror = 6,
    processing = 7,
    pending = 8,
    failed = 9,
    declined = 10,
    canceled_reversal = 11,
    expired = 12,
    partially_refunded = 14,
    processed = 15,
    refunded = 16,
    reversed = 17,
    voided = 18,
}


public enum PaymentCycle
{
    Daily = 1,
    Weekly = 2,
    SemiMonthly = 3,
    Monthly = 4,
    Yearly = 5
}


public enum PaymentType
{
    Recurring = 1,
    Instant = 2
}

public enum PaypalTxnCode
{
    recurring_payment_failed = 1,
    recurring_payment_suspended_due_to_max_failed_payment = 2,
    recurring_payment_profile_created = 3,
    recurring_payment_profile_cancel = 4,
    recurring_payment_suspended = 5
}