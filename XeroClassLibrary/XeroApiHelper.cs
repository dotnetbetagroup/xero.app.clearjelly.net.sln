﻿using System;
using System.Configuration;
using Xero.Api.Core;
//using Xero.Api.Example.Applications.Partner;
//using Xero.Api.Example.Applications.Public;
//using Xero.Api.Example.MVC.Stores;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;

/// <summary>
/// Summary description for XeroApiHelper
/// </summary>
/// 
public class ApplicationSettings
{
    public string BaseApiUrl { get; set; }
    public Consumer Consumer { get; set; }
    public object Authenticator { get; set; }
}

public static class XeroApiHelper
{
    private static ApplicationSettings _applicationSettings;
    private static String uName;
    private static NLog.Logger _logger;
    static XeroApiHelper()
    {
        try
        {

            _logger = NLog.LogManager.GetCurrentClassLogger();
            _logger.Info(uName + " ~Execution of XeroApiHelper started.");
            // Refer to README.md for details
            //var callbackUrl = "http://localhost:55859/Actions.aspx";
            var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
          
                var callbackUrl = siteUrl + "/Home.aspx";

                // var callbackUrl = "./Actions.aspx";
                var sqlStore = new SqlAccessTokenStore();
                var requestTokenStore = new MemoryRequestTokenStore();
                //var requestTokenStore = new SqlRequestTokenStore();
                var baseApiUrl = "https://api.xero.com";
                var basePartnerApiUrl = "https://api-partner.network.xero.com";

                var signingCertificatePath = @"C:\Certs\public_privatekey.pfx";
                var clientCertificatePath = @"C:\Certs\entrust-client.p12";
                var clientCertificatePassword = "mana@2010";
                var publicAppConsumerKey = "your-public-app-consumer-key";
                var publicAppConsumerSecret = "your-public-app-consumer-secret";
                var partnerConsumerKey = "S8PNXVUYUUAQJWO1BVXD9IQ51RNE8H";
                var partnerConsumerSecret = "KL9OZP2VD4TFFFM9WF78GK539HY6CQ";

                // Public Application Settings
                var publicConsumer = new Consumer(publicAppConsumerKey, publicAppConsumerSecret);

                var publicAuthenticator = new PublicMvcAuthenticator(baseApiUrl, baseApiUrl, callbackUrl, sqlStore,
                    publicConsumer, requestTokenStore);

                var publicApplicationSettings = new ApplicationSettings
                {
                    BaseApiUrl = baseApiUrl,
                    Consumer = publicConsumer,
                    Authenticator = publicAuthenticator
                };

                // Partner Application Settings
                var partnerConsumer = new Consumer(partnerConsumerKey, partnerConsumerSecret);

                var partnerAuthenticator = new PartnerMvcAuthenticator(basePartnerApiUrl, baseApiUrl, callbackUrl,
                    sqlStore, signingCertificatePath, clientCertificatePath, clientCertificatePassword,
                    partnerConsumer, requestTokenStore);

                var partnerApplicationSettings = new ApplicationSettings
                {
                    BaseApiUrl = basePartnerApiUrl,
                    Consumer = partnerConsumer,
                    Authenticator = partnerAuthenticator
                };

                // Pick one
                // Choose what sort of application is appropriate. Comment out the above code (Partner Application Settings/Public Application Settings) that are not used.

                //_applicationSettings = publicApplicationSettings;
                _applicationSettings = partnerApplicationSettings;
            //}
            //else
            //{
            //    _logger.Error((uName ?? "") + "Site Url is not defined.");
            //}
        }
        catch (Exception ex)
        {
            _logger.Error((uName ?? "") + "Execution of  XeroApiHelper Completed." + ex.Message, ex);
        }
    }

    public static ApiUser User()
    {
        try
        {
            _logger.Info(uName + " ~Execution of XeroApiHelper User started.");
            return new ApiUser { Name = uName };
        }
        catch (Exception ex)
        {
            _logger.Error((uName ?? "") + "Execution of  XeroApiHelper User Completed." + ex.Message, ex);
            throw;
        }
    }

    public static IConsumer Consumer()
    {
        try
        {
            _logger.Info(uName + " ~Execution of XeroApiHelper Consumer started.");
            return _applicationSettings.Consumer;
        }
        catch (Exception ex)
        {
            _logger.Error((uName ?? "") + "Execution of  XeroApiHelper Consumer Completed." + ex.Message, ex);
            throw;
        }

    }

    public static IMvcAuthenticator MvcAuthenticator(String inUserName)
    {
        try
        {
            _logger.Info(uName + " ~Execution of XeroApiHelper MvcAuthenticator started.");
            uName = inUserName;
            return (IMvcAuthenticator)_applicationSettings.Authenticator;
        }

        catch (Exception ex)
        {
            _logger.Error((uName ?? "") + "Execution of  XeroApiHelper MvcAuthenticator Completed." + ex.Message, ex);
            throw;
        }

    }

    public static XeroCoreApi CoreApi()
    {
        try
        {
            _logger.Info(uName + " ~Execution of XeroApiHelper CoreApi started.");


            if (_applicationSettings.Authenticator is ICertificateAuthenticator)
            {
                return new XeroCoreApi(_applicationSettings.BaseApiUrl, _applicationSettings.Authenticator as ICertificateAuthenticator,
                    _applicationSettings.Consumer, User(), new DefaultMapper(), new DefaultMapper());
            }

            if (_applicationSettings.Authenticator is IAuthenticator)
            {
                return new XeroCoreApi(_applicationSettings.BaseApiUrl, _applicationSettings.Authenticator as IAuthenticator,
                    _applicationSettings.Consumer, User(), new DefaultMapper(), new DefaultMapper());
            }

            return null;
        }
               catch (Exception ex)
        {
            _logger.Error((uName ?? "") + "Execution of  XeroApiHelper CoreApi Completed." + ex.Message, ex);

            throw;
        }
    }
}

