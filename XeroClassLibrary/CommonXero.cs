﻿using EntityModel;
using EntityModel.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace XeroUpdateService
{
    public static class CommonXero
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();
        public static bool CheckPremiumSubscription(string email)
        {
            try
            {
                //_logger.Info(email + " ~Execution of Common CheckPremiumSubscription method started.");
                var userdetails = CommonXero.GetCurrentLoggedInUser(email);
                var subscriptionIds = GetSubscriptionIds(email);
                var isPremiumSubscription = _clearJellyContext.CompanySubscriptions.Any(x => x.CompanyId == userdetails.CompanyId && x.IsActive == true && (x.EndDate > DateTime.Now || x.EndDate == null) && subscriptionIds.Contains(x.SubscriptionTypeId));
                return isPremiumSubscription;
            }
            catch (Exception ex)
            {
                //_logger.Error(email + " ~Execution of Common CheckPremiumSubscription method completed.");
                throw;
            }
        }

        public static User GetCurrentLoggedInUser(string loggedInUser)
        {
            try
            {
                _logger.Info(loggedInUser + " ~Execution of Common SaveSubscription method started.");
                var currentUser = loggedInUser;
                var user = _clearJellyContext.Users.First(a => a.Email == currentUser && a.IsActive);
                return user;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedInUser + " ~Execution of Common SaveSubscription method completed.", ex);
                throw;
            }
        }


        public static List<int> GetSubscriptionIds(string currentUser)
        {
            try
            {

                _logger.Info(currentUser + " ~Execution of Common GetSubscriptionIds method started.");
                var businessPremium = "business user premium";
                var AdvisorPremium = "advisor premium";
                var subscriptionIds = _clearJellyContext.SubscriptionTypes.Where(x => x.TypeName.Trim().ToLower().Contains(businessPremium) || x.TypeName.Trim().ToLower().Contains(AdvisorPremium)).Select(x => x.SubscriptionTypeId).ToList();
                var newids = string.Join(" ,", subscriptionIds.ToArray()); ;
                _logger.Info(currentUser + " ~Execution of Common GetSubscriptionIds method started.ids " + newids);
                return subscriptionIds;
            }
            catch (Exception ex)
            {
                _logger.Error(currentUser + " ~Execution of Common GetSubscriptionIds method completed.", ex);
                throw;
            }
        }

        public static string GetCompanyNameByUserId(string emailId)
        {
            try
            {
                _logger.Info(emailId + "Execution of Common GetCompanyNameByUserId event started.");
                var companyName = string.Empty;
                var userDetail = _clearJellyContext.Users.FirstOrDefault(a => a.Email == emailId);
                if (userDetail != null)
                {
                    companyName = userDetail.Company.Name;
                }
                return companyName;
            }
            catch (Exception ex)
            {
                _logger.Error(emailId + "Execution of Common GetCompanyNameByUserId event completed. :", ex);
                throw;
            }
        }


        public static void SendEmail(bool successStatus, string toEmailAddress)
        {
            try
            {
                _logger.Info(toEmailAddress + " ~Execution of SendEmail method started.");
                SmtpClient mailclient = new SmtpClient();

                mailclient.ServicePoint.MaxIdleTime = 2;
                var attachmentFrom = CommonXero.GetEmailAttachmentFrom();
                var replacePair = new Dictionary<string, string>();
                var toAddress = toEmailAddress;
                MailMessage mailMessage = new MailMessage();
                if (successStatus)
                {
                    mailMessage = GetMailMessage(EmailTemplates.ClearjellyModelBuiltSuccess, replacePair, toAddress, attachmentFrom);
                }
                else
                {
                    mailMessage = GetMailMessage(EmailTemplates.ClearjellyModelBuiltFailed, replacePair, toAddress, attachmentFrom);
                }
                mailMessage.IsBodyHtml = true;

                mailclient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                _logger.Error((toEmailAddress ?? "") + " ~Execution of XeroToCJ SendEmail completed:" + ex.Message, ex);
            }
        }


        public static string GetEmailAttachmentFrom()
        {
            try
            {
                var templateAttachmentFolder = ConfigurationManager.AppSettings["EmailTemplateAttachmentFolder"];
                if (templateAttachmentFolder == null)
                {
                    throw new Exception("EmailTemplateAttachment is not defined.");
                }
                return templateAttachmentFolder;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                throw ex;
            }

        }


        public static MailMessage GetMailMessage(EmailTemplates emailTemplate, Dictionary<string, string> contentParameters, string toAddress, string takeAttachmentsFrom = "")
        {
            try
            {
                var template = _clearJellyContext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == (int)emailTemplate);
                var mailMessage = new MailMessage
                {
                    Subject = template.Subject
                };
                mailMessage.To.Add(toAddress);
                var content = template.TemplateContent;
                foreach (var pair in contentParameters)
                {
                    content = content.Replace(pair.Key, pair.Value);
                }
                mailMessage.Body = content;

                if (!string.IsNullOrWhiteSpace(takeAttachmentsFrom))
                {
                    foreach (var attachment in template.EmailAttachments)
                    {
                        var attachmentFileName = attachment.AttachmentName;
                        var attachedFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/" + takeAttachmentsFrom + "/"));
                        mailMessage.Attachments.Add(new Attachment(attachedFile + attachmentFileName));
                    }
                }
                mailMessage.IsBodyHtml = true;
                return mailMessage;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Common GetMailMessage event completed. :" + ex.Message, ex);
                throw;
            }
        }
        public static void SendMailOnDatabaseDontMatch(string emailId, string companyName, string dbName)
        {
            try
            {
                _logger.Info((emailId + " ~Execution of Common SendMailOnDatabaseDontMatch event started."));
                var replacePair = DatabaseDontMatchErrorPair(emailId, companyName, dbName);
                var adminEmail = ConfigurationManager.AppSettings["RegistrationNotificationEmail"];
                if (string.IsNullOrEmpty(adminEmail))
                {
                    throw new Exception("SendMailOnDatabaseDontMatch adminEmail value is null");
                }
                var mailMessage = GetMailMessage(EmailTemplates.DatabaseError, replacePair, adminEmail);
                mailMessage.CC.Add(GetCCEmailId());
                mailMessage.IsBodyHtml = true;
                SmtpClient mailclient = new SmtpClient();
                mailclient.ServicePoint.MaxIdleTime = 2;
                //mailclient.EnableSsl = true;
                mailclient.Send(mailMessage);
                _logger.Info((emailId + " ~Execution of Common SendMailOnDatabaseDontMatch event completed."));
            }
            catch (Exception ex)
            {
                _logger.Error(emailId + "Execution of Common SendMailOnDatabaseDontMatch event completed. :", ex);
                throw;
            }

        }

        private static Dictionary<string, string> DatabaseDontMatchErrorPair(string emailId, string companyName, string dbName)
        {
            _logger.Info(" ~Execution of Common DatabaseDontMatchErrorPair method started.");
            var replacePair = new Dictionary<string, string>();
            replacePair.Add(EmailParameters.CompanyName, companyName);
            replacePair.Add(EmailParameters.EmailId, emailId);
            replacePair.Add(EmailParameters.DbName, dbName);
            return replacePair;
        }

        public static string GetCCEmailId()
        {
            try
            {
                var CCEmail = ConfigurationManager.AppSettings["CCEmailId"];
                if (CCEmail == null)
                {
                    throw new Exception("CC email id is not defined.");
                }
                return CCEmail;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                throw ex;
            }

        }


        public static string GetDataBaseName()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetDataBaseName event started.");
                var databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                if (databaseName == null)
                {
                    throw new Exception("Database Name is not defined");
                }
                return databaseName;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetDataBaseName event completed.", ex);
                throw;
            }

        }


        public class EmailParameters
        {
            public const string stParanthasis = "{";
            public const string endParanthasis = "}";

            #region Registration Email Parameters
            public const string FirstName = stParanthasis + "FirstName" + endParanthasis;
            public const string LastName = stParanthasis + "LastName" + endParanthasis;
            public const string LoginLink = stParanthasis + "LoginLink" + endParanthasis;
            public const string ActivationLink = stParanthasis + "ActivationLink" + endParanthasis;
            public const string SafeIPLink = stParanthasis + "SafeIPLink" + endParanthasis;
            #endregion

            #region Registration Success Email to Admin
            public const string UserFullName = stParanthasis + "UserFullName" + endParanthasis;
            public const string MobileNumber = stParanthasis + "MobileNumber" + endParanthasis;
            public const string CompanyName = stParanthasis + "CompanyName" + endParanthasis;
            public const string RegistrationDate = stParanthasis + "RegistrationDate" + endParanthasis;
            public const string SubscriptionType = stParanthasis + "SubscriptionType" + endParanthasis;
            #endregion

            #region Forgot Password
            public const string ResetPasswordLink = stParanthasis + "ResetPasswordLink" + endParanthasis;
            #endregion

            #region SendMail On Api Exception
            public const string OrgName = stParanthasis + "OrgName" + endParanthasis;
            public const string StartDate = stParanthasis + "StartDate" + endParanthasis;
            public const string EndDate = stParanthasis + "EndDate" + endParanthasis;
            public const string FunctionName = stParanthasis + "FunctionName" + endParanthasis;
            #endregion

            #region SendMail On Payment Notification
            public const string PayerEmail = stParanthasis + "PayerEmail" + endParanthasis;
            public const string Amount = stParanthasis + "Amount" + endParanthasis;
            public const string PaymentDate = stParanthasis + "PaymentDate" + endParanthasis;
            public const string PayerFirstName = stParanthasis + "PayerFirstName" + endParanthasis;
            public const string PayerLastName = stParanthasis + "PayerLastName" + endParanthasis;
            public const string Subscription = stParanthasis + "Subscription" + endParanthasis;
            public const string Currency = stParanthasis + "Currency" + endParanthasis;
            public const string InvoiceNumber = stParanthasis + "InvoiceNumber" + endParanthasis;
            public const string PaypalCompanyName = stParanthasis + "PaypalCompanyName" + endParanthasis;
            public const string PayerEmailId = stParanthasis + "PayerEmailId" + endParanthasis;
            public const string RecurringId = stParanthasis + "RecurringId" + endParanthasis;
            public const string OnlineInvoiceLink = stParanthasis + "OnlineInvoiceLink" + endParanthasis;
            #endregion

            #region SendMail On datbasedon'tMatch Error

            public const string EmailId = stParanthasis + "EmailId" + endParanthasis;
            public const string DbName = stParanthasis + "DbName" + endParanthasis;

            #endregion

            #region JedoxError
            public const string UserEmailId = stParanthasis + "UserEmailId" + endParanthasis;
            public const string UserOldPassword = stParanthasis + "UserOldPassword" + endParanthasis;
            public const string UserNewPassword = stParanthasis + "UserNewPassword" + endParanthasis;
            #endregion
        }

    }
}
