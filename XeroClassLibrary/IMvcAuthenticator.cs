﻿using Xero.Api.Infrastructure.Interfaces;


public interface IMvcAuthenticator
{
    string GetRequestTokenAuthorizeUrl(string userId);
    IToken RetrieveAndStoreAccessToken(string userId, string tokenKey, string verfier, string organisationShortCode);
}
