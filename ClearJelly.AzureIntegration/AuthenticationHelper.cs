﻿using System;
using System.Threading.Tasks;
using ClearJelly.Configuration;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net.Http;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System.Collections.Generic;

namespace ClearJelly.AzureIntegration
{
    public class AuthenticationHelper
    {
       
        public static ActiveDirectoryClient GetActiveDirectoryClient()
        {
            var token = GetToken();
            Uri baseServiceUri = new Uri(ConfigSection.ServiceRootURL);
            ActiveDirectoryClient activeDirectoryClient =
                new ActiveDirectoryClient(baseServiceUri,
                    async () => token);

            return activeDirectoryClient;
        }


        public async Task CreateUserAzureAd(Entities.User user, string country, string pass)
        {
            try
            {
                var adClient = GetActiveDirectoryClient();
                var newUser = new User()
                {
                    // Required settings
                    DisplayName = user.FirstName + " " + user.LastName,
                    UserPrincipalName = user.Email,
                    PasswordProfile = new PasswordProfile()
                    {
                        Password = pass,
                        ForceChangePasswordNextLogin = false
                    },
                    MailNickname = user.FirstName + user.LastName,
                    AccountEnabled = true,
                    // Some (not all) optional settings
                    GivenName = user.FirstName,
                    Surname = user.LastName,
                    //Mobile = user.Mobile != null ? user.Mobile : string.Empty,
                    Country = country
                };
                Task.Run(() => adClient.Users.AddUserAsync(newUser).Wait());

                //var group = new Group()
                //{
                //    DisplayName = "",
                //    Manager = newUser
                //};
                //adClient.Groups.AddGroupAsync(group).Wait();
                //var foundGroups = adClient.Groups.Where(group1 => group1.DisplayName == "").ExecuteAsync().Result;
            }

            catch (Exception ex)
            {
                //_logger.Error( "Error creation User in Azure. :" + ex.Message);

            }
        }
        public static string GetToken()
        {
            try
            {
                AuthenticationContext authenticationContext = new AuthenticationContext(ConfigSection.AuthString, false);
                ClientCredential clientCred = new ClientCredential(ConfigSection.IdaClientId, ConfigSection.ClientSecretAzure);
                AuthenticationResult token = authenticationContext.AcquireToken(ConfigSection.IdaResourceUrl, clientCred);
                return token.AccessToken;


            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void test()
        {
            var client = new HttpClient();
            var uri = "https://login.microsoftonline.com/1265953a-956a-474e-b818-e38b55da0a96/oauth2/authorize";
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("resource", "https://AgilityPlanningTST.onmicrosoft.com/apapi"),
                new KeyValuePair<string, string>("client_id", "de2bc9d4-2758-4183-9fe0-9d2eb8fe6742"),
                new KeyValuePair<string, string>("client_secret", "nddHpyMtFxnUB6zvVU/U15ELshCvBP9gJdqQKjn1cKA="),
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", "timapan@AgilityPlanningTST.onmicrosoft.com"),
                new KeyValuePair<string, string>("password", "Managility@2017"),
                new KeyValuePair<string, string>("scope", "openid")
             };

            var content = new FormUrlEncodedContent(pairs);

            var response = client.PostAsync(uri, content).Result;

            string result = string.Empty;

            if (response.IsSuccessStatusCode)
            {

                result = response.Content.ReadAsStringAsync().Result;
            }
        }
    }

}

