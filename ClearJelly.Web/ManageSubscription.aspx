﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ManageSubscription.aspx.cs" Inherits="ClearJelly.Web.ManageSubscription" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Manage Subscription</h1>
            </div>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
                <asp:Literal runat="server" ID="SubscriptionExpiration" />
            </p>
            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!</strong> Subscription has been cancelled successfully.
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!</strong> Something went wrong, please try again.
            </div>

            <div class="registration-subtitle">
                <h4>Active Subscriptions:</h4>
            </div>

            <div id="activeSubscriptiondv">
            </div>

            <asp:HiddenField ID="HdnSubscriptionTypeId" runat="server" />
            <asp:HiddenField ID="HdnSubscriptionMonths" runat="server" />
            <asp:HiddenField ID="HdnGSTRate" runat="server" />
            <asp:HiddenField ID="HdnAdditionalUser" runat="server" />
            <asp:HiddenField ID="HdnQuantity" runat="server" />
            <asp:HiddenField ID="HdnNetAmount" runat="server" />
            <asp:HiddenField ID="HdnTotal" runat="server" />
            <asp:HiddenField ID="HdnGST" runat="server" />
            <div class="clearfix">
            </div>
            <br />
            <br />

            <div class="registration-subtitle">
                <h4>Subscription Plans:</h4>
            </div>
            <div id="dvSubscriptionPlan">
                <table id="tblSubscriptionPlan">
                    <thead>
                        <tr>
                            <th class="width25">Plan</th>
                            <th class="width8">Entity</th>
                            <th class="width10">Monthly</th>
                            <th class="width12">Monthly Price for 12 months contract</th>
                            <th class="width10">Additional User Price per month</th>
                            <th class="width10">No. Of Pack</th>
                            <th class="width25">Subscription Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% foreach (var subscription in _subscriptionTypes)
                            {
                        %>
                        <tr>
                            <td>
                                <%=subscription.TypeName %>
                            </td>
                            <td>
                                <%=subscription.Entities %>
                            </td>
                            <td>
                                <span class="float-right">$ 
                                    <label class="monthlyFeeLbl" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                        <%=subscription.MonthlyFee %>
                                    </label>
                                </span>
                            </td>
                            <td>
                                <span class="float-right">$ 
                                    <label class="yearlyContractPrice" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                        <%=subscription.YearlyContractFee %>
                                    </label>
                                </span>
                            </td>
                            <td>
                                <span class="float-right">$
                                     <label class="additionalUserPrice" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                         <%=subscription.AdditionalUserFee %>
                                     </label>
                                </span>
                            </td>
                            <td>
                                <input type="text" class="quantityTxt width100 form-control numeric" value="<%=subscription.Quantity %>" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                            </td>
                            <td>
                                <% if (subscription.IsCurrentSubscription)
                                    {
                                        if (subscription.IsYearlySubscription)
                                        {
                                %>
                                <span class="col-md-offset-1">
                                    <input class="monthlyRd ml5" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> Monthly</input>
                                </span>
                                <span class="col-md-offset-1">
                                    <input class="yearlyRd" type="radio" name="subscriptionRd" checked="checked" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> 12 Month contract</input>
                                </span>
                                <%
                                    }
                                    else
                                    {
                                %>
                                <span class="col-md-offset-1">
                                    <input class="monthlyRd ml5" type="radio" name="subscriptionRd" checked="checked" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> Monthly</input>
                                </span>
                                <span class="col-md-offset-1">
                                    <input class="yearlyRd" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> 12 Month contract</input>
                                </span>
                                <%
                                        }
                                    }
                                    else
                                    {
                                %>
                                <span class="col-md-offset-1">
                                    <input class="monthlyRd ml5" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> Monthly</input>
                                </span>
                                <span class="col-md-offset-1">
                                    <input class="yearlyRd" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> 12 Month contract</input>
                                </span>
                                <%
                                    } %>
                            </td>
                        </tr>
                        <%
                            } %>
                        <tr class="amountRow">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><span class="pull-right">Subscription Amount: <span class="pl8"><strong>
                                <label id="subscriptionAmtLbl"></label>
                            </strong></span></span></td>
                        </tr>
                        <%--<tr class="amountRow">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><span class="pull-right">Additional Users(<label id="addtionalUserLbl"></label>): <span class="pl8">
                                <label id="additionalUserAmtLbl"></label>
                            </span></span></td>
                        </tr>--%>
                        <tr class="amountRow">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><span class="pull-right">Gross Amount: <span class="pl8"><strong>
                                <label id="grossAmtLbl"></label>
                            </strong></span></span></td>
                        </tr>
                        <tr class="amountRow">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <span class="pull-right">Add GST(<label id="gstRateLbl"></label>): <span class="pl8">
                                    <label id="gstAmtLbl"></label>
                                </span>
                                </span>
                            </td>
                        </tr>
                        <tr class="amountRow">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><span class="pull-right">Net Amount: <span class="pl8"><strong>
                                <label id="netAmtLbl"></label>
                            </strong></span></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="width22 pull-right">
                    <div class="form-group">
                        <div class="col-md-6">
                            <asp:Button runat="server" OnClick="BuyNow_Click" Text="Buy Now" CssClass="btn btn-success btn-block" ID="btnBuy" />
                        </div>
                        <div class="col-md-6">
                            <asp:HyperLink runat="server" NavigateUrl="~/Home.aspx" CssClass="btn btn-default btn-block">
                                        Cancel
                            </asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./Scripts/XeroWebApp/ManageSubscription.js"></script>
</asp:Content>

