﻿using ClearJelly.Entities;
using ClearJelly.ViewModels.UserMappingModels;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace ClearJelly.Web
{
    public partial class UploadPayroll : System.Web.UI.Page
    {

        OleDbConnection Econ;
        SqlConnection con;
        private static NLog.Logger _logger;


        protected void Page_Load(object sender, EventArgs e)
        {
            string uName = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (String.IsNullOrEmpty(uName))
            {
                Response.Redirect("~/Account/Login.aspx", false);
            }

            if (Request.QueryString["filenotvalid"] == "false")
            {
                lblFailure.Text = "The File is not of proper format.";
                FailPanel.Visible = true;
                InfoPanel.Visible = false;
            }

            if (Request.QueryString["isSuccess"] == "true")
            {
                FailPanel.Visible = false;
                InfoPanel.Visible = true;
            }

            if (Request.QueryString["isSuccess"] == "false")
            {
                FailPanel.Visible = true;
                InfoPanel.Visible = false;
            }
        }

        public UploadPayroll()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        private string GetConnectionString()
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    var sqlconn = ConfigurationManager.ConnectionStrings["CompanyDBConnection"].ConnectionString;
                    if (HttpContext.Current.Session["Jelly_user"] != null)
                    {
                        var companyName = Common.Common.GetCompanyNameByUserId((HttpContext.Current.Session["Jelly_user"].ToString()));
                        sqlconn += "database=Xero_" + companyName + ";";
                    }
                    return sqlconn;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll GetConnectionString method completed.", ex);
                throw;
            }
        }

        private CheckHeaderAndContent InsertExcelRecords(string FilePath)
        {
            try
            {
                var uploadedData = ImportingDataFromExcel(FilePath);
                if (uploadedData.IsFileHeaderValid)
                {
                    MakeTable(uploadedData.lstUploadCustomDataViewModel);
                }
                return uploadedData;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll InsertExcelRecords method completed.", ex);
                throw;
            }
        }

        protected void writeUploadToDB(DataTable entries)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll writeJournalsToDB method started.");

                if (CheckValidDatabase())
                {
                    CheckTabelExists();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(GetConnectionString()))
                    {
                        BulkInsertUploadCustom(bulkCopy, entries);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll writeJournalsToDB:" + ex.Message, ex);
                throw;
            }
        }

        public void BulkInsertUploadCustom(SqlBulkCopy bulkCopy, DataTable entries)
        {
            _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " Execution of UploadPayroll BulkInsertUploadCustom started");
            bulkCopy.DestinationTableName = "UploadPayroll";

            foreach (DataColumn col in entries.Columns)
            {
                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            }

            try
            {
                bulkCopy.WriteToServer(entries);
            }
            catch (SqlException ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " Execution of UploadPayroll relational table:" + ex.Message, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll relational table:" + ex.Message, ex);
                throw;
            }
        }

        public bool CheckValidDatabase()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckValidDatabase method started.");
                var companyName = Common.Common.GetCompanyNameByUserId((HttpContext.Current.Session["Jelly_user"].ToString()));
                var finalCompanyName = "Xero_" + companyName;
                var connection = new SqlConnectionStringBuilder(GetConnectionString());
                string dbName = connection.InitialCatalog;
                if (finalCompanyName.ToLower() == dbName.ToLower())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckValidDatabase method completed.", ex);
                throw;
            }
        }

        public void MakeTable(List<UserPayroll> uplaodCustomData)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll MakeTable method started.");
                DataTable UploadCustomData = new DataTable("UploadCustomData");
                // Add three column objects to the table. 
                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                UploadCustomData.Columns.Add(Date);

                DataColumn PayItemType = new DataColumn();
                PayItemType.DataType = System.Type.GetType("System.String");
                PayItemType.ColumnName = "PayItemType";
                UploadCustomData.Columns.Add(PayItemType);

                DataColumn Employee = new DataColumn();
                Employee.DataType = System.Type.GetType("System.String");
                Employee.ColumnName = "Employee";
                UploadCustomData.Columns.Add(Employee);

                DataColumn PayItem = new DataColumn();
                PayItem.DataType = System.Type.GetType("System.String");
                PayItem.ColumnName = "PayItem";
                UploadCustomData.Columns.Add(PayItem);

                DataColumn EmployeeGroup = new DataColumn();
                EmployeeGroup.DataType = System.Type.GetType("System.String");
                EmployeeGroup.ColumnName = "EmployeeGroup";
                UploadCustomData.Columns.Add(EmployeeGroup);

                DataColumn Amount = new DataColumn();
                Amount.DataType = System.Type.GetType("System.String");
                Amount.ColumnName = "Amount";
                UploadCustomData.Columns.Add(Amount);

                DataColumn OrgName = new DataColumn();
                OrgName.DataType = System.Type.GetType("System.String");
                OrgName.ColumnName = "OrgName";
                UploadCustomData.Columns.Add(OrgName);

                DataColumn TrackingCode1 = new DataColumn();
                TrackingCode1.DataType = System.Type.GetType("System.String");
                TrackingCode1.ColumnName = "TrackingCode1";
                UploadCustomData.Columns.Add(TrackingCode1);

                DataColumn TrackingCode2 = new DataColumn();
                TrackingCode2.DataType = System.Type.GetType("System.String");
                TrackingCode2.ColumnName = "TrackingCode2";
                UploadCustomData.Columns.Add(TrackingCode2);
                // Create an array for DataColumn objects.
                DataColumn[] keys = new DataColumn[1];

                // Add some new rows to the collection. 

                foreach (var item in uplaodCustomData)
                {
                    DataRow row = UploadCustomData.NewRow();
                    row["Date"] = item.Date == null ? (object)DBNull.Value : item.Date;
                    row["PayItemType"] = item.PayItemType;
                    row["Employee"] = item.Employee;
                    row["PayItem"] = item.PayItem;
                    row["EmployeeGroup"] = item.EmployeeGroup;
                    row["Amount"] = item.Amount;
                    row["OrgName"] = item.OrgName;
                    row["TrackingCode1"] = item.TrackingCategory1;
                    row["TrackingCode2"] = item.TrackingCategory2;
                    UploadCustomData.Rows.Add(row);
                }

                writeUploadToDB(UploadCustomData);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll MakeTable method completed.", ex);
                throw;
            }
        }

        public CheckHeaderAndContent ImportingDataFromExcel(string fileName)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll ImportingDataFromExcel method started.");
                bool properFormat = false;

                List<UserPayroll> singlerow = new List<UserPayroll>();
                if (System.IO.File.Exists(fileName))
                {
                    using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                    {

                        var dbContext = new ClearJellyEntities();
                        string sheetName, fileExtension = Path.GetExtension(fs.Name).ToLower();
                        if (fileExtension.Equals(".xlsx"))
                        {
                            XSSFWorkbook wb = new XSSFWorkbook(fs);
                            XSSFSheet sh; XSSFRow row;
                            properFormat = CheckColumnHeaderCount(wb);
                            if (properFormat)
                            {
                                for (int i = 0; i < 1; i++)
                                {
                                    sheetName = wb.GetSheetAt(i).SheetName.ToLower();
                                    sh = (XSSFSheet)wb.GetSheet(sheetName);
                                    var currentWBRecords = sh.LastRowNum;
                                    for (int j = 1; j <= currentWBRecords; j++)
                                    {
                                        row = (XSSFRow)sh.GetRow(j);
                                        if (CheckRowIsNull(row))
                                        {

                                            ICell Date = row.Cells.Find(x => x.ColumnIndex == 0);
                                            singlerow.Add(ConvertToViewModel(row, Date));
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            HSSFWorkbook wb = new HSSFWorkbook(fs);
                            properFormat = CheckColumnHeaderCount(wb);
                            if (properFormat)
                            {
                                HSSFSheet sh; HSSFRow row;

                                for (int i = 0; i < wb.Count; i++)
                                {
                                    sheetName = wb.GetSheetAt(i).SheetName.ToLower();
                                    sh = (HSSFSheet)wb.GetSheet(sheetName);

                                    for (int j = 1; j <= sh.LastRowNum; j++)
                                    {
                                        row = (HSSFRow)sh.GetRow(j);
                                        if (CheckRowIsNull(row)) ;
                                        {
                                            ICell Date = row.Cells.Find(x => x.ColumnIndex == 0);
                                            singlerow.Add(ConvertToViewModel(row, Date));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return new CheckHeaderAndContent
                {
                    lstUploadCustomDataViewModel = singlerow,
                    IsFileHeaderValid = properFormat
                };
                //return singlerow;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll ImportingDataFromExcel method started.", ex);
                throw;
            }
        }

        private bool CheckRowIsNull(HSSFRow row)
        {
            try
            {
                var EntityName = row.Cells.Find(x => x.ColumnIndex == 0);
                var ClearJellyAccountCode = row.Cells.Find(x => x.ColumnIndex == 1);
                var SourceAccountCode = row.Cells.Find(x => x.ColumnIndex == 2);
                var SourceAccountName = row.Cells.Find(x => x.ColumnIndex == 3);
                var Scenario = row.Cells.Find(x => x.ColumnIndex == 4);
                var Date = row.Cells.Find(x => x.ColumnIndex == 5).ToString();
                var Comment = row.Cells.Find(x => x.ColumnIndex == 6);
                var Value = row.Cells.Find(x => x.ColumnIndex == 7);

                if (EntityName == null && ClearJellyAccountCode == null && SourceAccountCode == null && SourceAccountName == null && Scenario == null && Date == "" && Comment == null && Value == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckRowIsNull method started.", ex);
                throw;
            }
        }

        private bool CheckRowIsNull(XSSFRow row)
        {
            try
            {
                var EntityName = row.Cells.Find(x => x.ColumnIndex == 0);
                var ClearJellyAccountCode = row.Cells.Find(x => x.ColumnIndex == 1);
                var SourceAccountCode = row.Cells.Find(x => x.ColumnIndex == 2);
                var SourceAccountName = row.Cells.Find(x => x.ColumnIndex == 3);
                var Scenario = row.Cells.Find(x => x.ColumnIndex == 4);
                var Date = row.Cells.Find(x => x.ColumnIndex == 5) != null ? row.Cells.Find(x => x.ColumnIndex == 5).ToString() : null;
                var Comment = row.Cells.Find(x => x.ColumnIndex == 6);
                var Value = row.Cells.Find(x => x.ColumnIndex == 7);

                if (EntityName == null && ClearJellyAccountCode == null && SourceAccountCode == null && SourceAccountName == null && Scenario == null && Date == "" && Comment == null && Value == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckRowIsNull method completed.", ex);
                throw;
            }
        }

        public bool CheckColumnHeaderCount(XSSFWorkbook wb)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckColumnHeaderCount method started.");
                XSSFSheet sh; string sheetName;
                bool isExcelValid = true;
                //for (int sheetCnt = 0; sheetCnt < wb.Count; sheetCnt++)
                //{
                sheetName = wb.GetSheetAt(0).SheetName.ToLower();

                sh = (XSSFSheet)wb.GetSheet(sheetName);
                if (sh.GetRow(0) != null)
                {
                    short columnCount = sh.GetRow(0).LastCellNum;

                    string cellValue = string.Empty;
                    bool isJobNoExist = false;
                    if (sh.GetRow(0).GetCell(0) == null)
                    {
                        isExcelValid = false;
                        return false;
                    }

                    var JobCellValue = sh.GetRow(0).GetCell(0).ToString().Trim().ToLower();
                    var jobNumberHeader = GetEnumDescription((ExcelHeaderNames)0).ToLower();
                    int FixedColumnCnt = Enum.GetNames(typeof(ExcelHeaderNames)).Length;
                    if (JobCellValue == jobNumberHeader)
                    {
                        isJobNoExist = true;
                    }
                    else { FixedColumnCnt = FixedColumnCnt - 1; }

                    if (Convert.ToInt32(columnCount) < 8)
                    {
                        isExcelValid = false;
                        return isExcelValid;
                    }
                    for (int row = 0; row <= FixedColumnCnt - 1; row++)
                    {
                        //Get the cell value
                        cellValue = sh.GetRow(0).GetCell(row).ToString().Trim().ToLower();
                        if (isJobNoExist)
                        {
                            if (!(GetEnumDescription((ExcelHeaderNames)row).ToLower() == cellValue))
                            {
                                isExcelValid = false;
                                return false;
                            }
                        }
                        else
                        {
                            if (!(GetEnumDescription((ExcelHeaderNames)row + 1).ToLower() == cellValue))
                            {
                                isExcelValid = false;
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    isExcelValid = false;
                }
                //}
                return isExcelValid;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckColumnHeaderCount method completed.", ex);
                throw ex;
            }
        }

        public bool CheckColumnHeaderCount(HSSFWorkbook wb)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckColumnHeaderCount method started.");

                HSSFSheet sh; string sheetName;
                bool isExcelValid = true;
                for (int sheetCnt = 0; sheetCnt < wb.Count; sheetCnt++)
                {
                    sheetName = wb.GetSheetAt(sheetCnt).SheetName.ToLower();

                    sh = (HSSFSheet)wb.GetSheet(sheetName);
                    if (sh.GetRow(0) != null)
                    {
                        short columnCount = sh.GetRow(0).LastCellNum;

                        string cellValue = string.Empty;
                        bool isJobNoExist = false;
                        var JobCellValue = sh.GetRow(0).GetCell(0).ToString().Trim().ToLower();
                        var jobNumberHeader = GetEnumDescription((ExcelHeaderNames)0).ToLower();
                        int FixedColumnCnt = Enum.GetNames(typeof(ExcelHeaderNames)).Length;
                        if (JobCellValue == jobNumberHeader)
                        {
                            isJobNoExist = true;
                        }
                        else { FixedColumnCnt = FixedColumnCnt - 1; }


                        for (int row = 0; row <= FixedColumnCnt - 1; row++)
                        {
                            //Get the cell value
                            cellValue = sh.GetRow(0).GetCell(row).ToString().Trim().ToLower();
                            if (isJobNoExist)
                            {
                                if (!(GetEnumDescription((ExcelHeaderNames)row).ToLower() == cellValue))
                                {
                                    isExcelValid = false;
                                    return false;
                                }
                            }
                            else
                            {
                                if (!(GetEnumDescription((ExcelHeaderNames)row + 1).ToLower() == cellValue))
                                {
                                    isExcelValid = false;
                                    return false;
                                }
                            }

                        }
                    }
                    else
                    {
                        isExcelValid = false;
                    }
                }
                return isExcelValid;
            }
            catch (Exception ex)
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll CheckColumnHeaderCount method completed.", ex);
                throw;
            }
        }

        public string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public UserPayroll ConvertToViewModel(XSSFRow row, ICell Date)
        {
            try
            {
                var UserPayroll = new UserPayroll();

                ICell cell = Date;
                if (cell != null)
                {
                    if (cell.CellType == CellType.Numeric)
                    {
                        UserPayroll.Date = cell.DateCellValue;
                    }
                    else if (cell.CellType == CellType.Blank)
                    {
                        UserPayroll.Date = null;
                    }
                }
                UserPayroll.PayItemType = GetSubstringedString(row, 1) == "" ? null : GetSubstringedString(row, 1);
                UserPayroll.Employee = GetSubstringedString(row, 2) == "" ? null : GetSubstringedString(row, 2);
                UserPayroll.PayItem = GetSubstringedString(row, 3) == "" ? null : GetSubstringedString(row, 3);
                UserPayroll.EmployeeGroup = GetSubstringedString(row, 4) == "" ? null : GetSubstringedString(row, 4);


                //UploadCustomDataViewModel.Date = GetSubstringedString(row, 6, numberToMinus);
                UserPayroll.Amount = GetSubstringedString(row, 5) == "" ? null : GetSubstringedString(row, 5);
                UserPayroll.OrgName = GetSubstringedString(row, 6) == "" ? null : GetSubstringedString(row, 6);
                UserPayroll.TrackingCategory1 = GetSubstringedString(row, 7) == "" ? null : GetSubstringedString(row, 7);
                UserPayroll.TrackingCategory2 = GetSubstringedString(row, 8) == "" ? null : GetSubstringedString(row, 8);
                return UserPayroll;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll ConvertToViewModel method completed.", ex);
                throw;
            }
        }

        public UserPayroll ConvertToViewModel(HSSFRow row, ICell Date)
        {
            try
            {
                var UserPayroll = new UserPayroll();
                ICell cell = Date;
                if (cell != null)
                {
                    if (cell.CellType == CellType.Numeric)
                    {
                        UserPayroll.Date = cell.DateCellValue;
                    }
                    else if (cell.CellType == CellType.Blank)
                    {
                        UserPayroll.Date = null;
                    }
                }
                UserPayroll.PayItemType = GetSubstringedString(row, 1) == "" ? null : GetSubstringedString(row, 1);
                UserPayroll.Employee = GetSubstringedString(row, 2) == "" ? null : GetSubstringedString(row, 2);
                UserPayroll.PayItem = GetSubstringedString(row, 3) == "" ? null : GetSubstringedString(row, 3);
                UserPayroll.EmployeeGroup = GetSubstringedString(row, 4) == "" ? null : GetSubstringedString(row, 4);


                //UploadCustomDataViewModel.Date = GetSubstringedString(row, 6, numberToMinus);
                UserPayroll.Amount = GetSubstringedString(row, 5) == "" ? null : GetSubstringedString(row, 5);
                UserPayroll.OrgName = GetSubstringedString(row, 6) == "" ? null : GetSubstringedString(row, 6);
                UserPayroll.TrackingCategory1 = GetSubstringedString(row, 7) == "" ? null : GetSubstringedString(row, 7);
                UserPayroll.TrackingCategory2 = GetSubstringedString(row, 8) == "" ? null : GetSubstringedString(row, 8);
                return UserPayroll;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll ConvertToViewModel method completed.", ex);
                throw;
            }
        }

        public string GetSubstringedString(XSSFRow row, int rownum)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll GetSubstringedString method started.");
                if (row != null)
                {
                    ICell cellValue = row.Cells.Find(x => x.ColumnIndex == rownum);

                    if (cellValue != null)
                    {
                        if (cellValue.CellType == CellType.Formula)
                        {
                            try
                            {
                                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).NumericCellValue);
                            }
                            catch
                            {
                                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).StringCellValue);
                            }
                        }
                        else if (cellValue.CellType == CellType.Blank)
                        {
                            return string.Empty;
                        }
                    }
                    return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll btnUploadClick method completed.", ex);
                throw;
            }
        }

        public string GetSubstringedString(HSSFRow row, int rownum)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll GetSubstringedString method started.");
                if (row != null)
                {
                    ICell cellValue = row.Cells.Find(x => x.ColumnIndex == rownum);

                    if (cellValue != null)
                    {
                        if (cellValue.CellType == CellType.Formula)
                        {
                            try
                            {
                                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).NumericCellValue);
                            }
                            catch
                            {
                                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).StringCellValue);
                            }
                        }
                        else if (cellValue.CellType == CellType.Numeric)
                        {
                            return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).DateCellValue);
                        }
                        else if (cellValue.CellType == CellType.Blank)
                        {
                            return string.Empty;
                        }

                    }
                    return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll btnUploadClick method completed.", ex);
                throw;
            }

        }

        protected void btnUploadClick(object sender, EventArgs e)
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(ExcelFileUpload.FileName);
            String extension = Path.GetExtension(ExcelFileUpload.FileName);
            var fname = Path.Combine(Server.MapPath("Uploads"), fileNameWithoutExtension + "_" + DateTime.Now.Ticks + extension);
            try
            {
                ExcelFileUpload.SaveAs(fname);
                string strExtensionName = System.IO.Path.GetExtension(ExcelFileUpload.FileName);
                var InsertStatus = InsertExcelRecords(fname);

                if (!InsertStatus.IsFileHeaderValid)
                {
                    //lblFailure.Text = "The File is not of proper format.";
                    //FailPanel.Visible = true;
                    //InfoPanel.Visible = false;
                    Response.Redirect("~/UploadPayroll?filenotvalid=false", false);

                }
                else
                {
                    Response.Redirect("~/UploadPayroll?isSuccess=true", false);

                }
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll btnUploadClick method completed.", ex);
                Response.Redirect("~/UploadPayroll?isSuccess=false", false);

            }
            finally
            {
                if (System.IO.File.Exists(fname))
                {
                    File.Delete(fname);
                }
                //Response.Redirect(Request.Url.AbsoluteUri);
            }
        }

        public void CheckTabelExists()
        {
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;

            try
            {
                string cmdText = @"IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES 
                       WHERE TABLE_NAME='UploadPayroll') SELECT 1 ELSE SELECT 0";
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand DateCheck = new SqlCommand(cmdText, MyConnection);
                int x = Convert.ToInt32(DateCheck.ExecuteScalar());
                if (x != 1)
                {
                    SqlCommand cmd = new SqlCommand("CREATE TABLE[dbo].[UploadPayroll]( [Date] [datetime] NULL, [PayItemType][nvarchar](max) NULL, [Employee] [nvarchar](max) NULL, [PayItem] [nvarchar](max) NULL, [EmployeeGroup] [nvarchar](max) NULL, [Amount] [nvarchar](max) NULL,  [OrgName] [nvarchar](max) NULL,[TrackingCode1] [nvarchar](max) NULL,  [TrackingCode2] [nvarchar](max) NULL) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]", MyConnection);
                    cmd.ExecuteReader();

                }
            }
            catch (SqlException ex)
            {
                _logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message);
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        public enum ExcelHeaderNames
        {
            [Description("Date")]
            Date = 0,
            [Description("Pay Item Type")]
            PayItemType = 1,
            [Description("Employee")]
            Employee = 2,
            [Description("Pay Item")]
            PayItem = 3,
            [Description("Employee Group")]
            EmployeeGroup = 4,
            [Description("Amount")]
            Amount = 5,
            [Description("Org Name")]
            OrgName = 6,
            [Description("Tracking Code 1")]
            TrackingCode1 = 7,
            [Description("Tracking Code 2")]
            TrackingCode2 = 8,
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UserPayroll> GetCustomUploadList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll  GetCustomUploadList event started.");
                var currentPage = new UploadPayroll();

                var customUpload = currentPage.GetDataFromDatabasetoDataTable();
                return customUpload;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UploadPayroll  GetCustomUploadList event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static bool ClearAllData()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadPayroll  ClearAllData event started.");
                var currentPage = new UploadPayroll();
                var isSuccess = currentPage.ClearAllRecordsFromUserDb();
                return isSuccess;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UploadPayroll  ClearAllData event completed.", ex);
                throw;
            }
        }

        public bool ClearAllRecordsFromUserDb()
        {
            _logger.Info(" ~Execution of ClearAllData ClearAllRecordsFromUserDb method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @"delete from [UploadPayroll]";
            bool isSuccess = false;
            try
            {
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();
                isSuccess = true;
            }
            catch (SqlException ex)
            {
                _logger.Error(" ~An SQL error occurred during execution of UploadPayroll ClearAllRecordsFromUserDb method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of UploadPayroll ClearAllRecordsFromUserDb method. Error details:" + ex.Message, ex);
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return isSuccess;
        }


        public List<UserPayroll> GetDataFromDatabasetoDataTable()
        {
            _logger.Info(" ~Execution of UploadPayroll GetDataFromDatabasetoDataTable method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Select * from [UploadPayroll] ";
            List<UserPayroll> lstUploadCustom = new List<UserPayroll>();
            try
            {
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    UserPayroll uploadCustom = new UserPayroll();
                    uploadCustom.DisplayDate = myReader["Date"].ToString() != string.Empty ? DateTime.Parse(myReader["Date"].ToString()).ToShortDateString() : string.Empty;
                    uploadCustom.PayItemType = myReader["PayItemType"].ToString();
                    uploadCustom.Employee = myReader["Employee"].ToString();
                    uploadCustom.PayItem = myReader["PayItem"].ToString();
                    uploadCustom.EmployeeGroup = myReader["EmployeeGroup"].ToString();
                    uploadCustom.Amount = myReader["Amount"].ToString();
                    uploadCustom.OrgName = myReader["OrgName"].ToString();
                    uploadCustom.TrackingCategory1 = myReader["TrackingCode1"].ToString();
                    uploadCustom.TrackingCategory2 = myReader["TrackingCode2"].ToString();
                    lstUploadCustom.Add(uploadCustom);

                }
                return lstUploadCustom;
            }
            catch (SqlException ex)
            {
                _logger.Error(" ~An SQL error occurred during execution of clear_table_modified_after method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of XeroToCJ clear_table_modified_after method. Error details:" + ex.Message, ex);
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }
    }
}