﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Storage.Basic;
using XeroApi;
using XeroApi.Model;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using System.Web.Services;
using ClearJelly.XeroApp;
using ClearJelly.Configuration;
using ClearJelly.Services;

namespace ClearJelly.Web
{
    public partial class Default : Page
    {
        private const String UserAgent = "ClearJelly Xero Addon";
        // Demo App - use for development only
        // the callback url is localhost
        private readonly string consumerKey;
        private readonly string consumerSecret;
        private static string strConn2;
        private static string metaDBConnection;
        private static NLog.Logger logger;
        public delegate void Worker();
        private static Thread worker;
        private static string databaseName;
        // ClearJelly Pubic Test App
        // the callback url is:manadev.cloudapp.net:
        //private const String ConsumerKey = "8WBSIQ2YN5KKIEJN2HMZTLJAP3C4M9";
        //private const String ConsumerSecret = "WBUGSTFWSZVE127454OHOXWXKPNA8X";
        //test
        //private static readonly X509Certificate2 OAuthCertificate = new X509Certificate2(System.Web.HttpContext.Current.Server.MapPath("/Certs/public_privatekey.pfx"), "mana@2010");
        //private static readonly X509Certificate2 ClientSslCertificate = new X509Certificate2(System.Web.HttpContext.Current.Server.MapPath("/Certs/entrust-client.p12"), "mana@2010");
        private int xeroApiCallsCount;

        public Default()
        {

            if (System.Configuration.ConfigurationManager.AppSettings["ApplicationEnvironment"] == "Debug")
            {
                consumerKey = "S8PNXVUYUUAQJWO1BVXD9IQ51RNE8H";
                consumerSecret = "KL9OZP2VD4TFFFM9WF78GK539HY6CQ";
            }
            else
            {
                consumerKey = "S8PNXVUYUUAQJWO1BVXD9IQ51RNE8H";
                consumerSecret = "KL9OZP2VD4TFFFM9WF78GK539HY6CQ";
            }
        }

        static Default()
        {
            logger = NLog.LogManager.GetCurrentClassLogger();
            strConn2 = ConfigSection.CompanyDBConnection;
            metaDBConnection = ConfigSection.DefaultConnection;
            databaseName = Common.Common.GetDataBaseName();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Index.aspx", true);

            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of Page_Load event started."); 

            var userName = this.Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (userName == null || userName == "")
            {
                userName = User.Identity.Name;
            }
            if (Session["login"] != "valid" && userName == null)
            {
                Response.Redirect("~/Index.aspx");
            }
            if (!IsPostBack)
            {
                GetUserOrganisations();
            }

            String DbName = GetUserDBName();
            strConn2 += "database=Xero_" + DbName + ";";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of Page_Load event completed.");
        }

        private void processOrgToken()
        {
            var accessToken = new DevDefined.OAuth.Storage.Basic.AccessToken();
            var requestToken = new DevDefined.OAuth.Storage.Basic.RequestToken();
            String OrgShortCode = Session["OrgShortCode"].ToString();
            String OrgName = Session["OrgShortCode"].ToString();
            if (HasTokenInDB(OrgShortCode) == true)
            {
                AccessToken latest_token = getAccessToken(OrgShortCode);
                accessToken.TokenSecret = latest_token.TokenSecret;
                accessToken.Token = latest_token.Token;
                accessToken.SessionHandle = latest_token.SessionHandle;

                if (isTokenExpired(accessToken) == true || Session["repository"] == null)
                {
                    InMemoryTokenRepository tok = new InMemoryTokenRepository();
                    tok.SaveRequestToken(requestToken);
                    tok.SaveAccessToken(accessToken);
                    bool flag = true;
                    try
                    {
                        //new_session.RenewAccessToken();
                    }
                    catch (OAuthException ex)
                    {
                        flag = false;
                        if (Request.Params["oauth_verifier"] == null)
                        {
                            logger.Error((Session["Jelly_user"] ?? "").ToString() + " Authorisation Error:" + ex.Message.ToString());
                        }
                        if (ex.Message.ToString() == "The access token has not been authorized, or has been revoked by the user")
                        {
                            logger.Error((Session["Jelly_user"] ?? "").ToString() + " Authorisation Error:" + ex.Message.ToString());
                            invoiceCreatedLabel.Text = "The selected Xero organisation needs to be re-authorise agin! Please go to the Authorisatin page to do so.";
                        }
                    }
                }
            }
            else
            {
                invoiceCreatedLabel.Text = "The selected Xero organisation needs to be re-authorise agin! Please go to the Authorisatin page to do so.";
            }
        }

        public void cmdGetAllJournals(object sender, EventArgs e)
        {

            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetAllJournals method started.");
            var repository = (Repository)Session["repository"];

            if (repository == null)
            {
                this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                return;
            }
            GetAllJournalsWithPagination(repository);
            this.invoiceCreatedLabel.Text = "All journals have been updated!";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetAllJournals method completed.");
        }

        public void cmdgetAllInvoices(object sender, EventArgs e)
        {

            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdgetAllInvoices method started.");
            var repository = (Repository)Session["repository"];

            if (repository == null)
            {
                this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                return;
            }
            GetAllInvoicesWithPagination(repository);
            this.invoiceCreatedLabel.Text = "All invoices have been updated!";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdgetAllInvoices method completed.");
        }

        public void cmdGetAllCreditNotes(object sender, EventArgs e)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetAllCreditNotes method started.");
            var repository = (Repository)Session["repository"];

            if (repository == null)
            {
                this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                return;
            }

            GetAllCreditNotesWithPagination(repository);
            this.invoiceCreatedLabel.Text = "All credit notes have been updated!";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetAllCreditNotes method completed.");
        }

        public void cmdGetAllContacts(object sender, EventArgs e)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetAllContacts method started.");

            var repository = (Repository)Session["repository"];

            if (repository == null)
            {
                this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                return;
            }
            GetAllContacts(repository);
            this.invoiceCreatedLabel.Text = "All contacts have been updated!";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetAllContacts method completed.");
        }

        public void cmdgetAllAccounts(object sender, EventArgs e)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdgetAllAccounts method started.");
            var repository = (Repository)Session["repository"];

            if (repository == null)
            {
                this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                return;
            }
            GetAccounts(repository);
            this.invoiceCreatedLabel.Text = "All account codes have been updated!";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdgetAllAccounts method completed.");
        }

        public void cmdGetCategories(object sender, EventArgs e)
        {

            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetCategories method started.");
            var repository = (Repository)Session["repository"];

            if (repository == null)
            {
                this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                return;
            }

            getTrackingCategories(repository);
            this.invoiceCreatedLabel.Text = "All categories have been updated!";
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdGetCategories method completed.");
        }

        protected void GetAllJournalsWithPagination(Repository repository)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllJournalsWithPagination method started.");

            String selectedOrgShortCode = Session["orgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();

            List<Journal> allJournals = new List<Journal>();
            List<Journal> batchOfJournals;
            int skip = 290;
            string sqlStr = "";
            DataTable entries = new DataTable("Journals");

            DataColumn JID = new DataColumn();
            JID.DataType = System.Type.GetType("System.String");
            JID.ColumnName = "JID";
            entries.Columns.Add(JID);

            DataColumn JNumber = new DataColumn();
            JNumber.DataType = System.Type.GetType("System.String");
            JNumber.ColumnName = "JNumber";
            entries.Columns.Add(JNumber);

            DataColumn Reference = new DataColumn();
            Reference.DataType = System.Type.GetType("System.String");
            Reference.ColumnName = "Reference";
            entries.Columns.Add(Reference);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);

            DataColumn CreatedDateUTC = new DataColumn();
            CreatedDateUTC.DataType = System.Type.GetType("System.String");
            CreatedDateUTC.ColumnName = "CreatedDateUTC";
            entries.Columns.Add(CreatedDateUTC);


            DataColumn AccountName = new DataColumn();
            AccountName.DataType = System.Type.GetType("System.String");
            AccountName.ColumnName = "AccountName";
            entries.Columns.Add(AccountName);

            DataColumn AccountCode = new DataColumn();
            AccountCode.DataType = System.Type.GetType("System.String");
            AccountCode.ColumnName = "AccountCode";
            entries.Columns.Add(AccountCode);

            DataColumn AccountType = new DataColumn();
            AccountType.DataType = System.Type.GetType("System.String");
            AccountType.ColumnName = "AccountType";
            entries.Columns.Add(AccountType);

            DataColumn NetAmt = new DataColumn();
            NetAmt.DataType = System.Type.GetType("System.String");
            NetAmt.ColumnName = "NetAmt";
            entries.Columns.Add(NetAmt);

            DataColumn GrossAmt = new DataColumn();
            GrossAmt.DataType = System.Type.GetType("System.String");
            GrossAmt.ColumnName = "GrossAmt";
            entries.Columns.Add(GrossAmt);

            DataColumn ValidationErrors = new DataColumn();
            ValidationErrors.DataType = System.Type.GetType("System.String");
            ValidationErrors.ColumnName = "ValidationErrors";
            entries.Columns.Add(ValidationErrors);

            DataColumn ValidationStatus = new DataColumn();
            ValidationStatus.DataType = System.Type.GetType("System.String");
            ValidationStatus.ColumnName = "ValidationStatus";
            entries.Columns.Add(ValidationStatus);

            DataColumn Warnings = new DataColumn();
            Warnings.DataType = System.Type.GetType("System.String");
            Warnings.ColumnName = "Warnings";
            entries.Columns.Add(Warnings);

            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn TrackingCategory1 = new DataColumn();
            TrackingCategory1.DataType = System.Type.GetType("System.String");
            TrackingCategory1.ColumnName = "TrackingCategory1";
            entries.Columns.Add(TrackingCategory1);

            DataColumn TrackingCategory1_Option = new DataColumn();
            TrackingCategory1_Option.DataType = System.Type.GetType("System.String");
            TrackingCategory1_Option.ColumnName = "TrackingCategory1_Option";
            entries.Columns.Add(TrackingCategory1_Option);

            DataColumn TrackingCategory2 = new DataColumn();
            TrackingCategory2.DataType = System.Type.GetType("System.String");
            TrackingCategory2.ColumnName = "TrackingCategory2";
            entries.Columns.Add(TrackingCategory2);

            DataColumn TrackingCategory2_Option = new DataColumn();
            TrackingCategory2_Option.DataType = System.Type.GetType("System.String");
            TrackingCategory2_Option.ColumnName = "TrackingCategory2_Option";
            entries.Columns.Add(TrackingCategory2_Option);

            DataColumn JournalLines = new DataColumn();
            JournalLines.DataType = System.Type.GetType("System.String");
            JournalLines.ColumnName = "JournalLines";
            entries.Columns.Add(JournalLines);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);

            allJournals.Clear();
            DateTime start = DateTime.Now;

            long lastJournalNumber = 0;
            System.Threading.Thread.Sleep(5000);
            try
            {
                DateTime threeMonthAgo = DateTime.Now.AddMonths(-3);
                Journal jTmp = repository.Journals.Skip(skip).ToList().First();
                skip = Convert.ToInt32(jTmp.JournalNumber) - 1;
                lastJournalNumber = jTmp.JournalNumber;
                xeroApiCallsCount++;
                while ((batchOfJournals = repository.Journals.Skip(skip).ToList()).Count > 0)
                {
                    allJournals.AddRange(batchOfJournals);
                    skip += (batchOfJournals.Count);
                    lastJournalNumber = allJournals.Last().JournalNumber;
                    batchOfJournals.Clear();
                    xeroApiCallsCount++;
                    System.Threading.Thread.Sleep(1000);

                }
                xeroApiCallsCount++;
            }
            catch (XeroApi.Exceptions.ApiResponseException ex)
            {
                queue_xero_request("Journals", lastJournalNumber);
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.Message);
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.StackTrace.ToString());
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~general error reading journals:" + ex.Message);
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.StackTrace.ToString());
            }


            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Number of Journals processed:" + allJournals.Count());

            int x = 0;

            entries.Clear();
            try
            {

                if (allJournals.Any())
                {

                    foreach (Journal item in allJournals)
                    {
                        foreach (JournalLine line in item.JournalLines)
                        {
                            DataRow row = entries.NewRow();
                            row["JID"] = item.JournalID;
                            row["Reference"] = item.Reference;
                            row["JNumber"] = item.JournalNumber;
                            row["Date"] = item.JournalDate.ToString("dd/MMM/yyyy");
                            row["CreatedDateUTC"] = item.CreatedDateUTC.ToString("dd/MMM/yyyy");
                            row["AccountName"] = line.AccountName;
                            row["AccountCode"] = line.AccountCode;
                            row["AccountType"] = line.AccountType;
                            row["NetAmt"] = line.NetAmount.ToString();
                            row["GrossAmt"] = line.GrossAmount;

                            if (line.ValidationErrors.Any())
                            {
                                row["ValidationErrors"] = line.ValidationErrors.First().Message;
                            }
                            row["ValidationStatus"] = line.ValidationStatus;
                            if (line.Warnings.Any())
                            {
                                row["Warnings"] = line.Warnings.First().Message;
                            }
                            row["Description"] = line.Description;

                            if (line.TrackingCategories.Count == 1)
                            {
                                row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                row["TrackingCategory1_option"] = line.TrackingCategories[0].Option;
                            }
                            else
                            {
                                row["TrackingCategory1"] = null;
                                row["TrackingCategory1_option"] = null;
                                row["TrackingCategory2"] = null;
                                row["TrackingCategory2_Option"] = null;
                            }

                            if (line.TrackingCategories.Count == 2)
                            {
                                row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                row["TrackingCategory1_option"] = line.TrackingCategories[0].Option;
                                row["TrackingCategory2"] = line.TrackingCategories[2].Name;
                                row["TrackingCategory2_Option"] = line.TrackingCategories[2].Option;
                            }
                            row["JournalLines"] = item.JournalLines.Count;

                            row["OrgShortCode"] = selectedOrgShortCode;
                            row["OrgName"] = selectedOrgName;

                            entries.Rows.Add(row);
                            x++;
                        }
                    }
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~There are " + allJournals.Count + " journals in the general ledger, starting with " + allJournals.First().JournalNumber + " and ending with " + allJournals.Last().JournalNumber);

                    Console.WriteLine(x + "=" + entries.Rows.Count);

                }
                else
                {
                    Console.WriteLine("There are no journals in the general ledger");
                }
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error reading journal lines:" + ex.Message);
            }

            writeJournalsToDB(entries);
        }

        protected void writeJournalsToDB(DataTable entries)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "Journals";
                bulkCopy.ColumnMappings.Add("JID", "JID");
                bulkCopy.ColumnMappings.Add("Reference", "Reference");
                bulkCopy.ColumnMappings.Add("JNumber", "JNumber");
                bulkCopy.ColumnMappings.Add("Date", "Date");
                bulkCopy.ColumnMappings.Add("CreatedDateUTC", "CreatedDateUTC");
                bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
                bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
                bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
                bulkCopy.ColumnMappings.Add("NetAmt", "NetAmt");
                bulkCopy.ColumnMappings.Add("GrossAmt", "GrossAmt");
                bulkCopy.ColumnMappings.Add("ValidationErrors", "ValidationErrors");
                bulkCopy.ColumnMappings.Add("ValidationStatus", "ValidationStatus");
                bulkCopy.ColumnMappings.Add("Warnings", "Warnings");
                bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
                bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
                bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
                //bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
                bulkCopy.ColumnMappings.Add("JournalLines", "JournalLines");
                bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
                bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                try
                {
                    //Console.WriteLine("Copying to the relational table...");
                    // logger.Info((Session["Jelly_user"] ?? "").ToString() + " Copying to the relational table...");
                    // Write from the source to the destination.
                    clear_table("Journals");
                    bulkCopy.WriteToServer(entries);
                    //logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllJournalsWithPagination method completed.");
                }
                catch (SqlException ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error copying to relational table. Error details:" + ex.Message);
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error copying to relational table. Error details:" + ex.Message);
                }
            }
        }

        protected void queue_xero_request(String request_type, float last_itemID)
        {
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            //var repository = (Repository)Session["repository"];
            //String Org = repository.Organisation.Name;
            String sqlStr = @" INSERT INTO [xero_request_queue] " +
                            " ([userID],[OrgShortCode],[CallDate],[RequestStatus],[RequestType],[ItemID]) " +
                            " VALUES(  '" + Session["Jelly_user"] + "','" + Session["orgShortCode"] + "'" +
                            " ,convert(datetime,'" + DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss.fff",
                                                                                CultureInfo.InvariantCulture) + "',103)," +
                            " 'queued' , '" + request_type + "','" + last_itemID + "');";

            ; //and organisation='" + Org + "' ";
            int count = 0;
            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {

                }
            }
            catch (SqlException ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Writing journal to sql db:" + ex.Message);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Writing journal to sql db:" + ex.Message);
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }

        }
        protected void GetAccounts(Repository repository)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAccounts method started.");
            // API v2.15 Get a list of accounts that can be used when creating expense claims
            var Accounts = repository.Accounts.ToList();

            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();


            DataTable entries = new DataTable("ChartOfAccounts");


            DataColumn AccountID = new DataColumn();
            AccountID.DataType = System.Type.GetType("System.String");
            AccountID.ColumnName = "AccountID";
            entries.Columns.Add(AccountID);

            DataColumn BankAccountNumber = new DataColumn();
            BankAccountNumber.DataType = System.Type.GetType("System.String");
            BankAccountNumber.ColumnName = "BankAccountNumber";
            entries.Columns.Add(BankAccountNumber);

            DataColumn Class = new DataColumn();
            Class.DataType = System.Type.GetType("System.String");
            Class.ColumnName = "Class";
            entries.Columns.Add(Class);

            DataColumn Code = new DataColumn();
            Code.DataType = System.Type.GetType("System.String");
            Code.ColumnName = "Code";
            entries.Columns.Add(Code);

            DataColumn CurrencyCode = new DataColumn();
            CurrencyCode.DataType = System.Type.GetType("System.String");
            CurrencyCode.ColumnName = "CurrencyCode";
            entries.Columns.Add(CurrencyCode);

            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn EnablePaymentsToAccount = new DataColumn();
            EnablePaymentsToAccount.DataType = System.Type.GetType("System.String");
            EnablePaymentsToAccount.ColumnName = "EnablePaymentsToAccount";
            entries.Columns.Add(EnablePaymentsToAccount);

            DataColumn Name = new DataColumn();
            Name.DataType = System.Type.GetType("System.String");
            Name.ColumnName = "Name";
            entries.Columns.Add(Name);

            DataColumn ReportingCode = new DataColumn();
            ReportingCode.DataType = System.Type.GetType("System.String");
            ReportingCode.ColumnName = "ReportingCode";
            entries.Columns.Add(ReportingCode);

            DataColumn ReportingCodeName = new DataColumn();
            ReportingCodeName.DataType = System.Type.GetType("System.String");
            ReportingCodeName.ColumnName = "ReportingCodeName";
            entries.Columns.Add(ReportingCodeName);

            DataColumn ShowInExpenseClaims = new DataColumn();
            ShowInExpenseClaims.DataType = System.Type.GetType("System.String");
            ShowInExpenseClaims.ColumnName = "ShowInExpenseClaims";
            entries.Columns.Add(ShowInExpenseClaims);

            DataColumn Status = new DataColumn();
            Status.DataType = System.Type.GetType("System.String");
            Status.ColumnName = "Status";
            entries.Columns.Add(Status);

            DataColumn SystemAccount = new DataColumn();
            SystemAccount.DataType = System.Type.GetType("System.String");
            SystemAccount.ColumnName = "SystemAccount";
            entries.Columns.Add(SystemAccount);

            DataColumn Type = new DataColumn();
            Type.DataType = System.Type.GetType("System.String");
            Type.ColumnName = "Type";
            entries.Columns.Add(Type);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);


            foreach (var account in Accounts)
            {
                DataRow row = entries.NewRow();

                row["AccountID"] = account.AccountID;
                row["BankAccountNumber"] = account.BankAccountNumber;
                row["Class"] = account.Class;
                row["Code"] = account.Code;
                row["CurrencyCode"] = account.CurrencyCode;
                row["Description"] = account.Description;
                row["EnablePaymentsToAccount"] = account.EnablePaymentsToAccount;
                row["Name"] = account.Name;
                row["ReportingCode"] = account.ReportingCode;
                row["ReportingCodeName"] = account.ReportingCodeName;
                row["ShowInExpenseClaims"] = account.ShowInExpenseClaims;
                row["Status"] = account.Status;
                row["SystemAccount"] = account.SystemAccount;
                row["Type"] = account.Type;
                row["OrgShortCode"] = selectedOrgShortCode;
                row["OrgName"] = selectedOrgName;

                entries.Rows.Add(row);
            }
            clear_table("ChartOfAccounts");
            // insert into sql table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "ChartOfAccounts";
                try
                {
                    //Console.WriteLine("Copying to the relational table...");
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    bulkCopy.WriteToServer(entries);
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAccounts method completed.");
                }
                catch (SqlException ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An SQL error during Copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error during Copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
        }
        protected void GetAllInvoicesWithPagination(Repository repository)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllInvoicesWithPagination method started.");
            int fullUpdateFlag = 1;
            DateTime lastUpdatedDate = new DateTime();

            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();

            // Get all journals from the general ledger using the ?offset=xxx parameter
            List<Invoice> allInvoices = new List<Invoice>();
            List<Invoice> batchOfInvoices;
            List<String> IDs = new List<String>();
            int skip = 0;
            string sqlStr = "";
            DataTable entries = new DataTable("Invoices");


            DataColumn AmountCredited = new DataColumn();
            AmountCredited.DataType = System.Type.GetType("System.String");
            AmountCredited.ColumnName = "AmountCredited";
            entries.Columns.Add(AmountCredited);

            DataColumn AmountDue = new DataColumn();
            AmountDue.DataType = System.Type.GetType("System.String");
            AmountDue.ColumnName = "AmountDue";
            entries.Columns.Add(AmountDue);

            DataColumn AmountPaid = new DataColumn();
            AmountPaid.DataType = System.Type.GetType("System.String");
            AmountPaid.ColumnName = "AmountPaid";
            entries.Columns.Add(AmountPaid);

            DataColumn BrandingThemeID = new DataColumn();
            BrandingThemeID.DataType = System.Type.GetType("System.String");
            BrandingThemeID.ColumnName = "BrandingThemeID";
            entries.Columns.Add(BrandingThemeID);

            DataColumn Contact = new DataColumn();
            Contact.DataType = System.Type.GetType("System.String");
            Contact.ColumnName = "Contact";
            entries.Columns.Add(Contact);

            DataColumn CreditNotes = new DataColumn();
            CreditNotes.DataType = System.Type.GetType("System.String");
            CreditNotes.ColumnName = "CreditNotes";
            entries.Columns.Add(CreditNotes);

            DataColumn CurrencyCode = new DataColumn();
            CurrencyCode.DataType = System.Type.GetType("System.String");
            CurrencyCode.ColumnName = "CurrencyCode";
            entries.Columns.Add(CurrencyCode);

            DataColumn CurrencyRate = new DataColumn();
            CurrencyRate.DataType = System.Type.GetType("System.String");
            CurrencyRate.ColumnName = "CurrencyRate";
            entries.Columns.Add(CurrencyRate);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);

            DataColumn DueDate = new DataColumn();
            DueDate.DataType = System.Type.GetType("System.String");
            DueDate.ColumnName = "DueDate";
            entries.Columns.Add(DueDate);

            DataColumn ExpectedPaymentDate = new DataColumn();
            ExpectedPaymentDate.DataType = System.Type.GetType("System.String");
            ExpectedPaymentDate.ColumnName = "ExpectedPaymentDate";
            entries.Columns.Add(ExpectedPaymentDate);

            DataColumn ExternalLinkProviderName = new DataColumn();
            ExternalLinkProviderName.DataType = System.Type.GetType("System.String");
            ExternalLinkProviderName.ColumnName = "ExternalLinkProviderName";
            entries.Columns.Add(ExternalLinkProviderName);

            DataColumn FullyPaidOnDate = new DataColumn();
            FullyPaidOnDate.DataType = System.Type.GetType("System.String");
            FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
            entries.Columns.Add(FullyPaidOnDate);

            DataColumn HasAttachments = new DataColumn();
            HasAttachments.DataType = System.Type.GetType("System.String");
            HasAttachments.ColumnName = "HasAttachments";
            entries.Columns.Add(HasAttachments);

            DataColumn InvoiceID = new DataColumn();
            InvoiceID.DataType = System.Type.GetType("System.String");
            InvoiceID.ColumnName = "InvoiceID";
            entries.Columns.Add(InvoiceID);

            DataColumn InvoiceNumber = new DataColumn();
            InvoiceNumber.DataType = System.Type.GetType("System.String");
            InvoiceNumber.ColumnName = "InvoiceNumber";
            entries.Columns.Add(InvoiceNumber);

            DataColumn LineAmountTypes = new DataColumn();
            LineAmountTypes.DataType = System.Type.GetType("System.String");
            LineAmountTypes.ColumnName = "LineAmountTypes";
            entries.Columns.Add(LineAmountTypes);

            DataColumn LineItemsCount = new DataColumn();
            LineItemsCount.DataType = System.Type.GetType("System.String");
            LineItemsCount.ColumnName = "LineItemsCount";
            entries.Columns.Add(LineItemsCount);

            DataColumn AccountCode = new DataColumn();
            AccountCode.DataType = System.Type.GetType("System.String");
            AccountCode.ColumnName = "AccountCode";
            entries.Columns.Add(AccountCode);

            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn DiscountRate = new DataColumn();
            DiscountRate.DataType = System.Type.GetType("System.String");
            DiscountRate.ColumnName = "DiscountRate";
            entries.Columns.Add(DiscountRate);


            DataColumn ItemCode = new DataColumn();
            ItemCode.DataType = System.Type.GetType("System.String");
            ItemCode.ColumnName = "ItemCode";
            entries.Columns.Add(ItemCode);

            DataColumn LineAmount = new DataColumn();
            LineAmount.DataType = System.Type.GetType("System.String");
            LineAmount.ColumnName = "LineAmount";
            entries.Columns.Add(LineAmount);

            DataColumn Quantity = new DataColumn();
            Quantity.DataType = System.Type.GetType("System.String");
            Quantity.ColumnName = "Quantity";
            entries.Columns.Add(Quantity);

            DataColumn TaxAmount = new DataColumn();
            TaxAmount.DataType = System.Type.GetType("System.String");
            TaxAmount.ColumnName = "TaxAmount";
            entries.Columns.Add(TaxAmount);

            DataColumn TaxType = new DataColumn();
            TaxType.DataType = System.Type.GetType("System.String");
            TaxType.ColumnName = "TaxType";
            entries.Columns.Add(TaxType);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);

            DataColumn Payments = new DataColumn();
            Payments.DataType = System.Type.GetType("System.String");
            Payments.ColumnName = "Payments";
            entries.Columns.Add(Payments);

            DataColumn PlannedPaymentDate = new DataColumn();
            PlannedPaymentDate.DataType = System.Type.GetType("System.String");
            PlannedPaymentDate.ColumnName = "PlannedPaymentDate";
            entries.Columns.Add(PlannedPaymentDate);

            DataColumn Reference = new DataColumn();
            Reference.DataType = System.Type.GetType("System.String");
            Reference.ColumnName = "Reference";
            entries.Columns.Add(Reference);

            DataColumn SentToContact = new DataColumn();
            SentToContact.DataType = System.Type.GetType("System.String");
            SentToContact.ColumnName = "SentToContact";
            entries.Columns.Add(SentToContact);

            DataColumn Status = new DataColumn();
            Status.DataType = System.Type.GetType("System.String");
            Status.ColumnName = "Status";
            entries.Columns.Add(Status);

            DataColumn SubTotal = new DataColumn();
            SubTotal.DataType = System.Type.GetType("System.String");
            SubTotal.ColumnName = "SubTotal";
            entries.Columns.Add(SubTotal);

            DataColumn Total = new DataColumn();
            Total.DataType = System.Type.GetType("System.String");
            Total.ColumnName = "Total";
            entries.Columns.Add(Total);

            DataColumn TotalDiscount = new DataColumn();
            TotalDiscount.DataType = System.Type.GetType("System.String");
            TotalDiscount.ColumnName = "TotalDiscount";
            entries.Columns.Add(TotalDiscount);

            DataColumn TotalTax = new DataColumn();
            TotalTax.DataType = System.Type.GetType("System.String");
            TotalTax.ColumnName = "TotalTax";
            entries.Columns.Add(TotalTax);

            DataColumn Type = new DataColumn();
            Type.DataType = System.Type.GetType("System.String");
            Type.ColumnName = "Type";
            entries.Columns.Add(Type);

            DataColumn UpdatedDateUTC = new DataColumn();
            UpdatedDateUTC.DataType = System.Type.GetType("System.String");
            UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
            entries.Columns.Add(UpdatedDateUTC);

            DataColumn Url = new DataColumn();
            Url.DataType = System.Type.GetType("System.String");
            Url.ColumnName = "Url";
            entries.Columns.Add(Url);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);


            allInvoices.Clear();
            // repository.Invoices.OrderBy
            // Invoice jTmp = repository.Invoices.ToList().First();
            // skip = Convert.ToInt32(jTmp.InvoiceNumber) - 1;
            skip = 0;
            List<Guid> guids;
            try
            {
                DateTime threeMonthAgo = DateTime.Now.AddMonths(-3);
                //batchOfInvoices = repository.Invoices.Where(c => c.Date >= threeMonthAgo).ToList();
                batchOfInvoices = repository.Invoices.ToList();
                while (batchOfInvoices.Any())
                {
                    //Console.WriteLine("Fetched {0} Invoices from API using skip={1}", batchOfInvoices.Count, skip);
                    //Console.WriteLine(Convert.ToInt32(jTmp.InvoiceNumber)+"Test " + repository.Invoices.Skip(skip).Count());
                    System.Threading.Thread.Sleep(1000);
                    allInvoices.AddRange(batchOfInvoices);
                    batchOfInvoices = repository.Invoices.OrderBy(c => c.InvoiceID).Skip(skip).ToList();
                    skip += 100;
                    batchOfInvoices.Clear();
                }
            }
            catch (XeroApi.Exceptions.ApiResponseException ex)
            {
                queue_xero_request("Invoices", allInvoices.Count());
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.Message);
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.StackTrace.ToString());
            }
            //Console.WriteLine(allInvoices.Count());
            int x = 0;

            entries.Clear();
            if (allInvoices.Any())
            {

                foreach (Invoice item in allInvoices)
                {
                    IDs.Add(item.InvoiceID.ToString());
                    DataRow row = entries.NewRow();
                    row["AmountCredited"] = item.AmountCredited;
                    row["AmountDue"] = item.AmountDue;
                    row["AmountPaid"] = item.AmountPaid;
                    row["BrandingThemeID"] = item.BrandingThemeID;
                    row["Contact"] = item.Contact.ContactID;
                    row["CreditNotes"] = item.CreditNotes.Count.ToString();
                    row["CurrencyCode"] = item.CurrencyCode;
                    row["CurrencyRate"] = item.CurrencyRate;
                    if (item.Date == null)
                    {
                        row["Date"] = null;
                    }
                    else
                    {
                        row["Date"] = item.Date.Value.ToString("dd/MMM/yyyy");
                    }

                    if (item.DueDate == null)
                    {
                        row["DueDate"] = null;
                    }
                    else
                    {
                        row["DueDate"] = item.DueDate.Value.ToString("dd/MMM/yyyy");
                    }

                    //row["ExpectedPaymentDate"] = item.ExpectedPaymentDate;
                    row["ExternalLinkProviderName"] = item.ExternalLinkProviderName;
                    row["FullyPaidOnDate"] = item.FullyPaidOnDate;
                    row["HasAttachments"] = item.HasAttachments;
                    row["InvoiceID"] = item.InvoiceID;
                    row["InvoiceNumber"] = item.InvoiceNumber;
                    row["LineAmountTypes"] = item.LineAmountTypes;
                    row["LineItemsCount"] = item.LineItems.Count.ToString();
                    row["AccountCode"] = null;
                    row["Description"] = null;
                    row["DiscountRate"] = null;
                    row["ItemCode"] = null;
                    row["LineAmount"] = null;
                    row["Quantity"] = null;
                    row["TaxAmount"] = null;
                    row["TaxType"] = null;
                    row["UnitAmount"] = null;
                    row["Payments"] = item.Payments.Count.ToString();
                    //row["PlannedPaymentDate"] = item.PlannedPaymentDate;
                    row["Reference"] = item.Reference;
                    row["SentToContact"] = item.SentToContact;
                    row["Status"] = item.Status;
                    row["SubTotal"] = item.SubTotal;
                    row["Total"] = item.Total;
                    row["TotalDiscount"] = item.TotalDiscount;
                    row["TotalTax"] = item.TotalTax;
                    row["Type"] = item.Type;
                    row["UpdatedDateUTC"] = item.UpdatedDateUTC.Value.ToString("dd/MMM/yyyy");
                    row["Url"] = item.Url;
                    row["OrgShortCode"] = selectedOrgShortCode;
                    row["OrgName"] = selectedOrgName;


                    entries.Rows.Add(row);
                    x++;
                }
            }
            else
            {
                Console.WriteLine("There are no invoices avaailable!");
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "Invoices";

                // Number of records to be processed in one go
                //sbc.BatchSize = batchSize;

                // Add your column mappings here
                /*bulkCopy.ColumnMappings.Add("JID", "JID");
                bulkCopy.ColumnMappings.Add("Reference", "Reference");
                bulkCopy.ColumnMappings.Add("JNumber", "JNumber");
                bulkCopy.ColumnMappings.Add("Date", "Date");
                bulkCopy.ColumnMappings.Add("CreatedDateUTC", "CreatedDateUTC");
                bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
                bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
                bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
                bulkCopy.ColumnMappings.Add("NetAmt", "NetAmt");
                bulkCopy.ColumnMappings.Add("GrossAmt", "GrossAmt");
                bulkCopy.ColumnMappings.Add("ValidationErrors", "ValidationErrors");
                bulkCopy.ColumnMappings.Add("ValidationStatus", "ValidationStatus");
                bulkCopy.ColumnMappings.Add("Warnings", "Warnings");
                bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
                bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
                bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
                //bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
                bulkCopy.ColumnMappings.Add("JournalLines", "JournalLines");*/


                try
                {
                    Console.WriteLine("Copying to the relational table...");
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    // Write from the source to the destination.
                    clear_table("Invoices");
                    bulkCopy.WriteToServer(entries);
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllInvoicesWithPagination method completed.");
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                    Debug.WriteLine(ex.Message.ToString());
                }
            }

            GetAllIndvoicsByDetail(repository, IDs);

        }

        protected void GetAllIndvoicsByDetail(Repository repository, List<String> IDs)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllIndvoicsByDetail method started.");
            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();


            DataTable entries = new DataTable("InvoiceDetails");

            DataColumn InvoiceID = new DataColumn();
            InvoiceID.DataType = System.Type.GetType("System.String");
            InvoiceID.ColumnName = "InvoiceID";
            entries.Columns.Add(InvoiceID);

            DataColumn AccountCode = new DataColumn();
            AccountCode.DataType = System.Type.GetType("System.String");
            AccountCode.ColumnName = "AccountCode";
            entries.Columns.Add(AccountCode);

            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn DiscountRate = new DataColumn();
            DiscountRate.DataType = System.Type.GetType("System.String");
            DiscountRate.ColumnName = "DiscountRate";
            entries.Columns.Add(DiscountRate);

            DataColumn ItemCode = new DataColumn();
            ItemCode.DataType = System.Type.GetType("System.String");
            ItemCode.ColumnName = "ItemCode";
            entries.Columns.Add(ItemCode);

            DataColumn LineAmount = new DataColumn();
            LineAmount.DataType = System.Type.GetType("System.String");
            LineAmount.ColumnName = "LineAmount";
            entries.Columns.Add(LineAmount);

            DataColumn Quantity = new DataColumn();
            Quantity.DataType = System.Type.GetType("System.String");
            Quantity.ColumnName = "Quantity";
            entries.Columns.Add(Quantity);

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            entries.Columns.Add(ContactID);

            DataColumn type = new DataColumn();
            type.DataType = System.Type.GetType("System.String");
            type.ColumnName = "type";
            entries.Columns.Add(type);

            DataColumn status = new DataColumn();
            status.DataType = System.Type.GetType("System.String");
            status.ColumnName = "status";
            entries.Columns.Add(status);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);


            DataColumn LineAmountTypes = new DataColumn();
            LineAmountTypes.DataType = System.Type.GetType("System.String");
            LineAmountTypes.ColumnName = "LineAmountTypes";
            entries.Columns.Add(LineAmountTypes);

            DataColumn TaxAmount = new DataColumn();
            TaxAmount.DataType = System.Type.GetType("System.String");
            TaxAmount.ColumnName = "TaxAmount";
            entries.Columns.Add(TaxAmount);

            DataColumn TaxType = new DataColumn();
            TaxType.DataType = System.Type.GetType("System.String");
            TaxType.ColumnName = "TaxType";
            entries.Columns.Add(TaxType);

            DataColumn TrackingID = new DataColumn();
            TrackingID.DataType = System.Type.GetType("System.String");
            TrackingID.ColumnName = "TrackingID";
            entries.Columns.Add(TrackingID);

            DataColumn TrackingOption = new DataColumn();
            TrackingOption.DataType = System.Type.GetType("System.String");
            TrackingOption.ColumnName = "TrackingOption";
            entries.Columns.Add(TrackingOption);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);


            int x = 1;
            foreach (String id in IDs)
            {

                System.Threading.Thread.Sleep(1000);
                Double i = (x / (IDs.Count));

                //this.invoiceCreatedLabel.Text = "Total Number of Incoices:" + IDs.Count + " <br />" + x;
                Guid inv_id = new Guid(id);
                Invoice invoice = new Invoice();

                try
                {
                    invoice = repository.FindById<Invoice>(inv_id);
                }
                catch (XeroApi.Exceptions.ApiResponseException ex)
                {
                    queue_xero_request("InvoiceDetails", x);
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.Message);
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~xero api error reading journals:" + ex.StackTrace.ToString());
                }
                //Debug.Console.WriteLine(x);
                Debug.WriteLine(x);
                x++;

                foreach (LineItem item in invoice.LineItems)
                {
                    DataRow row = entries.NewRow();
                    row["InvoiceID"] = id;
                    row["AccountCode"] = item.AccountCode;
                    row["Description"] = item.Description;
                    // row["DiscountRate"] = item.DiscountRate;
                    row["ItemCode"] = item.ItemCode;
                    row["LineAmount"] = item.LineAmount;
                    row["Quantity"] = item.Quantity;
                    row["ContactID"] = invoice.Contact.ContactID;
                    row["type"] = invoice.Type;
                    row["status"] = invoice.Status;
                    row["Date"] = invoice.Date.Value.ToString("dd/MMM/yyyy"); ;
                    row["LineAmountTypes"] = invoice.LineAmountTypes;
                    row["TaxAmount"] = item.TaxAmount;
                    row["TaxType"] = item.TaxType;
                    if (item.Tracking.Count == 1)
                    {
                        row["TrackingID"] = item.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = item.Tracking[0].Option;
                    }
                    else if (item.Tracking.Count == 2)
                    {
                        row["TrackingID"] = item.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = item.Tracking[0].Option;
                    }
                    else
                    {
                        row["TrackingID"] = null;
                        row["TrackingOption"] = null;
                    }

                    row["UnitAmount"] = item.UnitAmount;
                    row["OrgShortCode"] = selectedOrgShortCode;
                    row["OrgName"] = selectedOrgName;

                    entries.Rows.Add(row);
                }
                Session["State"] = 100;

            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "InvoiceDetails";
                bulkCopy.ColumnMappings.Add("InvoiceID", "InvoiceID");
                bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
                bulkCopy.ColumnMappings.Add("Description", "Description");
                bulkCopy.ColumnMappings.Add("DiscountRate", "DiscountRate");
                bulkCopy.ColumnMappings.Add("ItemCode", "ItemCode");
                bulkCopy.ColumnMappings.Add("LineAmount", "LineAmount");
                bulkCopy.ColumnMappings.Add("Quantity", "Quantity");
                bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
                bulkCopy.ColumnMappings.Add("type", "type");
                bulkCopy.ColumnMappings.Add("status", "status");
                bulkCopy.ColumnMappings.Add("Date", "Date");
                bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
                bulkCopy.ColumnMappings.Add("TaxType", "TaxType");
                bulkCopy.ColumnMappings.Add("TaxAmount", "TaxAmount");
                bulkCopy.ColumnMappings.Add("TrackingID", "TrackingID");
                bulkCopy.ColumnMappings.Add("TrackingOption", "TrackingOption");
                bulkCopy.ColumnMappings.Add("UnitAmount", "UnitAmount");
                bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
                bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                try
                {
                    Console.WriteLine("Copying to the relational table...");
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    // Write from the source to the destination.
                    clear_table("InvoiceDetails");
                    bulkCopy.WriteToServer(entries);
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllIndvoicsByDetail method completed.");
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error during copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                    Debug.WriteLine(ex.Message.ToString());
                }
            }
        }
        protected void getTrackingCategories(Repository repository)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of getTrackingCategories method started.");
            // Get the tracking categories in this org
            IQueryable<TrackingCategory> trackingCategories = repository.TrackingCategories;

            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();


            DataTable entries = new DataTable("TrackingCategories");

            DataColumn trackingCategoryID = new DataColumn();
            trackingCategoryID.DataType = System.Type.GetType("System.String");
            trackingCategoryID.ColumnName = "trackingCategoryID";
            entries.Columns.Add(trackingCategoryID);

            DataColumn Category = new DataColumn();
            Category.DataType = System.Type.GetType("System.String");
            Category.ColumnName = "Category";
            entries.Columns.Add(Category);

            DataColumn Option = new DataColumn();
            Option.DataType = System.Type.GetType("System.String");
            Option.ColumnName = "Option";
            entries.Columns.Add(Option);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);

            foreach (var trackingCategory in trackingCategories)
            {
                Console.WriteLine(string.Format("Tracking Category: {0}", trackingCategory.Name));

                foreach (var trackingOption in trackingCategory.Options)
                {
                    DataRow row = entries.NewRow();
                    row["trackingCategoryID"] = trackingCategory.TrackingCategoryID;
                    row["Category"] = trackingCategory.Name;
                    row["Option"] = trackingOption.Name;
                    row["OrgShortCode"] = selectedOrgShortCode;
                    row["OrgName"] = selectedOrgName;

                    entries.Rows.Add(row);
                }
            }

            clear_table("TrackingCategories");
            // insert into sql table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "TrackingCategories";
                try
                {
                    Console.WriteLine("Copying to the relational table...");
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    bulkCopy.WriteToServer(entries);
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of getTrackingCategories method completed.");
                }
                catch (SqlException ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An SQL error during copying to the relational table" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error during copying to the relational table" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
            Console.WriteLine("Tracking categories have been imported!");
        }

        protected void GetAllContacts(Repository repository)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllContacts method started.");
            // Construct a linq expression to call 'GET Contacts'...
            //int invoiceCount = repository.Contacts.Count(c => c.UpdatedDateUTC >= DateTime.UtcNow.AddMonths(-1));
            int count = repository.Contacts.Count();
            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();

            //------------------------------------------------------------//
            //    Addresses: creating the relevant data table and columns
            //------------------------------------------------------------//            
            DataTable Contact_Address = new DataTable("Contact_Address");

            DataColumn AddressID = new DataColumn();
            AddressID.DataType = System.Type.GetType("System.String");
            AddressID.ColumnName = "AddressID";
            Contact_Address.Columns.Add(AddressID);

            DataColumn adressContactID = new DataColumn();
            adressContactID.DataType = System.Type.GetType("System.String");
            adressContactID.ColumnName = "adressContactID";
            Contact_Address.Columns.Add(adressContactID);

            DataColumn AddressLine1 = new DataColumn();
            AddressLine1.DataType = System.Type.GetType("System.String");
            AddressLine1.ColumnName = "AddressLine1";
            Contact_Address.Columns.Add(AddressLine1);

            DataColumn AddressLine2 = new DataColumn();
            AddressLine2.DataType = System.Type.GetType("System.String");
            AddressLine2.ColumnName = "AddressLine2";
            Contact_Address.Columns.Add(AddressLine2);

            DataColumn AddressLine3 = new DataColumn();
            AddressLine3.DataType = System.Type.GetType("System.String");
            AddressLine3.ColumnName = "AddressLine3";
            Contact_Address.Columns.Add(AddressLine3);


            DataColumn AddressLine4 = new DataColumn();
            AddressLine4.DataType = System.Type.GetType("System.String");
            AddressLine4.ColumnName = "AddressLine4";
            Contact_Address.Columns.Add(AddressLine4);

            DataColumn AttentionTo = new DataColumn();
            AttentionTo.DataType = System.Type.GetType("System.String");
            AttentionTo.ColumnName = "AttentionTo";
            Contact_Address.Columns.Add(AttentionTo);

            DataColumn City = new DataColumn();
            City.DataType = System.Type.GetType("System.String");
            City.ColumnName = "City";
            Contact_Address.Columns.Add(City);

            DataColumn Country = new DataColumn();
            Country.DataType = System.Type.GetType("System.String");
            Country.ColumnName = "Country";
            Contact_Address.Columns.Add(Country);

            DataColumn PostalCode = new DataColumn();
            PostalCode.DataType = System.Type.GetType("System.String");
            PostalCode.ColumnName = "PostalCode";
            Contact_Address.Columns.Add(PostalCode);

            DataColumn Region = new DataColumn();
            Region.DataType = System.Type.GetType("System.String");
            Region.ColumnName = "Region";
            Contact_Address.Columns.Add(Region);

            DataColumn Contact_Address_OrgShortCode = new DataColumn();
            Contact_Address_OrgShortCode.DataType = System.Type.GetType("System.String");
            Contact_Address_OrgShortCode.ColumnName = "OrgShortCode";
            Contact_Address.Columns.Add(Contact_Address_OrgShortCode);

            DataColumn Contact_Address_OrgShortCode_OrgName = new DataColumn();
            Contact_Address_OrgShortCode_OrgName.DataType = System.Type.GetType("System.String");
            Contact_Address_OrgShortCode_OrgName.ColumnName = "OrgName";
            Contact_Address.Columns.Add(Contact_Address_OrgShortCode_OrgName);


            //------------------------------------------------------------//
            //    Contacts: creating the relevant data table and columns
            //------------------------------------------------------------//
            DataTable Contacts_Table = new DataTable("Contacts_Table");

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            Contacts_Table.Columns.Add(ContactID);

            DataColumn GroupCount = new DataColumn();
            GroupCount.DataType = System.Type.GetType("System.String");
            GroupCount.ColumnName = "GroupCount";
            Contacts_Table.Columns.Add(GroupCount);

            DataColumn ContactName = new DataColumn();
            ContactName.DataType = System.Type.GetType("System.String");
            ContactName.ColumnName = "ContactName";
            Contacts_Table.Columns.Add(ContactName);

            DataColumn AccountsPayable_Outstanding = new DataColumn();
            AccountsPayable_Outstanding.DataType = System.Type.GetType("System.String");
            AccountsPayable_Outstanding.ColumnName = "AccountsPayable_Outstanding";
            Contacts_Table.Columns.Add(AccountsPayable_Outstanding);

            DataColumn AccountsPayable_Overdue = new DataColumn();
            AccountsPayable_Overdue.DataType = System.Type.GetType("System.String");
            AccountsPayable_Overdue.ColumnName = "AccountsPayable_Overdue";
            Contacts_Table.Columns.Add(AccountsPayable_Overdue);

            DataColumn AccountsReceivable_Outstanding = new DataColumn();
            AccountsReceivable_Outstanding.DataType = System.Type.GetType("System.String");
            AccountsReceivable_Outstanding.ColumnName = "AccountsReceivable_Outstanding";
            Contacts_Table.Columns.Add(AccountsReceivable_Outstanding);

            DataColumn AccountsReceivable_Overdue = new DataColumn();
            AccountsReceivable_Overdue.DataType = System.Type.GetType("System.String");
            AccountsReceivable_Overdue.ColumnName = "AccountsReceivable_Overdue";
            Contacts_Table.Columns.Add(AccountsReceivable_Overdue);

            DataColumn ContactStatus = new DataColumn();
            ContactStatus.DataType = System.Type.GetType("System.String");
            ContactStatus.ColumnName = "ContactStatus";
            Contacts_Table.Columns.Add(ContactStatus);

            DataColumn FirstName = new DataColumn();
            FirstName.DataType = System.Type.GetType("System.String");
            FirstName.ColumnName = "FirstName";
            Contacts_Table.Columns.Add(FirstName);

            DataColumn LastName = new DataColumn();
            LastName.DataType = System.Type.GetType("System.String");
            LastName.ColumnName = "LastName";
            Contacts_Table.Columns.Add(LastName);

            DataColumn IsCustomer = new DataColumn();
            IsCustomer.DataType = System.Type.GetType("System.String");
            IsCustomer.ColumnName = "IsCustomer";
            Contacts_Table.Columns.Add(IsCustomer);

            DataColumn IsSupplier = new DataColumn();
            IsSupplier.DataType = System.Type.GetType("System.String");
            IsSupplier.ColumnName = "IsSupplier";
            Contacts_Table.Columns.Add(IsSupplier);

            DataColumn PaymentTerms_Bills_Day = new DataColumn();
            PaymentTerms_Bills_Day.DataType = System.Type.GetType("System.String");
            PaymentTerms_Bills_Day.ColumnName = "PaymentTerms_Bills_Day";
            Contacts_Table.Columns.Add(PaymentTerms_Bills_Day);

            DataColumn PaymentTerms_Bills_Type = new DataColumn();
            PaymentTerms_Bills_Type.DataType = System.Type.GetType("System.String");
            PaymentTerms_Bills_Type.ColumnName = "PaymentTerms_Bills_Type";
            Contacts_Table.Columns.Add(PaymentTerms_Bills_Type);

            DataColumn PaymentTerms_Sales_Day = new DataColumn();
            PaymentTerms_Sales_Day.DataType = System.Type.GetType("System.String");
            PaymentTerms_Sales_Day.ColumnName = "PaymentTerms_Sales_Day";
            Contacts_Table.Columns.Add(PaymentTerms_Sales_Day);

            DataColumn PaymentTerms_Sales_Type = new DataColumn();
            PaymentTerms_Sales_Type.DataType = System.Type.GetType("System.String");
            PaymentTerms_Sales_Type.ColumnName = "PaymentTerms_Sales_Type";
            Contacts_Table.Columns.Add(PaymentTerms_Sales_Type);

            DataColumn Contacts_Table_OrgShortCode = new DataColumn();
            Contacts_Table_OrgShortCode.DataType = System.Type.GetType("System.String");
            Contacts_Table_OrgShortCode.ColumnName = "OrgShortCode";
            Contacts_Table.Columns.Add(Contacts_Table_OrgShortCode);

            DataColumn Contacts_Table_OrgName = new DataColumn();
            Contacts_Table_OrgName.DataType = System.Type.GetType("System.String");
            Contacts_Table_OrgName.ColumnName = "OrgName";
            Contacts_Table.Columns.Add(Contacts_Table_OrgName);


            foreach (XeroApi.Model.Contact contact in repository.Contacts)
            {

                DataRow row1 = Contacts_Table.NewRow();
                row1["ContactID"] = contact.ContactID;
                row1["GroupCount"] = contact.ContactGroups.Count.ToString();
                row1["ContactName"] = contact.Name;
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsPayable != null)
                    {
                        if (contact.Balances.AccountsPayable.Outstanding.HasValue)
                        {
                            row1["AccountsPayable_Outstanding"] = contact.Balances.AccountsPayable.Outstanding.Value.ToString();
                        }
                    }
                }
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsPayable != null)
                    {
                        if (contact.Balances.AccountsPayable.Overdue.HasValue)
                        {
                            row1["AccountsPayable_Overdue"] = contact.Balances.AccountsPayable.Overdue.Value.ToString();
                        }
                    }
                }
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsReceivable != null)
                    {
                        if (contact.Balances.AccountsReceivable.Outstanding.HasValue)
                        {
                            row1["AccountsReceivable_Outstanding"] = contact.Balances.AccountsReceivable.Outstanding.Value.ToString();
                        }
                    }
                }
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsReceivable != null)
                    {
                        if (contact.Balances.AccountsReceivable.Overdue.HasValue)
                        {
                            row1["AccountsReceivable_Overdue"] = contact.Balances.AccountsReceivable.Overdue.Value.ToString();
                        }
                    }
                }
                row1["ContactStatus"] = contact.ContactStatus;
                row1["FirstName"] = contact.FirstName;
                row1["LastName"] = contact.LastName;
                row1["IsCustomer"] = contact.IsCustomer.ToString();
                row1["IsSupplier"] = contact.IsSupplier.ToString();
                if (contact.PaymentTerms != null)
                {

                    if (contact.PaymentTerms.Bills != null)
                    {
                        row1["PaymentTerms_Bills_Day"] = contact.PaymentTerms.Bills.Day.ToString();
                        row1["PaymentTerms_Bills_Type"] = contact.PaymentTerms.Bills.Type.ToString();
                    }
                    if (contact.PaymentTerms.Sales != null)
                    {
                        row1["PaymentTerms_Sales_Day"] = contact.PaymentTerms.Sales.Day.ToString();
                        row1["PaymentTerms_Sales_Type"] = contact.PaymentTerms.Sales.Type.ToString();
                    }
                }

                row1["OrgShortCode"] = selectedOrgShortCode;
                row1["OrgName"] = selectedOrgName;

                Contacts_Table.Rows.Add(row1);
                int address_id = 0;

                foreach (Address address in contact.Addresses)
                {
                    DataRow row = Contact_Address.NewRow();
                    row["AddressID"] = address_id.ToString();
                    row["adressContactID"] = contact.ContactID.ToString();
                    row["AddressLine1"] = address.AddressLine1;
                    row["AddressLine2"] = address.AddressLine2;
                    row["AddressLine3"] = address.AddressLine3;
                    row["AddressLine4"] = address.AddressLine4;
                    row["AttentionTo"] = address.AttentionTo;
                    row["Country"] = address.Country;
                    row["PostalCode"] = address.PostalCode;
                    row["City"] = address.City;
                    row["Region"] = address.Region;
                    row["OrgShortCode"] = selectedOrgShortCode;
                    row["OrgName"] = selectedOrgName;
                    Contact_Address.Rows.Add(row);
                    address_id++;
                }
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "Contact_Address";
                bulkCopy.ColumnMappings.Add("AddressID", "AddressID");
                bulkCopy.ColumnMappings.Add("adressContactID", "ContactID");
                bulkCopy.ColumnMappings.Add("AddressLine1", "AddressLine1");
                bulkCopy.ColumnMappings.Add("AddressLine2", "AddressLine2");
                bulkCopy.ColumnMappings.Add("AddressLine3", "AddressLine3");
                bulkCopy.ColumnMappings.Add("AddressLine4", "AddressLine4");
                bulkCopy.ColumnMappings.Add("AttentionTo", "AttentionTo");
                bulkCopy.ColumnMappings.Add("Country", "Country");
                bulkCopy.ColumnMappings.Add("PostalCode", "PostalCode");
                bulkCopy.ColumnMappings.Add("City", "City");
                bulkCopy.ColumnMappings.Add("Region", "Region");
                bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
                bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                try
                {
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    clear_table("Contact_Address");
                    // Write from the source to the destination.
                    bulkCopy.WriteToServer(Contact_Address);
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "Contacts";
                bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
                bulkCopy.ColumnMappings.Add("GroupCount", "GroupCount");
                bulkCopy.ColumnMappings.Add("ContactName", "ContactName");
                bulkCopy.ColumnMappings.Add("AccountsPayable_Outstanding", "AccountsPayable_Outstanding");
                bulkCopy.ColumnMappings.Add("AccountsPayable_Overdue", "AccountsPayable_Overdue");
                bulkCopy.ColumnMappings.Add("AccountsReceivable_Outstanding", "AccountsReceivable_Outstanding");
                bulkCopy.ColumnMappings.Add("AccountsReceivable_Overdue", "AccountsReceivable_Overdue");
                bulkCopy.ColumnMappings.Add("ContactStatus", "ContactStatus");
                bulkCopy.ColumnMappings.Add("FirstName", "FirstName");
                bulkCopy.ColumnMappings.Add("LastName", "LastName");
                bulkCopy.ColumnMappings.Add("IsCustomer", "IsCustomer");
                bulkCopy.ColumnMappings.Add("IsSupplier", "IsSupplier");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Bills_Day", "PaymentTerms_Bills_Day");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Bills_Type", "PaymentTerms_Bills_Type");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Sales_Day", "PaymentTerms_Sales_Day");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Sales_Type", "PaymentTerms_Sales_Type");
                bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
                bulkCopy.ColumnMappings.Add("OrgName", "OrgName");

                try
                {
                    // Write from the source to the destination.
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    clear_table("Contacts");
                    bulkCopy.WriteToServer(Contacts_Table);
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllContacts method completed.");
                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error on Page_Load of Default.ASPX. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
        }

        protected void GetAllCreditNotesWithPagination(Repository repository)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllCreditNotesWithPagination method started.");
            int fullUpdateFlag = 1;

            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();

            DateTime lastUpdatedDate = new DateTime();
            DataTable entries = new DataTable("CreditNotes");
            List<String> IDs = new List<String>();

            DataColumn Allocations = new DataColumn();
            Allocations.DataType = System.Type.GetType("System.String");
            Allocations.ColumnName = "Allocations";
            entries.Columns.Add(Allocations);

            DataColumn AppliedAmount = new DataColumn();
            AppliedAmount.DataType = System.Type.GetType("System.String");
            AppliedAmount.ColumnName = "AppliedAmount";
            entries.Columns.Add(AppliedAmount);

            DataColumn BrandingThemeID = new DataColumn();
            BrandingThemeID.DataType = System.Type.GetType("System.String");
            BrandingThemeID.ColumnName = "BrandingThemeID";
            entries.Columns.Add(BrandingThemeID);

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            entries.Columns.Add(ContactID);

            DataColumn CreditNoteID = new DataColumn();
            CreditNoteID.DataType = System.Type.GetType("System.String");
            CreditNoteID.ColumnName = "CreditNoteID";
            entries.Columns.Add(CreditNoteID);

            DataColumn CreditNoteNumber = new DataColumn();
            CreditNoteNumber.DataType = System.Type.GetType("System.String");
            CreditNoteNumber.ColumnName = "CreditNoteNumber";
            entries.Columns.Add(CreditNoteNumber);

            DataColumn CurrencyCode = new DataColumn();
            CurrencyCode.DataType = System.Type.GetType("System.String");
            CurrencyCode.ColumnName = "CurrencyCode";
            entries.Columns.Add(CurrencyCode);

            DataColumn CurrencyRate = new DataColumn();
            CurrencyRate.DataType = System.Type.GetType("System.String");
            CurrencyRate.ColumnName = "CurrencyRate";
            entries.Columns.Add(CurrencyRate);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);

            DataColumn DueDate = new DataColumn();
            DueDate.DataType = System.Type.GetType("System.String");
            DueDate.ColumnName = "DueDate";
            entries.Columns.Add(DueDate);

            DataColumn FullyPaidOnDate = new DataColumn();
            FullyPaidOnDate.DataType = System.Type.GetType("System.String");
            FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
            entries.Columns.Add(FullyPaidOnDate);

            DataColumn LineAmountTypes = new DataColumn();
            LineAmountTypes.DataType = System.Type.GetType("System.String");
            LineAmountTypes.ColumnName = "LineAmountTypes";
            entries.Columns.Add(LineAmountTypes);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);

            DataColumn Reference = new DataColumn();
            Reference.DataType = System.Type.GetType("System.String");
            Reference.ColumnName = "Reference";
            entries.Columns.Add(Reference);

            DataColumn RemainingCredit = new DataColumn();
            RemainingCredit.DataType = System.Type.GetType("System.String");
            RemainingCredit.ColumnName = "RemainingCredit";
            entries.Columns.Add(RemainingCredit);

            DataColumn SentToContact = new DataColumn();
            SentToContact.DataType = System.Type.GetType("System.String");
            SentToContact.ColumnName = "SentToContact";
            entries.Columns.Add(SentToContact);

            DataColumn Status = new DataColumn();
            Status.DataType = System.Type.GetType("System.String");
            Status.ColumnName = "Status";
            entries.Columns.Add(Status);

            DataColumn SubTotal = new DataColumn();
            SubTotal.DataType = System.Type.GetType("System.String");
            SubTotal.ColumnName = "SubTotal";
            entries.Columns.Add(SubTotal);

            DataColumn Total = new DataColumn();
            Total.DataType = System.Type.GetType("System.String");
            Total.ColumnName = "Total";
            entries.Columns.Add(Total);

            DataColumn TotalTax = new DataColumn();
            TotalTax.DataType = System.Type.GetType("System.String");
            TotalTax.ColumnName = "TotalTax";
            entries.Columns.Add(TotalTax);

            DataColumn Type = new DataColumn();
            Type.DataType = System.Type.GetType("System.String");
            Type.ColumnName = "Type";
            entries.Columns.Add(Type);

            DataColumn UpdatedDateUTC = new DataColumn();
            UpdatedDateUTC.DataType = System.Type.GetType("System.String");
            UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
            entries.Columns.Add(UpdatedDateUTC);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);

            int x = 1;

            List<CreditNote> all_notes = new List<CreditNote>();
            if (txtSD.Text == null || txtSD.Text.Trim() == "")
            {
                all_notes = repository.CreditNotes.ToList();
            }
            else
            {
                lastUpdatedDate = DateTime.ParseExact(txtSD.Text, "dd/MMM/yyyy", CultureInfo.InvariantCulture);
                //all_notes = repository.CreditNotes.ToList();           
                //.Where(c => c.UpdatedDateUTC >= DateTime.UtcNow.AddMonths(-1));
                all_notes = repository.CreditNotes.Where(c => c.UpdatedDateUTC >= lastUpdatedDate).ToList();
                fullUpdateFlag = 0;
            }


            foreach (CreditNote credit in all_notes)// repository.CreditNotes.ToList())
            {
                DataRow row = entries.NewRow();
                IDs.Add(credit.CreditNoteID.ToString());
                row["Allocations"] = credit.Allocations.Count;
                row["AppliedAmount"] = credit.AppliedAmount;
                row["BrandingThemeID"] = credit.BrandingThemeID;
                row["ContactID"] = credit.Contact.ContactID;
                row["CreditNoteID"] = credit.CreditNoteID;
                row["CreditNoteNumber"] = credit.CreditNoteNumber;
                row["CurrencyCode"] = credit.CurrencyCode;
                row["CurrencyRate"] = credit.CurrencyRate;
                if (credit.Date == null)
                {
                    row["Date"] = null;
                }
                else
                {
                    row["Date"] = credit.Date.Value.ToString("dd/MMM/yyyy");
                }

                if (credit.DueDate == null)
                {
                    row["Date"] = null;
                }
                else
                {
                    row["DueDate"] = credit.DueDate.Value.ToString("dd/MMM/yyyy");
                }

                if (credit.FullyPaidOnDate == null)
                {
                    row["FullyPaidOnDate"] = null;
                }
                else
                {
                    row["FullyPaidOnDate"] = credit.FullyPaidOnDate.Value.ToString("dd/MMM/yyyy");
                }


                row["LineAmountTypes"] = credit.LineAmountTypes;
                row["Reference"] = credit.Reference;
                row["RemainingCredit"] = credit.RemainingCredit;
                row["SentToContact"] = credit.SentToContact;
                row["Status"] = credit.Status;
                row["SubTotal"] = credit.SubTotal;
                row["Total"] = credit.Total;
                row["TotalTax"] = credit.TotalTax;
                row["Type"] = credit.Type;

                if (credit.UpdatedDateUTC == null)
                {
                    row["UpdatedDateUTC"] = null;
                }
                else
                {
                    row["UpdatedDateUTC"] = credit.UpdatedDateUTC.Value.ToString("dd/MMM/yyyy");
                }

                row["OrgShortCode"] = selectedOrgShortCode;
                row["OrgName"] = selectedOrgName;

                entries.Rows.Add(row);
            }


            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "CreditNotes";

                // Number of records to be processed in one go
                //sbc.BatchSize = batchSize;

                // Add your column mappings here
                bulkCopy.ColumnMappings.Add("Allocations", "Allocations");
                bulkCopy.ColumnMappings.Add("AppliedAmount", "AppliedAmount");
                bulkCopy.ColumnMappings.Add("BrandingThemeID", "BrandingThemeID");
                bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
                bulkCopy.ColumnMappings.Add("CreditNoteID", "CreditNoteID");
                bulkCopy.ColumnMappings.Add("CreditNoteNumber", "CreditNoteNumber");
                bulkCopy.ColumnMappings.Add("CurrencyCode", "CurrencyCode");
                bulkCopy.ColumnMappings.Add("CurrencyRate", "CurrencyRate");
                bulkCopy.ColumnMappings.Add("Date", "Date");
                bulkCopy.ColumnMappings.Add("DueDate", "DueDate");
                bulkCopy.ColumnMappings.Add("FullyPaidOnDate", "FullyPaidOnDate");
                bulkCopy.ColumnMappings.Add("LineAmountTypes", "LineAmountTypes");
                bulkCopy.ColumnMappings.Add("UnitAmount", "UnitAmount");
                bulkCopy.ColumnMappings.Add("Reference", "Reference");
                bulkCopy.ColumnMappings.Add("RemainingCredit", "RemainingCredit");
                bulkCopy.ColumnMappings.Add("SentToContact", "SentToContact");
                bulkCopy.ColumnMappings.Add("Status", "Status");
                bulkCopy.ColumnMappings.Add("SubTotal", "SubTotal");
                bulkCopy.ColumnMappings.Add("Total", "Total");
                bulkCopy.ColumnMappings.Add("TotalTax", "TotalTax");
                bulkCopy.ColumnMappings.Add("Type", "Type");
                bulkCopy.ColumnMappings.Add("UpdatedDateUTC", "UpdatedDateUTC");
                bulkCopy.ColumnMappings.Add("OrgShortCode", "OrgShortCode");
                bulkCopy.ColumnMappings.Add("OrgName", "OrgName");
                try
                {
                    Console.WriteLine("Copying to the relational table...");
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");
                    // Write from the source to the destination.
                    if (fullUpdateFlag == 1)
                    {
                        clear_table("CreditNotes");
                    }
                    else
                    {
                        clear_table_section("CreditNotes", lastUpdatedDate);

                    }
                    bulkCopy.WriteToServer(entries);

                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
            GetCreditNotesByDetail(repository, IDs, fullUpdateFlag, lastUpdatedDate);
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetAllCreditNotesWithPagination method completed.");
        }

        protected void GetCreditNotesByDetail(Repository repository, List<String> IDs, int fullUpdateFlag, DateTime lastUpdatedDate)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetCreditNotesByDetail method started.");

            String selectedOrgShortCode = Session["OrgShortCode"].ToString();
            String selectedOrgName = Session["OrgName"].ToString();


            DataTable entries = new DataTable("CreditNotesDetails");


            DataColumn InvoiceID = new DataColumn();
            InvoiceID.DataType = System.Type.GetType("System.String");
            InvoiceID.ColumnName = "InvoiceID";
            entries.Columns.Add(InvoiceID);

            DataColumn AccountCode = new DataColumn();
            AccountCode.DataType = System.Type.GetType("System.String");
            AccountCode.ColumnName = "AccountCode";
            entries.Columns.Add(AccountCode);


            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn DiscountRate = new DataColumn();
            DiscountRate.DataType = System.Type.GetType("System.String");
            DiscountRate.ColumnName = "DiscountRate";
            entries.Columns.Add(DiscountRate);


            DataColumn ItemCode = new DataColumn();
            ItemCode.DataType = System.Type.GetType("System.String");
            ItemCode.ColumnName = "ItemCode";
            entries.Columns.Add(ItemCode);


            DataColumn LineAmount = new DataColumn();
            LineAmount.DataType = System.Type.GetType("System.String");
            LineAmount.ColumnName = "LineAmount";
            entries.Columns.Add(LineAmount);

            DataColumn Quantity = new DataColumn();
            Quantity.DataType = System.Type.GetType("System.String");
            Quantity.ColumnName = "Quantity";
            entries.Columns.Add(Quantity);

            DataColumn TaxAmount = new DataColumn();
            TaxAmount.DataType = System.Type.GetType("System.String");
            TaxAmount.ColumnName = "TaxAmount";
            entries.Columns.Add(TaxAmount);


            DataColumn TaxType = new DataColumn();
            TaxType.DataType = System.Type.GetType("System.String");
            TaxType.ColumnName = "TaxType";
            entries.Columns.Add(TaxType);

            DataColumn TrackingID = new DataColumn();
            TrackingID.DataType = System.Type.GetType("System.String");
            TrackingID.ColumnName = "TrackingID";
            entries.Columns.Add(TrackingID);

            DataColumn TrackingOption = new DataColumn();
            TrackingOption.DataType = System.Type.GetType("System.String");
            TrackingOption.ColumnName = "TrackingOption";
            entries.Columns.Add(TrackingOption);

            DataColumn Allocations = new DataColumn();
            Allocations.DataType = System.Type.GetType("System.String");
            Allocations.ColumnName = "Allocations";
            entries.Columns.Add(Allocations);

            DataColumn AppliedAmount = new DataColumn();
            AppliedAmount.DataType = System.Type.GetType("System.String");
            AppliedAmount.ColumnName = "AppliedAmount";
            entries.Columns.Add(AppliedAmount);

            DataColumn BrandingThemeID = new DataColumn();
            BrandingThemeID.DataType = System.Type.GetType("System.String");
            BrandingThemeID.ColumnName = "BrandingThemeID";
            entries.Columns.Add(BrandingThemeID);

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            entries.Columns.Add(ContactID);

            DataColumn CreditNoteID = new DataColumn();
            CreditNoteID.DataType = System.Type.GetType("System.String");
            CreditNoteID.ColumnName = "CreditNoteID";
            entries.Columns.Add(CreditNoteID);

            DataColumn CreditNoteNumber = new DataColumn();
            CreditNoteNumber.DataType = System.Type.GetType("System.String");
            CreditNoteNumber.ColumnName = "CreditNoteNumber";
            entries.Columns.Add(CreditNoteNumber);

            DataColumn CurrencyCode = new DataColumn();
            CurrencyCode.DataType = System.Type.GetType("System.String");
            CurrencyCode.ColumnName = "CurrencyCode";
            entries.Columns.Add(CurrencyCode);

            DataColumn CurrencyRate = new DataColumn();
            CurrencyRate.DataType = System.Type.GetType("System.String");
            CurrencyRate.ColumnName = "CurrencyRate";
            entries.Columns.Add(CurrencyRate);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);

            DataColumn DueDate = new DataColumn();
            DueDate.DataType = System.Type.GetType("System.String");
            DueDate.ColumnName = "DueDate";
            entries.Columns.Add(DueDate);

            DataColumn FullyPaidOnDate = new DataColumn();
            FullyPaidOnDate.DataType = System.Type.GetType("System.String");
            FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
            entries.Columns.Add(FullyPaidOnDate);

            DataColumn LineAmountTypes = new DataColumn();
            LineAmountTypes.DataType = System.Type.GetType("System.String");
            LineAmountTypes.ColumnName = "LineAmountTypes";
            entries.Columns.Add(LineAmountTypes);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);

            DataColumn Reference = new DataColumn();
            Reference.DataType = System.Type.GetType("System.String");
            Reference.ColumnName = "Reference";
            entries.Columns.Add(Reference);

            DataColumn RemainingCredit = new DataColumn();
            RemainingCredit.DataType = System.Type.GetType("System.String");
            RemainingCredit.ColumnName = "RemainingCredit";
            entries.Columns.Add(RemainingCredit);

            DataColumn SentToContact = new DataColumn();
            SentToContact.DataType = System.Type.GetType("System.String");
            SentToContact.ColumnName = "SentToContact";
            entries.Columns.Add(SentToContact);

            DataColumn Status = new DataColumn();
            Status.DataType = System.Type.GetType("System.String");
            Status.ColumnName = "Status";
            entries.Columns.Add(Status);

            DataColumn SubTotal = new DataColumn();
            SubTotal.DataType = System.Type.GetType("System.String");
            SubTotal.ColumnName = "SubTotal";
            entries.Columns.Add(SubTotal);

            DataColumn Total = new DataColumn();
            Total.DataType = System.Type.GetType("System.String");
            Total.ColumnName = "Total";
            entries.Columns.Add(Total);

            DataColumn TotalTax = new DataColumn();
            TotalTax.DataType = System.Type.GetType("System.String");
            TotalTax.ColumnName = "TotalTax";
            entries.Columns.Add(TotalTax);

            DataColumn Type = new DataColumn();
            Type.DataType = System.Type.GetType("System.String");
            Type.ColumnName = "Type";
            entries.Columns.Add(Type);

            DataColumn UpdatedDateUTC = new DataColumn();
            UpdatedDateUTC.DataType = System.Type.GetType("System.String");
            UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
            entries.Columns.Add(UpdatedDateUTC);

            DataColumn OrgShortCode = new DataColumn();
            OrgShortCode.DataType = System.Type.GetType("System.String");
            OrgShortCode.ColumnName = "OrgShortCode";
            entries.Columns.Add(OrgShortCode);

            DataColumn OrgName = new DataColumn();
            OrgName.DataType = System.Type.GetType("System.String");
            OrgName.ColumnName = "OrgName";
            entries.Columns.Add(OrgName);

            int x = 1;
            foreach (String id in IDs)
            {
                System.Threading.Thread.Sleep(1000);

                Guid note_id = new Guid(id);
                CreditNote note = repository.FindById<CreditNote>(note_id);
                foreach (LineItem line in note.LineItems)
                {
                    DataRow row = entries.NewRow();

                    row["InvoiceID"] = null;
                    row["AccountCode"] = line.AccountCode;
                    row["Description"] = line.Description;
                    // row["DiscountRate"] = line.DiscountRate;
                    row["ItemCode"] = line.ItemCode;
                    row["LineAmount"] = line.LineAmount;
                    row["Quantity"] = line.Quantity;
                    row["TaxAmount"] = line.TaxAmount;
                    row["TaxType"] = line.TaxType;
                    if (line.Tracking.Count == 1)
                    {
                        row["TrackingID"] = line.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = line.Tracking[0].Option;
                    }
                    else if (line.Tracking.Count == 2)
                    {
                        row["TrackingID"] = line.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = line.Tracking[0].Option;
                    }
                    else
                    {
                        row["TrackingID"] = null;
                        row["TrackingOption"] = null;
                    }
                    row["Allocations"] = note.Allocations.Count;
                    row["AppliedAmount"] = note.AppliedAmount;
                    row["BrandingThemeID"] = note.BrandingThemeID;
                    row["ContactID"] = note.Contact.ContactID;
                    row["CreditNoteID"] = note.CreditNoteID;
                    row["CreditNoteNumber"] = note.CreditNoteNumber;
                    row["CurrencyCode"] = note.CurrencyCode;
                    row["CurrencyRate"] = note.CurrencyRate;
                    if (note.Date == null)
                    {
                        row["Date"] = null;
                    }
                    else
                    {
                        row["Date"] = note.Date.Value.ToString("dd/MMM/yyyy");
                    }

                    if (note.DueDate == null)
                    {
                        row["DueDate"] = null;
                    }
                    else
                    {
                        row["DueDate"] = note.DueDate.Value.ToString("dd/MMM/yyyy");
                    }
                    if (note.FullyPaidOnDate == null)
                    {
                        row["FullyPaidOnDate"] = null;
                    }
                    else
                    {
                        row["FullyPaidOnDate"] = note.FullyPaidOnDate.Value.ToString("dd/MMM/yyyy");
                    }

                    row["LineAmountTypes"] = note.LineAmountTypes;
                    row["Reference"] = note.Reference;
                    row["RemainingCredit"] = note.RemainingCredit;
                    row["SentToContact"] = note.SentToContact;
                    row["Status"] = note.Status;
                    row["SubTotal"] = note.SubTotal;
                    row["Total"] = note.Total;
                    row["TotalTax"] = note.TotalTax;
                    row["Type"] = note.Type;
                    if (note.UpdatedDateUTC == null)
                    {
                        row["UpdatedDateUTC"] = null;
                    }
                    else
                    {
                        row["UpdatedDateUTC"] = note.UpdatedDateUTC.Value.ToString("dd/MMM/yyyy");
                    }

                    row["OrgShortCode"] = selectedOrgShortCode;
                    row["OrgName"] = selectedOrgName;


                    entries.Rows.Add(row);
                }
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "CreditNotesDetails";
                try
                {
                    Console.WriteLine("Copying to the relational table...");
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Copying to the relational table...");

                    // Write from the source to the destination.
                    if (fullUpdateFlag == 1)
                    {
                        clear_table("CreditNotesDetails");
                    }
                    else
                    {
                        clear_table_section("CreditNotesDetails", lastUpdatedDate);
                    }
                    bulkCopy.WriteToServer(entries);
                    logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetCreditNotesByDetail method completed.");

                }
                catch (Exception ex)
                {
                    logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during copying to the relational table. Error details:" + ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void clear_table(String teble_name)
        {
            //logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Delete FROM  " + teble_name + " Where OrgShortCode='" + Session["orgShortCode"] + "'";
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {

                }
                //logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method completed.");
            }
            catch (SqlException ex)
            {
                //logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table method. Error details:" + ex.Message);            
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                //logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An error occurred during execution of clear_table method. Error details:" + ex.Message);
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        private void clear_table_section(String teble_name, DateTime lastUpdatedDate)
        {
            //logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Delete FROM " + teble_name + "  WHERE UpdatedDateUTC >= convert(date,'" + lastUpdatedDate + "',103) WHERE OrgShortCode='" + Session["orgShortCode"] + "'";
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {

                }
                //logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method completed.");
            }
            catch (SqlException ex)
            {
                //logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                //logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An error occurred during execution of clear_table_section method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        protected void cmdUpdateCubes(object sender, EventArgs e)
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdUpdateCubes method started.");
            string username = Session["Jelly_user"].ToString();

            JedoxEtl jedoxManager = new JedoxEtl();

            //String to_execute = @"" +
            //                  " -j default -c username="+username;
            //System.Diagnostics.Process.Start(to_execute);
            /*  string str_Path = Server.MapPath(".\\etlclient\\etlclient.bat -s localhost -p ClearJellySystemOps") ;
              ProcessStartInfo processInfo = new ProcessStartInfo(str_Path);
              processInfo.UseShellExecute = false;
              Process batchProcess = new Process();
              batchProcess.StartInfo = processInfo;
              batchProcess.Start();
                  //  http://localhost:59680/etlclient/etlclient.bat*/
            //jETL.ETLServerPortTypeChannel x = jETL.ETLServerPortTypeChannel();
            //x.Open();
            //x.getNames(new jETL.getNamesRequest() );
            //var JedoxETL = new JedoxEtl();
            //JedoxEtl.ServerStatus x = new JedoxETL.ServerStatus();
            //JedoxETL.ETLServer s = new JedoxETL.ETLServer();
            //JedoxETL.Variable username_variable = new JedoxETL.Variable();
            //username_variable.name = "username";
            //username_variable.value = username;

            //var status = s.execute("ClearJellySystemOps.jobs.default", new JedoxETL.Variable[] { username_variable });
            Dictionary<string, string> ClearJellySystemOpsVariables = new Dictionary<string, string>();
            ClearJellySystemOpsVariables.Add("username", username);

            var jobId = jedoxManager.jedoxExecuteSystemOpsJob(ClearJellySystemOpsVariables, "ClearJellySystemOps.jobs.default");

            while (jedoxManager.jedoxExecuteStatus(jobId) == "Running")
            {
                System.Threading.Thread.Sleep(1000);
            }

            Dictionary<string, string> XeroVariables = new Dictionary<string, string>();
            XeroVariables.Add("Database", username);

            var jobId2 = jedoxManager.jedoxExecuteJob(XeroVariables, "Xero.jobs.default");

            while (jedoxManager.jedoxExecuteStatus(jobId2) == "Running")
            {
                System.Threading.Thread.Sleep(1000);
            }
            Dictionary<string, string> FolderGenerationVariables = new Dictionary<string, string>();
            FolderGenerationVariables.Add("UserGroup", username);

            var jobId3 = jedoxManager.jedoxExecuteJob(FolderGenerationVariables, "FolderGeneration.jobs.default");

            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdUpdateCubes method completed.");
        }

        private void createJedoxModel()
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of createJedoxModel method started.");
            string username = Session["Jelly_user"].ToString();

            JedoxEtl jedoxManager = new JedoxEtl();

            Dictionary<string, string> ClearJellySystemOpsVariables = new Dictionary<string, string>();
            ClearJellySystemOpsVariables.Add("username", username);
            ClearJellySystemOpsVariables.Add("Jedox_DB", Regex.Replace(Session["orgName"].ToString(), @"[^0-9a-zA-Z]+", ""));

            Dictionary<string, string> XeroJobsVariables = new Dictionary<string, string>();
            XeroJobsVariables.Add("database", Regex.Replace(Session["orgName"].ToString(), @"[^0-9a-zA-Z]+", ""));
            XeroJobsVariables.Add("OrgName", Session["orgName"].ToString());
            XeroJobsVariables.Add("Jedox_DB", Regex.Replace(Session["orgName"].ToString(), @"[^0-9a-zA-Z]+", ""));
            XeroJobsVariables.Add("UpdateFlag", "update");

            Dictionary<string, string> FolderGenerationVariables = new Dictionary<string, string>();
            FolderGenerationVariables.Add("CompanyName", Regex.Replace(Session["orgName"].ToString(), @"[^0-9a-zA-Z]+", ""));
            FolderGenerationVariables.Add("UserGroup", Regex.Replace(Session["orgName"].ToString(), @"[^0-9a-zA-Z]+", "") + "_group");

            var jobId = jedoxManager.jedoxExecuteSystemOpsJob(ClearJellySystemOpsVariables, "ClearJellySystemOps.jobs.default");

            while (jedoxManager.jedoxExecuteStatus(jobId) == "Running")
            {
                System.Threading.Thread.Sleep(1000);
            }

            XeroJobsVariables["database"] = GetUserDBName();
            var jobId2 = jedoxManager.jedoxExecuteJob(XeroJobsVariables, "Xero.jobs.default");

            while (jedoxManager.jedoxExecuteStatus(jobId) == "Running")
            {
                System.Threading.Thread.Sleep(1000);
            }

            var jobId3 = jedoxManager.jedoxExecuteJob(FolderGenerationVariables, "FolderGeneration.jobs.default");


            while (jedoxManager.jedoxExecuteStatus(jobId3) == "Running")
            {
                System.Threading.Thread.Sleep(1000);
            }




            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of createJedoxModel method completed.");
        }

        protected void cmdAutomatic(object sender, EventArgs e)
        {
            //Response.Redirect("./AutomaticUpdate.aspx");
            if (CompanyListDropDown.SelectedIndex == 0)
            {
                invoiceCreatedLabel.Text = "Please select an organisation form the drop down.";
            }
            else
            {
                logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdAutomatic method started.");

                processOrgToken();
                xeroApiCallsCount = getApiCallCount();

                invoiceCreatedLabel.Text = "An email will be sent to you when the process is completed.";

                Init(Work);
                //Work();
                logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdAutomatic method completed.");
            }


        }
        public void Work()
        {
            var repository = (Repository)Session["repository"];
            var successStatus = false;
            try
            {
                logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of Work method started.");
                //retrieve the repository from the session state

                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker_Repository.An error occurred during execution of Work method. Error details:" + ex.Message);
                successStatus = false;

            }
            try
            {
                GetAllInvoicesWithPagination(repository);
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker general all invoices.An error occurred during execution of Work method. Error details:" + ex.Message + "|StackTrace:" + ex.StackTrace.ToString());
                successStatus = false;
            }
            try
            {
                GetAllContacts(repository);
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker general contacts.An error occurred during execution of Work method. Error details:" + ex.Message + "|StackTrace:" + ex.StackTrace.ToString());
                successStatus = false;
            }

            try
            {
                GetAllCreditNotesWithPagination(repository);
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker general credit notes.An error occurred during execution of Work method. Error details:" + ex.Message + "|StackTrace:" + ex.StackTrace.ToString());
                successStatus = false;
            }

            try
            {
                GetAccounts(repository);
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker general Accounts.An error occurred during execution of Work method. Error details:" + ex.Message + "|StackTrace:" + ex.StackTrace.ToString());
                successStatus = false;
            }
            try
            {
                getTrackingCategories(repository);
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker general tracking categories.An error occurred during execution of Work method. Error details:" + ex.Message + "|StackTrace:" + ex.StackTrace.ToString());
                successStatus = false;
            }
            try
            {
                GetAllJournalsWithPagination(repository);
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker general get journals.An error occurred during execution of Work method. Error details:" + ex.Message + "|StackTrace:" + ex.StackTrace.ToString());
                successStatus = false;
            }

            try
            {
                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly User");
                }

                bool checkPremium = Common.Common.CheckPremiumSubscription(Session["Jelly_user"].ToString());
                successStatus = true;
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~Worker_Jedox.An error occurred during execution of Work method. Error details:" + ex.Message);
                successStatus = false;

            }
            SendEmail(successStatus);
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of Work method completed.");

        }
        public new static void Init(Worker work)
        {
            worker = new Thread(new ThreadStart(work));
            worker.Start();
        }
        [WebMethod]
        public static int GetJournalLineCount()
        {

            int journalLineCount = 0;

            var repository = (Repository)HttpContext.Current.Session["repository"];
            if (repository != null)
            {
                var batchOfJournals = new List<Journal>();
                int skip = 290;
                Journal jTmp = repository.Journals.Skip(skip).First();
                skip = Convert.ToInt32(jTmp.JournalNumber) - 1;
                while ((batchOfJournals = repository.Journals.Skip(skip).ToList()).Count > 0)
                {
                    journalLineCount += batchOfJournals.Sum(x => x.JournalLines.Count);
                    skip += (batchOfJournals.Count);
                }
            }
            logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of GetJournalLineCount method completed.");
            return journalLineCount;
        }
        private void SendEmail(bool successStatus)
        {
            try
            {
                logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of SendEmail method started.");
                SmtpMailSendingService.SendEmail(successStatus, GetEmail());
            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during execution of SendEmail method. Error details:" + ex.Message);
            }
        }
        protected String GetUserDBName()
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetUserDBName method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String tmpConn = strConn2 + "database=" + databaseName + ";";
            var userName = this.Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (userName == null || userName == "")
            {
                userName = User.Identity.Name;
            }
            String email = "";
            String sqlStr = @" SELECT B.Name " +
                            "	  FROM [" + databaseName + "].[dbo].[User] A " +
                            "	  Inner JOIN [" + databaseName + "].[dbo].[Company] B " +
                            "	  ON" +
                            "	  A.CompanyId=B.CompanyId " +
                            "	  where email= '" + userName + "'";
            try
            {
                MyConnection = new SqlConnection(tmpConn);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An SQL error occurred during execution of GetUserDBName method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during execution of GetUserDBName method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetUserDBName method completed.");
            return (email);
        }
        protected String GetEmail()
        {
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetEmail method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" select email from  ClearJelly.dbo.[user] where email= '" + UserName + "'";
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An SQL error occurred during execution of GetEmail method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during execution of GetEmail method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetEmail method completed.");
            return (email);
        }

        protected AccessToken getAccessToken(String OrgShortCode)
        {
            AccessToken token = new AccessToken();
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();

            String sqlStr = @" Select top 1 * FROM ClearJelly.dbo.AccessTokens " +
                             " Where UserName=   '" + UserName + "' and OrgShortCode='" + OrgShortCode + "'   order by [ExpiryDateUtc] desc ";
            ;
            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    token.SessionHandle = myReader["SessionHandle"].ToString();
                    token.Token = myReader["Token"].ToString();
                    token.TokenSecret = myReader["TokenSecret"].ToString();
                }
            }
            catch (SqlException ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (token);
        }
        protected bool isTokenExpired(AccessToken token)
        {
            bool result = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Select count(*) FROM ClearJelly.dbo.AccessTokens " +
                             " Where UserName=   '" + UserName + "' " +
                             " AND convert(datetime,[ExpiryDateUtc],103) >convert(datetime,'" + DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss.fff",
                                                                                CultureInfo.InvariantCulture) + "',103)";
            int count = 0;
            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    count = myReader.GetInt32(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }

            if (count > 0)
            {
                result = false;
            }
            return (result);
        }

        protected bool HasTokenInDB(String OrgShortCode)
        {
            bool result = false;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            //var repository = (Repository)Session["repository"];
            //String Org = repository.Organisation.Name;
            String sqlStr = @" Select count(*) FROM ClearJelly.dbo.AccessTokens " +
                             " Where UserName=   '" + UserName + "' and OrgShortCode='" + OrgShortCode + "' ";
            int count = 0;
            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    count = myReader.GetInt32(0);
                }
            }
            catch (SqlException ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }

            if (count > 0)
            {
                result = true;
            }
            return (result);
        }

        protected bool UpdateAccessToken(AccessToken token, String OrgShortCode)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            var repository = (Repository)Session["repository"];
            CultureInfo Au_date = new CultureInfo("en-AU");
            String sqlStr = @" UPDATE ClearJelly.dbo.AccessTokens " +
                             "      SET [ExpiryDateUtc]= '" + (token.ExpiryDateUtc).Value.ToString(Au_date) + "' ," +
                             "                  [Token]= '" + token.Token + "'  ," +
                             "                  [TokenSecret]='" + token.TokenSecret + "'," +
                             "                  [SessionHandle]='" + token.SessionHandle + "' " +
                             "      Where UserName='" + UserName + "' AND OrgShortCode ='" + OrgShortCode + "' ;";

            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }
        protected bool InsertAccessToken(AccessToken token, String OrgShortCode)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            var repository = (Repository)Session["repository"];
            String Org = repository.Organisation.Name;
            CultureInfo Au_date = new CultureInfo("en-AU");
            String email = "";
            String sqlStr = @" Insert Into ClearJelly.dbo.AccessTokens (UserName,[ExpiryDateUtc],[Token],[TokenSecret],[SessionHandle],[organisation],OrgShortCode) VALUES" +
                             "      ('" + UserName + "','" + (token.ExpiryDateUtc).Value.ToString(Au_date) + "' ," +
                             "                   '" + token.Token + "'  ," +
                             "                  '" + token.TokenSecret + "'," +
                             "                  '" + token.SessionHandle + "', " +
                             "					'" + Org + "','" + OrgShortCode + "'	" +
                             "      );";

            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }
        protected bool UpdateRequestToken(RequestToken token)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Insert Into ClearJelly.dbo.RequestTokens Values" +
                             "          ('" + UserName + "'  ," +
                             "          '" + token.AccessToken + "'  ," +
                             "          '" + token.CallbackUrl + "'  ," +
                             "          '" + token.Realm + "'  ," +
                             "          '" + token.Token + "'  ," +
                             "          '" + token.TokenSecret + "'  ," +
                             "          '" + token.Verifier + "'  )"
                             ;
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }
        protected bool UpdateConsumerSession(Object session)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Insert Into  ClearJelly.dbo.ConsumerSessions Values('" + UserName + "','" + session.ToString() + "' ) ";
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }

        protected int getApiCallCount()
        {
            int result = 0;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            //var repository = (Repository)Session["repository"];
            //String Org = repository.Organisation.Name;
            String sqlStr = @" Select CallCount FROM [" + databaseName + "].[dbo].[xero_api_calls] " +
                             " Where OrgShortCode=   '" + Request.Params["org"] + "'"; //and organisation='" + Org + "' ";
            int count = 0;
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    result = myReader.GetInt32(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }


            return (result);
        }

        private void GetUserOrganisations()
        {
            var companies = new Dictionary<string, string>();
            int result = 0;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            var userName = this.Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (userName == null || userName == "")
            {
                userName = User.Identity.Name;
            }
            //var repository = (Repository)Session["repository"];
            //String Org = repository.Organisation.Name;
            //        String sqlStr = @" UPDATE [ClearJelly].[dbo].[xero_api_calls]  SET CallCount= " + CallCount +
            //                         " Where OrgShortCode=   '" + Request.Params["org"] + "'"; //and organisation='" + Org + "' ";


            String sqlStr = @"SELECT  " +
                             "    distinct [OrgName], [OrgShortCode] " +
                             "     " +
                             " FROM [" + databaseName + "].[dbo].[Xero_User_Org] A " +
                             "   inner join [User] B " +
                             "   ON " +
                             "   A.CompanyID=B.CompanyId " +
                             "   where " +
                             "   UPPER(Ltrim(RTRIM(B.Email)))=UPPER('" + userName.Trim() + "') ";

            int count = 0;
            try
            {
                MyConnection = new SqlConnection(metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    companies.Add(myReader.GetString(1), myReader.GetString(0));
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }

            CompanyListDropDown.DataSource = companies;
            CompanyListDropDown.DataTextField = "Value";
            CompanyListDropDown.DataValueField = "Key";
            CompanyListDropDown.DataBind();
            CompanyListDropDown.Items.Insert(0, "-Select your organisation-");
        }

        protected void CompanyListDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CompanyListDropDown.SelectedIndex > 0)
            {
                Session["orgShortCode"] = CompanyListDropDown.SelectedItem.Value;
                Session["orgName"] = CompanyListDropDown.SelectedItem.Text;
            }
        }
    }
}