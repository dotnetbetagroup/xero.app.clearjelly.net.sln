﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ClearJelly.Web.Index" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="home-main">
        <header>
            <nav class="navbar-default">
                <div class="header-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="logo">
                                    <h1><a href="/">
                                        <img class="logoImgCss" src="/images/logo.png" alt="" /></a></h1>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12 responsive-menu">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <div class="top-menu-main">
                                        <ul>
                                            <li><a href="/Home.aspx">Home</a></li>
                                            <li><a href="http://www.clearjelly.net/#clear-jelly-overview">About</a></li>
                                            <li><a href="http://clearjelly.net/#/managility">Contact</a></li>
                                            <li><a href="https://managility.freshdesk.com">Support</a></li>
                                            <li><a id="loginButton">Login</a></li>
                                            <li><a runat="server" id="registerButton" class="login-link">Register
                                            </a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-6 header-right-icon-main">
                                <div class="header-right-icon">
                                    <img src="/images/right-xero-icon.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>


        <div class="banar-main">
            <div class="container">
                <h2>Transparent and Flexible as Jelly</h2>
                <p>
                    The Clear Jelly Business Intelligence Add-on gives you full control, insight<br />
                    and simulation options with your data in cloud-based accounting services<br>
                    (like Xero or Saasu) as well as other sources
                </p>
                <div class="try-btn"><a href="/Account/Register.aspx">Try Now</a></div>
                <ul>
                    <li><a href="#key-benift">Key Benefits<i><img src="/images/top-drop-arrow.png" alt="" /></i></a></li>
                    <li><a href="#description">Description<i><img src="/images/top-drop-arrow.png" alt="" /></i></a></li>
                    <li><a href="#what-is-xero">What is Xero?<i><img src="/images/top-drop-arrow.png" alt="" /></i></a></li>
                    <li><a href="#how-to-connect">How to Connect<i><img src="/images/top-drop-arrow.png" alt="" /></i></a></li>
                </ul>
            </div>
        </div>



        <div class="key-benift" id="key-benift">
            <div class="container">
                <div class="heading">
                    <h2>Key Benefits</h2>
                    <p>
                        Clear Jelly is an integrated framework that adds professional business intelligence<br>
                        functionality to cloud-based accounting platforms. It offers the following features:
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="key-benifit-box">
                            <div class="icon">
                                <img src="/images/icon1.png" alt="" />
                            </div>
                            <h3>Work With<br />
                                Flexible Reporting</h3>
                            <p>Using the Clear Jelly Excel-Add inor Web App you can easily design reports and dashboards with complete flexibility. Once created they are linked with your live data & you can change to any point of view required in addition to drill down and drill through options to transaction level. Share your reports as normal Excel files or publish them on the web with detailed security rights options.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="key-benifit-box">
                            <div class="icon">
                                <img src="/images/icon2.png" alt="" />
                            </div>
                            <h3>Publish Visually<br />
                                Stunning Dashboards</h3>
                            <p>Using the Clear Jelly Excel-Add in or Web App you can easily designreports and dashboards with complete flexibility. Once created they are linked with your live data & you can change to any point of view required in addition to drill down and drill through options to transaction level. Share your reports as normal Excel files or publish them on the web with detailed security rights options.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="key-benifit-box">
                            <div class="icon">
                                <img src="/images/icon3.png" alt="" />
                            </div>
                            <h3>Effective Planning &<br />
                                Modelling (Premium)</h3>
                            <p>Planning and budgeting have never been easier: use extensive options for top/down bottom planning on any level of the model. Modify structures as required e.g. simulating theintroduction of a new product, organisational changes or acquisitions.</p>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="key-benifit-box">
                            <div class="icon">
                                <img src="/images/icon-4.png" alt="" />
                            </div>
                            <h3>Powerful Calculation<br />
                                Engine (Premium)</h3>
                            <p>Easily create comprehensive calculations for financial formulas or more comprehensive business logic combining all areas of your system.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="key-benifit-box">
                            <div class="icon">
                                <img src="/images/icon5.png" alt="" />
                            </div>
                            <h3>Professional User<br />
                                Rights (Premium)</h3>
                            <p>Clear Jelly enables you to setup exact user rights what user are allowed to see, where they are allowed to change data etc. Reports will automatically take into account user rights and only display what information and features are available to the current user.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="key-benifit-box">
                            <div class="icon">
                                <img src="/images/icon6.png" alt="" />
                            </div>
                            <h3>Backup and<br />
                                Offline Access</h3>
                            <p>Clear Jelly Premium maintains a relational or multidimensional data model with all your Accounting Service (Xero/Saasu) transactions that can be used offline with advanced reporting and ad hocquery options.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="description" id="description">
            <div class="container">
                <div class="heading">
                    <h2>Description</h2>
                    <p>
                        The Clear Jelly Business Intelligence Add-on enables unparalleled insight, advanced planning/<br>
                        simulation options and full control of your data in Xero. Easily integrate all your other sources like<br>
                        web analytics, social media results, other corporate systems and publish your results in beautiful<br>
                        interactive management dashboards and reports.
                    </p>
                </div>

                <div class="image-disc">
                    <a class="" data-height="720" data-lighter="/images/dashboard_xero.png" data-width="1280" href="/images/dashboard_xero.png">
                        <img src="/images/description-bottom-img.png" alt="" />
                    </a>
                </div>
            </div>
        </div>


        <div class="what-is-xero-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="what-is-xero" id="what-is-xero">
                            <div class="heading">
                                <h2>What is Xero?</h2>
                                <p>
                                    Xero is easy to use online accounting software that’s<br>
                                    designed specifically for small businesses.
                                </p>
                            </div>
                            <div class="xero-icon">
                                <img src="/images/what-is-xero-icon.png" alt="" />
                            </div>
                            <ul>
                                <li>• It's all online, so you can work when and where you want to. Just login with your PC, Mac or mobile.</li>
                                <li>• Your bank statements are automatically imported and categorized, letting you see your cashflow in real-time.</li>
                                <li>• Invite your team and work together on financials. You can collaborate over your up-to-date numbers.</li>
                                <li>• Xero has all you need to run your business – including invoicing, paying bills, sales tax returns, reporting & more.</li>
                                <span>Find out more or Try Xero Accounting Software for free.</span>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="how-to-connect" id="how-to-connect">
                            <div class="heading">
                                <h2>How To Connect</h2>
                                <p>
                                    After you have registered at the Clear Jelly site<br>
                                    app.ClearJelly.net, you will see this screen:
                                </p>
                            </div>
                            <div class="how-to-connect-img">
                                <a class="" data-height="720" data-lighter="/images/how_to_connect.png" data-width="1280" href="/images/how_to_connect.png">
                                    <img src="/images/how-to-connect.png" alt="">
                                </a>
                            </div>
                            <p>
                                Click on <i>“Add and authorise Xero organisations”</i> to link
                                    <br>
                                your Xero company file with Clear Jelly. This process will
                                    <br>
                                create cloud-based relational (based on MS SQL Server)<br>
                                and a multi-dimensional data model with your data.
                            </p>
                            <p>
                                Linking the accounts should only take a few<br>
                                minutes. For larger models, this can take longer. Once it<br>
                                is finished you will receive a notification at the email<br>
                                address you have specified with your account.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                        <ul>
                            <li><a href="/privacypolicy.aspx">Privacy Policy</a></li>
                            <li><a href="/terms-of-use.aspx">Terms of Use</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 pull-right text-right copy-right-text-main">&copy; 2015 Managility Pty Ltd</div>
                </div>
            </div>
        </div>

    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/Scripts/jquery-1.11.2.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/Scripts/bootstrap.js"></script>
    <script src="/Scripts/jquery.lighter.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $('a[href*=#]:not([href=#])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>
</asp:Content>
