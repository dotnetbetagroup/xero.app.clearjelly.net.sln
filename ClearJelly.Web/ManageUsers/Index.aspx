﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ManageUsers_Index" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Manage Users</h1>
            </div>
            <p class="text-danger">
                <asp:Label runat="server" ID="LblFailure" />
            </p>

            <div class="alert alert-danger" role="alert" id="errorText" runat="server" visible="false">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <asp:Label runat="server" ID="lblerror" />
            </div>

            <div class="alert alert-success" role="alert" id="succssDiv" runat="server" visible="false">
                <a class="close" onclick="$('.alert').hide()">×</a>
            <asp:Label runat="server" ID="lblsuccessMsg" CssClass="successMessage" />
            </div>

            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div class="row">
                <div class="col-md-11">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-2 col-md-offset-11">
                                <div class="col-md-11 col-md-offset-5">
                                    <button id="addUser" type="button" class="btn btn-success btn-block">Add User</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="manageuserdv">
            </div>
        </div>
    </div>
    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/ManageUserIndex.js">
    </script>
</asp:Content>