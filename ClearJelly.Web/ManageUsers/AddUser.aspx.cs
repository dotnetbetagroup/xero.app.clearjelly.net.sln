﻿using System.Configuration;
using System.Data.SqlClient;
using System.Net.Configuration;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.Script.Services;
using ClearJelly.Entities;
using ClearJelly.XeroApp;
using PaypalIntegration;
using PaypalIntegration.PayPallHelper;
using ClearJelly.ViewModels.PayPalViewModels;
using ClearJelly.ViewModels;
using ClearJelly.Services;

namespace ClearJelly.Web.ManageUsers
{

    public partial class ManageUsers_AddUser : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;
        public ManageUsers_AddUser()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx", false);
                }
                if (Session["IsSubscriptionExpired"] == null)
                {
                    //throw new Exception("Invalid session IsSubscriptionExpired");
                }

                if (Convert.ToBoolean(Session["IsSubscriptionExpired"]))
                {
                    if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                    {
                        Response.Redirect("~/ManageSubscription.aspx", false);
                    }
                }
                if (!IsPostBack)
                {
                    if (Session["IsAdmin"] != null && !Convert.ToBoolean(Session["IsAdmin"]))
                        Response.Redirect("~/Account/Login.aspx", false);
                    var token = Request.QueryString["token"];
                    var payerId = Request.QueryString["PayerID"];
                    var Recurring = Request.QueryString["Recurring"];
                    if (Session["New_UserId"] != null && !string.IsNullOrEmpty(token))
                    {
                        if (!string.IsNullOrEmpty(payerId))
                        {
                            if (Session["New_UserId"] == null)
                            {
                                throw new Exception("Invalid session New_UserId");
                            }
                            if (Session["Jelly_user"] == null)
                            {
                                throw new Exception("Invalid session Jelly_user");
                            }
                            var newUserId = Convert.ToInt32(Session["New_UserId"].ToString());
                            var newUser = _clearJellyContext.Users.Single(x => x.UserId == newUserId && x.IsDeleted);
                            var companySubscription = GetCurrentCompanySubscription();
                            var oldRecurringProfileId = companySubscription.RecurringProfileId;
                            if (Recurring != null && !Convert.ToBoolean(Recurring))
                            {
                                decimal paidAmount = 0;
                                var TransactionReference = DoPayment(token, companySubscription, payerId, out paidAmount);
                                Session.Remove("New_UserId");
                                //token , payerid, transaction refe
                                PaypalCheckout(newUser, TransactionReference, paidAmount, token, payerId);
                            }
                            else
                            {
                                var recurringProfileId = StartRecurringPayment(token, payerId, newUser.CompanyId, companySubscription);
                                var email = Session["Jelly_user"].ToString();
                                var currentUser = GetCurrentUser(email);
                                UpdatePaypalDetails(currentUser, token, payerId, recurringProfileId, companySubscription);

                                if (!bool.Parse(Common.Common.GetIpnInclude()))
                                {
                                    if (!string.IsNullOrEmpty(recurringProfileId))
                                    {
                                        EnableUser(newUser, companySubscription);
                                    }
                                }
                                Session.Remove("New_UserId");
                                CancelRecurringProfile(oldRecurringProfileId);
                                Response.Redirect("~/ManageUsers/Index.aspx?IsSuccess=true", false);
                            }
                        }
                        else
                        {

                            Response.Redirect("~/ManageUsers/Index.aspx?IsSuccess=false", false);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser Page_Load event completed.", ex);
            }

        }

        private void EnableUser(User newUser, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser EnableUser event started.");
                newUser.IsDeleted = false;
                companySubscription.AdditionalUsers += 1;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser EnableUser event completed.", ex);
            }

        }

        private void CancelRecurringProfile(string oldRecurringProfileId)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser CancelRecurringProfile event started.");
                var expChkt = new PayPalExpressCheckout();
                //Delete Recurring Payment profile
                var mngRecurringStatus = expChkt.ManageRecurringPaymentsProfileStatus(oldRecurringProfileId, PayPalEnums.RecurringProfileAction.Cancel);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser CancelRecurringProfile event completed.", ex);
            }

        }

        protected void AddUser_Click(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser AddUser_Click event started.");
                if (Session["Jelly_user"] != null)
                {
                    string email = Email.Text.Trim();
                    if (Common.Common.UserExists(email))
                    {
                        ErrorMessage.Text = "The user already exists. Please enter a different username.";
                        return;
                    }
                    var selectedSubscription = int.Parse(HdnSubscription.Value);
                    Session["selectedCompanySubscriptionId"] = selectedSubscription;
                    var registeredUser = addUser(email, selectedSubscription);
                    SendActivationLink(email, registeredUser);

                    AddUserPaypalCheckout(registeredUser);
                }
                else { throw new Exception("Invalid session Jelly_user"); }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser AddUser_Click event completed.", ex);
            }
        }




        private User GetCurrentUser(string email)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetCurrentUser event started.");
                return _clearJellyContext.Users.Single(x => x.Email == email && x.IsActive && !x.IsDeleted);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetCurrentUser event completed.", ex);
                throw ex;
            }

        }


        private string StartRecurringPayment(string token, string payerId, int companyId, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser StartRecurringPayment event started.");
                //GetExpressCheckoutDetails 
                var exp = new PayPalExpressCheckout();
                decimal orderTotal;
                if (companySubscription.IsYearlySubscription)
                {
                    var grossAmount = (companySubscription.SubscriptionType.YearlyContractFee * companySubscription.Quantity) +
                                      (companySubscription.SubscriptionType.AdditionalUserFee * (companySubscription.AdditionalUsers + 1));
                    var gstAmount = Math.Round(companySubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                }
                else
                {
                    var grossAmount = (companySubscription.SubscriptionType.MonthlyFee * companySubscription.Quantity) +
                                      (companySubscription.SubscriptionType.AdditionalUserFee * (companySubscription.AdditionalUsers + 1));
                    var gstAmount = Math.Round(companySubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                }
                var startDate = CalculateRecurringProfileStartDate(companySubscription.StartDate);
                //var initialAmount = GetInitialAmountForAdditionalUser(startDate, companySubscription);
                var reecurringProfile = new RecurringProfile()
                {
                    Amount = orderTotal,
                    BillingFrequency = 1,
                    BillingPeriod = Common.Common.GetBillingPeriodPaypal().ToLower() == "day" ? PayPalEnums.RecurringPeriod.Day : PayPalEnums.RecurringPeriod.Month,
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    StartDate = startDate,
                    ItemName = companySubscription.SubscriptionType.TypeName.Trim(),
                    Description = companySubscription.SubscriptionType.TypeName.Trim()
                };
                var resRecurring = exp.CreateRecurringPaymentsProfile(reecurringProfile, token);
                if (resRecurring.IsSystemFailure)
                {
                    var recuiringAmountDetails = getPayalDetails(orderTotal, payerId, token, resRecurring.TransactionReference, resRecurring.FailureMessage);
                    //send mail with all details.
                    //sendMailonFailPayment(recuiringAmountDetails);
                    Response.Redirect("~/ManageUsers/Index.aspx?IsSuccess=false", false);
                }
                return resRecurring.ProfileId;
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser StartRecurringPayment event completed.", ex);
                throw ex;
            }

        }

        private string DoPayment(string token, CompanySubscription companySubscription, string payerId, out decimal paidAmount)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of DoPayment StartRecurringPayment event started.");
                var exp = new PayPalExpressCheckout();
                var startDate = CalculateRecurringProfileStartDate(companySubscription.StartDate);
                var paypalRequestAmt = GetInitialAmountForAdditionalUser(startDate, companySubscription);
                var amount = paypalRequestAmt.OrderTotal;
                paidAmount = paypalRequestAmt.OrderTotal;
                var currency = PayPalEnums.CurrencyCodeType.AUD;
                var varToken = token;
                var paypalPayerId = payerId;
                var resRecurring = exp.DoExpressCheckoutPayment(amount, currency, varToken, paypalPayerId);
                if (resRecurring.IsSystemFailure)
                {
                    Response.Redirect("~/ManageUsers/Index.aspx?IsSuccess=false", false);
                }

                return resRecurring.TransactionReference;
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser StartRecurringPayment event completed.", ex);
                throw ex;
            }

        }

        private PaypalRequestAmountViewModel GetInitialAmountForAdditionalUser(DateTime recurringProfileStartDate, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetInitialAmountForAdditionalUser event started.");
                var daysInterval = (recurringProfileStartDate - DateTime.Now.Date).Days;
                PaypalRequestAmountViewModel paypalRequestAmountViewModel = new PaypalRequestAmountViewModel();
                decimal initialGrossAmount;
                if (companySubscription.IsYearlySubscription)
                {
                    initialGrossAmount = daysInterval * companySubscription.SubscriptionType.AdditionalUserFee / 30;
                }
                else
                {
                    initialGrossAmount = daysInterval * companySubscription.SubscriptionType.AdditionalUserFee / 30;
                }
                var initialGstAmount = Math.Round(initialGrossAmount * companySubscription.Company.Country.GSTRate / 100, 2);
                var initialAmount = Math.Round(initialGrossAmount + initialGstAmount, 2);
                paypalRequestAmountViewModel.OrderAmount = Math.Round(initialGrossAmount, 2);
                paypalRequestAmountViewModel.OrderGStAmt = initialGstAmount;
                paypalRequestAmountViewModel.OrderTotal = initialAmount;
                return paypalRequestAmountViewModel;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetInitialAmountForAdditionalUser event completed.", ex);
                throw ex;
            }

        }

        /// <summary>
        /// Function to get the next due date for the Recurring Payment Profile.
        /// </summary>
        /// <param name="companyProfileStartDate">The initial Start Date of the Recurring Payment Profile.</param>
        /// <returns>The next due date for the Recurring Payment Profile</returns>
        private DateTime CalculateRecurringProfileStartDate(DateTime companyProfileStartDate)
        {
            try
            {
                var currentDate = DateTime.Now.Date;
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser CalculateRecurringProfileStartDate event started.");
                // If the Start Date of the recurring profile which is passed as the parameter is <= 28
                //company subscription (recuring Date) start date is less than or equal to 28
                if (companyProfileStartDate.Day <= 28)
                {
                    /*
                    If the next due date of the recurring profile is in the past. (i.e.the payment is already done for the current month)
                    Set the new start date of the recurring profile to the same day of the next month.
                    */
                    if (companyProfileStartDate.Day <= currentDate.Day)
                    {
                        //recuiring profile date is same day next month.
                        var latestPaymentDate = new DateTime(currentDate.Year, currentDate.Month, companyProfileStartDate.Day);
                        var recurringProfileStartDate = latestPaymentDate.AddMonths(1);
                        return recurringProfileStartDate;
                    }
                    /*
                    next due date of the recuiring profile is in future. (i.e.recuiring payment will be occurred in next few days)
                    set the start date of the recuiring profile to the same day of same(current) month.
                    */
                    else
                    {
                        //recuiring profile start date =  same day current month.
                        var recurringProfileStartDate = new DateTime(currentDate.Year, currentDate.Month, companyProfileStartDate.Day);
                        return recurringProfileStartDate;
                    }
                }
                //If the day is greater than 28 
                //for only 29,30,31
                else
                {
                    var paymentStartDay = companyProfileStartDate.Day;
                    var paymentDate = companyProfileStartDate.AddMonths(1);
                    /*
                    check is any month(fromstart-current)had got wrong date.
                    wrong date : starting from recuiring date after adding 1 month if you get wrong date than we need to start from 1st.
                    i.e. 30 feb. we have to start it from 1 march.
                    if any wrong date than we have to start from 1st date.
                    */
                    while (paymentDate <= currentDate)
                    {

                        if (paymentDate.Day != paymentStartDay)
                        {
                            paymentStartDay = 1;
                            break;
                        }
                        //add for loop iteration.
                        paymentDate = paymentDate.AddMonths(1);
                    }
                    //execute only if from above loop we got paymentstartday 1(means got wrong date and break on loop)
                    //recuiring profile start date will be 1st date(day) nextmonth.
                    if (paymentStartDay == 1)
                    {
                        //nextmonth 1st day is our recuiring date.
                        var nextMonthDate = currentDate.AddMonths(1);
                        var recurringProfileStartDate = new DateTime(nextMonthDate.Year, nextMonthDate.Month, paymentStartDay);
                        return recurringProfileStartDate;
                    }

                    /*
                    //if not break in loop(No wrong date till now)  than this will execute.
                    If the next due date of the recurring profile is in the past. (i.e.the payment is already done for the current month)
                    Set the new start date of the recurring profile to the same day of the next month.
                    */
                    if (companyProfileStartDate.Day <= currentDate.Day)
                    {
                        var latestPaymentDate = new DateTime(currentDate.Year, currentDate.Month, companyProfileStartDate.Day);
                        var nextMonthDate = latestPaymentDate.AddMonths(1);
                        //after addiing month if day is not same.
                        //i.e.in 31march jan Add 1 month will give us 30 april.
                        //add one more day so recuiring start from 1st.(as per paypal)
                        if (latestPaymentDate.Day != nextMonthDate.Day)
                        {
                            var recurringProfileStartDate = nextMonthDate.AddDays(1);
                            return recurringProfileStartDate;
                        }
                        return nextMonthDate;
                    }
                    //recuiring profile start date's day is big than current Day.
                    /*
                    next due date of the recuiring profile is in future. (i.e.recuiring payment will be occurred in next few days)
                    set the start date of the recuiring profile to the same day of same(current) month.
                    in below code we added 1 month in previous payment date
                   */
                    else
                    {
                        //find previous month 
                        var previousMonthDate = currentDate.AddMonths(-1);
                        //find previous month payment date.
                        var latestPaymentDate = new DateTime(previousMonthDate.Year, previousMonthDate.Month, companyProfileStartDate.Day);
                        //get next payment date
                        var currentMonthPaymentDate = latestPaymentDate.AddMonths(1);
                        //after addiing month if day is not same.
                        //i.e.in 31march jan Add 1 month will give us 30 april.
                        // add one more day so it will so recuiring start from 1st.
                        if (latestPaymentDate.Day != currentMonthPaymentDate.Day)
                        {
                            var recurrintProfileStartDate = currentMonthPaymentDate.AddDays(1);
                            return recurrintProfileStartDate;
                        }
                        return currentMonthPaymentDate;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser CalculateRecurringProfileStartDate event completed.", ex);
                throw ex;
            }
        }

        private void UpdatePaypalDetails(User user, string token, string payerId, string recurringProfileId, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser UpdatePaypalDetails event started.");
                user.PaypalToken = token;
                user.PaypalPayerId = payerId;
                companySubscription.RecurringProfileId = recurringProfileId;
                //if user tried to add user in trail subscription we start payment for that user.
                companySubscription.EndDate = null;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser UpdatePaypalDetails event completed.", ex);
            }
        }

        private void PaypalCheckout(User registeredUser, string transactionReference, decimal paidAmount, string oldToken, string oldPayerId)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser PaypalCheckout event started.");
                //var currentSubscription = GetCurrentCompanySubscription(registeredUser.CompanyId);
                var currentSubscription = GetCurrentCompanySubscription();

                var expChkt = new PayPalExpressCheckout();
                var orderDetailItem = new List<OrderDetailsItem>();
                var currentUrl = Request.Url.GetLeftPart(UriPartial.Path) + "?Recurring=true";
                var failUrl = Request.Url.GetLeftPart(UriPartial.Authority) + "/ManageUsers/Index.aspx?IsSuccess=false";
                decimal orderTotal;
                decimal orderAmount;
                decimal orderGST;
                if (currentSubscription.IsYearlySubscription)
                {
                    var grossAmount = (currentSubscription.SubscriptionType.YearlyContractFee * currentSubscription.Quantity) +
                                      (currentSubscription.SubscriptionType.AdditionalUserFee * (currentSubscription.AdditionalUsers + 1));
                    var gstAmount = Math.Round(currentSubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                    orderAmount = grossAmount;
                    orderGST = gstAmount;
                }
                else
                {
                    var grossAmount = (currentSubscription.SubscriptionType.MonthlyFee * currentSubscription.Quantity) +
                                      (currentSubscription.SubscriptionType.AdditionalUserFee * (currentSubscription.AdditionalUsers + 1));
                    var gstAmount = Math.Round(currentSubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                    orderAmount = grossAmount;
                    orderGST = gstAmount;
                }
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = currentSubscription.SubscriptionType.TypeName.Trim(),
                    IsRecurringPayment = true,
                    UnitPrice = orderAmount,
                    Quantity = 1
                });
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = "GST(10%)",
                    IsRecurringPayment = true,
                    UnitPrice = orderGST,
                    Quantity = 1
                });
                // Get Token
                var reqOrderDetail = new OrderDetails()
                {
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    DisplayShippingAddress = PayPalEnums.PayPalNoShipping.DoNotDisplay,
                    OrderTotal = orderTotal,
                    Items = orderDetailItem,
                    CustomField = Session["AdditionalUserID"].ToString() + "_" + (Enum.GetName(typeof(SaasuOrXero), SaasuOrXero.Xero))

                };
                var resToken = expChkt.SetExpressCheckout(reqOrderDetail, failUrl, currentUrl);
                if (!resToken.IsSystemFailure)
                {

                    Session["New_UserId"] = registeredUser.UserId;
                }
                else
                {
                    // store details in session..
                    var addUserPaidAmountDetails = getPayalDetails(paidAmount, oldPayerId, oldToken, transactionReference, resToken.FailureMessage);
                    Session["PaidDetails"] = addUserPaidAmountDetails;
                    Response.Redirect("~/ManageUsers/Index.aspx?IsSuccess=false", false);
                }
                Response.Redirect(resToken.RedirectUrl, false);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser PaypalCheckout event completed.", ex);
            }
        }

        //TODO:Update email body and also add in email template.
        private void sendMailonFailPayment(PaypalResponseViewModel recuiringDetails = null)
        {
            var paidDetails = (PaypalResponseViewModel)Session["PaidDetails"];

            var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            if (siteUrl != null)
            {
                StringBuilder sbody = new StringBuilder();
                //sbody.Append(
                //    "<div style='font-family:Calibri;font-size:14px;'><div>Hello Admin" +
                //    ",</div> <div> <br/>A new user has been registered on Clearjelly with the below details:" +
                //    "</div> ");
                sbody.Append("<div> <br/> <strong>Login: </strong> Test");

                sbody.Append("</div><br/><br/><div>Thanks,</div><div>ClearJelly</div></div>");
                var fromAddress = GetFromAddress();
                var clearJellyInfoEmail = "";
                if (clearJellyInfoEmail == null)
                {
                    throw new Exception("clearJelly Info Email is not defined.");
                }
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(fromAddress, clearJellyInfoEmail, "Error occurred in adduser payment", sbody.ToString());
                System.Net.Mail.SmtpClient mailclient = new System.Net.Mail.SmtpClient();
                mailclient.ServicePoint.MaxIdleTime = 2;
                if (SmtpMailSendingService.GetEnableSSLKey())
                {
                    mailclient.EnableSsl = true;
                }
                mail.IsBodyHtml = true;
                mailclient.Send(mail);

            }
            else
            {
                _logger.Error((Session["Registred_user"] ?? "") + "Site Url is not defined.");
            }
        }

        private PaypalResponseViewModel getPayalDetails(decimal paidAmount, string payerId, string tokenId, string transactionRef, string failureMessage)
        {
            return new PaypalResponseViewModel()
            {
                Amount = paidAmount,
                PayerId = payerId,
                TokenID = tokenId,
                TransactionReference = transactionRef,
                FailureMessage = failureMessage,
            };
        }

        private int GetSelectedCompanySubscription()
        {
            var selectedSubscription = Session["selectedCompanySubscriptionId"];
            if (selectedSubscription == null)
            {
                throw new Exception("Invalid selectedSubscriptionId");
            }
            var companySubscriptionId = int.Parse(selectedSubscription.ToString());
            return companySubscriptionId;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static AdditionUserDetailViewModel GetAdditionalUserFeeDetails(int subscriptionId)
        {
            var currentPage = new ManageUsers_AddUser();
            try
            {
                //var selectedSubscription = int.Parse(currentPage.HdnSubscription.Value);
                _logger.Info((currentPage.Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetAdditionalUserFeeDetails event started.");
                currentPage.Session["selectedCompanySubscriptionId"] = subscriptionId;
                var currentSubscription = currentPage.GetCurrentCompanySubscription();
                decimal orderTotal;
                decimal gstAmount;
                decimal grossAmount;
                if (currentSubscription.IsYearlySubscription)
                {
                    grossAmount = (currentSubscription.SubscriptionType.YearlyContractFee * currentSubscription.Quantity) +
                                     (currentSubscription.SubscriptionType.AdditionalUserFee * (currentSubscription.AdditionalUsers + 1));
                    gstAmount = Math.Round(currentSubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                }
                else
                {
                    grossAmount = (currentSubscription.SubscriptionType.MonthlyFee * currentSubscription.Quantity) +
                                     (currentSubscription.SubscriptionType.AdditionalUserFee * (currentSubscription.AdditionalUsers + 1));
                    gstAmount = Math.Round(currentSubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                }
                var startDate = currentPage.CalculateRecurringProfileStartDate(currentSubscription.StartDate);
                var paypalRequestAmt = currentPage.GetInitialAmountForAdditionalUser(startDate, currentSubscription);

                var AdditionUserDetailVm = new AdditionUserDetailViewModel();
                AdditionUserDetailVm.AdditionUserGst = paypalRequestAmt.OrderGStAmt;
                AdditionUserDetailVm.AdditionUserPrice = paypalRequestAmt.OrderAmount;
                AdditionUserDetailVm.AdditionalUserGrossAmount = paypalRequestAmt.OrderTotal;
                AdditionUserDetailVm.SubsctiptionOrderTotal = orderTotal;
                AdditionUserDetailVm.SubscriptionGrossAmount = grossAmount;
                AdditionUserDetailVm.SubsctiptionGstOrderTotal = gstAmount;
                return AdditionUserDetailVm;
            }
            catch (Exception ex)
            {
                _logger.Error((currentPage.Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetAdditionalUserFeeDetails event completed.", ex);
                throw;
            }
        }


        private void AddUserPaypalCheckout(User registeredUser)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser PaypalCheckout event started.");
                //we will take selected admin subscription while add new user.
                //var currentSubscription = GetCurrentCompanySubscription(registeredUser.CompanyId);

                var currentSubscription = GetCurrentCompanySubscription();

                var expChkt = new PayPalExpressCheckout();
                var orderDetailItem = new List<OrderDetailsItem>();
                var currentUrl = HttpContext.Current.Request.Url.AbsoluteUri + "?Recurring=false";
                var failUrl = Request.Url.GetLeftPart(UriPartial.Authority) + "/ManageUsers/Index.aspx?IsSuccess=false";
                decimal orderTotal;
                Decimal orderAmount;
                Decimal orderGST;
                if (currentSubscription.IsYearlySubscription)
                {
                    var grossAmount = (currentSubscription.SubscriptionType.YearlyContractFee * currentSubscription.Quantity) +
                                      (currentSubscription.SubscriptionType.AdditionalUserFee * (currentSubscription.AdditionalUsers + 1));
                    var gstAmount = Math.Round(currentSubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                    orderAmount = grossAmount;
                    orderGST = gstAmount;
                }
                else
                {
                    var grossAmount = (currentSubscription.SubscriptionType.MonthlyFee * currentSubscription.Quantity) +
                                      (currentSubscription.SubscriptionType.AdditionalUserFee * (currentSubscription.AdditionalUsers + 1));
                    var gstAmount = Math.Round(currentSubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    orderTotal = grossAmount + gstAmount;
                    orderAmount = grossAmount;
                    orderGST = gstAmount;
                }


                var startDate = CalculateRecurringProfileStartDate(currentSubscription.StartDate);
                var paypalRequestAmt = GetInitialAmountForAdditionalUser(startDate, currentSubscription);
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = "One time prorate charge for the new additional user",
                    IsRecurringPayment = false,
                    UnitPrice = paypalRequestAmt.OrderAmount,
                    Quantity = 1
                });
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = "GST(10%)",
                    IsRecurringPayment = false,
                    UnitPrice = paypalRequestAmt.OrderGStAmt,
                    Quantity = 1
                });
                // Get Token
                var reqOrderDetail = new OrderDetails()
                {
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    DisplayShippingAddress = PayPalEnums.PayPalNoShipping.DoNotDisplay,
                    //OrderTotal = orderTotal + initialAmount,
                    OrderTotal = paypalRequestAmt.OrderTotal,
                    Items = orderDetailItem,
                    CustomField = Session["AdditionalUserID"].ToString() + "_" + (Enum.GetName(typeof(SaasuOrXero), SaasuOrXero.Xero))

                };
                var resToken = expChkt.SetExpressCheckout(reqOrderDetail, failUrl, currentUrl);
                if (!resToken.IsSystemFailure)
                {
                    Session["New_UserId"] = registeredUser.UserId;
                }
                else
                {

                    Response.Redirect("~/ManageUsers/Index.aspx?IsSuccess=false", false);
                }
                Response.Redirect(resToken.RedirectUrl, false);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser PaypalCheckout event completed.", ex);
            }
        }
        private CompanySubscription GetCurrentCompanySubscription()
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetCurrentCompanySubscription event started.");
                var companySubscriptionId = GetSelectedCompanySubscription();
                var currentDate = DateTime.Now.Date;
                return _clearJellyContext.CompanySubscriptions.Single(x => x.CompanySubscriptionId == companySubscriptionId
                                                            && x.IsActive && (!x.EndDate.HasValue || x.EndDate.Value >= currentDate));
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetCurrentCompanySubscription event completed.", ex);
                throw ex;
            }

        }

        private User addUser(string email, int selectedSubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser addUser event started.");
                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var adminEmail = Session["Jelly_user"].ToString();
                var adminUser =
                    _clearJellyContext.Users.SingleOrDefault(
                        x => x.IsAdmin && x.Email == adminEmail && x.IsActive && !x.IsDeleted);
                var encryptPassword = EncryptDecrypt.EncryptString(Password.Text);
                var activationCode = Guid.NewGuid();
                var profilePicture = ProfilePicture.PostedFile != null ? email + System.IO.Path.GetExtension(ProfilePicture.FileName) : null;
                //The user would be treated as a deleted user until the new payment is completed successfully.
                var newUser = new User
                {
                    Email = email,
                    Password = encryptPassword,
                    FirstName = FirstName.Text,
                    LastName = LastName.Text,
                    //Address = Address.Text,
                    Mobile = Mobile.Text,
                    LoginType = 0,
                    ProfilePicture = profilePicture,
                    ActivationCode = activationCode.ToString(),
                    RegistrationDate = DateTime.Now.Date,
                    IsAdmin = false,
                    IsDeleted = true,
                    CompanyId = adminUser.CompanyId
                };
                var additionalUser = new AdditionalUser
                {
                    CompanySubscriptionId = selectedSubscription,
                    UserId = newUser.UserId,
                    JoiningDate = DateTime.Now
                };
                _clearJellyContext.Users.Add(newUser);
                _clearJellyContext.AdditionalUsers.Add(additionalUser);
                _clearJellyContext.SaveChanges();
                Session["AdditionalUserID"] = additionalUser.AdditionalUserId;
                if (ProfilePicture.PostedFile.ContentLength > 0)
                {
                    var imagePath = WebConfigurationManager.AppSettings["ProfilePicturePath"];
                    System.IO.FileInfo file = new System.IO.FileInfo(imagePath);
                    file.Directory.Create();
                    ProfilePicture.SaveAs(imagePath + profilePicture);
                }
                return newUser;
            }
            catch (SqlException ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser  addUser event sql exception completed.", ex);
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser addUser event completed.", ex);
                throw ex;
            }
        }

        private void SendActivationLink(string email, User registeredUser)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser SendActivationLink event started.");
                string currentDomain = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                SmtpMailSendingService.SendActivationLink(email, registeredUser, currentDomain);
                _logger.Info("currentDomain:  Request.Url.GetLeftPart: " + currentDomain);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser SendActivationLink event completed.", ex);
            }
        }

        private string GetFromAddress()
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetFromAddress event started.");
                System.Configuration.Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
                return settings.Smtp.From;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of AddUser GetFromAddress event completed.", ex);
                throw ex;
            }
        }
    }
}