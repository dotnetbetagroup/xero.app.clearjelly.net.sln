﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.Entities;
using PaypalIntegration.PayPallHelper;
using ClearJelly.ViewModels.ManageUsers;

namespace ClearJelly.Web.ManageUsers
{

    public partial class ManageUsers_Index : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;
        public ManageUsers_Index()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx", false);
                }
                if (Session["IsSubscriptionExpired"] == null)
                {
                    //throw new Exception("Invalid session IsSubscriptionExpired");
                }

                if (Convert.ToBoolean(Session["IsSubscriptionExpired"]))
                {
                    if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                    {
                        Response.Redirect("~/ManageSubscription.aspx", false);
                    }
                }
                if (!IsPostBack)
                {
                    if (Session["IsAdmin"] != null && !Convert.ToBoolean(Session["IsAdmin"]))
                        Response.Redirect("~/Account/Login.aspx", false);
                }

                var errorMsg = Request.QueryString["IsSuccess"];
                if (errorMsg != null)
                {
                    var issuccess = Convert.ToBoolean(errorMsg);
                    if (issuccess)
                    {
                        succssDiv.Visible = true;
                        lblsuccessMsg.Text = "New User added successfully.";
                    }
                    else
                    {
                        errorText.Visible = true;
                        lblerror.Text = "Your payment is not successful please contact administrator.";
                    }

                }

            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Index Page_Load event completed.", ex);

            }

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<ManageUserViewModel> GetUserList()
        {
            try
            {
                var manageUserPage = new ManageUsers_Index();
                var userList = manageUserPage.GetCompanyUsers();
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Index GetUserList event completed.", ex);
                throw;
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static bool DeleteUser(string userId)
        {
            try
            {
                var userIdInt = int.Parse(userId);
                var manageUserPage = new ManageUsers_Index();
                var successStatus = manageUserPage.DeleteUser(userIdInt);
                return successStatus;
            }
            catch (InvalidOperationException ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Index GetCompanyUsers event completed.", ex);
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Index GetCompanyUsers event completed.", ex);
                return false;
            }
        }

        private List<ManageUserViewModel> GetCompanyUsers()
        {
            try
            {
                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }

                var email = Session["Jelly_user"].ToString();
                var currentUser = _clearJellyContext.Users.SingleOrDefault(x => x.IsActive && x.Email == email && !x.IsDeleted);
                var companyUsers =
                    _clearJellyContext.Users.Where(x => x.CompanyId == currentUser.CompanyId && !x.IsAdmin && !x.IsDeleted)
                        .Select(x => new ManageUserViewModel
                        {
                            UserId = x.UserId,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            Email = x.Email,
                            Mobile = x.Mobile,
                            IsActive = x.IsActive
                        }).ToList();
                return companyUsers;
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Index GetCompanyUsers event completed.", ex);
                throw ex;
            }
        }

        private bool DeleteUser(int userId)
        {
            try
            {
                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_user"].ToString();
                var currentUser = GetCurrentUser(email);
                var user = _clearJellyContext.Users.Single(x => x.UserId == userId && !x.IsDeleted);
                if (user == null || !currentUser.IsAdmin || currentUser.CompanyId != user.CompanyId)
                {
                    throw new InvalidOperationException();
                }
                var currentCompanySubscription = GetCurrentCompanySubscription(userId);
                UpdateRecurringProfile(currentCompanySubscription);
                user.IsDeleted = true;
                currentCompanySubscription.AdditionalUsers -= 1;
                _clearJellyContext.SaveChanges();
                return true;
            }
            catch (InvalidOperationException ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Index DeleteUser event completed.", ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Index DeleteUser event completed.", ex);
                throw;
            }
        }

        private void UpdateRecurringProfile(CompanySubscription currentCompanySubscription)
        {
            try
            {
                decimal newOrderTotal;
                if (currentCompanySubscription.IsYearlySubscription)
                {
                    var grossAmount = (currentCompanySubscription.SubscriptionType.YearlyContractFee * currentCompanySubscription.Quantity) +
                                      (currentCompanySubscription.SubscriptionType.AdditionalUserFee * (currentCompanySubscription.AdditionalUsers - 1));
                    var gstAmount = Math.Round(currentCompanySubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    newOrderTotal = grossAmount + gstAmount;
                }
                else
                {
                    var grossAmount = (currentCompanySubscription.SubscriptionType.MonthlyFee * currentCompanySubscription.Quantity) +
                                      (currentCompanySubscription.SubscriptionType.AdditionalUserFee * (currentCompanySubscription.AdditionalUsers - 1));
                    var gstAmount = Math.Round(currentCompanySubscription.Company.Country.GSTRate * grossAmount / 100, 2);
                    newOrderTotal = grossAmount + gstAmount;
                }
                var exp = new PayPalExpressCheckout();
                var reecurringProfile = new RecurringProfile()
                {
                    Amount = newOrderTotal
                };
                var recurringProfileId = currentCompanySubscription.RecurringProfileId.Trim();
                var resRecurring = exp.UpdateRecurringPaymentsProfile(reecurringProfile, recurringProfileId);
                if (resRecurring.IsSystemFailure)
                {
                    Response.Redirect("~/ManageUsers/Index.aspx", false);
                }
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Index GetCurrentUser event completed.", ex);
            }

        }

        private User GetCurrentUser(string email)
        {
            try
            {
                return _clearJellyContext.Users.Single(x => x.Email == email && x.IsActive && !x.IsDeleted);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Index GetCurrentUser event completed.", ex);
                throw;
            }

        }

        private CompanySubscription GetCurrentCompanySubscription(int userId)
        {
            try
            {
                var currentDate = DateTime.Now.Date;
                var additionalUserSubscription = _clearJellyContext.AdditionalUsers.First(x => x.UserId == userId);
                return _clearJellyContext.CompanySubscriptions.Single(x => x.CompanySubscriptionId == additionalUserSubscription.CompanySubscriptionId
                                                            && x.IsActive && (!x.EndDate.HasValue || x.EndDate.Value >= currentDate));
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Index GetCurrentCompanySubscription event completed.", ex);
                throw ex;
            }

        }
    }
}