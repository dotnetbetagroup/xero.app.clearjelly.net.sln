﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="ManageUsers_AddUser" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>

    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Add User</h1>
            </div>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </p>

            <br />
            <br />
            <div class="registration-subtitle">
                <h4>Active Subscriptions:</h4>
            </div>

            <div id="activeSubscriptiondv">
            </div>
         
            <asp:HiddenField ID="HdnSubscription" runat="server" />
            <div class="panel-group pt15">
                <div class="panel panel-default gray-bg cursorPointer" id="PriceButton">
                    <div class="panel-heading collapseHeading" data-toggle="collapse" href="#profile">

                        <div>
                            <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i>
                            Price Details
                            </h4>
                        </div>

                    </div>
                    <div id="profile" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-12 ">
                                <div class="clearfix"></div>
                                <div class="container-fluid mb30">

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="AddUserFee" CssClass=" col-xs-4 pt10">Additional User Fee</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="AddUserFee" ReadOnly="true" CssClass="padding-topManageUser form-control "></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="SubscriptionFee" CssClass=" col-xs-4 pt10">Renewed Fee</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="SubscriptionFee" ReadOnly="true" CssClass="form-control "></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="AddGSTFee" CssClass=" col-xs-4 pt10">Additional GST Fee</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="AddGSTFee" ReadOnly="true" CssClass=" padding-topManageUser form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="SubscriptionGSTFee" CssClass=" col-xs-4 pt10">Renewed GST Fee</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="SubscriptionGSTFee" ReadOnly="true" CssClass=" form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="AddTotalFee" CssClass="col-xs-4 pt10">Total Fee</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="AddTotalFee" ReadOnly="true" CssClass="padding-topManageUser  form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="SubscriptionTotalFee" CssClass=" col-xs-4 pt10">Renewed Total Fee</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="SubscriptionTotalFee" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div class="form-horizontal">
                    <div class="clearfix"></div>
                    <div class="mb10">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" ValidationGroup="AddUserValidations"/>
                        </div>
            <div class="panel-group">
                <div class="panel panel-default gray-bg cursorPointer">
                    <div class="panel-heading collapseHeading" id="profile" data-toggle="collapse" href="#profile2">

                        <div>
                            <h4 class="panel-title"><i class="glyphicon glyphicon-minus"></i>
                              Personal Details
                            </h4>
                        </div>

                    </div>
                    <div id="profile2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-12 ">
                                <div class="clearfix"></div>
                                <div class="container-fluid mb30">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="control-label col-xs-4 pt10">First Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" ValidationGroup="AddUserValidations" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" ValidationGroup="AddUserValidations"
                                                        CssClass="text-danger" ErrorMessage="First Name is required." Display="None" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="LastName" CssClass="control-label col-xs-4 pt10">Last Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="LastName" CssClass="form-control" ValidationGroup="AddUserValidations" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName" ValidationGroup="AddUserValidations"
                                                        CssClass="text-danger" ErrorMessage="Last Name is required." Display="None" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="Email" CssClass="control-label col-xs-4 pt10" ValidationGroup="AddUserValidations">Email Address<span class="mandatory">&nbsp;*</span></asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="Email" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" ValidationGroup="AddUserValidations"
                                                        CssClass="text-danger" ErrorMessage="Email is required." Display="None" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="AddUserValidations"
                                                        ControlToValidate="Email" ErrorMessage="Please enter valid email."
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        CssClass="text-danger" Display="None"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="Mobile" CssClass="control-label col-xs-4 pt10">Mobile</asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="Mobile" CssClass="form-control numeric" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="control-label col-xs-4 pt10">Password<span class="mandatory">&nbsp;*</span></asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" ValidationGroup="AddUserValidations" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" ValidationGroup="AddUserValidations"
                                                        CssClass="text-danger" ErrorMessage="Password is required." Display="None" />
                                                    <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="Password" ValidationGroup="AddUserValidations"
                                                        ValidationExpression="^[a-zA-Z0-9'@&amp;_#.\s]{5,}.\*?$" ErrorMessage="Password must contain minimum 6 characters." CssClass="text-danger" Display="None" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="control-label col-xs-4 pt10">Confirm Password<span class="mandatory">&nbsp;*</span></asp:Label>
                                                <div class="col-xs-8">
                                                    <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" ValidationGroup="AddUserValidations" />
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword" ValidationGroup="AddUserValidations"
                                                        CssClass="text-danger" ErrorMessage="Confirm password is required." Display="None" />
                                                    <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" ValidationGroup="AddUserValidations"
                                                        CssClass="text-danger" ErrorMessage="Password and confirm password do not match." Display="None" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%-- <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="control-label col-xs-3">Address</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Address" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>--%>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label runat="server" AssociatedControlID="ProfilePicture" CssClass="control-label col-xs-4 pt10">Profile Picture</asp:Label>
                                                <div class="col-xs-8 mt5">
                                                    <asp:FileUpload ID="ProfilePicture" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-topManageUser">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-xs-3 col-xs-offset-4">
                                                    <asp:Button runat="server" OnClick="AddUser_Click" Text="Add" ValidationGroup="AddUserValidations" CssClass="btn btn-success btn-block" ID="btnRegister" />
                                                </div>
                                                <div class="col-xs-3">
                                                    <asp:HyperLink runat="server" NavigateUrl="~/ManageUsers/Index.aspx" CssClass="btn btn-default btn-block">
                                        Cancel
                                                    </asp:HyperLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../Scripts/XeroWebApp/AddUser.js"></script>
</asp:Content>