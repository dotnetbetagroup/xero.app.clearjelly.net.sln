﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Privacypolicy.aspx.cs" Inherits="ClearJelly.Web.Privacypolicy" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <link rel="stylesheet" href="Content/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    <script src="Scripts/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <div class="homePage pad-80">
        <h2>Managility Privacy Policy</h2>
        <br/>
        <p>
            At Managility, we take privacy very seriously. We’ve updated our privacy policy (Policy) to ensure that we communicate to You, in the clearest way possible, how we treat personal information. We encourage You to read this Policy carefully. It will help You make informed decisions about sharing Your personal information with us.
        The defined terms in this Policy have the same meaning as in our Terms of Use, which You should read together with this Policy. By accessing our Website and using our Service, You consent to the terms of this Policy and agree to be bound by it and our Terms of Use.
        </p>
    </div>
    <br/>
    <div class="homePage pad-80">
        <h3>Managility collects Your personal information
        </h3>
        <br/>
        <p>
            Managility is a provider of analytics solutions that are smart, flexible and secure and give business owners and their advisors unparalleled insight and modelling options to simulate and plan the future for all aspects of their organisation..
        </p>
        <p>
            The Service involves the storage of Data about a company or individual. That Data can include personal information. “Personal information” is information about an identifiable individual, and may include information such as the individual’s name, email address, telephone number, bank account details, taxation details, and accounting and financial information.
        </p>
        <p>
            Managility may collect personal information directly from You when You:
        </p>
        <ul>
            <li>register to use the Service,</li>
            <li>use the Service,</li>
            <li>post to the Managility Community forum or on our blog,</li>
            <li>contact the Managility support team, and</li>
            <li>visit our Website.</li>
        </ul>
        <p>
            You can always choose not to provide Your personal information to Managility, but it may mean that we are unable to provide You with the Service.
        </p>
    </div>
    <br/>
    <div class="homePage pad-80">
        <h3>Managility may receive personal information from You about others
        </h3><br/>
        <p>
            Through Your use of the Service, Managility may also collect information from You about someone else. If You provide Managility with personal information about someone else, You must ensure that You are authorized to disclose that information to Managility and that, without Managility taking any further steps required by applicable data protection or privacy laws, Managility may collect, use and disclose such information for the purposes described in this Policy.
        </p>
        <p>
            This means that You must take reasonable steps to ensure the individual concerned is aware of and/or consents to the various matters detailed in this Policy, including the fact that their personal information is being collected, the purposes for which that information is being collected, the intended recipients of that information, the individual's right to obtain access to that information, Managility’s identity, and how to contact Managility.
        Where requested to do so by Managility, You must also assist Managility with any requests by the individual to access or update the personal information You have collected from them and entered into the Service.
        </p>
    </div>
    <br/>
    <div class="homePage pad-80">
        <h3>Managility collects, holds, and uses Your personal information for limited purposes
        </h3><br/>
        <p>
            Managility collects Your personal information so that we can provide You with the Service and any related services You may request. In doing so, Managility may use the personal information we have collected from You for purposes related to the Services including to:
        </p>
        <ul>
            <li>verify Your identity,</li>
            <li>administer the Service,</li>
            <li>notify You of new or changed services offered in relation to the Service,</li>
            <li>carry out marketing or training relating to the Service,</li>
            <li>assist with the resolution of technical support issues or other issues relating to the Service,</li>
            <li>comply with laws and regulations in applicable jurisdictions, and</li>
            <li>communicate with You.</li>
        </ul>
        <p>
            By using the Service, You consent to Your personal information being collected, held and used in this way and for any other use You authorize. Managility will only use Your personal information for the purposes described in this Policy or with Your express permission.
        </p>
        <p>
            It is Your responsibility to keep Your password to the Service safe. You should notify us as soon as possible if You become aware of any misuse of Your password, and immediately change your password within the Service or via the <a>“Forgotten Password”</a> process.
        </p>
    </div>
    <br/>
    <div class="homePage pad-80">
        <h3>Managility can aggregate Your non-personally identifiable data
        </h3><br/>
        <p>
            By using the Service, You agree that Managility can access, aggregate and use non-personally identifiable data Managility has collected from You. This data will in no way identify You or any other individual.
        </p>
        <p>
            Managility may use this aggregated non-personally identifiable data to:
        </p>
        <ul>
            <li>assist us to better understand how our customers are using the Service,</li>
            <li>provide our customers with further information regarding the uses and benefits of the Service,</li>
            <li>enhance small business productivity, including by creating useful business insights from that aggregated data and allowing You to benchmark Your business’ performance against that aggregated data, and</li>
            <li>otherwise to improve the Service.</li>
        </ul>
    </div>
    <br/>
    <div class="homePage pad-80">
        <h3>Managility holds your personal information by default on servers located in Australia. On request we are able to physically store information at servers in different jurisdictions.
        </h3><br/>
        <p>
            All Data, including personal and non-personal information, that is entered into the Service by You, or automatically imported on Your instruction, is transferred to Managility’s servers as a function of transmission across the Internet. By using the Service, You consent to Your personal information being transferred to our servers as set out in this Policy.
        </p>
        <p>
            Currently our servers are located in Australia, primarily by Microsoft Inc. (Microsoft), and Your personal information will be routed through, and stored on, those servers as part of the Service. Microsoft complies with relevant aspects of the U.S.-EU Safe Harbor Framework and has certified that it adheres to relevant Safe Harbor Privacy Principles. If the location of our servers change in the future, we will update this Policy. You should review our Policy regularly to keep informed of any updates.
        </p>
        <p>
            By providing Your personal information to Managility, You consent to Managility storing Your personal information on servers hosted in Australia. While Your personal information will be stored on servers located in Australia, it will remain within Managility’s effective control at all times. The server host’s role is limited to providing a hosting and storage service to Managility, and we’ve taken steps to ensure that our server hosts do not have access to, and use the necessary level of protection for, Your personal information.
        </p>
    </div><br/>
    <div class="clearfix"></div>
    <div class="homePage pad-80">
        <h3>Managility takes steps to protect your personal information
        </h3><br/>
        <p>
            Managility is committed to protecting the security of Your personal information and we take all reasonable precautions to protect it from unauthorized access, modification or disclosure. Your personal information is stored on secure servers that have SSL Certificates issued by leading certificate authoritiy GeoTrust, and all Data transferred between You and the Service is encrypted.
        </p>
        <p>
            However, the Internet is not in itself a secure environment and we cannot give an absolute assurance that Your information will be secure at all times. Transmission of personal information over the Internet is at Your own risk and You should only enter, or instruct the entering of, personal information to the Service within a secure environment.
        </p>
        <p>
            We will advise You at the first reasonable opportunity upon discovering or being advised of a security breach where Your personal information is lost, stolen, accessed, used, disclosed, copied, modified, or disposed of by any unauthorized persons or in any unauthorized manner.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>Managility only discloses Your Personal Information in limited circumstances
        </h3><br/>
        <p>
            Managility will only disclose the personal information You have provided to us to entities outside the Managility group of companies if it is necessary and appropriate to facilitate the purpose for which Your personal information was collected pursuant to this Policy, including the provision of the Service.
        </p>
        <p>
            Managility will not otherwise disclose Your personal information to a third party unless You have provided Your express consent. However, You should be aware that Managility may be required to disclose Your personal information without Your consent in order to comply with any court orders, subpoenas, or other legal process or investigation including by tax authorities, if such disclosure is required by law. Where possible and appropriate, we will notify You if we are required by law to disclose Your personal information.
        </p>
        <p>
            The third parties who host our servers do not control, and are not permitted to access or use Your personal information except for the limited purpose of storing the information. This means that, for the purposes of Australian privacy legislation and Australian users of the Service, Managility does not currently “disclose” personal information to third parties located overseas.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>Managility does not store Your credit card details
        </h3><br/>
        <p>
            If You choose to pay for the Service by credit card, Your credit card details are not stored by the Service and cannot be accessed by Managility staff. Your credit card details are encrypted and securely stored by PayPal Inc.  to enable Managility to automatically bill your credit card on a recurring basis.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>You may request access to Your personal information
        </h3>      <br/>
        <p>
            It is Your responsibility to ensure that the personal information You provide to us is accurate, complete and up-to-date. You may request access to the information we hold about You, or request that we update or correct any personal information we hold about You, by setting out Your request in writing and sending it to us at support@Managility.com.au
        </p>
        <p>
            Managility will process Your request as soon as reasonably practicable, provided we are not otherwise prevented from doing so on legal grounds. If we are unable to meet Your request, we will let you know why. For example, it may be necessary for us to deny Your request if it would have an unreasonable impact on the privacy or affairs of other individuals, or if it is not reasonable and practicable for us to process Your request in the manner You have requested. In some circumstances, it may be necessary for us to seek to arrange access to Your personal information through a mutually agreed intermediary (for example, the Subscriber).
        </p>
        <p>
            We’ll only keep Your personal information for as long as we require it for the purposes of providing You with the Service. However, we may also be required to keep some of Your personal information for specified periods of time, for example under certain laws relating to corporations, money laundering, and financial reporting legislation.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>Managility uses cookies
        </h3><br/>
        <p>
            In providing the Service, Managility utilizes "cookies". A cookie is a small text file that is stored on Your computer for record-keeping purposes. A cookie does not identify You personally or contain any other information about You but it does identify Your computer.
        </p>
        <p>
            We and some of our affiliates and third-party service providers may use a combination of “persistent cookies” (cookies that remain on Your hard drive for an extended period of time) and “session ID cookies” (cookies that expire when You close Your browser) on the Website to, for example, track overall site usage, and track and report on Your use and interaction with ad impressions and ad services.
        </p>
        <p>
            You can set your browser to notify You when You receive a cookie so that You will have an opportunity to either accept or reject it in each instance. However, You should note that refusing cookies may have a negative impact on the functionality and usability of the Website.
        </p>
        <p>
            We do not respond to or honor “Do Not Track” requests at this time.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>You can opt-out of any email communications
        </h3><br/>
        <p>
            Managility sends billing information, product information, Service updates and Service notifications to You via email. Our emails will contain clear and obvious instructions describing how You can choose to be removed from any mailing list not essential to the Service. Managility will remove You at Your request.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>You are responsible for transfer of Your data to third-party applications
        </h3><br/>
        <p>
            The Service may allow You, the Subscriber, or another Invited User within the relevant subscription to the Service to transfer Data, including Your personal information, electronically to and from third-party applications. Managility has no control over, and takes no responsibility for, the privacy practices or content of these applications. You are responsible for checking the privacy policy of any such applications so that You can be informed of how they will handle personal information.
        </p>
    </div><br/>
    <div class="homePage pad-80">
        <h3>Managility has a privacy complaints process
        </h3><br/>
        <p>
            If You wish to complain about how we have handled Your personal information, please provide our Privacy Officer with full details of Your complaint and any supporting documentation:
        </p>
        <ul>
            <li>by e-mail at support@Managility.com.au, or</li>
            <li>by letter to The Privacy Officer, Managility Pty Ltd, Level 10, 65 York St, Sydney NSW 2000.</li>
        </ul>
        Our Privacy Officer will endeavor to:
    <ul>
        <li>provide an initial response to Your query or complaint within 10 business days, and</li>
        <li>investigate and attempt to resolve Your query or complaint within 30 business days or such longer period as is necessary and notified to you by our Privacy Officer.</li>
    </ul>
    </div><br/>
    <div class="homePage pad-80">
        <h3>This policy may be updated from time to time
        </h3><br/>
        <p>
            Managility reserves the right to change this Policy at any time, and any amended Policy is effective upon posting to this Website. Managility will make every effort to communicate any significant changes to You via email or notification via the Service. Your continued use of the Service will be deemed acceptance of any amended Policy.
        </p>
    </div><br/>
    <script type="text/javascript" src="Scripts/XeroWebApp/Index.js">    </script>
    <style type="text/css">
        li h4 {
            padding-top: 10px;
        }

        li .prettyPhotoCaption {
            padding-top: 5px;
            margin-left: 20px;
        }

        li.detailcontent {
            margin-left: 20px;
        }
    </style>
</asp:Content>

