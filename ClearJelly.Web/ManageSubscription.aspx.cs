﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using ClearJelly.ViewModels.Account;
using ClearJelly.Entities;
using PaypalIntegration.PayPallHelper;
using PaypalIntegration;

namespace ClearJelly.Web
{
    public partial class ManageSubscription : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        protected List<SubscriptionTypeViewModel> _subscriptionTypes = new List<SubscriptionTypeViewModel>();
        private static NLog.Logger _logger;
        public ManageSubscription()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    _logger.Info((Session["Jelly_user"] ?? "") + " ~Redirecting to ManageSubscription as the subscriptoin is not valid, or user is not logged in.");
                    Response.Redirect("~/Account/Login.aspx?RedirectUrl=ManageSubscription", false);
                }
                if (!IsPostBack)
                {
                    //if (Session["IsAdmin"] != null && !Convert.ToBoolean(Session["IsAdmin"]))
                    //    Response.Redirect("~/Account/Login.aspx", false);

                    var token = Request.QueryString["token"];
                    var payerId = Request.QueryString["PayerID"];
                    _logger.Info((Session["Jelly_user"] ?? "") + " ~token=" + token + ", payerId=" + payerId);

                    if (!string.IsNullOrEmpty(token))
                    {
                        if (!string.IsNullOrEmpty(payerId) && Session["CompanySubscriptionId"] != null)
                        {
                            int companySubscriptionId;
                            var result = int.TryParse(Session["CompanySubscriptionId"].ToString(), out companySubscriptionId);
                            if (!result)
                            {
                                throw new InvalidOperationException();
                            }
                            var user = GetCurrentUser(Session["Jelly_user"].ToString());
                            var companySubscription = _clearJellyContext.CompanySubscriptions.Single(x => x.CompanySubscriptionId == companySubscriptionId);
                            _logger.Info((Session["Jelly_user"] ?? "") + " ~Creating Recurring Payment Profile");
                            var recurringProfileId = StartRecurringPayment(token, companySubscription);
                            _logger.Info((Session["Jelly_user"] ?? "") + " ~Recurring ProfileId=" + recurringProfileId);
                            _logger.Info((Session["Jelly_user"] ?? "") + " ~Updating paypal details in db");
                            UpdatePaypalDetails(user, token, payerId, recurringProfileId, companySubscription);
                            _logger.Info((Session["Jelly_user"] ?? "") + " ~Updated paypal details in db");
                            //Now use can have multiple subscription..
                            //DeactiveOtherPlans(companySubscription);

                            if (!bool.Parse(Common.Common.GetIpnInclude()))
                            {
                                if (!string.IsNullOrEmpty(recurringProfileId))
                                {
                                    ActivatePlan(companySubscription);
                                }
                            }
                            Response.Redirect("~/Home.aspx?status=true", false);
                            _logger.Info((Session["Jelly_user"] ?? "") + " ~Paypal token work done.");
                        }
                        else
                        {
                            ErrorMessage.Text = "Your request could not be completed at this time. Please try again later.";
                        }
                        Session.Remove("CompanySubscriptionId");
                    }
                }
                ClientScript.RegisterClientScriptBlock(GetType(),
                 "isPostBack",
                 String.Format("var isPostBack = {0};", IsPostBack.ToString().ToLower()),
                 true);
                var currentUser = GetCurrentUser(Session["Jelly_user"].ToString());
                var currentSubscription = GetCurrentSubscription(currentUser);
                _subscriptionTypes = GetSubscriptionPlans(currentSubscription);
                SetCurrentSubscriptionPlanValues(currentSubscription, currentUser);
                var currentDate = DateTime.Now.Date;
                if (
                    _clearJellyContext.CompanySubscriptions.Count(
                        x =>
                            x.CompanyId == currentUser.CompanyId && x.IsActive &&
                            (!x.EndDate.HasValue || x.EndDate.Value >= currentDate)) == 0)
                {
                    SubscriptionExpiration.Text = "You do not have any active subscriptions. To continue using ClearJelly services, please subscribe to a plan from the below subscriptions. If you have already purchased please wait untill payment is clear.";
                }
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription Page_Load event completed :" + ex.Message, ex);
            }
        }

        private void SetCurrentSubscriptionPlanValues(CompanySubscription currentSubscription, User currentUser)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription SetCurrentSubscriptionPlanValues event started.");

                if (!IsPostBack)
                {
                    var country = currentSubscription.Company != null ? (_clearJellyContext.Countries.FirstOrDefault(x => x.CountryId == currentSubscription.Company.CountryId)) : (_clearJellyContext.Countries.FirstOrDefault(x => x.CountryId == currentUser.Company.CountryId));
                    HdnGSTRate.Value = country != null ? country.GSTRate.ToString() : null;
                    HdnQuantity.Value = currentSubscription.Quantity.ToString();
                    HdnAdditionalUser.Value = currentSubscription.AdditionalUsers.ToString();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription SetCurrentSubscriptionPlanValues event completed :" + ex.Message, ex);
                throw;
            }
        }

        private User GetCurrentUser(string email)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription GetCurrentUser event started.");
                return _clearJellyContext.Users.Single(x => x.Email == email && x.IsActive && !x.IsDeleted);
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription GetCurrentUser event completed :" + ex.Message, ex);
                throw;
            }
        }


        private CompanySubscription GetCurrentSubscription(User currentUser)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription GetCurrentSubscription event started.");
                var currentSubscription = _clearJellyContext.CompanySubscriptions.Where(x => x.CompanyId == currentUser.CompanyId)
                                          .OrderByDescending(x => x.IsActive).ThenByDescending(x => x.EndDate ?? DateTime.MaxValue).FirstOrDefault();
                if (currentSubscription == null)
                {
                    return new CompanySubscription();
                }
                return currentSubscription;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription GetCurrentSubscription event completed :" + ex.Message, ex);
                throw;
            }

        }

        private List<SubscriptionTypeViewModel> GetSubscriptionPlans(CompanySubscription currentSubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription GetSubscriptionPlans event started.");

                return _clearJellyContext.SubscriptionTypes.Where(a => a.IsActive).OrderBy(x => x.MonthlyFee).Select(x => new SubscriptionTypeViewModel
                {
                    Description = x.Description,
                    SubscriptionTypeId = x.SubscriptionTypeId,
                    MonthlyFee = x.MonthlyFee,
                    TypeName = x.TypeName,
                    AdditionalUserFee = x.AdditionalUserFee,
                    Entities = x.Entities,
                    YearlyContractFee = x.YearlyContractFee,
                    IsCurrentSubscription = x.SubscriptionTypeId == currentSubscription.SubscriptionTypeId,
                    IsYearlySubscription = currentSubscription.IsYearlySubscription,
                    Quantity = (currentSubscription.SubscriptionTypeId == x.SubscriptionTypeId) ? currentSubscription.Quantity : 1
                }).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription GetSubscriptionPlans event completed :" + ex.Message, ex);
                throw;
            }
        }

        private string StartRecurringPayment(string token, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription StartRecurringPayment event started.");


                //GetExpressCheckoutDetails 
                var exp = new PayPalExpressCheckout();
                var paymentStartDate = DateTime.Now.Date;
                decimal billingAmount;
                decimal grossAmount;
                decimal gstAmount;
                if (companySubscription.IsYearlySubscription)
                {
                    //grossAmount = companySubscription.SubscriptionType.YearlyContractFee * companySubscription.Quantity +
                    //              companySubscription.AdditionalUsers * companySubscription.SubscriptionType.AdditionalUserFee;
                    grossAmount = companySubscription.SubscriptionType.YearlyContractFee * companySubscription.Quantity;
                    gstAmount = Math.Round(grossAmount * companySubscription.Company.Country.GSTRate / 100, 2);
                    billingAmount = grossAmount + gstAmount;
                }
                else
                {
                    //grossAmount = companySubscription.SubscriptionType.MonthlyFee * companySubscription.Quantity +
                    //    companySubscription.AdditionalUsers * companySubscription.SubscriptionType.AdditionalUserFee;
                    grossAmount = companySubscription.SubscriptionType.MonthlyFee * companySubscription.Quantity;

                    gstAmount = Math.Round(grossAmount * companySubscription.Company.Country.GSTRate / 100, 2);
                    billingAmount = grossAmount + gstAmount;
                }
                var reecurringProfile = new RecurringProfile()
                {
                    Amount = billingAmount,
                    BillingFrequency = 1,
                    BillingPeriod = Common.Common.GetBillingPeriodPaypal().ToLower() == "day" ? PayPalEnums.RecurringPeriod.Day : PayPalEnums.RecurringPeriod.Month,
                    //BillingPeriod = PayPalEnums.RecurringPeriod.Week,
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    StartDate = paymentStartDate,
                    ItemName = companySubscription.SubscriptionType.TypeName.Trim(),
                    Description = companySubscription.SubscriptionType.TypeName.Trim()
                };
                var resRecurring = exp.CreateRecurringPaymentsProfile(reecurringProfile, token);
                if (resRecurring.IsSystemFailure)
                {
                    Response.Redirect("~/Home.aspx", false);
                }
                return resRecurring.ProfileId;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription StartRecurringPayment event completed :" + ex.Message, ex);
                throw;
            }
        }

        private int UpdatePaypalDetails(User user, string token, string payerId, string recurringProfileId, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription UpdatePaypalDetails event started.");
                user.PaypalToken = token;
                user.PaypalPayerId = payerId;
                companySubscription.RecurringProfileId = recurringProfileId;
                _clearJellyContext.SaveChanges();
                return user.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription UpdatePaypalDetails event completed :" + ex.Message, ex);
                throw;
            }
        }

        private bool DeactiveOtherPlans(CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription DeactiveOtherPlans event started.");
                var subscription = _clearJellyContext.CompanySubscriptions
                    .FirstOrDefault(x => x.CompanySubscriptionId == companySubscription.CompanySubscriptionId);

                if (!subscription.EndDate.HasValue || subscription.EndDate.Value >= DateTime.Now.Date)
                {
                    subscription.EndDate = DateTime.Now;
                }
                if (!string.IsNullOrEmpty(subscription.RecurringProfileId) && subscription.IsActive)
                {
                    var expChkt = new PayPalExpressCheckout();
                    //Delete Recurring Payment profile
                    expChkt.ManageRecurringPaymentsProfileStatus(subscription.RecurringProfileId, PayPalEnums.RecurringProfileAction.Cancel);
                }
                subscription.IsActive = false;

                return _clearJellyContext.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription DeactiveOtherPlans event completed :" + ex.Message, ex);
                throw;
            }
        }

        private void ActivatePlan(CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription ActivatePlan event started.");
                if (companySubscription != null)
                {
                    Session["IsSubscriptionExpired"] = false;
                    companySubscription.IsActive = true;
                    companySubscription.EndDate = null;
                    _clearJellyContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription ActivatePlan event completed :" + ex.Message, ex);
            }
        }

        protected void BuyNow_Click(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription BuyNow_Click event started.");
                var email = Session["Jelly_user"].ToString();
                var currentUser = GetCurrentUser(email);
                var currentSubscription = GetCurrentSubscription(currentUser);
                var companySubscriptionId = addCompanySubscription(currentUser, currentSubscription);
                SubscribePlan(currentUser.UserId, companySubscriptionId);

            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription BuyNow_Click event completed :" + ex.Message, ex);
            }
        }

        private int addCompanySubscription(User currentUser, CompanySubscription currentSubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription addCompanySubscription event started.");
                int subscriptionTypeId;
                var result = int.TryParse(HdnSubscriptionTypeId.Value, out subscriptionTypeId);
                if (!result)
                {
                    throw new InvalidOperationException();
                }
                var selectedSubscription =
                    _clearJellyContext.SubscriptionTypes.Single(x => x.IsActive && x.SubscriptionTypeId == subscriptionTypeId);
                var companySubscription = new CompanySubscription
                {
                    CompanyId = currentUser.CompanyId,
                    StartDate = DateTime.Now.Date,
                    EndDate = null,
                    SubscriptionTypeId = subscriptionTypeId,
                    //AdditionalUsers = currentSubscription.AdditionalUsers,
                    AdditionalUsers = 0,
                    Quantity = Convert.ToInt32(HdnQuantity.Value),
                    IsYearlySubscription = HdnSubscriptionMonths.Value == "12"
                };
                _clearJellyContext.CompanySubscriptions.Add(companySubscription);
                _clearJellyContext.SaveChanges();
                return companySubscription.CompanySubscriptionId;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription addCompanySubscription event completed :" + ex.Message, ex);
                throw;
            }
        }

        private void SubscribePlan(int userId, int companySubscriptionId)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription SubscribePlan event started.");

                int subscriptionTypeId;
                var result = int.TryParse(HdnSubscriptionTypeId.Value, out subscriptionTypeId);
                if (!result)
                {
                    throw new InvalidOperationException();
                }
                var subscriptionType = GetSubscriptionTypeById(subscriptionTypeId);
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Calling PaypalCheckout.");
                PaypalCheckout(subscriptionType, companySubscriptionId);
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription SubscribePlan event ended.");
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription SubscribePlan event completed :" + ex.Message, ex);

                throw;
            }
        }

        public SubscriptionTypeViewModel GetSubscriptionTypeById(int subscriptionTypeId)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription GetSubscriptionTypeById event started.");
                return _clearJellyContext.SubscriptionTypes.Where(x => x.IsActive && x.SubscriptionTypeId == subscriptionTypeId)
                    .Select(x => new SubscriptionTypeViewModel
                    {
                        Description = x.Description,
                        MonthlyFee = x.MonthlyFee,
                        YearlyContractFee = x.YearlyContractFee,
                        TypeName = x.TypeName,
                        SubscriptionTypeId = x.SubscriptionTypeId
                    }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription GetSubscriptionTypeById event completed :" + ex.Message, ex);
                throw;
            }
        }

        private void PaypalCheckout(SubscriptionTypeViewModel subscriptionType, int companySubscriptionId)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription PaypalCheckout event started.");

                var expChkt = new PayPalExpressCheckout();
                var orderDetailItem = new List<OrderDetailsItem>();
                var currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                Decimal orderTotal;
                var result = Decimal.TryParse(HdnNetAmount.Value, out orderTotal);

                Decimal orderAmount;
                Decimal.TryParse(HdnTotal.Value, out orderAmount);
                Decimal orderGST;
                Decimal.TryParse(HdnGST.Value, out orderGST);

                if (!result)
                {
                    throw new InvalidOperationException();
                }
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = subscriptionType.TypeName.Trim(),
                    IsRecurringPayment = true,
                    UnitPrice = orderAmount,
                    Quantity = 1
                });
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = "GST(10%)",
                    IsRecurringPayment = true,
                    UnitPrice = orderGST,
                    Quantity = 1
                });
                // Get Token
                var reqOrderDetail = new OrderDetails()
                {
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    DisplayShippingAddress = PayPalEnums.PayPalNoShipping.DoNotDisplay,
                    OrderTotal = orderTotal,
                    Items = orderDetailItem
                };
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Calling SetExpressCheckout.");
                var resToken = expChkt.SetExpressCheckout(reqOrderDetail, currentUrl, currentUrl);

                if (!resToken.IsSystemFailure)
                {
                    Session["CompanySubscriptionId"] = companySubscriptionId;
                }
                else
                {
                    _logger.Info((Session["Jelly_user"] ?? "") + " ~Paypal Response: " + resToken.FailureMessage);
                }
                Response.Redirect(resToken.RedirectUrl, false);
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription PaypalCheckout event ended.");
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageSubscription PaypalCheckout event completed :" + ex.Message, ex);
            }
        }

        #region Multiple Subscriptions
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<ActiveSubscriptionsViewModel> GetActiveSubscriptions()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription GetActiveSubscriptions event started.");

                var currentPage = new ManageSubscription();
                return currentPage.GetActiveSubscriptionData();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + "Execution of ManageSubscription GetSubscriptionPlans event completed :" + ex.Message, ex);
                throw;
            }
        }

        private List<ActiveSubscriptionsViewModel> GetActiveSubscriptionData()
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription GetActiveSubscriptionData event started.");

                var user = GetCurrentUser(Session["Jelly_user"].ToString());
                var currentDate = DateTime.Now.Date;
                var companySubscriptions = user.Company.CompanySubscriptions.Where(a => a.IsActive && (a.EndDate == null || a.EndDate >= currentDate));
                return companySubscriptions.Select(x => new ActiveSubscriptionsViewModel
                {
                    CompanySubscriptionId = x.CompanySubscriptionId,
                    SubscriptionTypeId = x.SubscriptionTypeId,
                    AdditionalUsers = x.AdditionalUsers,
                    Description = x.SubscriptionType != null ? x.SubscriptionType.Description : null,
                    TypeName = x.SubscriptionType != null ? x.SubscriptionType.TypeName : null,
                    Quantity = x.Quantity,
                    IsYearly = x.IsYearlySubscription,
                    StartDate = x.StartDate.ToShortDateString()

                }).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + "Execution of ManageSubscription GetActiveSubscriptionData event completed :" + ex.Message, ex);
                throw;
            }

        }

        [WebMethod]
        public static bool CancelSubscription(int companySubscriptionId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UnblockUser CancelSubscription  event started.");
                var currentPage = new ManageSubscription();
                return currentPage.CancelCompanySubscription(companySubscriptionId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription CancelSubscription event completed.", ex);
                throw ex;
            }
        }

        private bool CancelCompanySubscription(int companySubscriptionId)
        {
            try
            {
                var companySubscription = _clearJellyContext.CompanySubscriptions.Single(x => x.CompanySubscriptionId == companySubscriptionId);
                var currentDate = DateTime.Now;

                var returnStatus = DeactiveOtherPlans(companySubscription);
                DeleteChildUser(companySubscriptionId);
                var isSubscriptionExpired = _clearJellyContext.CompanySubscriptions.Count(x =>
                           x.CompanyId == companySubscription.CompanyId && x.IsActive &&
                           (!x.EndDate.HasValue || x.EndDate.Value >= currentDate)) == 0;
                Session["IsSubscriptionExpired"] = isSubscriptionExpired;
                return returnStatus;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription CancelCompanySubscription event completed.", ex);
                throw;
            }
        }

        private void DeleteChildUser(int companySubscriptionId)
        {
            try
            {
                //remove all child users who have this subscription..
                var additionalUserSubscription = _clearJellyContext.AdditionalUsers.Where(a => a.CompanySubscriptionId == companySubscriptionId).ToList();
                foreach (var additionalUser in additionalUserSubscription)
                {
                    var instantpaymentAdditionalUser = _clearJellyContext.InstantPayments.FirstOrDefault(x => x.AdditionalUserId == additionalUser.AdditionalUserId);
                    if (instantpaymentAdditionalUser != null)
                    {
                        _clearJellyContext.InstantPayments.Remove(instantpaymentAdditionalUser);
                    }
                    var user = additionalUser.User;
                    _clearJellyContext.AdditionalUsers.Remove(additionalUser);
                    _clearJellyContext.SaveChanges();

                    //if child user have no any subscription left than remove user.
                    var AnyUserSubscription = _clearJellyContext.AdditionalUsers.Any(a => a.UserId == user.UserId);
                    if (!AnyUserSubscription)
                    {
                        _clearJellyContext.Users.Remove(user);
                    }
                }
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageSubscription DeleteChildUser event completed.", ex);
                throw;
            }
        }

        #endregion
    }
}