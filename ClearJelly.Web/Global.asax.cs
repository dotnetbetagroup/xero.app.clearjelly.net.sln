﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;

namespace ClearJelly.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterCustomRoutes(RouteTable.Routes);
        }
        protected void Application_Error()
        {
            Exception lastException = Server.GetLastError();
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Fatal(lastException);
            var page = HttpContext.Current.Handler as Page;
            //if (page != null)
            //{
            //    var currentPage = page.AppRelativeVirtualPath;
            //    if (currentPage.ToLower().Contains("admin"))
            //    {
            //        HttpContext.Current.Response.Redirect("~/Admin/Error.aspx");
            //    }
            //    else
            //    {
            //        HttpContext.Current.Response.Redirect("~/Error.aspx");
            //    }
            //}
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            //except edit template cos allow to add special character in email template.

            var allowedIPs = ConfigurationManager.AppSettings["allowedIPs"].ToString();
            if (ConfigurationManager.AppSettings["MaintenanceMode"] == "true")
            {
                if (!Request.IsLocal && allowedIPs != (Request.UserHostAddress))
                {
                    HttpContext.Current.Response.Redirect("~/SiteUnderMaintainence.html");
                    HttpContext.Current.RewritePath("SiteUnderMaintenance.aspx");
                }
            }
            else
            {
                if (!Request.Path.Contains("/Admin/EditTemplate"))
                {
                    string[] formKeysExceptions = new[] { "__VIEWSTATE", "__EVENTVALIDATION", "__OK" };
                    foreach (string key in Request.QueryString)
                        Common.Common.CheckInput(Request.QueryString[key]);
                    foreach (string key in Request.Form.AllKeys.Where(k => !formKeysExceptions.Contains(k)))
                        Common.Common.CheckInput(Request.Form[key]);
                    foreach (string key in Request.Cookies)
                        Common.Common.CheckInput(Request.Cookies[key].Value);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    if (HttpContext.Current.Request.IsSecureConnection.Equals(false)
                        && HttpContext.Current.Request.IsLocal.Equals(false)
                        && HttpContext.Current.Request.Url.ToString() != "http://" + Request.ServerVariables["HTTP_HOST"] + "/MaintenancePage.aspx")
                    {
                        Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
                    }
                }
            }
        }

        protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)

        {
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)
            {
                if (Common.Common.CheckSSLCertificateExpiryDate() && HttpContext.Current.Request.Url.ToString() != ("http://" + Request.ServerVariables["HTTP_HOST"] + "/MaintenancePage.aspx"))
                {
                    Response.Redirect("http://" + Request.ServerVariables["HTTP_HOST"] + "/MaintenancePage.aspx");
                }
            }
        }


        protected void Session_End(Object sender, EventArgs e)
        {
            try
            {
                //remove temporary session of current sesion 
                var templateAttachmentFolder = Common.Common.GetEmailAttachmentTempFrom();
                var sessionId = Session.SessionID;
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/" + templateAttachmentFolder + "/");
                var sessionFolder = System.IO.Path.Combine(path + "/" + sessionId);
                bool directoryExists = System.IO.Directory.Exists(sessionFolder);
                if (directoryExists)
                {
                    System.IO.Directory.Delete(sessionFolder, true);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            var currentUrl = HttpContext.Current.Request.Url;
            var url = HttpContext.Current.Request.Url;
            var page = HttpContext.Current.Handler as Page;

            if (page != null)
            {
                string[] exceptPageList = new[] { "~/index.aspx","~/maintenancepage.aspx","~/siteundermaintenance.aspx","~/terms-of-use.aspx","~/privacypolicy.aspx","~/terms_of_use.aspx","~/paypalnotification.aspx", "~/account/login.aspx","~/platformchoices.aspx", "~/admin/index.aspx", "~/account/forgetpassword.aspx",
                    "~/account/resetpassword.aspx", "~/account/register.aspx", "~/account/activateaccount.aspx", "~/account/registrationmsaccount.aspx", "~/account/accountactivation.aspx" ,"~/error.aspx"
                };
                var currentPage = page.AppRelativeVirtualPath;

                if (currentPage.ToLower().Contains("admin"))
                {
                    if (HttpContext.Current.Session != null && page != null && Session["Jelly_Admin_user"] == null && !exceptPageList.Any(a => exceptPageList.Contains(currentPage.ToLower())))
                    {
                        Session["isUserExpired"] = "true";
                        Response.Redirect("/Admin");
                    }
                }
                else
                {
                    if (HttpContext.Current.Session != null && page != null && User == null && !exceptPageList.Any(a => exceptPageList.Contains(currentPage.ToLower())))
                    {
                        Session["isUserExpired"] = "true";
                        Response.Redirect("/Account/Login");
                    }

                }
                //if subscription expired redirect user to managesubscription page.
                if (Session["IsSubscriptionExpired"] != null && Convert.ToBoolean(Session["IsSubscriptionExpired"]))
                {
                    if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                    {
                        if (!currentPage.ToLower().Contains("~/managesubscription.aspx"))
                        {
                            Response.Redirect("~/ManageSubscription.aspx", false);
                        }
                    }
                }
            }
        }



        protected void Session_Start(object sender, EventArgs e)
        {

        }

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{

        //}

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        //protected void Session_End(object sender, EventArgs e)
        //{

        //}

        protected void Application_End(object sender, EventArgs e)
        {

        }

        void RegisterCustomRoutes(RouteCollection routes)
        {

        }
    }
}