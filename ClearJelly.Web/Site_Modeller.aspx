﻿
<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Site_Modeller.aspx.cs" Inherits="ClearJelly.Web.Site_Modeller" %>

<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="ClearJelly.Web.Common" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>
        <%="" %>
        <% if (string.IsNullOrEmpty(Page.Title))
               { %>
        <%: Page.Title %>  Agility Console
        <% }
            else
            { %>
        <%: Page.Title %>

        <% } %></title>

    <asp:PlaceHolder runat="server">
        <%: Styles.Render("~/bundles/bootstrapcss") %>
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <%--<link href="/Content/jquery-ui.css" rel="stylesheet">--%>
    <script type="text/javascript" src="https://assets.freshdesk.com/widget/freshwidget.js"></script>

</head>
<body>

    <form runat="server">

        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="row">
                    <div class="navbar-header">
                        <asp:ScriptManager runat="server">
                            <Scripts>
                                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                                <%--Framework Scripts--%>
                                <asp:ScriptReference Name="MsAjaxBundle" />
                                <asp:ScriptReference Name="jquery" />
                                <asp:ScriptReference Name="bootstrap" />
                                <asp:ScriptReference Name="respond" />
                                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                                <asp:ScriptReference Name="WebFormsBundle" />
                                <%--Site Scripts--%>
                                <asp:ScriptReference Name="common" />
                            </Scripts>
                        </asp:ScriptManager>
                        <%: Scripts.Render("~/bundles/bootstrap") %>
                        <%--<script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>--%>
                        <%--  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>--%>
                        <a runat="server" href="~/Index.aspx" class="navbar-brand">
                            <img height="100" width="100" src="../Images/Logo.png" />
                        </a>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a runat="server" href="/Modeller.aspx">Modeller</a></li>
                                <li><a runat="server" href="#">About</a></li>
                                <li><a runat="server" href="#">Contact</a></li>
                                <li><a runat="server" href="https://managility.freshdesk.com">Support</a></li>
                                <li><a runat="server" href="/Subscription.aspx">Subscription</a></li>
                                <li><a runat="server" href="/ManageIPRequest.aspx">Safe IP</a></li>
                                <li><a runat="server" href="/Downloads.aspx">Download</a></li>
                            </ul>
                            <%--  <asp:LoginView runat="server" ViewStateMode="Disabled">
                        <AnonymousTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Register">Register</a></li>
                                <li><a runat="server" href="~/Account/Login">Log in</a></li>
                            </ul>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Manage" title="Manage your account">Hello, <%: Context.User.Identity.GetUserName()  %>!</a></li>
                                <li>
                                    <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/" OnLoggingOut="Unnamed_LoggingOut" />
                                </li>
                            </ul>
                        </LoggedInTemplate>
                    </asp:LoginView>--%>
                            <asp:Panel ID="UserMenu" runat="server" Visible="False">
                                <div class="user-box">

                                    <div class="user-text">
                                        <%--<span class="profile-ava">
                                            <img alt="" style="width: 50px" src='<%=Session["UserImageSrc"]%>' class="img-responsive">
                                        </span>
                                        <a href="javascript:void(0)" class="user-name"><%=Session["User_fullname"]%>
                                        </a>--%>
                                    </div>
                                    <div class="user-drop-box">
                                        <asp:Panel ID="ManageUserLinkAdmin" runat="server" Visible="False">
                                            <ul>
                                                <li>
                                                    <div class="user-drop-fild">
                                                        <div class="user-drop-text">
                                                            <div class="user-drop-name">
                                                                <%--<span style="word-wrap: break-word;"><%=Session["User_fullname"] %>
                                                                </span>
                                                                <span style="font-size: 70%; word-wrap: break-word;"><%=Session["Jelly_user"] %>
                                                                </span>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="user-drop-fild">
                                                        <a runat="server" href="~/EditProfile.aspx">
                                                            <div class="user-drop-icon">
                                                                <img runat="server" src="~/Images/edit-img.png" />
                                                            </div>
                                                            <span>Edit Profile</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="user-drop-fild">
                                                        <a runat="server" href="~/ManageUsers/Index.aspx">
                                                            <div class="user-drop-icon">
                                                                <img runat="server" src="~/Images/edit-img.png" />
                                                            </div>
                                                            <span>Manage Users</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="user-drop-fild">
                                                        <asp:LinkButton runat="server" OnClick="LogOut_Click">
                                                <div class="user-drop-icon">
                                                    <img runat="server" src="~/Images/sign-out.png" />
                                                </div>
                                                <span>Logout</span>
                                                        </asp:LinkButton>
                                                    </div>
                                                </li>
                                            </ul>
                                        </asp:Panel>
                                        <asp:Panel ID="ManageUserLinkUser" runat="server" Visible="False">
                                            <ul>

                                                <li>
                                                    <div class="user-drop-fild">
                                                        <div class="user-drop-text">
                                                            <div class="user-drop-name">
                                                                <%--<span style="word-wrap: break-word;"><%=Session["User_fullname"] %>
                                                                </span>
                                                                <span style="font-size: 70%; word-wrap: break-word;"><%=Session["Jelly_user"] %>
                                                                </span>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="user-drop-fild">
                                                        <a runat="server" href="~/EditProfile.aspx">
                                                            <div class="user-drop-icon">
                                                                <img runat="server" src="~/Images/edit-img.png" />
                                                            </div>
                                                            <span>Edit Profile</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="user-drop-fild">
                                                        <asp:LinkButton runat="server" OnClick="LogOut_Click">
                                                <div class="user-drop-icon">
                                                    <img runat="server" src="~/Images/sign-out.png" />
                                                </div>
                                                <span>Logout</span>
                                                        </asp:LinkButton>
                                                    </div>
                                                </li>
                                            </ul>
                                        </asp:Panel>
                                    </div>
                                    <img src="../Images/Thumbnails/Agility_logo_1.png" class="right-logo" width="80" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="LoginMenu" runat="server" Visible="False">
                                <div class="user-box">
                                    <div class="user-text">
                                        <a runat="server" id="loginButton" class="login-link">
                                            <img runat="server" src="~/Images/Login_Logo.png" width="14" height="14" />
                                            Login
                                        </a>
                                    </div>
                                    <div class="user-text">
                                        <a runat="server" id="registerButton" class="login-link">Register
                                        </a>
                                    </div>
                                    <img src="../Images/Thumbnails/Agility_logo_1.png" class="right-logo" width="80" />
                                </div>

                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="col-md-main-content">
            <div>
                <div class="mrg-b padding-top-20">
                    <div>

                        <asp:ContentPlaceHolder ID="MainContent" runat="server">
                        </asp:ContentPlaceHolder>
                    </div>
                    <hr />

                    <footer>
                        <%-- <asp:Image runat="server" Width="60" Height="60" ImageUrl="~/Images/Logo.jpg" />--%>
                        <div class="col-md-offset-5">
                            <script type="text/javascript">
                                FreshWidget.init("", { "queryString": "&widgetType=popup", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Agility Help Team", "buttonColor": "white", "buttonBg": "#4e5166", "alignment": "4", "offset": "235px", "formHeight": "500px", "url": "https://managility.freshdesk.com" });
                            </script>
                            <a runat="server" href="~/privacypolicy.aspx" target="_blank">Privacy Policy</a>
                            &nbsp;|&nbsp;
                            <asp:HyperLink ID="lnkTerms" runat="server" NavigateUrl="~/terms-of-use.aspx" Target="_blank">
                            Terms of use
                            </asp:HyperLink>
                            <div class="float-right">
                                &copy; <%: DateTime.Now.Year %> Managility Pty Ltd
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade" tabindex="-1" role="dialog" id="registrationPopup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Login to</h4>
                </div>
                <%--    <div class="modal-body">
                    <img width="100" height="100" src="../Images/xero-logo.png" id="ClearJelly" />
                    <img width="100" height="100" src="../Images/Saasu-logo.png" id="Saasu" />
                </div>--%>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script>
        var baseUrl = '@Url.Content("~/")';


    </script>


    <!-- begin olark code -->
    <script data-cfasync="false" type='text/javascript'>
        $('#registerButton').click(function () {
            //$("#poupheading").html("Register to ");
            //$(".modal-body").removeClass('login');
            //$(".modal-body").addClass('register');
            //bindRegisterClickEvent();
            //$('#registrationPopup').modal('show');
            //window.location.href = "../Account/Register.aspx";
            window.location.href = "../Account/Register.aspx";
        });
        $('#loginButton').click(function () {
            //$("#poupheading").html("Login to ");
            //$(".modal-body").addClass('login');
            //$(".modal-body").removeClass('register');
            //bindLoginClickEvent();
            //$('#registrationPopup').modal('show');
            //window.location.href = "../Account/Login.aspx";
            window.location.href = "../Account/Login.aspx";
        });

        function bindLoginClickEvent() {
            $('.login #ClearJelly').click(function () {
                //$('#registrationPopup').modal('hide');
                window.location.href = "../Account/Login.aspx";
            });
            $('.login #Saasu').click(function () {
                $('#registrationPopup').modal('hide');
                window.location.href = "../Account/Login.aspx";
            });
        }

        function bindRegisterClickEvent() {
            $('.register #ClearJelly').click(function () {
                $('#registrationPopup').modal('hide');
                window.location.href = "../Account/Register.aspx";
            });
            $('.register #Saasu').click(function () {
                $('#registrationPopup').modal('hide');
                window.location.href = "../Account/Register.aspx";
            });
        }

        /*<![CDATA[*/window.olark || (function (c) {
            var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load"; var nt = function () {
                f[z] = function () {
                    (a.s = a.s || []).push(arguments)
                }; var a = f[z]._ = {
                }, q = c.methods.length; while (q--) {
                    (function (n) {
                        f[z][n] = function () {
                            f[z]("call", n, arguments)
                        }
                    })(c.methods[q])
                } a.l = c.loader; a.i = nt; a.p = {
                    0: +new Date
                }; a.P = function (u) {
                    a.p[u] = new Date - a.p[0]
                }; function s() {
                    a.P(r); f[z](r)
                } f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s); var ld = function () {
                    function p(hd) {
                        hd = "head"; return ["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
                    } var i = "body", m = d[i]; if (!m) {
                        return setTimeout(ld, 100)
                    } a.P(1); var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o; n.style.display = "none"; m.insertBefore(n, m.firstChild).id = z; b.frameBorder = "0"; b.id = z + "-loader"; if (/MSIE[ ]+6/.test(navigator.userAgent)) {
                        b.src = "javascript:false"
                    } b.allowTransparency = "true"; v[j](b); try {
                        b.contentWindow[g].open()
                    } catch (w) {
                        c[e] = d[e]; o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';"; b[k] = o + "void(0);"
                    } try {
                        var t = b.contentWindow[g]; t.write(p()); t.close()
                    } catch (x) {
                        b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
                    } a.P(2)
                }; ld()
            }; nt()
        })({
            loader: "static.olark.com/jsclient/loader0.js", name: "olark", methods: ["configure", "extend", "declare", "identify"]
        });
        /// custom configuration goes here (www.olark.com/documentation) /
        olark.identify('3121-523-10-4238');/*]]>*/
    </script>
    <noscript><a href="https://www.olark.com/site/3121-523-10-4238/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
    <!-- end olark code -->
</body>
</html>

<div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: #666666; z-index: 30001; opacity: .43; filter: alpha(opacity=70); display: none">
    <p style="position: absolute; top: 30%; left: 45%; color: White;">
        Loading, please wait...<img src="../Images/loading1.gif" />
    </p>
</div>

<%--tracking code--%>
<script>
        (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-74653908-2', 'auto');
        ga('send', 'pageview');
</script>

<script>
    var sqlBlacklist = new Array(<%= ClearJelly.Web.Common.Common.SqlBlackList.Aggregate((a,b)=>"'"+a+"','"+b+"'") %>);
    var sqlBlacklistErrorMessage = "<span class=errorMessage>Please enter a valid value. The following characters are not allowed: (" + sqlBlacklist.join(", ") + ")</span>";

    $(".numeric").bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $(".numeric").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("input").blur(function (e) {
        //var charCode = !e.charCode ? e.which : e.charCode;
        var selectedInput = $(this);
        var thisval = selectedInput.val();

        $(this).parents('form').submit(function (e) {
            var formId = $(selectedInput).parents('form').attr('id');
            if ($("#" + formId + " :input.invalidinput").length > 0) {
                console.log('not allowed');
                e.preventDefault();
            }
        });

        var isvalid = true;
        for (var cnt = 0; cnt < sqlBlacklist.length; cnt++) {
            var specialChar = sqlBlacklist[cnt];
            if (jQuery.inArray(specialChar, thisval) !== -1) {
                isvalid = false;
                if (!($(this).hasClass('invalidinput'))) {
                    $(this).addClass('invalidinput');
                }
                if ($(this).parent().find(".errorMessage").length == 0) {
                    $(this).parent().append(sqlBlacklistErrorMessage);
                }
            }
        }
        if (isvalid) {
            $(this).removeClass('invalidinput');
            $(this).parent().find(".errorMessage").remove();
        }


    });

</script>
