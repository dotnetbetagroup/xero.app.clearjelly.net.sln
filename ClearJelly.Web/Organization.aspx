﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Organization.aspx.cs" Inherits="ClearJelly.Web.Organization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>

    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Manage Organization</h1>
            </div>
            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!&nbsp; </strong><span id="successMsg"></span>
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!&nbsp; </strong>Something went wrong, please try again.
            </div>

            <div id="manageOrganizationdv"></div>

            <script type="text/javascript" src="../Scripts/XeroWebApp/Organization.js"></script>
        </div>
    </div>
</asp:Content>

