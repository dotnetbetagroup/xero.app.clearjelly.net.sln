﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageIPRequest.aspx.designer.cs" Inherits="ClearJelly.Web.ManageIPRequest" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>

    <div class="container-fluid gray-bg pt15 pb15 mb15">
        <div class="row">
            <div class="col-md-12 gray-bg">
                <div class="login-title pb10">
                    <h1>Manage IP Address </h1>
                </div>
                <div class="alert alert-success display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Success!</strong> <span id="SuccessMsg">Success.</span>
                </div>
                <div class="alert alert-danger display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Error!</strong> Some error occured please try after some time.
                </div>
                <p>
                    <asp:Label runat="server" ID="SuccessMessage" Visible="False" Text="Your changes have been saved successfully!" CssClass="successMessage" />
                </p>
                <p class="text-danger">
                    <asp:Literal runat="server" ID="ErrorMessage" />
                </p>
                <div class="row centerLabel">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="currentIP" CssClass="control-label col-xs-3">Current IP</asp:Label>
                            <div class="col-xs-5">
                                <asp:TextBox runat="server" ID="currentIP" disabled="disabled" CssClass="form-control" />
                            </div>
                            <div class="col-xs-3">
                                  <a class="save save btn btn-success btn-block" id="saveButton" runat="server">Add To Safe</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="userIpListdv"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../Scripts/XeroWebApp/UserSafeIp.js"></script>
</asp:Content>
