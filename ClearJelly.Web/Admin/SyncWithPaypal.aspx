﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="SyncWithPaypal.aspx.cs" CodeFile="SyncWithPaypal.aspx.cs" Inherits="ClearJelly.Web.Admin.SyncWithPaypal" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
       <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>

     <%--<asp:LinkButton ID="btnUpdate" runat="server" OnClick="btnSyncPaypal_Click"></asp:LinkButton>--%>
  


    <div class="section">
        <div class="row">
            <div class="col-md-12 gray-bg">
                <div class="login-title pb10">
                    <h1>Paypal Subscription</h1>
                </div>

                <div class="alert alert-success display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Success!&nbsp; </strong><span id="successMsg"></span>
                </div>
                <div class="alert alert-danger display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Error!&nbsp; </strong>Something went wrong, please try again.
                </div>

                <div class="col-md-6 mt15">Last Updated on: <span id="lastUpdatedDate"></span></div>

                <div class="col-md-2 pull-right margin-top-10px mb15"> <button type="button" class="btn btn-success btn-block syncDataButton">Sync With Paypal</button>  </div> 

              <div id="paypalSubscription">   </div>
                </div>
            </div>
        </div>



    <div class="modal fade" tabindex="-1" role="dialog" id="ReccuringProfilePopup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="poupheading">Reccurring Profiles</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                              <div id="HistoryRecurringProfile"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
      <script type="text/javascript" src="../Scripts/XeroWebApp/SyncWithPaypal.js">  </script>
    </asp:Content>


