﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="ManageSampleLinks.aspx.cs" CodeFile="ManageSampleLinks.aspx.cs" Inherits="ClearJelly.Web.Admin.ManageSampleLinks" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Sample Links</h1>
            </div>

            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!</strong> <span id="successMsg">Sample link updated successfully.</span>
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div class="addbuttonDiv">
                <button type="button" id="btnAddNew" class="btn btn-success">
                    <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add New</button>
            </div>
            <div id="bindDatadv">
            </div>
            
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="EditSampleLinkPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Sample Link</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" id="sampleLinkId" />
                                <asp:Label runat="server" AssociatedControlID="customTitle" CssClass="control-label col-xs-4">Title<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-4">
                                    <asp:TextBox runat="server" ID="customTitle" CssClass="form-control" />
                                    <span class="mandatory display-none validationMsgs" id="titleMsg">Please enter title.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DownloadLink" CssClass="control-label col-xs-4">Download Link<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Downloadlink" CssClass="form-control" />
                                    <span class="mandatory display-none validationMsgs" id="downloadMsg">Please enter download link.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/XeroWebApp/ManageSampleLinks.js">
    </script>
</asp:Content>
