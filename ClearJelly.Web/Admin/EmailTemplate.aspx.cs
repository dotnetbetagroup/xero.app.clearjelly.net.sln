﻿using ClearJelly.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.ViewModels.Admin;

namespace ClearJelly.Web.Admin
{
    public partial class EmailTemplate : System.Web.UI.Page
    {

        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

        public EmailTemplate()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
                //remove temporary attachment folder.
                Session.Remove("TempAttachment");
                Session.Remove("deleteDBAttachments");

                if (!IsPostBack)
                {
                    CategoryDropDown.DataSource = GetCategory();
                    CategoryDropDown.DataBind();
                    var isSuccess = Session["isUpdateSuccess"];
                    successDiv.Visible = false;
                    errorDiv.Visible = false;
                    if (isSuccess != null)
                    {
                        if (Convert.ToBoolean(isSuccess))
                        {
                            successDiv.Visible = true;
                        }
                        else
                        {
                            errorDiv.Visible = true;
                        }
                    }
                    else
                    {
                        successDiv.Visible = false;
                        errorDiv.Visible = false;
                    }
                    Session.Remove("isUpdateSuccess");
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate Page_Load event completed.", ex);
            }
        }



        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<EmailTemplateViewModel> GetEmailTemplateList(int categoryId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetEmailTemplateList event started.");
                var currentPage = new EmailTemplate();
                var userList = currentPage.GetEmailTemplates(categoryId);
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetEmailTemplateList event completed.", ex);
                throw;
            }
        }
        private List<DropDownViewModel> GetCategory()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetCategory  event started.");
                var categories = _clearJellyContext.EmailCategories;
                var categoryList = new List<DropDownViewModel>();

                var DefaultData = new DropDownViewModel()
                {
                    Id = 0,
                    Name = "Select All"
                };
                categoryList.Add(DefaultData);

                categoryList.AddRange(categories.Select(a => new DropDownViewModel()
                {
                    Id = a.EmailCategoryId,
                    Name = a.Name
                }).ToList());

                return categoryList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetCategory event completed.", ex);
                throw;
            }
        }

        private List<EmailTemplateViewModel> GetEmailTemplates(int? categoryId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetEmailTemplates  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_Admin_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var emailTemplates = _clearJellyContext.EmailTemplates.Where(a => a.IsActive);
                if (categoryId != 0 && categoryId != null)
                {
                    emailTemplates = emailTemplates.Where(a => a.EmailCategoryId == categoryId);
                }
                var companyUsersData = emailTemplates.Select(x => new EmailTemplateViewModel
                {
                    Category = x.EmailCategory != null ? x.EmailCategory.Name : null,
                    EmailTemplateId = x.EmailTemplateId,
                    IsActive = x.IsActive,
                    Name = x.Name,
                    Subject = x.Subject,
                    TemplateContent = x.TemplateContent,
                    CategoryId = x.EmailCategoryId
                }).ToList();
                return companyUsersData;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetEmailTemplates event completed.", ex);
                throw;
            }
        }
        [WebMethod]
        public static bool Update(int templateId, string subject)
        {
            try
            {
                _logger.Info(" ~Execution of ManageSampleLinks Update event started.");
                var currentPage = new EmailTemplate();
                currentPage.UpdateDashboardLink(templateId, subject);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Home Update event completed.", ex);
                throw ex;
            }
        }

        public bool UpdateDashboardLink(int templateId, string subject)
        {
            try
            {
                _logger.Info(" ~Execution of Home UpdateDashboardLink event started.");
                var dashboardLinks = _clearJellyContext.EmailTemplates.FirstOrDefault(x => x.EmailTemplateId == templateId);
                if (dashboardLinks != null)
                {
                    dashboardLinks.Subject = subject;
                    _clearJellyContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Info(" ~Execution of Home UpdateDashboardLink event started.");
                throw;
            }
        }

    }
}