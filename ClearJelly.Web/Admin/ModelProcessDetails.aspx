﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="ModelProcessDetails.aspx.cs" CodeFile="ModelProcessDetails.aspx.cs" Inherits="ClearJelly.Web.Admin.ModelProcessDetails" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <script src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Model Process</h1>
            </div>
            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!&nbsp; </strong><span id="successMsg"></span>
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!&nbsp; </strong><span id="FailureMsg">Something went wrong, please try again.</span>
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div class="col-md-12 gray-bg padding-top-10">
                <input type="hidden" id="hdnEmailTemplateId" runat="server" />
                <div class="row centerLabel">
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group ">
                            <asp:Label runat="server" AssociatedControlID="FilterDropDown" CssClass="control-label col-xs-3">User </asp:Label>
                            <asp:DropDownList ID="FilterDropDown" CssClass="form-control pointerCursor width200" runat="server" DataTextField="Name" DataValueField="Id">
                            </asp:DropDownList>
                        </div>

                    </div>

                    <div class="col-md-3 col-xs-12">
                        <div class="form-group ">
                            <asp:Label runat="server" AssociatedControlID="OrgDropDown" CssClass="control-label col-xs-3">Organization </asp:Label>
                            <asp:DropDownList ID="OrgDropDown" CssClass="form-control pointerCursor width200" runat="server" DataTextField="Name" DataValueField="Id">
                            </asp:DropDownList>
                        </div>

                    </div>

                    <div class="col-md-2 col-xs-12">
                        <div class="form-group ">
                            <asp:Label runat="server" AssociatedControlID="txtStartDate" CssClass="col-md-12 col-xs-6">Start Date </asp:Label>
                            <div class="col-md-12">
                                <div class="col-md-9 padding0">
                                    <asp:TextBox class="form-control " ID="txtStartDate" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    <img id="imgStartDate" src="../Images/calendar_icon.png"" alt="Smiley face" height="30" width="30">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-xs-12">
                        <div class="form-group ">
                            <asp:Label runat="server" AssociatedControlID="txtEndDate" CssClass="col-md-12 col-xs-6">End Date </asp:Label>
                            <div class="col-md-12">
                                <div class="col-md-9 padding0">
                                    <asp:TextBox class="form-control" ID="txtEndDate" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    <img id="imgEndDate" src="../Images/calendar_icon.png"" alt="Smiley face" height="30" width="30">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-xs-12 marginTop41 pull-right padding-right0">
                        <input type="button" class="btn btn-default btn-block" id="ClearButton" value="Clear">
                    </div>
                    <div class="col-md-1 col-xs-12 marginTop41 pull-right padding-right0">
                        <input type="button" class="btn btn-success btn-block" id="searchModelProces" value="Search">
                    </div>
                </div>
            </div>

            <div id="ModelProcessDiv">
            </div>

        </div>
    </div>

    <div class="modal fade " tabindex="-1" role="dialog" id="ModelProcessPopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Model Process</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="modelProcessInner"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script src="../Scripts/XeroWebApp/AdminModelProcess.js"></script>
   
</asp:Content>
