﻿using ClearJelly.Entities;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.ViewModels.Admin;
using NLog;
using System.Linq;

namespace ClearJelly.Web.Admin
{
    public partial class UserSubscriptions : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static Logger _logger;
        private static string ChosenService = "";

        public UserSubscriptions()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ChosenService = Session["ChosenService"].ToString();
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions Page_Load event completed.", ex);
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UserSubscriptionModel> GetUserList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUserList event started.");
                var currentPage = new UserSubscriptions();
                var userList = currentPage.GetUsers();
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUserList event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UserSubscriptionListModel> GetUserSubscriptionList(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUserList event started.");
                var currentPage = new UserSubscriptions();
                var userList = currentPage.GetUserSubscriptions(userId);
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUserList event completed.", ex);
                throw ex;
            }
        }




        [WebMethod]
        public static bool ActivateUser(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions ActivateUser  event started.");
                return ClearJelly.Web.Common.Common.addUserSchema(userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions ActivateUser event completed.", ex);
                throw ex;
            }
        }

        private List<UserSubscriptionModel> GetUsers()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUsers  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var currentDate = DateTime.Now;
                var companyUsers = _clearJellyContext.Users.Where(x => !x.IsDeleted)
                        .Select(x => new UserSubscriptionModel
                        {
                            UserId = x.UserId,
                            IsActive = x.IsActive,
                            Name = x.FirstName + " " + x.LastName,
                            Email = x.Email,
                            RegistrationDate = x.RegistrationDate.ToShortDateString(),
                            CompanyName = x.Company.Name,
                            IsAdmin = x.IsAdmin,
                            HasAnyActiveSubscription = x.Company.CompanySubscriptions.Any(a => a.IsActive && (a.EndDate == null || a.EndDate >= currentDate))
                        }).ToList();
                return companyUsers;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions ActivateUser event completed.", ex);
                throw ex;
            }
        }
        private List<UserSubscriptionListModel> GetUserSubscriptions(int userId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUserSubscriptions  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                if (user != null)
                {
                    var companyUsers = _clearJellyContext.CompanySubscriptions.Where(x => x.CompanyId == user.CompanyId).ToList()
                            .Select(x => new UserSubscriptionListModel
                            {
                                SubscriptionType = x.SubscriptionType.TypeName,
                                IsActive = x.IsActive,
                                StartDate = x.StartDate.ToShortDateString(),
                                EndDate = x.EndDate.HasValue ? x.EndDate.Value.ToShortDateString() : string.Empty,
                                NoOfUsers = x.AdditionalUsers,
                                StartedAsTrial = x.StartedAsTrial,
                                IsYearly = x.IsYearlySubscription,
                                NoOfPacks = x.Quantity,
                            }).ToList();
                    return companyUsers;
                }
                return new List<UserSubscriptionListModel>();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserSubscriptions GetUserSubscriptions event completed.", ex);
                throw ex;
            }
        }
    }
}