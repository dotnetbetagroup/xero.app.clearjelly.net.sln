﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" CodeBehind="Index.aspx.designer.cs" Inherits="ClearJelly.Web.Admin.Index"  Async="true" %>

<%--<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>--%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div>
        <div class="col-md-12 gray-bg">
            <section id="loginForm">
                <div class="login-title">
                    <h1>Login</h1>
                    <span>Please login into your account.</span>
                </div>
                <div class="login-box mb30">
                    <div class="alert alert-warning" role="alert" id="errorText" runat="server">
                        <a class="close" onclick="$('.alert').hide()">×</a>
                        <strong>Warning!</strong>&nbsp; Your session is expired please login to continue.
                    </div>

                    <div class="clearfix"></div>
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="EmailAddress">Email Address</asp:Label>
                        <asp:TextBox runat="server" ID="EmailAddress" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="EmailAddress" Display="None"
                            CssClass="text-danger" ErrorMessage="Username is required." />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                            ControlToValidate="EmailAddress" ErrorMessage="Please enter valid email." Display="None"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            CssClass="text-danger"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Password">Password</asp:Label>
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="Password is required." Display="None" />
                    </div>
                    <div class="form-group mb10">
                        <asp:CheckBox runat="server" ID="RememberMe" />
                        <asp:Label runat="server" AssociatedControlID="RememberMe" CssClass="lbl-remember-me">Remember me?</asp:Label>
                    </div>
                    <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-success btn-block" />
                    <%--<div class="row">
                        <div class="col-sm-6 mt15 mb15">
                            <asp:HyperLink runat="server" NavigateUrl="~/Account/ForgetPassword.aspx">Forgot Password?</asp:HyperLink>
                        </div>
                        <div class="col-sm-6 mt15 mb15 text-right">
                            <asp:HyperLink runat="server" NavigateUrl="~/Account/Register" ID="RegisterHyperLink" ViewStateMode="Disabled">Registration</asp:HyperLink>
                        </div>
                    </div>--%>
                </div>
            </section>
        </div>

        <%--<div class="col-md-4">
            <section id="socialLoginForm">
                <uc:openauthproviders runat="server" id="OpenAuthLogin" />
            </section>
        </div>--%>
    </div>
     <script type="text/javascript" src="//platform.linkedin.com/in.js">
         api_key: <%= System.Configuration.ConfigurationManager.AppSettings["LinkedInAppKey"] %>
         authorize: false
         onLoad: linkedIn.onLinkedInLoad
     </script>
    <script type="text/javascript">
        var facebookAppId = <%=System.Configuration.ConfigurationManager.AppSettings["FBLoginAppId"].ToString()%>;
    </script>
    <script type="text/javascript" src="../Scripts/XeroWebApp/Login.js"></script>
</asp:Content>


