﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Web.Http;
using ClearJelly.Entities;
using ClearJelly.ViewModels.Admin;

namespace ClearJelly.Web.Admin
{
    public partial class ManageSampleLinks : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

        public ManageSampleLinks()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks Page_Load event completed.", ex);
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<SampleLinkModel> GetSampleLinks()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetSampleLinks event started.");
                var currentPage = new ManageSampleLinks();
                var sampleLinks = currentPage.GetSampleLinksData();
                return sampleLinks;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetSampleLinks event completed.", ex);
                throw ex;
            }
        }
        [WebMethod]
        public static bool Update(SampleLinkModel model)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks Update event started.");
                var currentPage = new ManageSampleLinks();
                if (model.SampleLinkId == null)
                {
                    currentPage.AddSampleLink(model);
                }
                else
                {
                    currentPage.UpdateSampleLink(model);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks Update event completed.", ex);
                throw ex;
            }
        }



        [WebMethod]
        //[ScriptMethod(UseHttpGet = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static bool UpdateSortOrder([FromBody]int id, [FromBody]int fromPosition, [FromBody]int toPosition, [FromBody]string direction, [FromBody] string group)
        {
            try
            {
                //string id = "1";
                //string fromPosition, string direction, string toPosition, string group
                //string fromPosition, direction, toPosition = string.Empty, group = string.Empty;
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks UpdateSortOrder event started.");
                var currentPage = new ManageSampleLinks();

                currentPage.UpdateSampleLinkSortOrder(id, fromPosition, toPosition, direction);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks UpdateSortOrder event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        public static bool Delete(int sampleLinkId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks Delete  event started.");
                var currentPage = new ManageSampleLinks();
                return currentPage.DeleteSampleLink(sampleLinkId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks Delete event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static SampleLinkModel GetEditSampleLink(int sampleLinkId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetEditSampleLink event started.");
                var currentPage = new ManageSampleLinks();
                var sampleLinks = currentPage.GetEditSampleLinksData(sampleLinkId);
                return sampleLinks;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetEditSampleLink event completed.", ex);
                throw ex;
            }
        }
        private void UpdateSampleLink(SampleLinkModel model)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks UpdateSampleLink  event started.");
                var sampleLink = _clearJellyContext.SampleLinks.FirstOrDefault(s => s.SampleLinkId == model.SampleLinkId);
                sampleLink.DownloadLink = model.DownloadLink;
                sampleLink.Title = model.Title;
                //sampleLink.SortOrder = model.SortOrder;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks UpdateSampleLink event completed.", ex);
                throw ex;
            }
        }


        private void UpdateSampleLinkSortOrder(int id, int fromPosition, int toPosition, string direction)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks UpdateSampleLink  event started.");
                //var sampleLink = _clearJellyContext.SampleLinks.FirstOrDefault(s => s.SampleLinkId == id);
                //sampleLink.SortOrder = toPosition;
                //_clearJellyContext.SaveChanges();
                if (direction == "back")
                {
                    var movedCompanies = _clearJellyContext.SampleLinks
                                .Where(c => (toPosition <= c.SortOrder && c.SortOrder <= fromPosition))
                                .ToList();

                    foreach (var company in movedCompanies)
                    {
                        company.SortOrder++;
                    }
                }
                else
                {
                    var movedCompanies = _clearJellyContext.SampleLinks
                                .Where(c => (fromPosition <= c.SortOrder && c.SortOrder <= toPosition))
                                .ToList();
                    foreach (var company in movedCompanies)
                    {
                        company.SortOrder--;
                    }
                }

                _clearJellyContext.SampleLinks.First(c => c.SampleLinkId == id).SortOrder = toPosition;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks UpdateSampleLink event completed.", ex);
                throw ex;
            }
        }

        private bool DeleteSampleLink(int sampleLinkId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks DeleteSampleLink  event started.");
                var sampleLink = _clearJellyContext.SampleLinks.FirstOrDefault(s => s.SampleLinkId == sampleLinkId);
                _clearJellyContext.SampleLinks.Remove(sampleLink);
                _clearJellyContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks DeleteSampleLink event completed.", ex);
                throw ex;
            }
        }

        private void AddSampleLink(SampleLinkModel model)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks AddSampleLink  event started.");
                var nextSortOrder = _clearJellyContext.SampleLinks.Any() ? _clearJellyContext.SampleLinks.Max(a => a.SortOrder) + 1 : 1;
                var sampleLink = new SampleLink();
                sampleLink.DownloadLink = model.DownloadLink;
                sampleLink.Title = model.Title;
                sampleLink.SortOrder = nextSortOrder;
                _clearJellyContext.SampleLinks.Add(sampleLink);
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks AddSampleLink event completed.", ex);
                throw ex;
            }
        }


        private SampleLinkModel GetEditSampleLinksData(int sampleLinkId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetEditSampleLinksData  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var samplelink = _clearJellyContext.SampleLinks.FirstOrDefault(a => a.SampleLinkId == sampleLinkId);
                if (samplelink != null)
                {
                    return new SampleLinkModel
                    {
                        DownloadLink = samplelink.DownloadLink,
                        SampleLinkId = samplelink.SampleLinkId,
                        Title = samplelink.Title,
                        SortOrder = samplelink.SortOrder
                    };
                }
                else { return new SampleLinkModel(); }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetEditSampleLinksData event completed.", ex);
                throw ex;
            }
        }


        private List<SampleLinkModel> GetSampleLinksData()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetSampleLinksData  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var samplelinks = _clearJellyContext.SampleLinks.Select(x => new SampleLinkModel
                {
                    DownloadLink = x.DownloadLink,
                    SampleLinkId = x.SampleLinkId,
                    Title = x.Title,
                    SortOrder = x.SortOrder
                }).ToList();
                return samplelinks;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSampleLinks GetSampleLinksData event completed.", ex);
                throw ex;
            }
        }
    }
}