﻿using ClearJelly.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.ViewModels.Admin;

namespace ClearJelly.Web.Admin
{
    public partial class ActivateUser : System.Web.UI.Page
    {

        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;
        private static string ChosenService = "";

        public ActivateUser()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }

            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser Page_Load event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<ActivateUserModel> GetUserList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser GetUserList event started.");
                var currentPage = new ActivateUser();
                var userList = currentPage.GetUsers();

                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser GetUserList event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        public static bool ActivateUserEvent(int userId)
        {

            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser ActivateUser  event started.");
                return ClearJelly.Web.Common.Common.addUserSchema(userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser ActivateUser event completed.", ex);
                throw ex;
            }
        }

        private List<ActivateUserModel> GetUsers()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser GetUsers  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var companyUsers = _clearJellyContext.Users.Where(x => !x.IsDeleted)
                        .Select(x => new ActivateUserModel
                        {
                            UserId = x.UserId,
                            isActive = x.IsActive,
                            Name = x.FirstName + " " + x.LastName,
                            Email = x.Email,
                        }).ToList();
                return companyUsers;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser ActivateUser event completed.", ex);
                throw ex;
            }
        }
    }
}