﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="ActivateUser.aspx.cs" Inherits="ClearJelly.Web.Admin.ActivateUser" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
   <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>User Activation</h1>
            </div>

            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>  
               <strong>Success!</strong> User activated successfully.
            </div>
            <div class="alert alert-danger display-none" role="alert">
               <a class="close" onclick="$('.alert').hide()">×</a>  
               <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div id="activteuserdv">
            </div>
        </div>
    </div>
    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/ActivateUser.js">
    </script>
</asp:Content>
