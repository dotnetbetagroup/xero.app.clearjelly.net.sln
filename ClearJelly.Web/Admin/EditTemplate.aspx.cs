﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClearJelly.Entities;
using ClearJelly.ViewModels.Admin;

namespace ClearJelly.Web.Admin
{
    public partial class EditTemplate : System.Web.UI.Page
    {
        //display on  aspx page.
        protected List<TemplateParameters> templateParameters = new List<TemplateParameters>();
        protected List<TemplateAttachment> templateAttachment = new List<TemplateAttachment>();
        private static NLog.Logger _logger;

        public EditTemplate()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx", false);
                }
                successDiv.Visible = false;
                errorDiv.Visible = false;
                if (Request.QueryString["templateId"] != null)
                {
                    var templateId = int.Parse(Request.QueryString["templateId"]);
                    if (!IsPostBack)
                    {
                        CategoryDropDown.DataSource = GetCategory();
                        CategoryDropDown.DataBind();
                        BindTemplateControls(templateId);
                        BindAttachment(templateId);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate Page_Load event completed.", ex);
            }
        }


        protected void uploadFile_Click(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate uploadFile_Click event started.");
                var emailTemplateId = Convert.ToInt32(hdnEmailTemplateId.Value);
                if (UploadImages.HasFiles)
                {
                    using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                    {
                        var emailTemplate = _clearJellyContext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == emailTemplateId);
                        if (emailTemplate != null)
                        {
                            foreach (HttpPostedFile uploadedFile in UploadImages.PostedFiles)
                            {
                                SaveToTemporary(uploadedFile);
                            }
                        }
                        successDiv.Visible = true;
                        BindTemplateParameters(emailTemplateId);
                        BindTemplateControls(emailTemplateId);
                    }
                }

            }
            catch (Exception ex)
            {
                errorDiv.Visible = true;
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate uploadFile_Click event completed.", ex);
            }
        }

        protected void btnUpdateTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate btnUpdateTemplate_Click event started.");
                using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                {
                    var emailTemplateId = Convert.ToInt32(hdnEmailTemplateId.Value);
                    var emailTemplate = _clearJellyContext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == emailTemplateId);
                    if (emailTemplate != null)
                    {
                        emailTemplate.EmailCategoryId = Convert.ToInt32(CategoryDropDown.SelectedValue);
                        emailTemplate.IsActive = chkActive.Checked;
                        emailTemplate.Name = templateName.Text;
                        emailTemplate.Subject = subject.Text;
                        emailTemplate.TemplateContent = ContentArea.Text;
                        SaveFromTemporary(emailTemplateId);
                        DeleteSavedAttachmentFromDB();
                        var result = _clearJellyContext.SaveChanges() > 0;
                        RemoveTempAttachmentData();
                        Session["isUpdateSuccess"] = true;
                        Response.Redirect("~/Admin/EmailTemplate", false);
                    }
                }
            }
            catch (Exception ex)
            {
                RemoveTempAttachmentData();
                Session["isUpdateSuccess"] = false;
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate btnUpdateTemplate_Click event completed.", ex);
                Response.Redirect("~/Admin/EmailTemplate", false);
            }
        }

        protected void deleteAttachment_click(object sender, ImageClickEventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate deleteAttachment_click event started.");
                var imageAttachmentId = int.Parse(imageDeleteId.Value);
                var isdbSaved = bool.Parse(isSaved.Value);
                if (isdbSaved)
                {
                    DeleteSavedAttachment(imageAttachmentId);
                }
                else
                {
                    DeleteAttachmentFromTemp(imageAttachmentId);
                }
                var emailTemplateId = Convert.ToInt32(hdnEmailTemplateId.Value);
                BindAttachment(emailTemplateId);
                BindTemplateControls(emailTemplateId);
                successDiv.Visible = true;
            }
            catch (Exception ex)
            {
                errorDiv.Visible = true;
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate deleteAttachment_click event completed.", ex);
            }
        }


        #region Private
        private List<DropDownViewModel> GetCategory()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate GetCategory event started.");
                var categoryList = new List<DropDownViewModel>();
                using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                {
                    var categories = _clearJellyContext.EmailCategories;
                    var DefaultData = new DropDownViewModel()
                    {
                        Id = -1,
                        Name = "Select"
                    };
                    categoryList.Add(DefaultData);

                    categoryList.AddRange(categories.Select(a => new DropDownViewModel()
                    {
                        Id = a.EmailCategoryId,
                        Name = a.Name
                    }).ToList());
                }
                return categoryList;
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetCategory event completed.", ex);
                throw;
            }
        }

        private void SaveEmailAttachment(string attachedFileName, string uploadedFileName, int emailTemplateId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate SaveEmailAttachment event started.");
                using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                {
                    var attachment = new EmailAttachment()
                    {
                        AttachmentName = attachedFileName,
                        FileName = uploadedFileName,
                        EmailTemplateId = emailTemplateId
                    };
                    _clearJellyContext.EmailAttachments.Add(attachment);

                    _clearJellyContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate SaveEmailAttachment event completed.", ex);
            }
        }

        private int GetTempAttachmentId()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate GetTempAttachmentId event started.");
                List<TemplateAttachment> tempAttachment = GetTempAttachment();
                tempAttachment = tempAttachment.Where(a => !a.IsSaved).ToList();
                return tempAttachment.Any() ? tempAttachment.Max(a => a.EmailAttachmentId) + 1 : 1;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetTempAttachmentId event completed.", ex);
                throw;
            }
        }

        private void BindTemplateParameters(int templateId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate BindTemplateParameters event started.");
                using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                {
                    var template = _clearJellyContext.EmailTemplates.FirstOrDefault(s => s.EmailTemplateId == templateId);
                    if (template != null)
                    {

                        var parameters = template.EmailParameters;
                        templateParameters.AddRange(parameters.Select(a => new TemplateParameters()
                        {
                            EmailParameterId = a.EmailParameterId,
                            Description = a.Description,
                            Name = a.Name,
                        }).ToList());
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate BindTemplateParameters event completed.", ex);
                throw;
            }

        }

        private void BindTemplateControls(int templateId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate BindTemplateControls event started.");
                using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                {
                    var template = _clearJellyContext.EmailTemplates.FirstOrDefault(s => s.EmailTemplateId == templateId);
                    if (template != null)
                    {
                        if (template.EmailCategory != null)
                        {
                            CategoryDropDown.SelectedValue = template.EmailCategory.EmailCategoryId.ToString();
                        }
                        templateName.Text = template.Name;
                        ContentArea.Text = template.TemplateContent;
                        if (template.IsActive) chkActive.Checked = true;
                        hdnEmailTemplateId.Value = templateId.ToString();
                        subject.Text = template.Subject;

                        BindTemplateParameters(templateId);
                        BindAttachment(templateId);
                    }

                }


            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate BindTemplateControls event completed.", ex);
                throw;
            }
        }

        private List<TemplateAttachment> GetTempAttachment()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate GetTempAttachment event started.");
                var templateTempAttachment = new List<TemplateAttachment>();
                var TempAttachment = Session["TempAttachment"];
                if (TempAttachment != null)
                {
                    var sessionTempAttachment = (List<TemplateAttachment>)Session["TempAttachment"];
                    templateTempAttachment.AddRange(sessionTempAttachment);
                }
                return templateTempAttachment;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate GetTempAttachment event completed.", ex);
                throw;
            }

        }

        private void BindAttachment(int templateId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate BindAttachment event started.");
                templateAttachment = new List<TemplateAttachment>();
                List<TemplateAttachment> tempAttachment = GetTempAttachment();

                var deleteDBAttachments = Session["deleteDBAttachments"];
                List<int> deletedAttachments = new List<int>();
                if (deleteDBAttachments != null)
                {
                    deletedAttachments = (List<int>)Session["deleteDBAttachments"];
                }
                using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                {
                    var template = _clearJellyContext.EmailTemplates.FirstOrDefault(s => s.EmailTemplateId == templateId);
                    var attachments = template.EmailAttachments;
                    if (deletedAttachments.Any())
                    {
                        attachments = attachments.Where(a => !deletedAttachments.Contains(a.EmailAttachmentId)).ToList();
                    }
                    if (template != null)
                    {
                        templateAttachment.AddRange(attachments.Select(a => new TemplateAttachment()
                        {
                            EmailAttachmentId = a.EmailAttachmentId,
                            AttachmentName = a.AttachmentName,
                            FileName = a.FileName,
                            IsSaved = true,
                        }));

                    }
                    if (tempAttachment.Any())
                    {
                        templateAttachment.AddRange(tempAttachment);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate BindAttachment event completed.", ex);
                throw;
            }
        }

        private void DeleteAttachmentFromTemp(int imageAttachmentId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate DeleteAttachmentFromTemp event started.");
                List<TemplateAttachment> tempAttachment = GetTempAttachment();
                var templateAttachment = tempAttachment.FirstOrDefault(a => a.EmailAttachmentId == imageAttachmentId && !a.IsSaved);
                DeleteTempAttachedFile(templateAttachment.AttachmentName);
                tempAttachment.Remove(templateAttachment);
                Session["TempAttachment"] = tempAttachment;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate DeleteAttachmentFromTemp event completed.", ex);
                throw;
            }

        }

        private void DeleteSavedAttachment(int imageAttachmentId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate DeleteSavedAttachment event started.");
                var deleteDBAttachments = Session["deleteDBAttachments"];
                var attachmentList = new List<int>();
                if (deleteDBAttachments != null)
                {
                    attachmentList = (List<int>)Session["deleteDBAttachments"];
                }
                attachmentList.Add(imageAttachmentId);
                Session["deleteDBAttachments"] = attachmentList;

            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate DeleteSavedAttachment event completed.", ex);
                throw;
            }
        }

        private void DeleteSavedAttachmentFromDB()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate DeleteSavedAttachmentFromDB event started.");
                var deleteDBAttachments = Session["deleteDBAttachments"];
                var attachmentList = new List<int>();
                if (deleteDBAttachments != null)
                {
                    attachmentList = (List<int>)Session["deleteDBAttachments"];
                    using (ClearJellyEntities _clearJellyContext = new ClearJellyEntities())
                    {
                        var emailAttachments = _clearJellyContext.EmailAttachments.Where(a => attachmentList.Contains(a.EmailAttachmentId));
                        foreach (var emailAttachment in emailAttachments)
                        {
                            DeleteSavedAttachedFile(emailAttachment.AttachmentName);
                        }
                        _clearJellyContext.EmailAttachments.RemoveRange(emailAttachments);
                        _clearJellyContext.SaveChanges();
                        if (emailAttachments.Any())
                        {
                            var templateId = emailAttachments.First().EmailTemplateId;
                            BindAttachment(templateId.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate DeleteSavedAttachmentFromDB event completed.", ex);
                throw;
            }
        }

        private string CreateTempFolder()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate CreateTempFolder event started.");
                var tempAttachmentFolder = Common.Common.GetEmailAttachmentTempFrom();
                var tempPath = Path.Combine(Server.MapPath("/" + tempAttachmentFolder));
                var sessionId = HttpContext.Current.Session.SessionID;
                var sessionFolder = Path.Combine(Server.MapPath("/" + tempAttachmentFolder + "/" + sessionId));

                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }
                if (!Directory.Exists(sessionFolder))
                {
                    Directory.CreateDirectory(sessionFolder);
                }
                return sessionFolder;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate CreateTempFolder event completed.", ex);
                throw;
            }

        }

        private void SaveToTemporary(HttpPostedFile uploadedFile)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate SaveToTemporary event started.");
                var uploadedFileName = uploadedFile.FileName;
                var attachedFileName = Guid.NewGuid() + uploadedFileName;
                string tempFolder = CreateTempFolder();

                uploadedFile.SaveAs(Path.Combine(tempFolder, attachedFileName));
                var nextTempAttachmentId = GetTempAttachmentId();
                var attachments = GetTempAttachment();
                attachments.Add(new TemplateAttachment()
                {
                    AttachmentName = attachedFileName,
                    FileName = uploadedFileName,
                    EmailAttachmentId = nextTempAttachmentId,
                    IsSaved = false
                });
                Session["TempAttachment"] = attachments;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate SaveToTemporary event completed.", ex);
                throw;
            }
        }

        private void FileMoveFromTemp(string attachedFileName)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate FileMoveFromTemp event started.");
                var sessionId = HttpContext.Current.Session.SessionID;
                var attachmentTempFolder = Common.Common.GetEmailAttachmentTempFrom();
                var attachmentFolder = Common.Common.GetEmailAttachmentFrom();
                var sessionFolder = Path.Combine(Server.MapPath("/" + attachmentTempFolder + "/" + sessionId));
                var sourceFileName = Path.Combine(sessionFolder, attachedFileName);
                var attachmetFolderPath = Path.Combine(Server.MapPath("/" + attachmentFolder));
                //create dir if not exists.
                if (!Directory.Exists(attachmetFolderPath))
                {
                    Directory.CreateDirectory(attachmetFolderPath);
                }
                var destFileName = Path.Combine(attachmetFolderPath + "/" + attachedFileName);
                File.Move(sourceFileName, destFileName);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate FileMoveFromTemp event completed.", ex);
                throw;
            }
        }

        private void DeleteTempAttachedFile(string attachedFileName)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate DeleteTempAttachedFile event started.");
                var sessionId = HttpContext.Current.Session.SessionID;
                var temptAchmentFolder = Common.Common.GetEmailAttachmentTempFrom();
                var sessionFolder = Path.Combine(Server.MapPath("/" + temptAchmentFolder + "/" + sessionId));
                var sourceFileName = Path.Combine(sessionFolder, attachedFileName);
                File.Delete(sourceFileName);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate DeleteTempAttachedFile event completed.", ex);
            }
        }

        private void DeleteSavedAttachedFile(string attachedFileName)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate DeleteSavedAttachedFile event started.");
                var attachmentFolder = Common.Common.GetEmailAttachmentFrom();
                var destFileName = Path.Combine(Server.MapPath("/" + attachmentFolder + "/" + attachedFileName));
                if (File.Exists(destFileName))
                {
                    File.Delete(destFileName);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate DeleteSavedAttachedFile event completed.", ex);
                throw;
            }
        }

        private void SaveFromTemporary(int emailTemplateId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate SaveFromTemporary event started.");
                var sessionTempAttachment = Session["TempAttachment"];
                if (sessionTempAttachment != null)
                {
                    var tempAttachment = (List<TemplateAttachment>)sessionTempAttachment;
                    if (tempAttachment.Any())
                    {
                        tempAttachment = tempAttachment.Where(a => !a.IsSaved).ToList();

                        foreach (var attachment in tempAttachment)
                        {
                            FileMoveFromTemp(attachment.AttachmentName);

                            SaveEmailAttachment(attachment.AttachmentName, attachment.FileName, emailTemplateId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate SaveFromTemporary event completed.", ex);
                throw;
            }
        }

        private void RemoveTempAttachmentData()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EditTemplate RemoveTempAttachmentData event started.");
                Session.Remove("TempAttachment");
                Session.Remove("deleteDBAttachments");
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of EmailTemplate RemoveTempAttachmentData event completed.", ex);
                throw;
            }
        }


        #endregion
    }
}