﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Globalization;
using System.ComponentModel;
using System.Web.Http;
using ClearJelly.Entities;
using ClearJelly.ViewModels.Admin;
using ClearJelly.XeroApp;

namespace ClearJelly.Web.Admin
{
    public partial class UserActivities : System.Web.UI.Page
    {

        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;
        private static string ChosenService = "";

        public UserActivities()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ChosenService"] != null)
            {
                ChosenService = Session["ChosenService"].ToString();
            }

            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
                if (!IsPostBack)
                {
                    FilterDropDown.DataSource = GetFilters();
                    FilterDropDown.DataBind();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities Page_Load event completed.", ex);
            }
        }

        private List<DropDownViewModel> GetFilters()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetFilters  event started.");
                var categories = _clearJellyContext.EmailCategories;
                var categoryList = new List<DropDownViewModel>();

                var list = Enum.GetValues(typeof(UserFilter)).Cast<UserFilter>();
                var Filters = (from p in list
                               select new DropDownViewModel
                               {
                                   Name = GetEnumDescription(p),
                                   Id = ((int)p)
                               }).ToList();
                var blank = new DropDownViewModel { Name = " Select All ", Id = 0 };
                Filters.Insert(0, blank);
                return Filters.ToList();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetFilters event completed.", ex);
                throw;
            }
        }
        public static string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }



        #region Get UserList
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UserActivitiesViewModel> GetUserList(int filterId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserList event started.");
                var currentPage = new UserActivities();
                var userList = currentPage.GetUsers(filterId);
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserList event completed.", ex);
                throw ex;
            }
        }
        private List<UserActivitiesViewModel> GetUsers(int filterId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUsers  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var currentDate = DateTime.Now;
                var companyUsers = _clearJellyContext.Users.Where(x => !x.IsDeleted);
                switch (filterId)
                {
                    //active users.
                    case 1:
                        companyUsers = companyUsers.Where(s => s.IsActive);
                        break;
                    //have any active subscription
                    case 2:
                        companyUsers = companyUsers.Where(s => s.Company.CompanySubscriptions.Any(a => a.IsActive && (a.EndDate == null || a.EndDate >= currentDate)));
                        break;
                    //non active users.
                    case 3:
                        companyUsers = companyUsers.Where(s => !s.IsActive);
                        break;
                    //blocked users
                    case 4:
                        companyUsers = companyUsers.Where(s => s.LastloginFailed != null);
                        break;
                    //suspended users.
                    case 5:
                        companyUsers = companyUsers.Where(s => s.SuspendedUsers.Any());
                        break;
                    default:
                        break;
                }
                var companyUserList = companyUsers.ToList().Select(x => new UserActivitiesViewModel
                {
                    UserId = x.UserId,
                    IsActive = x.IsActive,
                    Name = x.FirstName + " " + x.LastName,
                    Email = x.Email,
                    CompanyName = x.Company.Name,
                    IsAdmin = x.IsAdmin,
                    IsSuspended = x.SuspendedUsers.Any(),
                    RegistrationDate = x.RegistrationDate,
                    SuspendedDueTo = x.SuspendedUsers.Any() ? x.SuspendedUsers.OrderByDescending(y => y.SuspendedDate).FirstOrDefault().Reason : null,
                    IsBlocked = x.LastloginFailed != null,
                    IsStartedAsTrail = x.Company.CompanySubscriptions.Any(a => a.StartedAsTrial && a.EndDate != null),
                    HasAnyActiveSubscription = x.Company.CompanySubscriptions.Any(a => a.IsActive && (a.EndDate == null || a.EndDate >= currentDate)),
                    PaidVersion = x.Company.CompanySubscriptions.Any(a => (a.RecurringProfileId != null && a.IsActive && a.EndDate == null)),

                }).ToList();

                return companyUserList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities ActivateUser event completed.", ex);
                throw ex;
            }
        }
        #endregion

        #region User Subscriptions
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UserSubscriptionListModel> GetUserSubscriptionList(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserList event started.");
                var currentPage = new UserActivities();
                var userList = currentPage.GetUserSubscriptions(userId);
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserList event completed.", ex);
                throw ex;
            }
        }
        private List<UserSubscriptionListModel> GetUserSubscriptions(int userId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserSubscriptions  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                if (user != null)
                {
                    var companyUsers = _clearJellyContext.CompanySubscriptions.Where(x => x.CompanyId == user.CompanyId).ToList()
                            .Select(x => new UserSubscriptionListModel
                            {
                                SubscriptionType = x.SubscriptionType.TypeName,
                                IsActive = x.IsActive,
                                StartDate = x.StartDate.ToShortDateString(),
                                EndDate = x.EndDate.HasValue ? x.EndDate.Value.ToShortDateString() : string.Empty,
                                NoOfUsers = x.AdditionalUsers,
                                StartedAsTrial = x.StartedAsTrial,
                                IsYearly = x.IsYearlySubscription,
                                NoOfPacks = x.Quantity
                            }).ToList();
                    return companyUsers;
                }
                return new List<UserSubscriptionListModel>();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserSubscriptions event completed.", ex);
                throw ex;
            }
        }
        #endregion

        #region Active User
        [WebMethod]
        public static bool ActivateUser(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser ActivateUser  event started.");
                return ClearJelly.Web.Common.Common.addUserSchema(userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ActivateUser ActivateUser event completed.", ex);
                throw ex;
            }
        }
        #endregion

        #region Unblock User

        [WebMethod]
        public static bool UnblockUser(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser ActivateUser  event started.");
                var currentPage = new UserActivities();
                return currentPage.UnblockSelectedUser(userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser UnblockUser event completed.", ex);
                throw ex;
            }
        }


        [WebMethod]
        public static void ImpersonateUser(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser ActivateUser  event started.");
                var currentPage = new UserActivities();
                currentPage.ImpersonateSelectedUser(userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser UnblockUser event completed.", ex);
                throw ex;
            }
        }

        private void ImpersonateSelectedUser(int userId)
        {
            using (var dbContext = new ClearJellyEntities())
            {
                var userDetails = dbContext.Users.FirstOrDefault(x => x.UserId == userId);
                if (userDetails != null)
                {
                    var decryptedPassword = EncryptDecrypt.DecryptString(userDetails.Password);
                    //var abc = dbContext.ImpersonateUserSp(userDetails.Company1.Name, userDetails.Email, decryptedPassword);
                }


            }
        }


        private bool UnblockSelectedUser(int userId)
        {
            try
            {
                var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                if (user != null)
                {
                    user.LastloginFailed = null;
                    _clearJellyContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser UnblockSelectedUser event completed.", ex);
                throw ex;
            }

        }
        #endregion

        #region Suspend User
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static UserSuspendViewModel GetUserSuspendDetails(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserSuspendDetails event started.");
                var currentPage = new UserActivities();
                var userDetails = currentPage.GetUserSuspendData(userId);
                return userDetails;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserSuspendDetails event completed.", ex);
                throw ex;
            }
        }

        private UserSuspendViewModel GetUserSuspendData(int userId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserSuspendDetails event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                if (user != null)
                {
                    var additionalUser = _clearJellyContext.Users.Count(a => a.CompanyId == user.CompanyId);
                    return new UserSuspendViewModel()
                    {
                        SuspendDate = DateTime.Now.ToShortDateString(),
                        SuspendReason = null,
                        UserId = userId,
                        AnyAdditionalUser = additionalUser > 1
                    };
                }
                return new UserSuspendViewModel();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserSuspendDetails event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        public static bool SuspendUser(UserSuspendViewModel model)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities SuspendUser  event started.");
                string loggedinUser = HttpContext.Current.Session["Jelly_Admin_user"] != null ? HttpContext.Current.Session["Jelly_Admin_user"].ToString() : string.Empty;
                return ClearJelly.Web.Common.Common.SuspendUser(loggedinUser, model.UserId, model.SuspendReason);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities SuspendUser event completed.", ex);
                throw ex;
            }
        }
        #endregion

        #region Reactivate
        [WebMethod]
        public static bool ReActivateUser(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities ReActivateUser  event started.");
                string loggedinUser = HttpContext.Current.Session["Jelly_Admin_user"] != null ? HttpContext.Current.Session["Jelly_Admin_user"].ToString() : string.Empty;
                return ClearJelly.Web.Common.Common.ReactivateUser(loggedinUser, userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities ReActivateUser event completed.", ex);
                throw ex;
            }
        }
        #endregion

        #region DeleteUser
        [WebMethod]
        public static bool DeleteUser(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities DeleteUser  event started.");
                string loggedinUser = HttpContext.Current.Session["Jelly_Admin_user"] != null ? HttpContext.Current.Session["Jelly_Admin_user"].ToString() : string.Empty;
                return ClearJelly.Web.Common.Common.DeleteUser(loggedinUser, userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities DeleteUser event completed.", ex);
                throw ex;
            }
        }
        #endregion


        #region CollectOutstanding
        [WebMethod]
        public static UserSubscriptionList GetOutstandingDetails(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetOutstandingDetails  event started.");
                var currentPage = new UserActivities();
                var userDetails = currentPage.GetSubscriptionRecurringProfile(userId);
                return userDetails;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetOutstandingDetails event completed.", ex);
                throw ex;
            }
        }



        public UserSubscriptionList GetSubscriptionRecurringProfile(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetSubscriptionRecurringProfile  event started.");
                var CompanyId = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId).CompanyId;
                var subcriptionDetails = _clearJellyContext.CompanySubscriptions.Where(x => x.CompanyId == CompanyId && x.RecurringProfileId != null).ToList();
                UserSubscriptionList userSubscriptionList = new UserSubscriptionList();
                foreach (var subscription in subcriptionDetails)
                {
                    UserSubscriptionDetailsViewModel usersubscription = new UserSubscriptionDetailsViewModel();
                    usersubscription.RecurringProfileId = subscription.RecurringProfileId;
                    usersubscription.SubscriptionName = subscription.SubscriptionType.TypeName;
                    usersubscription.OutStandingBalance = GetOustandingBillDetailsFromPaypal(subscription.RecurringProfileId);
                    userSubscriptionList.lstUserSubscriptionDetailsViewModel.Add(usersubscription);
                }

                //    .Select(x => new UserSubscriptionDetailsViewModel
                //{
                //    RecurringProfileId = x.RecurringProfileId,
                //    SubscriptionName = x.SubscriptionType.TypeName,
                //}).ToList();




                return userSubscriptionList;
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetSubscriptionRecurringProfile event completed.", ex);
                throw;
            }
        }



        public static Dictionary<string, string> GetOustandingBillDetailsFromPaypal(string recurringProfileId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetOustandingBillDetailsFromPaypal  event started.");
                var paypalResponse = ClearJelly.Web.Common.Common.GetRecurringPaymentDetails(recurringProfileId);
                return paypalResponse;


            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetOustandingBillDetailsFromPaypal event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        public static Dictionary<string, string> CollectOutstandingBalance(string recurringProfileId)
        {
            var paypalResponse = ClearJelly.Web.Common.Common.CollectOutstandingBalance(recurringProfileId);
            return paypalResponse;
        }


        #endregion


        #region ExtendSubscription
        [WebMethod]
        public static bool ExtendSubscriptionUser(int userId, int daystoExtend)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities ExtendSubscriptionUser  event started.");
                string loggedinUser = HttpContext.Current.Session["Jelly_Admin_user"] != null ? HttpContext.Current.Session["Jelly_Admin_user"].ToString() : string.Empty;
                return ClearJelly.Web.Common.Common.ExtendSubscriptionUser(loggedinUser, userId, daystoExtend);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities ExtendSubscriptionUser event completed.", ex);
                throw ex;
            }
        }
        #endregion
    }
}