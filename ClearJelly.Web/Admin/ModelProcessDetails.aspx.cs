﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.Entities;
using ClearJelly.ViewModels.Admin;
using ClearJelly.XeroApp;

namespace ClearJelly.Web.Admin
{
    public partial class ModelProcessDetails : System.Web.UI.Page
    {
        private static NLog.Logger _logger;

        public ModelProcessDetails()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                FilterDropDown.DataSource = GetFilters();
                FilterDropDown.DataTextField = "Email";
                FilterDropDown.DataValueField = "UserId";
                FilterDropDown.DataBind();
                FilterDropDown.Items.Insert(0, new ListItem("Select All", "0", true));
                FilterDropDown.SelectedIndex = 0;
            }
        }



        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static ModelProcessListViewModel GetModelProcessList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails  GetModelProcessList event started.");
                var currentPage = new ModelProcessDetails();
                var companySubscription = currentPage.GetModelProcessData(0, null, null, null);
                return companySubscription;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails  GetModelProcessList event completed.", ex);
                throw;
            }

        }

        private List<User> GetFilters()
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Executio n of Admin_ModelProcessDetails GetFilters  event started.");

                    var userList = dbContext.Users.Where(x => x.IsActive == true).ToList().Select(x => new User
                    {
                        UserId = x.UserId,
                        Email = x.Email,
                    }).OrderBy(x => x.Email).ToList();

                    return userList;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetFilters event completed.", ex);
                throw;
            }
        }



        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static ListXeroOrganization BindOrganizationDropdown(int userId)
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    var userDetails = dbContext.Users.FirstOrDefault(x => x.UserId == userId);
                    int companyId = 0;
                    if (userDetails != null)
                    {
                        companyId = userDetails.CompanyId;
                    }
                    var currentPage = new ModelProcessDetails();
                    return currentPage.GetOrganization(companyId);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private ListXeroOrganization GetOrganization(int companyId)
        {
            try
            {

                ListXeroOrganization lstXeroOrg = new ListXeroOrganization();
                using (var dbContext = new ClearJellyEntities())
                {
                    _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Executio n of Admin_ModelProcessDetails GetFilters  event started.");

                    var xero_OrgList = dbContext.Xero_User_Org.Where(x => x.CompanyID == companyId).ToList().Select(x => new Xero_User_Org
                    {
                        OrgName = x.OrgName,
                        CompanyID = x.CompanyID
                    }).ToList();

                    lstXeroOrg.lst_Xero_Org = xero_OrgList;
                    return lstXeroOrg;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetFilters event completed.", ex);
                throw;
            }
        }


        public static string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public ModelProcessListViewModel GetModelProcessData(int userId, string orgName, string startDate, string endDate)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails  GetModelProcessData event started.");

                using (var dbContext = new ClearJellyEntities())
                {
                    List<ModelProcessViewModel> listModelProcess = new List<ModelProcessViewModel>();
                    List<ModelProcess> lstmodelProcess = new List<ModelProcess>();

                    lstmodelProcess = GetModelProcessListData(userId, orgName, startDate, endDate);

                    var lstModelProcess = lstmodelProcess.GroupBy(ac => new
                    {
                        ac.OrganizationName,
                        ac.UserId,
                    }).ToList();

                    foreach (var item in lstModelProcess)
                    {
                        ModelProcessViewModel modelProcess = new ModelProcessViewModel();
                        modelProcess.CreatedDate = item.OrderByDescending(x => x.StartDate).First().CreatedDate != null ? item.OrderByDescending(x => x.StartDate).First().CreatedDate.Value.ToShortDateString() : string.Empty;
                        modelProcess.StartDate = item.OrderByDescending(x => x.StartDate).First().CreatedDate != null ? item.OrderByDescending(x => x.StartDate).First().StartDate.ToShortDateString() : string.Empty;
                        modelProcess.EndDate = item.OrderByDescending(x => x.StartDate).First().EndDate != null ? item.OrderByDescending(x => x.StartDate).First().EndDate.Value.ToShortDateString() : string.Empty;
                        modelProcess.FromDate = item.OrderByDescending(x => x.StartDate).First().FromDate != null ? item.OrderByDescending(x => x.StartDate).First().FromDate.Value.ToShortDateString() : string.Empty;
                        modelProcess.ToDate = item.OrderByDescending(x => x.StartDate).First().ToDate != null ? item.OrderByDescending(x => x.StartDate).First().ToDate.Value.ToShortDateString() : string.Empty;
                        modelProcess.XeroErrorCode = item.OrderByDescending(x => x.StartDate).First().XeroErrorCode;
                        modelProcess.SuccessOrFail = item.OrderByDescending(x => x.StartDate).First().XeroErrorCode == (int)XeroErrorCodes.Ok ? "Success" : "Fail";
                        modelProcess.OrganizationName = item.OrderByDescending(x => x.StartDate).First().OrganizationName;
                        modelProcess.UserName = item.OrderByDescending(x => x.StartDate).First().User.Email;
                        modelProcess.UserId = item.OrderByDescending(x => x.StartDate).First().UserId;
                        modelProcess.ModelProcessId = item.OrderByDescending(x => x.StartDate).First().ModelProcessId;
                        modelProcess.Status = Enum.GetName(typeof(StatusEnum), item.OrderByDescending(x => x.StartDate).First().Status);
                        modelProcess.ProcessType = Enum.GetName(typeof(ModelType), item.OrderByDescending(x => x.StartDate).First().ProcessType);
                        listModelProcess.Add(modelProcess);
                    }

                    ModelProcessListViewModel modelProcesslistVm = new ModelProcessListViewModel();
                    if (userId > 0)
                    {
                        var userDetails = dbContext.Users.First(x => x.UserId == userId);

                        if (orgName == null)
                        {
                            modelProcesslistVm.lstXeroOrg = GetOrganization(userDetails.CompanyId);
                        }
                    }
                    modelProcesslistVm.lstModelProcessVm = listModelProcess;
                    return modelProcesslistVm;
                }


            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails  GetModelProcessData event started.");
                throw;
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static List<ModelProcessViewModel> ShowModelProcessList(string modelProcessId)
        {

            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails ShowModelProcessList event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    int processId = int.Parse(modelProcessId);
                    List<ModelProcessViewModel> lstmodelProcessVm = new List<ModelProcessViewModel>();
                    var modelProcessDetail = dbContext.ModelProcesses.FirstOrDefault(x => x.ModelProcessId == processId);
                    var lstModelProcess = dbContext.ModelProcesses.Where(x => x.OrganizationName == modelProcessDetail.OrganizationName && x.UserId == modelProcessDetail.UserId).OrderByDescending(x => x.StartDate).Take(200).ToList();

                    foreach (var item in lstModelProcess)
                    {
                        ModelProcessViewModel modelProcessVm = new ModelProcessViewModel();

                        modelProcessVm.StartDate = item.StartDate != null ? item.StartDate.ToShortDateString() : string.Empty;
                        modelProcessVm.CreatedDate = item.CreatedDate != null ? item.CreatedDate.Value.ToShortDateString() : string.Empty;
                        modelProcessVm.EndDate = item.EndDate != null ? item.EndDate.Value.ToShortDateString() : string.Empty;
                        modelProcessVm.FromDate = item.FromDate != null ? item.FromDate.Value.ToShortDateString() : string.Empty;
                        modelProcessVm.ToDate = item.ToDate != null ? item.ToDate.Value.ToShortDateString() : string.Empty;
                        modelProcessVm.XeroErrorCode = item.XeroErrorCode;
                        modelProcessVm.OrganizationName = item.OrganizationName;
                        modelProcessVm.UserName = item.User.Email;
                        modelProcessVm.UserId = item.UserId;
                        modelProcessVm.ModelProcessId = item.ModelProcessId;
                        modelProcessVm.Status = Enum.GetName(typeof(StatusEnum), item.Status);
                        modelProcessVm.ProcessType = Enum.GetName(typeof(ModelType), item.ProcessType);
                        lstmodelProcessVm.Add(modelProcessVm);
                    }
                    return lstmodelProcessVm;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails ShowModelProcessList event completed.", ex);
                throw;
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static ModelProcessListViewModel GetModelProcessFilterList(int userId, string orgName, string startDate, string endDate)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetModelProcessFilterList event started.");
                var currentPage = new ModelProcessDetails();
                ModelProcessListViewModel userList = new ModelProcessListViewModel();
                if (startDate == null && endDate == null)
                {
                    if (userId != 0)
                    {

                        userList = currentPage.GetModelProcessData(userId, orgName, startDate, endDate);
                    }
                    else
                    {
                        userList = currentPage.GetModelProcessData(userId, orgName, startDate, endDate);
                    }
                }
                else
                {
                    userList = currentPage.GetModelProcess(userId, orgName, startDate, endDate);
                }



                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetModelProcessFilterList event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static ModelProcessListViewModel GetModelProcessFilterListPost(int userId, string orgName, string startDate, string endDate)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetModelProcessFilterListPost event started.");
                var currentPage = new ModelProcessDetails();
                ModelProcessListViewModel userList = new ModelProcessListViewModel();
                if (startDate == null && endDate == null)
                {
                    if (userId != 0)
                    {

                        userList = currentPage.GetModelProcessData(userId, orgName, startDate, endDate);
                    }
                    else
                    {
                        userList = currentPage.GetModelProcessData(userId, orgName, startDate, endDate);
                    }
                }
                else
                {
                    userList = currentPage.GetModelProcess(userId, orgName, startDate, endDate);
                }
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetModelProcessFilterListPost event completed.", ex);
                throw ex;
            }
        }

        private ModelProcessListViewModel GetModelProcess(int userId, string orgName, string startDate, string endDate)
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails GetUsers  event started.");
                    if (Session["Jelly_Admin_user"] == null)
                    {
                        throw new Exception("Invalid session Jelly_user");
                    }
                    var email = Session["Jelly_Admin_user"].ToString();

                    List<ModelProcess> modelProcesses = new List<ModelProcess>();
                    if (userId != 0)
                    {
                        if (startDate != null)
                        {
                            DateTime strtDate = DateTime.Parse(startDate);

                            if (startDate != null && endDate == null)
                            {
                                if (orgName != null)
                                {
                                    modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && x.OrganizationName == orgName && (EntityFunctions.TruncateTime(x.StartDate) == strtDate.Date)).ToList();
                                }
                                else
                                {
                                    modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && (EntityFunctions.TruncateTime(x.StartDate) == strtDate.Date)).ToList();
                                }
                            }
                            else
                            {
                                DateTime edDate = DateTime.Parse(endDate);
                                if (orgName != null)
                                {
                                    modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && x.OrganizationName == orgName && (EntityFunctions.TruncateTime(x.StartDate) >= strtDate.Date) && (EntityFunctions.TruncateTime(x.StartDate) <= edDate.Date)).ToList();
                                }
                                else
                                {
                                    modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && (EntityFunctions.TruncateTime(x.StartDate) >= strtDate.Date) && (EntityFunctions.TruncateTime(x.StartDate) <= edDate.Date)).ToList();
                                }
                            }
                        }
                        else if (orgName != null)
                        {
                            modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && x.OrganizationName == orgName).ToList();
                        }
                        else
                        {
                            modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId).ToList();
                        }

                    }
                    else if (userId == 0 && startDate != null && endDate == null)
                    {
                        DateTime strtDate = DateTime.Parse(startDate);
                        modelProcesses = dbContext.ModelProcesses.Where(x => EntityFunctions.TruncateTime(x.StartDate) == strtDate.Date).ToList();
                    }
                    else if (userId == 0 && startDate != null && endDate != null)
                    {
                        DateTime strtDate = DateTime.Parse(startDate);
                        DateTime edDate = DateTime.Parse(endDate);
                        modelProcesses = dbContext.ModelProcesses.Where(x => EntityFunctions.TruncateTime(x.StartDate) >= strtDate.Date && EntityFunctions.TruncateTime(x.StartDate) <= edDate.Date).ToList();
                    }

                    var modelProcessList = modelProcesses.AsEnumerable().Select(x => new ModelProcessViewModel
                    {
                        CreatedDate = x.CreatedDate != null ? x.CreatedDate.Value.ToShortDateString() : string.Empty,
                        EndDate = x.EndDate != null ? x.EndDate.Value.ToShortDateString() : string.Empty,
                        FromDate = x.FromDate != null ? x.FromDate.Value.ToShortDateString() : string.Empty,
                        ToDate = x.ToDate != null ? x.ToDate.Value.ToShortDateString() : string.Empty,
                        XeroErrorCode = x.XeroErrorCode,
                        OrganizationName = x.OrganizationName,
                        UserName = x.User.Email,
                        UserId = x.UserId,
                        ModelProcessId = x.ModelProcessId,
                        Status = Enum.GetName(typeof(StatusEnum), x.Status),
                        ProcessType = Enum.GetName(typeof(ModelType), x.ProcessType),

                    }).ToList();
                    ModelProcessListViewModel modelProcesslistVm = new ModelProcessListViewModel();
                    if (userId > 0)
                    {
                        var userDetails = dbContext.Users.First(x => x.UserId == userId);

                        if (orgName == null)
                        {
                            modelProcesslistVm.lstXeroOrg = GetOrganization(userDetails.CompanyId);
                        }
                    }
                    modelProcesslistVm.lstModelProcessVm = modelProcessList;
                    return modelProcesslistVm;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ModelProcessDetails ActivateUser event completed.", ex);
                throw ex;
            }
        }


        public List<ModelProcess> GetModelProcessListData(int userId, string orgName, string startDate, string endDate)
        {

            List<ModelProcess> modelProcesses = new List<ModelProcess>();
            using (var dbContext = new ClearJellyEntities())
            {

                if (userId != 0)
                {
                    if (startDate != null)
                    {
                        DateTime strtDate = DateTime.Parse(startDate);
                        DateTime edDate = DateTime.Parse(endDate);
                        if (orgName != null)
                        {
                            modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && x.OrganizationName == orgName && (EntityFunctions.TruncateTime(x.StartDate) >= strtDate.Date && EntityFunctions.TruncateTime(x.StartDate) <= edDate.Date)).ToList();

                        }
                        else
                        {
                            modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && (EntityFunctions.TruncateTime(x.StartDate) >= strtDate.Date && EntityFunctions.TruncateTime(x.StartDate) <= edDate.Date)).ToList();
                        }
                    }
                    else if (orgName != null)
                    {
                        modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId && x.OrganizationName == orgName).ToList();
                    }
                    else
                    {
                        modelProcesses = dbContext.ModelProcesses.Where(x => x.UserId == userId).ToList();
                    }

                }
                else
                {
                    modelProcesses = dbContext.ModelProcesses.ToList();
                }
                return modelProcesses.Select(x => new ModelProcess
                {
                    User = x.User,
                    ModelProcessId = x.ModelProcessId,
                    CreatedDate = x.CreatedDate,
                    EndDate = x.EndDate,
                    FromDate = x.FromDate,
                    OrganizationName = x.OrganizationName.Trim(),
                    ProcessType = x.ProcessType,
                    StartDate = x.StartDate,
                    Status = x.Status,
                    UserId = x.UserId,
                    XeroErrorCode = x.XeroErrorCode,
                    ToDate = x.ToDate
                }).ToList();
            }
        }
    }
}