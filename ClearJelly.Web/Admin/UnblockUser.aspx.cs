﻿using ClearJelly.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.ViewModels.Admin;

namespace ClearJelly.Web.Admin
{
    public partial class UnblockUser : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

        public UnblockUser()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of UnblockUser Page_Load event completed.", ex);
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UnblockUserModel> GetUserList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser GetUserList event started.");
                var currentPage = new UnblockUser();
                var userList = currentPage.GetUsers();
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser GetUserList event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        public static bool UnblockUserMethod(int userId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser ActivateUser  event started.");
                var currentPage = new UnblockUser();
                return currentPage.UnblockSelectedUser(userId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser UnblockUser event completed.", ex);
                throw ex;
            }
        }

        private List<UnblockUserModel> GetUsers()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser GetUsers  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var companyUsers = _clearJellyContext.Users.Where(x => !x.IsDeleted && x.LastloginFailed != null).ToList();
                var companyUsersData = companyUsers.Select(x => new UnblockUserModel
                {
                    UserId = x.UserId,
                    isBlock = x.LastloginFailed == null,
                    Name = x.FirstName + " " + x.LastName,
                    Email = x.Email,
                    blockTime = x.LastloginFailed != null ? x.LastloginFailed.Value.ToString("dd-MMM-yyyy : HH:mm") : null
                }).ToList();
                return companyUsersData;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser UnblockUser event completed.", ex);
                throw ex;
            }
        }

        private bool UnblockSelectedUser(int userId)
        {
            try
            {
                var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                if (user != null)
                {
                    user.LastloginFailed = null;
                    _clearJellyContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UnblockUser UnblockSelectedUser event completed.", ex);
                throw ex;
            }

        }
    }
}