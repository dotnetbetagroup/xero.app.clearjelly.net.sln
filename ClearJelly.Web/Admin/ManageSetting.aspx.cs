﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClearJelly.Entities;

namespace ClearJelly.Web.Admin
{
    public partial class Admin_ManageSetting : System.Web.UI.Page
    {
        private static NLog.Logger _logger;
        private readonly ClearJellyEntities _clearJellyContext;
        public Admin_ManageSetting()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _clearJellyContext = new ClearJellyEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static bool UpdateSettingValue(int key, string keyValue)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ManageSetting UpdateSettingValue  event started.");
                return ClearJelly.Web.Common.Common.UpdateSetting(key, keyValue);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ManageSetting UpdateSettingValue event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<Setting> GetSettingList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ManageSetting GetSettingList event started.");
                var currentPage = new Admin_ManageSetting();
                var keyList = currentPage.GetAdminSetting();
                return keyList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ManageSetting GetSettingList event completed.", ex);
                throw;
            }

        }

        private List<Setting> GetAdminSetting()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ManageSetting GetAdminSetting event started.");
                var keys = _clearJellyContext.Settings.ToList();
                return keys;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin_ManageSetting GetAdminSetting event completed.", ex);
                throw ex;
            }
        }
    }
}