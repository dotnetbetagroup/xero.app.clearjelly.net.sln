﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="UnblockUser.aspx.cs" CodeFile="UnblockUser.aspx.cs" Inherits="ClearJelly.Web.Admin.UnblockUser" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Unblock Users</h1>
            </div>

            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>  
               <strong>Success!</strong> User Unblocked successfully.
            </div>
            <div class="alert alert-danger display-none" role="alert">
               <a class="close" onclick="$('.alert').hide()">×</a>  
               <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div id="unblockUserdv">
            </div>
        </div>
    </div>
    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/UnblockUser.js">
    </script>
</asp:Content>
