﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="UserActivities.aspx.cs" CodeFile="UserActivities.aspx.cs" Inherits="ClearJelly.Web.Admin.UserActivities" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
   
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>User Activities</h1>
            </div>
            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!&nbsp; </strong><span id="successMsg"></span>
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!&nbsp; </strong><span id="FailureMsg">Something went wrong, please try again.</span>
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div class="col-md-12 gray-bg padding-top-10">
                <input type="hidden" id="hdnEmailTemplateId" runat="server" />
                <div class="row centerLabel">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group ">
                            <asp:Label runat="server" AssociatedControlID="FilterDropDown" CssClass="control-label col-xs-3">Filter </asp:Label>
                            <div class="col-xs-8">
                                <asp:DropDownList ID="FilterDropDown" CssClass="form-control pointerCursor width200" runat="server" DataTextField="Name" DataValueField="Id">
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div id="userdv">
            </div>

        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="subscriptionPopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">User Subscriptions </h4>
                </div>
                <div class="modal-body">
                    <div id="userSubscriptiondv">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" tabindex="-1" role="dialog" id="SuspendedsubscriptionPopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Outstanding Balance </h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success display-none" role="alert">
                        <a class="close" onclick="$('.alert').hide()">×</a>
                        <strong>Success!&nbsp; </strong><span id="suspendsuccessMsg"></span>
                    </div>
                    <div class="alert alert-danger display-none" role="alert">
                        <a class="close" onclick="$('.alert').hide()">×</a>
                        <strong>Error!&nbsp; </strong><span id="suspendFailureMsg">Something went wrong, please try again.</span>
                    </div>
                    <div id="suspendeduserSubscriptiondv">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" tabindex="-1" role="dialog" id="extendSubscriptionPopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Extend Trial Subscription Days </h4>
                </div>
                <div class="modal-body">
                    <div id="userSubscriptiondvs">
                    </div>
                </div>

                <div class="row centerLabel" id="extendUpdateDiv">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="control-label col-xs-3">Extend Trial Period (in Days)<span class="mandatory">&nbsp;*</span></asp:Label>
                            <div class="col-xs-4">
                                <asp:TextBox runat="server" ID="numberOfDays" CssClass="form-control numeric" />
                                <span class="mandatory display-none validationMsgs" id="ExtendDaysValidation">Please enter Days.</span>

                            </div>
                            <span style="display: none" id="ActiveStatus"></span>
                        </div>
                    </div>
                </div>
                <div class="row centerLabel display-none" id="StatusValidation">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <b>Note:</b><br />
                                Extend trial subscriptions is not allowed because trial subscription is not active.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" id="saveDiv">
                    <button type="button" id="btnExtendSave" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" tabindex="-1" role="dialog" id="suspendUserPopup">
        <input type="hidden" id="haveAnyAdditionaluser" />
        <input type="hidden" id="suspendUserId" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="suspendPoupheading">Suspend User</h4>
                </div>
                <div class="modal-body">
                    <div class="row centerLabel">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label col-xs-4">Suspend Date</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="suspendedDate" disabled="disabled" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row centerLabel">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label col-xs-4">Suspend Reason<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="suspendReason" CssClass="form-control" />
                                    <span class="mandatory display-none validationMsgs" id="suspendReasonMsg">Please enter reason.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row centerLabel" id="associateUserNote">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <b>Note:</b><br />
                                    The User has other associated user(s) with the company. 
                                    Suspending this user will also suspend the other associated user(s).
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnSave" class="btn btn-success">Suspend</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <script type="text/javascript" src="../Scripts/XeroWebApp/UserActivities.js">
    </script>
</asp:Content>

