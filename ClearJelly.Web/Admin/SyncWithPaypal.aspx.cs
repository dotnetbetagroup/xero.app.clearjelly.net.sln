﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.Entities;
using ClearJelly.ViewModels.Admin;
using ClearJelly.XeroApp;
using PaypalIntegration.PayPallHelper;

namespace ClearJelly.Web.Admin
{
    public partial class SyncWithPaypal : System.Web.UI.Page
    {
        private static NLog.Logger _logger;

        public SyncWithPaypal()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static void btnSyncPaypal_Click()
        {
            try
            {
                var currentpage = new SyncWithPaypal();
                currentpage.SyncWithPaypalMethod();
            }
            catch (Exception)
            {

                throw;
            }
        }


        public void SyncWithPaypalMethod()
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    var lstActiveUser = dbContext.Users.Where(x => x.IsActive == true).ToList();

                    foreach (var item in lstActiveUser)
                    {
                        var activeCompanySubscription = dbContext.CompanySubscriptions.Where(x => x.CompanyId == item.CompanyId && x.EndDate == null && x.RecurringProfileId != null).ToList();
                        foreach (var subscription in activeCompanySubscription)
                        {
                            var expChkt = new PayPalExpressCheckout();
                            var resRecurring = expChkt.GetRecurringPaymentsProfileDetails(subscription.RecurringProfileId);
                            var status = ((PayPalPaymentResponse)resRecurring).PayPalResponse["STATUS"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["STATUS"].ToLower() : null;
                            var lastPaymentAmount = ((PayPalPaymentResponse)resRecurring).PayPalResponse["REGULARAMT"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["REGULARAMT"].ToLower() : null;
                            var NoofCycles = ((PayPalPaymentResponse)resRecurring).PayPalResponse["NUMCYCLESCOMPLETED"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["NUMCYCLESCOMPLETED"].ToLower() : null;
                            var recurringStartDate = ((PayPalPaymentResponse)resRecurring).PayPalResponse["PROFILESTARTDATE"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["PROFILESTARTDATE"].ToLower() : null;
                            var PaypalSyncUpdate = ConfigurationManager.AppSettings["PaypalSyncUpdate"];
                            if (PaypalSyncUpdate == null)
                            {
                                throw new Exception("PaypalSyncUpdate is not defined.");
                            }

                            if (bool.Parse(PaypalSyncUpdate))
                            {

                                if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Expired).ToLower())
                                {
                                    if (status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower())
                                    {
                                        DeactivateSubscription(subscription.RecurringProfileId);
                                        SuspendUser(subscription.RecurringProfileId, "User is suspended due to maximum failed payments attempt by paypal");
                                    }
                                    else if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Expired).ToLower())
                                    {
                                        DeactivateSubscription(subscription.RecurringProfileId);
                                    }

                                    InsertintoSubscriptionHistory(subscription, status, NoofCycles, lastPaymentAmount, recurringStartDate);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void InsertintoSubscriptionHistory(CompanySubscription subscription, string status, string NoofCycles, string lastPaymentAmount, string recurringStartDate)
        {
            try
            {
                _logger.Info(" ~Execution of SyncWithPaypal InsertintoSubscriptionHistory event started.");
                using (var dbContext = new ClearJellyEntities())
                {

                    int statusPaypal = 0;
                    if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower())
                    {
                        statusPaypal = (int)UserStatus.Cancelled;
                    }
                    else if (status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower())
                    {
                        statusPaypal = (int)UserStatus.Suspended;

                    }
                    else if (status == Enum.GetName(typeof(UserStatus), UserStatus.Expired).ToLower())
                    {
                        statusPaypal = (int)UserStatus.Expired;
                    }
                    var recordExists = dbContext.PaypalSyncDataHistories.Any(x => x.RecurringProfileId == subscription.RecurringProfileId);
                    if (!recordExists)
                    {
                        PaypalSyncDataHistory companySubscriptionHistory = new PaypalSyncDataHistory();
                        companySubscriptionHistory.CompanySubscriptionId = subscription.CompanySubscriptionId;
                        companySubscriptionHistory.RecurringProfileId = subscription.RecurringProfileId;
                        companySubscriptionHistory.CreatedDate = DateTime.Now;
                        companySubscriptionHistory.HistoryId = Guid.NewGuid();
                        companySubscriptionHistory.Status = statusPaypal;
                        companySubscriptionHistory.LastPaymentAmount = decimal.Parse(lastPaymentAmount);
                        companySubscriptionHistory.NoOfCycles = int.Parse(NoofCycles);
                        dbContext.PaypalSyncDataHistories.Add(companySubscriptionHistory);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of SyncWithPaypal InsertintoSubscriptionHistory event completed.", ex);
                throw;
            }
        }



        private void SuspendUser(string recurringProfileId, string message)
        {
            try
            {
                _logger.Info(" ~Execution of SyncWithPaypal SuspendUser event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var currentDate = DateTime.Now.Date;
                    var companySubscription = dbContext.CompanySubscriptions.First(x => x.RecurringProfileId == recurringProfileId);
                    var getUserCompany = dbContext.CompanySubscriptions.Any(x => x.CompanyId == companySubscription.CompanyId && x.IsActive == true && x.RecurringProfileId != recurringProfileId);
                    if (!getUserCompany)
                    {
                        var getUser = dbContext.Users.FirstOrDefault(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                        Common.Common.SuspendUser(getUser.Email, getUser.UserId, message);
                    }
                    var recurringPayment = dbContext.RecurringPayments.FirstOrDefault(x => x.RecurringProfileId == recurringProfileId);

                }
            }
            catch (Exception ex)
            {
                _logger.Error("~Execution of SyncWithPaypal SuspendUser event completed.", ex);
                throw;
            }
        }


        public static void DeactivateSubscription(string recurringProfileId)
        {
            try
            {
                _logger.Info(" ~Execution of SyncWithPaypal DeactivateSubscription event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var companySubscription = dbContext.CompanySubscriptions.First(x => x.RecurringProfileId == recurringProfileId);
                    companySubscription.IsActive = false;
                    dbContext.SaveChanges();
                    var getUserCompnay = dbContext.CompanySubscriptions.Any(x => x.CompanyId == companySubscription.CompanyId && x.IsActive == true && x.RecurringProfileId != recurringProfileId);
                    if (!getUserCompnay)
                    {
                        var getUserDetails = dbContext.Users.FirstOrDefault(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                        Common.Common.DeActivateSQLUser(getUserDetails.Email, getUserDetails.Email);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of SyncWithPaypal DeactivateSubscription event completed.", ex);
                throw;
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<SubscriptionHistoryViewModel> GetPaypalSubscriptionList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of SyncWithPaypal  GetPaypalSubscriptionList event started.");
                var currentPage = new SyncWithPaypal();
                var companySubscription = currentPage.GetCompanySubscriptionHistory();
                return companySubscription;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of SyncWithPaypal  GetPaypalSubscriptionList event completed.", ex);
                throw;
            }

        }

        private List<SubscriptionHistoryViewModel> GetCompanySubscriptionHistory()
        {
            try
            {

                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of SyncWithPaypal GetCompanySubscriptionHistory event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    //var companySubscription = dbContext.CompanySubscriptionHistories.OrderByDescending(x => x.LastPaymentDate).GroupBy(x=>x.CompanySubscriptionId).Select(y => new SubscriptionHistoryViewModel
                    //{
                    //    RecurringProfileId = y.FirstOrDefault().CompanySubscription.RecurringProfileId,
                    //    Amount = y.FirstOrDefault().LastPaymentAmount.ToString(),
                    //    LastPaymentOn = y.FirstOrDefault().LastPaymentDate
                    //}).ToList();

                    List<SubscriptionHistoryViewModel> lstSubscriptionHistory = new List<SubscriptionHistoryViewModel>();
                    var date = dbContext.PaypalSyncDataHistories.OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                    if (date != null)
                    {
                        var companySubscription = dbContext.PaypalSyncDataHistories.GroupBy(x => x.CompanySubscriptionId).ToList();
                        foreach (var item in companySubscription)
                        {

                            SubscriptionHistoryViewModel subscriptionhisVM = new SubscriptionHistoryViewModel();
                            subscriptionhisVM.LastUpdatedDate = item.OrderByDescending(x => x.CreatedDate).FirstOrDefault().CreatedDate.ToShortDateString();
                            subscriptionhisVM.CompanyName = item.FirstOrDefault().CompanySubscription.Company.Name.ToString();
                            subscriptionhisVM.UserName = item.FirstOrDefault().CompanySubscription.Company.Users.First(x => x.IsAdmin == true).Email;
                            subscriptionhisVM.Amount = item.OrderByDescending(x => x.CreatedDate).FirstOrDefault().LastPaymentAmount.ToString();
                            subscriptionhisVM.RecurringProfileId = item.FirstOrDefault().CompanySubscription.RecurringProfileId;
                            subscriptionhisVM.NoOfCycles = item.OrderByDescending(x => x.CreatedDate).FirstOrDefault().NoOfCycles;
                            lstSubscriptionHistory.Add(subscriptionhisVM);
                        }
                    }
                    return lstSubscriptionHistory;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of SyncWithPaypal GetCompanySubscriptionHistory event completed.", ex);
                throw ex;
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static List<SubscriptionHistoryViewModel> ShowReccuringProlileList(string recurringProfileId)
        {

            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of SyncWithPaypal ShowReccuringProlileList event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    List<SubscriptionHistoryViewModel> lstsubscriptionHistoryVm = new List<SubscriptionHistoryViewModel>();
                    var companySubscriptionList = dbContext.PaypalSyncDataHistories.Where(x => x.CompanySubscription.RecurringProfileId == recurringProfileId).ToList();
                    foreach (var item in companySubscriptionList)
                    {
                        SubscriptionHistoryViewModel subscriptionHistoryVm = new SubscriptionHistoryViewModel();
                        subscriptionHistoryVm.HistoryId = item.HistoryId;
                        subscriptionHistoryVm.LastPaymentAmount = item.LastPaymentAmount;
                        subscriptionHistoryVm.NoOfCycles = item.NoOfCycles;
                        subscriptionHistoryVm.RecurringProfileId = item.CompanySubscription.RecurringProfileId;
                        subscriptionHistoryVm.Status = GetEnumDescription((PaypalTxnCode)item.Status);
                        lstsubscriptionHistoryVm.Add(subscriptionHistoryVm);
                    }
                    return lstsubscriptionHistoryVm;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of SyncWithPaypal ShowReccuringProlileList event completed.", ex);
                throw;
            }
        }


        public static string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }
}