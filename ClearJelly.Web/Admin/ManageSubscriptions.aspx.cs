﻿using ClearJelly.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ClearJelly.ViewModels.Admin;

namespace ClearJelly.Web.Admin
{
    public partial class Admin_ManageSubscriptions : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

        public Admin_ManageSubscriptions()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_Admin_user"] == null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions Page_Load event completed.", ex);
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<ManageSubscriptionViewModel> GetSubscriptionList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetSubscriptionList event started.");
                var currentPage = new Admin_ManageSubscriptions();
                var userList = currentPage.GetSubscriptionTypes();
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetSubscriptionList event completed.", ex);
                throw ex;
            }
        }

        private List<ManageSubscriptionViewModel> GetSubscriptionTypes()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetSubscriptionTypes  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var currentDate = DateTime.Now;
                var subscriptionTypes = _clearJellyContext.SubscriptionTypes
                        .Select(x => new ManageSubscriptionViewModel
                        {
                            AdditionalUserFee = x.AdditionalUserFee,
                            Description = x.Description != null ? x.Description : string.Empty,
                            Entities = x.Entities,
                            MonthlyFee = x.MonthlyFee,
                            SubscriptionTypeId = x.SubscriptionTypeId,
                            TypeName = x.TypeName,
                            YearlyContractFee = x.YearlyContractFee,
                            SubscribedCompanies = x.CompanySubscriptions.Count(),
                            IsActive = x.IsActive
                        }).ToList();
                return subscriptionTypes;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetSubscriptionTypes event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        public static bool SaveSubscription(ManageSubscriptionViewModel model)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions SaveSubscription event started.");
                var loggedinUser = HttpContext.Current.Session["Jelly_Admin_user"];
                if (loggedinUser == null)
                {
                    throw new Exception("Invalid session Jelly_Admin_user.");
                }
                return Common.Common.SaveSubscription(loggedinUser.ToString(), model);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions SaveSubscription event completed.", ex);
                throw ex;
            }
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static ManageSubscriptionViewModel GetEditSubscriptionData(int subscriptionTypeId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetEditSubscriptionData event started.");
                var currentPage = new Admin_ManageSubscriptions();
                var subscriptionType = currentPage.GetEditSubscription(subscriptionTypeId);
                return subscriptionType;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetEditSubscriptionData event completed.", ex);
                throw ex;
            }
        }

        private ManageSubscriptionViewModel GetEditSubscription(int subscriptionTypeId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetEditSubscription  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var subscriptionType = _clearJellyContext.SubscriptionTypes.FirstOrDefault(a => a.SubscriptionTypeId == subscriptionTypeId);
                if (subscriptionType != null)
                {
                    return new ManageSubscriptionViewModel
                    {
                        AdditionalUserFee = subscriptionType.AdditionalUserFee,
                        Description = subscriptionType.Description,
                        Entities = subscriptionType.Entities,
                        MonthlyFee = subscriptionType.MonthlyFee,
                        SubscriptionTypeId = subscriptionType.SubscriptionTypeId,
                        TypeName = subscriptionType.TypeName,
                        YearlyContractFee = subscriptionType.YearlyContractFee,
                        IsActive = subscriptionType.IsActive,
                    };
                }
                else { return new ManageSubscriptionViewModel(); }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions GetEditSubscription event completed.", ex);
                throw ex;
            }
        }
        //

        [WebMethod]
        public static bool Delete(int subscriptionTypeId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions Delete  event started.");
                var currentPage = new Admin_ManageSubscriptions();
                return currentPage.DeleteSubscription(subscriptionTypeId);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions Delete event completed.", ex);
                throw ex;
            }
        }


        private bool DeleteSubscription(int subscriptionTypeId)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions DeleteSubscription  event started.");
                var subscriptionType = _clearJellyContext.SubscriptionTypes.FirstOrDefault(s => s.SubscriptionTypeId == subscriptionTypeId);
                if (subscriptionType.CompanySubscriptions.Any())
                {
                    return false;
                }
                _clearJellyContext.SubscriptionTypes.Remove(subscriptionType);
                _clearJellyContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of ManageSubscriptions DeleteSubscription event completed.", ex);
                throw ex;
            }
        }
    }
}