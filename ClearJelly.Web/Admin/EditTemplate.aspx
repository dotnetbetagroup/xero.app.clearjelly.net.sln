﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="EditTemplate.aspx.cs" CodeBehind="EditTemplate.aspx.cs"Inherits="ClearJelly.Web.Admin.EditTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="section">
        <div class="col-md-12 gray-bg padding-top-10">
            <div class="login-title pb10">
                <h1>Edit Email Template </h1>
            </div>

            <div id="successDiv" runat="server" class="alert alert-success" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!</strong> Attachment file(s) updated successfully it will update finally when you update template.
            </div>

            <div id="errorDiv" runat="server" class="alert alert-danger" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:ValidationSummary runat="server" CssClass="text-danger" />
            <input type="hidden" id="hdnEmailTemplateId" runat="server" />
            <div class="row centerLabel">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="templateName" CssClass="control-label col-xs-2">Category</asp:Label>
                        <div class="col-xs-10">
                            <asp:DropDownList ID="CategoryDropDown" ValidationGroup="g1" CssClass="form-control pointerCursor width325" runat="server" DataTextField="Name" DataValueField="Id">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator InitialValue="-1" ID="Req_ID" Display="Dynamic"
                                ValidationGroup="g1" runat="server" ControlToValidate="CategoryDropDown"
                                CssClass="text-danger" ErrorMessage="Please select category."></asp:RequiredFieldValidator>

                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="templateName" CssClass="control-label col-xs-2">Name</asp:Label>
                        <div class="col-xs-10">
                            <asp:TextBox runat="server" CssClass="form-control" ID="templateName"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="text-danger" Display="Dynamic" ID="templateNamevalidation" ValidationGroup="g1" runat="server" ControlToValidate="templateName" ErrorMessage="Please enter Template Name.">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Subject" CssClass="control-label col-xs-2">Subject</asp:Label>
                        <div class="col-xs-10">
                            <asp:TextBox runat="server" CssClass="form-control" ID="subject"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="text-danger" Display="Dynamic" ID="RequiredFieldValidator1" ValidationGroup="g1" runat="server" ControlToValidate="subject" ErrorMessage="Please enter Subject.">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>

                </div>

                <div class="col-md-6 col-xs-12">
                    <%--<div class="registration-subtitle pb10">
                        <h4>Parameters:</h4>
                    </div>--%>
                    <table class="table table-striped cust-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Parameter Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% if (templateParameters.Count() == 0)
                                { %>
                            <tr class="odd">
                                <td valign="top" colspan="3" class="dataTables_empty">No data available.</td>
                            </tr>
                            <% } %>
                            <% foreach (var parameter in templateParameters)
                                { %>
                            <tr>
                                <td><a class="pointerCursor insertclick">Insert</a></td>
                                <td class="name">{<%=parameter.Name%>}</td>
                                <td><%= parameter.Description %> </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="registration-subtitle pb10 form-group">
                <asp:Label runat="server" AssociatedControlID="ContentArea" CssClass="control-label col-xs-2">Content</asp:Label>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <asp:TextBox ID="ContentArea" CssClass="form-control" TextMode="multiline" Columns="50" Rows="20" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="templateName" CssClass="control-label col-xs-2">Active</asp:Label>
                        <div class="col-xs-10">
                            <asp:CheckBox runat="server" ID="chkActive" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="registration-subtitle pb10 form-group">
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UploadImages" CssClass="control-label col-xs-2">Attachment</asp:Label>
                                <div class="col-xs-10">
                                    <asp:FileUpload runat="server" ID="UploadImages" AllowMultiple="true" onchange="uploadAttachmentchange()" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 display-none">
                            <asp:Button runat="server" ID="uploadedFile" Text="Upload" CssClass="btn btn-success btn-block" OnClick="uploadFile_Click" />
                        </div>
                    </div>
                </div>

            </div>

            <div class="row mt15">
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-sm-11 col-sm-offset-1">
                            <% foreach (var attachment in templateAttachment)
                                { %>
                            <div class="col-sm-3">
                                <span><%=attachment.FileName%> </span>
                                <span>
                                    <asp:ImageButton runat="server" class="attachmentFile" OnClientClick="setDeleteImageId(this)" OnClick="deleteAttachment_click" ImageUrl="~/Images/cross.ico" />
                                    <span class="display-none emailAttachmentId"><%=attachment.EmailAttachmentId%></span>
                                    <span class="display-none isSaved"><%=attachment.IsSaved%></span>
                                </span>
                            </div>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row mt15">
                <div class="form-group">
                    <div class="col-md-6 col-xs-12"></div>
                    <div class="col-md-6 col-xs-12">
                        <div class="col-md-3 col-md-offset-6">
                            <asp:Button runat="server" Text="Update" ValidationGroup="g1" CausesValidation="True" OnClick="btnUpdateTemplate_Click" CssClass="btn btn-success btn-block" ID="btnUpdateTemplate" />
                        </div>
                        <div class="col-md-3">
                            <asp:HyperLink runat="server" NavigateUrl="~/Admin/EmailTemplate.aspx" CssClass="btn btn-default btn-block">
                                        Cancel
                            </asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="imageDeleteId" runat="server" />
        <asp:HiddenField ID="isSaved" runat="server" />
    </div>

    <script src="../tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="../tinymce/js/tinymce/jquery.tinymce.min.js"></script>

    <script type="text/javascript">


        $('.insertclick').click(function () {
            var parameterName = $(this).parent().siblings('.name').text();
            insertEmoticonAtTextareaCursor('insertPattern', parameterName);
        });

        function uploadAttachmentchange() {
            $("#<%=uploadedFile.ClientID%>").click();
        }

        function setDeleteImageId(_this) {

            var emailAttachmentId = $(_this).siblings(".emailAttachmentId").text();
            var isSaved = $(_this).siblings(".isSaved").text();
            $("#<%=imageDeleteId.ClientID%>").val(emailAttachmentId);
            $("#<%=isSaved.ClientID%>").val(isSaved);
        }



        function insertEmoticonAtTextareaCursor(ID, text) {
            var sel = tinyMCE.activeEditor.selection;
            sel.setContent('<span id="_math_marker">' + text + '</span>');
        }
    </script>

    <script type="text/javascript">
        tinyMCE.init({
            mode: "textareas",
            plugins: "media",
            plugins: "code media",
            //toolbar: "code",
            //menubar: 'file edit insert view format table tools'
            //theme_advanced_buttons1: "code",
            //media_strict: false,
            //menubar: 'file edit insert view format table tools'
            menu: {
                file: { title: 'File', items: 'newdocument' },
                edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
                insert: { title: 'Insert', items: 'link media | template hr' },
                view: { title: 'View', items: 'visualaid' },
                format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
                table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
                tools: { title: 'Tools', items: 'spellchecker code' }
            }
        });
    </script>

</asp:Content>
