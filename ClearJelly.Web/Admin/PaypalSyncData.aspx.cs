﻿

using ClearJelly.Entities;
using ClearJelly.ViewModels.Admin;
using ClearJelly.XeroApp;
using PaypalIntegration.PayPallHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace ClearJelly.Web.Admin
{
    public partial class PaypalSyncData : System.Web.UI.Page
    {
        private static NLog.Logger _logger;

        public PaypalSyncData()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ActiveFilter.DataSource = GetFilters();
                ActiveFilter.DataBind();

                PaidTrialFilter.DataSource = GetPaidTrialFilter();
                PaidTrialFilter.DataBind();

            }
        }

        private List<DropDownViewModel> GetFilters()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of AdminPaidTrialUser GetFilters  event started.");

                var list = Enum.GetValues(typeof(UserStatus)).Cast<UserStatus>();
                var Filters = (from p in list
                               select new DropDownViewModel
                               {
                                   Name = GetEnumDescription(p),
                                   Id = ((int)p)
                               }).ToList();
                var blank = new DropDownViewModel { Name = " Select All ", Id = 0 };
                Filters.Insert(0, blank);
                return Filters.ToList();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of AdminPaidTrialUser GetFilters event completed.", ex);
                throw;
            }
        }

        private List<DropDownViewModel> GetPaidTrialFilter()
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of AdminPaidTrialUser GetFilters  event started.");

                var list = Enum.GetValues(typeof(PaidOrTial)).Cast<PaidOrTial>();
                var Filters = (from p in list
                               select new DropDownViewModel
                               {
                                   Name = GetEnumDescription(p),
                                   Id = ((int)p)
                               }).ToList();
                var blank = new DropDownViewModel { Name = " Select All ", Id = 0 };
                Filters.Insert(0, blank);
                return Filters.ToList();
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of AdminPaidTrialUser GetFilters event completed.", ex);
                throw;
            }
        }

        public static string GetEnumDescription(Enum value)
        {
            try
            {
                var field = value.GetType().GetField(value.ToString());
                var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : value.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static void btnSyncPaypal_Click()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUsers  event started.");
                var currentpage = new PaypalSyncData();
                currentpage.PaypalSyncUser();
            }
            catch (Exception)
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUsers  event started.");
                throw;
            }
        }

        #region Get UserList
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<PaidOrTrialViewModel> GetUserList(int filterId, int? paidTrial = null)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserList event started.");
                var currentPage = new PaypalSyncData();
                var userList = currentPage.GetUsers(filterId, paidTrial);
                return userList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUserList event completed.", ex);
                throw ex;
            }
        }

        private List<PaidOrTrialViewModel> GetUsers(int filterId, int? paidTrial)
        {
            try
            {
                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities GetUsers  event started.");
                if (Session["Jelly_Admin_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_Admin_user"].ToString();
                var currentDate = DateTime.Now;
                var paidOrTrial = new List<Entities.PaypalSyncData>();
                using (var dbContext = new ClearJellyEntities())
                {
                    bool isTrialOrPaid = false;
                    if (paidTrial != null && paidTrial != 0)
                    {
                        isTrialOrPaid = (int)PaidOrTial.Paid == paidTrial ? true : false;
                    }
                    switch (filterId)
                    {
                        //active users.
                        case 1:
                            if (paidTrial == null || paidTrial == 0)
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Active).ToList();
                            }
                            else
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Active && s.IsPaid == isTrialOrPaid).ToList();
                            }
                            break;
                        //Suspended Subscription
                        case 2:
                            if (paidTrial == null || paidTrial == 0)
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Suspended).ToList();
                            }
                            else
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Suspended && s.IsPaid == isTrialOrPaid).ToList();
                            }
                            break;
                        //Cancelled Subscription
                        case 3:
                            if (paidTrial == null || paidTrial == 0)
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Cancelled).ToList();
                            }
                            else
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Cancelled && s.IsPaid == isTrialOrPaid).ToList();
                            }
                            break;
                        //expired subscription
                        case 4:
                            if (paidTrial == null || paidTrial == 0)
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Expired).ToList();
                            }
                            else
                            {
                                paidOrTrial = dbContext.PaypalSyncDatas.Where(s => s.Status == (int)UserStatus.Expired && s.IsPaid == isTrialOrPaid).ToList();
                            }
                            break;

                        default:
                            paidOrTrial = dbContext.PaypalSyncDatas.ToList();
                            break;
                    }

                    var companyUserList = paidOrTrial.AsEnumerable().Select(x => new PaidOrTrialViewModel
                    {
                        IsPaid = x.IsPaid,
                        CompanySubscriptionId = x.CompanySubscriptionId,
                        Email = x.Email,
                        LastPaymentDate = x.LastPaymentDate.HasValue ? x.LastPaymentDate.Value.ToShortDateString() : string.Empty,
                        Status = Enum.GetName(typeof(UserStatus), x.Status),
                        NoOfCycles = x.NoOfCycles,

                        SubscriptionName = x.IsPaid == true ? x.CompanySubscription.SubscriptionType.TypeName : x.CompanySubscription.SubscriptionType.TypeName + "(Trial)",
                    }).ToList();

                    return companyUserList;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UserActivities ActivateUser event completed.", ex);
                throw ex;
            }
        }
        #endregion

        public void PaypalSyncUser()
        {
            try
            {
                //DeleteAllRecordsFromPaidOrTrial();
                bool PaypalSyncUpdate = bool.Parse(Common.Common.GetPaypalSyncUpdate());
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser  PaypalSyncUser event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var lstActiveUser = dbContext.Users.Where(x => x.IsAdmin == true && x.Email != "hiren@zealousys.com").ToList();

                    foreach (var item in lstActiveUser)
                    {
                        var activeCompanySubscription = dbContext.CompanySubscriptions.Where(x => x.CompanyId == item.CompanyId && x.IsActive == true).ToList();
                        foreach (var subscription in activeCompanySubscription)
                        {
                            var paidOrTrial = new ClearJelly.Entities.PaypalSyncData();
                            var expChkt = new PayPalExpressCheckout();
                            if (subscription.EndDate != null)
                            {
                                paidOrTrial.Email = item.Email;
                                paidOrTrial.IsPaid = false;
                                paidOrTrial.NoOfCycles = 0;
                                paidOrTrial.LastPaymentDate = null;
                                paidOrTrial.CompanySubscriptionId = subscription.CompanySubscriptionId;
                                paidOrTrial.Status = subscription.EndDate > DateTime.Now ? (int)UserStatus.Active : (int)UserStatus.Expired;
                            }
                            else
                            {
                                var resRecurring = expChkt.GetRecurringPaymentsProfileDetails(subscription.RecurringProfileId);
                                var status = ((PayPalPaymentResponse)resRecurring).PayPalResponse["STATUS"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["STATUS"].ToLower() : null;
                                var lastPaymentAmount = ((PayPalPaymentResponse)resRecurring).PayPalResponse["REGULARAMT"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["REGULARAMT"].ToLower() : null;
                                var NoofCycles = ((PayPalPaymentResponse)resRecurring).PayPalResponse["NUMCYCLESCOMPLETED"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["NUMCYCLESCOMPLETED"].ToLower() : null;
                                var recurringStartDate = ((PayPalPaymentResponse)resRecurring).PayPalResponse["PROFILESTARTDATE"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["PROFILESTARTDATE"].ToLower() : null;
                                var lastPaymentDate = ((PayPalPaymentResponse)resRecurring).PayPalResponse["LASTPAYMENTDATE"] != null ? ((PayPalPaymentResponse)resRecurring).PayPalResponse["LASTPAYMENTDATE"].ToLower() : null;
                                int statusofSubscrip = (int)UserStatus.Active;

                                if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Expired).ToLower())
                                {
                                    if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower())
                                    {
                                        statusofSubscrip = (int)UserStatus.Cancelled;
                                    }
                                    else if (status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower())
                                    {
                                        statusofSubscrip = (int)UserStatus.Suspended;
                                    }
                                    else
                                    {
                                        statusofSubscrip = (int)UserStatus.Expired;
                                    }
                                }

                                paidOrTrial.Email = item.Email;
                                paidOrTrial.IsPaid = true;
                                paidOrTrial.NoOfCycles = Int16.Parse(NoofCycles);
                                paidOrTrial.LastPaymentDate = lastPaymentDate == null ? (DateTime?)null : convertIsoToDateTime(lastPaymentDate);
                                paidOrTrial.CompanySubscriptionId = subscription.CompanySubscriptionId;
                                paidOrTrial.Status = statusofSubscrip;
                                if (PaypalSyncUpdate)
                                {
                                    if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Expired).ToLower())

                                        if (status == Enum.GetName(typeof(UserStatus), UserStatus.Suspended).ToLower())
                                        {
                                            DeactivateSubscription(subscription.RecurringProfileId);
                                            SuspendUser(subscription.RecurringProfileId, "User is suspended due to maximum failed payments attempt by paypal");
                                        }
                                        else if (status == Enum.GetName(typeof(UserStatus), UserStatus.Cancelled).ToLower() || status == Enum.GetName(typeof(UserStatus), UserStatus.Expired).ToLower())
                                        {
                                            DeactivateSubscription(subscription.RecurringProfileId);
                                        }

                                    InsertintoSubscriptionHistory(paidOrTrial, lastPaymentAmount, subscription.RecurringProfileId);
                                }
                            }

                            InsertintoPaidOrTrail(paidOrTrial, subscription.CompanySubscriptionId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser  PaypalSyncUser event completed.", ex);
                throw;
            }
        }

        private void SuspendUser(string recurringProfileId, string message)
        {
            try
            {
                _logger.Info(" ~Execution of SyncWithPaypal SuspendUser event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var currentDate = DateTime.Now.Date;
                    var companySubscription = dbContext.CompanySubscriptions.First(x => x.RecurringProfileId == recurringProfileId);
                    var getUserCompany = dbContext.CompanySubscriptions.Any(x => x.CompanyId == companySubscription.CompanyId && x.IsActive == true && x.RecurringProfileId != recurringProfileId);
                    if (!getUserCompany)
                    {
                        var getUser = dbContext.Users.FirstOrDefault(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                        Common.Common.SuspendUser(getUser.Email, getUser.UserId, message);
                    }
                    var recurringPayment = dbContext.RecurringPayments.FirstOrDefault(x => x.RecurringProfileId == recurringProfileId);

                }
            }
            catch (Exception ex)
            {
                _logger.Error("~Execution of SyncWithPaypal SuspendUser event completed.", ex);
                throw;
            }
        }

        public static void DeactivateSubscription(string recurringProfileId)
        {
            try
            {
                _logger.Info(" ~Execution of SyncWithPaypal DeactivateSubscription event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var companySubscription = dbContext.CompanySubscriptions.First(x => x.RecurringProfileId == recurringProfileId);
                    companySubscription.IsActive = false;
                    dbContext.SaveChanges();
                    var getUserCompnay = dbContext.CompanySubscriptions.Any(x => x.CompanyId == companySubscription.CompanyId && x.IsActive == true && x.RecurringProfileId != recurringProfileId);
                    if (!getUserCompnay)
                    {
                        var getUserDetails = dbContext.Users.FirstOrDefault(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                        Common.Common.DeActivateSQLUser(getUserDetails.Email, getUserDetails.Email);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of SyncWithPaypal DeactivateSubscription event completed.", ex);
                throw;
            }
        }

        private void InsertintoSubscriptionHistory(Entities.PaypalSyncData paidOrTrial, string lastPaymentAmount, string recurringProfileId)
        {
            try
            {
                _logger.Info(" ~Execution of SyncWithPaypal InsertintoSubscriptionHistory event started.");
                using (var dbContext = new ClearJellyEntities())
                {

                    int statusPaypal = 0;
                    if (paidOrTrial.Status == (int)UserStatus.Cancelled)
                    {
                        statusPaypal = (int)UserStatus.Cancelled;
                    }
                    else if (paidOrTrial.Status == (int)UserStatus.Suspended)
                    {
                        statusPaypal = (int)UserStatus.Suspended;

                    }
                    else if (paidOrTrial.Status == (int)UserStatus.Expired)
                    {
                        statusPaypal = (int)UserStatus.Expired;
                    }
                    var recordExists = dbContext.PaypalSyncDataHistories.Any(x => x.RecurringProfileId == recurringProfileId);
                    if (!recordExists)
                    {
                        PaypalSyncDataHistory companySubscriptionHistory = new PaypalSyncDataHistory();
                        companySubscriptionHistory.CompanySubscriptionId = paidOrTrial.CompanySubscriptionId;
                        companySubscriptionHistory.RecurringProfileId = recurringProfileId;
                        companySubscriptionHistory.CreatedDate = DateTime.Now;
                        companySubscriptionHistory.HistoryId = Guid.NewGuid();
                        companySubscriptionHistory.Status = statusPaypal;
                        companySubscriptionHistory.LastPaymentAmount = decimal.Parse(lastPaymentAmount);
                        companySubscriptionHistory.NoOfCycles = paidOrTrial.NoOfCycles;
                        dbContext.PaypalSyncDataHistories.Add(companySubscriptionHistory);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of SyncWithPaypal InsertintoSubscriptionHistory event completed.", ex);
                throw;
            }
        }

        public DateTime convertIsoToDateTime(string iso)
        {
            try
            {
                return DateTime.ParseExact(iso, "yyyy-MM-dd't'HH:mm:ss'z'", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<PaidOrTrialViewModel> GetPaidTrialList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser  GetPaypalSubscriptionList event started.");
                var currentPage = new PaypalSyncData();
                var companySubscription = currentPage.GetCompanySubscriptionHistory();
                return companySubscription;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser  GetPaypalSubscriptionList event completed.", ex);
                throw;
            }

        }

        private List<PaidOrTrialViewModel> GetCompanySubscriptionHistory()
        {
            try
            {

                _logger.Info((Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser GetCompanySubscriptionHistory event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var paidTrialData = dbContext.PaypalSyncDatas.ToList();
                    List<PaidOrTrialViewModel> lstPaidOrtrial = new List<PaidOrTrialViewModel>();
                    foreach (var item in paidTrialData)
                    {
                        PaidOrTrialViewModel paidOrTrial = new PaidOrTrialViewModel();
                        paidOrTrial.IsPaid = item.IsPaid;
                        paidOrTrial.CompanySubscriptionId = item.CompanySubscriptionId;
                        paidOrTrial.Email = item.Email;
                        paidOrTrial.LastPaymentDate = item.LastPaymentDate.HasValue ? item.LastPaymentDate.Value.ToShortDateString() : string.Empty;
                        paidOrTrial.Status = Enum.GetName(typeof(UserStatus), item.Status);
                        paidOrTrial.NoOfCycles = item.NoOfCycles;
                        paidOrTrial.SubscriptionName = item.IsPaid == true ? item.CompanySubscription.SubscriptionType.TypeName : item.CompanySubscription.SubscriptionType.TypeName + " (Trial)";
                        lstPaidOrtrial.Add(paidOrTrial);
                    }
                    return lstPaidOrtrial;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser GetCompanySubscriptionHistory event completed.", ex);
                throw ex;
            }
        }

        public void DeleteAllRecordsFromPaidOrTrial()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser DeleteAllRecordsFromPaidOrTrial event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    dbContext.Database.ExecuteSqlCommand("Delete from [PaypalSyncData]");
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser DeleteAllRecordsFromPaidOrTrial event completed.", ex);
                throw ex;
            }
        }

        private void InsertintoPaidOrTrail(ClearJelly.Entities.PaypalSyncData paidOrTrial, int companySubscriptionId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser InsertintoPaidOrTrail event started.");
                using (var dbCotext = new ClearJellyEntities())
                {
                    var recordstoCheck = dbCotext.PaypalSyncDatas.FirstOrDefault(x => x.CompanySubscriptionId == companySubscriptionId);
                    if (recordstoCheck != null)
                    {
                        if (recordstoCheck.Status != paidOrTrial.Status)
                        {
                            recordstoCheck.Status = paidOrTrial.Status;
                        }
                    }
                    else
                    {
                        dbCotext.PaypalSyncDatas.Add(paidOrTrial);
                    }
                    dbCotext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of PaypalSyncUser InsertintoPaidOrTrail event completed.", ex);
                throw ex;
            }
        }
    }
}