﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="UserSubscriptions.aspx.cs" CodeFile="UserSubscriptions.aspx.cs" Inherits="ClearJelly.Web.Admin.UserSubscriptions" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>User Subscriptions</h1>
            </div>
            <input type="hidden" id="IsOnlyActiveSubscription" />
           
            <div class="form-group">
            <label class="control-label col-xs-2" style="color: black;font-weight: bold;margin-left: -10px;" for="Active">
                Only Active:
                    <input type="checkbox" id="chkOnlyActive" />
                </label>
            </div>
            
            <br />
            <br />
            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!</strong> successfully.
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div id="userdv">
            </div>

        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="subscriptionPopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">User Subscriptions </h4>
                </div>
                <div class="modal-body">
                    <div id="userSubscriptiondv">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/UserSubscriptions.js">
    </script>
</asp:Content>
