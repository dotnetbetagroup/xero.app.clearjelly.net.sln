﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="PaypalSyncData.aspx.cs" Inherits="ClearJelly.Web.Admin.PaypalSyncData" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>

    <%--<asp:LinkButton ID="btnUpdate" runat="server" OnClick="btnSyncPaypal_Click"></asp:LinkButton>--%>

    <div class="section">
        <div class="row">
            <div class="col-md-12 gray-bg">
                <div class="login-title pb10">
                    <h1>Paypal Subscription</h1>
                </div>

                <div class="alert alert-success display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Success!&nbsp; </strong><span id="successMsg"></span>
                </div>
                <div class="alert alert-danger display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Error!&nbsp; </strong>Something went wrong, please try again.
                </div>

                <div class="col-md-10 gray-bg padding-top-10">
                    <div class="centerLabel">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group ">
                                <asp:Label runat="server" AssociatedControlID="ActiveFilter" CssClass="control-label col-xs-3">Filter </asp:Label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="ActiveFilter" CssClass="form-control pointerCursor width200" runat="server" DataTextField="Name" DataValueField="Id">
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                    </div>
                      <div class="centerLabel">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group ">
                                <asp:Label runat="server" AssociatedControlID="PaidTrialFilter" CssClass="control-label col-xs-4">Paid Or Trial </asp:Label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="PaidTrialFilter" CssClass="form-control pointerCursor width200" runat="server" DataTextField="Name" DataValueField="Id">
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-2 pull-right margin-top-10px mb15">
                    <button type="button" class="btn btn-success btn-block syncDataButton">Sync With Paypal</button>
                </div>

                <div id="PaidOrTrialDiv"></div>
            </div>
        </div>
    </div>
    <script src="../Scripts/XeroWebApp/PaypalSyncUser.js"></script>
</asp:Content>
