﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeBehind="EmailTemplate.aspx.cs" Inherits="ClearJelly.Web.Admin.EmailTemplate" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Email Templates </h1>
            </div>

            <div id="successDiv" runat="server" class="alert alert-success" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!</strong> Template updated successfully.
            </div>
            <div id="errorDiv" runat="server" class="alert alert-danger" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />

            <div class="col-md-12 gray-bg padding-top-10">
                <input type="hidden" id="hdnEmailTemplateId" runat="server" />
                <div class="row centerLabel">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group ">
                            <asp:Label runat="server" AssociatedControlID="CategoryDropDown" CssClass="control-label col-xs-3">Email Category</asp:Label>
                            <div class="col-xs-8">
                                <asp:DropDownList ID="CategoryDropDown" CssClass="form-control pointerCursor width200" runat="server" DataTextField="Name" DataValueField="Id">
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="EditLinkPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Sample Link</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" id="sampleLinkId" />
                                <asp:Label runat="server" AssociatedControlID="customTitle" CssClass="control-label textalignright col-xs-3">Title<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ReadOnly="true" ID="customTitle" CssClass="form-control" />
                                    <%--<span class="mandatory display-none validationMsgs" id="titleMsg">Please enter title.</span>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DownloadLink" CssClass="control-label textalignright col-xs-3">Download Link<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Downloadlink" CssClass="form-control" />
                                    <span class="mandatory display-none validationMsgs" id="downloadMsg">Please enter download link.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

            <br />
            <div id="dv">
            </div>
        </div>
    </div>
    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/EmailTemplate.js">
    </script>
</asp:Content>
