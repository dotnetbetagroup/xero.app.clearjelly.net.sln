﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeFile="ManageSubscriptions.aspx.cs" Inherits="ClearJelly.Web.Admin.Admin_ManageSubscriptions" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>
    <div class="section">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Manage Subscriptions</h1>
            </div>
            <div class="alert alert-success display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Success!</strong> <span id="successMsg">Sample link updated successfully.</span>
            </div>
            <div class="alert alert-danger display-none" role="alert">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <strong>Error!</strong> Something went wrong, please try again.
            </div>
            <asp:Label runat="server" ID="LblSuccess" CssClass="successMessage" />
            <div class="addbuttonDiv">
                <button type="button" id="btnAddNew" class="btn btn-success">
                    <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add New</button>
            </div>
            <div id="manageSubscriptiondv">
            </div>

        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="Popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Subscription Types</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" id="subscriptionTypeId" />
                                <asp:Label runat="server" AssociatedControlID="Name" CssClass="control-label col-xs-4">Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                                    <span class="mandatory display-none validationMsgs" id="nameMsg">Please enter Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">

                                <asp:Label runat="server" AssociatedControlID="Description" CssClass="control-label col-xs-4">Description</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Description" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MonthlyFee" CssClass="control-label col-xs-4">Monthly Fee<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="MonthlyFee" CssClass="form-control numeric twoDecimal" />
                                    <span class="mandatory display-none validationMsgs" id="monthlyMsg">Please enter Monthly Fee.</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="YearlyFee" CssClass="control-label col-xs-4">Yearly Fee<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="YearlyFee" CssClass="form-control numeric twoDecimal" />
                                    <span class="mandatory display-none validationMsgs" id="yearlyMsg">Please enter Yearly Fee.</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="AdditionalUserFee" CssClass="control-label col-xs-4">Additional User Fee<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="AdditionalUserFee" CssClass="form-control numeric twoDecimal" />
                                    <span class="mandatory display-none validationMsgs" id="additionalMsg">Please enter Additional User Fee.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Entities" CssClass="numeric control-label col-xs-4">Entities<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Entities" CssClass="numeric form-control" />
                                    <span class="mandatory display-none validationMsgs" id="entitiesMsg">Please enter Entities.</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label col-xs-4">Active</asp:Label>
                                <div class="col-xs-8">
                                    <input type="checkbox" value="true" name="IsActive" id="IsActive" checked="checked">
                                    <input type="hidden" value="false" name="IsActive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script type="text/javascript" src="../Scripts/XeroWebApp/ManageSubscriptions.js">
    </script>
</asp:Content>
