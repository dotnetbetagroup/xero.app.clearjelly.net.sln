﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeFile="ManageSetting.aspx.cs" Inherits="ClearJelly.Web.Admin.Admin_ManageSetting" %>


<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>

    <div class="section">
        <div class="row">
            <div class="col-md-12 gray-bg">
                <div class="login-title pb10">
                    <h1>Manage Settings</h1>
                </div>

                <div class="alert alert-success display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Success!&nbsp; </strong><span id="successMsg"></span>
                </div>
                <div class="alert alert-danger display-none" role="alert">
                    <a class="close" onclick="$('.alert').hide()">×</a>
                    <strong>Error!&nbsp; </strong>Something went wrong, please try again.
                </div>
                <div id="manageAppsettingdv">
                </div>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="AppSettingPopup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="poupheading">Update Setting</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <input type="hidden" id="AppKeyId" />
                                    <asp:Label runat="server" CssClass="control-label col-xs-4">Value<span class="mandatory">&nbsp;*</span></asp:Label>
                                    <div class="col-xs-4">
                                        <asp:TextBox runat="server" ID="Value" CssClass="form-control" />
                                        <span class="mandatory display-none validationMsgs" id="titleMsg">Please enter value.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/ManageSettings.js">
    </script>
</asp:Content>


