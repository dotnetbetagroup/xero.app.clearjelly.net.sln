﻿using System.Web;
using System;
using System.Linq;
using System.Data.SqlClient;
using System.Threading;
using ClearJelly.Entities;
using ClearJelly.XeroApp;

namespace ClearJelly.Web.Admin
{
    public partial class Index : System.Web.UI.Page
    {
        private static NLog.Logger _logger;
        private readonly ClearJellyEntities _clearJellyContext;

        public Index()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //if user try to open login page than redirect to admin home page.
                //currently admin home apge is activateUser.
                if (Session["Jelly_Admin_user"] != null)
                {
                    Response.Redirect("~/Admin/UserActivities", false);
                }

                if (Session["isUserExpired"] != null && Session["isUserExpired"].ToString() == "true")
                {
                    Session.Remove("isUserExpired");
                    errorText.Visible = true;
                }
                else { errorText.Visible = false; }

            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin  Page_Load event completed.", ex);
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {


            if (IsValid)
            {
                var email = EmailAddress.Text;
                var encryptedPassword = EncryptDecrypt.EncryptString(Password.Text);
                try
                {
                    var currentUser = _clearJellyContext.AdminUsers.SingleOrDefault(x => x.Email == email && x.Password == encryptedPassword);
                    if (currentUser == null)
                    {
                        FailureText.Text = "Invalid username or password.";
                        ErrorMessage.Visible = true;
                    }
                    else
                    {
                        Session["login"] = "valid";
                        Session["Jelly_Admin_user"] = email;
                        Session["User_fullname"] = currentUser.FirstName + " " + currentUser.LastName;
                        var redirectUrl = HttpContext.Current.Request.QueryString["RedirectUrl"];
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Admin/UserActivities.aspx", false);
                    }
                }
                catch (SqlException ex)
                {
                    _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin Login event sql exception completed.", ex);
                }
                catch (ThreadAbortException ex)
                {
                    _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin Login event thread abort exception completed.", ex);
                }
                catch (Exception ex)
                {
                    _logger.Error((Session["Jelly_Admin_user"] ?? "") + " ~Execution of Admin Login event completed.", ex);
                }
            }
        }
    }
}