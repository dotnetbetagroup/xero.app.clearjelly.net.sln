﻿using System;
using System.Linq;
using ClearJelly.Entities;

namespace ClearJelly.Web
{


    public partial class EditProfile : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

        public EditProfile()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SuccessMessage.Visible = false;
                FailureMessage.Visible = false;
                Passwordfailure.Visible = false;

                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx", false);
                }
                if (Session["IsSubscriptionExpired"] == null)
                {
                    //throw new Exception("Invalid session IsSubscriptionExpired");
                }

                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }

                if (Convert.ToBoolean(Session["IsSubscriptionExpired"]))
                {
                    if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                    {
                        Response.Redirect("~/ManageSubscription.aspx", false);
                    }
                }
                if (!IsPostBack)
                {
                    if (Session["Jelly_user"] == null)
                    {
                        throw new Exception("Invalid session Jelly_user");
                    }
                    var email = Session["Jelly_user"].ToString();
                    var user = GetCurrentUser(email);
                    var userToUpdate = new User();
                    if (Request.QueryString["userId"] != null)
                    {
                        var userId = int.Parse(Request.QueryString["userId"]);
                        userToUpdate = _clearJellyContext.Users
                            .FirstOrDefault(x => x.UserId == userId && !x.IsDeleted);
                        if (userToUpdate == null || (user.UserId != userToUpdate.UserId && (user.CompanyId != userToUpdate.CompanyId
                           || !user.IsAdmin)))
                        {
                            Response.Redirect("~/Account/Login.aspx", false);
                        }
                    }
                    else
                    {
                        userToUpdate = user;
                    }
                    UpdateUserDetails(userToUpdate);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of EditProfile Page_Load event completed.", ex);
            }
        }


        protected void SaveProfile_Click(object sender, EventArgs e)
        {

            var oldDecryptedPassword = string.Empty;
            var user = new User();
            try
            {
                if (Request.QueryString["userId"] != null)
                {
                    var userId = int.Parse(Request.QueryString["userId"]);
                    user = _clearJellyContext.Users
                        .FirstOrDefault(x => x.IsActive && x.UserId == userId && !x.IsDeleted);
                }
                else
                {
                    if (Session["Jelly_user"] == null)
                    {
                        throw new Exception("Invalid session Jelly_user");
                    }
                    var email = Session["Jelly_user"].ToString();
                    user = GetCurrentUser(email);
                }
                user.FirstName = FirstName.Text;
                user.LastName = LastName.Text;
                user.Mobile = Mobile.Text;
                user.Address = Address.Text;
                if (user.IsAdmin)
                {
                    user.Company.Name = CompanyName.Text;
                    user.Company.Address = CompanyAddress.Text;
                    //user.Company.PhoneNumber = PhoneNumber.Text;
                    user.Company.Website = Website.Text;
                }
                _clearJellyContext.SaveChanges();
                var profilePicture = ProfilePicture.PostedFile != null ? user.Email + System.IO.Path.GetExtension(ProfilePicture.FileName) : null;
                if (ProfilePicture.PostedFile != null)
                {
                    var imagePath = System.Web.Configuration.WebConfigurationManager.AppSettings["ProfilePicturePath"];
                    System.IO.FileInfo file = new System.IO.FileInfo(imagePath);
                    file.Directory.Create();
                    ProfilePicture.SaveAs(imagePath + profilePicture);
                }
                var IsPasswordResetSuccess = false;
                if (!string.IsNullOrEmpty(Password.Text) && !string.IsNullOrEmpty(Password.Text.Trim()))
                {
                    IsPasswordResetSuccess = Common.Common.ResetUserPassword(user, Password.Text, false);
                    if (IsPasswordResetSuccess)
                    {
                        SuccessMessage.Visible = true;
                    }
                    else
                    {
                        Passwordfailure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FailureMessage.Visible = true;
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Edit Profile SaveProfile_Click event completed.", ex);
            }
        }

        private void UpdateUserDetails(User user)
        {
            try
            {
                FirstName.Text = user.FirstName;
                LastName.Text = user.LastName;
                Email.Text = user.Email;
                Password.Text = user.Password;
                ConfirmPassword.Text = user.Password;
                Mobile.Text = user.Mobile;
                Address.Text = user.Address;
                if (user.IsAdmin)
                {
                    CompanyDetails.Visible = true;
                    CompanyName.Text = user.Company.Name;
                    CompanyAddress.Text = user.Company.Address;
                    // PhoneNumber.Text = user.Company.PhoneNumber;
                    Website.Text = user.Company.Website;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of EditProfile UpdateUserDetails event completed.", ex);
                throw;
            }
        }

        private User GetCurrentUser(string email)
        {
            try
            {
                return _clearJellyContext.Users.Single(x => x.Email == email && x.IsActive && !x.IsDeleted);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of EditProfile GetCurrentUser event completed.", ex);
                throw;
            }
        }
    }
}