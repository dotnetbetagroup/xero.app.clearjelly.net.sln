﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using XeroApi;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using ClearJelly.Entities;
using ClearJelly.Configuration;

namespace ClearJelly.Web
{
    public partial class _Default : Page
    {
        private const String UserAgent = "ClearJelly Xero Addon";
        // Demo App - use for development only
        // the callback url is localhost
        private readonly string _consumerKey;
        private readonly string _consumerSecret;
        private static string _strConn2;
        private static string _metaDBConnection;
        private static NLog.Logger _logger;
        public delegate void Worker();
        private static Thread worker;
        // ClearJelly Pubic Test App
        // the callback url is:manadev.cloudapp.net:
        //private const String ConsumerKey = "8WBSIQ2YN5KKIEJN2HMZTLJAP3C4M9";
        //private const String ConsumerSecret = "WBUGSTFWSZVE127454OHOXWXKPNA8X";
        //test
        //private static readonly X509Certificate2 OAuthCertificate = new X509Certificate2(System.Web.HttpContext.Current.Server.MapPath("/Certs/public_privatekey.pfx"), "mana@2010");
        //private static readonly X509Certificate2 ClientSslCertificate = new X509Certificate2(System.Web.HttpContext.Current.Server.MapPath("/Certs/entrust-client.p12"), "mana@2010");
        private int xeroApiCallsCount;
        private readonly ClearJellyEntities _clearJellyContext;
        private static string databaseName;

        public _Default()
        {

            if (System.Configuration.ConfigurationManager.AppSettings["ApplicationEnvironment"] == "Debug")
            {
                _consumerKey = "S8PNXVUYUUAQJWO1BVXD9IQ51RNE8H";
                _consumerSecret = "KL9OZP2VD4TFFFM9WF78GK539HY6CQ";
            }
            else
            {
                _consumerKey = "S8PNXVUYUUAQJWO1BVXD9IQ51RNE8H";
                _consumerSecret = "KL9OZP2VD4TFFFM9WF78GK539HY6CQ";
            }
        }

        static _Default()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _strConn2 = ConfigSection.CompanyDBConnection;
            _metaDBConnection = ConfigSection.DefaultConnection;

            databaseName = Common.Common.GetDataBaseName();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var accessToken = new DevDefined.OAuth.Storage.Basic.AccessToken();
            var requestToken = new DevDefined.OAuth.Storage.Basic.RequestToken();
            //try
            //{
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of Page_Load event started.");
            //Step3. When loading the page, if the page was loaded from callback, the Request contains an oauth_verifier param. 
            if (Session["login"] != "valid" && Session["Jelly_user"] == null)
            {
                Response.Redirect("~/Account/Login.aspx");
            }
            if (Request.Params["oauth_verifier"] != null)
            {
                FinishAuthorisation(Request.Params["oauth_verifier"], Request.Params["org"]);
            }
            else
            {
                cmdConnect_Click();
            }
            //String OrgShortCode = Session["OrgShortCode"].ToString();

            //}
            //catch(Exception ex)
            // {
            //    _logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error during Page_Load of Default.ASPX. Error details:" + ex.Message);
            // }
        }

        protected void cmdConnect_Click()
        {
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of cmdConnect_Click event started.");
            //clear_table("PnL");
            /* IOAuthSession consumerSession = new XeroApiPublicSession(UserAgent, _consumerKey, _consumerSecret, new InMemoryTokenRepository());

             consumerSession.MessageLogger = new DebugMessageLogger();

             //Step 1. Get a Request Token

             var callbackUri = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port, "Default.aspx");
             RequestToken requestToken = consumerSession.GetRequestToken(callbackUri.Uri);


             //Step 2. Get the user to log into Xero using the request Token and redirect to the authorisation URL
             var authorisationUrl = consumerSession.GetUserAuthorizationUrl();
             Session["ConsumerSession"] = consumerSession;
             _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~User has been redirected to Xero website for connecting to API.");
             Response.Redirect(authorisationUrl);*/

            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~User has been redirected to Xero website for connecting to API.");
            //xeroAuthorize();


        }

        protected void FinishAuthorisation(String verifier, String OrgShortCode)
        {
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of FinishAuthorisation method started.");
            var consumerSession = (IOAuthSession)Session["ConsumerSession"];
            var accessToken = new DevDefined.OAuth.Storage.Basic.AccessToken();

            try
            {
                //Exchange the RequestToken for an AccessToken using the verifier code from the callback url
                accessToken = consumerSession.ExchangeRequestTokenForAccessToken(verifier);

            }
            catch (OAuthException)
            {
                throw;
            }

            OAuthConsumerContext e = new OAuthConsumerContext();
            //create a repository with the authenticated session
            var repository = new Repository(consumerSession);
            //consumerSession.AccessToken.
            UpdateRequestToken(consumerSession.GetRequestToken());
            Session["repository"] = repository;

            Session["OrgShortCode"] = OrgShortCode;

            String OrgName = repository.Organisation.Name;
            Session["OrgName"] = OrgName;


            addXeroOrgToDB(OrgShortCode, OrgName);
            InsertAccessToken(accessToken, OrgShortCode);
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of FinishAuthorisation method completed.");
            Response.Redirect("~/Home.aspx");
        }

        private void addXeroOrgToDB(String OrgShortCode, String OrgName)
        {
            try
            {
                _logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");

                string strCompanyId = Session["Jelly_UserCompanyId"].ToString();
                if (string.IsNullOrEmpty(strCompanyId))
                {
                    throw new Exception("strCompanyId is null");
                }

                if (!string.IsNullOrEmpty(strCompanyId))
                {
                    using (var dbContext = new ClearJellyEntities())
                    {
                        int intCompanyId = Int32.Parse(strCompanyId);
                        bool checkOrgExists = dbContext.Xero_User_Org.Any(x => x.CompanyID == intCompanyId && x.OrgShortCode == OrgShortCode);
                        if (!checkOrgExists)
                        {
                            var xeroOrg = new Xero_User_Org();
                            xeroOrg.CompanyID = intCompanyId;
                            xeroOrg.OrgShortCode = OrgShortCode;
                            xeroOrg.isActive = true;
                            xeroOrg.OrgName = OrgName;
                            dbContext.Xero_User_Org.Add(xeroOrg);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Home addXeroOrgToDB event completed.", ex);

                throw;
            }

        }

        private static void clear_table(String teble_name)
        {
            //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Truncate Table " + teble_name;
            try
            {
                MyConnection = new SqlConnection(_strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {

                }
                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table method completed.");
            }
            catch (SqlException ex)
            {
                //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table method. Error details:" + ex.Message);            
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An error occurred during execution of clear_table method. Error details:" + ex.Message);
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        private static void clear_table_section(String teble_name, DateTime lastUpdatedDate)
        {
            //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Delete FROM " + teble_name + "  WHERE UpdatedDateUTC >= convert(date,'" + lastUpdatedDate + "',103)";
            try
            {
                MyConnection = new SqlConnection(_strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {

                }
                //_logger.Info(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~Execution of clear_table_section method completed.");
            }
            catch (SqlException ex)
            {
                //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                //_logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An error occurred during execution of clear_table_section method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        protected String GetUserDBName()
        {
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetUserDBName method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String tmpConn = _strConn2 + "database= " + databaseName + ";";
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" SELECT B.Name " +
                            "	  FROM [" + databaseName + "].[dbo].[User] A " +
                            "	  Inner JOIN [" + databaseName + "].[dbo].[Company] B " +
                            "	  ON" +
                            "	  A.CompanyId=B.CompanyId " +
                            "	  where email= '" + UserName + "'";
            try
            {
                MyConnection = new SqlConnection(tmpConn);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {
                _logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An SQL error occurred during execution of GetUserDBName method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                _logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during execution of GetUserDBName method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetUserDBName method completed.");
            return (email);
        }

        protected String GetEmail()
        {
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetEmail method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" select email from  ClearJelly.dbo.[user] where email= '" + UserName + "'";
            try
            {
                MyConnection = new SqlConnection(_strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {
                _logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An SQL error occurred during execution of GetEmail method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                _logger.Fatal((Session["Jelly_user"] ?? "").ToString() + " ~An error occurred during execution of GetEmail method. Error details:" + ex.Message);
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            _logger.Info((Session["Jelly_user"] ?? "").ToString() + " ~Execution of GetEmail method completed.");
            return (email);
        }

        protected DevDefined.OAuth.Storage.Basic.AccessToken getAccessToken()
        {
            var token = new DevDefined.OAuth.Storage.Basic.AccessToken();
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Select * FROM ClearJelly.dbo.AccessTokens " +
                             " Where UserName=   '" + UserName + "'";
            ;
            try
            {
                MyConnection = new SqlConnection(_metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    token.SessionHandle = myReader["SessionHandle"].ToString();
                    token.Token = myReader["Token"].ToString();
                    token.TokenSecret = myReader["TokenSecret"].ToString();
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (token);
        }

        protected bool isTokenExpired(DevDefined.OAuth.Storage.Basic.AccessToken token)
        {
            bool result = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Select count(*) FROM ClearJelly.dbo.AccessTokens " +
                             " Where UserName=   '" + UserName + "' " +
                             " AND convert(datetime,[ExpiryDateUtc],103) >convert(datetime,'" + DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss.fff",
                                                                                CultureInfo.InvariantCulture) + "',103)";
            int count = 0;
            try
            {
                MyConnection = new SqlConnection(_metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    count = myReader.GetInt32(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }

            if (count > 0)
            {
                result = false;
            }
            return (result);
        }

        protected bool HasTokenInDB(String OrgShortCode)
        {
            bool result = false;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            //var repository = (Repository)Session["repository"];
            //String Org = repository.Organisation.Name;
            String sqlStr = @" Select count(*) FROM ClearJelly.dbo.AccessTokens " +
                             " Where UserName=   '" + UserName + "' and OrgShortCode='" + OrgShortCode + "' ";
            int count = 0;
            try
            {
                MyConnection = new SqlConnection(_metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    count = myReader.GetInt32(0);
                }
            }
            catch (SqlException ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }

            if (count > 0)
            {
                result = true;
            }
            return (result);
        }
        //protected void xeroAuthorize()
        //{
        //    // var signingCert = GetCertificateFromStore("E=marketing@clearjelly.net, CN=ClearJelly, O=Managility Pty Ltd, L=Sydney, S=NSW, C=AU");
        //    //var sslCert = GetCertificateFromStore("CN=ClearJelly, O=Xero, C=NZ");
        //    //clear_table("PnL");
        //    //IOAuthSession consumerSession = new XeroApiPublicSession(UserAgent, _consumerKey, _consumerSecret, new InMemoryTokenRepository());
        //    IOAuthSession consumerSession = new XeroApiPartnerSession(
        //                                                                UserAgent,
        //                                                                _consumerKey,
        //                                                                OAuthCertificate,
        //                                                                ClientSslCertificate,
        //                                                                new InMemoryTokenRepository());
        //    //(UserAgent, _consumerKey, _consumerSecret, new InMemoryTokenRepository());
        //    consumerSession.MessageLogger = new DebugMessageLogger();

        //    var callbackUri = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port, "AddXeroOrgs.aspx");
        //    RequestToken requestToken = consumerSession.GetRequestToken(callbackUri.Uri);


        //    System.Diagnostics.Debug.WriteLine("Request Token Key: {0}", requestToken.Token);
        //    System.Diagnostics.Debug.WriteLine("Request Token Secret: {0}", requestToken.TokenSecret);

        //    //UpdateRequestToken(requestToken);

        //    //Step 2. Get the user to log into Xero using the request Token and redirect to the authorisation URL
        //    var authorisationUrl = consumerSession.GetUserAuthorizationUrl();
        //    Session["ConsumerSession"] = consumerSession;
        //    Response.Redirect(authorisationUrl);

        //}
        protected bool UpdateAccessToken(DevDefined.OAuth.Storage.Basic.AccessToken token)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            var repository = (Repository)Session["repository"];
            String Org = repository.Organisation.Name;
            CultureInfo Au_date = new CultureInfo("en-AU");
            String sqlStr = @" UPDATE ClearJelly.dbo.AccessTokens " +
                             "      SET [ExpiryDateUtc]= '" + (token.ExpiryDateUtc).Value.ToString(Au_date) + "' ," +
                             "                  [Token]= '" + token.Token + "'  ," +
                             "                  [TokenSecret]='" + token.TokenSecret + "'," +
                             "                  [SessionHandle]='" + token.SessionHandle + "' " +
                             "      Where UserName='" + UserName + "' AND Organisation ='" + Org + "' ;";

            try
            {
                MyConnection = new SqlConnection(_metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }

        protected bool InsertAccessToken(DevDefined.OAuth.Storage.Basic.AccessToken token, String OrgShortCode)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            var repository = (Repository)Session["repository"];
            String Org = repository.Organisation.Name;
            CultureInfo Au_date = new CultureInfo("en-AU");
            String email = "";
            String sqlStr = @" Delete from ClearJelly.dbo.AccessTokens Where [organisation]='" + Org + "' AND OrgShortCode='" + OrgShortCode + "' ; " +
                             " Insert Into ClearJelly.dbo.AccessTokens (UserName,[ExpiryDateUtc],[Token],[TokenSecret],[SessionHandle],[organisation],OrgShortCode) VALUES" +
                             "      ('" + UserName + "','" + (token.ExpiryDateUtc).Value.ToString(Au_date) + "' ," +
                             "                   '" + token.Token + "'  ," +
                             "                  '" + token.TokenSecret + "'," +
                             "                  '" + token.SessionHandle + "', " +
                             "					'" + Org + "','" + OrgShortCode + "'	" +
                             "      );";

            try
            {
                MyConnection = new SqlConnection(_metaDBConnection);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }

        protected bool UpdateRequestToken(DevDefined.OAuth.Storage.Basic.RequestToken token)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Insert Into ClearJelly.dbo.RequestTokens Values" +
                             "          ('" + UserName + "'  ," +
                             "          '" + token.AccessToken + "'  ," +
                             "          '" + token.CallbackUrl + "'  ," +
                             "          '" + token.Realm + "'  ," +
                             "          '" + token.Token + "'  ," +
                             "          '" + token.TokenSecret + "'  ," +
                             "          '" + token.Verifier + "'  )"
                             ;
            try
            {
                MyConnection = new SqlConnection(_strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }

        protected bool UpdateConsumerSession(Object session)
        {
            bool status = true;
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String UserName = this.Session["Jelly_user"].ToString();
            String email = "";
            String sqlStr = @" Insert Into  ClearJelly.dbo.ConsumerSessions Values('" + UserName + "','" + session.ToString() + "' ) ";
            try
            {
                MyConnection = new SqlConnection(_strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    email = myReader.GetString(0);
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
                status = false;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return (status);
        }
    }
}