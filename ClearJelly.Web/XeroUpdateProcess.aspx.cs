﻿using ClearJelly.Entities;
using ClearJelly.XeroApp;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace ClearJelly.Web
{
    public partial class XeroUpdateProcess : System.Web.UI.Page
    {
        private static NLog.Logger _logger;
        public delegate void Worker();
        private readonly ClearJellyEntities _clearJellyContext;
        private static Thread worker;
        public XeroUpdateProcess()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _clearJellyContext = new ClearJellyEntities();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info(" ~Execution of XeroUpdateProcess UpdateModelFromTaskScheduler event started.");
                //string orgCode = Request.Form["orgCode"];
                string orgName = Request.Form["orgName"];
                string startDate = Request.Form["startDate"];
                string endDate = Request.Form["endDate"];
                string userName = Request.Form["userName"];

                var userDetails = _clearJellyContext.Users.FirstOrDefault(x => x.Email == userName);
                if (userDetails == null)
                {
                    throw new Exception("XeroUpdateProcess No user found");
                }

                var xeroDetails = _clearJellyContext.Xero_User_Org.FirstOrDefault(x => x.OrgName == orgName && x.CompanyID == userDetails.CompanyId && x.isActive == true);

                if (xeroDetails == null)
                {
                    throw new Exception("XeroUpdateProcess speicified organization not found for the user");
                }

                UpdateModelFromTaskScheduler(xeroDetails.OrgShortCode, orgName, startDate, endDate, userName);


            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of XeroUpdateProcess UpdateModelFromTaskScheduler event completed.", ex);
            }
        }


        public void UpdateModelFromTaskScheduler(string orgCode, string orgName, string startDateParam, string endDateParam, string userName)
        {
            try
            {
                _logger.Info(" ~Execution of XeroUpdateProcess UpdateModelFromTaskScheduler event started.");
                PartnerMvcAuthenticator partner_auth = null;
                partner_auth = (PartnerMvcAuthenticator)XeroApiHelper.MvcAuthenticator(userName);
                var accessToken = partner_auth.RetrieveAndStoreAccessToken(userName, null, null, orgCode);

                CultureInfo Au_date = new CultureInfo("en-AU");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-AU");
                String inOrgShortCode = orgCode;
                String inOrgName = orgName;
                DateTime startDate = DateTime.UtcNow.AddMonths(-1);
                DateTime endDate = DateTime.UtcNow;

                startDate = DateTime.ParseExact(startDateParam, "dd/MM/yyyy", Au_date);

                endDate = DateTime.ParseExact(endDateParam, "dd/MM/yyyy", Au_date);

                _logger.Info("User date selection startDate: " + startDate + " EndDate: " + endDate);

                var api = XeroApiHelper.CoreApi();
                _logger.Info("API Details:" + api);
                string decryptedPassword = null;
                var currentUser = _clearJellyContext.Users.SingleOrDefault(x => x.IsActive && x.Email == userName && !x.IsDeleted);
                if (currentUser != null)
                {
                    decryptedPassword = EncryptDecrypt.DecryptString(currentUser.Password);
                }

                XeroToCJ obj = new XeroToCJ(api, userName, decryptedPassword, inOrgName, inOrgShortCode);
                Init(() => Work_Update(obj, inOrgShortCode, userName, currentUser.UserId, startDateParam, endDateParam), currentUser.UserId, inOrgName, (int)ModelType.Update, userName);
            }
            catch (Exception ex)
            {
                _logger.Error("~Execution of XeroUpdateProcess UpdateModelFromTaskScheduler completed", ex);
                throw;

            }
        }


        public new static void Init(Worker work, int userId, string orgName, int ModelType, string userName)
        {
            try
            {
                _logger.Info(userName + " ~Execution of Init method started.");

                worker = new Thread(new ThreadStart(work));
                worker.Start();
                var homeObj = new XeroUpdateProcess();
                homeObj.InsertModelProcess(userId, orgName, ModelType, userName);
            }
            catch (Exception ex)
            {
                _logger.Error(userName + " ~Execution of Home Init event completed.", ex);

                throw;
            }
        }

        private void InsertModelProcess(int userId, string OrgName, int ModelType, string userName)
        {
            try
            {
                _logger.Info(userName + " ~Execution of InsertModelProcess method started.");
                ModelProcess model = new ModelProcess()
                {
                    UserId = userId,
                    StartDate = DateTime.Now,
                    ProcessType = ModelType,
                    Status = (int)StatusEnum.Running,
                    OrganizationName = OrgName
                };
                _clearJellyContext.ModelProcesses.Add(model);
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error(userName + " ~Execution of Home InsertModelProcess event completed.", ex);
                throw ex;
            }

        }



        public void Work_Update(XeroToCJ obj, string org, string username, int userId, string startDateParam, string endDateParam)
        {
            int errorCodes = 0;
            try
            {
                CultureInfo Au_date = new CultureInfo("en-AU");

                DateTime startDate = DateTime.UtcNow.AddMonths(-1);
                DateTime endDate = DateTime.UtcNow;
                startDate = DateTime.ParseExact(startDateParam, "dd/MM/yyyy", Au_date);
                endDate = DateTime.ParseExact(endDateParam, "dd/MM/yyyy", Au_date);
                SqlAccessTokenStore.orgShortCode = org;
                errorCodes = obj.UpdateModel(startDate, endDate);

            }
            catch (Exception ex)
            {
                _logger.Error(username + " ~Execution of Home Work_Update event completed.", ex);

                throw;
            }
            finally
            {
                var homeObj = new XeroUpdateProcess();
                homeObj.UpdateModelStatus(userId, errorCodes);
            }
        }


        private void UpdateModelStatus(int UserId, int errorCodes)
        {
            try
            {
                _logger.Info("userId=" + UserId + " ~Execution of Home UpdateModelStatus method started.");
                var runningStatusId = (int)StatusEnum.Running;
                var modelProcess = _clearJellyContext.ModelProcesses.Where(a => a.UserId == UserId && a.Status == runningStatusId).OrderByDescending(a => a.StartDate).FirstOrDefault();
                modelProcess.Status = errorCodes == (int)System.Net.HttpStatusCode.OK ? (int)StatusEnum.Success : (int)StatusEnum.Fail;
                modelProcess.XeroErrorCode = errorCodes;
                modelProcess.EndDate = DateTime.Now;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error("userId=" + UserId + " ~Execution of Home UpdateModelStatus event completed.", ex);
                throw ex;
            }
        }
    }
}