﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ClearJelly.Web.Account.Login" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="col-md-12">
            <section id="loginForm">
                <div class="login-title">
                    <h1>Login</h1>
                    <span>Please login into your account.</span>
                </div>
               

                <div class="col-sm-12 mt15 text-center">
                    <asp:Button class="btn windows-logo img-responsive cancel btn-info width-200 icon-microsoft "
                        Text="Sign Up"
                        OnClick="Register" runat="server"></asp:Button>
                </div>
                <div class="col-sm-12 mt15 text-center">                    
                    <asp:Button class="btn windows-logo img-responsive cancel btn-primary width-200 icon-microsoft"
                        AlternateText="Connect to Quickbooks"
                        Text="Sign In With Microsoft"
                        OnClick="LogInWithAd" runat="server">
                    </asp:Button>
                </div>
                 <div class=" fb-login-button col-sm-12 mt15 text-center" onlogin="checkLoginState();" scope=" public_profile,email" id="fbButton" > 
                      Login with Facebook
                  <%--  <span class="btn windows-logo img-responsive cancel btn-primary width-200" nlogin="checkLoginState();" scope=" public_profile,email" id="fbButton">  Login with Facebook </span> --%>      

                 </div>
            </section>
        </div>
    </div>

    <script type="text/javascript" src="//platform.linkedin.com/in.js">
         api_key: <%= System.Configuration.ConfigurationManager.AppSettings["LinkedInAppKey"] %>
         authorize: false
         onLoad: linkedIn.onLinkedInLoad
    </script>
    <script type="text/javascript">
        var facebookAppId = <%=System.Configuration.ConfigurationManager.AppSettings["FBLoginAppId"].ToString()%>;
    </script>
    <script type="text/javascript" src="../Scripts/XeroWebApp/Login.js"></script>
</asp:Content>


