﻿using ClearJelly.Entities;
using System;
using System.Linq;
using ClearJelly.XeroApp;

namespace ClearJelly.Web.Account
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        private static NLog.Logger _logger;
        private readonly ClearJellyEntities _clearJellyContext;

        public ResetPassword()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var _email = Request.QueryString["email"];
            try
            {
                var _code = Request.QueryString["code"];

                var requestedUser =
                    _clearJellyContext.Users.FirstOrDefault(
                        x => x.PwdResetCode == _code && x.Email == _email && !x.IsDeleted);
                if (requestedUser == null)
                {
                    lbl_expired.Visible = true;
                    Panel_reset_pwd.Visible = false;
                }
                else if (!requestedUser.IsActive)
                {
                    lbl_msg.Text = "Please activate your account to reset the password.";
                }
                else
                {
                    lbl_expired.Visible = false;
                    Panel_reset_pwd.Visible = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_email ?? "") + " ~Execution of ResetPassword Page_Load event completed.", ex);
                throw ex;

            }
        }
        protected void btn_change_pwd_Click(object sender, EventArgs e)
        {
            var _email = Request.QueryString["email"];
            bool checkUpdateFlag = false;
            var oldDecryptPassword = string.Empty;
            try
            {

                var _code = Request.QueryString["code"];
                var encryptPass = EncryptDecrypt.EncryptString(txt_pwd.Text);
                var requestedUser =
                    _clearJellyContext.Users.FirstOrDefault(
                        x => x.PwdResetCode == _code && x.Email == _email && !x.IsDeleted);
                if (requestedUser == null)
                {
                    lbl_msg.Text = "Your Password link has been expired.";
                }
                else
                {

                    var message = string.Empty;
                    var resetUserPassword = ClearJelly.Web.Common.Common.ResetUserPassword(requestedUser, txt_pwd.Text, true);

                    if (resetUserPassword)
                    {
                        lbl_msg.Text = "Your Password has been Changed successfully.<br><a href='/Account/Login.aspx'>Click Here</a> to login.";
                    }
                    else
                    {
                        lbl_msg.Text = "Some error occurred. Please try again after some time";
                    }


                    //oldDecryptPassword = EncryptDecrypt.DecryptString(requestedUser.Password);
                    //update sql login user password.

                    //requestedUser.PwdResetCode = null;
                    //requestedUser.Password = encryptPass;

                    Panel_reset_pwd.Visible = false;
                    txt_pwd.Text = "";
                    txt_retype_pwd.Text = "";
                }
                //Response.Redirect("~/Account/Login.aspx");
            }
            catch (Exception ex)
            {
                lbl_msg.Text = "An error has occured while changing the password. Please try again.";
                _logger.Error((_email ?? "") + " ~Execution of ResetPassword btn_change_pwd_Click event completed.", ex);
                if (checkUpdateFlag)
                {
                    ClearJelly.Web.Common.Common.resetSqlOldPassword(_email, txt_pwd.Text, oldDecryptPassword);
                }
                throw ex;
            }

        }


        //protected void resetJedoxPass(String userName, String pass)
        //{
        //    try
        //    {
        //        JedoxETL.ServerStatus x = new JedoxETL.ServerStatus();
        //        JedoxETL.ETLServer s = new JedoxETL.ETLServer();

        //        JedoxETL.Variable username_variable = new JedoxETL.Variable();
        //        username_variable.name = "username";
        //        username_variable.value = userName;

        //        JedoxETL.Variable password_variable = new JedoxETL.Variable();
        //        username_variable.name = "password";
        //        username_variable.value = pass;

        //        var status = s.execute("ClearJellySystemOps.jobs.ResetPassword", new JedoxETL.Variable[] { username_variable, password_variable, });

        //        while (s.getExecutionStatus(status.id.Value, true, true, true).status == "Running")
        //        {
        //            System.Threading.Thread.Sleep(1000);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((userName ?? "") + " ~Execution of ResetPassword resetJedoxPass event completed.", ex);
        //        throw ex;
        //    }
        //}
    }
}