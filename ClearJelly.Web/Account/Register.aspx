﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="~/Account/Register.aspx.designer.cs" CodeFile="Register.aspx.cs" Inherits="ClearJelly.Web.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Registration</h1>
            </div>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </p>
            <asp:HiddenField ID="HdnSubscriptionTypeId" runat="server" />
            <asp:HiddenField ID="HdnSubscriptionMonths" runat="server" />
            <asp:HiddenField ID="HdnCountryId" runat="server" />
            <asp:HiddenField ID="HdnQuantity" runat="server" />
            <asp:HiddenField ID="HdnNetAmount" runat="server" />

            <asp:HiddenField ID="HdnTotal" runat="server" />
            <asp:HiddenField ID="HdnGST" runat="server" />

            <div class="container-fluid mb30">
                <div class="form-horizontal">
                    <div class="clearfix"></div>
                    <div class="mb10">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="col-sm-6 mt15 mb15">
                                <div class="fb-login-button" onlogin="checkLoginState();" scope=" public_profile,email" id="fbButton">
                                    Signup with Facebook
                                </div>
                            </div>

                            <div class="col-sm-4 mt15 mb15 text-right">
                                <div id="divLinkedin">
                                    <script type="in/Login">
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row1">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="col-xs-offset-0" style="margin-right: 20px;">
                                    All fields with<span class="mandatory">&nbsp;*&nbsp;</span>are required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="registration-subtitle pb10">
                        <h4>Personal Details</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="control-label col-xs-4">First Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                        CssClass="text-danger" ErrorMessage="First Name is required." Display="None" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="LastName" CssClass="control-label col-xs-4">Last Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                                        CssClass="text-danger" ErrorMessage="Last Name is required." Display="None" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Email" CssClass="control-label col-xs-4">Username<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-3">
                                    <asp:TextBox runat="server" ID="Email" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                        CssClass="text-danger" ErrorMessage="Email  is required." Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                        ControlToValidate="Email" ErrorMessage="Please enter valid username"
                                        ValidationExpression="\w+([-+.']\w+)*"
                                        CssClass="text-danger" Display="None"></asp:RegularExpressionValidator>
                                </div>
                                <label class="control-label">@AgilityPlanningTST.onmicrosoft.com</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Mobile" CssClass="control-label col-xs-4">Mobile</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Mobile" CssClass="form-control numeric" />
                                    <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="Mobile"
                                        CssClass="text-danger" ErrorMessage="Mobile is required." Display="None" />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <asp:Label runat="server" AssociatedControlID="UserEmail" CssClass="control-label col-xs-4">Email Address<span class="mandatory">&nbsp;*</span></asp:Label>
                                        <div class="col-md-8">
                                            <asp:TextBox runat="server" ID="UserEmail" CssClass="form-control" />
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="UserEmail"
                                                CssClass="text-danger" ErrorMessage="Email  is required." Display="None" />
                                           <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                        ControlToValidate="UserEmail" ErrorMessage="Please enter valid email address"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        CssClass="text-danger" Display="None"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                               
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="control-label col-xs-4">Password<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                                        CssClass="text-danger" ErrorMessage="Password is required." Display="None" />
                                    <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="Password"
                                        ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
                                        ErrorMessage="Minimum 8 characters, at least 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="control-label col-xs-4">Confirm Password<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                                        CssClass="text-danger" ErrorMessage="Confirm password is required." Display="None" />
                                    <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                                        CssClass="text-danger" ErrorMessage="Password and confirm password do not match." Display="None" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="control-label col-xs-4">Address</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Address" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ProfilePicture" CssClass="control-label col-xs-4">Profile Picture</asp:Label>
                                <div class="col-xs-8 mt5">
                                    <asp:FileUpload ID="ProfilePicture" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="registration-subtitle pb10 pt5">
                        <h4>Company Details</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CompanyName" CssClass="control-label col-xs-4">Company Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="CompanyName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CompanyName"
                                        CssClass="text-danger" ErrorMessage="Company Name is required." Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="CompanyName"
                                        ValidationExpression="^[a-zA-Z0-9-_\s]+$" ErrorMessage="Special characters are not allowed in Company Name." CssClass="text-danger" Display="None" />
                                    <span id="CompanyExistValidation" class="text-danger display-none">The Company already exists.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Website" CssClass="control-label col-xs-4">Website</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Website" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CompanyAddress" CssClass="control-label col-xs-4">Company Address</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="CompanyAddress" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CountryDropDown" CssClass="control-label col-xs-4">Country</asp:Label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="CountryDropDown" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryDropDown" InitialValue="Please select" ErrorMessage="Please select country." Display="None" />
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <%--<div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <div class="col-xs-3"></div>
                                <div class="col-xs-8">
                                    <label class="control-label">Please enter the code from the below image:</label>
                                    <BotDetect:Captcha ID="SampleCaptcha" runat="server" ImageSize="200, 50" />
                                    <asp:TextBox ID="CaptchaCodeTextBox" runat="server" class="form-control margin-top-10px" />
                                </div>
                            </div>
                        </div>--%>

                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8">
                                    <div class="g-recaptcha" data-sitekey="<%=ConfigurationManager.AppSettings["Google.ReCaptcha.SiteKey"] %>"></div>
                                </div>
                            </div>
                        </div>

                        <div class="registration-subtitle pt5">
                            <h4>Subscription Options</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label col-xs-4"></label>
                                    <div class="col-xs-8">
                                        <input id="rdTrial" type="radio" name="BuyOption" checked="checked" value="FreeTrial"> Free Trial</input>
                                        <input id="rdBuyNow" class="ml40" type="radio" name="BuyOption" value="BuyNow"> Buy Now</input>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="subscriptionPlanDv">
                            <div class="registration-subtitle pb10 pt5">
                                <h4>Subscription Plans:</h4>
                            </div>
                            <div id="dvSubscriptionPlan">
                                <table id="tblSubscriptionPlan">
                                    <thead>
                                        <tr>
                                            <th>Plan</th>
                                            <th class="width8">Entity</th>
                                            <th class="width10">Monthly</th>
                                            <th class="width12">Monthly Price for 12 months contract</th>
                                            <th class="width10">Additional User Price per month</th>
                                            <th class="width10">No. Of Pack</th>
                                            <th>Subscription Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% foreach (var subscription in _subscriptionTypes)
                                            {
                                        %>
                                        <tr>
                                            <td>
                                                <%=subscription.TypeName %>
                                            </td>
                                            <td>
                                                <%=subscription.Entities %>
                                            </td>
                                            <td>
                                                <span class="float-right">$ 
                                    <label class="monthlyFeeLbl" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                        <%=subscription.MonthlyFee %>
                                    </label>
                                                </span>
                                            </td>
                                            <td>
                                                <span class="float-right">$ 
                                    <label class="yearlyContractPrice" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                        <%=subscription.YearlyContractFee %>
                                    </label>
                                                </span>
                                            </td>
                                            <td>
                                                <span class="float-right">$
                                     <label class="additionalUserPrice" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                         <%=subscription.AdditionalUserFee %>
                                     </label>
                                                </span>
                                            </td>
                                            <td>
                                                <input type="text" class="quantityTxt width100 form-control numeric" value="<%=subscription.Quantity %>" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>">
                                            </td>
                                            <td>
                                                <% if (subscription.IsCurrentSubscription)
                                                    {
                                                        if (subscription.IsYearlySubscription)
                                                        {
                                                %>
                                                <span class="col-md-offset-1">
                                                    <input class="monthlyRd ml5" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> Monthly</input>
                                                </span>
                                                <span class="col-md-offset-1">
                                                    <input class="yearlyRd" type="radio" name="subscriptionRd" checked="checked" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> 12 Month contract</input>
                                                </span>
                                                <%
                                                    }
                                                    else
                                                    {
                                                %>
                                                <span class="col-md-offset-1">
                                                    <input class="monthlyRd ml5" type="radio" name="subscriptionRd" checked="checked" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> Monthly</input>
                                                </span>
                                                <span class="col-md-offset-1">
                                                    <input class="yearlyRd" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> 12 Month contract</input>
                                                </span>
                                                <%
                                                        }
                                                    }
                                                    else
                                                    {
                                                %>
                                                <span class="col-md-offset-1">
                                                    <input class="monthlyRd ml5" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> Monthly</input>
                                                </span>
                                                <span class="col-md-offset-1">
                                                    <input class="yearlyRd" type="radio" name="subscriptionRd" data-subscriptiontypeid="<%=subscription.SubscriptionTypeId %>"> 12 Month contract</input>
                                                </span>
                                                <%
                                                    } %>
                                            </td>
                                        </tr>
                                        <%
                                            } %>
                                        <tr class="amountRow">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><span class="pull-right">Gross Amount: <span class="pl8"><strong>
                                                <label id="grossAmtLbl"></label>
                                            </strong></span></span></td>
                                        </tr>
                                        <tr class="amountRow">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <span class="pull-right">Add GST(<label id="gstRateLbl"></label>): <span class="pl8">
                                                    <label id="gstAmtLbl"></label>
                                                </span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="amountRow">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><span class="pull-right">Net Amount: <span class="pl8"><strong>
                                                <label id="netAmtLbl"></label>
                                            </strong></span></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row pt15">
                        <div id="dvTerms" class="col-md-6 col-xs-12">
                            <label class="control-label col-xs-4" data-lblblank="ForSubscription"></label>
                            <asp:CheckBox ID="chkboxTerms" runat="server" />
                            <asp:Label ID="lblTerms" runat="server" Text="I accept and agree to"></asp:Label>
                            <asp:HyperLink ID="lnkTerms" runat="server" NavigateUrl="~/terms-of-use.html" Target="_blank">
                terms of use.
                            </asp:HyperLink>
                        </div>
                    </div>

                    <div class="row">
                        <div id="dvBtnRow" class="col-md-6 col-xs-12 pt10">
                            <label class="control-label col-xs-4" data-lblblank="ForSubscription"></label>
                            <div id="dvBtns" class="col-xs-8">
                                <%-- <div class="col-xs-4" id="dvBtnRegister">
                                    <asp:Button runat="server" OnClick="Trial_Click" Text="Submit" style="display: none;" CssClass="btn btn-success btn-block" ID="btnRegister" />
                                    <asp:Button runat="server" OnClientClick="return trialConfirm()" Text="Free Trial" CssClass="btn btn-success btn-block" ID="btnRegisterClientSide" />
                                </div>--%>
                                <div class="col-xs-6" id="dvBtnRegister" style="padding-top: 5px;">
                                    <asp:Button runat="server" OnClick="Trial_Click" Text="Free Trial" CssClass="btn btn-success btn-block" ID="btnRegister" />
                                    <%--<asp:Button runat="server" OnClick="Trial_Click" Text="Submit" style="display: none;" CssClass="btn btn-success btn-block" ID="btnRegister" />--%>
                                    <%--<asp:Button runat="server" OnClientClick="return trialConfirm()" Text="Free Trial" CssClass="btn btn-success btn-block" ID="btnRegisterClientSide" />--%>
                                </div>
                                <div class="col-xs-6" id="dvbtnBuy" style="display: none;">
                                    <asp:Button runat="server" OnClick="BuyNow_Click" Text="Buy Now" CssClass="btn btn-success btn-block" ID="btnBuy" />
                                </div>
                                <div class="col-xs-6" id="dvbtnCancel">
                                    <asp:HyperLink runat="server" NavigateUrl="~/Account/Login.aspx" CssClass="btn btn-default btn-block">
                                        Cancel
                                    </asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript" src="//platform.linkedin.com/in.js">
         api_key: <%= System.Configuration.ConfigurationManager.AppSettings["LinkedInAppKey"] %>
         authorize: false
         onLoad: linkedIn.onLinkedInLoad
    </script>

    <script type="text/javascript">
        function trialConfirm() {
            bootbox.confirm("You will be registered to the current subscription on a trial basis for the subscription type selected without any charges.<br/>" +
                "However Paypal will collect your Paypal or credit card details in case your subscription is not cancelled before the trials ends in 30 days.", function (result) {
                    if (result) {
                        document.getElementById('<%= btnRegister.ClientID %>').click();
                    }
                    console.log(result);
                });
            return false;
        }

        //$(".LBD_CaptchaImageDiv a").removeAttr('href').removeAttr('onclick').attr('title','Registration Captcha');
        //BotDetect.RegisterCustomHandler('PostReloadImage',
        //    function() {
        //        return false;
        //        // your code goes here
        //    });

        var facebookAppId = <%=System.Configuration.ConfigurationManager.AppSettings["FBLoginAppId"].ToString()%>;



    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="../Scripts/XeroWebApp/Register.js"> </script>
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3820744.js"></script>

</asp:Content>
