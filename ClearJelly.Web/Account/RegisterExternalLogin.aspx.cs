﻿using ClearJelly.Web.Common;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;

namespace ClearJelly.Web.Account
{
    public partial class RegisterExternalLogin : System.Web.UI.Page
    {
        private static NLog.Logger _logger;

        public RegisterExternalLogin()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected string ProviderName
        {
            get { return (string)ViewState["ProviderName"] ?? String.Empty; }
            private set { ViewState["ProviderName"] = value; }
        }

        protected string ProviderAccountKey
        {
            get { return (string)ViewState["ProviderAccountKey"] ?? String.Empty; }
            private set { ViewState["ProviderAccountKey"] = value; }
        }

        protected void Page_Load()
        {
            try
            {// Process the result from an auth provider in the request
                ProviderName = IdentityHelper.GetProviderNameFromRequest(Request);
                if (String.IsNullOrEmpty(ProviderName))
                {
                    Response.Redirect("~/Account/Login", false);
                }
                if (!IsPostBack)
                {
                    var manager = new UserManager();
                    var loginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo();
                    if (loginInfo == null)
                    {
                        Response.Redirect("~/Account/Login", false);
                    }
                    var user = manager.Find(loginInfo.Login);
                    if (user != null)
                    {
                        IdentityHelper.SignIn(manager, user, isPersistent: false);
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                    }
                    else if (User.Identity.IsAuthenticated)
                    {
                        // Apply Xsrf check when linking
                        var verifiedloginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo(IdentityHelper.XsrfKey, User.Identity.GetUserId());
                        if (verifiedloginInfo == null)
                        {
                            Response.Redirect("~/Account/Login", false);
                        }

                        var result = manager.AddLogin(User.Identity.GetUserId(), verifiedloginInfo.Login);
                        if (result.Succeeded)
                        {
                            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        }
                        else
                        {
                            AddErrors(result);
                            return;
                        }
                    }
                    else
                    {
                        userName.Text = loginInfo.DefaultUserName;
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of RegisterExternalLogin Page_Load event completed.", ex);
                throw ex;
            }
        }

        protected void LogIn_Click(object sender, EventArgs e)
        {
            try
            {
                CreateAndLoginUser();
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of RegisterExternalLogin LogIn_Click event completed.", ex);
                throw ex;
            }

        }

        private void CreateAndLoginUser()
        {
            try
            {
                if (!IsValid)
                {
                    return;
                }
                var manager = new UserManager();
                var user = new ApplicationUser() { UserName = userName.Text };
                IdentityResult result = manager.Create(user);
                if (result.Succeeded)
                {
                    var loginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo();
                    if (loginInfo == null)
                    {
                        Response.Redirect("~/Account/Login", false);
                        return;
                    }
                    result = manager.AddLogin(user.Id, loginInfo.Login);
                    if (result.Succeeded)
                    {
                        IdentityHelper.SignIn(manager, user, isPersistent: false);
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        return;
                    }
                }
                AddErrors(result);
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of RegisterExternalLogin CreateAndLoginUser event completed.", ex);
                throw ex;
            }

        }

        private void AddErrors(IdentityResult result)
        {
            try
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of RegisterExternalLogin AddErrors event completed.", ex);
                throw;
            }

        }
    }
}