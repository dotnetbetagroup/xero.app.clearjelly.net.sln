﻿using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ClearJelly.Web.Common;

namespace ClearJelly.Web.Account
{
    public partial class OpenAuthProviders : System.Web.UI.UserControl
    {

        private static NLog.Logger _logger;

        public OpenAuthProviders()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    var provider = Request.Form["provider"];
                    if (provider == null)
                    {
                        return;
                    }
                    // Request a redirect to the external login provider
                    string redirectUrl = ResolveUrl(String.Format(CultureInfo.InvariantCulture, "~/Account/RegisterExternalLogin?{0}={1}&returnUrl={2}", IdentityHelper.ProviderNameKey, provider, ReturnUrl));
                    var properties = new Microsoft.Owin.Security.AuthenticationProperties() { RedirectUri = redirectUrl };
                    // Add xsrf verification when linking accounts
                    if (Context.User.Identity.IsAuthenticated)
                    {
                        properties.Dictionary[IdentityHelper.XsrfKey] = Context.User.Identity.GetUserId();
                    }
                    Context.GetOwinContext().Authentication.Challenge(properties, provider);
                    Response.StatusCode = 401;
                    Response.End();
                }
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of OpenAuthProvider Page_Load event completed.", ex);
            }

        }

        public string ReturnUrl { get; set; }

        public IEnumerable<string> GetProviderNames()
        {
            try
            {
                return Context.GetOwinContext().Authentication.GetExternalAuthenticationTypes().Select(t => t.AuthenticationType);
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of OpenAuthProvider GetProviderNames event completed.", ex);
                throw ex;
            }

        }
    }
}