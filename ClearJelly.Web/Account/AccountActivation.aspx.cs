﻿using ClearJelly.Entities;
using System;
using System.Linq;
using System.Web;
using ClearJelly.Web.Common;

namespace ClearJelly.Web.Account
{
    public partial class AccountActivation : System.Web.UI.Page
    {

        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

        public AccountActivation()
        {
            try
            {
                _logger = NLog.LogManager.GetCurrentClassLogger();
                _clearJellyContext = new ClearJellyEntities();
            }
            catch (Exception ex)
            {
                _logger.Info("AccountActivation "+ ex.Message + "\n" + ex);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Acccount Activation Page_Load event started.");
                var _code = Request.QueryString["code"];
                var _email = Request.QueryString["email"];
                var requestedUser =
                    _clearJellyContext.Users.SingleOrDefault(
                        x => x.ActivationCode == _code && x.Email == _email && !x.IsDeleted);
                if (requestedUser == null)
                {
                    lbl_expired.Visible = true;
                    Panel_reset_pwd.Visible = false;
                }
                else
                {
                    lbl_expired.Visible = false;
                    Panel_reset_pwd.Visible = true;
                    //activate User and Create Company database for the admin.
                    ClearJelly.Web.Common.Common.addUserSchema(requestedUser.UserId);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Acccount Activation Page_Load event completed.", ex);
                errorActivation.Visible = true;
                lbl_expired.Visible = false;
                Panel_reset_pwd.Visible = false;
            }
        }
    }
}