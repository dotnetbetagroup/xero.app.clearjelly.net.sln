﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="AccountActivation.aspx.cs" Inherits="ClearJelly.Web.Account.AccountActivation" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <title>Account Activation</title>
    <div>
        &nbsp;&nbsp;
      
        <br />
        <br />
        <br />
        <div>

            <asp:Label runat="server" ID="errorActivation" Text=" We are facing some problem while activating your account. Please contact the Site Administrator at admin@clearjelly.net to activate your account." Visible="False"></asp:Label>

            <asp:Label runat="server" ID="lbl_expired" Text="The requested link is expired. Please contact the Site Administrator at admin@clearjelly.net for any query." Visible="False"></asp:Label>
            <asp:Panel ID="Panel_reset_pwd" runat="server" Visible="false">
                <div>
                    <asp:Label runat="server" Text="Your account has been activated successfully."></asp:Label>
                </div>
                <asp:HyperLink runat="server" NavigateUrl="~/Account/Login.aspx">Click here to login</asp:HyperLink>
            </asp:Panel>
        </div>
    </div>
    <script>
        //$(document).ready(function () {
        //    $("#divLoading").show();
        //});
        //$(window).load(function () {
        //    // executes when complete page is fully loaded, including all frames, objects and images
        //    $("#divLoading").hide();
        //});
    </script>
</asp:Content>
