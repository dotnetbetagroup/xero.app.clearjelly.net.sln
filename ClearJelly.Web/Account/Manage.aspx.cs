﻿using ClearJelly.Web.Common;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClearJelly.XeroApp;

namespace ClearJelly.Web.Account
{
    public partial class Manage : System.Web.UI.Page
    {
        private static NLog.Logger _logger;

        public Manage()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected bool CanRemoveExternalLogins
        {
            get;
            private set;
        }

        private bool HasPassword(ClearJelly.Web.Common.UserManager manager)
        {
            try
            {
                var user = manager.FindById(User.Identity.GetUserId());
                return (user != null && user.PasswordHash != null);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage HasPassword event completed.", ex);
                throw;
            }

        }

        protected void Page_Load()
        {
            try
            {
                if (!IsPostBack)
                {
                    // Determine the sections to render
                    UserManager manager = new UserManager();
                    if (HasPassword(manager))
                    {
                        changePasswordHolder.Visible = true;
                    }
                    else
                    {
                        setPassword.Visible = true;
                        changePasswordHolder.Visible = false;
                    }
                    CanRemoveExternalLogins = manager.GetLogins(User.Identity.GetUserId()).Count() > 1;

                    // Render success message
                    var message = Request.QueryString["m"];
                    if (message != null)
                    {
                        // Strip the query string from action
                        Form.Action = ResolveUrl("~/Account/Manage");

                        SuccessMessage =
                            message == "ChangePwdSuccess" ? "Your password has been changed."
                            : message == "SetPwdSuccess" ? "Your password has been set."
                            : message == "RemoveLoginSuccess" ? "The account was removed."
                            : String.Empty;
                        successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
                    }
                }

            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage Page_Load event completed.", ex);
            }

        }

        protected void ChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    UserManager manager = new UserManager();
                    var encryptNewPassword = EncryptDecrypt.EncryptString(NewPassword.Text);
                    var encryptCurrentPassword = EncryptDecrypt.EncryptString(CurrentPassword.Text);
                    IdentityResult result = manager.ChangePassword(User.Identity.GetUserId(), encryptCurrentPassword, encryptNewPassword);
                    if (result.Succeeded)
                    {
                        var user = manager.FindById(User.Identity.GetUserId());
                        IdentityHelper.SignIn(manager, user, isPersistent: false);
                        Response.Redirect("~/Account/Manage?m=ChangePwdSuccess", false);
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }

            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage ChangePassword_Click event completed.", ex);
            }

        }

        protected void SetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    // Create the local login info and link the local account to the user
                    UserManager manager = new UserManager();
                    var encryptPassword = EncryptDecrypt.EncryptString(password.Text);
                    IdentityResult result = manager.AddPassword(User.Identity.GetUserId(), encryptPassword);
                    if (result.Succeeded)
                    {
                        Response.Redirect("~/Account/Manage?m=SetPwdSuccess", false);
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage SetPassword_Click event completed.", ex);
            }

        }

        public IEnumerable<UserLoginInfo> GetLogins()
        {
            try
            {
                UserManager manager = new UserManager();
                var accounts = manager.GetLogins(User.Identity.GetUserId());
                CanRemoveExternalLogins = accounts.Count() > 1 || HasPassword(manager);
                return accounts;
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage GetLogins event completed.", ex);
                throw ex;
            }

        }

        public void RemoveLogin(string loginProvider, string providerKey)
        {
            try
            {
                UserManager manager = new UserManager();
                var result = manager.RemoveLogin(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
                string msg = String.Empty;
                if (result.Succeeded)
                {
                    var user = manager.FindById(User.Identity.GetUserId());
                    IdentityHelper.SignIn(manager, user, isPersistent: false);
                    msg = "?m=RemoveLoginSuccess";
                }
                Response.Redirect("~/Account/Manage" + msg, false);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage RemoveLogin event completed.", ex);
            }

        }

        private void AddErrors(IdentityResult result)
        {
            try
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }

            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Manage AddErrors event completed.", ex);
            }

        }
    }
}