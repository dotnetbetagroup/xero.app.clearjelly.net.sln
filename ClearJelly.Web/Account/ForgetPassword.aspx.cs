﻿using System;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Collections.Generic;
using System.Net.Mail;
using ClearJelly.Entities;
using ClearJelly.Web.Common;
using ClearJelly.XeroApp;
using ClearJelly.Services;

namespace ClearJelly.Web.Account
{
    public partial class ForgetPassword : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;

    public ForgetPassword()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _clearJellyContext = new ClearJellyEntities(); ;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btn_send_Click(object sender, EventArgs e)
        {
            try
            {
                var requestedUser = _clearJellyContext.Users.SingleOrDefault(x => x.Email == txt_email.Text && !x.IsDeleted);
                if (requestedUser == null)
                {
                    lbl_msg.Text = "Email address is not registered with ClearJelly. Please enter a valid Email address.";
                    txt_email.Text = "";
                    return;
                }
                else
                {
                    //update activation code in db.
                    var code = Guid.NewGuid();
                    requestedUser.PwdResetCode = code.ToString();
                    SmtpMailSendingService.SendForgotPasswordMailMessage(requestedUser, code); 
                    lbl_msg.Text = "A link to reset the password has been sent to your registered email address";
                    lbl_msg.ForeColor = System.Drawing.Color.Green;
                    txt_email.Text = "";
                    _clearJellyContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // if there will be any error created at the time of the sending mail then it goes inside the catch
                //and display the error in the label
                lbl_msg.Text = "Something went wrong, please try again. If the problem persists, please contact the site administrator.";
                _logger.Error((txt_email.Text ?? "") + " ~Execution of Forgot Password btn_send_Click event completed.", ex);
            }
            finally
            {
            }
        }

        private string GetFromAddress()
        {
            try
            {
                System.Configuration.Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
                return settings.Smtp.From;
            }
            catch (Exception ex)
            {

                _logger.Error((txt_email.Text ?? "") + " ~Execution of Forgot Password GetFromAddress event completed.", ex);
                throw ex;
            }

        }
    }
}