﻿using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using ClearJelly.Entities;
using ClearJelly.ViewModels.Account;
using ClearJelly.Web.Common;
using ClearJelly.XeroApp;
using PaypalIntegration.PayPallHelper;
using PaypalIntegration;
using ClearJelly.Configuration;
using ClearJelly.AzureIntegration;
using System.Threading.Tasks;
using System.Net;
using ClearJelly.Services;

namespace ClearJelly.Web.Account
{
    public partial class Register : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        protected List<SubscriptionTypeViewModel> _subscriptionTypes = new List<SubscriptionTypeViewModel>();
        private static NLog.Logger _logger;
        private string _chosenService = "";

        public Register()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //var aa = AuthenticationHelper.GetToken();
            try
            {
                //GetFromAddress();             
                _chosenService = Request.QueryString["chosenService"];
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register Page_Load event started.");
                ClientScript.RegisterClientScriptBlock(GetType(),
                 "isPostBack",
                 String.Format("var isPostBack = {0};", IsPostBack.ToString().ToLower()),
                 true);

                if ((User != null && User.Identity.Name != "") || (Session["Jelly_user"] != null && Session["Jelly_user"].ToString() != ""))
                {
                    Response.Redirect("~/Home", false);
                }

                if (!IsPostBack)
                {
                    var token = Request.QueryString["token"];
                    var payerId = Request.QueryString["PayerID"];
                    _logger.Info((Session["Registred_user"] ?? "") + " ~token=" + token + ", payerId=" + payerId);
                    Session.Remove("Registred_user");
                    if (Session["Registered_userId"] != null
                        && !string.IsNullOrEmpty(token))
                    {
                        if (!string.IsNullOrEmpty(payerId))
                        {
                            if (Session["Registered_userId"] == null)
                            {
                                throw new Exception("Invalid Session Registered_userId");
                            }
                            else
                            {
                                var registeredUserId = Session["Registered_userId"].ToString();
                                var userId = Convert.ToInt32(registeredUserId);

                                var user = _clearJellyContext.Users.Single(x => x.UserId == userId && !x.IsDeleted);
                                var companySubscription = _clearJellyContext.CompanySubscriptions.OrderByDescending(x => x.StartDate)
                                    .First(x => x.CompanyId == user.CompanyId && !x.IsActive);
                                _logger.Info((Session["Registred_user"] ?? "") + " ~Creating Recurring Payment Profile");
                                var recurringProfileId = StartRecurringPayment(token, companySubscription);
                                _logger.Info((Session["Registred_user"] ?? "") + " ~Recurring ProfileId=" + recurringProfileId);
                                _logger.Info((Session["Registred_user"] ?? "") + " ~Updating paypal details in db");

                                UpdatePaypalDetails(user, token, payerId, recurringProfileId, companySubscription);
                                _logger.Info((Session["Registred_user"] ?? "") + " ~Updated paypal details in db");

                                if (!bool.Parse(ClearJelly.Web.Common.Common.GetIpnInclude()))
                                {
                                    if (recurringProfileId != null)
                                    {
                                        ActivatePlan(companySubscription);
                                    }
                                }
                                _logger.Info((Session["Registred_user"] ?? "") + " ~Paypal token work done.");
                                Session.Abandon();
                                Session.Remove("Registred_user");
                                Session.Remove("IsTrial");
                                //if error while sending email.
                                if (Session["IsEmailSendingFailed"] != null && Convert.ToBoolean(Session["IsEmailSendingFailed"]) == true)
                                {
                                    Session.Remove("IsEmailSendingFailed");
                                    Response.Redirect("~/Account/ActivateAccount.aspx?activationLinkFail=true", false);
                                }
                                else
                                {
                                    Response.Redirect("~/Account/ActivateAccount.ASPX?success=true", false);
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Account/ActivateAccount.ASPX?success=false", false);
                        }
                        Session.Remove("Registered_userId");

                    }
                    else
                    {
                        PopulateCountryList();
                        if (Session["LinkedinModel"] != null)
                        {
                            try
                            {
                                var linkedinModel = (LinkedinResponseViewModel)Session["LinkedinModel"];
                                Session["LinkedinId"] = linkedinModel.UserId;
                                SetLinkedinData(linkedinModel);
                            }
                            catch (Exception ex)
                            {
                                _logger.Error((Session["Registred_user"] ?? "") + "Register Page_Load Linkdin :" + ex.Message, ex);
                            }
                            finally
                            {
                                Session.Remove("LinkedinModel");
                            }
                        }
                        else if (Session["FacebookModel"] != null)
                        {
                            try
                            {
                                var facebookModel = (FacebookResponseViewModel)Session["FacebookModel"];
                                Session["FacebookId"] = facebookModel.Id;
                                SetFacebookData(facebookModel);
                            }
                            catch (Exception ex)
                            {
                                _logger.Error((Session["Registred_user"] ?? "") + "Register Page_Load Facebook :" + ex.Message, ex);
                            }
                            finally
                            {
                                Session.Remove("FacebookModel");
                            }
                        }
                    }
                }
                else
                {
                    PopulateCountryList(HdnCountryId.Value);
                }
                _subscriptionTypes = GetSubscriptionPlans();
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Registred_user"] ?? "") + "Execution of Page_Load event completed. :" + ex.Message, ex);
            }
        }

        private void SetLinkedinData(LinkedinResponseViewModel linkedinModel)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register SetLinkedinData event started.");
                FirstName.Text = linkedinModel.FirstName;
                LastName.Text = linkedinModel.LastName;
                Email.Text = linkedinModel.EmailAddress;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register SetLinkedinData event completed. :" + ex.Message, ex);
                throw;
            }

        }

        private void SetFacebookData(FacebookResponseViewModel facebookModel)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register SetFacebookData event started.");
                if (facebookModel.Name != null)
                {
                    FirstName.Text = facebookModel.FirstName;
                    LastName.Text = facebookModel.LastName;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register SetFacebookData event completed. :" + ex.Message, ex);
                throw;
            }
        }

        private void PopulateCountryList(string selectedCountryId = null)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register PopulateCountryList event started.");
                var countries = _clearJellyContext.Countries.ToList();
                CountryDropDown.Items.Insert(0, "Please select");
                for (int index = 0; index < countries.Count; index++)
                {
                    var countryListItem = new ListItem(countries[index].Name, countries[index].CountryId.ToString());
                    var dropDownIndex = index + 1;
                    CountryDropDown.Items.Insert(dropDownIndex, countryListItem);
                    CountryDropDown.Items[dropDownIndex].Attributes.Add("data-gstrate",
                        countries[index].GSTRate.ToString());
                    if (string.IsNullOrEmpty(selectedCountryId) && countryListItem.Text == "Australia")
                    {
                        CountryDropDown.Items[dropDownIndex].Selected = true;
                    }
                }
                if (!string.IsNullOrEmpty(selectedCountryId))
                {
                    CountryDropDown.SelectedValue = selectedCountryId;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register PopulateCountryList event completed. :" + ex.Message, ex);
                throw;
            }
        }

        private string StartRecurringPayment(string token, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register StartRecurringPayment event started.");
                //GetExpressCheckoutDetails 
                var exp = new PayPalExpressCheckout();
                var paymentStartDate = DateTime.Now.Date;
                //trial recuiring profile start after one month.

                if (Session["IsTrial"] == null)
                {
                    throw new Exception("Invalid session IsTrial.");
                }
                var isTrial = Convert.ToBoolean(Session["IsTrial"]);

                if (isTrial)
                {
                    paymentStartDate = paymentStartDate.AddMonths(1);
                }
                decimal billingAmount;
                if (companySubscription.IsYearlySubscription)
                {
                    var grossAmount = companySubscription.SubscriptionType.YearlyContractFee * companySubscription.Quantity;
                    var gstAmount = Math.Round(grossAmount * companySubscription.Company.Country.GSTRate / 100, 2);
                    billingAmount = grossAmount + gstAmount;
                }
                else
                {
                    var grossAmount = companySubscription.SubscriptionType.MonthlyFee * companySubscription.Quantity;
                    var gstAmount = Math.Round(grossAmount * companySubscription.Company.Country.GSTRate / 100, 2);
                    billingAmount = grossAmount + gstAmount;
                }
                var reecurringProfile = new RecurringProfile()
                {
                    Amount = billingAmount,
                    BillingFrequency = 1,
                    BillingPeriod = ClearJelly.Web.Common.Common.GetBillingPeriodPaypal().ToLower() == "day" ? PayPalEnums.RecurringPeriod.Day : PayPalEnums.RecurringPeriod.Month,
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    StartDate = paymentStartDate,
                    ItemName = companySubscription.SubscriptionType.TypeName.Trim(),
                    Description = companySubscription.SubscriptionType.TypeName.Trim()
                };
                var resRecurring = exp.CreateRecurringPaymentsProfile(reecurringProfile, token);
                if (resRecurring.IsSystemFailure)
                {
                    _logger.Error(resRecurring.FailureMessage);
                    Response.Redirect("~/Account/ActivateAccount.ASPX?success=false", false);
                }
                return resRecurring.ProfileId;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register StartRecurringPayment event completed. :" + ex.Message, ex);
                throw;
            }
        }

        private List<SubscriptionTypeViewModel> GetSubscriptionPlans()
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register GetSubscriptionPlans event started.");
                var subscriptionTypes = _clearJellyContext.SubscriptionTypes.Where(a => a.IsActive).OrderBy(x => x.MonthlyFee).Select(x => new SubscriptionTypeViewModel
                {
                    Description = x.Description,
                    SubscriptionTypeId = x.SubscriptionTypeId,
                    MonthlyFee = x.MonthlyFee,
                    TypeName = x.TypeName,
                    AdditionalUserFee = x.AdditionalUserFee,
                    Entities = x.Entities,
                    YearlyContractFee = x.YearlyContractFee,
                    Quantity = 1
                }).ToList();
                if (subscriptionTypes.Count > 0)
                {
                    subscriptionTypes[0].IsCurrentSubscription = true;
                }
                return subscriptionTypes;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register GetSubscriptionPlans event completed. :" + ex.Message, ex);
                throw;
            }

        }

        public void ActivatePlan(CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register ActivatePlan event started.");
                if (companySubscription != null)
                {
                    companySubscription.IsActive = true;
                    companySubscription.EndDate = null;
                    _clearJellyContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register ActivatePlan event completed. :" + ex.Message, ex);
            }
            finally
            {
                Session.Abandon();
            }

        }

        private int UpdatePaypalDetails(User user, string token, string payerId, string recurringProfileId, CompanySubscription companySubscription)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of Register UpdatePaypalDetails event started.");
                user.PaypalToken = token;
                user.PaypalPayerId = payerId;
                companySubscription.RecurringProfileId = recurringProfileId;
                _clearJellyContext.SaveChanges();
                return user.CompanyId;
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of Register UpdatePaypalDetails event completed. :" + ex.Message, ex);
                throw;
            }

        }

        protected void BuyNow_Click(object sender, EventArgs e)
        {
            try
            {

                Session["IsTrial"] = false;
                string EncodedResponse = Request.Form["g-Recaptcha-Response"];
                bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "True" ? true : false);
                //bool isValidCaptcha = SampleCaptcha.Validate(CaptchaCodeTextBox.Text);
                if (IsCaptchaValid)
                {
                    Session["Registred_user"] = Email;
                    _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register BuyNow_Click event started.");
                    if (this.chkboxTerms.Checked)
                    {
                        var email = Email.Text.Trim();
                        if (ClearJelly.Web.Common.Common.UserExists(email))
                        {
                            ErrorMessage.Text = "The user already exists. Please enter a different username.";
                            return;
                        }
                        //if (companyExists())
                        //{
                        //    ErrorMessage.Text = "The company is already registered. Please enter a different company name.";
                        //    return;
                        //}
                        const bool isTrial = false;

                        var profilePicture = ProfilePicture.PostedFile != null ? email + System.IO.Path.GetExtension(ProfilePicture.FileName) : null;
                        try
                        {
                            if (ProfilePicture.PostedFile.ContentLength > 0)
                            {
                                var imagePath = WebConfigurationManager.AppSettings["ProfilePicturePath"];
                                System.IO.FileInfo file = new System.IO.FileInfo(imagePath);
                                file.Directory.Create();
                                ProfilePicture.SaveAs(imagePath + profilePicture);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register buyNow_Click profile picture save. :" + ex.Message, ex);
                            ErrorMessage.Text = "Profile pic could not be saved, please contact the siteadministrator at admin@clearjelly.net";
                            return;
                        }

                        var registeredUser = addUser(email, isTrial, profilePicture, _chosenService);

                        _logger.Info((Session["Registred_user"] ?? "") + " ~Sending activation email.");
                        string currentDomain = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        _logger.Info("currentDomain:  Request.Url.GetLeftPart: " + currentDomain);
                        try
                        {
                            ClearJelly.Web.Common.Common.SendActivationLink(email, registeredUser, currentDomain);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register BuyNow_Click SendActivationLink event. :" + ex.Message, ex);
                            //Response.Redirect("~/Account/ActivateAccount.aspx?activationLinkFail=true", false);
                            Session["IsEmailSendingFailed"] = true;
                        }

                        _logger.Info((Session["Registred_user"] ?? "") + " ~Calling SubscribePlan.");
                        SubscribePlan(registeredUser.UserId);
                        _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register BuyNow_Click event completed.");
                    }
                }
                else
                {
                    ErrorMessage.Text = "Please enter a valid verification code.";
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register BuyNow_Click event completed. :" + ex.Message, ex);
                ErrorMessage.Text = "Some Internal error occurred please contact siteadministrator at admin@clearjelly.net.";
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // initial page setup
            if (!IsPostBack)
            {
                // Captcha code properties
                //CaptchaCodeTextBox.Text = "";
                //SampleCaptcha.CodeLength = 4;
            }
        }

        protected void Trial_Click(object sender, EventArgs e)
        {
            try
            {
                string EncodedResponse = Request.Form["g-Recaptcha-Response"];
                var test = ClearJelly.Web.Common.ReCaptchaClass.Validate(EncodedResponse);
                bool IsCaptchaValid = (test == "true" ? true : false);
                //bool isValidCaptcha = SampleCaptcha.Validate(CaptchaCodeTextBox.Text);
                if (IsCaptchaValid)
                {
                    Session["Registred_user"] = Email.Text.Trim();
                    _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register Trial_Click event started.");
                    Session["IsTrial"] = true;
                    if (this.chkboxTerms.Checked)
                    {

                        String email = Email.Text.Trim() + "@" + ConfigSection.Tenant;

                        if (ClearJelly.Web.Common.Common.UserExists(email))
                        {
                            ErrorMessage.Text = "The user already exists. Please enter a different username.";
                            return;
                        }
                        if (ClearJelly.Web.Common.Common.EmailExists(UserEmail.Text))
                        {
                            ErrorMessage.Text = "The email already exists. Please enter a different email.";
                            return;
                        }
                        //if (companyExists())
                        //{
                        //    ErrorMessage.Text = "The company is already registered. Please enter a different company name.";
                        //    return;
                        //}
                        const bool isTrial = true;
                        var profilePicture = ProfilePicture.PostedFile != null ? email + System.IO.Path.GetExtension(ProfilePicture.FileName) : null;
                        try
                        {
                            if (ProfilePicture.PostedFile.ContentLength > 0)
                            {
                                var imagePath = WebConfigurationManager.AppSettings["ProfilePicturePath"];
                                System.IO.FileInfo file = new System.IO.FileInfo(imagePath);
                                file.Directory.Create();
                                ProfilePicture.SaveAs(imagePath + profilePicture);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register Trial_Click profile picture save. :" + ex.Message, ex);
                            ErrorMessage.Text = "Profile pic could not be saved, please contact the siteadministrator at admin@clearjelly.net";
                            return;
                        }

                        var registeredUser = addUser(email, isTrial, profilePicture, _chosenService);
                        string currentDomain = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        //try
                        //{
                        //    ClearJelly.Web.Common.Common.SendActivationLink(email, registeredUser, currentDomain);
                        //}
                        //catch (Exception ex)
                        //{
                        //    _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register Trial_Click SendActivationLink event. :" + ex.Message, ex);
                        //    //Response.Redirect("~/Account/ActivateAccount.aspx?activationLinkFail=true", false);
                        //    Session["IsEmailSendingFailed"] = true;
                        //}
                        Session.Remove("Registered_userId");
                        //Response.Redirect("~/Account/ActivateAccount.ASPX?success=true", false);
                        if (Session["IsEmailSendingFailed"] != null && Convert.ToBoolean(Session["IsEmailSendingFailed"]) == true)
                        {
                            Session.Remove("IsEmailSendingFailed");
                            Response.Redirect("~/Account/ActivateAccount.aspx?activationLinkFail=true", false);
                        }
                        else
                        {
                            Response.Redirect("~/Account/ActivateAccount.ASPX?success=true", false);
                        }
                        //SubscribePlan(registeredUser.UserId);
                        //addUserSchema();
                    }
                    else
                    {
                        ErrorMessage.Text = "You have to accept the terms and conditions to proceed.";
                    }
                }
                else
                {
                    ErrorMessage.Text = "Please enter valid captcha.";
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Registred_user"] ?? "") + "~Execution of Register Trial_Click event completed. :" + ex.Message, ex);
                ErrorMessage.Text = "Some Internal error occurred please contact siteadministrator at admin@clearjelly.net.";

            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static bool companyExists(string companyName)
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    _logger.Info(" ~Execution of Register companyExists event started.");
                    var company = dbContext.Companies.SingleOrDefault(x => x.Name == companyName);
                    if (company == null)
                        return false;
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Execution of Register companyExists event completed. :" + ex.Message, ex);
            }
            return false;
        }


        private void SubscribePlan(int userId)
        {
            try
            {
                _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register SubscribePlan event started.");
                int subscriptionTypeId;
                var result = int.TryParse(HdnSubscriptionTypeId.Value, out subscriptionTypeId);
                if (!result)
                {
                    throw new InvalidOperationException();
                }
                var subscriptionType = GetSubscriptionTypeById(subscriptionTypeId);
                PaypalCheckout(subscriptionType, userId);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register SubscribePlan event completed. :" + ex.Message, ex);
                throw;
            }

        }

        public SubscriptionTypeViewModel GetSubscriptionTypeById(int subscriptionTypeId)
        {
            try
            {
                _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register GetSubscriptionTypeById event started.");
                return _clearJellyContext.SubscriptionTypes.Where(x => x.IsActive && x.SubscriptionTypeId == subscriptionTypeId)
                    .Select(x => new SubscriptionTypeViewModel
                    {
                        Description = x.Description,
                        MonthlyFee = x.MonthlyFee,
                        YearlyContractFee = x.YearlyContractFee,
                        TypeName = x.TypeName,
                        SubscriptionTypeId = x.SubscriptionTypeId
                    }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register GetSubscriptionTypeById event completed. :" + ex.Message, ex);
                throw;
            }

        }

        private void PaypalCheckout(SubscriptionTypeViewModel subscriptionType, int userId)
        {
            try
            {

                _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register PaypalCheckout event started.");

                var expChkt = new PayPalExpressCheckout();
                var orderDetailItem = new List<OrderDetailsItem>();
                var currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                decimal orderTotal;
                var result = decimal.TryParse(HdnNetAmount.Value, out orderTotal);

                decimal orderAmount;
                decimal.TryParse(HdnTotal.Value, out orderAmount);
                decimal orderGST;
                decimal.TryParse(HdnGST.Value, out orderGST);

                if (!result)
                {
                    throw new InvalidOperationException();
                }
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = subscriptionType.TypeName.Trim(),
                    IsRecurringPayment = true,
                    UnitPrice = orderAmount,
                    Quantity = 1
                });
                orderDetailItem.Add(new OrderDetailsItem
                {
                    Description = "GST(10%)",
                    IsRecurringPayment = true,
                    UnitPrice = orderGST,
                    Quantity = 1
                });
                // Get Token
                var reqOrderDetail = new OrderDetails()
                {
                    CurrencyCodeType = PayPalEnums.CurrencyCodeType.AUD,
                    DisplayShippingAddress = PayPalEnums.PayPalNoShipping.DoNotDisplay,
                    OrderTotal = orderTotal,
                    Items = orderDetailItem
                };

                var resToken = expChkt.SetExpressCheckout(reqOrderDetail, currentUrl, currentUrl);
                if (!resToken.IsSystemFailure)
                {
                    Session["Registered_userId"] = userId;
                }
                Response.Redirect(resToken.RedirectUrl, false);
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register PaypalCheckout event completed. :" + ex.Message, ex);
                throw;
            }
        }

        private User addUser(String email, bool isTrial, string profilePicture, string service)
        {
            try
            {
                _logger.Info((Session["Registred_user"] ?? "") + " ~Execution of Register addUser event started.");
                var encryptPassword = EncryptDecrypt.EncryptString(Password.Text);
                var currentSubscription = string.Empty;
                var activationCode = Guid.NewGuid();
                int countryId;
                var result = int.TryParse(HdnCountryId.Value, out countryId);
                if (!result)
                {
                    throw new InvalidOperationException();
                }
                var country = _clearJellyContext.Countries.Single(x => x.CountryId == countryId);
                var company = new Company
                {
                    Name = CompanyName.Text,
                    Address = CompanyAddress.Text,
                    Website = Website.Text,
                    CountryId = country.CountryId
                };

                var newUser = new User
                {
                    Email = email,
                    UserEmail = UserEmail.Text,
                    Password = encryptPassword,
                    FirstName = FirstName.Text,
                    LastName = LastName.Text,
                    Address = Address.Text,
                    Mobile = Mobile.Text,
                    ProfilePicture = profilePicture,
                    ActivationCode = activationCode.ToString(),
                    RegistrationDate = DateTime.Now.Date,
                    IsAdmin = true,
                    CompanyId = company.CompanyId,
                    ChosenService = service
                };
                if (Session["LinkedinId"] != null)
                {
                    newUser.LinkedInUserId = Session["LinkedinId"].ToString();
                    newUser.LoginType = 1;
                    Session.Remove("LinkedinId");
                }
                else if (Session["FacebookId"] != null)
                {
                    newUser.FBUserId = Session["FacebookId"].ToString();
                    newUser.LoginType = 1;
                    Session.Remove("FacebookId");
                }
                _logger.Info("Start creation User in Azure :");
                AuthenticationHelper authenticationHelper = new AuthenticationHelper();
                 authenticationHelper.CreateUserAzureAd(newUser, country.Name, Password.Text);
                _logger.Info("Finish creation User in Azure :");
                //var createUserAd = authenticationHelper.CreateUserAzureAd(newUser, country.Name, Password.Text);
                // createUserAd.Wait();
                try
                {
                    _clearJellyContext.Users.Add(newUser);
                    _clearJellyContext.Companies.Add(company);
                }
                catch (Exception ex)
                {

                }

                var companySubscription = new CompanySubscription();
                if (!isTrial)
                {
                    int subscriptionTypeId;
                    result = int.TryParse(HdnSubscriptionTypeId.Value, out subscriptionTypeId);
                    if (!result)
                    {
                        throw new InvalidOperationException();
                    }
                    var selectedSubscription =
                        _clearJellyContext.SubscriptionTypes.Single(x => x.IsActive && x.SubscriptionTypeId == subscriptionTypeId);
                    companySubscription = new CompanySubscription
                    {
                        CompanyId = company.CompanyId,
                        StartDate = DateTime.Now.Date,
                        EndDate = null,
                        SubscriptionTypeId = subscriptionTypeId,
                        AdditionalUsers = 0,
                        Quantity = Convert.ToInt32(HdnQuantity.Value),
                        StartedAsTrial = isTrial,
                        IsYearlySubscription = HdnSubscriptionMonths.Value == "12"
                    };
                    currentSubscription = selectedSubscription.TypeName;
                    currentSubscription = isTrial ? selectedSubscription.TypeName + " (Trial)" : selectedSubscription.TypeName;
                }
                else
                {
                    var trialSubscription = _clearJellyContext.SubscriptionTypes.Where(a => a.IsActive)
                       .OrderByDescending(x => x.Entities == 1)
                       .ThenByDescending(x => x.MonthlyFee)
                       .First();
                    companySubscription = new CompanySubscription
                    {
                        CompanyId = company.CompanyId,
                        StartDate = DateTime.Now.Date,
                        EndDate = DateTime.Now.Date.AddMonths(1),
                        SubscriptionTypeId = trialSubscription.SubscriptionTypeId,
                        IsActive = false,
                        AdditionalUsers = 0,
                        Quantity = 1,
                        IsYearlySubscription = false,
                        StartedAsTrial = true
                    };
                    currentSubscription = trialSubscription.TypeName + " (Trial)";
                }
                _clearJellyContext.CompanySubscriptions.Add(companySubscription);
                _clearJellyContext.SaveChanges();

                try
                {
                    SmtpMailSendingService.SendRegisteredUserDetailsToClient(UserEmail.Text, newUser, currentSubscription);
                }
                catch (Exception ex)
                {
                    _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register SendRegisteredUserDetailsToAdmin event. :" + ex.Message, ex);
                    //Response.Redirect("~/Account/ActivateAccount.aspx?activationLinkFail=true", false);
                    Session["IsEmailSendingFailed"] = true;
                }

                return newUser;
            }

            catch (Exception ex)
            {
                _logger.Error((Session["Registred_user"] ?? "") + "Execution of Register addUser event completed. :" + ex.Message, ex);
                throw;
            }
        }


        //private void addUserSchema()
        //{
        //    try
        //    {
        //        var companyName = CompanyName.Text;
        //        var userName = Email.Text;
        //        var password = Password.Text;
        //        //TODO: Give proper Databasae name as per requirement and test whether it is created successfully.
        //        SqlConnection MyConnection = null;
        //        String sqlStr = @" Execute [dbo].[Create_Xero_DB_Template] '" + companyName + "','" + userName + "','" + password + "'";
        //        var strCon = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //        MyConnection = new SqlConnection(strCon);
        //        MyConnection.Open();
        //        SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
        //        myCommand.ExecuteReader();
        //    }
        //    catch (SqlException ex)
        //    {
        //        Console.WriteLine(ex.Message.ToString());
        //        Debug.WriteLine(ex.Message.ToString());
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message.ToString());
        //        Debug.WriteLine(ex.Message.ToString());
        //        throw;
        //    }
        //}


        //private List<SubscriptionTypeViewModel> GetSubscriptionPlansService()
        //{
        //    return _clearJellyContext.SubscriptionTypes.OrderBy(x => x.MonthlyFee).Select(x => new SubscriptionTypeViewModel
        //    {
        //        Description = x.Description,
        //        SubscriptionTypeId = x.SubscriptionTypeId,
        //        MonthlyFee = x.MonthlyFee,
        //        TypeName = x.TypeName,
        //        AdditionalUserFee = x.AdditionalUserFee,
        //        Entities = x.Entities,
        //        YearlyContractFee = x.YearlyContractFee
        //    }).ToList();
        //}

        //[WebMethod]
        //[ScriptMethod(UseHttpGet = true)]
        //public static List<SubscriptionTypeViewModel> GetSubscriptionPlans()
        //{
        //    var registerPage = new Account_Register();
        //    var subscriptionPlans = registerPage.GetSubscriptionPlansService();
        //    return subscriptionPlans;
        //}
    }
}