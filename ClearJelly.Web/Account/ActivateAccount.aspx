﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ActivateAccount.aspx.cs" Inherits="ClearJelly.Web.Account.ActivateAccount" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <title>Activate Your Account</title>
    <div>
        &nbsp;&nbsp;
      
        <br />
        <br />
        <br />
        <div>
            Thank you for registering with ClearJelly! 
        </div>
        <div>
            <asp:Label runat="server" ID="lbl_success" Text="Your account has been successfully created. To start using it, click the activation link sent on your registered Email Address." Visible="False"></asp:Label>
            <asp:Label runat="server" ID="lbl_error" Text="Your payment details could not be verified at this time however you can complete the registration by clicking the activation link sent on your registered Email Address and complete the payment details later." Visible="False"></asp:Label>
             <asp:Label runat="server" ID="lbl_errorFail" Text="Your account has been created successfully but the system could not send the activation email. Please contact the Site Administrator at admin@clearjelly.net to activate your account." Visible="False"></asp:Label>
        </div>
        <div id="emailCheck" runat="server">
            In case you haven't received the Activation Email within 5 minutes, please check your Junk Email folder.
        </div>
    </div>
</asp:Content>
