﻿using System.Web;
using System.Web.Services;
using System;
using System.Linq;
using System.Web.UI;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using ClearJelly.Entities;
using Intuit.Ipp.OAuth2PlatformClient;
using ClearJelly.QBApp;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Text.RegularExpressions;
using ClearJelly.ViewModels;
using ClearJelly.Web.Common;
using NLog;
using ClearJelly.ViewModels.Account;
//using ClearJelly.QBApp.QBMappingModels;
using ClearJelly.Web.Common.Helpers;
using ClearJelly.XeroApp;
using ClearJelly.QBApp.QBService;

namespace ClearJelly.Web.Account
{
    public partial class Login : System.Web.UI.Page
    {

        private ClearJellyEntities _clearJellyContext;
        private static Logger _logger;
        private string _incoming_state;
        private string _realmId;
        private string _code;

        private static OAuth2Manager _manager = new OAuth2Manager(
                    OAuth2Settings.RedirectUri,
                    OAuth2Settings.DiscoveryUrl,
                    OAuth2Settings.ClientID,
                    OAuth2Settings.ClientSecret,
                    ""
                );

        public Login()
        {
            _clearJellyContext = new ClearJellyEntities();
            _logger = LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //var logout = Session["LogOut"];
            //if (logout != null)
            //{
            //    Session.Remove("LogOut");
            //}
            User user = new User();
            //try
            //{

            //}
            //catch (Exception ex)
            //{
            //    _logger.Error(ex.Message);
            //}

            if (User != null && User.Identity.Name != "")
            {
                user = _clearJellyContext.Users.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                //Session["User_fullname"] = user.FirstName + " " + user.LastName;
                //Session["Jelly_userId"] = user.UserId;
                //Session["Jelly_user"] = User.Identity.Name;
                Response.Redirect("~/Home.aspx", true);
            }
            if (Session["isUserExpired"] != null && Session["isUserExpired"].ToString() == "true")
            {
                Session.Remove("isUserExpired");
                //errorText.Visible = true;
            }
            //else { errorText.Visible = false; }

            //var userName = this.Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            //if (userName == null || userName == "")
            //{
            //var userName = User.Identity.Name;
            //}
            this.AsyncMode = true;
        }

        protected void LogInWithAd(object sender, EventArgs e)
        {
            HttpContext.Current.GetOwinContext()
                  .Authentication.Challenge(new AuthenticationProperties { RedirectUri = "/" },
                      OpenIdConnectAuthenticationDefaults.AuthenticationType);
        }

        protected void Register(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/Register.aspx", true);
        }

        public static bool emailIsValid(string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        public UserAndSubscriptionViewModel UserSubscriptionDetails(string userName, string password)
        {
            try
            {
                using (var dbContext = _clearJellyContext)
                {
                    UserAndSubscriptionViewModel userSubscriptions = new UserAndSubscriptionViewModel();
                    if (userName != null && password != null)
                    {

                        var userDetails = dbContext.Users.FirstOrDefault(x => x.Email == userName && x.Password == password);


                        if (userDetails != null)
                        {
                            var currentDate = DateTime.Now.Date;
                            var companySubscription = dbContext.CompanySubscriptions.
                                Where(x => x.CompanyId == userDetails.CompanyId
                                && x.IsActive == true
                                && (!x.EndDate.HasValue || x.EndDate.Value >= currentDate)
                            ).ToList();

                            foreach (var item in companySubscription)
                            {
                                ActiveSubscriptions actSub = new ActiveSubscriptions();
                                actSub.SubscriptionName = item.SubscriptionType.TypeName;
                                actSub.EndDate = item.EndDate.HasValue ? item.EndDate.Value.ToShortDateString() : null;
                                userSubscriptions.ActiveSubscriptions.Add(actSub);
                            }
                            var serverName = Common.Common.GetAzureServerName();
                            userSubscriptions.UserName = userDetails.Email + "@" + serverName;
                            // userSubscriptions.UserName = userDetails.Email;
                            userSubscriptions.DatabaseName = "AP_" + userDetails.Company.Name;
                            userSubscriptions.Password = EncryptDecrypt.DecryptString(password);
                            userSubscriptions.ServerName = serverName + ".database.windows.net";
                        }
                    }
                    return userSubscriptions;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Login UserSubscriptionDetails event completed.", ex);
                throw;
            }
        }

        private string LoginUser(User currentUser, string email, bool socialAccount, string redirectUrl = "")
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    var currentDate = DateTime.Now.Date;
                    var isSubscriptionExpired = dbContext.CompanySubscriptions.
                        Count(
                            x =>
                                x.CompanyId == currentUser.CompanyId && x.IsActive &&
                                (!x.EndDate.HasValue || x.EndDate.Value >= currentDate)) == 0;
                    Session["login"] = "valid";
                    Session["Jelly_user"] = email;
                    Session["User_fullname"] = currentUser.FirstName + " " + currentUser.LastName;
                    Session["IsAdmin"] = currentUser.IsAdmin;
                    Session["IsSubscriptionExpired"] = isSubscriptionExpired;
                    Session["Jelly_UserId"] = currentUser.UserId;
                    Session["Jelly_UserCompanyId"] = currentUser.CompanyId;
                    Session["UserSubscriptionDetails"] = UserSubscriptionDetails(email, currentUser.Password);
                    //update last login date after successfull attempt
                    currentUser.LastloginFailed = null;
                    dbContext.SaveChanges();

                    if (!socialAccount)
                    {
                        redirectUrl = HttpContext.Current.Request.QueryString["RedirectUrl"];
                    }
                    var returnPage = "";
                    if (redirectUrl == "ManageSubscription" || isSubscriptionExpired)
                    {
                        if (!socialAccount)
                        {
                            System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.Redirect("~/ManageSubscription.aspx", false);
                        }
                        else
                        {
                            returnPage = "ManageSubscription.aspx";
                            return returnPage;
                        }
                    }
                    else
                    {
                        if (!socialAccount)
                        {
                            System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.Redirect("~/Home.aspx", false);
                        }
                        else
                        {
                            returnPage = "Home.aspx";
                            return returnPage;
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Login LoginUsers event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        public static string LinkedinLoginCallback(LinkedinResponseViewModel linkedinData)
        {
            try
            {
                //LinkedinResponseViewModel model = new LinkedinResponseViewModel();
                var objLogin = new Login();
                var returnUrl = objLogin.AuthenticateLinkedInUser(linkedinData);
                return returnUrl;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Login LinkedinLoginCallback event completed.", ex);
                throw;
            }

        }

        public string AuthenticateLinkedInUser(LinkedinResponseViewModel linkedinData)
        {
            try
            {
                var returnUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
                User user = _clearJellyContext.Users.SingleOrDefault(x => x.Email.Trim().ToLower() == linkedinData.EmailAddress.Trim().ToLower() && x.IsActive && !x.IsDeleted);
                if (user == null)
                {
                    // add
                    Session["LinkedinModel"] = linkedinData;
                    returnUrl += "Account/Register.aspx";
                }
                else
                {
                    user.LinkedInUserId = linkedinData.UserId;
                    user.LoginType = 1;
                    _clearJellyContext.SaveChanges();
                    var socialAccount = true;
                    var redirectUrl = linkedinData.RedirectUrl;
                    returnUrl += LoginUser(user, user.Email, socialAccount, returnUrl);
                }
                return returnUrl;

            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Login FBLoginCallback event completed.", ex);
                throw ex;
            }

        }

        [WebMethod]
        public static string FBLoginCallback(FacebookResponseViewModel facebookData)
        {
            try
            {
                var objLogin = new Login();
                var returnUrl = objLogin.AuthenticateFacebookUser(facebookData);
                return returnUrl;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Login FBLoginCallback event completed.", ex);
                throw ex;
            }

        }

        public string AuthenticateFacebookUser(FacebookResponseViewModel facebookData)
        {
            try
            {
                var returnUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
                User user = _clearJellyContext.Users.SingleOrDefault(x => x.FBUserId == facebookData.Id && x.IsActive && !x.IsDeleted);
                if (user == null)
                {
                    // add
                    Session["FacebookModel"] = facebookData;
                    returnUrl += "Account/Register.aspx";
                }
                else
                {
                    var socialAccount = true;
                    var redirectUrl = facebookData.RedirectUrl;
                    returnUrl += LoginUser(user, user.Email, socialAccount, redirectUrl);
                }
                return returnUrl;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + " ~Execution of Login AuthenticateFacebookUser event completed.", ex);
                throw ex;
            }
        }

        protected async void AuthenticateQuickBook(object sender, EventArgs e)
        {

            await _manager.ConnectToQuickBooks();
            RedirectConfig config = _manager.DoOAuth();
            if (config != null)
            {
                Response.Redirect(config.AuthRequest, config.Target, config.WindowOptions);
            }
        }

        //public async Task PerformCodeExchange()
        //{
        //    _logger.Info("Exchanging code for tokens.");

        //    string id_token = "";
        //    string refresh_token = "";
        //    string access_token = "";
        //    bool isTokenValid = false;

        //    UserQb userInfo = new UserQb();

        //    //Request Oauth2 tokens
        //    var tokenClient = new TokenClient(_manager.TokenEndpoint, OAuth2Settings.ClientID, OAuth2Settings.ClientSecret);

        //    TokenResponse accesstokenCallResponse = await tokenClient.RequestTokenFromCodeAsync(_code, OAuth2Settings.RedirectUri);
        //    if (accesstokenCallResponse.HttpStatusCode == HttpStatusCode.OK)
        //    {
        //        //save the refresh token in persistent store so that it can be used to refresh short lived access tokens
        //        refresh_token = accesstokenCallResponse.RefreshToken;
        //        if (!_manager.Dictionary.ContainsKey("refreshToken"))
        //        {
        //            _manager.Dictionary.Add("refreshToken", refresh_token);
        //        }
        //        _logger.Info("Refresh token obtained.");

        //        //access token
        //        access_token = accesstokenCallResponse.AccessToken;
        //        _logger.Info("Access token obtained.");

        //        if (!_manager.Dictionary.ContainsKey("accessToken"))
        //        {
        //            _manager.Dictionary.Add("accessToken", access_token);
        //        }

        //        //Identity Token (returned only for OpenId scope)
        //        id_token = accesstokenCallResponse.IdentityToken;
        //        _logger.Info("Id token obtained.");

        //        //validate idToken
        //        isTokenValid = await _manager.IsIdTokenValid(id_token);
        //        _logger.Info("Validating Id Token. --->" + isTokenValid.ToString());

        //        _logger.Info("Calling UserInfo");

        //        UserInfoResponse userInfoResponse = await _manager.GetUserInfo(access_token, refresh_token);
        //        _logger.Info("userInfo httpStatusCode --->" + userInfoResponse.HttpStatusCode.ToString());
        //        if (userInfoResponse.HttpStatusCode == HttpStatusCode.OK)
        //        {
        //            //Read UserInfo Details
        //            IEnumerable<System.Security.Claims.Claim> userData = userInfoResponse.Json.ToClaims();

        //            foreach (System.Security.Claims.Claim item in userData)
        //            {
        //                if (item.Type == "sub" && item.Value != null)
        //                    userInfo.Sub = item.Value;
        //                if (item.Type == "email" && item.Value != null)
        //                    userInfo.Email = item.Value;
        //                if (item.Type == "emailVerified" && item.Value != null)
        //                    userInfo.EmailVerified = item.Value;
        //                if (item.Type == "givenName" && item.Value != null)
        //                    userInfo.GivenName = item.Value;
        //                if (item.Type == "familyName" && item.Value != null)
        //                    userInfo.FamilyName = item.Value;
        //                if (item.Type == "phoneNumber" && item.Value != null)
        //                    userInfo.PhoneNumber = item.Value;
        //                if (item.Type == "phoneNumberVerified" && item.Value != null)
        //                    userInfo.PhoneNumberVerified = item.Value;

        //                if (item.Type == "address" && item.Value != null)
        //                {
        //                    Address jsonObject = JsonConvert.DeserializeObject<Address>(item.Value);

        //                    if (jsonObject.StreetAddress != null)
        //                        userInfo.StreetAddress = jsonObject.StreetAddress;
        //                    if (jsonObject.Locality != null)
        //                        userInfo.Locality = jsonObject.Locality;
        //                    if (jsonObject.Region != null)
        //                        userInfo.Region = jsonObject.Region;
        //                    if (jsonObject.PostalCode != null)
        //                        userInfo.PostalCode = jsonObject.PostalCode;
        //                    if (jsonObject.Country != null)
        //                        userInfo.Country = jsonObject.Country;
        //                }
        //            }
        //            _logger.Info("User information received and saved.");
        //        }
        //    }
        //    else if (accesstokenCallResponse.HttpStatusCode == HttpStatusCode.Unauthorized && Session["RefreshToken"] != null)
        //    {
        //        //Validate if refresh token was already saved in session and use that to regenerate the access token.

        //        _logger.Info("Exchanging refresh token for access token.");
        //        //Handle exception 401 and then make this call
        //        // Call RefreshToken endpoint to get new access token when you recieve a 401 Status code
        //        TokenResponse refereshtokenCallResponse = await _manager.RefreshToken(refresh_token);
        //        if (accesstokenCallResponse.HttpStatusCode == HttpStatusCode.OK)
        //        {
        //            //save the refresh token in persistent store so that it can be used to refresh short lived access tokens
        //            refresh_token = accesstokenCallResponse.RefreshToken;
        //            if (!_manager.Dictionary.ContainsKey("refreshToken"))
        //            {
        //                _manager.Dictionary.Add("refreshToken", refresh_token);
        //            }
        //            else
        //            {
        //                _manager.Dictionary["refreshToken"] = refresh_token;
        //            }

        //            _logger.Info("Refresh token obtained.");

        //            access_token = accesstokenCallResponse.AccessToken;

        //            _logger.Info("Access token obtained.");
        //            if (_manager.Dictionary.ContainsKey("accessToken"))
        //            {
        //                _manager.Dictionary.Add("accessToken", access_token);
        //            }
        //            else
        //            {
        //                _manager.Dictionary["accessToken"] = access_token;
        //            }

        //            //Identity Token (returned only for OpenId scope)
        //            id_token = accesstokenCallResponse.IdentityToken;
        //            _logger.Info("Id token obtained.");

        //            //validate idToken
        //            isTokenValid = await _manager.IsIdTokenValid(id_token);
        //            _logger.Info("Validating Id Token. --->" + isTokenValid.ToString());

        //            _logger.Info("Calling UserInfo");
        //            UserInfoResponse userInfoResponse = await _manager.GetUserInfo(access_token, refresh_token);
        //            _logger.Info("userInfo httpStatusCode --->" + userInfoResponse.HttpStatusCode.ToString());
        //            if (userInfoResponse.HttpStatusCode == HttpStatusCode.OK)
        //            {
        //                //Read UserInfo Details
        //                IEnumerable<System.Security.Claims.Claim> userData = userInfoResponse.Json.ToClaims();

        //                foreach (System.Security.Claims.Claim item in userData)
        //                {
        //                    if (item.Type == "sub" && item.Value != null)
        //                        userInfo.Sub = item.Value;
        //                    if (item.Type == "email" && item.Value != null)
        //                        userInfo.Email = item.Value;
        //                    if (item.Type == "emailVerified" && item.Value != null)
        //                        userInfo.EmailVerified = item.Value;
        //                    if (item.Type == "givenName" && item.Value != null)
        //                        userInfo.GivenName = item.Value;
        //                    if (item.Type == "familyName" && item.Value != null)
        //                        userInfo.FamilyName = item.Value;
        //                    if (item.Type == "phoneNumber" && item.Value != null)
        //                        userInfo.PhoneNumber = item.Value;
        //                    if (item.Type == "phoneNumberVerified" && item.Value != null)
        //                        userInfo.PhoneNumberVerified = item.Value;

        //                    if (item.Type == "address" && item.Value != null)
        //                    {
        //                        Address jsonObject = JsonConvert.DeserializeObject<Address>(item.Value);

        //                        if (jsonObject.StreetAddress != null)
        //                            userInfo.StreetAddress = jsonObject.StreetAddress;
        //                        if (jsonObject.Locality != null)
        //                            userInfo.Locality = jsonObject.Locality;
        //                        if (jsonObject.Region != null)
        //                            userInfo.Region = jsonObject.Region;
        //                        if (jsonObject.PostalCode != null)
        //                            userInfo.PostalCode = jsonObject.PostalCode;
        //                        if (jsonObject.Country != null)
        //                            userInfo.Country = jsonObject.Country;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    //Redirect to pop-up window for C2QB flows
        //    if (Request.Url.Query == "")
        //    {
        //        //Response.Redirect(Request.RawUrl);
        //        Response.Redirect(Request.RawUrl);
        //    }
        //    else
        //    {
        //        Response.Redirect(Request.RawUrl.Replace(Request.Url.Query, ""));
        //    }
        //}
    }
}