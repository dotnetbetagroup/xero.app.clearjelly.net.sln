﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  CodeFile="~/Account/ResetPassword.aspx.cs" Inherits="ClearJelly.Web.Account.ResetPassword" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <title>Reset Password</title>
    <div>
        &nbsp;&nbsp;
      
        <br />
        <br />
        <br />
        <div>
            <asp:Label runat="server" ID="lbl_expired" Text="The requested link is expired." Visible="False"></asp:Label>
            <asp:Label ID="lbl_msg" runat="server"></asp:Label>
            <asp:Panel ID="Panel_reset_pwd" runat="server" Visible="false">
                <table class="tblResetPwd">
                    <tr>
                        <td class="width-180px">Enter Your New Password:</td>s
                        <td>
                            <asp:TextBox ID="txt_pwd" runat="server" TextMode="Password"></asp:TextBox>
                              <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_pwd"
                                        CssClass="text-danger" ErrorMessage="Password is required." Display="None" />
                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_pwd"
    ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
    ErrorMessage="Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />

                         <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txt_pwd" ErrorMessage="*required" CssClass="text-danger"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="txt_pwd" Display="Dynamic"
                               ValidationExpression="^[a-zA-Z0-9'@&#.\s]{6,}$" ErrorMessage="Password must contain minimum 6 characters." CssClass="text-danger" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>Retype Password</td>
                        <td>
                            <asp:TextBox ID="txt_retype_pwd" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                ControlToValidate="txt_retype_pwd" ErrorMessage="*required" CssClass="text-danger"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server"
                                ControlToCompare="txt_pwd" ControlToValidate="txt_retype_pwd" Display="Dynamic"></asp:CompareValidator>
                            <%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Account_reset" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="MainContent">
    <title>Reset Password</title>
    <div>
        &nbsp;&nbsp;
      
        <br />
        <br />
        <br />
        <div>
            <asp:Label runat="server" ID="Label1" Text="The requested link is expired." Visible="False"></asp:Label>
            <asp:Label ID="Label2" runat="server"></asp:Label>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <table class="tblResetPwd">
                    <tr>
                        <td class="width-180px">Enter Your New Password:</td>
                        <td>
                            <asp:TextBox ID="TextBox1" runat="server" TextMode="Password"></asp:TextBox>
                              <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_pwd"
                                        CssClass="text-danger" ErrorMessage="Password is required." Display="None" />
                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_pwd"
    ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
    ErrorMessage="Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />

                         <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txt_pwd" ErrorMessage="*required" CssClass="text-danger"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="txt_pwd" Display="Dynamic"
                               ValidationExpression="^[a-zA-Z0-9'@&#.\s]{6,}$" ErrorMessage="Password must contain minimum 6 characters." CssClass="text-danger" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>Retype Password</td>
                        <td>
                            <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txt_retype_pwd" ErrorMessage="*required" CssClass="text-danger"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server"
                                ControlToCompare="txt_pwd" ControlToValidate="txt_retype_pwd" Display="Dynamic"
                                ErrorMessage="Password and confirm password do not match." CssClass="text-danger"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="width-180px">&nbsp;</td>
                        <td>
                            <asp:Button ID="Button1" CssClass="margin-top-8px" runat="server" OnClick="btn_change_pwd_Click"
                                Text="Change Password" />
                        </td>
                    </tr>
                    <tr>
                        <td class="width-180px"></td>
                        <td>
                            <%--<asp:Label ID="lbl_msg" runat="server"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
    ErrorMessage="Password and confirm password do not match." CssClass="text-danger"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="width-180px">&nbsp;</td>
                        <td>
                            <asp:Button ID="btn_change_pwd" CssClass="margin-top-8px" runat="server" OnClick="btn_change_pwd_Click"
                                Text="Change Password" />
                        </td>
                    </tr>
                    <tr>
                        <td class="width-180px"></td>
                        <td>
                            <%--<asp:Label ID="lbl_msg" runat="server"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
