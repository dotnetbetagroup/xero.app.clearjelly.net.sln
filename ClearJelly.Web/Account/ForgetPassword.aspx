﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" Inherits="ClearJelly.Web.Account.ForgetPassword" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
        <title>Forget Password</title>
            <div>
                &nbsp;&nbsp;
      
        <br />
                <br />
                <br />


                <table class="tblForgetPwd">
                    <tr>
                        <td>Enter registered email address: </td>
                        <td>
                            <asp:TextBox ID="txt_email" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="txt_email" ErrorMessage="*required" CssClass="text-danger"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txt_email" ErrorMessage="Enter valid email."
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                  CssClass="text-danger"></asp:RegularExpressionValidator></td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="btn_send" CssClass="margin-top-10px" runat="server" OnClick="btn_send_Click" Text="Send" />


                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
    <div>
        <asp:Label ID="lbl_msg" runat="server">
        Note: An email with a link to reset your password will be sent to your registered email address.</asp:Label>
    </div>
</asp:Content>
