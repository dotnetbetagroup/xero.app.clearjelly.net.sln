﻿using System;

namespace ClearJelly.Web.Account
{
    public partial class ActivateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["success"] == "true")
            {
                lbl_success.Visible = true;
            }
            else if ((Request.QueryString["success"] == "false"))
            {
                lbl_error.Visible = true;
            }
            if (Request.QueryString["activationLinkFail"] == "true")
            {
                emailCheck.Visible = false;
                lbl_errorFail.Visible = true;
            }
        }
    }
}