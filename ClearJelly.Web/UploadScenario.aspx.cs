﻿using System;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using EntityModel;
using System.Linq;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Globalization;
using ClearJelly.ViewModels.UserMappingModels;
using ClearJelly.Entities;

namespace ClearJelly.Web
{
    public partial class UploadScenario : System.Web.UI.Page
    {

        OleDbConnection Econ;
        SqlConnection con;
        private static NLog.Logger _logger;


        protected void Page_Load(object sender, EventArgs e)
        {
            string uName = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (String.IsNullOrEmpty(uName))
            {
                Response.Redirect("~/Account/Login.aspx", false);
            }
        }

        public UploadScenario()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }


        private string GetConnectionString()
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    var sqlconn = ConfigurationManager.ConnectionStrings["CompanyDBConnection"].ConnectionString;
                    if (HttpContext.Current.Session["Jelly_user"] != null)
                    {
                        var companyName = Common.Common.GetCompanyNameByUserId((HttpContext.Current.Session["Jelly_user"].ToString()));
                        sqlconn += "database=Xero_" + companyName + ";";
                    }
                    return sqlconn;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario GetConnectionString method completed.", ex);
                throw;
            }
        }



        //private CheckHeaderAndContent InsertExcelRecords(string FilePath)
        //{
        //    try
        //    {
        //        var uploadedData = ImportingDataFromExcel(FilePath);
        //        if (uploadedData.IsFileHeaderValid)
        //        {
        //            MakeTable(uploadedData.lstUploadCustomDataViewModel);
        //        }
        //        return uploadedData;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario InsertExcelRecords method completed.", ex);
        //        throw;
        //    }
        //}

        protected void writeUploadToDB(DataTable entries)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario writeJournalsToDB method started.");

                if (CheckValidDatabase())
                {
                    CheckTabelExists();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(GetConnectionString()))
                    {
                        BulkInsertUploadCustom(bulkCopy, entries);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario writeJournalsToDB:" + ex.Message, ex);
                throw;
            }
        }


        public void BulkInsertUploadCustom(SqlBulkCopy bulkCopy, DataTable entries)
        {

            _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " Execution of UploadScenario BulkInsertUploadCustom started");
            bulkCopy.DestinationTableName =
                           "UploadScenario";

            bulkCopy.ColumnMappings.Add("EntityName", "EntityName");
            bulkCopy.ColumnMappings.Add("ClearJellyAccountCode", "ClearJellyAccountCode");
            bulkCopy.ColumnMappings.Add("SourceAccountCode", "SourceAccountCode");
            bulkCopy.ColumnMappings.Add("SourceAccountName", "SourceAccountName");
            bulkCopy.ColumnMappings.Add("Scenario", "Scenario");
            bulkCopy.ColumnMappings.Add("Date", "Date");
            bulkCopy.ColumnMappings.Add("Comment", "Comment");
            bulkCopy.ColumnMappings.Add("Value", "Value");
            bulkCopy.ColumnMappings.Add("TrackingCode1", "TrackingCode1");
            bulkCopy.ColumnMappings.Add("TrackingCode2", "TrackingCode2");

            try
            {
                bulkCopy.WriteToServer(entries);
            }
            catch (SqlException ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " Execution of UploadScenario relational table:" + ex.Message, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario relational table:" + ex.Message, ex);
                throw;
            }
        }



        public bool CheckValidDatabase()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckValidDatabase method started.");
                var companyName = Common.Common.GetCompanyNameByUserId((HttpContext.Current.Session["Jelly_user"].ToString()));
                var finalCompanyName = "Xero_" + companyName;
                var connection = new SqlConnectionStringBuilder(GetConnectionString());
                string dbName = connection.InitialCatalog;
                if (finalCompanyName.ToLower() == dbName.ToLower())
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckValidDatabase method completed.", ex);
                throw;
            }
        }


        public void MakeTable(List<UploadCustomDataViewModel> uplaodCustomData)
        {

            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario MakeTable method started.");
                DataTable UploadCustomData = new DataTable("UploadCustomData");
                // Add three column objects to the table. 
                DataColumn EntityName = new DataColumn();
                EntityName.DataType = System.Type.GetType("System.String");
                EntityName.ColumnName = "EntityName";
                UploadCustomData.Columns.Add(EntityName);

                DataColumn ClearJellyAccountCode = new DataColumn();
                ClearJellyAccountCode.DataType = System.Type.GetType("System.String");
                ClearJellyAccountCode.ColumnName = "ClearJellyAccountCode";
                UploadCustomData.Columns.Add(ClearJellyAccountCode);

                DataColumn SourceAccountCode = new DataColumn();
                SourceAccountCode.DataType = System.Type.GetType("System.String");
                SourceAccountCode.ColumnName = "SourceAccountCode";
                UploadCustomData.Columns.Add(SourceAccountCode);

                DataColumn SourceAccountName = new DataColumn();
                SourceAccountName.DataType = System.Type.GetType("System.String");
                SourceAccountName.ColumnName = "SourceAccountName";
                UploadCustomData.Columns.Add(SourceAccountName);

                DataColumn Scenario = new DataColumn();
                Scenario.DataType = System.Type.GetType("System.String");
                Scenario.ColumnName = "Scenario";
                UploadCustomData.Columns.Add(Scenario);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.DateTime");
                Date.ColumnName = "Date";
                UploadCustomData.Columns.Add(Date);

                DataColumn Comment = new DataColumn();
                Comment.DataType = System.Type.GetType("System.String");
                Comment.ColumnName = "Comment";
                UploadCustomData.Columns.Add(Comment);

                DataColumn Value = new DataColumn();
                Value.DataType = System.Type.GetType("System.String");
                Value.ColumnName = "Value";
                UploadCustomData.Columns.Add(Value);

                DataColumn TrackingCode1 = new DataColumn();
                TrackingCode1.DataType = System.Type.GetType("System.String");
                TrackingCode1.ColumnName = "TrackingCode1";
                UploadCustomData.Columns.Add(TrackingCode1);

                DataColumn TrackingCode2 = new DataColumn();
                TrackingCode2.DataType = System.Type.GetType("System.String");
                TrackingCode2.ColumnName = "TrackingCode2";
                UploadCustomData.Columns.Add(TrackingCode2);
                // Create an array for DataColumn objects.
                DataColumn[] keys = new DataColumn[1];

                // Add some new rows to the collection. 


                foreach (var item in uplaodCustomData)
                {
                    DataRow row = UploadCustomData.NewRow();
                    row["EntityName"] = item.EntityName;
                    row["ClearJellyAccountCode"] = item.ClearJellyAccountCode;
                    row["SourceAccountCode"] = item.SourceAccountCode;
                    row["SourceAccountName"] = item.SourceAccountName;
                    row["Scenario"] = item.Scenario;
                    row["Date"] = item.Date == null ? (object)DBNull.Value : item.Date;
                    row["Comment"] = item.Comment;
                    row["Value"] = item.Value;
                    row["TrackingCode1"] = item.TrackingCode1;
                    row["TrackingCode2"] = item.TrackingCode2;
                    UploadCustomData.Rows.Add(row);
                }

                writeUploadToDB(UploadCustomData);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario MakeTable method completed.", ex);
                throw;
            }
        }


        //public CheckHeaderAndContent ImportingDataFromExcel(string fileName)
        //{
        //    try
        //    {
        //        _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario ImportingDataFromExcel method started.");
        //        int count = 0; bool properFormat = false;
        //        bool IsRecordSkip = false;
        //        int skipRecordCnt = 0;
        //        List<UploadCustomDataViewModel> singlerow = new List<UploadCustomDataViewModel>();
        //        if (System.IO.File.Exists(fileName))
        //        {
        //            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
        //            {

        //                var dbContext = new ClearJellyEntities();
        //                string sheetName, fileExtension = Path.GetExtension(fs.Name).ToLower();
        //                int InsertedRecords = 0;

        //                if (fileExtension.Equals(".xlsx"))
        //                {
        //                    XSSFWorkbook wb = new XSSFWorkbook(fs);
        //                    XSSFSheet sh; XSSFRow row;
        //                    properFormat = CheckColumnHeaderCount(wb);
        //                    if (properFormat)
        //                    {
        //                        for (int i = 0; i < 1; i++)
        //                        {
        //                            sheetName = wb.GetSheetAt(i).SheetName.ToLower();
        //                            sh = (XSSFSheet)wb.GetSheet(sheetName);
        //                            var currentWBRecords = sh.LastRowNum;
        //                            for (int j = 1; j <= currentWBRecords; j++)
        //                            {
        //                                row = (XSSFRow)sh.GetRow(j);
        //                                if (CheckRowIsNull(row))
        //                                {

        //                                    ICell Date = row.Cells.Find(x => x.ColumnIndex == 5);
        //                                    ICell Value = row.Cells.Find(x => x.ColumnIndex == 7);
        //                                    singlerow.Add(ConvertToViewModel(row, Date, Value));
        //                                }

        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    HSSFWorkbook wb = new HSSFWorkbook(fs);
        //                    properFormat = CheckColumnHeaderCount(wb);
        //                    if (properFormat)
        //                    {
        //                        HSSFSheet sh; HSSFRow row;

        //                        for (int i = 0; i < wb.Count; i++)
        //                        {
        //                            sheetName = wb.GetSheetAt(i).SheetName.ToLower();
        //                            sh = (HSSFSheet)wb.GetSheet(sheetName);

        //                            for (int j = 1; j <= sh.LastRowNum; j++)
        //                            {
        //                                row = (HSSFRow)sh.GetRow(j);
        //                                if (CheckRowIsNull(row)) ;
        //                                {
        //                                    ICell Date = row.Cells.Find(x => x.ColumnIndex == 5);
        //                                    ICell Value = row.Cells.Find(x => x.ColumnIndex == 7);
        //                                    singlerow.Add(ConvertToViewModel(row, Date, Value));
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        return new CheckHeaderAndContent
        //        {
        //            lstUploadCustomDataViewModel = singlerow,
        //            IsFileHeaderValid = properFormat
        //        };
        //        //return singlerow;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario ImportingDataFromExcel method started.", ex);
        //        throw;
        //    }
        //}


        private bool CheckRowIsNull(HSSFRow row)
        {
            try
            {
                var EntityName = row.Cells.Find(x => x.ColumnIndex == 0);
                var ClearJellyAccountCode = row.Cells.Find(x => x.ColumnIndex == 1);
                var SourceAccountCode = row.Cells.Find(x => x.ColumnIndex == 2);
                var SourceAccountName = row.Cells.Find(x => x.ColumnIndex == 3);
                var Scenario = row.Cells.Find(x => x.ColumnIndex == 4);
                var Date = row.Cells.Find(x => x.ColumnIndex == 5).ToString();
                var Comment = row.Cells.Find(x => x.ColumnIndex == 6);
                var Value = row.Cells.Find(x => x.ColumnIndex == 7);

                if (EntityName == null && ClearJellyAccountCode == null && SourceAccountCode == null && SourceAccountName == null && Scenario == null && Date == "" && Comment == null && Value == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckRowIsNull method started.", ex);
                throw;
            }
        }

        private bool CheckRowIsNull(XSSFRow row)
        {
            try
            {
                var EntityName = row.Cells.Find(x => x.ColumnIndex == 0);
                var ClearJellyAccountCode = row.Cells.Find(x => x.ColumnIndex == 1);
                var SourceAccountCode = row.Cells.Find(x => x.ColumnIndex == 2);
                var SourceAccountName = row.Cells.Find(x => x.ColumnIndex == 3);
                var Scenario = row.Cells.Find(x => x.ColumnIndex == 4);
                var Date = row.Cells.Find(x => x.ColumnIndex == 5) != null ? row.Cells.Find(x => x.ColumnIndex == 5).ToString() : null;
                var Comment = row.Cells.Find(x => x.ColumnIndex == 6);
                var Value = row.Cells.Find(x => x.ColumnIndex == 7);

                if (EntityName == null && ClearJellyAccountCode == null && SourceAccountCode == null && SourceAccountName == null && Scenario == null && Date == "" && Comment == null && Value == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckRowIsNull method completed.", ex);
                throw;
            }
        }





        public bool CheckColumnHeaderCount(XSSFWorkbook wb)
        {
            try
            {

                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckColumnHeaderCount method started.");
                XSSFSheet sh; string sheetName;
                bool isExcelValid = true;
                for (int sheetCnt = 0; sheetCnt < wb.Count; sheetCnt++)
                {
                    sheetName = wb.GetSheetAt(sheetCnt).SheetName.ToLower();

                    sh = (XSSFSheet)wb.GetSheet(sheetName);
                    if (sh.GetRow(0) != null)
                    {
                        short columnCount = sh.GetRow(0).LastCellNum;

                        string cellValue = string.Empty;
                        bool isJobNoExist = false;
                        if (sh.GetRow(0).GetCell(0) == null)
                        {
                            isExcelValid = false;
                            return false;
                        }

                        var JobCellValue = sh.GetRow(0).GetCell(0).ToString().Trim().ToLower();
                        var jobNumberHeader = GetEnumDescription((ExcelHeaderNames)0).ToLower();
                        int FixedColumnCnt = Enum.GetNames(typeof(ExcelHeaderNames)).Length;
                        if (JobCellValue == jobNumberHeader)
                        {
                            isJobNoExist = true;
                        }
                        else { FixedColumnCnt = FixedColumnCnt - 1; }

                        if (Convert.ToInt32(columnCount) < 8)
                        {
                            isExcelValid = false;
                            return isExcelValid;
                        }
                        for (int row = 0; row <= FixedColumnCnt - 1; row++)
                        {
                            //Get the cell value
                            cellValue = sh.GetRow(0).GetCell(row).ToString().Trim().ToLower();
                            if (isJobNoExist)
                            {
                                if (!(GetEnumDescription((ExcelHeaderNames)row).ToLower() == cellValue))
                                {
                                    isExcelValid = false;
                                    return false;
                                }
                            }
                            else
                            {
                                if (!(GetEnumDescription((ExcelHeaderNames)row + 1).ToLower() == cellValue))
                                {
                                    isExcelValid = false;
                                    return false;
                                }
                            }
                        }
                    }
                }
                return isExcelValid;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckColumnHeaderCount method completed.", ex);
                throw ex;
            }
        }

        public bool CheckColumnHeaderCount(HSSFWorkbook wb)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckColumnHeaderCount method started.");

                HSSFSheet sh; string sheetName;
                bool isExcelValid = true;
                for (int sheetCnt = 0; sheetCnt < wb.Count; sheetCnt++)
                {
                    sheetName = wb.GetSheetAt(sheetCnt).SheetName.ToLower();

                    sh = (HSSFSheet)wb.GetSheet(sheetName);
                    if (sh.GetRow(0) != null)
                    {
                        short columnCount = sh.GetRow(0).LastCellNum;

                        string cellValue = string.Empty;
                        bool isJobNoExist = false;
                        var JobCellValue = sh.GetRow(0).GetCell(0).ToString().Trim().ToLower();
                        var jobNumberHeader = GetEnumDescription((ExcelHeaderNames)0).ToLower();
                        int FixedColumnCnt = Enum.GetNames(typeof(ExcelHeaderNames)).Length;
                        if (JobCellValue == jobNumberHeader)
                        {
                            isJobNoExist = true;
                        }
                        else { FixedColumnCnt = FixedColumnCnt - 1; }

                        if (Convert.ToInt32(columnCount) < 52)
                        {
                            isExcelValid = false;
                            return isExcelValid;
                        }
                        for (int row = 0; row <= FixedColumnCnt - 1; row++)
                        {
                            //Get the cell value
                            cellValue = sh.GetRow(0).GetCell(row).ToString().Trim().ToLower();
                            if (isJobNoExist)
                            {
                                if (!(GetEnumDescription((ExcelHeaderNames)row).ToLower() == cellValue))
                                {
                                    isExcelValid = false;
                                    return false;
                                }
                            }
                            else
                            {
                                if (!(GetEnumDescription((ExcelHeaderNames)row + 1).ToLower() == cellValue))
                                {
                                    isExcelValid = false;
                                    return false;
                                }
                            }

                        }
                    }
                }
                return isExcelValid;
            }
            catch (Exception ex)
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario CheckColumnHeaderCount method completed.", ex);
                throw;
            }
        }

        public string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public UploadCustomDataViewModel ConvertToViewModel(XSSFRow row, ICell Date, ICell Value)
        {
            try
            {
                var UploadCustomDataViewModel = new UploadCustomDataViewModel();
                UploadCustomDataViewModel.EntityName = GetSubstringedString(row, 0) == "" ? null : GetSubstringedString(row, 0);
                UploadCustomDataViewModel.ClearJellyAccountCode = GetSubstringedString(row, 1) == "" ? null : GetSubstringedString(row, 1);
                UploadCustomDataViewModel.SourceAccountCode = GetSubstringedString(row, 2) == "" ? null : GetSubstringedString(row, 2);
                UploadCustomDataViewModel.SourceAccountName = GetSubstringedString(row, 3) == "" ? null : GetSubstringedString(row, 3);
                UploadCustomDataViewModel.Scenario = GetSubstringedString(row, 4) == "" ? null : GetSubstringedString(row, 4);
                ICell cell = Date;
                if (cell != null)
                {
                    if (cell.CellType == CellType.Numeric)
                    {
                        UploadCustomDataViewModel.Date = cell.DateCellValue;
                    }
                    else if (cell.CellType == CellType.Blank)
                    {
                        UploadCustomDataViewModel.Date = null;
                    }
                }
                //if (Value != null)
                //{
                //    if (Value.CellType == CellType.Formula)
                //    {
                //        UploadCustomDataViewModel.Value = Value.NumericCellValue.ToString();
                //    }
                //    else if (Value.CellType == CellType.Blank)
                //    {
                //        UploadCustomDataViewModel.Value = null;
                //    }else
                //    {
                //        UploadCustomDataViewModel.Value = GetSubstringedString(row, 7) == "" ? null : GetSubstringedString(row, 7);
                //    }
                //}
                //UploadCustomDataViewModel.Date = GetSubstringedString(row, 6, numberToMinus);
                UploadCustomDataViewModel.Comment = GetSubstringedString(row, 6) == "" ? null : GetSubstringedString(row, 6);
                UploadCustomDataViewModel.Value = GetSubstringedString(row, 7) == "" ? null : GetSubstringedString(row, 7);
                UploadCustomDataViewModel.TrackingCode1 = GetSubstringedString(row, 8) == "" ? null : GetSubstringedString(row, 8);
                UploadCustomDataViewModel.TrackingCode2 = GetSubstringedString(row, 9) == "" ? null : GetSubstringedString(row, 9);
                return UploadCustomDataViewModel;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario ConvertToViewModel method completed.", ex);
                throw;
            }
        }


        public UploadCustomDataViewModel ConvertToViewModel(HSSFRow row, ICell Date, ICell Value)
        {
            try
            {
                var UploadCustomDataViewModel = new UploadCustomDataViewModel();
                UploadCustomDataViewModel.EntityName = GetSubstringedString(row, 0) == "" ? null : GetSubstringedString(row, 0);
                UploadCustomDataViewModel.ClearJellyAccountCode = GetSubstringedString(row, 1) == "" ? null : GetSubstringedString(row, 1);
                UploadCustomDataViewModel.SourceAccountCode = GetSubstringedString(row, 2) == "" ? null : GetSubstringedString(row, 2);
                UploadCustomDataViewModel.SourceAccountName = GetSubstringedString(row, 3) == "" ? null : GetSubstringedString(row, 3);
                UploadCustomDataViewModel.Scenario = GetSubstringedString(row, 4) == "" ? null : GetSubstringedString(row, 4);
                ICell cell = Date;
                if (cell.CellType == CellType.Numeric)
                {
                    UploadCustomDataViewModel.Date = cell.DateCellValue;
                }
                else if (cell.CellType == CellType.Blank)
                {
                    UploadCustomDataViewModel.Date = null;
                }

                //if (Value.CellType == CellType.Formula)
                //{
                //    UploadCustomDataViewModel.Value = Value.NumericCellValue.ToString();
                //}
                //else if (Value.CellType == CellType.Blank)
                //{
                //    UploadCustomDataViewModel.Value = null;
                //}
                //else
                //{
                //    UploadCustomDataViewModel.Value = GetSubstringedString(row, 7) == "" ? null : GetSubstringedString(row, 7);
                //}
                //UploadCustomDataViewModel.Date = GetSubstringedString(row, 6, numberToMinus);
                UploadCustomDataViewModel.Comment = GetSubstringedString(row, 6) == "" ? null : GetSubstringedString(row, 6);
                UploadCustomDataViewModel.Value = GetSubstringedString(row, 7) == "" ? null : GetSubstringedString(row, 7);
                UploadCustomDataViewModel.TrackingCode1 = GetSubstringedString(row, 8) == "" ? null : GetSubstringedString(row, 8);
                UploadCustomDataViewModel.TrackingCode2 = GetSubstringedString(row, 9) == "" ? null : GetSubstringedString(row, 9);
                return UploadCustomDataViewModel;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario ConvertToViewModel method completed.", ex);
                throw;
            }
        }


        public string GetSubstringedString(XSSFRow row, int rownum)
        {
            if (row != null)
            {
                ICell cellValue = row.Cells.Find(x => x.ColumnIndex == rownum);

                if (cellValue != null)
                {
                    if (cellValue.CellType == CellType.Formula)
                    {
                        try
                        {
                            return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).NumericCellValue);
                        }
                        catch
                        {
                            return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).StringCellValue);
                        }
                    }
                    else if (cellValue.CellType == CellType.Blank)
                    {
                        return string.Empty;
                    }
                }
                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum));
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetSubstringedString(HSSFRow row, int rownum)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario GetSubstringedString method started.");
                if (row != null)
                {
                    ICell cellValue = row.Cells.Find(x => x.ColumnIndex == rownum);

                    if (cellValue != null)
                    {
                        if (cellValue.CellType == CellType.Formula)
                        {
                            try
                            {
                                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).NumericCellValue);
                            }
                            catch
                            {
                                return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).StringCellValue);
                            }
                        }
                        else if (cellValue.CellType == CellType.Numeric)
                        {
                            return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum).DateCellValue);
                        }
                        else if (cellValue.CellType == CellType.Blank)
                        {
                            return string.Empty;
                        }

                    }
                    return Convert.ToString(row.Cells.Find(x => x.ColumnIndex == rownum));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario btnUploadClick method completed.", ex);
                throw;
            }

        }

        protected void btnUploadClick(object sender, EventArgs e)
        {
            try
            {

                //var timeticks = DateTime.Now.Ticks;
                //var fname = Path.Combine(Server.MapPath("Uploads"), Path.GetFileName(ExcelFileUpload.FileName));
                //ExcelFileUpload.SaveAs(fname);
                //var InsertStatus = InsertExcelRecords(fname);
                //if (!InsertStatus.IsFileHeaderValid)
                //{
                //    lblFailure.Text = "The File is not of proper format.";
                //    FailPanel.Visible = true;
                //    InfoPanel.Visible = false;
                //}
                //else
                //{
                //    FailPanel.Visible = false;
                //    InfoPanel.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario btnUploadClick method completed.", ex);
                FailPanel.Visible = true;
                InfoPanel.Visible = false;
            }
        }



        public void CheckTabelExists()
        {
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;

            try
            {
                string cmdText = @"IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES 
                       WHERE TABLE_NAME='UploadScenario') SELECT 1 ELSE SELECT 0";
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand DateCheck = new SqlCommand(cmdText, MyConnection);
                int x = Convert.ToInt32(DateCheck.ExecuteScalar());
                if (x != 1)
                {
                    SqlCommand cmd = new SqlCommand("CREATE TABLE[dbo].[UploadScenario]([EntityName][nvarchar](max) NULL, [ClearJellyAccountCode] [nvarchar](max) NULL, [SourceAccountCode] [nvarchar](max) NULL, [SourceAccountName] [nvarchar](max) NULL, [Scenario] [nvarchar](max) NULL,  [Date] [datetime] NULL, [Comment] [nvarchar](max) NULL, [Value] [nvarchar](max) NULL,  [TrackingCode1] [nvarchar](max) NULL,  [TrackingCode2] [nvarchar](max) NULL) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]", MyConnection);
                    cmd.ExecuteReader();

                }
            }
            catch (SqlException ex)
            {
                _logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message);
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        public enum ExcelHeaderNames
        {
            [Description("Entity Name")]
            EntityName = 0,
            [Description("Clear Jelly Account Code")]
            ClearJellyAccountCode = 1,
            [Description("Source Account Code")]
            SourceAccountCode = 2,
            [Description("Source Account Name")]
            SourceAccountName = 3,
            [Description("Scenario")]
            Scenario = 4,
            [Description("Date")]
            Date = 5,
            [Description("Comment")]
            Comment = 6,
            [Description("Value")]
            Value = 7,
            [Description("TrackingCode1")]
            TrackingCode1 = 8,
            [Description("TrackingCode2")]
            TrackingCode2 = 9,
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UploadCustomDataViewModel> GetCustomUploadList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario  GetCustomUploadList event started.");
                var currentPage = new UploadScenario();
                currentPage.CheckTabelExists();
                var customUpload = currentPage.GetDataFromDatabasetoDataTable();
                return customUpload;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UploadScenario  GetCustomUploadList event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static bool ClearAllData()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of UploadScenario  ClearAllData event started.");
                var currentPage = new UploadScenario();
                var isSuccess = currentPage.ClearAllRecordsFromUserDb();
                return isSuccess;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of UploadScenario  ClearAllData event completed.", ex);
                throw;
            }
        }

        public bool ClearAllRecordsFromUserDb()
        {
            _logger.Info(" ~Execution of ClearAllData ClearAllRecordsFromUserDb method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @"delete from [UploadScenario]";
            bool isSuccess = false;
            try
            {
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();
                isSuccess = true;

            }
            catch (SqlException ex)
            {
                _logger.Error(" ~An SQL error occurred during execution of UploadScenario ClearAllRecordsFromUserDb method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of UploadScenario ClearAllRecordsFromUserDb method. Error details:" + ex.Message, ex);
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return isSuccess;
        }


        public List<UploadCustomDataViewModel> GetDataFromDatabasetoDataTable()
        {

            _logger.Info(" ~Execution of UploadScenario GetDataFromDatabasetoDataTable method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Select * from [UploadScenario] ";
            List<UploadCustomDataViewModel> lstUploadCustom = new List<UploadCustomDataViewModel>();
            try
            {

                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();



                while (myReader.Read())
                {
                    UploadCustomDataViewModel uploadCustom = new UploadCustomDataViewModel();
                    uploadCustom.EntityName = myReader["EntityName"].ToString();
                    uploadCustom.ClearJellyAccountCode = myReader["ClearJellyAccountCode"].ToString();
                    uploadCustom.SourceAccountCode = myReader["SourceAccountCode"].ToString();
                    uploadCustom.SourceAccountName = myReader["SourceAccountName"].ToString();
                    uploadCustom.Scenario = myReader["Scenario"].ToString();
                    uploadCustom.DisplayDate = myReader["Date"].ToString() != string.Empty ? DateTime.Parse(myReader["Date"].ToString()).ToShortDateString() : string.Empty;
                    uploadCustom.Comment = myReader["Comment"].ToString();
                    uploadCustom.Value = myReader["Value"].ToString();
                    uploadCustom.TrackingCode1 = myReader["TrackingCode1"].ToString();
                    uploadCustom.TrackingCode2 = myReader["TrackingCode2"].ToString();
                    lstUploadCustom.Add(uploadCustom);

                }
                return lstUploadCustom;
            }
            catch (SqlException ex)
            {
                _logger.Error(" ~An SQL error occurred during execution of clear_table_modified_after method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of XeroToCJ clear_table_modified_after method. Error details:" + ex.Message, ex);
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }
    }
}