﻿using ClearJelly.Entities;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using ClearJelly.ViewModels;
using ClearJelly.XeroApp;

namespace ClearJelly.Web
{
    public partial class AccountMapping : System.Web.UI.Page
    {
        private static NLog.Logger _logger;

        protected void Page_Load(object sender, EventArgs e)
        {
            string uName = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (String.IsNullOrEmpty(uName))
            {
                Response.Redirect("~/Account/Login.aspx", false);
            }
            if (Request.QueryString["filenotvalid"] == "false")
            {
                lblFailure.Text = "The File is not of proper format.";
                FailPanel.Visible = true;
                InfoPanel.Visible = false;
            }

            if (Request.QueryString["isSuccess"] == "true")
            {
                FailPanel.Visible = false;
                InfoPanel.Visible = true;
            }

            if (Request.QueryString["isSuccess"] == "false")
            {
                FailPanel.Visible = true;
                InfoPanel.Visible = false;
            }
        }

        public AccountMapping()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        private string GetConnectionString()
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    var sqlconn = ConfigurationManager.ConnectionStrings["CompanyDBConnection"].ConnectionString;
                    if (HttpContext.Current.Session["Jelly_user"] != null)
                    {
                        var companyName = ClearJelly.Web.Common.Common.GetCompanyNameByUserId((HttpContext.Current.Session["Jelly_user"].ToString()));
                        sqlconn += "database=Xero_" + companyName + ";";
                    }
                    return sqlconn;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping GetConnectionString method completed.", ex);
                throw;
            }
        }

        private AccountMappingVm InsertExcelRecords(string FilePath)
        {
            try
            {
                var AccountMappingVm = GetRequestsDataFromExcel(FilePath);
                if (AccountMappingVm.IsHeaderValid)
                {
                    bool isSuccess = writeUploadToDB(AccountMappingVm.AccountDatatable);

                }
                return AccountMappingVm;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping InsertExcelRecords method completed.", ex);
                throw;
            }
        }

        protected bool writeUploadToDB(DataTable entries)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping writeJournalsToDB method started.");
                bool isSuccess = false;
                if (CheckValidDatabase())
                {
                    CheckTabelExists();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(GetConnectionString()))
                    {
                        isSuccess = BulkInsertUploadCustom(bulkCopy, entries);
                    }
                }
                return isSuccess;

            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping writeJournalsToDB:" + ex.Message, ex);
                throw;
            }
        }

        public bool BulkInsertUploadCustom(SqlBulkCopy bulkCopy, DataTable entries)
        {
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            var InsertQuery = string.Empty;
            bool isSuccess = false;
            try
            {
                for (int i = 0; entries.Rows.Count > i; i++)
                {
                    var entry1 = entries.Rows[i].ItemArray.GetValue(0).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(0).ToString();
                    var entry2 = entries.Rows[i].ItemArray.GetValue(1).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(1).ToString();
                    var entry3 = entries.Rows[i].ItemArray.GetValue(2).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(2).ToString();
                    var entry4 = entries.Rows[i].ItemArray.GetValue(3).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(3).ToString();
                    var entry5 = entries.Rows[i].ItemArray.GetValue(4).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(4).ToString();
                    var entry6 = entries.Rows[i].ItemArray.GetValue(5).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(5).ToString();
                    var entry7 = entries.Rows[i].ItemArray.GetValue(6).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(6).ToString();
                    var entry8 = entries.Rows[i].ItemArray.GetValue(7).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(7).ToString();
                    var entry9 = entries.Rows[i].ItemArray.GetValue(8).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(8).ToString();
                    var entry10 = entries.Rows[i].ItemArray.GetValue(9).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(9).ToString();
                    var entry11 = entries.Rows[i].ItemArray.GetValue(10).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(10).ToString();
                    var entry12 = entries.Rows[i].ItemArray.GetValue(11).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(11).ToString();
                    var entry13 = entries.Rows[i].ItemArray.GetValue(12).ToString() == string.Empty ? null : entries.Rows[i].ItemArray.GetValue(12).ToString();


                    InsertQuery = InsertQuery + "INSERT INTO AccountMappingTable " +
                       "(OrgName,Name,Code,Mapping1,Mapping2,Mapping3,Mapping4,Mapping5,Mapping6,Mapping7,Mapping8,Mapping9,Mapping10) " +
                       "VALUES ('" + entry1 + "','"
                       + entry2 + "','"
                       + entry3 + "','"
                       + entry4 + "','"
                       + entry5 + "','"
                       + entry6 + "','"
                       + entry7 + "','"
                       + entry8 + "','"
                       + entry9 + "','"
                       + entry10 + "','"
                        + entry11 + "','"
                         + entry12 + "','"
                       + entry13 + "')";
                }

                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand DateCheck = new SqlCommand(InsertQuery, MyConnection);
                DateCheck.ExecuteReader();
                isSuccess = true;
            }
            catch (SqlException ex)
            {
                _logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return isSuccess;
        }

        public bool CheckValidDatabase()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping CheckValidDatabase method started.");
                var companyName = ClearJelly.Web.Common.Common.GetCompanyNameByUserId((HttpContext.Current.Session["Jelly_user"].ToString()));
                var finalCompanyName = "Xero_" + companyName;
                var connection = new SqlConnectionStringBuilder(GetConnectionString());
                string dbName = connection.InitialCatalog;
                if (finalCompanyName.ToLower() == dbName.ToLower())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping CheckValidDatabase method completed.", ex);
                throw;
            }
        }

        public string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        protected void btnUploadClick(object sender, EventArgs e)
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(ExcelFileUpload.FileName);
            String extension = Path.GetExtension(ExcelFileUpload.FileName);
            var fname = Path.Combine(Server.MapPath("Uploads"), fileNameWithoutExtension + "_" + DateTime.Now.Ticks + extension);
            try
            {
                ExcelFileUpload.SaveAs(fname);
                AccountMappingVm accountmappingVm = InsertExcelRecords(fname);
                //Response.Redirect(Request.Url.AbsoluteUri);
                if (!accountmappingVm.IsHeaderValid)
                {
                    Response.Redirect("~/AccountMapping?filenotvalid=false", false);

                    //lblFailure.Text = "File is not of proper format";
                    //FailPanel.Visible = true;
                    //InfoPanel.Visible = false;
                }
                else if (accountmappingVm.IsSuccess)
                {
                    Response.Redirect("~/AccountMapping?isSuccess=true", false);
                    //FailPanel.Visible = false;
                    //InfoPanel.Visible = true;
                }
                else
                {
                    Response.Redirect("~/AccountMapping?isSuccess=false", false);
                    //FailPanel.Visible = true;
                    //InfoPanel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping btnUploadClick method completed.", ex);
                Response.Redirect("~/AccountMapping?isSuccess=false", false);
                FailPanel.Visible = true;
                InfoPanel.Visible = false;
            }
            finally
            {
                if (System.IO.File.Exists(fname))
                {
                    File.Delete(fname);
                }
            }
        }

        public void CheckTabelExists()
        {
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;

            try
            {
                string cmdText = @"IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES 
                       WHERE TABLE_NAME='AccountMappingTable') SELECT 1 ELSE SELECT 0";
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand DateCheck = new SqlCommand(cmdText, MyConnection);
                int x = Convert.ToInt32(DateCheck.ExecuteScalar());
                if (x != 1)
                {
                    SqlCommand cmd = new SqlCommand("CREATE TABLE[dbo].[AccountMappingTable]([OrgName][nvarchar](max) NULL,[Name][nvarchar](max) NULL,[Code][nvarchar](max) NULL, [Mapping1] [nvarchar](max) NULL, [Mapping2] [nvarchar](max) NULL, [Mapping3] [nvarchar](max) NULL, [Mapping4] [nvarchar](max) NULL,  [Mapping5] [nvarchar](max) NULL, [Mapping6] [nvarchar](max) NULL, [Mapping7] [nvarchar](max) NULL, [Mapping8] [nvarchar](max) NULL, [Mapping9] [nvarchar](max) NULL, [Mapping10] [nvarchar](max) NULL) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]", MyConnection);
                    cmd.ExecuteReader();

                }
            }
            catch (SqlException ex)
            {
                _logger.Fatal(HttpContext.Current.Session["Jelly_user"] ?? "" + " ~An SQL error occurred during execution of clear_table_section method. Error details:" + ex.Message);
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<AccountMappingViewModel> GetCustomUploadList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping  GetCustomUploadList event started.");
                var currentPage = new AccountMapping();
                //currentPage.CheckTabelExists();
                var customUpload = currentPage.GetDataFromDatabasetoDataTable();
                return customUpload;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of AccountMapping  GetCustomUploadList event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static bool ClearAllData()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of AccountMapping  GetCustomUploadList event started.");
                var currentPage = new AccountMapping();
                var isSuccess = currentPage.ClearAllRecordsFromUserDb();
                return isSuccess;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of AccountMapping  GetCustomUploadList event completed.", ex);
                throw;
            }
        }

        public bool ClearAllRecordsFromUserDb()
        {
            _logger.Info(" ~Execution of AccountMapping ClearAllRecordsFromUserDb method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @"delete from [AccountMappingTable]";
            bool isSuccess = false;
            try
            {
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();
                isSuccess = true;

            }
            catch (SqlException ex)
            {
                _logger.Error(" ~An SQL error occurred during execution of ClearAllRecordsFromUserDb method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of AccountMapping ClearAllRecordsFromUserDb method. Error details:" + ex.Message, ex);
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
            return isSuccess;
        }

        public List<AccountMappingViewModel> GetDataFromDatabasetoDataTable()
        {

            _logger.Info(" ~Execution of AccountMapping GetDataFromDatabasetoDataTable method started.");
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Select * from [AccountMappingTable]";
            List<AccountMappingViewModel> lstUploadCustom = new List<AccountMappingViewModel>();
            try
            {
                MyConnection = new SqlConnection(GetConnectionString());
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    AccountMappingViewModel uploadCustom = new AccountMappingViewModel();
                    uploadCustom.OrgName = myReader["OrgName"].ToString();
                    uploadCustom.Name = myReader["Name"].ToString();
                    uploadCustom.Code = myReader["Code"].ToString();
                    uploadCustom.Mapping1 = myReader["Mapping1"].ToString();
                    uploadCustom.Mapping2 = myReader["Mapping2"].ToString();
                    uploadCustom.Mapping3 = myReader["Mapping3"].ToString();
                    uploadCustom.Mapping4 = myReader["Mapping4"].ToString();
                    uploadCustom.Mapping5 = myReader["Mapping5"].ToString();
                    uploadCustom.Mapping6 = myReader["Mapping6"].ToString();
                    uploadCustom.Mapping7 = myReader["Mapping7"].ToString();
                    uploadCustom.Mapping8 = myReader["Mapping8"].ToString();
                    uploadCustom.Mapping9 = myReader["Mapping9"].ToString();
                    uploadCustom.Mapping10 = myReader["Mapping10"].ToString();
                    lstUploadCustom.Add(uploadCustom);
                }
                return lstUploadCustom;
            }
            catch (SqlException ex)
            {
                _logger.Error(" ~An SQL error occurred during execution of AccountMapping  GetDataFromDatabasetoDataTable method. Error details:" + ex.Message + " sql:" + sqlStr, ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of AccountMapping GetDataFromDatabasetoDataTable method. Error details:" + ex.Message, ex);
                throw;
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        private static ISheet GetFileStream(string fullFilePath)
        {
            var fileExtension = Path.GetExtension(fullFilePath);
            string sheetName;
            ISheet sheet = null;
            switch (fileExtension)
            {
                case ".xlsx":
                    using (var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var wb = new XSSFWorkbook(fs);
                        sheetName = wb.GetSheetAt(0).SheetName;
                        sheet = (XSSFSheet)wb.GetSheet(sheetName);
                    }
                    break;
                case ".xls":
                    using (var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var wb = new HSSFWorkbook(fs);
                        sheetName = wb.GetSheetAt(0).SheetName;
                        sheet = (HSSFSheet)wb.GetSheet(sheetName);
                    }
                    break;
            }
            return sheet;
        }

        private static AccountMappingVm GetRequestsDataFromExcel(string fullFilePath)
        {
            AccountMappingVm accountvm = new AccountMappingVm();
            try
            {
                _logger.Info(" ~Execution of AccountMapping GetRequestsDataFromExcel method started:");
                var sh = GetFileStream(fullFilePath);
                var dtExcelTable = new DataTable();
                dtExcelTable.Rows.Clear();
                dtExcelTable.Columns.Clear();
                var headerRow = sh.GetRow(0);
                int colCount = headerRow.LastCellNum;

                accountvm.IsHeaderValid = true;
                if (colCount > 13 || colCount < 13)
                {
                    accountvm.IsHeaderValid = false;
                    return accountvm;
                }

                for (var c = 0; c < colCount; c++)
                    dtExcelTable.Columns.Add(Enum.GetName(typeof(EnumAccountMapping), c));
                var i = 1;
                var currentRow = sh.GetRow(i);

                while (currentRow != null)
                {
                    var dr = dtExcelTable.NewRow();
                    for (var j = 0; j <= headerRow.Cells.Count; j++)
                    {
                        var cell = currentRow.GetCell(j);

                        if (cell != null)
                            switch (cell.CellType)
                            {
                                case CellType.Numeric:
                                    dr[j] = DateUtil.IsCellDateFormatted(cell)
                                        ? cell.DateCellValue.ToShortDateString()
                                        : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                    break;
                                case CellType.String:
                                    dr[j] = cell.StringCellValue;
                                    break;
                                case CellType.Blank:
                                    dr[j] = string.Empty;
                                    break;

                                case CellType.Formula:
                                    try
                                    {
                                        dr[j] = cell.NumericCellValue;
                                    }
                                    catch
                                    {
                                        dr[j] = cell.StringCellValue;
                                    }
                                    break;
                            }
                    }
                    dtExcelTable.Rows.Add(dr);
                    i++;
                    currentRow = sh.GetRow(i);
                }
                accountvm.AccountDatatable = dtExcelTable;
                accountvm.IsSuccess = true;
                return accountvm;
            }
            catch (Exception ex)
            {

                _logger.Error(" ~Execution of AccountMapping GetRequestsDataFromExcel method completed", ex);
                throw;
            }
        }
    }

    public class AccountMappingVm
    {
        public DataTable AccountDatatable { get; set; }
        public bool IsHeaderValid { get; set; }
        public bool IsSuccess { get; set; }
    }
}