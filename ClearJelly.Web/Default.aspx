﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ClearJelly.Web.Default" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        
        <table style="width: 300px">
          <tr>
            <td style="width: 300px">
              <asp:DropDownList runat="server" ID="CompanyListDropDown" AutoPostBack="True" OnSelectedIndexChanged="CompanyListDropDown_SelectedIndexChanged"></asp:DropDownList>
            </td>
          </tr>  
        </table>
        <br />
        <br />

        <table style="width: 50%">
              <tr>
                <td style="width: 300px">
                    <asp:Label ID="lblSD" runat="server" Width="220" Text="Start Date:" Visible="false" />
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <asp:TextBox ID="txtSD" runat="server" Width="220" Text="" Visible="false"/>
                     
                </td>
            </tr>
        </table>

        <asp:Button runat="server" ID="btnAuto" Width="220" Text="Create" OnClick="cmdAutomatic" />
        <br />
        <br />
        <asp:Label runat="server" ForeColor    ="#336699" Width="600px">
        Important:Performing this action will delete and recreate the Clear Jelly model for the specified organisation.
        All data and changes applied to Clear Jelly model prior will be lost.
        <br />
        </asp:Label>

        <asp:Label ID="invoiceCreatedLabel" runat="server" />
    </div>
    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/Default.js">
    </script>
</asp:Content>

