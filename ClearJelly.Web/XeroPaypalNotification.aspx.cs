﻿using ClearJelly.Entities;
using ClearJelly.ViewModels.PayPalViewModels;
using ClearJelly.Web.Common;
using ClearJelly.XeroApp;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using static PaypalIntegration.PayPalEnums;

namespace ClearJelly.Web
{
    public partial class XeroPaypalNotification : System.Web.UI.Page
    {
        private static NLog.Logger _logger;
        private static string _strConn2;
        private readonly ClearJellyEntities _clearJellyContext;
        private static string SaasuURL;
        public XeroPaypalNotification()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _clearJellyContext = new ClearJellyEntities();
            SaasuURL = Common.Common.GetSaasuURL();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _logger.Info(" ~Execution of PaypalNotification pageload event started.");
            ProcessPayPalIPNNotification();
        }

        private void ProcessPayPalIPNNotification()
        {
            try
            {
                _logger.Info(" ~Execution of paypalnotification ProcessPayPalIPNNotification event started.");
                byte[] param = Request.BinaryRead(Request.ContentLength);
                string strRequest = Encoding.ASCII.GetString(param);
                string ipnPost = strRequest;
                strRequest += "&cmd=_notify-validate";

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["paypalNotifyUrl"]);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = strRequest.Length;

                StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
                streamOut.Write(strRequest);
                streamOut.Close();
                StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
                string strResponse = streamIn.ReadToEnd();
                string logPathDir = Path.Combine("~/Messages/");
                var path = System.Web.HttpContext.Current.Server.MapPath(logPathDir);
                string logPath = string.Format("{0}\\{1}.txt", path, DateTime.Now.Ticks);
                File.WriteAllText(logPath, ipnPost);
                streamIn.Close();

                if (strResponse.Equals("VERIFIED"))
                {
                    _logger.Info(" ~Execution of paypalnotification verified event started.");
                    var resend = Request.Form["resend"];
                    if (!string.IsNullOrEmpty(resend) && resend.Contains("true"))
                    {
                        return;
                    }

                    string transactionType = Request.Form["txn_type"];
                    string Custom = Request.Form["custom"];
                    int AdditionalUserId = 0;
                    string projectname = null;
                    bool isReccurringProfile = false;
                    if (!string.IsNullOrEmpty(Custom))
                    {
                        AdditionalUserId = Convert.ToInt32(Custom.Split('_')[0]);
                    }
                    else
                    {
                        isReccurringProfile = true;
                    }


                    string RecurringPaymentId = string.IsNullOrEmpty(Request.Form["recurring_payment_id"]) ? null : Request.Form["recurring_payment_id"];
                    PaypalPaymentDetailsViewModel paypalPaymentDetails = new PaypalPaymentDetailsViewModel();
                    paypalPaymentDetails = GetPaymentDetailsFromResponse(RecurringPaymentId, AdditionalUserId, isReccurringProfile);
                    if (transactionType == Enum.GetName(typeof(PaypalTxnCode), PaypalTxnCode.recurring_payment_profile_created))
                    {
                        return;
                    }


                    // if recurring profile is cancelled.
                    else if (transactionType == Enum.GetName(typeof(PaypalTxnCode), PaypalTxnCode.recurring_payment_profile_cancel))
                    {
                        _logger.Info("CompanySubscription  is cancelled by paypal");
                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_profile_cancel;
                        PaypalPayments.CancelSubscriptionPaypalResponse(paypalPaymentDetails);
                    }


                    // if recurring payment fails.
                    else if (transactionType == Enum.GetName(typeof(PaypalTxnCode), PaypalTxnCode.recurring_payment_failed))
                    {
                        _logger.Info("CompanySubscription  is deactivated due to  failed payment by paypal");


                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_failed;
                        PaypalPayments.DeactivateSubscription(paypalPaymentDetails);
                    }


                    // if recurring payment is suspended due to max fails.
                    else if (transactionType == Enum.GetName(typeof(PaypalTxnCode), PaypalTxnCode.recurring_payment_suspended_due_to_max_failed_payment))
                    {
                        _logger.Info("User is suspended due to maximum failed payments attempt by paypal");
                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_suspended_due_to_max_failed_payment;
                        SuspendUser(paypalPaymentDetails, true);
                        //Common.SendMailOnUserSuspendedMaxFailures(paypalPaymentDetails);
                    }


                    // if admin suspends a user.
                    else if (transactionType == Enum.GetName(typeof(PaypalTxnCode), PaypalTxnCode.recurring_payment_suspended))
                    {
                        _logger.Info("User is suspended due to maximum failed payments attempt by paypal");
                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_suspended;
                        SuspendUser(paypalPaymentDetails, false);
                    }


                    else
                    {
                        if (paypalPaymentDetails.SaasuOrXero == Enum.GetName(typeof(SaasuOrXero), SaasuOrXero.Xero))
                        {
                            AddPaymentDetailstoDBFromPaypalResponse(paypalPaymentDetails);
                        }
                        if (paypalPaymentDetails.PaymentStatus == (int)PayPalPaymentStatus.Completed)
                        {
                            _logger.Info(" ~Execution of paypalnotification invoice event started.");
                            var receiverEmailAddress = System.Configuration.ConfigurationManager.AppSettings["paypalReceiverEmail"].ToLower();

                            if (string.IsNullOrEmpty(receiverEmailAddress))
                            {
                                throw new Exception("paypalnotification receiverEmailAddress is null");
                            }

                            if (HttpUtility.UrlDecode(paypalPaymentDetails.ReceiverEmailId).ToLower().Equals(receiverEmailAddress))
                            {
                                var PrivateAuthenticator = new PrivateXeroMethods(paypalPaymentDetails.EmailId);
                                var InvoiceCreated = PrivateAuthenticator.CreateInvoiceXero(paypalPaymentDetails);
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                _logger.Error(" ~Execution of paypalnotification ProcessPayPalIPNNotification WebMethod  event completed .", ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of paypalnotification ProcessPayPalIPNNotification event completed .", ex);
                throw;
            }
        }


        private void SuspendUser(PaypalPaymentDetailsViewModel paypalPaymentDetails, bool isMaxAttempt)
        {
            try
            {
                _logger.Info(" ~Execution of paypalnotification SuspendUser event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    // get company subscription from recurring profile id.
                    var companySubscription = dbContext.CompanySubscriptions.First(x => x.RecurringProfileId == paypalPaymentDetails.RecurringProfileId);

                    //check if suspended due to max attempts or by admin.
                    if (isMaxAttempt)
                    {
                        // if by max attempts check if there are any other subscriptions other than the suspended one. if no the suspend the user.
                        var getUserCompany = dbContext.CompanySubscriptions.Any(x => x.CompanyId == companySubscription.CompanyId && x.IsActive == true && x.RecurringProfileId != paypalPaymentDetails.RecurringProfileId);
                        if (!getUserCompany)
                        {
                            var getUser = dbContext.Users.FirstOrDefault(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                            Common.Common.SuspendUser(getUser.Email, getUser.UserId, "User is suspended due to maximum failed payments attempt by paypal");
                        }
                    }
                    var recurringPayment = dbContext.RecurringPayments.FirstOrDefault(x => x.RecurringProfileId == paypalPaymentDetails.RecurringProfileId);
                    if (recurringPayment == null)
                    {
                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_suspended_due_to_max_failed_payment;
                        PaypalPayments.SavePaymentDetailsToDB(paypalPaymentDetails);
                    }
                    else
                    {
                        recurringPayment.ModifiedDate = DateTime.Now;
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("~Execution of paypalnotification SuspendUser event completed.", ex);
                throw;
            }

        }


        private PaypalPaymentDetailsViewModel GetPaymentDetailsFromResponse(string recurringPaymentId, int AdditionalUserId, bool isReccurringProfile)
        {
            try
            {
                _logger.Info(" ~Execution of paypalnotification GetPaymentDetailsFromResponse event started.");
                PaypalPaymentDetailsViewModel paymentDetailsVM = new PaypalPaymentDetailsViewModel();
                string Custom = Request.Form["custom"];
                paymentDetailsVM = Common.Common.GetUserFromReccuringIdOrCompanyId(AdditionalUserId, recurringPaymentId);
                paymentDetailsVM.SaasuOrXero = Enum.GetName(typeof(SaasuOrXero), SaasuOrXero.Xero);
                paymentDetailsVM = ViewModelFromIPNResponse(paymentDetailsVM, recurringPaymentId, AdditionalUserId, isReccurringProfile);
                return paymentDetailsVM;
            }
            catch (Exception ex)
            {
                _logger.Error("~Execution of paypalnotification GetPaymentDetailsFromResponse event completed.", ex);
                throw;
            }
        }


        private PaypalPaymentDetailsViewModel ViewModelFromIPNResponse(PaypalPaymentDetailsViewModel paymentDetailsVM, string recurringPaymentId, int AdditionalUserId, bool isReccurringProfile)
        {
            try
            {
                var subscription = HttpUtility.UrlDecode(Request.Form["product_name"]);
                paymentDetailsVM.PaymentStatus = !string.IsNullOrEmpty(Request.Form["payment_status"]) ? (int)((PaypalPaymentStatus)Enum.Parse(typeof(PaypalPaymentStatus), Request.Form["payment_status"].ToLower())) : 0;
                paymentDetailsVM.Amount = string.IsNullOrEmpty(Request.Form["mc_gross"]) ? 0 : Decimal.Parse(Request.Form["mc_gross"]);
                paymentDetailsVM.PayerEmail = string.IsNullOrEmpty(Request.Form["payer_email"]) ? null : HttpUtility.UrlDecode(Request.Form["payer_email"]);
                paymentDetailsVM.PaymentCycle = string.IsNullOrEmpty(Request.Form["payment_cycle"]) ? 0 : (int)((PaymentCycle)Enum.Parse(typeof(PaymentCycle), Request.Form["payment_cycle"]));
                paymentDetailsVM.PaymentDate = string.IsNullOrEmpty(Request.Form["payment_date"]) ? (DateTime?)null : ConvertPaypalDatetimeToNormal(Request.Form["payment_date"]);
                paymentDetailsVM.NextPaymentDate = string.IsNullOrEmpty(Request.Form["next_payment_date"]) || Request.Form["next_payment_date"] == "N/A" ? (DateTime?)null : ConvertPaypalDatetimeToNormal(Request.Form["next_payment_date"]);
                paymentDetailsVM.RecurringCreatedDate = ConvertPaypalDatetimeToNormal(Request.Form["time_created"]);
                paymentDetailsVM.OutstandingBalance = string.IsNullOrEmpty(Request.Form["outstanding_balance"]) ? 0 : decimal.Parse(Request.Form["outstanding_balance"]);
                paymentDetailsVM.PayerId = string.IsNullOrEmpty(Request.Form["payer_id"]) ? null : Request.Form["payer_id"];
                paymentDetailsVM.Currency = !string.IsNullOrEmpty(Request.Form["mc_currency"]) ? Request.Form["mc_currency"] : Request.Form["currency_code"];
                paymentDetailsVM.RecurringProfileId = recurringPaymentId;
                paymentDetailsVM.ReceiverEmailId = Request.Form["receiver_email"];
                paymentDetailsVM.Subscription = !string.IsNullOrEmpty(subscription) ? subscription : "One time prorate charge for the new additional user";
                paymentDetailsVM.AdditionalUserID = AdditionalUserId;
                paymentDetailsVM.IsRecurringProfile = isReccurringProfile;
                return paymentDetailsVM;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        private void AddPaymentDetailstoDBFromPaypalResponse(PaypalPaymentDetailsViewModel paypalPaymentDetails)
        {
            string projectname = string.Empty;
            try
            {
                _logger.Info(" ~Execution of paypalnotification AddPaymentDetailstoDBFromPaypalResponse event started.");
                // IF direct payment then we will get Xero or Saasu in custom field.  
                if (paypalPaymentDetails.SaasuOrXero == Enum.GetName(typeof(SaasuOrXero), SaasuOrXero.Xero))
                {

                    if (paypalPaymentDetails.IsRecurringProfile)
                    {
                        PaypalPayments.AddPaymentsForRecurring(paypalPaymentDetails);
                    }
                    else
                    {
                        PaypalPayments.AddPaymentsForAddUser(paypalPaymentDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of paypalnotification AddPaymentDetailstoDBFromPaypalResponse event completed.", ex);
                throw;
            }
        }


        private DateTime ConvertPaypalDatetimeToNormal(string datetime)
        {
            DateTime paymentDate = DateTime.Now;
            if (!string.IsNullOrEmpty(datetime))
            {
                DateTime.TryParseExact(HttpUtility.UrlDecode(datetime),
            "HH:mm:ss MMM dd, yyyy PST", CultureInfo.InvariantCulture, DateTimeStyles.None, out paymentDate);

            }
            return paymentDate;
        }
    }
}
