﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="AccountMapping.aspx.cs" Inherits="ClearJelly.Web.AccountMapping" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>
    <%= Scripts.Render("~/bundles/jqueryConfirm") %>

    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Account Mapping</h1>
            </div>
            <asp:Panel ID="InfoPanel" runat="server" class="alert alert-success alert-dismissable" Visible="false">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <asp:Label ID="lblMessage" runat="server" Text="File uploaded successfully."></asp:Label>
            </asp:Panel>

            <asp:Panel ID="FailPanel" runat="server" class="alert alert-danger alert-dismissable" Visible="false">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <asp:Label ID="lblFailure" runat="server" Text="Some error occured. Please try after sometime."></asp:Label>
            </asp:Panel>
            <div id="successDiv" class="alert alert-success" role="alert" style="display: none">
                <a class="close" onclick="$('.alert').hide()">×</a>
                <span id="SuccessLabel"></span>
            </div>

            <div id="errorDiv" class="alert alert-danger" role="alert" style="display: none">
                <a class="close" onclick="$('.alert').hide()">×</a>
                Some error occured. Please try after sometime.
            </div>

            <div class="col-md-12 padding-right0">
                <div class="col-md-12 padding-right0">
                    <div class="col-md-3">
                        <asp:FileUpload ID="ExcelFileUpload" runat="server" />
                    </div>

                    <div class="col-md-1">
                        <asp:Button runat="server" ID="btnUpload" class="btn btn-default btn-success" OnClick="btnUploadClick"  style="display:none" Text="Upload" />
                    </div>
                    <div class="col-md-6 ">
                        <a href="SampleFiles/AccountMappigSample.xlsx" class="floatRightCss " download>
                            <img class="imageHeightStyle" title="Download sample" />
                        </a>

                    </div>
                    <div class="col-md-2 padding-right0">
                        <button type="button" id="ClearAllButton" class="btn btn-default btn-success  pull-right" >Clear All</button>
                    </div>
                </div>

                <asp:CustomValidator ID="CustomValidator1" runat="server" ForeColor="Red" ClientValidationFunction="ValidateFileUpload" ErrorMessage="Please select valid .xls or .xlsx file"></asp:CustomValidator>
            </div>

            <div id="UploadPayrolll"></div>
            <script src="Scripts/XeroWebApp/AccountMappingTable.js"></script>
        </div>
    </div>
    <style>
       
    </style>
</asp:Content>
