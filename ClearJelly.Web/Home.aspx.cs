﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using System.Web.Script.Services;
using System.Net;
using System.Threading.Tasks;
using Intuit.Ipp.OAuth2PlatformClient;
using Newtonsoft.Json.Linq;
using ClearJelly.Entities;
using ClearJelly.Web.Common;
using EntityModel.Entities;
using ClearJelly.XeroApp;
using ClearJelly.ViewModels.Home;
using ClearJelly.Configuration;
using ClearJelly.ABCFinancialApp;
using ClearJelly.QBApp.QBService;
using ClearJelly.ABCFinancialApp.Entities;
using ClearJelly.Services;
using System.Net.Mail;
using ClearJelly.Services.Helpers;
using ClearJelly.XeroApp.Helpers;
using ClearJelly.Configuration.Enums;

namespace ClearJelly.Web
{
    public partial class Home : Page
    {
        private static string _strConn2;
        private static string _metaDBConnection;
        private static NLog.Logger _logger;
        public delegate void Worker();
        private static Thread worker;
        private static ClearJellyEntities _clearJellyContext;
        private static bool isDbQuickBookCompleted = false;
        private static OAuth2Manager _manager;
        static HomeService HomeService;
        static string stateVal;
        static Dictionary<string, string> companies;
        static string code;
        static string incoming_state;
        static string realmId;
        private static DateTime _startDate;
        private static DateTime _endDate;
        private static int _modelType;
        public static int companyId;
        private static int _userId;
        private static string _orgName;
        private static string _user;
        private static string _currentDbName;
        public static User user;
        public static List<AbcClub> _listOfClubs;
        public static Dictionary<string, string> _xeroUserOrganisations;
        public static List<QuickBookCompany> qbUserCompanies;
        static Home()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _strConn2 = ConfigSection.CompanyDBConnection;
            companies = new Dictionary<string, string>();
            _metaDBConnection = ConfigSection.DefaultConnection;
            HomeService = new HomeService(_metaDBConnection);
            isDbQuickBookCompleted = false;
            _manager = new OAuth2Manager(
                    OAuth2Settings.RedirectUri,
                    OAuth2Settings.DiscoveryUrl,
                    OAuth2Settings.ClientID,
                    OAuth2Settings.ClientSecret,
                    ""
                );
            HomeService = new HomeService(_strConn2);
        }

        public Home()
        {
            _clearJellyContext = new ClearJellyEntities();
        }

        private void CompleteDropdownCompanies(string curentService)
        {
            var itemValues = System.Enum.GetValues(typeof(ChosenService));
            var itemNames = System.Enum.GetNames(typeof(ChosenService));
            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames[i], itemNames[i]);
                SelectServiceDropdown.Items.Add(item);
            }
            SelectServiceDropdown.Items.FindByText(curentService).Selected = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.FindControl("UserMenu").Visible = true;
                _user = User.Identity.Name;
                _logger.Info($"User Identity Name azure: " + User.Identity.Name);
                if (_user == null || _user == "")
                {
                    Response.Redirect("~/Account/Login.aspx", true);
                }
                _logger.Info($"Home start page_load, conectionString : " + ConfigSection.ClearJellyEntities);
                if (User.Identity.Name != null && user == null)
                {
                    try
                    {
                        user = HomeService.GetActiveUserByEmail(User.Identity.Name);
                        _listOfClubs = HomeService.GetAbcClubs(user.UserId);
                        _xeroUserOrganisations = HomeService.GetUserOrganisations(user.Email);
                        qbUserCompanies = HomeService.GetUserCompanies(user.UserId);
                    }
                    catch (Exception ex)
                    {
                        var AuthenticationManager = Context.GetOwinContext().Authentication;
                        AuthenticationManager.SignOut();
                        Response.Redirect("~/Account/Login.aspx", true);
                        _logger.Error($" exception:{ex.Message}");
                        return;
                    }
                }
                if (user.Email != User.Identity.Name)
                {
                    user = HomeService.GetActiveUserByEmail(User.Identity.Name);
                    _listOfClubs = HomeService.GetAbcClubs(user.UserId);
                    _xeroUserOrganisations = HomeService.GetUserOrganisations(user.Email);
                    qbUserCompanies = HomeService.GetUserCompanies(user.UserId);
                }

                if (!IsPostBack)
                {
                    LoadClubsDropDown();
                }
                if (user.Email == null)
                {
                    var AuthenticationManager = Context.GetOwinContext().Authentication;
                    AuthenticationManager.SignOut();
                    Response.Redirect("~/Account/Login.aspx", true);
                }
                companyId = user.CompanyId;
                if (!user.IsActive)
                {
                    Response.Redirect("~/Account/ActivateAccount.ASPX?success=true", false);
                }
                _userId = user.UserId;
                Session["Jelly_user"] = User.Identity.Name;
                Session["User_fullname"] = user.FirstName + " " + user.LastName;
                DataStartError.Text = string.Empty;
                DataEndError.Text = string.Empty;
                MessageLabel.Text = string.Empty;
                if (string.IsNullOrEmpty(_currentDbName))
                {
                    try
                    {
                        _currentDbName = HomeService.GetUserDBName(_user);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error while getting user db, " + ex.Message);
                    }
                }
                if (!_manager.Dictionary.ContainsKey("accessToken") && Request.QueryString != null && Request.QueryString.Count > 0)
                {
                    //Map the querytsring param values to Authorize response to get State and Code
                    var response = new AuthorizeResponse(Request.QueryString.ToString());
                    //extracts the state
                    if (response.State != null)
                    {
                        string CSRF;
                        //state returned after callback
                        incoming_state = response.State;
                        if (_manager.Dictionary.TryGetValue("CSRF", out CSRF))
                        {
                            if (CSRF != incoming_state)
                            {
                                _logger.Info("Invalid State");
                                _manager.Dictionary.Clear();
                            }
                            if (CSRF == incoming_state && response.RealmId != null)
                            {
                                realmId = response.RealmId;
                                if (!_manager.Dictionary.ContainsKey("realmId"))
                                {
                                    _manager.Dictionary.Add("realmId", realmId);
                                }

                                if (CSRF == incoming_state && response.Code != null)
                                {
                                    code = response.Code;
                                    _logger.Info("Authorization code obtained.");
                                    PageAsyncTask t = new PageAsyncTask(PerformCodeExchange);
                                    Page.RegisterAsyncTask(t);
                                    Page.ExecuteRegisteredAsyncTasks();
                                }
                            }
                        }
                    }
                }
                SubscriptionStatus.Text = "";
                if (Request.Params["status"] != null)
                {
                    SubscriptionStatus.Text = "Your subscription is added successfully. Once the payment is clear it will be available.";
                }
                if (!IsPostBack)
                {
                    GetSubscriptionDetails();
                }
                if (Request.Params["oauth_verifier"] != null && !IsPostBack)
                {
                    SqlAccessTokenStore delete_prev_token = new SqlAccessTokenStore();
                    delete_prev_token.DeletePrev(Request.Params["org"].ToString());
                    Authorize(Request.Params["oauth_token"], Request.Params["oauth_verifier"], Request.Params["org"]);
                    try
                    {
                        var api = XeroApiHelper.CoreApi();
                        var Name = api.Organisation.Name;
                        int allowCnt = 0;
                        var isAllowToAddOrg = IsAllowToAddNewOrg(Request.Params["org"], out allowCnt);
                        if (!isAllowToAddOrg)
                        {
                            MessageLabel.Text = " Cannot add more organizations. Your current subscription allows only " + allowCnt + " organizations.";
                        }
                        if (isAllowToAddOrg)
                        {
                            if (!HomeService.CheckModelExists(Name.Trim()))
                            {
                                _orgName = Name.Trim();
                                addXeroOrgToDB(Request.Params["org"], Name.Trim());
                                CreateModelOnSelectXeroOrg(Request.Params["org"], Name.Trim());
                                _xeroUserOrganisations = HomeService.GetUserOrganisations(user.Email);
                            }
                            Response.Redirect("~/Home", false);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error executing Page_Load method, exeption:" + ex.Message);
                    }
                }
                if (isDbQuickBookCompleted && (_manager.Dictionary.ContainsKey("accessToken")) && (_manager.Dictionary.ContainsKey("refreshToken")) && (_manager.Dictionary.ContainsKey("realmId")))
                {
                    isDbQuickBookCompleted = false;
                    var response = new AuthorizeResponse(Request.QueryString.ToString());
                    _manager.Dictionary["realmId"] = response.RealmId;
                    var tokenClient = new TokenClient(_manager.TokenEndpoint, _manager.ClientID, _manager.ClientSecret);
                    var test = tokenClient.RequestTokenFromCodeAsync(response.Code, _manager.RedirectURI);
                    TokenResponse accesstokenCallResponse = test.Result;
                    if (accesstokenCallResponse.HttpStatusCode == HttpStatusCode.OK)
                    {
                        var refresh_token = accesstokenCallResponse.RefreshToken;
                        refresh_token = HomeService.checkRefreshTockenByUser(_userId, accesstokenCallResponse.RefreshToken, _manager.Dictionary["realmId"].ToString(), accesstokenCallResponse.AccessToken);
                        _manager.Dictionary["refreshToken"] = refresh_token;
                        _logger.Info("Refresh token obtained.");
                        //access token
                        var access_token = accesstokenCallResponse.AccessToken;
                        _logger.Info("Access token obtained.");
                        _manager.Dictionary["accessToken"] = access_token;
                        _logger.Info("Validating Id Token.");
                        _logger.Info("Calling UserInfo");
                    }
                    var companyNametemp = _manager.QBOApiCallAsync(_currentDbName);
                    companyNametemp.Wait();
                    companyName.Text = companyNametemp.Result.CompanyName;
                    var newCompany = new QuickBookCompany
                    {
                        OrgName = companyNametemp.Result.CompanyName,
                        Id = Guid.NewGuid().ToString(),
                        isActive = true,
                        RealmId = response.RealmId
                    };
                    HomeService.AddCompany(newCompany, _userId);
                    qbUserCompanies = HomeService.GetUserCompanies(_userId);
                    GetUserOrganisationsQuickBooks(newCompany);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Page_Load event completed.", ex);
                Response.Redirect("~/Account/Login.aspx", true);
                return;
            }
        }

        private void LoadClubsDropDown()
        {
            ABCClubsList.Items.Clear();
            var itemValues = _listOfClubs;
            if (itemValues == null || itemValues.Count == 0)
            {
                ABCClubsList.Items.Add(new ListItem("No club selected", "No club selected"));
            }
            foreach (var club in itemValues)
            {
                ABCClubsList.Items.Add(new ListItem(club.ClubName, club.ClubNumber.ToString()));
            }
        }
        protected void ABCClubsList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void QuickbookClick(object sender, EventArgs e)
        {
            String UserName = _user != null ? _user : "";
            HomeService.updateUserChosenService("Quickbooks", UserName);
            _manager.Dictionary["ChosenService"] = "Quickbooks";
        }

        protected void XeroClick(object sender, EventArgs e)
        {
            String UserName = _user != null ? _user : "";
            HomeService.updateUserChosenService("Xero", UserName);
            _manager.Dictionary["ChosenService"] = "Xero";
        }

        private void ChangeSwerviceValueDropdown(string value)
        {
            _manager.Dictionary["ChosenService"] = value;
            if (value == ChosenService.Quickbooks.ToString())
            {
                CompanyListDropDown.Items.Clear();
                var companyList = qbUserCompanies;
                if (companyList.Count == 0)
                {
                    CompanyListDropDown.Items.Insert(0, "-Select your organisation-");
                    UpdateButtonHideShow(false);
                }
                companies = new Dictionary<string, string>();
                foreach (var item in companyList)
                {
                    GetUserOrganisationsQuickBooks(item);
                }
                AddClubAbc.Visible = false;
                ChooseClubNumber.Visible = false;
                QuickBooksControls.Visible = true;
                XeroControls.Visible = false;
                AbcControls.Visible = false;
                string dbName = "QuickBook_" + user.Company.Name;
                UserDatabaseName.Text = dbName;
            }
            if (value == ChosenService.ABC.ToString())
            {
                CompanyListDropDown.Items.Clear();
                CompanyListDropDown.Items.Insert(0, "-Select your organisation-");
                AbcControls.Visible = true;
                AddClubAbc.Visible = true;
                ChooseClubNumber.Visible = true;
                QuickBooksControls.Visible = false;
                XeroControls.Visible = false;
                UpdateButtonHideShow(true);
                divUpdateButton.Visible = false;
                string dbName = "ABC_Financial_" + user.Company.Name;
                UserDatabaseName.Text = dbName;
            }
            if (value == ChosenService.Xero.ToString())
            {
                CompanyListDropDown.Items.Clear();
                AbcControls.Visible = false;
                QuickBooksControls.Visible = false;
                XeroControls.Visible = true;
                AddClubAbc.Visible = false;
                ChooseClubNumber.Visible = false;
                companies = new Dictionary<string, string>();
                GetUserOrganisations(_user);
                string dbName = "Xero_" + user.Company.Name;
                UserDatabaseName.Text = dbName;
            }
        }
        protected void SelectServiceDropdown_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeSwerviceValueDropdown(SelectServiceDropdown.SelectedItem.Value);
        }

        private bool GetValueProcessRunning()
        {
            try
            {
                var homeObj = new Home();
                if (_userId == null)
                {
                    throw new Exception("session Jelly_UserId is not defined.");
                }
                var loggedinUser = _userId;
                var isAnyRunning = homeObj.isAnyRunnigProcess(loggedinUser);
                return isAnyRunning;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + "~Execution of GetValueProcessRunning method completed.", ex);
                throw;
            }
        }

        private bool GetDeleteOrgProcessRunning(string orgName)
        {
            try
            {
                _logger.Info((_user ?? "") + "~Execution of GetValueProcessRunning method started.");
                var homeObj = new Home();
                if (_userId == null)
                {
                    throw new Exception("session Jelly_UserId is not defined.");
                }
                var loggedinUser = _userId;
                var isAnyRunning = homeObj.isAnyRunnigModelProcess(loggedinUser, orgName);
                return isAnyRunning;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + "~Execution of GetValueProcessRunning method completed.", ex);
                throw;
            }
        }

        private bool IsAllowToAddNewOrg(string orgShortCode, out int AllowOrgCount)
        {
            try
            {
                var email = _user != null ? _user : "";
                AllowOrgCount = 0;
                var currentUser = user;
                if (currentUser != null)
                {
                    var currentDate = DateTime.Now.Date;
                    var currentSubscriptions = HomeService.GetCurrentSubscriptions(currentUser.CompanyId, email);
                    int allowCont = 0;
                    foreach (var currentSubscription in currentSubscriptions)
                    {
                        allowCont = allowCont + (currentSubscription.SubscriptionType.Entities * currentSubscription.Quantity);
                    }
                    AllowOrgCount = allowCont;
                    var isExists = HomeService.IsXeroCompanyExist(currentUser.CompanyId, orgShortCode);
                    if (isExists)
                    {
                        return true;
                    }
                    if (!isExists)
                    {
                        var userOrgCnt = HomeService.GetXeroCompanyCount(currentUser.CompanyId);
                        if (userOrgCnt < allowCont)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetSubscriptionDetails()
        {
            try
            {
                var email = _user != null ? _user : "";
                var currentUser = user;
                if (currentUser != null)
                {
                    if (SelectServiceDropdown.Items.Count == 0)
                    {
                        CompleteDropdownCompanies(ChosenService.ABC.ToString());
                    }
                    if (!_manager.Dictionary.ContainsKey("ChosenService"))
                    {
                        _manager.Dictionary.Add("ChosenService", ChosenService.ABC.ToString());
                    }
                    else
                    {
                        _manager.Dictionary["ChosenService"] = ChosenService.ABC.ToString();
                    }
                    ChangeSwerviceValueDropdown(ChosenService.ABC.ToString());
                    if (currentUser.IsAdmin)
                    {
                        ChangeSubscriptoinPanel.Visible = true;
                    }
                    var currentDate = DateTime.Now.Date;
                    var currentSubscriptions = HomeService.GetCurrentSubscriptions(currentUser.CompanyId, email);
                    string subscriptionStr = null;
                    //filter subscription which were assigned to user.
                    if (!currentUser.IsAdmin)
                    {
                        var childUserAssignedSubscription = HomeService.GetChildUserAssignedSubscription(currentUser.UserId);
                        currentSubscriptions = currentSubscriptions.Where(a => childUserAssignedSubscription.Contains(a.CompanySubscriptionId)).ToList();
                    }
                    if (currentSubscriptions.Any())
                    {
                        foreach (var subscription in currentSubscriptions)
                        {
                            subscriptionStr = subscriptionStr + ", " + subscription.SubscriptionType.TypeName;
                            if (subscription.EndDate != null)
                            {
                                subscriptionStr += " (Trial)";
                            }
                        }
                        if (currentSubscriptions.Any(a => a.EndDate == null))
                        {
                            ExpiryDate.Text = "N/A";
                        }
                        if (!currentSubscriptions.Any(a => a.EndDate == null))
                        {
                            var maxEndDate = currentSubscriptions.Max(a => a.EndDate);
                            ExpiryDate.Text = maxEndDate.HasValue ? maxEndDate.Value.ToShortDateString() : "N/A";
                        }
                        SubscriptionTypeName.Text = subscriptionStr.Trim(',');
                    }
                    if (!currentSubscriptions.Any())
                    {
                        SubscriptionTypeName.Text = "None";
                        ExpiryDate.Text = "None";
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetSubscriptionDetails event completed.", ex);
                throw;
            }
        }

        protected void cmdConnect_Click(object sender, EventArgs e)
        {
            try
            {
                var loggedinUserID = _userId;
                if (!isAnyRunnigProcess(loggedinUserID))
                {
                    string _email = _user;
                    bool dbExist = false;
                    if (!String.IsNullOrEmpty(_email) && _email != "")
                    {
                        string dbName = "Xero_" + user.Company.Name;
                        dbExist = Common.Common.CheckDatabaseExistsAsync(dbName);
                    }
                    if (!dbExist)
                    {
                        var _companyName = user.Company.Name;
                        var _userName = user.Email;
                        var _dbEncryptPass = user.Password;
                        var _password = EncryptDecrypt.DecryptString(_dbEncryptPass);
                        _logger.Info("Create Xero DB ");
                        XeroTablesGenerator xeroTablesGenerator = new XeroTablesGenerator(_companyName);
                        var res = xeroTablesGenerator.CreateQBDbAsync(_companyName);
                    }
                    Connect();
                }
                if (isAnyRunnigProcess(loggedinUserID))
                {
                    MessageLabel.Text = "A process is already running, please try after some time.";
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home cmdConnect_Click event completed.", ex);
                throw;
            }
        }

        private static void CheckDBQB(string _email)
        {
            bool dbExist = false;
            if (!String.IsNullOrEmpty(_email) && _email != "")
            {
                string dbName = "QuickBooks_" + user.Company.Name;
                dbExist = Common.Common.CheckDatabaseExistsAsync(dbName);
            }
            if (!dbExist)
            {
                var _companyName = user.Company.Name;
                var _userName = user.Email;
                var _dbEncryptPass = user.Password;
                var _password = EncryptDecrypt.DecryptString(_dbEncryptPass);
                try
                {
                    _logger.Info("Create DB QB");
                    QBTablesGenerator QBTablesGenerator = new QBTablesGenerator(_companyName);
                    var res = QBTablesGenerator.CreateQBDbAsync(_companyName);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error Create DB " + ex.Message);
                }
            }
        }

        protected async void ImgC2QB_Click(object sender, EventArgs e)
        {
            try
            {
                await _manager.GetDiscoveryData_JWKSkeys();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            string _email = _user;
            CheckDBQB(_email);
            doOAuth("C2QB");
        }

        protected async void ImgABC_Click(object sender, EventArgs e)
        {
            try
            {
                ClubError.Text = String.Empty;
                if (ABCClubsList.SelectedValue == "No club selected")
                {
                    ClubError.Text = "Add clubs first";
                    ClubError.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                if (txtSD.Text == null || txtSD.Text == string.Empty)
                {
                    DataStartError.Visible = true;
                    DataStartError.Text = "No start date selected";
                    DataStartError.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                if (txtED.Text == null || txtED.Text == string.Empty)
                {
                    DataEndError.Visible = true;
                    DataEndError.Text = "No end date selected";
                    DataEndError.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                _startDate = DateTime.ParseExact(txtSD.Text, "dd/MM/yyyy", null);
                _endDate = DateTime.ParseExact(txtED.Text, "dd/MM/yyyy", null);

                string email = _user;
                bool dbExist = false;
                if (!String.IsNullOrEmpty(email) && email != "")
                {
                    string dbName = "ABC_Financial_" + user.Company.Name;
                    dbExist = Common.Common.CheckDatabaseExistsAsync(dbName);
                }

                if (!dbExist)
                {
                    var companyName = user.Company.Name;
                    ABCFinancialsPaginasionService ABCService = new ABCFinancialsPaginasionService(_currentDbName);
                    var username = user.Email;
                    var pass = EncryptDecrypt.DecryptString(user.Password);
                    await ABCService.CreateAbcDbAsync(companyName);
                    InitABC(ABCClubsList.SelectedValue, user);
                    MessageLabel.Text = "Data loading process for ABC started, this process may take 2 minutes";
                    MessageLabel.Visible = true;
                    return;
                }

                var result = UpdateABC(ABCClubsList.SelectedValue, user);
                MessageLabel.Text = "Data loading process for ABC started, this process may take 2 minutes";
                MessageLabel.Visible = true;
                //AbcUpdateStatus.Text = "DB successful updated";
                if (!result)
                {
                    //AbcUpdateStatus.Text = "DB updated error";
                }
                _logger.Info($"Insert ClubCheckins Finished");
            }
            catch (Exception ex)
            {
                _logger.Error("ImgABC_Click", ex.Message);
            }
        }

        protected async void AddClub_Click(object sender, EventArgs e)
        {
            var userId = _userId;
            var clubExist = HomeService.CheckAbcClub(ClubNumber.Text, userId);
            if (clubExist)
            {
                ClubMessage.Text = "This club number alredy exist";
                return;
            }
            ABCFinancialsPaginasionService ABCService = new ABCFinancialsPaginasionService(_currentDbName);
            var clubWrong = ABCService.GetClubByNumber(ClubNumber.Text, userId);
            if (clubWrong == null)
            {
                ClubMessage.Text = "There is no such club number";
                return;
            }
            _listOfClubs = HomeService.GetAbcClubs(user.UserId);
            LoadClubsDropDown();
            ClubMessage.Text = "Club added successfully";
        }

        public static bool UpdateABC(string club, User currentUser)
        {
            _currentDbName = HomeService.GetUserDBName(_user);
            string _club = club;
            if (club == null || club == "")
            {
                _club = "9003";
            }
            try
            {
               // bool isCompleted = false;
                if (!String.IsNullOrEmpty(_currentDbName))
                {
                    ABCFinancialsPaginasionService ABCService = new ABCFinancialsPaginasionService(_currentDbName);
                   Task.Run(async () => await ABCService.UpdateMembers(_currentDbName, _club, _startDate, _endDate, currentUser));
                }
                
                return false;
            }
            catch (Exception ex)
            {
                SmtpMailSendingService.SendAbcDbUpdateErrorMailMessage(currentUser, ServiceTypes.ABC.ToString());
                _logger.Error((_user ?? "") + " ~Exeption of UpdateABC event.", ex.Message);
                return false;
            }
        }

        public static bool InitABC(string club, User currentUser)
        {
            _currentDbName = HomeService.GetUserDBName(_user);
            string _club = club;
            if (club == null || club == "")
            {
                _club = "9003";
            }
            try
            {
                bool isCompleted = false;
                if (!String.IsNullOrEmpty(_currentDbName))
                {
                    ABCFinancialsPaginasionService ABCService = new ABCFinancialsPaginasionService(_currentDbName);
                    isCompleted = Task.Run(async () => await ABCService.FetchAllDataAsync(_currentDbName, _club, _startDate, _endDate)).Result;
                }
                if (isCompleted)
                {
                    _logger.Info("InitABC isCompleted true");
                    SmtpMailSendingService.SendDbUpdateFinishedMailMessage(currentUser, ServiceTypes.ABC.ToString());
                    _logger.Info("Sending Service finished");
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                SmtpMailSendingService.SendAbcDbUpdateErrorMailMessage(currentUser, ServiceTypes.ABC.ToString());
                _logger.Error((_user ?? "") + " ~Exeption of InitABC event.", ex.Message);
                return false;
            }
        }

        public void doOAuth(string callMadeBy)
        {
            isDbQuickBookCompleted = true;
            _logger.Info("Intiating OAuth2 call to get code.");
            string authorizationRequest = "";
            string scopeVal = "";
            stateVal = CryptoRandom.CreateUniqueId();

            if (!_manager.Dictionary.ContainsKey("CSRF"))
            {
                _manager.Dictionary.Add("CSRF", stateVal);
            }

            if (callMadeBy == "C2QB")
            {
                if (!_manager.Dictionary.ContainsKey("callMadeBy"))
                {
                    _manager.Dictionary.Add("callMadeBy", callMadeBy);
                }
                if(_manager.Dictionary.ContainsKey("callMadeBy"))
                {
                    _manager.Dictionary["callMadeBy"] = callMadeBy;
                }

                scopeVal = OidcScopes.Accounting.GetStringValue() + " " + OidcScopes.Payment.GetStringValue();
            }
            if (callMadeBy == "OpenId" && callMadeBy != "C2QB")
            {
                if (!_manager.Dictionary.ContainsKey("callMadeBy"))
                {
                    _manager.Dictionary.Add("callMadeBy", callMadeBy);
                }
                if (_manager.Dictionary.ContainsKey("callMadeBy"))
                {
                    _manager.Dictionary["callMadeBy"] = callMadeBy;
                }

                scopeVal = OidcScopes.Accounting.GetStringValue() + " " + OidcScopes.Payment.GetStringValue()
                    + " " + OidcScopes.OpenId.GetStringValue() + " " + OidcScopes.Address.GetStringValue()
                    + " " + OidcScopes.Email.GetStringValue() + " " + OidcScopes.Phone.GetStringValue()
                    + " " + OidcScopes.Profile.GetStringValue();
            }

            _logger.Info("Setting up Authorize url");

            if (_manager.AuthorizationEndpoint != "" && _manager.AuthorizationEndpoint != null)
            {
                authorizationRequest = string.Format("{0}?client_id={1}&response_type=code&scope={2}&redirect_uri={3}&state={4}",
                        _manager.AuthorizationEndpoint,
                        _manager.ClientID,
                        scopeVal,
                        System.Uri.EscapeDataString(_manager.RedirectURI),
                        stateVal);
                _logger.Info("Calling AuthorizeUrl");
                Response.Redirect(authorizationRequest);
            }
            if (_manager.AuthorizationEndpoint == "" && _manager.AuthorizationEndpoint == null)
            {
                _logger.Error("Missing authorizationEndpoint url!");
            }
        }

        public async Task PerformCodeExchange()
        {
            isDbQuickBookCompleted = false;
            _logger.Info("Exchanging code for tokens.");

            string id_token = "";
            string refresh_token = "";
            string access_token = "";
            bool isTokenValid = false;
            var tokenClient = new TokenClient(_manager.TokenEndpoint, _manager.ClientID, _manager.ClientSecret);
            TokenResponse accesstokenCallResponse = await tokenClient.RequestTokenFromCodeAsync(code, _manager.RedirectURI);
            if (accesstokenCallResponse.HttpStatusCode == HttpStatusCode.OK)
            {
                //save the refresh token in persistent store so that it can be used to refresh short lived access tokens
                refresh_token = HomeService.checkRefreshTockenByUser(_userId, accesstokenCallResponse.RefreshToken, _manager.Dictionary["realmId"].ToString(), accesstokenCallResponse.AccessToken);
                _manager.Dictionary.Add("refreshToken", refresh_token);
                _logger.Info("Refresh token obtained.");
                access_token = accesstokenCallResponse.AccessToken;
                _logger.Info("Access token obtained.");
                if (!_manager.Dictionary.ContainsKey("accessToken"))
                {
                    _manager.Dictionary.Add("accessToken", access_token);
                }
                //Identity Token (returned only for OpenId scope)
                id_token = accesstokenCallResponse.IdentityToken;
                _logger.Info("Id token obtained.");
                isTokenValid = await _manager.IsIdTokenValid(id_token);
                _logger.Info("Validating Id Token.");
                _logger.Info("Calling UserInfo");
                var company = await _manager.QBOApiCall(_currentDbName);
                companyName.Text = company.CompanyName;
                var newCompany = new QuickBookCompany
                {
                    OrgName = company.CompanyName,
                    Id = Guid.NewGuid().ToString(),
                    isActive = true,
                    RealmId = realmId
                };
                HomeService.AddCompany(newCompany, _userId);

                qbUserCompanies = HomeService.GetUserCompanies(user.UserId);
                GetUserOrganisationsQuickBooks(newCompany);
                isDbQuickBookCompleted = false;
                UserInfoResponse userInfoResponse = await _manager.GetUserInfo(access_token, refresh_token);
                if (userInfoResponse.HttpStatusCode == HttpStatusCode.OK)
                {
                    company = await _manager.QBOApiCall(_currentDbName);
                    GetUserOrganisationsQuickBooks(newCompany);
                    companyName.Text = company.CompanyName;
                }
            }
        }

        public void Connect()
        {
            try
            {
                if (_user == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }

                String uName = _user;
                var authorizeUrl = "";
                Session["_authenticator"] = null;
                if (Session["_authenticator"] == null)
                {
                    try
                    {
                        var partner_auth = XeroApiHelper.MvcAuthenticator(uName);
                        Session["_authenticator"] = partner_auth;
                        authorizeUrl = partner_auth.GetRequestTokenAuthorizeUrl(uName);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Connect(): " + ex.Message + "\n" + ex);
                    }
                }
                authorizeUrl = ((PartnerMvcAuthenticator)Session["_authenticator"]).GetRequestTokenAuthorizeUrl(uName);
                Response.Redirect(authorizeUrl, false);
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Connect event completed.", ex);
                throw;
            }
        }

        public void UpdateButtonHideShow(bool showHide)
        {
            divUpdateButton.Visible = showHide;
            ImagetxtSD.Visible = showHide;
            ImagetxtED.Visible = showHide;
            UpdateModelDateStartText.Visible = showHide;
            UpdateModelDateEndText.Visible = showHide;
            UpdateModelDateStart.Visible = showHide;
            UpdateModelDateEnd.Visible = showHide;
            UpdateModelText.Visible = showHide;
        }

        public IToken Authorize(string oauth_token, string oauth_verifier, string org)
        {
            try
            {
                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                if (Session["_authenticator"] == null)
                {
                    throw new Exception("Invalid session _authenticator");
                }
                PartnerMvcAuthenticator auth = ((PartnerMvcAuthenticator)Session["_authenticator"]);
                String uName = _user;

                var accessToken = auth.RetrieveAndStoreAccessToken(uName, oauth_token, oauth_verifier, org);
                if (accessToken == null)
                    return (null);

                return accessToken;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Authorize event completed.", ex);
                throw;
            }
        }

        private void GetUserOrganisationsQuickBooks(QuickBookCompany company)
        {
            try
            {
                if (company != null && company.OrgName == null && company.RealmId != null)
                {
                    company.OrgName = HomeService.UpdateQBCompany(_userId, company.RealmId);
                }
                if (!companies.ContainsKey(company.RealmId) && company != null && company.RealmId != null)
                {
                    companies.Add(company.RealmId, company.OrgName);
                }
                if (companies.Count == 0)
                {
                    CompanyListDropDown.Items.Insert(0, "-Select your organisation-");
                    UpdateButtonHideShow(false);
                }
                if (companies.Count > 0)
                {
                    CompanyListDropDown.DataSource = companies;
                    CompanyListDropDown.DataTextField = "Value";
                    CompanyListDropDown.DataValueField = "Key";
                    CompanyListDropDown.DataBind();
                    UpdateButtonHideShow(true);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetUserOrganisations event completed.", ex);
            }
        }

        private void GetUserOrganisations(String uName)
        {
            try
            {
                companies = _xeroUserOrganisations;
                CompanyListDropDown.DataSource = companies;
                CompanyListDropDown.DataTextField = "Value";
                CompanyListDropDown.DataValueField = "Key";
                CompanyListDropDown.DataBind();
                CompanyListDropDown.Items.Insert(0, "-Select your organisation-");

                if (_orgName != null)
                {
                    CompanyListDropDown.SelectedIndex = CompanyListDropDown.Items.IndexOf(CompanyListDropDown.Items.FindByText(_orgName.ToString()));
                }
                if (CompanyListDropDown.Items.Count > 1 && _orgName == null)
                {
                    CompanyListDropDown.SelectedIndex = 1;
                    PartnerMvcAuthenticator partner_auth = null;
                    partner_auth = (PartnerMvcAuthenticator)XeroApiHelper.MvcAuthenticator(uName);
                    UpdateButtonHideShow(true);
                    return;
                }
                if (_orgName == null)
                {
                    UpdateButtonHideShow(false);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetUserOrganisations event completed.", ex);
                throw;
            }
        }

        protected void CompanyQickBookListDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _orgName = CompanyListDropDown.SelectedItem.Text;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home CompanyListDropDown_SelectedIndexChanged event completed.", ex);

                throw;
            }
        }

        protected void CompanyListDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        public static void Init(Worker work, int userId, string orgName, int ModelType, DateTime startDate, DateTime endDate)
        {
            try
            {
                _logger.Info(_user ?? "" + " ~Execution of Init method started.");
                worker = new Thread(new ThreadStart(work));
                worker.Start();
                var homeObj = new Home();
                HomeService.InsertModelProcess(userId, orgName, ModelType, startDate, endDate);
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Init event completed.", ex);
                throw;
            }
        }

        private void addXeroOrgToDB(String OrgShortCode, String OrgName)
        {
            try
            {
                _logger.Info(_user ?? "" + " ~Execution of clear_table method started.");
                int intCompanyId = companyId;
                bool checkOrgExists = HomeService.IsXeroCompanyExist(intCompanyId, OrgShortCode);
                if (!checkOrgExists)
                {
                    var xeroOrg = new Xero_User_Org();
                    xeroOrg.CompanyID = intCompanyId;
                    xeroOrg.OrgShortCode = OrgShortCode;
                    xeroOrg.isActive = true;
                    xeroOrg.OrgName = OrgName;
                    HomeService.AddXeroOrg(xeroOrg);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home addXeroOrgToDB event completed.", ex);
                throw;
            }
        }
        #region Get Report QB
        protected void getReportQB(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtSD.Text))
            {
                DataStartError.Visible = true;
                DataStartError.Text = "No start date selected";
                DataStartError.ForeColor = System.Drawing.Color.Red;
                return;
            }
            if (String.IsNullOrEmpty(txtED.Text))
            {
                DataEndError.Visible = true;
                DataEndError.Text = "No end date selected";
                DataEndError.ForeColor = System.Drawing.Color.Red;
                return;
            }

            _startDate = DateTime.ParseExact(txtSD.Text, "dd/MM/yyyy", null);
            _endDate = DateTime.ParseExact(txtED.Text, "dd/MM/yyyy", null);

            _strConn2 = ConfigSection.CompanyDBConnection + ";database=QuickBooks_" + _currentDbName + ";";
            //QBService = new QBService(_manager.Dictionary["accessToken"], _manager.Dictionary["realmId"], _strConn2);
            //var res = QBService.GetReport(CompanyQickBookListDropDown.SelectedItem.Value, _user, _startDate, _endDate, CompanyQickBookListDropDown.SelectedItem.Text);
            if (Request.Cookies["selectedCompany"] != null)
            {
                _orgName = Request.Cookies["selectedCompany"].Value.ToString();
                _orgName = _orgName.Replace("%20", " ");
            }
            if (_orgName == null)
            {
                _orgName = CompanyListDropDown.SelectedItem.Text;
            }
        }
        static void parseJson(JToken tokens)
        {
            if (tokens.HasValues && tokens.Path.IndexOf("ColData") < 0)
            {
                foreach (JToken x in tokens.Children())
                {
                    if (tokens.Path.IndexOf("Summary") < 0 && tokens.Path.IndexOf("Header") < 0 && tokens.Path.IndexOf("Section") < 0)
                    {
                        parseJson(x);
                    }
                }
            }
        }
        #endregion  End of Get XML From Report object  
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtSD.Text))
            {
                DataStartError.Visible = true;
                DataStartError.Text = "No start date selected";
                DataStartError.ForeColor = System.Drawing.Color.Red;
                return;
            }
            if (String.IsNullOrEmpty(txtED.Text))
            {
                DataEndError.Visible = true;
                DataEndError.Text = "No end date selected";
                DataEndError.ForeColor = System.Drawing.Color.Red;
                return;
            }
            _startDate = new DateTime();
            _endDate = new DateTime();
            _startDate = DateTime.ParseExact(txtSD.Text, "dd/MM/yyyy", null);
            _endDate = DateTime.ParseExact(txtED.Text, "dd/MM/yyyy", null);
            _modelType = (int)ModelType.Update;
            _orgName = CompanyListDropDown.SelectedItem.Text;
            _orgName = _orgName.ToString();
            if (_manager.Dictionary.ContainsKey("ChosenService") && _manager.Dictionary["ChosenService"] == "Quickbooks")
            {
                Response.Cookies["UpdateModelProcess"].Value = "running";
                Response.Cookies["UpdateModelProcess"].Expires = DateTime.Now.AddHours(1);
            }

            try
            {
                if (_userId == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var loggedinUserID = _userId;
                var homeObj = new Home();
                //var isAnyRunning = homeObj.isAnyRunnigProcess(loggedinUserID);
                //if (isAnyRunning)
                //{
                //    MessageLabel.Text = "A process is already running, please try after some time."; ;
                //    return;
                //}
                if (txtSD.Text.Trim() == "")
                {
                    MessageLabel.Text = "Please select a start date.";
                    return;
                }
                if (txtED.Text.Trim() == "")
                {
                    MessageLabel.Text = "Please select a end date.";
                    return;
                }
                if ((txtSD.Text.Trim() != "" && txtED.Text.Trim() != "") && (!_manager.Dictionary.ContainsKey("ChosenService")  || _manager.Dictionary["ChosenService"] != "Quickbooks"))
                {
                    CultureInfo Au_date = new CultureInfo("en-AU");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-AU");
                    String inOrgShortCode = CompanyListDropDown.SelectedItem.Value;
                    String inOrgName = CompanyListDropDown.SelectedItem.Text;
                    DateTime startDate = DateTime.UtcNow.AddMonths(-1);
                    DateTime endDate = DateTime.UtcNow;

                    startDate = DateTime.ParseExact(txtSD.Text.Trim(), "dd/MM/yyyy", Au_date);
                    endDate = DateTime.ParseExact(txtED.Text.Trim(), "dd/MM/yyyy", Au_date);

                    _logger.Info("User date selection startDate: " + startDate + " EndDate: " + endDate);
                    MessageLabel.Text = "An email will be sent to you when the process is completed.";

                    var api = XeroApiHelper.CoreApi();
                    if (_user == null)
                    {
                        throw new Exception("Invalid session Jelly_user");
                    }
                    var loggedinUser = _user;
                    string decryptedPassword = null;
                    var currentUser = HomeService.GetActiveUserByEmail(loggedinUser);
                    if (currentUser != null)
                    {
                        decryptedPassword = EncryptDecrypt.DecryptString(currentUser.Password);
                    }

                    XeroToCJ obj = new XeroToCJ(api, (_user ?? "").ToString(), decryptedPassword, inOrgName, inOrgShortCode);
                    Init(() => Work_Update(obj, inOrgShortCode, (_user ?? "").ToString(), currentUser.UserId, startDate, endDate), currentUser.UserId, inOrgName, (int)ModelType.Update, startDate, endDate);
                    txtSD.Text = "";
                    txtED.Text = "";
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home btnUpdate_Click event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static async Task<string> InitQB()
        {
            try
            {
                _logger.Info(_user ?? "" + " ~Execution of Init method started.");
                CheckDBQB(_user);
                HomeService.InsertModelProcess(_userId, _orgName, _modelType, _startDate, _endDate);
                var isCompleted = await UpdateQBDataAsync(_user, _startDate, _endDate, HomeService.GetRealmId(_orgName, _userId), _orgName);
                SmtpMailSendingService.SendDbUpdateFinishedMailMessage(user, ServiceTypes.QuickBooks.ToString());
                return isCompleted;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Init event completed.", ex);
                throw;
            }
        }

        private static async Task<string> UpdateQBDataAsync(string user, DateTime startDate, DateTime endDate, string realmId = null, string selectedCompany = null)
        {
            int errorCodes = 0;
            try
            {
                errorCodes = await _manager.UpdateQBData(_currentDbName, startDate, endDate, _user, realmId, selectedCompany);
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Work_Update event completed.", ex);
                return "Adding data failed.";
            }
            finally
            {
                HomeService.UpdateModelStatus(_userId, 200);
            }
            if (errorCodes == 0)
            {
                return "No data in date range.";
            }
            return "The process is completed, an email will be sent to you.";
        }

        private void CreateModelOnSelectXeroOrg(String OrgShortCode, String OrgName)
        {
            try
            {
                _logger.Info((_user ?? "").ToString() + " ~Execution of CreateModelOnSelectXeroOrg method started.");
                String inOrgShortCode = OrgShortCode;
                String inOrgName = OrgName;
                if (_user == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var loggedinUser = _user;
                string decryptedPassword = null;
                var currentUser = user;
                if (currentUser != null)
                {
                    decryptedPassword = EncryptDecrypt.DecryptString(currentUser.Password);
                }

                var homeObj = new Home();
                var isAnyRunning = homeObj.isAnyRunnigProcess(currentUser.UserId);
                if (isAnyRunning)
                {
                    MessageLabel.Text = "A process is already running, please try after some time.";
                }
                if (!isAnyRunning)
                {
                    var api = XeroApiHelper.CoreApi();
                    HomeService.InsertOrganizationDetails(api, OrgShortCode, currentUser.CompanyId);
                    DateTime endDate = DateTime.UtcNow;
                    DateTime startDate = DateTime.UtcNow.AddMonths(-6);
                    XeroToCJ obj = new XeroToCJ(api, (loggedinUser ?? "").ToString(), decryptedPassword, inOrgName, inOrgShortCode);
                    MessageLabel.Text = "An email will be sent to you when the process is completed.";
                    Init(() => HomeService.Work_create(obj, inOrgShortCode, currentUser.UserId, startDate, endDate), currentUser.UserId, inOrgName, (int)ModelType.Create, startDate, endDate);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "").ToString() + " ~Execution of CreateModelOnSelectXeroOrg method completed.", ex);
            }
        }

        private bool isAnyRunnigProcess(int userId)
        {
            try
            {
                var runningTypeId = StatusEnum.Running;
                return HomeService.GetModelProcesses(userId, runningTypeId);
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home isAnyRunnigProcess event completed.", ex);
                return false;
            }
        }

        private bool isAnyRunnigModelProcess(int userId, string inOrgName)
        {
            try
            {
                _logger.Info(_user ?? "" + " ~Execution of isAnyRunnigProcess method started.");
                var runningTypeId = (int)StatusEnum.Running;
                var runningProcess = HomeService.GetRunningProcesses(userId, runningTypeId, inOrgName);
                return runningProcess;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home isAnyRunnigProcess event completed.", ex);
                return false;
            }
        }

        public void Work_Update(XeroToCJ obj, string org, string username, int userId, DateTime startDate, DateTime endDate)
        {
            var isSuccess = false;
            int errorCodes = 0;
            try
            {
                CultureInfo Au_date = new CultureInfo("en-AU");
                SqlAccessTokenStore.orgShortCode = org;
                errorCodes = obj.UpdateModel(startDate, endDate);
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home Work_Update event completed.", ex);
                throw;
            }
            finally
            {
                var homeObj = new Home();
                HomeService.UpdateModelStatus(userId, errorCodes);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<SampleLinkViewModel> GetSampleLinks()
        {
            try
            {
                var homeObj = new Home();
                return homeObj.GetSampleLinksService();
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetSampleLinks event completed.", ex);
                throw;
            }
        }

        private List<SampleLinkViewModel> GetSampleLinksService()
        {
            try
            {
                var sampleLinks = _clearJellyContext.SampleLinks.Select(x => new SampleLinkViewModel
                {
                    Title = x.Title,
                    DownloadLink = x.DownloadLink,
                    SortOrder = x.SortOrder
                }).ToList();
                return sampleLinks;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetSampleLinksService event completed.", ex);
                throw ex;
            }
        }

        private List<EmailTemplate> GetSampleLinksDashboard()
        {
            try
            {
                var sampleLinks = _clearJellyContext.EmailTemplates.Where(x => x.EmailCategoryId == 6 && x.IsActive == true).ToList().Select(x => new EmailTemplate
                {
                    Subject = x.Subject,
                    Name = x.Name
                }).ToList();
                return sampleLinks;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetSampleLinksService event completed.", ex);

                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<EmailTemplate> GetLinksDashboard()
        {
            try
            {
                var homeObj = new Home();
                return homeObj.GetSampleLinksDashboard();
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetSampleLinks event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static string GetQuickGuide()
        {
            try
            {
                var homeObj = new Home();
                return HomeService.getQuickGuideData();
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + " ~Execution of Home GetQuickGuide event completed.", ex);
                throw ex;
            }
        }

        #region Process Running methods
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static bool IsPRocessRunning()
        {
            try
            {
                var currentPage = new Home();
                var isProcessRunning = currentPage.GetValueProcessRunning();
                return isProcessRunning;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + "~Execution of OnTimedEvent method completed.", ex);
                throw;
            }
        }

        [WebMethod]
        public static bool IsDeletedOrgProcessRunning(string orgName)
        {
            try
            {
                _logger.Info((_user ?? "") + "~Execution of OnTimedEvent method started.");
                var currentPage = new Home();
                var isProcessRunning = currentPage.GetDeleteOrgProcessRunning(orgName);
                return isProcessRunning;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + "~Execution of OnTimedEvent method completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static string IsMessageNotShown()
        {
            try
            {
                var loggedinUser = _userId;
                var loggeinUser = loggedinUser;
                string isMessageSeeen = HomeService.IsMessageNotSeenByUser(loggeinUser);
                return isMessageSeeen;
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + "~Execution of OnTimedEvent method completed.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static void ChangeIsMessageShownFlag()
        {
            try
            {
                _logger.Info((_user ?? "") + "~Execution of ChangeIsMessageShownFlag method started.");
                HomeService.ChangeFlagOfModelProcess();
            }
            catch (Exception ex)
            {
                _logger.Error((_user ?? "") + "~Execution of ChangeIsMessageShownFlag method completed.", ex);
                throw;
            }
        }
        #endregion
    }
}