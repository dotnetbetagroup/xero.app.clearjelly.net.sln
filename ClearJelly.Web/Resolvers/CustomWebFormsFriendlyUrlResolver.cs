﻿using Microsoft.AspNet.FriendlyUrls.Resolvers;

namespace ClearJelly.Web.Resolvers
{
    public class CustomWebFormsFriendlyUrlResolver : WebFormsFriendlyUrlResolver
    {
        public CustomWebFormsFriendlyUrlResolver()
        {

        }

        public override string ConvertToFriendlyUrl(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (path.ToLower().Contains("Admin"))
                {
                    return path;
                }
            }
            return base.ConvertToFriendlyUrl(path);
        }
    }
}