﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="CreateModel.aspx.cs" Inherits="ClearJelly.Web.CreateModel" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
    
        <h1>Xero Testbed</h1>
        <asp:Button ID="cmdSubmit" width="180" runat="server" Text="Connect to Xero" OnClick="cmdConnect_Click" />
        <br /><br />
        <asp:Button runat="server" ID="btnGetAllJournals" width="180" Text="Udate all Journals" OnClick="cmdGetAllJournals" />
        <br /><br />
        <asp:Button runat="server" ID="btnGetAllCreditNotes" width="180" Text="Update Credit Notes" OnClick="cmdGetAllCreditNotes" />
        <br /><br />
        <asp:Button runat="server" ID="btnGetAllContacts" width="180" Text="Update Contacts" OnClick="cmdGetAllContacts" />
        <br /><br />
        <asp:Button runat="server" ID="btnGetCategories" width="180" Text="Update tracking categories" OnClick="cmdGetCategories" />
        <br /><br />
        <asp:Button runat="server" ID="btngetAllInvoices" width="180" Text="Update all invoices" OnClick="cmdgetAllInvoices" />
        <br /><br />
        <asp:Button runat="server" ID="btnUpdateAccounts" width="180" Text="Update the chart of accounts" OnClick="cmdgetAllAccounts" />
        <br /><br />
            
        <asp:Button runat="server" ID="btnUpdateCubes" width="180" Text="Update cubes" OnClick="cmdUpdateCubes" />
        <br /><br />                                
    </div>
        <p>
        
 
        <asp:Label ID="invoiceCreatedLabel" runat="server"/>
 
        </p>
</asp:Content>


