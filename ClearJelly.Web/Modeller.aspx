﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Modeller.aspx.cs" Inherits="Modeller" %>--%>
<%--<%@ Page Title="Modeller Page" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site_Modeller.master" CodeFile="Modeller.aspx.cs" Inherits="Modeller" %>--%>
<%@ Page Title="Modeller Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Modeller.aspx.cs" Inherits="ClearJelly.Web.Modeller" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <script src="Scripts/AgilityScripts/Modeller.js"></script>
    <link href="Scripts/ModellerSPA/css/style.css" rel="stylesheet" />
    <link href="Scripts/ModellerSPA/css/var.css" rel="stylesheet" />
    <link href="Scripts/ModellerSPA/css/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="Scripts/ModellerSPA/css/kendo/kendo.common-material.min.css" rel="stylesheet" />
    <link href="Scripts/ModellerSPA/css/kendo/kendo.material.min.css" rel="stylesheet" />
    <link href="Scripts/ModellerSPA/css/kendo/kendo.material.mobile.min.css" rel="stylesheet" />
    <link href="Scripts/ModellerSPA/css/kendo/kendo.rtl.min.css" rel="stylesheet" />
    <script src="Scripts/ModellerSPA/jquery/dist/jquery.js"></script>
    <script src="Scripts/ModellerSPA/libs/jszipKendo/jszip.min.js"></script>
    <script src="Scripts/ModellerSPA/libs/angular/angular.js"></script>
    <script src="Scripts/ModellerSPA/libs/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="Scripts/ModellerSPA/libs/kendo/kendo.all.min.js"></script>
    <script src="Scripts/ModellerSPA/libs/angular-animate/angular-animate.js"></script>
    <script src="Scripts/ModellerSPA/libs/angular-touch/angular-touch.min.js"></script>
    <script src="Scripts/ModellerSPA/libs/angular-ui-grid/ui-grid.min.js"></script>
    <script src="Scripts/ModellerSPA/app/app.js"></script>
    <script src="Scripts/ModellerSPA/app/controllers/newDimensionController.js"></script>
    <script src="Scripts/ModellerSPA/app/controllers/mainController.js"></script>
    <script src="Scripts/ModellerSPA/app/controllers/dimensionsController.js"></script>
    <script src="Scripts/ModellerSPA/app/controllers/editDimensionController.js"></script>
    <script src="Scripts/ModellerSPA/app/controllers/cubesController.js"></script>
    <script src="Scripts/ModellerSPA/app/controllers/logController.js"></script>
    <script src="Scripts/ModellerSPA/app/services/common/httpHelperService.js"></script>
    <script src="Scripts/ModellerSPA/app/services/common/uiService.js"></script>
    <script src="Scripts/ModellerSPA/app/services/cubeService.js"></script>
    <script src="Scripts/ModellerSPA/app/services/dimensionService.js"></script>
    <script src="Scripts/ModellerSPA/app/services/logService.js"></script>
    <script src="Scripts/PivotGrid/dx.pivotgrid.js"></script>
    <script src="Scripts/PivotGrid/dx.pivotgrid.min.js"></script>
    <script src="Scripts/PivotGrid/GeneratePivotGrid.js"></script>
    <script src="Scripts/xml2json.js"></script>

    <asp:HiddenField ID="userName" runat="server" />
    <asp:HiddenField ID="userPassword" runat="server" />
    <asp:HiddenField ID="userDbName" runat="server" />
    <asp:HiddenField ID="userConnection" runat="server" />
    <asp:HiddenField ID="ApiUrl" runat="server" />
    <asp:HiddenField ID="DBApiUrl" runat="server" />
    <div ng-app="app" ng-controller="mainController as vm" ng-cloak>
        <script>
            window.ApiUrl = angular.element(document.getElementById("MainContent_ApiUrl")).val();
            window.DBApiUrl = angular.element(document.getElementById("MainContent_DBApiUrl")).val();
        </script>
        <div class="main-part">
            <div id="sidebar" class="sidebar-page k-content">

                <nav class="navbar navbar-default">
                    <div class="form-group clearfix">
                        <label for="sel1">Connection:</label>
                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control pull-left" ng-model="vm.userConnectionString" ng-change="vm.updateConnection()"></asp:DropDownList>

                        <div class="btn-group pull-right">
                            <a href="#"><i class="fa fa-power-off fa-lg" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-database fa-lg" aria-hidden="true"></i></a>
                        </div>

                    </div>
                    <button type="button" class="navbar-toggle opensidebar pull-right" ng-click="vm.uiService.toggleSidePanel($event)">
                        <span class="sr-only">Toggle navigation</span>
                        <i id="toggleSidebarIcon" class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                    </button>
                    <div>
                        <div id="navbar">
                            <div kendo-tree-view="tree"
                                k-data-source="vm.dimensionsTreeData"
                                k-on-change="selectedItem = dataItem" style="width: 300px; padding-left: 10px;">
                                <span k-template>
                                    <i class="fa fa-arrows-alt" aria-hidden="true" ng-show="dataItem.text == 'Dimensions'"></i>
                                    <i class="fa fa-expand" aria-hidden="true" ng-show="dataItem.text != 'Dimensions'"></i>
                                    <span ng-click="vm.selectDimension(dataItem.text);">{{dataItem.text}}</span>
                                    
                                </span>

                            </div>

                            <div kendo-tree-view="tree"
                                k-data-source="vm.cubesTreeData"
                                k-on-change="selectedItem = dataItem" style="width: 300px; padding-left: 10px;">
                                <span k-template>
                                    <i class="fa fa-cubes" aria-hidden="true" ng-show="dataItem.text == 'Cubes'"></i>
                                    <i class="fa fa-cube" aria-hidden="true" ng-show="dataItem.text != 'Cubes'"></i>
                                    <span ng-click="vm.selectCube(dataItem.text);">{{dataItem.text}}</span>
                                    <%--<button class='k-button pull-right add-cube' kendo-tooltip k-content="'Create cube'" style="padding: 1px 3px;" ng-click="vm.openNewCubeWindow($event)" ng-show="dataItem.text == 'Cubes'">

                                        <i class="fa fa-plus" aria-hidden="true"></i>

                                    </button>
                                    <button class='k-button pull-right remove-cube' kendo-tooltip k-content="'Remove cube'" style="padding: 1px 3px;" ng-click="vm.openDeleteCubeWindow($event, dataItem.text)" ng-show="dataItem.text != 'Cubes'">

                                        <i class="fa fa-minus" aria-hidden="true"></i>

                                    </button>--%>
                                </span>
                            </div>
                            <ul>
                                <%-- <li class="k-item k-first k-last ref-pivot">
                                    <a class="pivot" ng-click="vm.openPivotWindow()" href=""><i class="fa fa-keyboard-o" aria-hidden="true"></i>Pivot</a>
                                </li>--%>
                                <li class="k-item k-first k-last ref-pivot">
                                    <a class="pivot" ng-click="vm.$state.go('logs');" href=""><i class="fa fa-calendar" aria-hidden="true"></i>Log</a>
                                </li>
                            </ul>

                        </div>
                        <!--/.nav-collapse -->
                    </div>
                    <!--/.container-fluid -->
                </nav>

            </div>

            <%--<input id="pivot" hidden/>--%>
            <div id="view" class="wrapper clearfix" ui-view></div>
        </div>

<%--        <div class="o-popup" kendo-window="vm.createDimensionWindow"
            k-title="'Create New Dimension'"
            k-width="400"
            k-modal="true"
            k-visible="false" id="createDimensionWindow">
            <div style="margin-bottom: 15px;">
                <div class="form-group">
                    <label>Name: </label>
                    <input name="createDimInput" type="text" class="form-control" ng-model="vm.dimensionForCreate.Name" />
                    <p style="color: red; padding-top: 5px;" ng-show="vm.errorNoName">Please add new dimension's name</p>
                    <p style="color: red; padding-top: 5px;" ng-show="vm.errorInvalidDimensionName">Special characters are not allowed in dimension's name except for '_', '-' and '@'</p>
                </div>

                <div class="form-group">
                    <label>Columns: </label>
                    <input type="text" name="createDimInput" class="form-control" id="dimColumn" ng-model="vm.dimensionForCreate.Elements" />

                </div>
                <p>Enter column name separated by commas</p>

                <p>OR</p>
                <p>just paste data from Excel</p>
                <textarea name="createDimExcelData" style="width: 360px; height: 200px;"></textarea>
                <p style="color: red; padding-top: 5px;" ng-show="vm.errorNoDimParams">Please add dimension's elements or paste data from Excel</p>
            </div>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.createDimension()" class="btn btn-default btn-add">Create</button>
                <button type="reset" ng-click="vm.createDimensionWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.createCubeWindow"
            k-title="'Create New Cube'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <div style="margin-bottom: 15px;">
                <div class="form-group clearfix">
                    <label class="col-form-label">Name:</label>
                    <input type="text" id="cubeName" class="form-control" ng-model="vm.cubeForCreate.Name" />
                </div>
                <div class="form-group clearfix" ng-keydown="vm.uiService.signal($event)">
                    <label>Dimensions:</label>
                    <select kendo-multi-select k-options="vm.selectDimensionsOptions" k-ng-model="vm.dimensionsForCubeCreate" id="multiselect"></select>
                </div>
                <span ng-show="vm.isFieldsFilled" class="important">*All fields must be filled</span>
                <p style="color: red; padding-top: 5px;" ng-show="vm.errorInvalidCubeName">Special characters are not allowed in cube's name except for '_', '-' and '@'</p>
            </div>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.createCube()" class="btn btn-default btn-add">Create</button>
                <button type="reset" ng-click="vm.createCubeWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
            </div>
        </div>--%>

        <div class="o-popup" kendo-window="vm.errorDeleteDimensionWindow"
            k-title="'Are you sure you want to delete this?'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <div>
                <p><b>This dimension exists in the following cubes:</b> </p>
            </div>
            <p>{{vm.cubesHaveThisDim}} </p>
            <p></p>
            <div class="btn-holder" style="text-align: center;">
                <button type="reset" ng-click="vm.errorDeleteDimensionWindow.close();" class="btn btn-default btn-add">Ok</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.deleteDimensionWindow"
            k-title="'Delete Dimension?'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.dimensionForDelete}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.deleteDimension()" class="btn btn-default btn-delete">Delete</button>
                <button type="reset" ng-click="vm.deleteDimensionWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.deleteCubeWindow"
            k-title="'Delete Cube?'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.cubeForDelete}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.deleteCube()" class="btn btn-default btn-delete">Delete</button>
                <button type="reset" ng-click="vm.deleteCubeWindow.close();" class="btn btn-default btn-cancel">Cancel</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.createDimensionSuccessWindow"
            k-title="'Dimension was created successfully'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.dimensionForCreate.Name}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.createDimensionSuccessWindow.close();vm.closeCreateSuccessWindow();" class="btn btn-default btn-add">Ok</button>
            </div>
        </div>

        <div kendo-window="vm.popupDuplicateDimensionNameWindow"
            k-title="'Dimension with that name already exist'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.dimensionForCreate.Name}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.popupDuplicateDimensionNameWindow.close();vm.closeDuplicateDimensionNameWindow();" class="btn btn-default btn-add">Ok</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.deleteDimensionSuccessWindow"
            k-title="'Dimension was deleted successfully'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.dimensionForDelete}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.closeDeleteDimensionSuccessWindow()" class="btn btn-default btn-add">Ok</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.createCubeSuccessWindow"
            k-title="'Cube was created successfully'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.cubeForCreate.Name}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.createCubeSuccessWindow.close();vm.closeCreateSuccessWindow();" class="btn btn-default btn-add">Ok</button>
            </div>
        </div>

        <div class="o-popup" kendo-window="vm.deleteCubeSuccessWindow"
            k-title="'Cube was deleted successfully'"
            k-width="400"
            k-modal="true"
            k-visible="false">
            <label>Name: {{vm.cubeForDelete}}</label>
            <div class="btn-holder" style="text-align: center;">
                <button type="submit" ng-click="vm.deleteCubeSuccessWindow.close();vm.closeDeleteCubeSuccessWindow();" class="btn btn-default btn-add">Ok</button>
            </div>
        </div>
    </div>
</asp:Content>
