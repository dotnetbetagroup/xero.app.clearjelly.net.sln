﻿extern alias DevLocal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Logging;
using DevDefined.OAuth.Storage.Basic;
using XeroApi;
using XeroApi.Model;
using XeroApi.OAuth;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using NLog;

namespace ClearJelly.Web
{
    public partial class CreateModel : Page
    {

        private const String UserAgent = "ClearJelly Xero Addon";
        private const String ConsumerKey = "XGLPD4WPTWDBEVSXBH02PKFEIZZAAE";
        private const String ConsumerSecret = "XEK3RVIESATTGV1BAE8YGBHAAOAC4T";
        private static string strConn2 = ";user id=Dev_User;" +
                                         "password=dev@2017;" +
                                         "server=MANADEV;";

        Logger _logger = LogManager.GetCurrentClassLogger();

        protected void Page_Load(object sender, EventArgs e)
        {

            //Step3. When loading the page, if the page was loaded from callback, the Request contains an oauth_verifier param. 
            if (Session["login"] != "valid" && Session["Jelly_user"] == null)
            {
                Response.Redirect("~/Account/Login.aspx");
            }
            if (Request.Params["oauth_verifier"] != null)
            {
                //We need to finish the authorisation process by exchanging our RequestToken for an AccessToken 
                FinishAuthorisation(Request.Params["oauth_verifier"]);
            }

            strConn2 += "database=Xero_" + Session["Jelly_user"] + ";";
        }

        protected void cmdConnect_Click(object sender, EventArgs e)
        {
            //clear_table("PnL");
            IOAuthSession consumerSession = new XeroApiPublicSession(UserAgent, ConsumerKey, ConsumerSecret, new InMemoryTokenRepository());

            consumerSession.MessageLogger = new DebugMessageLogger();

            //Step 1. Get a Request Token

            var callbackUri = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port, "Default.aspx");
            RequestToken requestToken = consumerSession.GetRequestToken(callbackUri.Uri);


            //Step 2. Get the user to log into Xero using the request Token and redirect to the authorisation URL
            var authorisationUrl = consumerSession.GetUserAuthorizationUrl();
            Session["ConsumerSession"] = consumerSession;

            Response.Redirect(authorisationUrl);
        }

        protected void FinishAuthorisation(string verifier)
        {


            var consumerSession = (IOAuthSession)Session["ConsumerSession"];

            try
            {
                //Exchange the RequestToken for an AccessToken using the verifier code from the callback url
                AccessToken accessToken = consumerSession.ExchangeRequestTokenForAccessToken(verifier);

            }
            catch (OAuthException)
            {
                throw;
            }

            //create a repository with the authenticated session
            var repository = new Repository(consumerSession);

            Session["repository"] = repository;

            Response.Redirect("~/");

        }

        /* public void CreateInvoice(object sender, EventArgs e)
         {


             //retrieve the repository from the session state
             var repository = (Repository)Session["repository"];


             //if the repository is null, the user must authenticate
             if (repository == null)
             {
                 this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                 return;
             }
             GetAllJournalsWithPagination(repository);

             //Use the API wrapper to create an invoice
             var invoice = repository.Create(
                 new Invoice
                 {
                     Type = "ACCREC",
                     Contact = new Contact { Name = "Bob" },
                     Date = DateTime.Today,
                     DueDate = DateTime.Today.AddDays(14),
                     Status = "DRAFT",
                     LineItems = new LineItems
                         {
                             new LineItem
                             {
                                 Description = "Services Rendered",
                                 Quantity = 1,
                                 UnitAmount = 1,
                             }
                         }
                 });


             //write the details for the invoice in the label on the home page
             this.invoiceCreatedLabel.Text = String.Format("Invoice {0} was raised against {1} on {2} for {3}{4}",
                 invoice.InvoiceNumber,
                 invoice.Contact.Name, invoice.Date, invoice.Total,
                 invoice.CurrencyCode);
         }*/

        public void cmdGetAllJournals(object sender, EventArgs e)
        {
            try
            {
                //retrieve the repository from the session state
                var repository = (Repository)Session["repository"];


                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
                GetAllJournalsWithPagination(repository);
                this.invoiceCreatedLabel.Text = "All journals have been updated!";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        public void cmdgetAllInvoices(object sender, EventArgs e)
        {
            try
            {
                //retrieve the repository from the session state
                var repository = (Repository)Session["repository"];


                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
                GetAllInvoicesWithPagination(repository);
                this.invoiceCreatedLabel.Text = "All invoices have been updated!";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        public void cmdGetAllCreditNotes(object sender, EventArgs e)
        {
            try
            {
                //retrieve the repository from the session state
                var repository = (Repository)Session["repository"];


                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
                GetAllCreditNotesWithPagination(repository);
                this.invoiceCreatedLabel.Text = "All credit notes have been updated!";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        public void cmdGetAllContacts(object sender, EventArgs e)
        {
            try
            {
                //retrieve the repository from the session state
                var repository = (Repository)Session["repository"];


                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
                GetAllContacts(repository);
                this.invoiceCreatedLabel.Text = "All contacts have been updated!";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        public void cmdgetAllAccounts(object sender, EventArgs e)
        {
            try
            {
                //retrieve the repository from the session state
                var repository = (Repository)Session["repository"];


                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
                GetAccounts(repository);
                this.invoiceCreatedLabel.Text = "All account codes have been updated!";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        public void cmdGetCategories(object sender, EventArgs e)
        {
            try
            {
                //retrieve the repository from the session state
                var repository = (Repository)Session["repository"];


                //if the repository is null, the user must authenticate
                if (repository == null)
                {
                    this.invoiceCreatedLabel.Text = "There is no repository. You have not Authorized yet";
                    return;
                }
                getTrackingCategories(repository);
                this.invoiceCreatedLabel.Text = "All categories have been updated!";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        protected void GetAllJournalsWithPagination(Repository repository)
        {
            try
            {        // Get all journals from the general ledger using the ?offset=xxx parameter
                List<Journal> allJournals = new List<Journal>();
                List<Journal> batchOfJournals;
                int skip = 290;
                string sqlStr = "";
                DataTable entries = new DataTable("Journals");

                DataColumn JID = new DataColumn();
                JID.DataType = System.Type.GetType("System.String");
                JID.ColumnName = "JID";
                entries.Columns.Add(JID);


                //DataColumn Reference = new DataColumn();
                //Reference.DataType = System.Type.GetType("System.String");
                //Reference.ColumnName = "Reference";
                //entries.Columns.Add(Reference);


                DataColumn JNumber = new DataColumn();
                JNumber.DataType = System.Type.GetType("System.String");
                JNumber.ColumnName = "JNumber";
                entries.Columns.Add(JNumber);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.String");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn CreatedDateUTC = new DataColumn();
                CreatedDateUTC.DataType = System.Type.GetType("System.String");
                CreatedDateUTC.ColumnName = "CreatedDateUTC";
                entries.Columns.Add(CreatedDateUTC);


                DataColumn AccountName = new DataColumn();
                AccountName.DataType = System.Type.GetType("System.String");
                AccountName.ColumnName = "AccountName";
                entries.Columns.Add(AccountName);

                DataColumn AccountCode = new DataColumn();
                AccountCode.DataType = System.Type.GetType("System.String");
                AccountCode.ColumnName = "AccountCode";
                entries.Columns.Add(AccountCode);

                DataColumn AccountType = new DataColumn();
                AccountType.DataType = System.Type.GetType("System.String");
                AccountType.ColumnName = "AccountType";
                entries.Columns.Add(AccountType);

                DataColumn NetAmt = new DataColumn();
                NetAmt.DataType = System.Type.GetType("System.String");
                NetAmt.ColumnName = "NetAmt";
                entries.Columns.Add(NetAmt);

                DataColumn GrossAmt = new DataColumn();
                GrossAmt.DataType = System.Type.GetType("System.String");
                GrossAmt.ColumnName = "GrossAmt";
                entries.Columns.Add(GrossAmt);

                DataColumn ValidationErrors = new DataColumn();
                ValidationErrors.DataType = System.Type.GetType("System.String");
                ValidationErrors.ColumnName = "ValidationErrors";
                entries.Columns.Add(ValidationErrors);

                DataColumn ValidationStatus = new DataColumn();
                ValidationStatus.DataType = System.Type.GetType("System.String");
                ValidationStatus.ColumnName = "ValidationStatus";
                entries.Columns.Add(ValidationStatus);

                DataColumn Warnings = new DataColumn();
                Warnings.DataType = System.Type.GetType("System.String");
                Warnings.ColumnName = "Warnings";
                entries.Columns.Add(Warnings);

                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn TrackingCategory1 = new DataColumn();
                TrackingCategory1.DataType = System.Type.GetType("System.String");
                TrackingCategory1.ColumnName = "TrackingCategory1";
                entries.Columns.Add(TrackingCategory1);

                DataColumn TrackingCategory1_Option = new DataColumn();
                TrackingCategory1_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory1_Option.ColumnName = "TrackingCategory1_Option";
                entries.Columns.Add(TrackingCategory1_Option);

                DataColumn TrackingCategory2 = new DataColumn();
                TrackingCategory2.DataType = System.Type.GetType("System.String");
                TrackingCategory2.ColumnName = "TrackingCategory2";
                entries.Columns.Add(TrackingCategory2);

                DataColumn TrackingCategory2_Option = new DataColumn();
                TrackingCategory2_Option.DataType = System.Type.GetType("System.String");
                TrackingCategory2_Option.ColumnName = "TrackingCategory2_Option";
                entries.Columns.Add(TrackingCategory2_Option);

                DataColumn JournalLines = new DataColumn();
                JournalLines.DataType = System.Type.GetType("System.String");
                JournalLines.ColumnName = "JournalLines";
                entries.Columns.Add(JournalLines);

                allJournals.Clear();
                Journal jTmp = repository.Journals.Skip(skip).ToList().First();
                skip = Convert.ToInt32(jTmp.JournalNumber) - 1;

                while ((batchOfJournals = repository.Journals.Skip(skip).ToList()).Count > 0)
                {
                    Console.WriteLine("Fetched {0} journals from API using skip={1}", batchOfJournals.Count, skip);


                    allJournals.AddRange(batchOfJournals);
                    skip += (batchOfJournals.Count);
                    batchOfJournals.Clear();
                }

                //for(int i=0;i<300;i++)
                //{
                //    Journal J = repository.Journals.Where(;
                //    if(J.JournalLines.Any())
                //    {
                //        allJournals.Add(J);
                //    }

                //    skip += (i*100);
                //}
                Console.WriteLine(allJournals.Count());
                int x = 0;

                entries.Clear();
                if (allJournals.Any())
                {

                    foreach (Journal item in allJournals)
                    {

                        //item.JournalLines.Find()
                        foreach (JournalLine line in item.JournalLines)
                        {

                            DataRow row = entries.NewRow();
                            row["JID"] = item.JournalID;
                            row["Reference"] = item.Reference;
                            row["JNumber"] = item.JournalNumber;
                            row["Date"] = item.JournalDate;
                            row["CreatedDateUTC"] = item.CreatedDateUTC;
                            row["AccountName"] = line.AccountName;
                            row["AccountCode"] = line.AccountCode;
                            row["AccountType"] = line.AccountType;
                            row["NetAmt"] = line.NetAmount.ToString();
                            row["GrossAmt"] = line.GrossAmount;

                            if (line.ValidationErrors.Any())
                            {
                                row["ValidationErrors"] = line.ValidationErrors.First().Message;
                            }
                            row["ValidationStatus"] = line.ValidationStatus;
                            if (line.Warnings.Any())
                            {
                                row["Warnings"] = line.Warnings.First().Message;
                            }
                            row["Description"] = line.Description;

                            if (line.TrackingCategories.Count == 1)
                            {
                                row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                row["TrackingCategory1_option"] = line.TrackingCategories[0].Option;
                            }
                            else
                            {
                                row["TrackingCategory1"] = null;
                                row["TrackingCategory1_option"] = null;
                                row["TrackingCategory2"] = null;
                                row["TrackingCategory2_Option"] = null;
                            }

                            if (line.TrackingCategories.Count == 2)
                            {
                                row["TrackingCategory1"] = line.TrackingCategories[0].Name;
                                row["TrackingCategory1_option"] = line.TrackingCategories[0].Option;
                                row["TrackingCategory2"] = line.TrackingCategories[2].Name;
                                row["TrackingCategory2_Option"] = line.TrackingCategories[2].Option;
                            }
                            row["JournalLines"] = item.JournalLines.Count;
                            //else
                            //{

                            //    row["TrackingCategories"] = "None";
                            //}
                            entries.Rows.Add(row);
                            x++;
                        }

                    }


                    Console.WriteLine(
                        "There are {0} journals in the general ledger, starting with #{1} and ending with #{2}",
                        allJournals.Count, allJournals.First().JournalNumber, allJournals.Last().JournalNumber);
                    Console.WriteLine(x + "=" + entries.Rows.Count);

                }
                else
                {
                    Console.WriteLine("There are no journals in the general ledger");
                }

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
                {

                    bulkCopy.DestinationTableName =
                        "Journals";

                    // Number of records to be processed in one go
                    //sbc.BatchSize = batchSize;

                    // Add your column mappings here
                    bulkCopy.ColumnMappings.Add("JID", "JID");
                    bulkCopy.ColumnMappings.Add("Reference", "Reference");
                    bulkCopy.ColumnMappings.Add("JNumber", "JNumber");
                    bulkCopy.ColumnMappings.Add("Date", "Date");
                    bulkCopy.ColumnMappings.Add("CreatedDateUTC", "CreatedDateUTC");
                    bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
                    bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
                    bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
                    bulkCopy.ColumnMappings.Add("NetAmt", "NetAmt");
                    bulkCopy.ColumnMappings.Add("GrossAmt", "GrossAmt");
                    bulkCopy.ColumnMappings.Add("ValidationErrors", "ValidationErrors");
                    bulkCopy.ColumnMappings.Add("ValidationStatus", "ValidationStatus");
                    bulkCopy.ColumnMappings.Add("Warnings", "Warnings");
                    bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
                    bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
                    bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
                    //bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
                    bulkCopy.ColumnMappings.Add("JournalLines", "JournalLines");


                    try
                    {
                        Console.WriteLine("Copying to the relational table...");

                        // Write from the source to the destination.
                        clear_table("Journals");
                        bulkCopy.WriteToServer(entries);

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        protected void GetAccounts(Repository repository)
        {
            try
            {
                // API v2.15 Get a list of accounts that can be used when creating expense claims
                var Accounts = repository.Accounts.ToList();
                DataTable entries = new DataTable("ChartOfAccounts");


                DataColumn AccountID = new DataColumn();
                AccountID.DataType = System.Type.GetType("System.String");
                AccountID.ColumnName = "AccountID";
                entries.Columns.Add(AccountID);

                DataColumn BankAccountNumber = new DataColumn();
                BankAccountNumber.DataType = System.Type.GetType("System.String");
                BankAccountNumber.ColumnName = "BankAccountNumber";
                entries.Columns.Add(BankAccountNumber);

                DataColumn Class = new DataColumn();
                Class.DataType = System.Type.GetType("System.String");
                Class.ColumnName = "Class";
                entries.Columns.Add(Class);

                DataColumn Code = new DataColumn();
                Code.DataType = System.Type.GetType("System.String");
                Code.ColumnName = "Code";
                entries.Columns.Add(Code);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                entries.Columns.Add(CurrencyCode);

                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn EnablePaymentsToAccount = new DataColumn();
                EnablePaymentsToAccount.DataType = System.Type.GetType("System.String");
                EnablePaymentsToAccount.ColumnName = "EnablePaymentsToAccount";
                entries.Columns.Add(EnablePaymentsToAccount);

                DataColumn Name = new DataColumn();
                Name.DataType = System.Type.GetType("System.String");
                Name.ColumnName = "Name";
                entries.Columns.Add(Name);

                DataColumn ReportingCode = new DataColumn();
                ReportingCode.DataType = System.Type.GetType("System.String");
                ReportingCode.ColumnName = "ReportingCode";
                entries.Columns.Add(ReportingCode);

                DataColumn ReportingCodeName = new DataColumn();
                ReportingCodeName.DataType = System.Type.GetType("System.String");
                ReportingCodeName.ColumnName = "ReportingCodeName";
                entries.Columns.Add(ReportingCodeName);

                DataColumn ShowInExpenseClaims = new DataColumn();
                ShowInExpenseClaims.DataType = System.Type.GetType("System.String");
                ShowInExpenseClaims.ColumnName = "ShowInExpenseClaims";
                entries.Columns.Add(ShowInExpenseClaims);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn SystemAccount = new DataColumn();
                SystemAccount.DataType = System.Type.GetType("System.String");
                SystemAccount.ColumnName = "SystemAccount";
                entries.Columns.Add(SystemAccount);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                entries.Columns.Add(Type);




                foreach (var account in Accounts)
                {
                    DataRow row = entries.NewRow();

                    row["AccountID"] = account.AccountID;
                    row["BankAccountNumber"] = account.BankAccountNumber;
                    row["Class"] = account.Class;
                    row["Code"] = account.Code;
                    row["CurrencyCode"] = account.CurrencyCode;
                    row["Description"] = account.Description;
                    row["EnablePaymentsToAccount"] = account.EnablePaymentsToAccount;
                    row["Name"] = account.Name;
                    row["ReportingCode"] = account.ReportingCode;
                    row["ReportingCodeName"] = account.ReportingCodeName;
                    row["ShowInExpenseClaims"] = account.ShowInExpenseClaims;
                    row["Status"] = account.Status;
                    row["SystemAccount"] = account.SystemAccount;
                    row["Type"] = account.Type;

                    entries.Rows.Add(row);
                }
                clear_table("ChartOfAccounts");
                // insert into sql table
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
                {
                    bulkCopy.DestinationTableName =
                        "ChartOfAccounts";
                    try
                    {
                        //Console.WriteLine("Copying to the relational table...");
                        bulkCopy.WriteToServer(entries);
                    }
                    catch (SqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        protected void GetAllInvoicesWithPagination(Repository repository)
        {
            try
            {
                // Get all journals from the general ledger using the ?offset=xxx parameter
                List<Invoice> allInvoices = new List<Invoice>();
                List<Invoice> batchOfInvoices;
                List<String> IDs = new List<String>();
                int skip = 0;
                string sqlStr = "";
                DataTable entries = new DataTable("Invoices");


                DataColumn AmountCredited = new DataColumn();
                AmountCredited.DataType = System.Type.GetType("System.String");
                AmountCredited.ColumnName = "AmountCredited";
                entries.Columns.Add(AmountCredited);

                DataColumn AmountDue = new DataColumn();
                AmountDue.DataType = System.Type.GetType("System.String");
                AmountDue.ColumnName = "AmountDue";
                entries.Columns.Add(AmountDue);

                DataColumn AmountPaid = new DataColumn();
                AmountPaid.DataType = System.Type.GetType("System.String");
                AmountPaid.ColumnName = "AmountPaid";
                entries.Columns.Add(AmountPaid);

                DataColumn BrandingThemeID = new DataColumn();
                BrandingThemeID.DataType = System.Type.GetType("System.String");
                BrandingThemeID.ColumnName = "BrandingThemeID";
                entries.Columns.Add(BrandingThemeID);

                DataColumn Contact = new DataColumn();
                Contact.DataType = System.Type.GetType("System.String");
                Contact.ColumnName = "Contact";
                entries.Columns.Add(Contact);

                DataColumn CreditNotes = new DataColumn();
                CreditNotes.DataType = System.Type.GetType("System.String");
                CreditNotes.ColumnName = "CreditNotes";
                entries.Columns.Add(CreditNotes);

                DataColumn CurrencyCode = new DataColumn();
                CurrencyCode.DataType = System.Type.GetType("System.String");
                CurrencyCode.ColumnName = "CurrencyCode";
                entries.Columns.Add(CurrencyCode);

                DataColumn CurrencyRate = new DataColumn();
                CurrencyRate.DataType = System.Type.GetType("System.String");
                CurrencyRate.ColumnName = "CurrencyRate";
                entries.Columns.Add(CurrencyRate);

                DataColumn Date = new DataColumn();
                Date.DataType = System.Type.GetType("System.String");
                Date.ColumnName = "Date";
                entries.Columns.Add(Date);

                DataColumn DueDate = new DataColumn();
                DueDate.DataType = System.Type.GetType("System.String");
                DueDate.ColumnName = "DueDate";
                entries.Columns.Add(DueDate);

                DataColumn ExpectedPaymentDate = new DataColumn();
                ExpectedPaymentDate.DataType = System.Type.GetType("System.String");
                ExpectedPaymentDate.ColumnName = "ExpectedPaymentDate";
                entries.Columns.Add(ExpectedPaymentDate);

                DataColumn ExternalLinkProviderName = new DataColumn();
                ExternalLinkProviderName.DataType = System.Type.GetType("System.String");
                ExternalLinkProviderName.ColumnName = "ExternalLinkProviderName";
                entries.Columns.Add(ExternalLinkProviderName);

                DataColumn FullyPaidOnDate = new DataColumn();
                FullyPaidOnDate.DataType = System.Type.GetType("System.String");
                FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
                entries.Columns.Add(FullyPaidOnDate);

                DataColumn HasAttachments = new DataColumn();
                HasAttachments.DataType = System.Type.GetType("System.String");
                HasAttachments.ColumnName = "HasAttachments";
                entries.Columns.Add(HasAttachments);

                DataColumn InvoiceID = new DataColumn();
                InvoiceID.DataType = System.Type.GetType("System.String");
                InvoiceID.ColumnName = "InvoiceID";
                entries.Columns.Add(InvoiceID);

                DataColumn InvoiceNumber = new DataColumn();
                InvoiceNumber.DataType = System.Type.GetType("System.String");
                InvoiceNumber.ColumnName = "InvoiceNumber";
                entries.Columns.Add(InvoiceNumber);

                DataColumn LineAmountTypes = new DataColumn();
                LineAmountTypes.DataType = System.Type.GetType("System.String");
                LineAmountTypes.ColumnName = "LineAmountTypes";
                entries.Columns.Add(LineAmountTypes);

                DataColumn LineItemsCount = new DataColumn();
                LineItemsCount.DataType = System.Type.GetType("System.String");
                LineItemsCount.ColumnName = "LineItemsCount";
                entries.Columns.Add(LineItemsCount);

                DataColumn AccountCode = new DataColumn();
                AccountCode.DataType = System.Type.GetType("System.String");
                AccountCode.ColumnName = "AccountCode";
                entries.Columns.Add(AccountCode);

                DataColumn Description = new DataColumn();
                Description.DataType = System.Type.GetType("System.String");
                Description.ColumnName = "Description";
                entries.Columns.Add(Description);

                DataColumn DiscountRate = new DataColumn();
                DiscountRate.DataType = System.Type.GetType("System.String");
                DiscountRate.ColumnName = "DiscountRate";
                entries.Columns.Add(DiscountRate);


                DataColumn ItemCode = new DataColumn();
                ItemCode.DataType = System.Type.GetType("System.String");
                ItemCode.ColumnName = "ItemCode";
                entries.Columns.Add(ItemCode);

                DataColumn LineAmount = new DataColumn();
                LineAmount.DataType = System.Type.GetType("System.String");
                LineAmount.ColumnName = "LineAmount";
                entries.Columns.Add(LineAmount);

                DataColumn Quantity = new DataColumn();
                Quantity.DataType = System.Type.GetType("System.String");
                Quantity.ColumnName = "Quantity";
                entries.Columns.Add(Quantity);

                DataColumn TaxAmount = new DataColumn();
                TaxAmount.DataType = System.Type.GetType("System.String");
                TaxAmount.ColumnName = "TaxAmount";
                entries.Columns.Add(TaxAmount);

                DataColumn TaxType = new DataColumn();
                TaxType.DataType = System.Type.GetType("System.String");
                TaxType.ColumnName = "TaxType";
                entries.Columns.Add(TaxType);

                DataColumn UnitAmount = new DataColumn();
                UnitAmount.DataType = System.Type.GetType("System.String");
                UnitAmount.ColumnName = "UnitAmount";
                entries.Columns.Add(UnitAmount);

                DataColumn Payments = new DataColumn();
                Payments.DataType = System.Type.GetType("System.String");
                Payments.ColumnName = "Payments";
                entries.Columns.Add(Payments);

                DataColumn PlannedPaymentDate = new DataColumn();
                PlannedPaymentDate.DataType = System.Type.GetType("System.String");
                PlannedPaymentDate.ColumnName = "PlannedPaymentDate";
                entries.Columns.Add(PlannedPaymentDate);

                DataColumn Reference = new DataColumn();
                Reference.DataType = System.Type.GetType("System.String");
                Reference.ColumnName = "Reference";
                entries.Columns.Add(Reference);

                DataColumn SentToContact = new DataColumn();
                SentToContact.DataType = System.Type.GetType("System.String");
                SentToContact.ColumnName = "SentToContact";
                entries.Columns.Add(SentToContact);

                DataColumn Status = new DataColumn();
                Status.DataType = System.Type.GetType("System.String");
                Status.ColumnName = "Status";
                entries.Columns.Add(Status);

                DataColumn SubTotal = new DataColumn();
                SubTotal.DataType = System.Type.GetType("System.String");
                SubTotal.ColumnName = "SubTotal";
                entries.Columns.Add(SubTotal);

                DataColumn Total = new DataColumn();
                Total.DataType = System.Type.GetType("System.String");
                Total.ColumnName = "Total";
                entries.Columns.Add(Total);

                DataColumn TotalDiscount = new DataColumn();
                TotalDiscount.DataType = System.Type.GetType("System.String");
                TotalDiscount.ColumnName = "TotalDiscount";
                entries.Columns.Add(TotalDiscount);

                DataColumn TotalTax = new DataColumn();
                TotalTax.DataType = System.Type.GetType("System.String");
                TotalTax.ColumnName = "TotalTax";
                entries.Columns.Add(TotalTax);

                DataColumn Type = new DataColumn();
                Type.DataType = System.Type.GetType("System.String");
                Type.ColumnName = "Type";
                entries.Columns.Add(Type);

                DataColumn UpdatedDateUTC = new DataColumn();
                UpdatedDateUTC.DataType = System.Type.GetType("System.String");
                UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
                entries.Columns.Add(UpdatedDateUTC);

                DataColumn Url = new DataColumn();
                Url.DataType = System.Type.GetType("System.String");
                Url.ColumnName = "Url";
                entries.Columns.Add(Url);

                allInvoices.Clear();
                // repository.Invoices.OrderBy
                // Invoice jTmp = repository.Invoices.ToList().First();
                // skip = Convert.ToInt32(jTmp.InvoiceNumber) - 1;
                skip = 0;
                List<Guid> guids;
                batchOfInvoices = repository.Invoices.ToList();
                while (batchOfInvoices.Any())
                {
                    //Console.WriteLine("Fetched {0} Invoices from API using skip={1}", batchOfInvoices.Count, skip);
                    //Console.WriteLine(Convert.ToInt32(jTmp.InvoiceNumber)+"Test " + repository.Invoices.Skip(skip).Count());

                    allInvoices.AddRange(batchOfInvoices);
                    batchOfInvoices = repository.Invoices.OrderBy(c => c.InvoiceID).Skip(skip).ToList();
                    skip += 100;
                    batchOfInvoices.Clear();
                }

                //Console.WriteLine(allInvoices.Count());
                int x = 0;

                entries.Clear();
                if (allInvoices.Any())
                {

                    foreach (Invoice item in allInvoices)
                    {
                        IDs.Add(item.InvoiceID.ToString());
                        DataRow row = entries.NewRow();
                        row["AmountCredited"] = item.AmountCredited;
                        row["AmountDue"] = item.AmountDue;
                        row["AmountPaid"] = item.AmountPaid;
                        row["BrandingThemeID"] = item.BrandingThemeID;
                        row["Contact"] = item.Contact.ContactID;
                        row["CreditNotes"] = item.CreditNotes.Count.ToString();
                        row["CurrencyCode"] = item.CurrencyCode;
                        row["CurrencyRate"] = item.CurrencyRate;
                        row["Date"] = item.Date;
                        row["DueDate"] = item.DueDate;
                        //                    row["ExpectedPaymentDate"] = item.ExpectedPaymentDate;
                        row["ExternalLinkProviderName"] = item.ExternalLinkProviderName;
                        row["FullyPaidOnDate"] = item.FullyPaidOnDate;
                        row["HasAttachments"] = item.HasAttachments;
                        row["InvoiceID"] = item.InvoiceID;
                        row["InvoiceNumber"] = item.InvoiceNumber;
                        row["LineAmountTypes"] = item.LineAmountTypes;
                        row["LineItemsCount"] = item.LineItems.Count.ToString();
                        row["AccountCode"] = null;
                        row["Description"] = null;
                        row["DiscountRate"] = null;
                        row["ItemCode"] = null;
                        row["LineAmount"] = null;
                        row["Quantity"] = null;
                        row["TaxAmount"] = null;
                        row["TaxType"] = null;
                        row["UnitAmount"] = null;
                        row["Payments"] = item.Payments.Count.ToString();
                        //                    row["PlannedPaymentDate"] = item.PlannedPaymentDate;
                        row["Reference"] = item.Reference;
                        row["SentToContact"] = item.SentToContact;
                        row["Status"] = item.Status;
                        row["SubTotal"] = item.SubTotal;
                        row["Total"] = item.Total;
                        row["TotalDiscount"] = item.TotalDiscount;
                        row["TotalTax"] = item.TotalTax;
                        row["Type"] = item.Type;
                        row["UpdatedDateUTC"] = item.UpdatedDateUTC;
                        row["Url"] = item.Url;
                        entries.Rows.Add(row);
                        x++;
                    }
                }
                else
                {
                    Console.WriteLine("There are no invoices avaailable!");
                }

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
                {

                    bulkCopy.DestinationTableName =
                        "Invoices";

                    // Number of records to be processed in one go
                    //sbc.BatchSize = batchSize;

                    // Add your column mappings here
                    /*bulkCopy.ColumnMappings.Add("JID", "JID");
                    bulkCopy.ColumnMappings.Add("Reference", "Reference");
                    bulkCopy.ColumnMappings.Add("JNumber", "JNumber");
                    bulkCopy.ColumnMappings.Add("Date", "Date");
                    bulkCopy.ColumnMappings.Add("CreatedDateUTC", "CreatedDateUTC");
                    bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
                    bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
                    bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
                    bulkCopy.ColumnMappings.Add("NetAmt", "NetAmt");
                    bulkCopy.ColumnMappings.Add("GrossAmt", "GrossAmt");
                    bulkCopy.ColumnMappings.Add("ValidationErrors", "ValidationErrors");
                    bulkCopy.ColumnMappings.Add("ValidationStatus", "ValidationStatus");
                    bulkCopy.ColumnMappings.Add("Warnings", "Warnings");
                    bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
                    bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
                    bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
                    //bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
                    bulkCopy.ColumnMappings.Add("JournalLines", "JournalLines");*/


                    try
                    {
                        Console.WriteLine("Copying to the relational table...");

                        // Write from the source to the destination.
                        clear_table("Invoices");
                        bulkCopy.WriteToServer(entries);

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Debug.WriteLine(ex.Message.ToString());
                    }
                }

                GetAllIndvoicsByDetail(repository, IDs);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        protected void GetAllIndvoicsByDetail(Repository repository, List<String> IDs)
        {

            this.invoiceCreatedLabel.Text = "X";
            DataTable entries = new DataTable("InvoiceDetails");

            DataColumn InvoiceID = new DataColumn();
            InvoiceID.DataType = System.Type.GetType("System.String");
            InvoiceID.ColumnName = "InvoiceID";
            entries.Columns.Add(InvoiceID);

            DataColumn AccountCode = new DataColumn();
            AccountCode.DataType = System.Type.GetType("System.String");
            AccountCode.ColumnName = "AccountCode";
            entries.Columns.Add(AccountCode);

            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn DiscountRate = new DataColumn();
            DiscountRate.DataType = System.Type.GetType("System.String");
            DiscountRate.ColumnName = "DiscountRate";
            entries.Columns.Add(DiscountRate);

            DataColumn ItemCode = new DataColumn();
            ItemCode.DataType = System.Type.GetType("System.String");
            ItemCode.ColumnName = "ItemCode";
            entries.Columns.Add(ItemCode);

            DataColumn LineAmount = new DataColumn();
            LineAmount.DataType = System.Type.GetType("System.String");
            LineAmount.ColumnName = "LineAmount";
            entries.Columns.Add(LineAmount);

            DataColumn Quantity = new DataColumn();
            Quantity.DataType = System.Type.GetType("System.String");
            Quantity.ColumnName = "Quantity";
            entries.Columns.Add(Quantity);

            DataColumn TaxAmount = new DataColumn();
            TaxAmount.DataType = System.Type.GetType("System.String");
            TaxAmount.ColumnName = "TaxAmount";
            entries.Columns.Add(TaxAmount);

            DataColumn TaxType = new DataColumn();
            TaxType.DataType = System.Type.GetType("System.String");
            TaxType.ColumnName = "TaxType";
            entries.Columns.Add(TaxType);

            DataColumn TrackingID = new DataColumn();
            TrackingID.DataType = System.Type.GetType("System.String");
            TrackingID.ColumnName = "TrackingID";
            entries.Columns.Add(TrackingID);

            DataColumn TrackingOption = new DataColumn();
            TrackingOption.DataType = System.Type.GetType("System.String");
            TrackingOption.ColumnName = "TrackingOption";
            entries.Columns.Add(TrackingOption);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);
            int x = 1;
            foreach (String id in IDs)
            {
                //if (x % 50 == 0)
                {
                    System.Threading.Thread.Sleep(1000);
                    Double i = (x / (IDs.Count));
                    Session["State"] = i + 1;

                }
                this.invoiceCreatedLabel.Text = "Total Number of Incoices:" + IDs.Count + " <br />" + x;
                Guid inv_id = new Guid(id);
                Invoice invoice = repository.FindById<Invoice>(inv_id);

                //Debug.Console.WriteLine(x);
                Debug.WriteLine(x);
                x++;

                foreach (LineItem item in invoice.LineItems)
                {
                    DataRow row = entries.NewRow();
                    row["InvoiceID"] = id;
                    row["AccountCode"] = item.AccountCode;
                    row["Description"] = item.Description;
                    //                row["DiscountRate"] = item.DiscountRate;
                    row["ItemCode"] = item.ItemCode;
                    row["LineAmount"] = item.LineAmount;
                    row["Quantity"] = item.Quantity;
                    row["TaxAmount"] = item.TaxAmount;
                    row["TaxType"] = item.TaxType;
                    if (item.Tracking.Count == 1)
                    {
                        row["TrackingID"] = item.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = item.Tracking[0].Option;
                    }
                    else if (item.Tracking.Count == 2)
                    {
                        row["TrackingID"] = item.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = item.Tracking[0].Option;
                    }
                    else
                    {
                        row["TrackingID"] = null;
                        row["TrackingOption"] = null;
                    }

                    row["UnitAmount"] = item.UnitAmount;
                    entries.Rows.Add(row);
                }
                Session["State"] = 100;
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "InvoiceDetails";
                try
                {
                    Console.WriteLine("Copying to the relational table...");

                    // Write from the source to the destination.
                    clear_table("InvoiceDetails");
                    bulkCopy.WriteToServer(entries);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Debug.WriteLine(ex.Message.ToString());
                }
            }
        }

        protected void getTrackingCategories(Repository repository)
        {
            // Get the tracking categories in this org
            IQueryable<TrackingCategory> trackingCategories = repository.TrackingCategories;

            DataTable entries = new DataTable("TrackingCategories");

            DataColumn trackingCategoryID = new DataColumn();
            trackingCategoryID.DataType = System.Type.GetType("System.String");
            trackingCategoryID.ColumnName = "trackingCategoryID";
            entries.Columns.Add(trackingCategoryID);

            DataColumn Category = new DataColumn();
            Category.DataType = System.Type.GetType("System.String");
            Category.ColumnName = "Category";
            entries.Columns.Add(Category);

            DataColumn Option = new DataColumn();
            Option.DataType = System.Type.GetType("System.String");
            Option.ColumnName = "Option";
            entries.Columns.Add(Option);


            foreach (var trackingCategory in trackingCategories)
            {
                Console.WriteLine(string.Format("Tracking Category: {0}", trackingCategory.Name));

                foreach (var trackingOption in trackingCategory.Options)
                {
                    DataRow row = entries.NewRow();
                    row["trackingCategoryID"] = trackingCategory.TrackingCategoryID;
                    row["Category"] = trackingCategory.Name;
                    row["Option"] = trackingOption.Name;
                    entries.Rows.Add(row);
                }
            }

            clear_table("TrackingCategories");
            // insert into sql table
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "TrackingCategories";
                try
                {
                    Console.WriteLine("Copying to the relational table...");
                    bulkCopy.WriteToServer(entries);
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            Console.WriteLine("Tracking categories have been imported!");
        }

        protected void GetAllContacts(Repository repository)
        {
            // Construct a linq expression to call 'GET Contacts'...
            //int invoiceCount = repository.Contacts.Count(c => c.UpdatedDateUTC >= DateTime.UtcNow.AddMonths(-1));
            int count = repository.Contacts.Count();

            //------------------------------------------------------------//
            //    Addresses: creating the relevant data table and columns
            //------------------------------------------------------------//            
            DataTable Contact_Address = new DataTable("Contact_Address");

            DataColumn AddressID = new DataColumn();
            AddressID.DataType = System.Type.GetType("System.String");
            AddressID.ColumnName = "AddressID";
            Contact_Address.Columns.Add(AddressID);

            DataColumn adressContactID = new DataColumn();
            adressContactID.DataType = System.Type.GetType("System.String");
            adressContactID.ColumnName = "adressContactID";
            Contact_Address.Columns.Add(adressContactID);

            DataColumn AddressLine1 = new DataColumn();
            AddressLine1.DataType = System.Type.GetType("System.String");
            AddressLine1.ColumnName = "AddressLine1";
            Contact_Address.Columns.Add(AddressLine1);

            DataColumn AddressLine2 = new DataColumn();
            AddressLine2.DataType = System.Type.GetType("System.String");
            AddressLine2.ColumnName = "AddressLine2";
            Contact_Address.Columns.Add(AddressLine2);

            DataColumn AddressLine3 = new DataColumn();
            AddressLine3.DataType = System.Type.GetType("System.String");
            AddressLine3.ColumnName = "AddressLine3";
            Contact_Address.Columns.Add(AddressLine3);


            DataColumn AddressLine4 = new DataColumn();
            AddressLine4.DataType = System.Type.GetType("System.String");
            AddressLine4.ColumnName = "AddressLine4";
            Contact_Address.Columns.Add(AddressLine4);

            DataColumn AttentionTo = new DataColumn();
            AttentionTo.DataType = System.Type.GetType("System.String");
            AttentionTo.ColumnName = "AttentionTo";
            Contact_Address.Columns.Add(AttentionTo);

            DataColumn City = new DataColumn();
            City.DataType = System.Type.GetType("System.String");
            City.ColumnName = "City";
            Contact_Address.Columns.Add(City);

            DataColumn Country = new DataColumn();
            Country.DataType = System.Type.GetType("System.String");
            Country.ColumnName = "Country";
            Contact_Address.Columns.Add(Country);

            DataColumn PostalCode = new DataColumn();
            PostalCode.DataType = System.Type.GetType("System.String");
            PostalCode.ColumnName = "PostalCode";
            Contact_Address.Columns.Add(PostalCode);

            DataColumn Region = new DataColumn();
            Region.DataType = System.Type.GetType("System.String");
            Region.ColumnName = "Region";
            Contact_Address.Columns.Add(Region);
            //------------------------------------------------------------//
            //    Contacts: creating the relevant data table and columns
            //------------------------------------------------------------//
            DataTable Contacts_Table = new DataTable("Contacts_Table");

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            Contacts_Table.Columns.Add(ContactID);

            DataColumn GroupCount = new DataColumn();
            GroupCount.DataType = System.Type.GetType("System.String");
            GroupCount.ColumnName = "GroupCount";
            Contacts_Table.Columns.Add(GroupCount);

            DataColumn ContactName = new DataColumn();
            ContactName.DataType = System.Type.GetType("System.String");
            ContactName.ColumnName = "ContactName";
            Contacts_Table.Columns.Add(ContactName);

            DataColumn AccountsPayable_Outstanding = new DataColumn();
            AccountsPayable_Outstanding.DataType = System.Type.GetType("System.String");
            AccountsPayable_Outstanding.ColumnName = "AccountsPayable_Outstanding";
            Contacts_Table.Columns.Add(AccountsPayable_Outstanding);

            DataColumn AccountsPayable_Overdue = new DataColumn();
            AccountsPayable_Overdue.DataType = System.Type.GetType("System.String");
            AccountsPayable_Overdue.ColumnName = "AccountsPayable_Overdue";
            Contacts_Table.Columns.Add(AccountsPayable_Overdue);

            DataColumn AccountsReceivable_Outstanding = new DataColumn();
            AccountsReceivable_Outstanding.DataType = System.Type.GetType("System.String");
            AccountsReceivable_Outstanding.ColumnName = "AccountsReceivable_Outstanding";
            Contacts_Table.Columns.Add(AccountsReceivable_Outstanding);

            DataColumn AccountsReceivable_Overdue = new DataColumn();
            AccountsReceivable_Overdue.DataType = System.Type.GetType("System.String");
            AccountsReceivable_Overdue.ColumnName = "AccountsReceivable_Overdue";
            Contacts_Table.Columns.Add(AccountsReceivable_Overdue);

            DataColumn ContactStatus = new DataColumn();
            ContactStatus.DataType = System.Type.GetType("System.String");
            ContactStatus.ColumnName = "ContactStatus";
            Contacts_Table.Columns.Add(ContactStatus);

            DataColumn FirstName = new DataColumn();
            FirstName.DataType = System.Type.GetType("System.String");
            FirstName.ColumnName = "FirstName";
            Contacts_Table.Columns.Add(FirstName);

            DataColumn LastName = new DataColumn();
            LastName.DataType = System.Type.GetType("System.String");
            LastName.ColumnName = "LastName";
            Contacts_Table.Columns.Add(LastName);

            DataColumn IsCustomer = new DataColumn();
            IsCustomer.DataType = System.Type.GetType("System.String");
            IsCustomer.ColumnName = "IsCustomer";
            Contacts_Table.Columns.Add(IsCustomer);

            DataColumn IsSupplier = new DataColumn();
            IsSupplier.DataType = System.Type.GetType("System.String");
            IsSupplier.ColumnName = "IsSupplier";
            Contacts_Table.Columns.Add(IsSupplier);

            DataColumn PaymentTerms_Bills_Day = new DataColumn();
            PaymentTerms_Bills_Day.DataType = System.Type.GetType("System.String");
            PaymentTerms_Bills_Day.ColumnName = "PaymentTerms_Bills_Day";
            Contacts_Table.Columns.Add(PaymentTerms_Bills_Day);

            DataColumn PaymentTerms_Bills_Type = new DataColumn();
            PaymentTerms_Bills_Type.DataType = System.Type.GetType("System.String");
            PaymentTerms_Bills_Type.ColumnName = "PaymentTerms_Bills_Type";
            Contacts_Table.Columns.Add(PaymentTerms_Bills_Type);

            DataColumn PaymentTerms_Sales_Day = new DataColumn();
            PaymentTerms_Sales_Day.DataType = System.Type.GetType("System.String");
            PaymentTerms_Sales_Day.ColumnName = "PaymentTerms_Sales_Day";
            Contacts_Table.Columns.Add(PaymentTerms_Sales_Day);

            DataColumn PaymentTerms_Sales_Type = new DataColumn();
            PaymentTerms_Sales_Type.DataType = System.Type.GetType("System.String");
            PaymentTerms_Sales_Type.ColumnName = "PaymentTerms_Sales_Type";
            Contacts_Table.Columns.Add(PaymentTerms_Sales_Type);



            foreach (XeroApi.Model.Contact contact in repository.Contacts)
            {

                DataRow row1 = Contacts_Table.NewRow();
                row1["ContactID"] = contact.ContactID;
                row1["GroupCount"] = contact.ContactGroups.Count.ToString();
                row1["ContactName"] = contact.Name;
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsPayable != null)
                    {
                        if (contact.Balances.AccountsPayable.Outstanding.HasValue)
                        {
                            row1["AccountsPayable_Outstanding"] = contact.Balances.AccountsPayable.Outstanding.Value.ToString();
                        }
                    }
                }
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsPayable != null)
                    {
                        if (contact.Balances.AccountsPayable.Overdue.HasValue)
                        {
                            row1["AccountsPayable_Overdue"] = contact.Balances.AccountsPayable.Overdue.Value.ToString();
                        }
                    }
                }
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsReceivable != null)
                    {
                        if (contact.Balances.AccountsReceivable.Outstanding.HasValue)
                        {
                            row1["AccountsReceivable_Outstanding"] = contact.Balances.AccountsReceivable.Outstanding.Value.ToString();
                        }
                    }
                }
                if (contact.Balances != null)
                {
                    if (contact.Balances.AccountsReceivable != null)
                    {
                        if (contact.Balances.AccountsReceivable.Overdue.HasValue)
                        {
                            row1["AccountsReceivable_Overdue"] = contact.Balances.AccountsReceivable.Overdue.Value.ToString();
                        }
                    }
                }
                row1["ContactStatus"] = contact.ContactStatus;
                row1["FirstName"] = contact.FirstName;
                row1["LastName"] = contact.LastName;
                row1["IsCustomer"] = contact.IsCustomer.ToString();
                row1["IsSupplier"] = contact.IsSupplier.ToString();
                if (contact.PaymentTerms != null)
                {

                    if (contact.PaymentTerms.Bills != null)
                    {
                        row1["PaymentTerms_Bills_Day"] = contact.PaymentTerms.Bills.Day.ToString();
                        row1["PaymentTerms_Bills_Type"] = contact.PaymentTerms.Bills.Type.ToString();
                    }
                    if (contact.PaymentTerms.Sales != null)
                    {
                        row1["PaymentTerms_Sales_Day"] = contact.PaymentTerms.Sales.Day.ToString();
                        row1["PaymentTerms_Sales_Type"] = contact.PaymentTerms.Sales.Type.ToString();
                    }
                }

                Contacts_Table.Rows.Add(row1);
                int address_id = 0;

                foreach (Address address in contact.Addresses)
                {
                    DataRow row = Contact_Address.NewRow();
                    row["AddressID"] = address_id.ToString();
                    row["adressContactID"] = contact.ContactID.ToString();
                    row["AddressLine1"] = address.AddressLine1;
                    row["AddressLine2"] = address.AddressLine2;
                    row["AddressLine3"] = address.AddressLine3;
                    row["AddressLine4"] = address.AddressLine4;
                    row["AttentionTo"] = address.AttentionTo;
                    row["Country"] = address.Country;
                    row["PostalCode"] = address.PostalCode;
                    row["City"] = address.City;
                    row["Region"] = address.Region;
                    Contact_Address.Rows.Add(row);
                    address_id++;
                }
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "Contact_Address";
                bulkCopy.ColumnMappings.Add("AddressID", "AddressID");
                bulkCopy.ColumnMappings.Add("adressContactID", "ContactID");
                bulkCopy.ColumnMappings.Add("AddressLine1", "AddressLine1");
                bulkCopy.ColumnMappings.Add("AddressLine2", "AddressLine2");
                bulkCopy.ColumnMappings.Add("AddressLine3", "AddressLine3");
                bulkCopy.ColumnMappings.Add("AddressLine4", "AddressLine4");
                bulkCopy.ColumnMappings.Add("AttentionTo", "AttentionTo");
                bulkCopy.ColumnMappings.Add("Country", "Country");
                bulkCopy.ColumnMappings.Add("PostalCode", "PostalCode");
                bulkCopy.ColumnMappings.Add("City", "City");
                bulkCopy.ColumnMappings.Add("Region", "Region");
                try
                {
                    clear_table("Contact_Address");
                    // Write from the source to the destination.
                    bulkCopy.WriteToServer(Contact_Address);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {
                bulkCopy.DestinationTableName =
                    "Contacts";
                bulkCopy.ColumnMappings.Add("ContactID", "ContactID");
                bulkCopy.ColumnMappings.Add("GroupCount", "GroupCount");
                bulkCopy.ColumnMappings.Add("ContactName", "ContactName");
                bulkCopy.ColumnMappings.Add("AccountsPayable_Outstanding", "AccountsPayable_Outstanding");
                bulkCopy.ColumnMappings.Add("AccountsPayable_Overdue", "AccountsPayable_Overdue");
                bulkCopy.ColumnMappings.Add("AccountsReceivable_Outstanding", "AccountsReceivable_Outstanding");
                bulkCopy.ColumnMappings.Add("AccountsReceivable_Overdue", "AccountsReceivable_Overdue");
                bulkCopy.ColumnMappings.Add("ContactStatus", "ContactStatus");
                bulkCopy.ColumnMappings.Add("FirstName", "FirstName");
                bulkCopy.ColumnMappings.Add("LastName", "LastName");
                bulkCopy.ColumnMappings.Add("IsCustomer", "IsCustomer");
                bulkCopy.ColumnMappings.Add("IsSupplier", "IsSupplier");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Bills_Day", "PaymentTerms_Bills_Day");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Bills_Type", "PaymentTerms_Bills_Type");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Sales_Day", "PaymentTerms_Sales_Day");
                bulkCopy.ColumnMappings.Add("PaymentTerms_Sales_Type", "PaymentTerms_Sales_Type");

                try
                {
                    // Write from the source to the destination.
                    clear_table("Contacts");
                    bulkCopy.WriteToServer(Contacts_Table);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        protected void GetAllCreditNotesWithPagination(Repository repository)
        {
            DataTable entries = new DataTable("CreditNotes");
            List<String> IDs = new List<String>();

            DataColumn Allocations = new DataColumn();
            Allocations.DataType = System.Type.GetType("System.String");
            Allocations.ColumnName = "Allocations";
            entries.Columns.Add(Allocations);

            DataColumn AppliedAmount = new DataColumn();
            AppliedAmount.DataType = System.Type.GetType("System.String");
            AppliedAmount.ColumnName = "AppliedAmount";
            entries.Columns.Add(AppliedAmount);

            DataColumn BrandingThemeID = new DataColumn();
            BrandingThemeID.DataType = System.Type.GetType("System.String");
            BrandingThemeID.ColumnName = "BrandingThemeID";
            entries.Columns.Add(BrandingThemeID);

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            entries.Columns.Add(ContactID);

            DataColumn CreditNoteID = new DataColumn();
            CreditNoteID.DataType = System.Type.GetType("System.String");
            CreditNoteID.ColumnName = "CreditNoteID";
            entries.Columns.Add(CreditNoteID);

            DataColumn CreditNoteNumber = new DataColumn();
            CreditNoteNumber.DataType = System.Type.GetType("System.String");
            CreditNoteNumber.ColumnName = "CreditNoteNumber";
            entries.Columns.Add(CreditNoteNumber);

            DataColumn CurrencyCode = new DataColumn();
            CurrencyCode.DataType = System.Type.GetType("System.String");
            CurrencyCode.ColumnName = "CurrencyCode";
            entries.Columns.Add(CurrencyCode);

            DataColumn CurrencyRate = new DataColumn();
            CurrencyRate.DataType = System.Type.GetType("System.String");
            CurrencyRate.ColumnName = "CurrencyRate";
            entries.Columns.Add(CurrencyRate);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);

            DataColumn DueDate = new DataColumn();
            DueDate.DataType = System.Type.GetType("System.String");
            DueDate.ColumnName = "DueDate";
            entries.Columns.Add(DueDate);

            DataColumn FullyPaidOnDate = new DataColumn();
            FullyPaidOnDate.DataType = System.Type.GetType("System.String");
            FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
            entries.Columns.Add(FullyPaidOnDate);

            DataColumn LineAmountTypes = new DataColumn();
            LineAmountTypes.DataType = System.Type.GetType("System.String");
            LineAmountTypes.ColumnName = "LineAmountTypes";
            entries.Columns.Add(LineAmountTypes);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);

            DataColumn Reference = new DataColumn();
            Reference.DataType = System.Type.GetType("System.String");
            Reference.ColumnName = "Reference";
            entries.Columns.Add(Reference);

            DataColumn RemainingCredit = new DataColumn();
            RemainingCredit.DataType = System.Type.GetType("System.String");
            RemainingCredit.ColumnName = "RemainingCredit";
            entries.Columns.Add(RemainingCredit);

            DataColumn SentToContact = new DataColumn();
            SentToContact.DataType = System.Type.GetType("System.String");
            SentToContact.ColumnName = "SentToContact";
            entries.Columns.Add(SentToContact);

            DataColumn Status = new DataColumn();
            Status.DataType = System.Type.GetType("System.String");
            Status.ColumnName = "Status";
            entries.Columns.Add(Status);

            DataColumn SubTotal = new DataColumn();
            SubTotal.DataType = System.Type.GetType("System.String");
            SubTotal.ColumnName = "SubTotal";
            entries.Columns.Add(SubTotal);

            DataColumn Total = new DataColumn();
            Total.DataType = System.Type.GetType("System.String");
            Total.ColumnName = "Total";
            entries.Columns.Add(Total);

            DataColumn TotalTax = new DataColumn();
            TotalTax.DataType = System.Type.GetType("System.String");
            TotalTax.ColumnName = "TotalTax";
            entries.Columns.Add(TotalTax);

            DataColumn Type = new DataColumn();
            Type.DataType = System.Type.GetType("System.String");
            Type.ColumnName = "Type";
            entries.Columns.Add(Type);

            DataColumn UpdatedDateUTC = new DataColumn();
            UpdatedDateUTC.DataType = System.Type.GetType("System.String");
            UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
            entries.Columns.Add(UpdatedDateUTC);


            int x = 1;

            foreach (CreditNote credit in repository.CreditNotes.ToList())
            {
                DataRow row = entries.NewRow();
                IDs.Add(credit.CreditNoteID.ToString());
                row["Allocations"] = credit.Allocations.Count;
                row["AppliedAmount"] = credit.AppliedAmount;
                row["BrandingThemeID"] = credit.BrandingThemeID;
                row["ContactID"] = credit.Contact.ContactID;
                row["CreditNoteID"] = credit.CreditNoteID;
                row["CreditNoteNumber"] = credit.CreditNoteNumber;
                row["CurrencyCode"] = credit.CurrencyCode;
                row["CurrencyRate"] = credit.CurrencyRate;
                row["Date"] = credit.Date;
                row["DueDate"] = credit.DueDate;
                row["FullyPaidOnDate"] = credit.FullyPaidOnDate;
                row["LineAmountTypes"] = credit.LineAmountTypes;
                row["Reference"] = credit.Reference;
                row["RemainingCredit"] = credit.RemainingCredit;
                row["SentToContact"] = credit.SentToContact;
                row["Status"] = credit.Status;
                row["SubTotal"] = credit.SubTotal;
                row["Total"] = credit.Total;
                row["TotalTax"] = credit.TotalTax;
                row["Type"] = credit.Type;
                row["UpdatedDateUTC"] = credit.UpdatedDateUTC;
                entries.Rows.Add(row);
            }


            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "CreditNotes";

                // Number of records to be processed in one go
                //sbc.BatchSize = batchSize;

                // Add your column mappings here
                /*bulkCopy.ColumnMappings.Add("JID", "JID");
                bulkCopy.ColumnMappings.Add("Reference", "Reference");
                bulkCopy.ColumnMappings.Add("JNumber", "JNumber");
                bulkCopy.ColumnMappings.Add("Date", "Date");
                bulkCopy.ColumnMappings.Add("CreatedDateUTC", "CreatedDateUTC");
                bulkCopy.ColumnMappings.Add("AccountName", "AccountName");
                bulkCopy.ColumnMappings.Add("AccountCode", "AccountCode");
                bulkCopy.ColumnMappings.Add("AccountType", "AccountType");
                bulkCopy.ColumnMappings.Add("NetAmt", "NetAmt");
                bulkCopy.ColumnMappings.Add("GrossAmt", "GrossAmt");
                bulkCopy.ColumnMappings.Add("ValidationErrors", "ValidationErrors");
                bulkCopy.ColumnMappings.Add("ValidationStatus", "ValidationStatus");
                bulkCopy.ColumnMappings.Add("Warnings", "Warnings");
                bulkCopy.ColumnMappings.Add("TrackingCategory1", "TrackingCategory1");
                bulkCopy.ColumnMappings.Add("TrackingCategory1_Option", "TrackingCategory1_Option");
                bulkCopy.ColumnMappings.Add("TrackingCategory2", "TrackingCategory2");
                //bulkCopy.ColumnMappings.Add("TrackingCategory2_Option", "TrackingCategory2_Option");
                bulkCopy.ColumnMappings.Add("JournalLines", "JournalLines");*/
                try
                {
                    Console.WriteLine("Copying to the relational table...");

                    // Write from the source to the destination.
                    clear_table("CreditNotes");
                    bulkCopy.WriteToServer(entries);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            GetCreditNotesByDetail(repository, IDs);
        }

        protected void GetCreditNotesByDetail(Repository repository, List<String> IDs)
        {
            DataTable entries = new DataTable("CreditNotesDetails");


            DataColumn InvoiceID = new DataColumn();
            InvoiceID.DataType = System.Type.GetType("System.String");
            InvoiceID.ColumnName = "InvoiceID";
            entries.Columns.Add(InvoiceID);

            DataColumn AccountCode = new DataColumn();
            AccountCode.DataType = System.Type.GetType("System.String");
            AccountCode.ColumnName = "AccountCode";
            entries.Columns.Add(AccountCode);


            DataColumn Description = new DataColumn();
            Description.DataType = System.Type.GetType("System.String");
            Description.ColumnName = "Description";
            entries.Columns.Add(Description);

            DataColumn DiscountRate = new DataColumn();
            DiscountRate.DataType = System.Type.GetType("System.String");
            DiscountRate.ColumnName = "DiscountRate";
            entries.Columns.Add(DiscountRate);


            DataColumn ItemCode = new DataColumn();
            ItemCode.DataType = System.Type.GetType("System.String");
            ItemCode.ColumnName = "ItemCode";
            entries.Columns.Add(ItemCode);


            DataColumn LineAmount = new DataColumn();
            LineAmount.DataType = System.Type.GetType("System.String");
            LineAmount.ColumnName = "LineAmount";
            entries.Columns.Add(LineAmount);

            DataColumn Quantity = new DataColumn();
            Quantity.DataType = System.Type.GetType("System.String");
            Quantity.ColumnName = "Quantity";
            entries.Columns.Add(Quantity);

            DataColumn TaxAmount = new DataColumn();
            TaxAmount.DataType = System.Type.GetType("System.String");
            TaxAmount.ColumnName = "TaxAmount";
            entries.Columns.Add(TaxAmount);


            DataColumn TaxType = new DataColumn();
            TaxType.DataType = System.Type.GetType("System.String");
            TaxType.ColumnName = "TaxType";
            entries.Columns.Add(TaxType);

            DataColumn TrackingID = new DataColumn();
            TrackingID.DataType = System.Type.GetType("System.String");
            TrackingID.ColumnName = "TrackingID";
            entries.Columns.Add(TrackingID);

            DataColumn TrackingOption = new DataColumn();
            TrackingOption.DataType = System.Type.GetType("System.String");
            TrackingOption.ColumnName = "TrackingOption";
            entries.Columns.Add(TrackingOption);

            DataColumn Allocations = new DataColumn();
            Allocations.DataType = System.Type.GetType("System.String");
            Allocations.ColumnName = "Allocations";
            entries.Columns.Add(Allocations);

            DataColumn AppliedAmount = new DataColumn();
            AppliedAmount.DataType = System.Type.GetType("System.String");
            AppliedAmount.ColumnName = "AppliedAmount";
            entries.Columns.Add(AppliedAmount);

            DataColumn BrandingThemeID = new DataColumn();
            BrandingThemeID.DataType = System.Type.GetType("System.String");
            BrandingThemeID.ColumnName = "BrandingThemeID";
            entries.Columns.Add(BrandingThemeID);

            DataColumn ContactID = new DataColumn();
            ContactID.DataType = System.Type.GetType("System.String");
            ContactID.ColumnName = "ContactID";
            entries.Columns.Add(ContactID);

            DataColumn CreditNoteID = new DataColumn();
            CreditNoteID.DataType = System.Type.GetType("System.String");
            CreditNoteID.ColumnName = "CreditNoteID";
            entries.Columns.Add(CreditNoteID);

            DataColumn CreditNoteNumber = new DataColumn();
            CreditNoteNumber.DataType = System.Type.GetType("System.String");
            CreditNoteNumber.ColumnName = "CreditNoteNumber";
            entries.Columns.Add(CreditNoteNumber);

            DataColumn CurrencyCode = new DataColumn();
            CurrencyCode.DataType = System.Type.GetType("System.String");
            CurrencyCode.ColumnName = "CurrencyCode";
            entries.Columns.Add(CurrencyCode);

            DataColumn CurrencyRate = new DataColumn();
            CurrencyRate.DataType = System.Type.GetType("System.String");
            CurrencyRate.ColumnName = "CurrencyRate";
            entries.Columns.Add(CurrencyRate);

            DataColumn Date = new DataColumn();
            Date.DataType = System.Type.GetType("System.String");
            Date.ColumnName = "Date";
            entries.Columns.Add(Date);

            DataColumn DueDate = new DataColumn();
            DueDate.DataType = System.Type.GetType("System.String");
            DueDate.ColumnName = "DueDate";
            entries.Columns.Add(DueDate);

            DataColumn FullyPaidOnDate = new DataColumn();
            FullyPaidOnDate.DataType = System.Type.GetType("System.String");
            FullyPaidOnDate.ColumnName = "FullyPaidOnDate";
            entries.Columns.Add(FullyPaidOnDate);

            DataColumn LineAmountTypes = new DataColumn();
            LineAmountTypes.DataType = System.Type.GetType("System.String");
            LineAmountTypes.ColumnName = "LineAmountTypes";
            entries.Columns.Add(LineAmountTypes);

            DataColumn UnitAmount = new DataColumn();
            UnitAmount.DataType = System.Type.GetType("System.String");
            UnitAmount.ColumnName = "UnitAmount";
            entries.Columns.Add(UnitAmount);

            DataColumn Reference = new DataColumn();
            Reference.DataType = System.Type.GetType("System.String");
            Reference.ColumnName = "Reference";
            entries.Columns.Add(Reference);

            DataColumn RemainingCredit = new DataColumn();
            RemainingCredit.DataType = System.Type.GetType("System.String");
            RemainingCredit.ColumnName = "RemainingCredit";
            entries.Columns.Add(RemainingCredit);

            DataColumn SentToContact = new DataColumn();
            SentToContact.DataType = System.Type.GetType("System.String");
            SentToContact.ColumnName = "SentToContact";
            entries.Columns.Add(SentToContact);

            DataColumn Status = new DataColumn();
            Status.DataType = System.Type.GetType("System.String");
            Status.ColumnName = "Status";
            entries.Columns.Add(Status);

            DataColumn SubTotal = new DataColumn();
            SubTotal.DataType = System.Type.GetType("System.String");
            SubTotal.ColumnName = "SubTotal";
            entries.Columns.Add(SubTotal);

            DataColumn Total = new DataColumn();
            Total.DataType = System.Type.GetType("System.String");
            Total.ColumnName = "Total";
            entries.Columns.Add(Total);

            DataColumn TotalTax = new DataColumn();
            TotalTax.DataType = System.Type.GetType("System.String");
            TotalTax.ColumnName = "TotalTax";
            entries.Columns.Add(TotalTax);

            DataColumn Type = new DataColumn();
            Type.DataType = System.Type.GetType("System.String");
            Type.ColumnName = "Type";
            entries.Columns.Add(Type);

            DataColumn UpdatedDateUTC = new DataColumn();
            UpdatedDateUTC.DataType = System.Type.GetType("System.String");
            UpdatedDateUTC.ColumnName = "UpdatedDateUTC";
            entries.Columns.Add(UpdatedDateUTC);
            int x = 1;
            foreach (String id in IDs)
            {
                if (x == 50)
                {
                    System.Threading.Thread.Sleep(30000);
                }
                Guid note_id = new Guid(id);
                CreditNote note = repository.FindById<CreditNote>(note_id);
                foreach (LineItem line in note.LineItems)
                {
                    DataRow row = entries.NewRow();

                    row["InvoiceID"] = null;
                    row["AccountCode"] = line.AccountCode;
                    row["Description"] = line.Description;
                    //               row["DiscountRate"] = line.DiscountRate;
                    row["ItemCode"] = line.ItemCode;
                    row["LineAmount"] = line.LineAmount;
                    row["Quantity"] = line.Quantity;
                    row["TaxAmount"] = line.TaxAmount;
                    row["TaxType"] = line.TaxType;
                    if (line.Tracking.Count == 1)
                    {
                        row["TrackingID"] = line.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = line.Tracking[0].Option;
                    }
                    else if (line.Tracking.Count == 2)
                    {
                        row["TrackingID"] = line.Tracking[0].TrackingCategoryID;
                        row["TrackingOption"] = line.Tracking[0].Option;
                    }
                    else
                    {
                        row["TrackingID"] = null;
                        row["TrackingOption"] = null;
                    }
                    row["Allocations"] = note.Allocations.Count;
                    row["AppliedAmount"] = note.AppliedAmount;
                    row["BrandingThemeID"] = note.BrandingThemeID;
                    row["ContactID"] = note.Contact.ContactID;
                    row["CreditNoteID"] = note.CreditNoteID;
                    row["CreditNoteNumber"] = note.CreditNoteNumber;
                    row["CurrencyCode"] = note.CurrencyCode;
                    row["CurrencyRate"] = note.CurrencyRate;
                    row["Date"] = note.Date;
                    row["DueDate"] = note.DueDate;
                    row["FullyPaidOnDate"] = note.FullyPaidOnDate;
                    row["LineAmountTypes"] = note.LineAmountTypes;
                    row["Reference"] = note.Reference;
                    row["RemainingCredit"] = note.RemainingCredit;
                    row["SentToContact"] = note.SentToContact;
                    row["Status"] = note.Status;
                    row["SubTotal"] = note.SubTotal;
                    row["Total"] = note.Total;
                    row["TotalTax"] = note.TotalTax;
                    row["Type"] = note.Type;
                    row["UpdatedDateUTC"] = note.UpdatedDateUTC;
                    entries.Rows.Add(row);
                }
            }

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn2))
            {

                bulkCopy.DestinationTableName =
                    "CreditNotesDetails";

                try
                {
                    Console.WriteLine("Copying to the relational table...");

                    // Write from the source to the destination.
                    clear_table("CreditNotesDetails");
                    bulkCopy.WriteToServer(entries);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void jedoxETL_getProjects()
        {


            // var b=new ETL.getExecutionListResponse();

            // Debug.WriteLine(b.@return);
            //var b = new ETL.getNames();

        }

        private static void clear_table(String teble_name)
        {
            SqlConnection MyConnection = null;
            SqlDataReader myReader = null;
            String sqlStr = @" Truncate Table " + teble_name;
            try
            {
                MyConnection = new SqlConnection(strConn2);
                MyConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStr, MyConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {

                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Debug.WriteLine(ex.Message.ToString());
            }
            finally
            {
                if (MyConnection != null)
                {
                    MyConnection.Close();
                }
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        protected void cmdUpdateCubes(object sender, EventArgs e)
        {
            Debug.WriteLine(Session["Jelly_user"]);
        }

    }
}