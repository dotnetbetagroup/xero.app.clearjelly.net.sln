﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AutomaticUpdate.aspx.cs" Inherits="AutomaticUpdate" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h1>Clear Jelly Configuration</h1>
         <table style="width: 50%">
              <tr>
                <td style="width: 300px">
                    <asp:Label ID="lblSD" runat="server" Width="220" Text="Start Date:" />
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <asp:TextBox ID="txtSD" runat="server" Width="220" Text="" />
                     <input id="clearSD" type="button" value="Clear"/> 
                </td>
            </tr>
        </table>

        <br />
        <br />
        <asp:Button ID="cmdSubmit" Width="220" runat="server" Text="Connect to Xero" OnClick="cmdConnect_Click" />

        <br />
        <br />
        <asp:Button runat="server" ID="btnCmdRunAllTasks" Width="220" Text="Build your Xero model" OnClick="cmdRunAllTasks" />
        <br />
        <br />

    </div>
    <p>
          <asp:Label ID="invoiceCreatedLabel" runat="server" />
    </p>
     <script type="text/javascript" src="../Scripts/XeroWebApp/AutomaticUpdate.js">
    </script>
</asp:Content>


