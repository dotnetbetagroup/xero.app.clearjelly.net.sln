﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ClearJelly.Web.Common.OAuth
{
    public class OAuth2Settings
    {
        public static string RedirectUri
        {
            get { return ConfigurationManager.AppSettings["redirectURI"]; }
        }
        public static string ClientID
        {
            get { return ConfigurationManager.AppSettings["clientID"]; }
        }
        public static string ClientSecret
        {
            get { return ConfigurationManager.AppSettings["clientSecret"]; }
        }
        public static string DiscoveryUrl
        {
            get { return ConfigurationManager.AppSettings["discoveryUrl"]; }
        }
        public static string LogPath
        {
            get { return " "; }
        }
    }
}