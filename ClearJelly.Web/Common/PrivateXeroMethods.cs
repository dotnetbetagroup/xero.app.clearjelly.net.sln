﻿using System;
using XeroApi;
using System.Security.Cryptography.X509Certificates;
using XeroApi.OAuth;
using XeroApi.Model;
using System.Linq;
using Xero.Api.Core.Model;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Infrastructure.Http;
using Xero.Api.Example.Applications.Private;
using Xero.Api.Serialization;
using Xero.Api.Core.Response;
using ClearJelly.ViewModels.PayPalViewModels;
using DevDefined.OAuth;
using ClearJelly.Services;


//reference: DevDefined.OAuth
namespace ClearJelly.Web
{

    public class PrivateXeroMethods
    {
        public class ApplicationSettings
        {
            public string BaseApiUrl { get; set; }
            public Consumer Consumer { get; set; }
            public X509Certificate Authenticator { get; set; }
            public string OnlineInvoiceLink { get; set; }
        }
        private static ApplicationSettings _applicationSettings;
        private static NLog.Logger _logger;
        private static String uName;
        private const string UserAgentString = "Xero.API.ScreenCast v1.0 (Private App Testing)";

    private readonly X509Certificate2 _certificate;
        public XeroHttpClient Client { get; private set; }
        private string privateConsumerKey;
        private static string BaseApiUrl;
        public Consumer consumer { get; set; }
        public PrivateXeroMethods(string logemail)
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            uName = logemail;
            BaseApiUrl = "https://api.xero.com";
            //var signingCertificatePath = @"C:\Certs\public_privatekey.pfx";
            //var clientCertificatePassword = "mana@2010";
            //var auth = new PrivateAuthenticator(signingCertificatePath, clientCertificatePassword);
            var auth = new PrivateAuthenticator(@"D:\public_privatekey.pfx", "123456");
            var privateConsumerKey = "H7KQ5FIGCAMJPQREGVJ0WKHXNQKCCC";
            var privateConsumerSecret = "UKS41SVWDPCTCGZHDEX7VEF8GIBE8D";
            var privateConsumer = new Consumer(privateConsumerKey, privateConsumerSecret);
            Client = new XeroHttpClient(BaseApiUrl, auth, privateConsumer, null, new DefaultMapper(), new DefaultMapper());
            var OnlineInvoiceLink = "/api.xro/2.0/Invoices/{0}/OnlineInvoice";
            var privateApplicationSettings = new ApplicationSettings
            {
                BaseApiUrl = BaseApiUrl,
                Consumer = privateConsumer,
                Authenticator = auth.Certificate,
                OnlineInvoiceLink = OnlineInvoiceLink
            };
            _applicationSettings = privateApplicationSettings;
        }

        public OnlineInvoice RetrieveOnlineInvoiceUrl(Guid invoiceId)
        {
            try
            {
                _logger.Info(uName + " ~Execution of PrivateXeroMethods RetrieveOnlineInvoiceUrl started.");
                var invoiceDetails = Client.Get<OnlineInvoice, OnlineInvoicesResponse>(string.Format(_applicationSettings.OnlineInvoiceLink, invoiceId));
                if (invoiceDetails != null)
                {
                    return invoiceDetails.First();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(uName + " ~Execution of PrivateXeroMethods RetrieveOnlineInvoiceUrl completed.", ex);
                throw;
            }
        }

        private Repository CreateRepository()
        {
            try
            {
                _logger.Info(uName + " ~Execution of PrivateXeroMethods CreateRepository started.");
                //DevDefined.OAuth.Consumer.IOAuthSession
                var consumerSession = new XeroApi.OAuth.XeroApiPrivateSession(UserAgentString, _applicationSettings.Consumer.ConsumerKey, (X509Certificate2)_applicationSettings.Authenticator);
                return new Repository(consumerSession);
            }
            catch (Exception ex)
            {
                _logger.Error((uName ?? "") + "Execution of  PrivateXeroMethods CreateRepository Completed.", ex);
                throw;
            }
        }

        public bool CreateInvoiceXero(PaypalPaymentDetailsViewModel payPalvm)
        {
            try
            {
                Repository repository = CreateRepository();
                _logger.Info(uName + " ~Execution of PrivateXeroMethods CreateInvoiceXero started.");
                if (repository == null)
                {
                    _logger.Error("repository is null in PrivateXeroMethods CreateInvoiceXero method");
                    return false;
                }
                XeroApi.Model.Organisation organisation = repository.Organisation;
                XeroApi.Model.Payment payment = CreateInvoice(payPalvm, repository);
                payPalvm.OnlineInvoiceLink = RetrieveOnlineInvoiceUrl(payment.Invoice.InvoiceID);
                byte[] invoicePdf = repository.FindById<XeroApi.Model.Invoice>(payment.Invoice.InvoiceID.ToString(), MimeTypes.ApplicationPdf);
        SmtpMailSendingService.SendMailOnInvoiceCreate(payPalvm, invoicePdf, payment.Invoice.InvoiceNumber);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(uName + "~Execution of PrivateXeroMethods CreateInvoiceXero completed. ", ex);
                throw;
            }
        }

        private XeroApi.Model.Payment CreateInvoice(PaypalPaymentDetailsViewModel payPalvm, Repository repository)
        {
            var createdInvoice = new XeroApi.Model.Invoice();
            try
            {
                LineItems items = new LineItems();
                items.Add(new XeroApi.Model.LineItem()
                {
                    Description = payPalvm.Subscription,
                    Quantity = 1,
                    UnitAmount = payPalvm.Amount,
                    AccountCode = "200",  //Sales
                    TaxType = "NONE"
                });

                var contact = new XeroApi.Model.Contact { EmailAddress = payPalvm.EmailId, Name = payPalvm.FirstName + " " + payPalvm.LastName + " " + "(" + payPalvm.CompanyName + ")" + " " + payPalvm.SaasuOrXero };
                repository.UpdateOrCreate(contact);
                XeroApi.Model.Invoice invoice = new XeroApi.Model.Invoice()
                {
                    Type = "ACCREC",
                    Contact = contact,
                    LineItems = items,
                    Date = DateTime.Now,
                    DueDate = DateTime.Now,
                    Status = "AUTHORISED",
                    SentToContact = true
                };

                createdInvoice = repository.Create(invoice);
                XeroApi.Model.Payment payment = new XeroApi.Model.Payment();
                if (createdInvoice.ValidationStatus == ValidationStatus.OK)
                {
                    payment = SavepaymentXero(createdInvoice, repository);
                }
                return payment;
            }
            catch (Exception ex)
            {
                _logger.Error(uName + " ~Execution of PrivateXeroMethods CreateInvoice completed.", ex);
              SmtpMailSendingService.SendPaymentFailureMail(payPalvm);
                DeleteInvoice(createdInvoice, repository);
                throw;
            }
        }

        private void DeleteInvoice(XeroApi.Model.Invoice invoice, Repository repository)
        {
            try
            {
                _logger.Info(uName + " ~Execution of PrivateXeroMethods DeleteInvoice started.");
                var deleteThisInvoice = new XeroApi.Model.Invoice { Status = "VOIDED", InvoiceID = invoice.InvoiceID };
                var DeleteInvoice = repository.UpdateOrCreate<XeroApi.Model.Invoice>(deleteThisInvoice);
                if (DeleteInvoice.ValidationStatus == ValidationStatus.ERROR)
                {
                    foreach (var message in DeleteInvoice.ValidationErrors)
                    {
                        _logger.Error(uName + "PrivateXeroMethods DeleteInvoice Method ValidationErrors" + message.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(uName + " ~Execution of PrivateXeroMethods DeleteInvoice completed.", ex);
                throw;
            }
        }

        //method used for update awaiting payments invoice(update sent mail flag).
        private void MailSentFlagXero(XeroApi.Model.Payment payment, Repository repository)
        {
            try
            {
                _logger.Info(uName + "~Execution of PrivateAuthenticator MailSentFlagXero started.");
                payment.Invoice.SentToContact = true;
                //update the invoice
                var sentFlag = repository.UpdateOrCreate<XeroApi.Model.Invoice>(payment.Invoice);
                if (sentFlag.ValidationStatus == ValidationStatus.ERROR)
                {
                    foreach (var message in sentFlag.ValidationErrors)
                    {
                        _logger.Error(uName + " " + message.Message);
                    }
                }
                _logger.Info(uName + "~Execution of PrivateAuthenticator MailSentFlagXero completed.");
            }
            catch (Exception ex)
            {
                _logger.Error(uName + "~Execution of PrivateAuthenticator MailSentFlagXero completed. ", ex);
                throw;
            }
        }


        private XeroApi.Model.Payment SavepaymentXero(XeroApi.Model.Invoice createdInvoice, Repository repository)
        {
            _logger.Info(uName + " ~Execution of PrivateXeroMethods SavepaymentXero started.");
            try
            {
                XeroApi.Model.Payment newPayment = new XeroApi.Model.Payment
                {
                    Invoice = createdInvoice,
                    Date = DateTime.Now,
                    Amount = createdInvoice.AmountDue.Value,
                    Account = new XeroApi.Model.Account { Code = "200" } //Sales
                };
                var paymentStatus = repository.UpdateOrCreate<XeroApi.Model.Payment>(newPayment);

                if (paymentStatus.ValidationStatus == ValidationStatus.ERROR)
                {
                    foreach (var message in paymentStatus.ValidationErrors)
                    {
                        _logger.Error(uName + " " + message.Message);
                    }
                }

                _logger.Info(uName + " ~Execution of  PrivateXeroMethods SavepaymentXero completed.");
                return paymentStatus;
            }
            catch (Exception ex)
            {
                _logger.Info(uName + " ~Execution of PrivateXeroMethods SavepaymentXero completed.", ex);
                throw;
            }
        }

    }
}