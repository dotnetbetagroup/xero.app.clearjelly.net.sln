﻿using ClearJelly.Configuration;
using Microsoft.Azure;
using Microsoft.Azure.Management.Sql;
using Microsoft.Azure.Management.Sql.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Data.SqlClient;

namespace ClearJelly.Web.Common
{
    /// <summary>
    /// Summary description for AzureConnection
    /// </summary>
    public static class AzureConnection
    {
        static AuthenticationResult _token;
        static SqlManagementClient _sqlMgmtClient;
        static string _resourceGroupName = string.Empty;
        static string _serverName = string.Empty;
        static string _databaseEdition = string.Empty;
        static string _databasePerfLevel = string.Empty;
        static AzureConnection()
        {
            // https://azure.microsoft.com/documentation/articles/resource-group-authenticate-service-principal/
            string _subscriptionId = "88454fdc-e309-47bf-a528-aed24e4097b4";
            string _tenantId = "78befcbb-ecce-410f-8e05-8bd8c101031a";// directory Id
            string _applicationId = "a70a4590-2d0d-4a2a-8f2c-ad5348f80ba6";
            string _applicationSecret = "oPJYEf24/wN43TiZX4q7Q6W6U/3AWTF+VUcMv+oycls="; // one time when register app.

            // Create management clients for the Azure resources your app needs to work with.
            // This app works with Resource Groups, and Azure SQL Database.
            _token = GetToken(_tenantId, _applicationId, _applicationSecret);

            // Instantiate management clients:
            _sqlMgmtClient = new SqlManagementClient(new TokenCloudCredentials(_subscriptionId, _token.AccessToken));

            // Authentication token
            // Azure resource variables
            _resourceGroupName = "Demo_Group";
            string _resourceGrouplocation = "Australia East";

            string _serverlocation = _resourceGrouplocation;
            _serverName = Common.GetAzureServerName();
            string _serverAdmin = "Nikin";
            string _serverAdminPassword = "ze@lous2016";

            string _firewallRuleName = "ClientIPAddress_2017-1-5_15-55-42";
            //string _startIpAddress = GetLocalIPAddress();
            //string _endIpAddress = GetLocalIPAddress();

            _databaseEdition = DatabaseEditions.Standard;
            _databasePerfLevel = "S2"; // "S0", "S1", and so on here for other tiers
        }

        private static AuthenticationResult GetToken(string tenantId, string applicationId, string applicationSecret)
        {
            AuthenticationContext authContext = new AuthenticationContext("https://login.windows.net/" + tenantId);
            _token = authContext.AcquireToken("https://management.core.windows.net/", new ClientCredential(applicationId, applicationSecret));
            return _token;
        }

        public static DatabaseCreateOrUpdateResponse CreateOrUpdateDatabase(string companyName, string userName, string password)
        {
            try
            {
                // Retrieve the server that will host this database
                Server currentServer = _sqlMgmtClient.Servers.Get(_resourceGroupName, _serverName).Server;

                var originalDb = _sqlMgmtClient.Databases.Get(_resourceGroupName, _serverName, "Demo_Clearjelly");
                // Create a database: configure create or update parameters and properties explicitly
                string databaseName = "Xero_" + companyName;
                DatabaseCreateOrUpdateParameters newDatabaseParameters = new DatabaseCreateOrUpdateParameters()
                {
                    Location = currentServer.Location,
                    Properties = new DatabaseCreateOrUpdateProperties()
                    {
                        CreateMode = DatabaseCreateMode.Copy,
                        Edition = _databaseEdition,
                        RequestedServiceObjectiveName = _databasePerfLevel,
                        SourceDatabaseId = originalDb.Database.Id
                    }
                };
                DatabaseCreateOrUpdateResponse dbResponse = _sqlMgmtClient.Databases.CreateOrUpdate(_resourceGroupName, _serverName, databaseName, newDatabaseParameters);
                return dbResponse;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static void DeleteDatabase(string databaseName)
        {
            try
            {
                // Retrieve the server that will host this database
                _sqlMgmtClient.Databases.Delete(_resourceGroupName, _serverName, databaseName);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void CreateUserForCurrentDb(string companyName, string userName, string password)
        {
            SqlConnection MynewConnection = new SqlConnection();
            SqlDataReader myReadernew = null;
            try
            {
                var connectionStringUser = ConfigSection.AzureUserDatabase;
                string tmpConnnew = connectionStringUser + "database=Xero_" + companyName + ";";
                string sqlStrnew = "IF NOT EXISTS (SELECT[name] FROM sys.database_principals WHERE[name] = '" + userName + "')BEGIN  CREATE USER [" + userName + "] WITH PASSWORD = '" + password + "';END EXEC sp_addrolemember 'db_owner', '" + userName + "';";


                // string sqlStrnew = @"CREATE USER [" + userName + "] WITH PASSWORD ='" + password + "' EXEC sp_addrolemember 'db_owner', '" + userName + "';";

                MynewConnection = new SqlConnection(tmpConnnew);
                MynewConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStrnew, MynewConnection);
                // myCommand.CommandTimeout = 10;
                myReadernew = myCommand.ExecuteReader();
            }
            catch (SqlException ex)
            {
                throw;
                //_logger.Fatal(UserName + " ~An SQL error occurred during execution of GetUserDBName method. Error details:", ex.Message);
            }
            finally
            {
                if (MynewConnection != null)
                {
                    MynewConnection.Close();
                }
                if (myReadernew != null)
                {
                    myReadernew.Close();
                }
            }
        }



        public static void UpdateUserForCurrentDb(string companyName, string userName, string password)
        {
            SqlConnection MynewConnection = new SqlConnection();
            SqlDataReader myReadernew = null;
            try
            {
                var connectionStringUser = ConfigSection.AzureUserDatabase;
                string tmpConnnew = connectionStringUser + "database=Xero_" + companyName + ";";
                //string sqlStrnew = "IF NOT EXISTS (SELECT[name] FROM sys.database_principals WHERE[name] = '"+ userName + "')BEGIN  CREATE USER "+ userName + " WITH PASSWORD = '"+ password + "';END EXEC sp_addrolemember 'db_owner', '" + userName + "';";


                string sqlStrnew = @"IF  EXISTS (SELECT  [name]FROM sys.database_principals WHERE   [name] = '" + userName + "')BEGIN ALTER USER [" + userName + "] WITH PASSWORD = '" + password + "' ; END";

                MynewConnection = new SqlConnection(tmpConnnew);
                MynewConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStrnew, MynewConnection);
                // myCommand.CommandTimeout = 10;
                myReadernew = myCommand.ExecuteReader();
            }
            catch (SqlException ex)
            {
                throw;
                //_logger.Fatal(UserName + " ~An SQL error occurred during execution of GetUserDBName method. Error details:", ex.Message);
            }
            finally
            {
                if (MynewConnection != null)
                {
                    MynewConnection.Close();
                }
                if (myReadernew != null)
                {
                    myReadernew.Close();
                }
            }
        }


        public static string ReturnIP()
        {
            var request = System.Web.HttpContext.Current.Request;
            var ServerVariables_HTTP_X_FORWARDED_FOR = (String)request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var ServerVariables_REMOTE_ADDR = (String)request.ServerVariables["REMOTE_ADDR"];
            string ip = "127.0.0.1";
            if (!string.IsNullOrEmpty(ServerVariables_HTTP_X_FORWARDED_FOR) &&
                !ServerVariables_HTTP_X_FORWARDED_FOR.ToLower().Contains("unknown"))
            {
                ServerVariables_HTTP_X_FORWARDED_FOR = ServerVariables_HTTP_X_FORWARDED_FOR.Trim();
                string[] ipRange = ServerVariables_HTTP_X_FORWARDED_FOR.Split(',');
                ip = ipRange[0];
            }
            else if (!string.IsNullOrEmpty(ServerVariables_REMOTE_ADDR))
            {
                ServerVariables_REMOTE_ADDR = ServerVariables_REMOTE_ADDR.Trim();
                ip = ServerVariables_REMOTE_ADDR;
            }
            return ip;
        }


        public static void SetDatabaseFirewallRule(string companyName, string rulename, string ipAddress)
        {
            SqlConnection MyFireWallConnection = null;
            SqlDataReader myFireWallnew = null;
            var connectionStringUser = ConfigSection.AzureUserDatabase;
            string tmpConnFireWall = connectionStringUser + "database=Xero_" + companyName + ";";

            string email = "";
            string ip = ReturnIP();
            string sqlStrnew = @"EXEC sp_set_database_firewall_rule @name = N'" + rulename + "', @start_ip_address = '" + ipAddress + "', @end_ip_address = '" + ipAddress + "'";
            try
            {
                MyFireWallConnection = new SqlConnection(tmpConnFireWall);
                MyFireWallConnection.Open();

                SqlCommand myCommand = new SqlCommand(sqlStrnew, MyFireWallConnection);
                // myCommand.CommandTimeout = 10;
                myFireWallnew = myCommand.ExecuteReader();

                while (myFireWallnew.Read())
                {
                    email = myFireWallnew.GetString(0);
                }
            }
            catch (SqlException ex)
            {
                throw;
                //_logger.Fatal(UserName + " ~An SQL error occurred during execution of GetUserDBName method. Error details:", ex.Message);
            }
            finally
            {
                if (MyFireWallConnection != null)
                {
                    MyFireWallConnection.Close();
                }
                if (myFireWallnew != null)
                {
                    myFireWallnew.Close();
                }

            }
        }
    }
}