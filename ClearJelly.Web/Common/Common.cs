﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using NLog;
using System.Data.Entity;
using ClearJelly.Entities;
using ClearJelly.XeroApp;
using ClearJelly.ViewModels.PayPalViewModels;
using PaypalIntegration.PayPallHelper;
using PaypalIntegration;
using ClearJelly.ViewModels.Admin;
using ClearJelly.Configuration;
using Dapper;
using System.Web.Configuration;
using System.Net.Configuration;

namespace ClearJelly.Web.Common
{
    public static class Common
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();

        public static string[] SqlBlackList = { ";", "=" };

        public static void CheckInput(string parameter)
        {
            CompareInfo comparer = CultureInfo.InvariantCulture.CompareInfo;

            //for (int i = 0; i < SqlBlackList.Length; i++)
            //{
            //    if (comparer.IndexOf(parameter, SqlBlackList[i], CompareOptions.IgnoreCase) >= 0)
            //    {
            //        //
            //        //Handle the discovery of suspicious Sql characters here
            //        //
            //        HttpContext.Current.Response.Redirect("~/Error.aspx");  //generic error page on your site
            //    }
            //}
        }

        public static MailMessage GetMailMessage(EmailTemplates emailTemplate, Dictionary<string, string> contentParameters, string toAddress, string takeAttachmentsFrom = "")
        {
            try
            {
                var template = _clearJellyContext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == (int)emailTemplate);
                var mailMessage = new MailMessage();
                mailMessage.Subject = template.Subject;
                mailMessage.From = new MailAddress("modelerproject@outlook.com", "Agility Planning");
                mailMessage.To.Add(toAddress);
                var content = template.TemplateContent;
                foreach (var pair in contentParameters)
                {
                    content = content.Replace(pair.Key, pair.Value);
                }
                mailMessage.Body = content;

                if (!string.IsNullOrWhiteSpace(takeAttachmentsFrom))
                {
                    foreach (var attachment in template.EmailAttachments)
                    {
                        var attachmentFileName = attachment.AttachmentName;
                        var attachedFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/" + takeAttachmentsFrom + "/"));
                        mailMessage.Attachments.Add(new System.Net.Mail.Attachment(attachedFile + attachmentFileName));
                    }
                }
                mailMessage.IsBodyHtml = true;
                return mailMessage;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Common GetMailMessage event completed. :" + ex.Message, ex);
                throw;
            }
        }

        public static bool GetEnableSSLKey()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetEnableSSLKey event started.");
                var configuation = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                MailSettingsSectionGroup settings = (MailSettingsSectionGroup)configuation.GetSectionGroup("system.net/mailSettings");
                if (settings.Smtp.Network.EnableSsl == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetDataBaseName event completed.", ex);
                throw;
            }

        }

        public static Dictionary<string, string> GetRegistrationSuccessPair(User registeredUser, string email, string currentDomain)
        {
            var clearJellyLink = currentDomain + "Account/Login.aspx";
            var replacePair = new Dictionary<string, string>();
            var activationCode = registeredUser.ActivationCode;

            var activationLink = currentDomain + "Account/AccountActivation.aspx?email=" + email + "&code=" + activationCode;
            replacePair.Add(EmailParameters.FirstName, registeredUser.FirstName);
            replacePair.Add(EmailParameters.LastName, registeredUser.LastName);
            replacePair.Add(EmailParameters.LoginLink, "<a href = " + clearJellyLink + "> " + clearJellyLink + "</a>");
            replacePair.Add(EmailParameters.ActivationLink, "<a href = " + activationLink + " > " + activationLink + " </a>");

            return replacePair;
        }

        public static void SendActivationLink(string toAddress, User registeredUser, string currentDomain)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Registred_user"] ?? "") + " ~Execution of Register SendActivationLink event started.");

                var replacePair = GetRegistrationSuccessPair(registeredUser, toAddress, currentDomain);
                var attachmentFrom = GetEmailAttachmentFrom();
                var mailMessage = GetMailMessage(EmailTemplates.RegistrationSuccessEmail, replacePair, toAddress, attachmentFrom);
                mailMessage.IsBodyHtml = true;
                mailMessage.CC.Add(GetCCEmailId());
                SmtpClient mailclient = new SmtpClient();
                mailclient.ServicePoint.MaxIdleTime = 2;
                if (Common.GetEnableSSLKey())
                {
                    mailclient.EnableSsl = true;
                }
                mailclient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register SendActivationLink event completed. :" + ex.Message, ex);
                throw;
            }

        }

        public static bool UserExists(String email)
        {
            try
            {
                _logger.Info((email ?? "") + " ~Execution of Common UserExists event started.");
                if (_clearJellyContext.Users.Any(x => x.Email.ToLower() == email.ToLower()))
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error((email ?? "") + "Execution of Common UserExists event completed. :" + ex.Message, ex);
                throw;
            }
        }
        public static bool EmailExists(String email)
        {
            try
            {
                _logger.Info((email ?? "") + " ~Execution of Common UserExists event started.");
                if (_clearJellyContext.Users.Any(x => x.UserEmail.ToLower() == email.ToLower()))
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error((email ?? "") + "Execution of Common UserExists event completed. :" + ex.Message, ex);
                throw;
            }
        }
        
        public static string GetEmailAttachmentFrom()
        {
            try
            {
                var templateAttachmentFolder = ConfigurationManager.AppSettings["EmailTemplateAttachmentFolder"];
                if (templateAttachmentFolder == null)
                {
                    throw new Exception("EmailTemplateAttachment is not defined.");
                }
                return templateAttachmentFolder;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                throw ex;
            }

        }

        public static bool TestConnection()
        {
            try
            {
                using (var dbcontext = new ClearJellyEntities())
                {
                    dbcontext.Database.Connection.Open();
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/MaintenancePage.aspx");
                return false;
            }
            return true;
        }

        public static string GetCompanyNameByUserId(string emailId)
        {
            try
            {
                _logger.Info(emailId + "Execution of Common GetCompanyNameByUserId event started.");
                var companyName = string.Empty;
                var userDetail = _clearJellyContext.Users.FirstOrDefault(a => a.Email == emailId);
                if (userDetail != null)
                {
                    companyName = userDetail.Company.Name;
                }
                return companyName;
            }
            catch (Exception ex)
            {
                _logger.Error(emailId + "Execution of Common GetCompanyNameByUserId event completed. :", ex);
                throw;
            }
        }

        public static string GetCCEmailId()
        {
            try
            {
                var CCEmail = ConfigurationManager.AppSettings["CCEmailId"];
                if (CCEmail == null)
                {
                    throw new Exception("CC email id is not defined.");
                }
                return CCEmail;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                throw ex;
            }

        }


        public static string GetPaypalSyncUpdate()
        {
            try
            {
                var PaypalSyncUpdate = ConfigurationManager.AppSettings["PaypalSyncUpdate"];
                if (PaypalSyncUpdate == null)
                {
                    throw new Exception("PaypalSyncUpdate is not defined.");
                }
                return PaypalSyncUpdate;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                throw ex;
            }

        }


        public static string GetEmailAttachmentTempFrom()
        {
            try
            {
                var templateTempAttachmentFolder = ConfigurationManager.AppSettings["TempAttachmentFolder"];
                if (templateTempAttachmentFolder == null)
                {
                    throw new Exception("TempAttachment is not defined.");
                }
                return templateTempAttachmentFolder;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetEmailAttachmentTempFrom event completed. :" + ex.Message, ex);
                throw ex;
            }

        }

        #region ExtendSubscriptionUser
        public static bool ExtendSubscriptionUser(string loggedinUser, int userId, int daystoExtend)
        {
            try
            {
                var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                if (user == null)
                {
                    throw new Exception("User not found.");
                }
                var userSubscriptions = _clearJellyContext.CompanySubscriptions.FirstOrDefault(a => a.CompanyId == user.CompanyId && a.StartedAsTrial && a.EndDate != null && a.IsActive == true);
                if (userSubscriptions != null)
                {
                    userSubscriptions.EndDate = userSubscriptions.EndDate.Value.AddDays(daystoExtend);
                    _clearJellyContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common ExtendSubscriptionUser method completed." + ex.Message, ex);
                return false;
            }

        }
        #endregion


        public static string GetSiteUrl()
        {
            try
            {
                var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                if (siteUrl == null)
                {
                    throw new Exception("Invalid Session SiteUrl");
                }
                return siteUrl;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "Execution of Register GetSiteUrl event completed. :" + ex.Message, ex);
                throw ex;
            }

        }

        

        public static string GetDataBaseName()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetDataBaseName event started.");
                var databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                if (databaseName == null)
                {
                    throw new Exception("Database Name is not defined");
                }
                return databaseName;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetDataBaseName event completed.", ex);
                throw;
            }

        }


        public static string GetIpnInclude()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetIpnInclude event started.");
                var ipnInclude = ConfigurationManager.AppSettings["IncludeIPN"];
                if (ipnInclude == null)
                {
                    throw new Exception("IncludeIPN is not defined");
                }
                return ipnInclude;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetIpnInclude event completed.", ex);
                throw;
            }
        }

        public static string GetIncludeAzure()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetIncludeAzure event started.");
                var databaseName = ConfigurationManager.AppSettings["IncludeAzure"];
                if (databaseName == null)
                {
                    throw new Exception("Include Azure is not defined");
                }
                return databaseName;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetIncludeAzure event completed.", ex);
                throw;
            }
        }



        public static string GetAzureServerName()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetAzureServerName event started.");
                var databaseName = ConfigurationManager.AppSettings["AzureServer"];
                if (databaseName == null)
                {
                    throw new Exception("Server Name is not defined");
                }
                return databaseName;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetAzureServerName event completed.", ex);
                throw;
            }

        }



        public static string GetSaasuURL()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetSaasuURL event started.");
                var saasuURL = ConfigurationManager.AppSettings["SaasuURL"];
                if (saasuURL == null)
                {
                    throw new Exception("saasuURL is not defined");
                }
                return saasuURL;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetSaasuURL event completed.", ex);
                throw;
            }

        }

        public static string GetBillingPeriodPaypal()
        {
            try
            {
                _logger.Info(" ~Execution of Common GetBillingPeriodPaypal event started.");
                var billingPeriod = ConfigurationManager.AppSettings["BillingPeriod"];
                if (billingPeriod == null)
                {
                    throw new Exception("billingPeriod is not defined");
                }
                return billingPeriod;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetDataBaseName event completed.", ex);
                throw;
            }

        }

        public static bool CheckSSLCertificateExpiryDate()
        {
            bool FlagSSl = false;
            try
            {
                var certificateName = GetCertificateName();
                var currentUrl = HttpContext.Current.Request.Url;
                _logger.Info("Request.Url" + currentUrl);
                X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);
                foreach (X509Certificate2 certificate in store.Certificates)
                {
                    //_logger.Info("certificate:" + certificate);
                    if (certificate.FriendlyName.ToLower() == certificateName)
                    {
                        var certificateDate = certificate.NotAfter;
                        //_logger.Info("certificate Date:" + certificateDate);
                        if (certificateDate <= DateTime.Now)
                        {
                            _logger.Info($"FlagSSl {certificateDate}: true");
                            FlagSSl = true;
                            //System.Web.HttpContext.Current.Response.Redirect("/MaintenancePage.aspx");
                        }
                    }

                    store.Close();
                }
                return FlagSSl;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + "~Execution of Common CheckSSLCertificateExpiryDate event completed.", ex.Message);
                return FlagSSl;
            }
        }

        public static string GetCertificateName()
        {
            try
            {

                var certificateName = string.Empty;

                if (HttpContext.Current.Session["CertificateName"] == null)
                {
                    string KeyName = Enum.GetName(typeof(SettingEnum), SettingEnum.Certificate);
                    certificateName = GetSettingKeyName(KeyName);
                    HttpContext.Current.Session["CertificateName"] = (string)certificateName;
                }
                else
                {
                    certificateName = HttpContext.Current.Session["CertificateName"].ToString();
                }
                return certificateName;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + "~Execution of Common GetCertificateName event completed.", ex);
                throw;
            }
        }




        public static string GetSettingKeyName(string KeyName)
        {
            try
            {
                _logger.Info("Try get GetCertificateName.");
                var keyValues = string.Empty;
                using (var dbcontext = new ClearJellyEntities())
                {
                    _logger.Info(dbcontext.Settings);
                    var SettingValue = dbcontext.Settings.FirstOrDefault(x => x.Name.ToLower().Contains(KeyName.ToLower()));
                    if (SettingValue != null)
                    {
                        keyValues = SettingValue.Value.ToLower();
                    }
                }
                return keyValues;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetSettingKeyName event completed.", ex.Message);
                throw;
            }
        }



        public static bool UpdateSetting(int id, string keyValue)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + "Execution of Common UpdateSetting event started.");
                var settingData = _clearJellyContext.Settings.FirstOrDefault(x => x.SettingId == id);
                settingData.Value = keyValue;
                settingData.UpdatedDate = DateTime.Now;
                _clearJellyContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + "Execution of Common UpdateSetting event completed.", ex.Message);
                return false;
            }
        }

        

        #region Delete User
        private static void DeleteDBUser()
        {

        }

        private static void DeleteRecuiringProfiles(string loggedinUser, int companyId)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common SuspendRecuiringProfiles method started.");
                var subscriptions = _clearJellyContext.CompanySubscriptions
                    .Where(x => x.CompanyId == companyId);
                var expChkt = new PayPalExpressCheckout();
                foreach (var subscription in subscriptions)
                {
                    if (!string.IsNullOrEmpty(subscription.RecurringProfileId) && subscription.IsActive)
                    {
                        //Delete Recurring Payment profile
                        var result = expChkt.ManageRecurringPaymentsProfileStatus(subscription.RecurringProfileId, PayPalEnums.RecurringProfileAction.Cancel);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common SuspendRecuiringProfiles method completed.", ex.Message);
                throw;
            }
        }

        public static bool DeleteUser(string loggedinUser, int userId)
        {
            try
            {

                using (var dbContextTransaction = _clearJellyContext.Database.BeginTransaction())
                {
                    try
                    {
                        var user = _clearJellyContext.Users.FirstOrDefault(a => a.UserId == userId);
                        if (user == null)
                        {
                            throw new Exception("User not found.");
                        }
                        DeleteRecuiringProfiles(loggedinUser, user.CompanyId);
                        var deletedCompanyId = MoveComapnyToDeleted(loggedinUser, user.CompanyId);
                        var deletefromPaypalSync = _clearJellyContext.PaypalSyncDatas.Where(x => x.Email == user.Email).FirstOrDefault();
                        if (deletefromPaypalSync != null)
                        {
                            _clearJellyContext.PaypalSyncDatas.Remove(deletefromPaypalSync);
                        }
                        Dictionary<int, int> UserMapDictinary = MoveUserToDeleted(loggedinUser, user.CompanyId, deletedCompanyId);
                        MoveDeletedOrganizationToDeleted(loggedinUser, UserMapDictinary);
                        MoveComapnySubscrToDeleted(loggedinUser, user.CompanyId, deletedCompanyId, UserMapDictinary);

                        _clearJellyContext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(loggedinUser + " ~Execution of Common DeleteUser method completed." + ex.Message, ex);
                        dbContextTransaction.Rollback();
                        return false;
                    }
                }
                DeleteUserAllOjbects(loggedinUser, userId);
                //AzureConnection.DeleteDatabase()
                return true;
                _logger.Info(loggedinUser + " ~Execution of Common DeleteUser method started.");

            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common DeleteUser method completed.", ex.Message);
                throw;
            }
        }


        private static void MoveDeletedOrganizationToDeleted(string loggedinUser, Dictionary<int, int> UserMapDictinary)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common MoveComapnySubscrToDeleted method started.");
                foreach (var user in UserMapDictinary)
                {
                    var deletedOrgRecord = _clearJellyContext.DeletedOrganizations.Where(a => a.UserId == user.Key).ToList();
                    foreach (var deletedRecord in deletedOrgRecord)
                    {
                        var deletedUserOrg = new DeletedUserOrganization()
                        {
                            OrgShortCode = deletedRecord.OrgShortCode,
                            OrgName = deletedRecord.OrgName,
                            DeletedDate = deletedRecord.DeletedDate,
                            DeletedUserId = user.Value,
                        };
                        _clearJellyContext.DeletedOrganizations.Remove(deletedRecord);
                        _clearJellyContext.DeletedUserOrganizations.Add(deletedUserOrg);
                    }

                    var modelProcessRemove = _clearJellyContext.ModelProcesses.Where(a => a.UserId == user.Key).ToList();
                    _clearJellyContext.ModelProcesses.RemoveRange(modelProcessRemove);

                }
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common CopyComapnySubscrToDeletedComapnySubscr method completed.", ex);
                throw;
            }
        }

        private static Dictionary<int, int> MoveUserToDeleted(string loggedInUser, int companyId, int deletedCompanyId)
        {
            try
            {
                _logger.Info(loggedInUser + " ~Execution of Common MoveUserToDeleted method started.");
                var companyUsers = _clearJellyContext.Users.Where(a => a.CompanyId == companyId).ToList();
                var maxDeletedUser = _clearJellyContext.DeletedUsers.Any() ? _clearJellyContext.DeletedUsers.Max(a => a.DeletedUserId) : 0;
                Dictionary<int, int> UserMapDictinary = new Dictionary<int, int>();
                var ExplicitDeletedUserId = maxDeletedUser + 1;
                foreach (var user in companyUsers)
                {
                    var deletedUser = new DeletedUser()
                    {
                        DeletedUserId = ExplicitDeletedUserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        ActivationCode = user.ActivationCode,
                        Address = user.Address,
                        Email = user.Email,
                        FBUserId = user.FBUserId,
                        IsActive = user.IsActive,
                        IsAdmin = user.IsAdmin,
                        IsDeleted = user.IsDeleted,
                        LastloginFailed = user.LastloginFailed,
                        LinkedInUserId = user.LinkedInUserId,
                        LoginType = user.LoginType,
                        Mobile = user.Mobile,
                        Organization = user.Organization,
                        Password = user.Password,
                        PaypalPayerId = user.PaypalPayerId,
                        PaypalToken = user.PaypalToken,
                        ProfilePicture = user.ProfilePicture,
                        PwdResetCode = user.PwdResetCode,
                        RegistrationDate = user.RegistrationDate,
                        DeletedCompanyId = deletedCompanyId
                    };
                    _clearJellyContext.DeletedUsers.Add(deletedUser);
                    UserMapDictinary.Add(user.UserId, deletedUser.DeletedUserId);
                    ExplicitDeletedUserId++;
                }
                return UserMapDictinary;

            }
            catch (Exception ex)
            {
                _logger.Error(loggedInUser + " ~Execution of Common MoveUserToDeleted method completed.", ex);
                throw;
            }
        }

        private static int MoveComapnyToDeleted(string loggedInUser, int companyId)
        {
            try
            {
                _logger.Info(loggedInUser + " ~Execution of Common MoveComapnyToDeleted method started.");
                var company = _clearJellyContext.Companies.FirstOrDefault(a => a.CompanyId == companyId);
                var maxDeletedComapny = _clearJellyContext.DeletedCompanies.Any() ?
                    _clearJellyContext.DeletedCompanies.Max(a => a.DeletedCompanyId) : 0;
                if (company == null)
                {
                    throw new Exception("company not found.");
                }
                else
                {
                    var companyData = new DeletedCompany
                    {
                        DeletedCompanyId = maxDeletedComapny + 1,
                        Name = company.Name,
                        Address = company.Address,
                        Website = company.Website,
                        CountryId = company.CountryId.Value
                    };
                    _clearJellyContext.DeletedCompanies.Add(companyData);
                    return companyData.DeletedCompanyId;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(loggedInUser + " ~Execution of Common MoveComapnyToDeleted method completed.", ex);
                throw;
            }
        }
        private static void DeleteUserAllOjbects(string loggedinUser, int userId)
        {
            try
            {
                using (var dbContext = new ClearJellyEntities())
                {
                    _logger.Info(loggedinUser + " ~Execution of Common DeleteDatabaseForUser method started.");
                    var requestedUser = dbContext.Users.First(x => x.UserId == userId);
                    if (requestedUser.IsAdmin)
                    {
                        var email = requestedUser.Email;
                        var sqlStr = @" Execute [dbo].[DELETE_USER_All_Objects] '" + email + "'";
                        dbContext.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
                        //AzureConnection.DeleteDatabase("Xero_" + requestedUser.Company.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common DeleteDatabaseForUser method completed.", ex);
                throw;
            }

        }


        public static void DeActivateSQLUser(string loggedinUser, string email)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common DeActivateSQLUser method started.");
                var sqlStr = @" Execute [dbo].[DeActivateSQLUser] '" + email + "'";
                _clearJellyContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common DeActivateSQLUser method completed.", ex);
                throw;
            }
        }


        //public static void DeActivateSQLUser(string loggedinUser, string email, string companyName)
        //{

        //    SqlConnection MynewConnection = new SqlConnection();
        //    SqlDataReader myReadernew = null;

        //    try
        //    {
        //        _logger.Info(loggedinUser + " ~Execution of Common DeActivateSQLUser method started.");
        //        var sqlStr = @" Execute [dbo].[DeActivateSQLUser] '" + email + "'";
        //        _clearJellyContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlStr);

        //        var connectionStringUser = System.Configuration.ConfigurationManager.ConnectionStrings["AzureUserDatabase"].ConnectionString;

        //        string tmpConnnew = connectionStringUser + "database=Xero_" + companyName + ";";
        //        string sqlStrnew = "IF EXISTS (SELECT[name] FROM sys.database_principals WHERE[name] = '" + email + "')BEGIN Drop USER[" + email + "] END ";

        //        //string sqlStrnew = @"Drop User ["+ email + "]";

        //        MynewConnection = new SqlConnection(tmpConnnew);
        //        MynewConnection.Open();

        //        SqlCommand myCommand = new SqlCommand(sqlStrnew, MynewConnection);
        //        // myCommand.CommandTimeout = 10;
        //        myReadernew = myCommand.ExecuteReader();

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(loggedinUser + " ~Execution of Common DeActivateSQLUser method completed.", ex);
        //        throw;
        //    }
        //    finally
        //    {
        //        if (MynewConnection != null)
        //        {
        //            MynewConnection.Close();
        //        }
        //        if (myReadernew != null)
        //        {
        //            myReadernew.Close();
        //        }
        //    }
        //}

        public static void ActivateSQLUser(string loggedinUser, string email, string companyName, string dbEncryptPass)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common ActivateSQLUser method started.");
                var sqlStr = @" Execute [dbo].[ActivateSQLUser] '" + email + "'";
                _clearJellyContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
                //var password = EncryptDecrypt.DecryptString(dbEncryptPass);
                //AzureConnection.CreateUserForCurrentDb(companyName, email, password);
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common ActivateSQLUser method completed.", ex);
                throw;
            }
        }

        //private static void DeleteUserAllObject(string loggedInUser, int companyId)
        //{
        //    try
        //    {
        //        _logger.Info(loggedInUser + " ~Execution of Common DeleteUserAllObject method started.");
        //        var company = _clearJellyContext.Companies.FirstOrDefault(a => a.CompanyId == companyId);
        //        var companyUsers = company.Users.Where(a => a.CompanyId == companyId).ToList();
        //        var companyuserIds = companyUsers.Select(a => a.UserId).ToList();
        //        //delete additional users.
        //        var additionalUsers = _clearJellyContext.AdditionalUsers.Where(a => a.UserId.HasValue && companyuserIds.Contains(a.UserId.Value));
        //        _clearJellyContext.AdditionalUsers.RemoveRange(additionalUsers);
        //        //delete company subscriptions.
        //        var companySubscriptions = company.CompanySubscriptions.Where(a => a.CompanyId == companyId).ToList();
        //        _clearJellyContext.CompanySubscriptions.RemoveRange(companySubscriptions);
        //        //delete users.
        //        _clearJellyContext.Users.RemoveRange(companyUsers);
        //        //delete company
        //        _clearJellyContext.Companies.Remove(company);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(loggedInUser + " ~Execution of Common DeleteUserAllObject method completed." + ex.Message, ex);
        //        throw;
        //    }
        //}

        private static void MoveComapnySubscrToDeleted(string loggedInUser, int companyId, int deletedCompanyId, Dictionary<int, int> UserMapDictinary)
        {
            try
            {
                _logger.Info(loggedInUser + " ~Execution of Common MoveComapnySubscrToDeleted method started.");
                var companySubscriptions = _clearJellyContext.CompanySubscriptions.Where(a => a.CompanyId == companyId).ToList();
                Dictionary<int, int> SubscriptionMapDictinary = new Dictionary<int, int>();
                var maxDeletedCompanySub = _clearJellyContext.DeletedCompanySubscriptions.Any() ? _clearJellyContext.DeletedCompanySubscriptions.Max(a => a.DeletedCompanySubscriptionId) : 0;
                var explicitDeletedCompanySubscriptionId = maxDeletedCompanySub + 1;
                foreach (var subscription in companySubscriptions)
                {
                    var deletedCompanySubscription = new DeletedCompanySubscription()
                    {
                        DeletedCompanySubscriptionId = explicitDeletedCompanySubscriptionId,
                        DeletedCompanyId = deletedCompanyId,
                        EndDate = subscription.EndDate,
                        IsActive = subscription.IsActive,
                        IsYearlySubscription = subscription.IsYearlySubscription,
                        Quantity = subscription.Quantity,
                        RecurringProfileId = subscription.RecurringProfileId,
                        StartDate = subscription.StartDate,
                        StartedAsTrial = subscription.StartedAsTrial,
                        SubscriptionTypeId = subscription.SubscriptionTypeId,
                        AdditionalUsers = subscription.AdditionalUsers,
                    };
                    _clearJellyContext.DeletedCompanySubscriptions.Add(deletedCompanySubscription);
                    explicitDeletedCompanySubscriptionId++;

                    var addtionalUserList = subscription.AdditionalUsers1.ToList();
                    foreach (var addtionalUser in addtionalUserList)
                    {
                        var deletedUserId = UserMapDictinary.FirstOrDefault(a => a.Key == addtionalUser.UserId).Value;
                        var deletedAdditionalUser = new DeletedAdditionalUser()
                        {
                            DeletedCompanySubscriptionId = deletedCompanySubscription.DeletedCompanySubscriptionId,
                            DeletedUserId = deletedUserId,
                            JoiningDate = addtionalUser.JoiningDate
                        };
                        _clearJellyContext.DeletedAdditionalUsers.Add(deletedAdditionalUser);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(loggedInUser + " ~Execution of Common CopyComapnySubscrToDeletedComapnySubscr method completed.", ex);
                throw;
            }
        }


        #endregion

        #region ReActivate User
        public static bool ReactivateUser(string loggedinUser, int userId)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common ReactivateUser method started.");
                var requestedUser = _clearJellyContext.Users.First(x => x.UserId == userId);
                //reactivate payment profiles.
                ReActivateRecuiringProfiles(loggedinUser, requestedUser.CompanyId);
                var companyUsers = _clearJellyContext.Users.Where(s => s.CompanyId == requestedUser.CompanyId).ToList();
                //delete suspended users.
                foreach (var user in companyUsers)
                {
                    DeleteSuspendedUser(loggedinUser, user.UserId);
                    //activate sql user of admin
                    if (user.IsAdmin)
                    {

                        ActivateSQLUser(loggedinUser, user.Email, user.Company.Name, user.Password);
                    }
                }

                //activate all user
                companyUsers.ForEach(a => a.IsActive = true);
                _clearJellyContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common ReactivateUser method completed.", ex);
                throw;
            }
        }
        private static void ReActivateRecuiringProfiles(string loggedinUser, int companyId)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common SuspendRecuiringProfiles method started.");
                var subscriptions = _clearJellyContext.CompanySubscriptions
                    .Where(x => x.CompanyId == companyId);
                var expChkt = new PayPalExpressCheckout();
                foreach (var subscription in subscriptions)
                {
                    if (!string.IsNullOrEmpty(subscription.RecurringProfileId) && subscription.IsActive)
                    {
                        //Reactivate Recurring Payment profile
                        expChkt.ManageRecurringPaymentsProfileStatus(subscription.RecurringProfileId, PayPalEnums.RecurringProfileAction.Reactivate);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common SuspendRecuiringProfiles method completed.", ex);
                throw;
            }
        }

        #endregion

        #region Suspend User
        public static bool SuspendUser(string loggedinUser, int userId, string reason)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common SuspendUser method started.");
                var suspendedUser = _clearJellyContext.Users.First(x => x.UserId == userId);
                //suspend all payment profiles
                SuspendRecuiringProfiles(loggedinUser, suspendedUser.CompanyId);
                //add suspended users (Not only activated bcz we take payment when user add and also recuiring payment)
                var suspendedCompanyUsers = _clearJellyContext.Users.Where(s => s.CompanyId == suspendedUser.CompanyId).ToList();
                foreach (var user in suspendedCompanyUsers)
                {
                    AddSuspendedUser(loggedinUser, user.UserId, reason);
                    //DeActivate sql user of admin
                    if (user.IsAdmin)
                    {
                        DeActivateSQLUser(loggedinUser, user.Email);
                    }
                }
                //inactive all users.
                suspendedCompanyUsers.ForEach(a => a.IsActive = false);
                //addd terminated user details.
                _clearJellyContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common SuspendUser method completed.", ex);
                throw;
            }
        }

        private static void AddSuspendedUser(string loggedinUser, int userId, string reason)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common AddTerminatedUser method started.");
                var terminatedUser = new SuspendedUser()
                {
                    UserId = userId,
                    Reason = reason,
                    SuspendedDate = DateTime.Now
                };
                _clearJellyContext.SuspendedUsers.Add(terminatedUser);
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common AddTerminatedUser method completed.", ex);
                throw;
            }
        }

        private static void SuspendRecuiringProfiles(string loggedinUser, int companyId)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common SuspendRecuiringProfiles method started.");
                var subscriptions = _clearJellyContext.CompanySubscriptions.Where(x => x.CompanyId == companyId);
                var expChkt = new PayPalExpressCheckout();
                foreach (var subscription in subscriptions)
                {
                    if (!string.IsNullOrEmpty(subscription.RecurringProfileId) && subscription.IsActive)
                    {
                        //Delete Recurring Payment profile
                        expChkt.ManageRecurringPaymentsProfileStatus(subscription.RecurringProfileId, PayPalEnums.RecurringProfileAction.Suspend);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common SuspendRecuiringProfiles method completed.", ex);
                throw;
            }
        }

        private static void DeleteSuspendedUser(string loggedinUser, int userId)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common DeleteTerminatedUser method started.");
                var terminatedUser = _clearJellyContext.SuspendedUsers.FirstOrDefault(a => a.UserId == userId);
                if (terminatedUser != null)
                {
                    _clearJellyContext.SuspendedUsers.Remove(terminatedUser);
                    _clearJellyContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common DeleteTerminatedUser method completed.", ex);
                throw;
            }
        }
        #endregion

        #region GetSubscriptionIds
        public static List<int> GetSubscriptionIds(string currentUser)
        {
            try
            {

                _logger.Info(currentUser + " ~Execution of Common GetSubscriptionIds method started.");
                var businessPremium = "business user premium";
                var AdvisorPremium = "advisor premium";
                var subscriptionIds = _clearJellyContext.SubscriptionTypes.Where(x => x.TypeName.Trim().ToLower().Contains(businessPremium) || x.TypeName.Trim().ToLower().Contains(AdvisorPremium)).Select(x => x.SubscriptionTypeId).ToList();
                var newids = string.Join(" ,", subscriptionIds.ToArray()); ;
                _logger.Info(currentUser + " ~Execution of Common GetSubscriptionIds method started.ids " + newids);
                return subscriptionIds;
            }
            catch (Exception ex)
            {
                _logger.Error(currentUser + " ~Execution of Common GetSubscriptionIds method completed.", ex);
                throw;
            }
        }
        #endregion

        #region CheckPremiumSubscription
        public static bool CheckPremiumSubscription(string email)
        {
            try
            {
                _logger.Info(email + " ~Execution of Common CheckPremiumSubscription method started.");
                var userdetails = Common.GetCurrentLoggedInUser(email);
                var subscriptionIds = GetSubscriptionIds(email);
                var isPremiumSubscription = _clearJellyContext.CompanySubscriptions.Any(x => x.CompanyId == userdetails.CompanyId && x.IsActive == true && (x.EndDate > DateTime.Now || x.EndDate == null) && subscriptionIds.Contains(x.SubscriptionTypeId));
                return isPremiumSubscription;
            }
            catch (Exception ex)
            {
                _logger.Error(email + " ~Execution of Common CheckPremiumSubscription method completed.");
                throw;
            }
        }
        #endregion

        #region GetCurrentLoggedInUser
        public static User GetCurrentLoggedInUser(string loggedInUser)
        {
            try
            {
                _logger.Info(loggedInUser + " ~Execution of Common SaveSubscription method started.");
                var currentUser = loggedInUser;
                var user = _clearJellyContext.Users.First(a => a.Email == currentUser && a.IsActive);
                return user;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedInUser + " ~Execution of Common SaveSubscription method completed.", ex);
                throw;
            }
        }
        #endregion

        #region CheckPremiumSubscriptionReset
        public static bool CheckPremiumSubscriptionReset(User currentUser)
        {
            try
            {
                _logger.Info(currentUser.Email + " ~Execution of Common CheckPremiumSubscriptionReset method started.");
                var subscriptionIds = GetSubscriptionIds(currentUser.Email);
                var companySubscription = _clearJellyContext.CompanySubscriptions.Any(x => x.CompanyId == currentUser.CompanyId && x.IsActive == true && subscriptionIds.Contains(x.SubscriptionTypeId));
                return companySubscription;
            }
            catch (Exception ex)
            {
                _logger.Error(currentUser + " ~Execution of Common SaveSubscription method completed.", ex);
                throw;
            }
        }
        #endregion

        #region Add User Schema
        public static bool addUserSchema(int userId)
        {
            string loggedinUser = "";
            try
            {
                var requestedUser = _clearJellyContext.Users.SingleOrDefault(x => x.UserId == userId && x.ActivationCode != null);
                if (requestedUser == null)
                {
                    return false;
                }
                loggedinUser = requestedUser.Email;
                _logger.Info(loggedinUser + " ~Execution of Common addUserSchema method started.");

                //create database for user.
                //var chosenService = requestedUser.ChosenService;
                //CreateDatabaseForUser(loggedinUser, userId, chosenService);
                requestedUser.IsActive = true;
                requestedUser.ActivationCode = null;
                _clearJellyContext.SaveChanges();
                return true;
            }
            catch (SqlException ex)
            {
                var message = ex.GetBaseException().Message;
                _logger.Error(loggedinUser + " ~Execution of SqlException Common addUserSchema method completed \n\n Message " + message + ".", ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common addUserSchema method completed.", ex);
                throw;
            }
        }

        public static bool CheckDatabaseExistsAsync(string databaseName)
        {
            bool result = false;
            try
            {
                _logger.Info("Start Checkin Database Exists.");
                var connectionStringUser = "Server=tcp:aptest.database.windows.net,1433;Persist Security Info=False;User ID=admin@agilityplanningtst.onmicrosoft.com;Password=Managility@2017;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Authentication=\"Active Directory Password\";Connection Timeout=30;Initial Catalog=master";
                using (SqlConnection connection = new SqlConnection(connectionStringUser))
                {
                    var sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);
                    var count = connection.QueryAsync<int>(sqlCreateDBQuery).Result.FirstOrDefault();
                    result = false;
                    if (count > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" Error Checkin Database Exists.", ex.Message);
                result = false;
            }
            return result;
        }

        //public static bool CheckDatabaseExistsAsync(string databaseName)
        //{
        //    bool result = false;
        //    try
        //    {
        //        var connectionStringUser = ConfigSection.ClearJellyEntities;
        //        using (SqlConnection connection = new SqlConnection(connectionStringUser))
        //        {
        //            var sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);
        //            var count = connection.QueryAsync<int>(sqlCreateDBQuery).Result.FirstOrDefault();
        //            result = false;
        //            if (count > 0)
        //            {
        //                result = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //    }
        //    return result;
        //}

        private static void CreateDatabaseForUser(string loggedinUser, int userId, string service)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common CreateDatabaseForUser method started.");
                var requestedUser = _clearJellyContext.Users.First(x => x.UserId == userId);
                if (!requestedUser.IsAdmin)
                {
                    return;
                }

                if (service == "Quickbooks")
                {
                    var companyName = requestedUser.Company.Name;
                    var userName = requestedUser.Email;
                    var dbEncryptPass = requestedUser.Password;
                    var password = EncryptDecrypt.DecryptString(dbEncryptPass);

                    var sqlStr = @" Execute [dbo].[Create_QuickBooks_DB_Template] '" + companyName + "','" + userName + "','" + password + "'";
                    _clearJellyContext.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
                }
                if (service == "ABC")
                {
                    var companyName = requestedUser.Company.Name;
                    var userName = requestedUser.Email;
                    var dbEncryptPass = requestedUser.Password;
                    var password = EncryptDecrypt.DecryptString(dbEncryptPass);

                    var sqlStr = @" Execute [dbo].[Create_ABC_Financial_DB_Template] '" + companyName + "','" + userName + "','" + password + "'";
                    _clearJellyContext.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
                }
                if (service == "Xero")
                {
                    var companyName = requestedUser.Company.Name;
                    var userName = requestedUser.Email;
                    var dbEncryptPass = requestedUser.Password;
                    var password = EncryptDecrypt.DecryptString(dbEncryptPass);

                    var sqlStr = @" Execute [dbo].[Create_Xero_DB_Template] '" + companyName + "','" + userName + "','" + password + "'";
                    _clearJellyContext.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sqlStr);
                }


            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common CreateDatabaseForUser method completed.", ex);
                throw;
            }

        }

        #endregion

        #region get User From recurring profile id

        public static void resetSqlOldPassword(string email, string newDecryptPassword, string oldDecryptPassword)
        {
            _clearJellyContext.UpdateUserPassword(email, newDecryptPassword, oldDecryptPassword);
        }


        public static PaypalPaymentDetailsViewModel GetUserFromReccuringIdOrCompanyId(int additionalUserId, string reccuringId)
        {
            try
            {
                _logger.Info(" ~Execution of Common GetUserFromReccuringIdOrCompanyId method started.");
                User userDetails = null;
                int CompanySubscriptionId = 0;
                if (!string.IsNullOrEmpty(reccuringId))
                {
                    var subscriptionDetails = _clearJellyContext.CompanySubscriptions.First(x => x.RecurringProfileId == reccuringId);
                    CompanySubscriptionId = subscriptionDetails.CompanySubscriptionId;
                    userDetails = subscriptionDetails.Company.Users.First(x => x.IsAdmin == true);
                }
                else
                {
                    var additionalUserDetails = _clearJellyContext.AdditionalUsers.First(x => x.AdditionalUserId == additionalUserId);
                    int companyID = additionalUserDetails.CompanySubscription.CompanyId;
                    var adminUserDetails = _clearJellyContext.Users.First(x => x.IsAdmin == true && x.CompanyId == companyID);
                    userDetails = adminUserDetails;
                }
                var payPalIPNViewModel = new PaypalPaymentDetailsViewModel();
                payPalIPNViewModel.CompanySubscriptionId = CompanySubscriptionId;
                payPalIPNViewModel.FirstName = userDetails.FirstName;
                payPalIPNViewModel.LastName = userDetails.LastName;
                payPalIPNViewModel.CompanyName = userDetails.Company.Name;
                payPalIPNViewModel.EmailId = userDetails.Email;
                return payPalIPNViewModel;
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common GetUserFromReccuringIdOrCompanyId method completed.", ex);
                throw;
            }
        }


        public static int GetCompanySubscriptionFromRecurringProfileId(string recurringProfileId)
        {
            //var subscriptionDetails = new CompanySubscription();
            int companySubscriptionId = 0;
            if (recurringProfileId != null)
            {
                var subscriptionDetails = _clearJellyContext.CompanySubscriptions.Any(x => x.RecurringProfileId == recurringProfileId);
                if (subscriptionDetails)
                {
                    companySubscriptionId = _clearJellyContext.CompanySubscriptions.FirstOrDefault(x => x.RecurringProfileId == recurringProfileId).CompanySubscriptionId;
                }
            }
            return companySubscriptionId;
        }

        #endregion

        #region CheckSiteUnderMaintenance
        public static bool CheckSiteUnderMaintenance()
        {
            try
            {
                _logger.Info("~Execution of Common CheckSiteUnderMaintenace method started.");
                string KeyName = Enum.GetName(typeof(SettingEnum), SettingEnum.Maintenance);
                var maintenanceValue = GetSettingKeyName(KeyName);
                if (maintenanceValue != null && maintenanceValue.Trim().ToLower() == "true")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of Common CheckSiteUnderMaintenace method completed.", ex);
                throw;
            }
        }
        #endregion


        #region PasswordChange


        public static bool ResetUserPassword(User user, string newPassword, bool isResetPassword)
        {
            try
            {
                _logger.Info(user.Email + " ~Execution of Common ResetUserPassword event started.");
                // step1 : Update Application Password
                var oldDecryptedPassword = EncryptDecrypt.DecryptString(user.Password);
                var encryptPass = EncryptDecrypt.EncryptString(newPassword);

                using (var dbContext = new ClearJellyEntities())
                {

                    user = dbContext.Users
                      .FirstOrDefault(x => x.IsActive && x.UserId == user.UserId && !x.IsDeleted);
                    user.Password = encryptPass;
                    if (isResetPassword)
                    {
                        user.PwdResetCode = null;
                    }
                    bool isUpdateSuccess = false;
                    //step 2 :  Update Sql User password
                    if (user.IsAdmin)
                    {

                        //AzureConnection.UpdateUserForCurrentDb(user.Company.Name, user.Email, newPassword);

                        UpdateUserPassword_Result checkupdateSuccess = _clearJellyContext.UpdateUserPassword(user.Email, oldDecryptedPassword, newPassword).FirstOrDefault();
                        if (checkupdateSuccess.isPasswordUpdateSuccess == 1)
                        {
                            isUpdateSuccess = true;
                            dbContext.SaveChanges();
                        }



                    }
                    else
                    {
                        dbContext.SaveChanges();
                        isUpdateSuccess = true;
                    }

                    //step 3 : if premium user then update jedox password
                    // if sql and user profile password changed successfully
                    if (isUpdateSuccess)
                    {
                        bool checkPremium = Common.CheckPremiumSubscription(user.Email);
                        if (checkPremium)
                        {
                            // resetJedoxPass(user.Email, newPassword, oldDecryptedPassword);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(user.Email + " ~Execution of Common ResetUserPassword event completed.", ex);
                throw;
            }
        }


        #endregion


       




        #region Manage Subscription
        public static bool SaveSubscription(string loggedinUser, ManageSubscriptionViewModel model)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common SaveSubscription method started.");
                if (model.SubscriptionTypeId == null)
                {
                    CreateSubscription(loggedinUser, model);
                }
                else { UpdateSubscription(loggedinUser, model); }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common SaveSubscription method completed.", ex);
                throw;
            }
        }


        private static void CreateSubscription(string loggedinUser, ManageSubscriptionViewModel model)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common CreateSubscription method started.");
                var subscription = new SubscriptionType()
                {
                    AdditionalUserFee = model.AdditionalUserFee,
                    Description = model.Description,
                    Entities = model.Entities,
                    MonthlyFee = model.MonthlyFee,
                    TypeName = model.TypeName,
                    IsActive = model.IsActive,
                    YearlyContractFee = model.YearlyContractFee,
                };
                _clearJellyContext.SubscriptionTypes.Add(subscription);
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common CreateSubscription method completed.", ex);
                throw;
            }
        }

        private static void UpdateSubscription(string loggedinUser, ManageSubscriptionViewModel model)
        {
            try
            {
                _logger.Info(loggedinUser + " ~Execution of Common UpdateSubscription method started.");
                var subscriptionType = _clearJellyContext.SubscriptionTypes.FirstOrDefault(a => a.SubscriptionTypeId == model.SubscriptionTypeId);

                subscriptionType.AdditionalUserFee = model.AdditionalUserFee;
                subscriptionType.Description = model.Description;
                subscriptionType.Entities = model.Entities;
                subscriptionType.MonthlyFee = model.MonthlyFee;
                subscriptionType.TypeName = model.TypeName;
                subscriptionType.IsActive = model.IsActive;
                subscriptionType.YearlyContractFee = model.YearlyContractFee;
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error(loggedinUser + " ~Execution of Common UpdateSubscription method completed.", ex);
                throw;
            }
        }
        #endregion

        #region GetRecurringPaymentDetails

        public static Dictionary<string, string> GetRecurringPaymentDetails(string recurringProfileId)
        {
            var exp = new PayPalExpressCheckout();
            //var resRecurring = exp.BillOutstandingAmount(10, "nothing", "I-7MEVSKEV2K1X");
            var resRecurring = exp.GetRecurringPaymentsProfileDetails(recurringProfileId);
            var obj = new Dictionary<string, string>();
            obj.Add("OutStandingBalance", ((PayPalPaymentResponse)resRecurring).PayPalResponse["OUTSTANDINGBALANCE"]);
            obj.Add("CurrencyCode", ((PayPalPaymentResponse)resRecurring).PayPalResponse["CURRENCYCODE"]);
            return obj;
        }
        public static Dictionary<string, string> CollectOutstandingBalance(string recurringProfileId)
        {
            var exp = new PayPalExpressCheckout();
            //var resRecurring = exp.BillOutstandingAmount(10, "nothing", "I-7MEVSKEV2K1X");
            var resRecurring = exp.BillOutstandingAmount(recurringProfileId);
            var obj = new Dictionary<string, string>();
            obj.Add("ACK", ((PayPalPaymentResponse)resRecurring).PayPalResponse["ACK"]);
            obj.Add("LongMessage", ((PayPalPaymentResponse)resRecurring).PayPalResponse["L_LONGMESSAGE0"]);
            return obj;
        }



        #endregion


    }

    public class EmailParameters
    {
        public const string stParanthasis = "{";
        public const string endParanthasis = "}";

        #region Registration Email Parameters
        public const string FirstName = stParanthasis + "FirstName" + endParanthasis;
        public const string LastName = stParanthasis + "LastName" + endParanthasis;
        public const string LoginLink = stParanthasis + "LoginLink" + endParanthasis;
        public const string ActivationLink = stParanthasis + "ActivationLink" + endParanthasis;
        public const string SafeIPLink = stParanthasis + "SafeIPLink" + endParanthasis;
        #endregion

        #region Registration Success Email to Admin
        public const string UserFullName = stParanthasis + "UserFullName" + endParanthasis;
        public const string MobileNumber = stParanthasis + "MobileNumber" + endParanthasis;
        public const string CompanyName = stParanthasis + "CompanyName" + endParanthasis;
        public const string RegistrationDate = stParanthasis + "RegistrationDate" + endParanthasis;
        public const string SubscriptionType = stParanthasis + "SubscriptionType" + endParanthasis;
        #endregion

        #region Forgot Password
        public const string ResetPasswordLink = stParanthasis + "ResetPasswordLink" + endParanthasis;
        #endregion

        #region SendMail On Api Exception
        public const string OrgName = stParanthasis + "OrgName" + endParanthasis;
        public const string StartDate = stParanthasis + "StartDate" + endParanthasis;
        public const string EndDate = stParanthasis + "EndDate" + endParanthasis;
        public const string FunctionName = stParanthasis + "FunctionName" + endParanthasis;
        #endregion

        #region SendMail On Payment Notification
        public const string PayerEmail = stParanthasis + "PayerEmail" + endParanthasis;
        public const string Amount = stParanthasis + "Amount" + endParanthasis;
        public const string PaymentDate = stParanthasis + "PaymentDate" + endParanthasis;
        public const string PayerFirstName = stParanthasis + "PayerFirstName" + endParanthasis;
        public const string PayerLastName = stParanthasis + "PayerLastName" + endParanthasis;
        public const string Subscription = stParanthasis + "Subscription" + endParanthasis;
        public const string Currency = stParanthasis + "Currency" + endParanthasis;
        public const string InvoiceNumber = stParanthasis + "InvoiceNumber" + endParanthasis;
        public const string PaypalCompanyName = stParanthasis + "PaypalCompanyName" + endParanthasis;
        public const string PayerEmailId = stParanthasis + "PayerEmailId" + endParanthasis;
        public const string RecurringId = stParanthasis + "RecurringId" + endParanthasis;
        public const string OnlineInvoiceLink = stParanthasis + "OnlineInvoiceLink" + endParanthasis;
        #endregion

        #region SendMail On datbasedon'tMatch Error

        public const string EmailId = stParanthasis + "EmailId" + endParanthasis;
        public const string DbName = stParanthasis + "DbName" + endParanthasis;

        #endregion

        #region JedoxError
        public const string UserEmailId = stParanthasis + "UserEmailId" + endParanthasis;
        public const string UserOldPassword = stParanthasis + "UserOldPassword" + endParanthasis;
        public const string UserNewPassword = stParanthasis + "UserNewPassword" + endParanthasis;
        #endregion
    }
}