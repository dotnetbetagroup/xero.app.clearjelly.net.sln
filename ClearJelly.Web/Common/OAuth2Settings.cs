﻿using ClearJelly.Configuration;

namespace ClearJelly.Web.Common
{
    public class OAuth2Settings
    {
        public static string RedirectUri
        {
            get { return ConfigSection.RedirectUri; }
        }
        public static string ClientID
        {
            get { return ConfigSection.ClientId; }
        }
        public static string ClientSecret
        {
            get { return ConfigSection.ClientSecret; }
        }
        public static string DiscoveryUrl
        {
            get { return ConfigSection.DiscoveryUrl; }
        }
        public static string LogPath
        {
            get { return " "; }
        }
    }
}