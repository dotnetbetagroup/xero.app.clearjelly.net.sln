﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace ClearJelly.Web.Common
{
    public class ReCaptchaClass
    {
        //using post request.
        //public static string Validate(string EncodedResponse)
        //{
        //    var client = new System.Net.WebClient();
        //    string siteKey = "6LeHdRsTAAAAABQdqTBBS6t1ZINHnkD7Lz1RO70F";
        //    string PrivateKey = "6LeHdRsTAAAAAA3LPIxOnasDEU-jixBIOgpThfPq";
        //    var request = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify");
        //    var postData = "secret=" + PrivateKey;
        //    postData += "&response=" + EncodedResponse;
        //    var data = Encoding.ASCII.GetBytes(postData);
        //    request.Method = "POST";
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.ContentLength = data.Length;
        //    using (var stream = request.GetRequestStream())
        //    {
        //        stream.Write(data, 0, data.Length);
        //    }
        //    var response = (HttpWebResponse)request.GetResponse();
        //    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //}

        private static NLog.Logger _logger;


        public static string Validate(string EncodedResponse)
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();

            try
            {
                _logger.Info((HttpContext.Current.Session["Registred_user"] ?? "") + " ~Execution of ReCaptchaClass Validate event started.");

                var client = new WebClient();

                string PrivateKey = ConfigurationManager.AppSettings["Google.ReCaptcha.Secret"]; //"6LeHdRsTAAAAAA3LPIxOnasDEU-jixBIOgpThfPq";

                var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse));

                var captchaResponse = JsonConvert.DeserializeObject<ReCaptchaClass>(GoogleReply);

                _logger.Info((HttpContext.Current.Session["Registred_user"] ?? "") + " ~Execution of ReCaptchaClass Validate google response." + GoogleReply);

                return captchaResponse.Success;

            }
            catch (System.Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Registred_user"] ?? "") + "~Execution of ReCaptchaClass Validate event completed. :" + ex.Message, ex);
                throw;
            }

        }

        [JsonProperty("success")]
        public string Success
        {
            get { return m_Success; }
            set { m_Success = value; }
        }

        private string m_Success;

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes
        {
            get { return m_ErrorCodes; }
            set { m_ErrorCodes = value; }
        }


        private List<string> m_ErrorCodes;
    }
}