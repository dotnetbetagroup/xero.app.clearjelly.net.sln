﻿using ClearJelly.Entities;
using ClearJelly.ViewModels.PayPalViewModels;
using ClearJelly.XeroApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PaypalPayments
/// </summary>
/// 
namespace ClearJelly.Web.Common
{

    public static class PaypalPayments
    {
        private static NLog.Logger _logger;
        static PaypalPayments()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public static void AddPaymentsForRecurring(PaypalPaymentDetailsViewModel paymentDetails)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments AddPaymentDetailsToDB event started.");
                if (paymentDetails != null)
                {
                    using (var dbContext = new ClearJellyEntities())
                    {
                        var IsSameMonthRecordExist = dbContext.RecurringPayments.Any(x => x.RecurringProfileId == paymentDetails.RecurringProfileId
                        && x.PaymentDate.Value.Month == paymentDetails.PaymentDate.Value.Month);
                        if (!IsSameMonthRecordExist)
                        {
                            paymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_profile_created;
                            SavePaymentDetailsToDB(paymentDetails);
                        }
                        else
                        {
                            var paymentRecord = dbContext.RecurringPayments.FirstOrDefault(x =>
                         x.PaymentDate.Value.Month == paymentDetails.PaymentDate.Value.Month
                            && x.RecurringProfileId == paymentDetails.RecurringProfileId);
                            if (paymentRecord != null)
                            {
                                paymentRecord.PaymentStatus = paymentDetails.PaymentStatus;
                                paymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_profile_created;
                                paymentRecord.ModifiedDate = DateTime.Now;
                                dbContext.SaveChanges();
                            }
                        }

                        if (paymentDetails.PaymentStatus == (int)PaypalPaymentStatus.completed)
                        {
                            ActivateSubscription(paymentDetails.RecurringProfileId);
                        }
                        //else
                        //{
                        //    DeactivateSubscription(paymentDetails);
                        //}

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments AddPaymentDetailsToDB event completed.", ex);
                throw;
            }
        }


        public static void DeactivateSubscription(PaypalPaymentDetailsViewModel paypalPaymentDetails)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments DeactivateSubscription event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var companySubscription = dbContext.CompanySubscriptions.First(x => x.RecurringProfileId == paypalPaymentDetails.RecurringProfileId);
                    companySubscription.IsActive = false;
                    dbContext.SaveChanges();
                    var getUserCompnay = dbContext.CompanySubscriptions.Any(x => x.CompanyId == companySubscription.CompanyId && x.IsActive == true && x.RecurringProfileId != paypalPaymentDetails.RecurringProfileId);
                    if (!getUserCompnay)
                    {
                        var getUserDetails = dbContext.Users.FirstOrDefault(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                        //Common.DeActivateSQLUser(getUserDetails.Email, getUserDetails.Email,getUserDetails.Company.Name);
                        Common.DeActivateSQLUser(getUserDetails.Email, getUserDetails.Email);
                    }
                    var recurringPayment = dbContext.RecurringPayments.FirstOrDefault(x => x.RecurringProfileId == paypalPaymentDetails.RecurringProfileId);
                    if (recurringPayment == null)
                    {
                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_failed;
                        SavePaymentDetailsToDB(paypalPaymentDetails);
                    }
                    else
                    {
                        recurringPayment.RecurringStatus = (int)PaypalTxnCode.recurring_payment_failed;
                        recurringPayment.ModifiedDate = DateTime.Now;
                        dbContext.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments DeactivateSubscription event completed.", ex);
                throw;
            }
        }


        public static void CancelSubscriptionPaypalResponse(PaypalPaymentDetailsViewModel paypalPaymentDetails)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments CancelSubscription event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var recurringPayment = dbContext.RecurringPayments.FirstOrDefault(x => x.RecurringProfileId == paypalPaymentDetails.RecurringProfileId);
                    if (recurringPayment == null)
                    {
                        paypalPaymentDetails.RecurringStatus = (int)PaypalTxnCode.recurring_payment_profile_cancel;
                        SavePaymentDetailsToDB(paypalPaymentDetails);
                    }
                    else
                    {
                        recurringPayment.RecurringStatus = (int)PaypalTxnCode.recurring_payment_profile_cancel;
                        recurringPayment.ModifiedDate = DateTime.Now;
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Info(" ~Execution of PaypalPayments CancelSubscription event completed.", ex);
            }
        }

        public static void ActivateSubscription(string recurringProfileId)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments ActivateSubscription event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var companySubscription = dbContext.CompanySubscriptions.FirstOrDefault(x => x.RecurringProfileId == recurringProfileId);
                    if (companySubscription != null)
                    {
                        companySubscription.IsActive = true;
                        companySubscription.EndDate = null;
                        dbContext.SaveChanges();
                    }
                    var getUserDetails = dbContext.Users.First(x => x.CompanyId == companySubscription.CompanyId && x.IsAdmin == true);
                    Common.ActivateSQLUser(getUserDetails.Email, getUserDetails.Email, getUserDetails.Company.Name, getUserDetails.Password);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments ActivateSubscription event completed.", ex);
                throw;
            }
        }


        public static void AddPaymentsForAddUser(PaypalPaymentDetailsViewModel paymentDetails)
        {
            try
            {
                SaveAdditionalUserPaymentDetailsToDB(paymentDetails);
                if (paymentDetails.PaymentStatus == (int)PaypalPaymentStatus.completed)
                {
                    ActiveAdditionalUser(paymentDetails.AdditionalUserID);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments ActivateSubscription event completed.", ex);
                throw;
            }
        }
        private static void ActiveAdditionalUser(int AdditionalUserId)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments ActiveAdditionalUser event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    var currentDate = DateTime.Now.Date;
                    var AdduserDetail = dbContext.AdditionalUsers.First(x => x.AdditionalUserId == AdditionalUserId);
                    var userDetail = dbContext.Users.First(x => x.UserId == AdduserDetail.UserId);
                    var companySubscription = dbContext.CompanySubscriptions.Single(x => x.CompanySubscriptionId == AdduserDetail.CompanySubscription.CompanySubscriptionId
                                                                && x.IsActive && (!x.EndDate.HasValue || x.EndDate.Value >= currentDate));
                    userDetail.IsDeleted = false;
                    companySubscription.AdditionalUsers += 1;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments ActiveAdditionalUser event completed.", ex);
                throw;
            }
        }


        public static bool SavePaymentDetailsToDB(PaypalPaymentDetailsViewModel paymentDetails)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments SavePaymentDetailsToDB event started.");
                bool isSuccess = false;
                using (var dbContext = new ClearJellyEntities())
                {
                    var payment = new RecurringPayment();
                    payment.Amount = paymentDetails.Amount;
                    payment.CompanySubscriptionId = paymentDetails.CompanySubscriptionId;
                    payment.Currency = paymentDetails.Currency;
                    payment.NextPaymentDate = paymentDetails.NextPaymentDate;
                    payment.OutstandingBalance = paymentDetails.OutstandingBalance;
                    payment.PayerEmail = paymentDetails.PayerEmail;
                    payment.PayerId = paymentDetails.PayerId;
                    payment.PaymentCycle = paymentDetails.PaymentCycle;
                    payment.PaymentDate = paymentDetails.PaymentDate;
                    payment.PaymentStatus = paymentDetails.PaymentStatus;
                    payment.RecurringCreatedDate = paymentDetails.RecurringCreatedDate;
                    payment.RecurringProfileId = paymentDetails.RecurringProfileId;
                    payment.CreatedDate = DateTime.Now;
                    payment.RecurringStatus = paymentDetails.RecurringStatus;
                    //payment.CompanyId = paymentDetails.CompanyId;
                    dbContext.RecurringPayments.Add(payment);
                    dbContext.SaveChanges();
                    isSuccess = true;
                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments SavePaymentDetailsToDB event completed.", ex);
                throw;
            }
        }


        private static void SaveAdditionalUserPaymentDetailsToDB(PaypalPaymentDetailsViewModel paymentDetails)
        {
            try
            {
                _logger.Info(" ~Execution of PaypalPayments SavePaymentDetailsToDB event started.");
                using (var dbContext = new ClearJellyEntities())
                {
                    bool AdditionalUserIdExists = dbContext.InstantPayments.Any(x => x.AdditionalUserId == paymentDetails.AdditionalUserID);
                    if (AdditionalUserIdExists)
                    {
                        var addtionalUserRecord = dbContext.InstantPayments.FirstOrDefault(x => x.AdditionalUserId == paymentDetails.AdditionalUserID);
                        addtionalUserRecord.PaymentStatus = paymentDetails.PaymentStatus;
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        var payment = new InstantPayment();
                        payment.Amount = paymentDetails.Amount;
                        payment.Currency = paymentDetails.Currency;
                        payment.PayerId = paymentDetails.PayerId;
                        payment.PaymentDate = paymentDetails.PaymentDate;
                        payment.PaymentStatus = paymentDetails.PaymentStatus;
                        payment.PayerEmailId = paymentDetails.PayerEmail;
                        payment.AdditionalUserId = paymentDetails.AdditionalUserID;
                        dbContext.InstantPayments.Add(payment);
                        dbContext.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(" ~Execution of PaypalPayments SavePaymentDetailsToDB event completed.", ex);
                throw;
            }
        }
    }
}