﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using ClearJelly.Entities;
using ClearJelly.Configuration;
using ClearJelly.ViewModels.Account;
using System.Web;

namespace ClearJelly.Web
{
    public partial class Modeller : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;

        public Modeller()
        {
            
            _clearJellyContext = new ClearJellyEntities();
        }

        private void HideConnectButtons(string service)
        {
            if (service == "Xero")
            {
                Master.FindControl("QuickBooksLogo").Visible = false;
                Master.FindControl("XeroLogo").Visible = true;
                Master.FindControl("UserMenu").Visible = true;
            }
            if (service == "Quickbooks")
            {
                var xeroLogo = Master.FindControl("XeroLogo");
                Master.FindControl("XeroLogo").Visible = false;
                Master.FindControl("QuickBooksLogo").Visible = true;
                Master.FindControl("UserMenu").Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //string mail = Session["Jelly_user"].ToString();
            //userName.Value = mail;
            //var currentUser = _clearJellyContext.Users.SingleOrDefault(x => x.IsActive && x.Email == mail && !x.IsDeleted);
            //userPassword.Value = EncryptDecrypt.DecryptString(currentUser.Password);
            //userDbName.Value = "AP_" + currentUser.Company.Name;
            //GetConnectionsAsync();
            string uName = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (String.IsNullOrEmpty(uName))
            {
                Response.Redirect("~/Account/Login.aspx", false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            var email = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            var currentUser = _clearJellyContext.Users.SingleOrDefault(x => x.IsActive && x.Email == email && !x.IsDeleted);
            var userData = (UserAndSubscriptionViewModel)Session["UserSubscriptionDetails"];
            if(userData == null)
            {
                return;
            }
            userName.Value = Session["Jelly_user"].ToString();
            userPassword.Value = userData.Password;
            userDbName.Value = userData.DatabaseName;
            ApiUrl.Value = ConfigSection.ApiEndpoint;
            DBApiUrl.Value = ConfigSection.ApiDBList;
            GetConnections();
            HideConnectButtons(currentUser.ChosenService);

        }

        protected void DropDownList1_DataBound(object sender, EventArgs e)
        {
            var userConnection = DropDownList1.SelectedValue;
        }

        public void GetConnections()
        {
            try
            {
                string url = ConfigSection.ApiDBList + "/api/home/ValidateUser?username=" + userName.Value + "&password=" + userPassword.Value;
                var request = WebRequest.Create(url) as HttpWebRequest;
                request.ContentType = "application/json; charset=utf-8";
                var response = request.GetResponse();

                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                var result = readStream.ReadToEnd();
                RootObject root = JsonConvert.DeserializeObject<RootObject>(result);
                List<string> source = new List<string>();
                foreach (var item in root.ActiveDatabases)
                {
                    source.Add(item.DatabaseName);
                }

                DropDownList1.DataSource = source;
                DropDownList1.DataBind();
                userConnection.Value = source.FirstOrDefault();
            }
            catch (Exception)
            {

            }
        }


    }

    public class ActiveSubscription
    {
        public string SubscriptionName { get; set; }
        public string EndDate { get; set; }
    }

    public class ActiveDatabas
    {
        public string DatabaseName { get; set; }
        public string ServerName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class RootObject
    {
        public List<ActiveSubscription> ActiveSubscriptions { get; set; }
        public List<ActiveDatabas> ActiveDatabases { get; set; }
    }
}