﻿using System;

namespace ClearJelly.Web
{
    public partial class Progress : System.Web.UI.Page
    {
        private int state = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["State"] != null)
            {
                state = Convert.ToInt32(Session["State"].ToString());
            }
            else
            {
                Session["State"] = 0;
            }
            if (state > 0 && state <= 99)
            {
                lblMessages.Text = "Processing...";
                panelProgress.Width = state * 30;
                lblPercent.Text = state * 10 + "%";
                Page.RegisterStartupScript("", "<script>window.setTimeout('window.Form1.submit()',100);</script>");
            }
            if (state == 100)
            {
                panelProgress.Visible = false;
                panelBarSide.Visible = false;
                lblMessages.Text = "Task Completed!";
                Page.RegisterStartupScript("", "<script>window.close();</script>");
            }

        }
    }
}