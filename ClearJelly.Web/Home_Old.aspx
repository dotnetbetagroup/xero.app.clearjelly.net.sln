﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home_Old.aspx.cs" Inherits="ClearJelly.Web.Home_Old" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%= Styles.Render("~/Content/DataTablecss") %>
    <%= Scripts.Render("~/bundles/jqueryDataTable") %>

    <div class="container-fluid gray-bg pt15 pb15 mb15">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="col-md-5">
                            <label class="lab-title">Current Subscription:</label>
                        </div>
                        <div class="col-md-7 text-left">
                            <asp:Label ID="SubscriptionTypeName" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                        </div>
                    </div>
                    <asp:Panel ID="ChangeSubscriptoinPanel" runat="server" Visible="False">
                        <div id="divChangeSubscription">
                            <%--   <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="SubscriptionTypeDropDown" CssClass="form-control"></asp:DropDownList>
                                </div>--%>
                            <div class="col-md-2 pull-right">
                                <input type="button" value="Change Subscription" id="btnChange" class="btn btn-success btn-block" />
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="form-group">
                        <div class="col-md-6">
                            <div class="col-md-5">
                                <label class="lab-title">Expiry Date:</label>
                            </div>
                            <div class="col-md-7 text-left">
                                <asp:Label ID="ExpiryDate" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid gray-bg pt15 pb15 mb15">
        <div class="row">
            <div class="col-lg-12">
                <label class="lab-title">Actions and Maintenance</label>
                <div id="divActionLinksList">
                    <table id="tblActionLink" class="table table-striped cust-table width100">
                        <thead>
                            <tr>
                                <th style="text-align:center" >Authorisation</th>
                                <th  style="text-align:center">Creating the Model</th>
                                <th  style="text-align:center" >Updating the Model</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td  align="center" valign="middle">        
                                     <asp:ImageButton Width="50" runat="server" OnClick="XeroConnect_Click" ImageUrl="Images/Thumbnails/connect.png" />                                     
                                </td>
                                <td  align="center" valign="middle"><a runat="server" href="~/Default.aspx"> <img Width="50" src="Images/Thumbnails/create model.png"/> </a></td>
                                <td  align="center" valign="middle"><a runat="server" href="~/ManualUpdate.aspx"><img Width="50" src="Images/Thumbnails/update.png"/> </a></td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <p >
                                          Add and authorise Xero organisations:      
                                    <br />You can add your organisation to ClearJelly 
                                    </p>
                                </td>
                                <td align="center" valign="middle">
                                    Create your ClearJelly model for the 
                                    <br />authorised organisations. You only 
                                    <br />need to do this once per organisation. 
                                </td>
                                 <td  align="center" valign="middle">
                                     Update your Clear Jelly model by choosing
                                     <br />a start date and the type of update you 
                                     <br />require.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid gray-bg pt15 pb15">
        <div class="row">
            <div class="col-lg-12">
                <label class="lab-title">Links and Downloads</label>
                <div id="divDownloadLinksList">
                    <table id="tblDownloadLink" class="table table-striped cust-table width100">
                        <thead>
                            <tr>
                                <th>Resources</th>
                                <th>Links and Download</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./Scripts/XeroWebApp/Home.js"></script>
</asp:Content>

