﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using ClearJelly.Entities;
using ClearJelly.Configuration;

namespace ClearJelly.Web
{
    public partial class Organization : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        private static NLog.Logger _logger;
        public Organization()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _clearJellyContext = new ClearJellyEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string uName = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            if (String.IsNullOrEmpty(uName))
            {
                Response.Redirect("~/Account/Login.aspx", false);
            }
            //GetUserOrganisations(uName);
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<Xero_User_Org> GetUserOrganisations()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Organization GetUserOrganisations event started.");
                var currentPage = new Organization();
                var orgList = currentPage.GetUserOrganisationsList();
                return orgList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Organization GetUserOrganisations event completed.", ex);
                throw;
            }
        }


        private List<Xero_User_Org> GetUserOrganisationsList()
        {
            string uName = Session["Jelly_user"] != null ? Session["Jelly_user"].ToString() : "";
            try
            {
                _logger.Info(uName + " ~Execution of Organization GetUserOrganisationsList event started.");
                var userCompany = _clearJellyContext.Users.FirstOrDefault(x => x.Email == uName);
                if (userCompany != null)
                {
                    var orgName = _clearJellyContext.Xero_User_Org.Where(x => x.CompanyID == userCompany.CompanyId).ToList().Select(x => new Xero_User_Org
                    {
                        CompanyID = x.CompanyID,
                        isActive = x.isActive,
                        OrgName = x.OrgName,
                        OrgShortCode = x.OrgShortCode,
                    }).ToList();
                    return orgName;
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.Info(uName + " ~Execution of Organization GetUserOrganisationsList event completed.", ex);
                throw;
            }
        }

        [WebMethod]
        public static bool DeleteOrganization(string orgCode, int companyId)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Organization DeleteOrganization  event started.");
                var currentPage = new Organization();
                bool checkSuccess = currentPage.DeleteOrganizationFromTable(orgCode, companyId);
                return checkSuccess;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Organization DeleteOrganization event completed.", ex);
                throw ex;
            }
        }


        public bool DeleteOrganizationFromTable(string orgCode, int companyId)
        {
            using (var dbContext = new ClearJellyEntities())
            {
                using (var dbContextTransaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Organization DeleteOrganizationFromTable  event started.");

                        if (HttpContext.Current.Session["Jelly_user"] == null)
                        {
                            throw new Exception("Session Jelly User is null");
                        }
                        var userID = HttpContext.Current.Session["Jelly_UserId"].ToString();
                        var companyDetail = dbContext.Companies.FirstOrDefault(s => s.CompanyId == companyId);
                        var companyName = string.Empty;
                        if (companyDetail != null)
                        {
                            companyName = companyDetail.Name;

                            var orgToDelete = dbContext.Xero_User_Org.FirstOrDefault(s => s.OrgShortCode == orgCode && s.CompanyID == companyDetail.CompanyId);
                            if (orgToDelete == null)
                            {
                                throw new Exception("Organisation to delete is null");
                            }
                            dbContext.Xero_User_Org.Remove(orgToDelete);

                            var orgDetailsToDelete = dbContext.OrganisationDetails.FirstOrDefault(x => x.OrgShortCode == orgCode && x.CompanyId == companyDetail.CompanyId);
                            if (orgDetailsToDelete != null)
                            {
                                dbContext.OrganisationDetails.Remove(orgDetailsToDelete);
                            }


                            //insert record in DeletedOrganization table
                            var deletedOrg = new DeletedOrganization();
                            deletedOrg.OrgName = orgToDelete.OrgName;
                            deletedOrg.OrgShortCode = orgCode;
                            deletedOrg.UserId = Int32.Parse(userID);
                            deletedOrg.DeletedDate = DateTime.Now;
                            dbContext.DeletedOrganizations.Add(deletedOrg);
                            dbContext.SaveChanges();
                            int checkSuccess = 0;
                            if (Common.Common.CheckDatabaseExistsAsync(companyName))
                            {
                                checkSuccess = DeleteAllOrgRecordFromTables(companyName, orgCode, orgToDelete.OrgName);
                            }
                            else
                            {
                                checkSuccess = 1;
                            }
                            DeleteFromUserCompany_Result chkSuccess = dbContext.DeleteFromUserCompany(companyName, orgCode, orgToDelete.OrgName).FirstOrDefault();
                            if (checkSuccess == 1 && chkSuccess.checksuccess == 1)
                            {
                                dbContextTransaction.Commit();
                                return true;
                            }
                        }
                        return false;
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        _logger.Error((HttpContext.Current.Session["Jelly_Admin_user"] ?? "") + " ~Execution of Organization DeleteOrganizationFromTable event completed.", ex);
                        throw;
                    }
                }
            }
        }





        public int DeleteAllOrgRecordFromTables(string companyName, string orgCode, string orgName)
        {
            var connectionStringUser = ConfigSection.AzureUserDatabase;
            string tmpConnDeleteRecord = connectionStringUser + "database=Xero_" + companyName + ";";
            SqlConnection con = new SqlConnection(tmpConnDeleteRecord);
            try
            {


                SqlCommand cmd = new SqlCommand("DeleteFromUserCompany", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlParameter returnParameter = cmd.Parameters.Add("@returnVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add(new SqlParameter("@companyName", companyName));
                cmd.Parameters.Add(new SqlParameter("@orgShortCode", orgCode));
                cmd.Parameters.Add(new SqlParameter("@orgName", orgName));
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //string retunvalue = (string)cmd.Parameters["@returnVal"].Value;
                int returnValue = Convert.ToInt32(returnParameter.Value);
                return returnValue;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                con.Close();
            }













            // SqlConnection DeleteOrgConnection = null;
            // SqlDataReader DeleteOrgReader = null;
            // int success = 0;
            // //var connectionStringUser = System.Configuration.ConfigurationManager.
            ////ConnectionStrings["AzureUserDatabase"].ConnectionString; ;
            // //string tmpConnDeleteRecord = connectionStringUser + "database=Xero_" + companyName + ";";
            // //string sqlStrnew = @"EXEC DeleteFromUserCompany @companyName = N'" + companyName + "', @orgShortCode = '" + orgCode + "', @orgName = '" + orgName + "'";
            // try
            // {
            //     //DeleteOrgConnection = new SqlConnection(tmpConnDeleteRecord);
            //     //DeleteOrgConnection.Open();

            //     //SqlCommand myCommand = new SqlCommand(sqlStrnew, DeleteOrgConnection);
            //     //// myCommand.CommandTimeout = 10;
            //     //DeleteOrgReader = myCommand.ExecuteReader();
            //     //while (DeleteOrgReader.Read())
            //     //{
            //     //    success = DeleteOrgReader.GetInt16(0);
            //     //}
            // }
            // catch (SqlException ex)
            // {
            //     throw;
            //     //_logger.Fatal(UserName + " ~An SQL error occurred during execution of GetUserDBName method. Error details:", ex.Message);
            // }
            // finally
            // {
            //     if (DeleteOrgConnection != null)
            //     {
            //         DeleteOrgConnection.Close();
            //     }
            //     if (DeleteOrgReader != null)
            //     {
            //         DeleteOrgReader.Close();
            //     }
            // }
        }


        //public bool InsertDeletedOrgTable(string orgName, string orgCode, int userId)
        //{
        //    try
        //    {
        //        _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Organization InsertDeletedOrgTable  event started.");
        //        var deletedOrg = new DeletedOrganization();
        //        deletedOrg.OrgName = orgName;
        //        deletedOrg.OrgShortCode = orgCode;
        //        deletedOrg.UserId = userId;
        //        deletedOrg.DeletedDate = DateTime.Now;
        //        _clearJellyContext.DeletedOrganizations.Add(deletedOrg);
        //        _clearJellyContext.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of Organization InsertDeletedOrgTable  event completed.", ex);
        //        throw;
        //    }
        //}

    }
}