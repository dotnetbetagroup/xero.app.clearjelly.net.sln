﻿using ClearJelly.Configuration;
using System;

namespace ClearJelly.Web
{
    public partial class PlatformChoices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/Register", true);
        }

        protected void Xero_Click(object sender, EventArgs e)
        {
            var isLogin = Request.QueryString["isLogin"];
            if (isLogin != null)
            {
                var isLoginPage = Convert.ToBoolean(isLogin);
                if (isLoginPage)
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
                else
                {
                    Response.Redirect("~/Account/Register.aspx");

                }
            }
            else
            {
                Response.Redirect("~/Account/Register.aspx");

            }

        }

        protected void Saasu_Click(object sender, EventArgs e)
        {
            var isLogin = Request.QueryString["isLogin"];
            if (isLogin != null)
            {
                var isLoginPage = Convert.ToBoolean(isLogin);
                if (isLoginPage)
                {
                    Response.Redirect("https://saasu.clearjelly.net/Account/Login.aspx");
                }
                else
                {
                    Response.Redirect("https://saasu.clearjelly.net/Account/Register.aspx");

                }
            }
            else
            {
                Response.Redirect("https://saasu.clearjelly.net/Account/Register.aspx");

            }
        }

        protected void Quickbook_Click(object sender, EventArgs e)
        {
            var isLogin = Request.QueryString["isLogin"];
            var chosenService = ChosenService.Quickbooks;
            if (isLogin != null)
            {
                var isLoginPage = Convert.ToBoolean(isLogin);
                if (isLoginPage)
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
                else
                {
                    Response.Redirect("~/Account/Register.aspx?chosenService=" + chosenService.ToString());

                }
            }
            else
            {
                Response.Redirect("~/Account/Register.aspx?chosenService=" + chosenService.ToString());

            }

        }
    }
}