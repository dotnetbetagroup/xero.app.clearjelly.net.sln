﻿using ClearJelly.Entities;
using ClearJelly.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace ClearJelly.Web
{
    public partial class Home_Old : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;
        public Home_Old()
        {
            _clearJellyContext = new ClearJellyEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != "valid" && Session["Jelly_user"] == null)
            {
                Response.Redirect("~/Account/Login.aspx");
            }
            if (Convert.ToBoolean(Session["IsSubscriptionExpired"]))
            {
                if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                {
                    Response.Redirect("~/ManageSubscription.aspx");
                }
            }
            if (!IsPostBack)
            {
                GetSubscriptionDetails();
                //GetSubscriptionTypes();
            }
        }

        private void GetSubscriptionDetails()
        {
            var email = Session["Jelly_user"].ToString();
            var currentUser = _clearJellyContext.Users.SingleOrDefault(x => x.IsActive && x.Email == email && !x.IsDeleted);
            if (currentUser != null)
            {
                if (currentUser.IsAdmin)
                {
                    ChangeSubscriptoinPanel.Visible = true;
                }
                var currentDate = DateTime.Now.Date;
                var currentSubscription =
                    _clearJellyContext.CompanySubscriptions.SingleOrDefault(
                        x =>
                            x.CompanyId == currentUser.CompanyId && x.IsActive &&
                            (!x.EndDate.HasValue || x.EndDate.Value >= currentDate));
                if (currentSubscription != null)
                {
                    var subscriptionTypeName = currentSubscription.SubscriptionType.TypeName;
                    if (currentSubscription.EndDate != null)
                    {
                        subscriptionTypeName += " (Trial)";
                    }
                    SubscriptionTypeName.Text = subscriptionTypeName;
                    ExpiryDate.Text = currentSubscription.EndDate.HasValue ? currentSubscription.EndDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "None";
                }
                else
                {
                    SubscriptionTypeName.Text = "None";
                    ExpiryDate.Text = "None";
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<SampleLinkViewModel> GetSampleLinks()
        {
            var homeObj = new Home_Old();
            return homeObj.GetSampleLinksService();
        }

        private List<SampleLinkViewModel> GetSampleLinksService()
        {
            var sampleLinks = _clearJellyContext.SampleLinks.Select(x => new SampleLinkViewModel
            {
                Title = x.Title,
                DownloadLink = x.DownloadLink
            }).ToList();
            return sampleLinks;
        }

        //private void GetSubscriptionTypes()
        //{
        //    var subscriptions = new Dictionary<int, string>();
        //    foreach (SubscriptionType subscription in _clearJellyContext.SubscriptionTypes)
        //    {
        //        subscriptions.Add(subscription.SubscriptionTypeId, subscription.TypeName);
        //    }
        //    SubscriptionTypeDropDown.DataSource = subscriptions;
        //    SubscriptionTypeDropDown.DataTextField = "Value";
        //    SubscriptionTypeDropDown.DataValueField = "Key";
        //    SubscriptionTypeDropDown.DataBind();
        //    SubscriptionTypeDropDown.Items.Insert(0, "-Change Subscription Plan-");
        //}
        protected void XeroConnect_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("./AddXeroOrgs.aspx");
        }
    }
}