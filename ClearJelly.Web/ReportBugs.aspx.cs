﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ClearJelly.Web
{
    public partial class ReportBugs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSumbitBug(object sender, EventArgs e)
        {
            GetBuilds();
        }

        public static void GetBuilds()
        {

            try
            {
                // enter vso account details
                var username = "";
                var password = "";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(
                        new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(
                            System.Text.ASCIIEncoding.ASCII.GetBytes(
                                string.Format("{0}:{1}", username, password))));

                    using (HttpResponseMessage response = client.GetAsync(
                                "https://managility.visualstudio.com/DefaultCollection/ClearJelly/_apis/build/builds").Result)
                    ///wit/workitems/${workitemtypename}?api-version={version}
                    {
                        response.EnsureSuccessStatusCode();
                        string responseBody = response.Content.ToString();
                        Console.WriteLine(responseBody);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}