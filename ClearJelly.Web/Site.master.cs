﻿using NLog;
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ClearJelly.Web
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        private static NLog.Logger _logger;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                _logger = NLog.LogManager.GetCurrentClassLogger();
                // The code below helps to protect against XSRF attacks
                var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid requestCookieGuidValue;
                if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                {
                    // Use the Anti-XSRF token from the cookie
                    _antiXsrfTokenValue = requestCookie.Value;
                    Page.ViewStateUserKey = _antiXsrfTokenValue;
                }
                else
                {
                    // Generate a new Anti-XSRF token and save to the cookie
                    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                    Page.ViewStateUserKey = _antiXsrfTokenValue;

                    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        HttpOnly = true,
                        Value = _antiXsrfTokenValue
                    };
                    if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                    {
                        responseCookie.Secure = true;
                    }
                    Response.Cookies.Set(responseCookie);
                }

                Page.PreLoad += master_Page_PreLoad;
            }
            catch (Exception ex)
            {
                _logger.Error("Site master Page_Init exception: " + ex.Message + "\n" + ex);
            }
            
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    QuickBooksLogo.Visible = false;
                    XeroLogo.Visible = false;
                    // Set Anti-XSRF token
                    ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                    ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                }
                else
                {
                    // Validate the Anti-XSRF token
                    if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                        || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                    {
                        throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Site master master_Page_PreLoad exception: " + ex.Message + "\n" + ex);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Jelly_Admin_user"] != null)
                {
                    Response.Redirect("~/Admin/Index", false);
                }
                if (Session["Jelly_user"] == null)
                {
                    UserMenu.Visible = false;
                    LoginMenu.Visible = true;
                }
                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    UserMenu.Visible = false;
                    LoginMenu.Visible = true;
                }
                else
                {
                    if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                    {
                        ManageUserLinkAdmin.Visible = true;
                    }
                    else
                    {
                        ManageUserLinkUser.Visible = true;
                    }
                    if (Session["Jelly_user"] == null)
                    {
                        UserMenu.Visible = false;
                    }
                    LoginMenu.Visible = false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Site master Page_Load exception: " + ex.Message+ "\n" + ex);
            }

        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
        }
        protected void LogOut_Click(object sender, EventArgs e)
        {
            var AuthenticationManager = Context.GetOwinContext().Authentication;
            AuthenticationManager.SignOut();
            //Session.Remove("Jelly_user");
            //Session["Jelly_user"] = null;
            //Session.Add("LogOut", "1");
            //Session["LogOut"] = "1";
            Response.Redirect("~/Account/Login.aspx");
            

        }
    }
}