﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EditablePivotGrid.Master" AutoEventWireup="true" CodeBehind="PivotGrid.aspx.cs" Inherits="ClearJelly.Web.PivotGrid" %>

<asp:Content ID="pageHead" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../Content/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="../Content/dx.light.css" />
    <%--<script src="Scripts/jquery-2.1.4.js"></script>--%>
    <%--<script src="Scripts/PivotGrid/GeneratePivotGrid.js"></script>--%>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %></h2>   
    <%--<div id="buttonContainer"></div>
    <div id="dxPivotGrid"></div>--%>
    <script>
        $(document).ready(function () {
            var connection  = angular.element(document.getElementById("MainContent_userConnection")).val();
            function loadScript(url, callback) {
                var script = document.createElement("script")
                script.type = "text/javascript";
                if (script.readyState) {  //IE
                    script.onreadystatechange = function () {
                        if (script.readyState === "loaded" || script.readyState === "complete") {
                            script.onreadystatechange = null;
                            GenerateSalesPivotGrid($("#MainContent_userName").val(), $("#MainContent_userPassword").val(), connection);
                        }
                    };
                }
                else {  //Others
                    script.onload = function () {
                        GenerateSalesPivotGrid($("#MainContent_userName").val(), $("#MainContent_userPassword").val(), connection);
                    };
                }

                script.src = url;
                document.getElementsByTagName("head")[0].appendChild(script);
            }

            // call the function...
            loadScript("../Scripts/dx.web.js", function () {
                alert('script ready!');
            });

        });



    </script>
    <style type="text/css">
       
        .filterDivs{
            padding-right:5px;
        }
    </style>
    <div class="dx-viewport">
        <div class="demo-container" id="demo-container">
            <div class="container">
                <div id="sales-fieldchooser" class="clearfix"></div>
                <div class="title-filter"><b>Applied Filters</b></div>
            <div id="filters" class="filter-wrap"></div>
            <div id="sales" class="p-5"></div>
            </div>

        </div>
        <div id="pivot-loader" style="padding-top: 345px;padding-left: 593px;">
        <i class="loader-spin fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
    </div>
    </div>
</asp:Content>


