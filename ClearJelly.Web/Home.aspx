﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="ClearJelly.Web.Home" Async="true" AsyncTimeout="600000" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--    <%= Styles.Render("~/Content/DataTablecss") %>--%>
    <%--   <%= Scripts.Render("~/bundles/jqueryDataTable") %>--%>
    <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/js.cookie.js"></script>
    <link href="Content/themes/base/jquery-ui.min.css" rel="stylesheet" />

    <div class="container-fluid gray-bg mb15 pt15">
        <div class="col-md-12">
            <div class="col-md-10 col-xs-8 mb10">
                <div class="col-md-3 col-sm-5 col-xs-3">
                    <label class="lab-title">Current Subscription:</label>
                </div>
                <div class="col-md-9 col-sm-7 col-xs-9 text-left">
                    <asp:Label ID="SubscriptionTypeName" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                </div>
            </div>
            <div class="alert alert-success display-none" role="alert">
                <a class="close">×</a>
                <strong>Success!&nbsp; </strong><span id="successMsg"></span>
            </div>
            <asp:Panel ID="ChangeSubscriptoinPanel" runat="server" Visible="False">
                <div id="divChangeSubscription">

                    <div class="col-md-2 pull-right">
                        <input type="button" value="Manage Subscriptions" id="btnChange" class="width150 btn btn-primary btn-block" />
                    </div>
                </div>
            </asp:Panel>
            <div class="col-md-10 col-xs-8 mb10">
                <div class="col-md-3 col-sm-5 col-xs-3">
                    <label class="lab-title">Expiry Date:</label>
                </div>
                <div class="col-md-9 col-sm-7 col-xs-9 text-left">
                    <asp:Label ID="ExpiryDate" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                </div>
            </div>
            <div class="col-md-2 pull-right" style="margin-top: 10px;">
                <input type="button" class="width150 btn btn-block  guideButton" id="quickGuide" value="Quick Guide">
            </div>
            <div class="col-md-10 col-xs-8 mb10">
                <div class="col-md-3 col-sm-5 col-xs-3">
                    <label class="lab-title">Database Name:</label>
                </div>
                <div class="col-md-9 col-sm-7 col-xs-9 text-left">
                    <asp:Label ID="UserDatabaseName" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                </div>
            </div>
            <%--<div class="col-xs-12">
                <div class="row">
                    <div class="col-md-10">
                        <div class="col-md-3">
                            <label class="lab-title">Access Details:</label>
                        </div>
                        <div class="col-md-3 text-left">
                            <asp:Label ID="AccessDetails" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <label class="lab-title">Database:</label>
                        </div>
                        <div class="col-md-3 text-left">
                            <asp:Label ID="DatabaseName" runat="server" Style="font-weight: bold; color: black"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>--%>
            <%-- <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-10">
                        
                    </div>
                </div>
            </div>--%>
        </div>

    </div>

    <div>
        <table style="width: 100%">
            <tr>
                <td style="width: 10%; text-align: left">
                    <asp:DropDownList runat="server" Style="width: 220px; font-size: 14px;" ID="CompanyListDropDown" AutoPostBack="True"></asp:DropDownList>
                </td>

                <td style="text-align: right;">
                    <label class="lab-title">For security purposes every IP address that connects to Clear Jelly needs to be registered. Please click on this button:</label>
                    <input type="button" class="width150 btn btn-default btn-default" id="addSafeIP" value="Add Safe IP">
                </td>
            </tr>

            <tr>
                <td>
    </div>
    </td>
            </tr>
        </table>
        <button id="ABC" style="display: none"></button>
    <asp:Label ID="MessageLabel" AutoPostBack="True" runat="server" ForeColor="#336699" />
    <asp:HiddenField ID="processEnded" Value="false" runat="server" />
    <asp:Label ID="AbcUpdateStatus" AutoPostBack="True" runat="server" ForeColor="#336699" />
    <asp:Label ID="SubscriptionStatus" runat="server" ForeColor="#336699" />
    <%--<span class="loaderSpan" id="loaderSpan">
        <img src="Images/ajax-loader-span.gif" />
    </span>--%>
    <div style="overflow: auto;">
        <table class="table table-striped cust-table width50" style="min-width: 970px;">
            <thead>
                <tr>
                    <th colspan="6">Building Your Clear Jelly Model Process Steps (click on the symbol to start) :
                    </th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td>
                        <span style="font-weight: bold; padding-top: 11px;">Authorisation
                        </span>
                        <asp:DropDownList ID="SelectServiceDropdown" Style="font-size: 14px;" runat="server" OnSelectedIndexChanged="SelectServiceDropdown_OnSelectedIndexChanged" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="text-align: center"><span runat="server" id="UpdateModelDateStartText">Start Date</span></th>
                    <th style="text-align: center"><span runat="server" id="UpdateModelDateEndText">End Date</span></th>
                </tr>

                <tr>
                    <td style="text-align: left; width: 30%;">
                        <div>
                            <div class="col-md-4" id="XeroControls" runat="server">
                                <asp:ImageButton ID="Xerobtn" runat="server" Style="cursor: pointer" OnClick="cmdConnect_Click" ImageUrl="Images/Thumbnails/connect_xero_button_blue.png" />
                                <div id="IscompanyNameShow" runat="server">
                                    <span>Company Name: </span>
                                    <asp:Label ID="companyName" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-4" id="QuickBooksControls" runat="server">
                                <div class="row-md-4">
                                    <asp:ImageButton ID="btnC2QB" Style="cursor: pointer" runat="server" AlternateText="Connect to Quickbooks"
                                        ImageAlign="left"
                                        ImageUrl="Images/Thumbnails/C2QB_white_btn_lg_default.png"
                                        OnClick="ImgC2QB_Click" Height="40px" Width="200px" />
                                </div>
                            </div>
                            <div id="AbcControls" runat="server">
                                <div class="col-md-12">
                                    <div class="row-md-12">
                                        <asp:ImageButton ID="ImageButton1" Style="cursor: pointer; border-radius: 5px;" runat="server" AlternateText="Connect to ABC Financial"
                                            ImageAlign="left"
                                            ImageUrl="Images/ABC-logo.png"
                                            OnClick="ImgABC_Click" Height="40px" Width="180px" />
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px">
                                    <asp:Label ID="ClubError" runat="server" />
                                </div>
                            </div>
                        </div>
                    </td>
                    <td style="text-align: left; width: 30%;">
                        <div id="ChooseClubNumber" runat="server">
                            <div>
                                <p>Choose club number</p>
                            </div>
                            <asp:DropDownList ID="ABCClubsList" Style="font-size: 14px;" runat="server" EnableViewState="true" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td style="text-align: left; width: 35%;">
                        <div id="AddClubAbc" runat="server">
                            <div style="padding-bottom: 8px;">Enter club number</div>
                            <asp:TextBox class="" ID="ClubNumber" runat="server" Style="width: 130px" />
                            <asp:Button runat="server" OnClick="AddClub_Click" Text="Add Club" />
                            <asp:Label ID="ClubMessage" runat="server">  </asp:Label>
                        </div>
                    </td>
                    <td style="text-align: left; width: 25%;">
                        <div id="divUpdateButton" runat="server">
                            <div runat="server" id="UpdateModel">
                                <div style="font-weight: bold; padding-top: 8px; padding-right: 2px;">Update the Model</div>
                                <asp:LinkButton ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"><img  src="Images/Thumbnails/update.png"/> </asp:LinkButton>
                            </div>
                        </div>
                    </td>

                    <td runat="server" id="UpdateModelDateStartImg" style="width: 1%;">
                        <asp:Image ID="ImagetxtSD" runat="server" Style="cursor: pointer" src="Images/calendar_icon.png" />
                        <div runat="server" id="UpdateModelDateStart">
                            <div class="col-md-9" style="padding: 0px">
                                <asp:TextBox class="" ID="txtSD" runat="server" Style="width: 130px" />
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <div style="margin-top: 25px;">
                            <asp:Label class="required" ID="DataStartError" runat="server" Style="width: 130px" />
                        </div>
                    </td>

                    <td runat="server" id="UpdateModelDateEndImg" style="width: 1%;">
                        <asp:Image ID="ImagetxtED" runat="server" Style="cursor: pointer" src="Images/calendar_icon.png" />
                        <div runat="server" id="UpdateModelDateEnd">
                            <div class="col-md-9" style="padding: 0px">
                                <asp:TextBox class="" ID="txtED" runat="server" Style="width: 130px" />

                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <div style="margin-top: 25px;">
                            <asp:Label class="required" ID="DataEndError" runat="server" Style="width: 130px" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left">
                        <asp:Label class="required" ID="ProcessEndlabel" runat="server" Visible="False">An email will be sent when process is complete</asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: left" runat="server" id="UpdateModelText">Please choose a start date and end date to specify the period of the update.
                    </td>
                </tr>
            </tbody>

        </table>
    </div>
    <table class="table table-striped cust-table width50">
        <tr>
            <th colspan="4">Accessing Your Model :
            </th>
        </tr>

        <tbody>
            <tr>
                <td style="text-align: left; width: 33.33%;">
                    <a class="font-color" target="_blank" href="https://powerbi.microsoft.com/en-us/downloads/">
                        <img width="50" src="/Images/Thumbnails/step3.png" />
                        <div style="font-weight: bold; padding-top: 5px;">Dashboard (Power BI)</div>
                    </a>
                </td>
                <td style="text-align: left; width: 10.33%;"></td>
                <td style="text-align: left; width: 33.33%;">
                    <img width="50" src="/Images/Thumbnails/clearjellyIcon.png" />
                    <div style="font-weight: bold; padding-top: 5px;">
                        <a class="font-color" target="_blank" href="https://managility.atlassian.net/wiki/spaces/CJKB/overview">ClearJelly Knowledge Base
                        </a>
                    </div>
                </td>

            </tr>
            <tr>

                <td style="text-align: left; width: 33.33%;">Connect to your Clear Jelly Model via Microsoft Power BI. 
                        Download the required client <a id="DownloadpowerBiclient">here</a> (Only available for Windows Operating Systems).
                        Once the client is installed you can start with this: 
                        <a id="sampleDashboardBI">Sample dashboard</a>
                    enter you email and password when prompted. 
                </td>
                <td style="text-align: left; width: 10.33%;"></td>
                <td>Consult the Clear Jelly Knowledge Base for answers to common questions in FAQs, step by step user guides to connect to your Clear Jelly data model in How to and solution to problems in Known issues.</td>
            </tr>
        </tbody>

    </table>
    <br />
    </div>

    <div class="container-fluid gray-bg">
        <div class="row">
            <div class="col-lg-12">
                <label class="lab-title">Links and Downloads</label>
                <div id="divDownloadLinksList">
                    <table id="tblDownloadLink" class="table table-striped cust-table">
                        <thead>
                            <tr>
                                <th>SortOrder</th>
                                <th>Resources</th>
                                <th>Links and Download</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="quickGuidePopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="poupheading">Quick Guide</h4>
                </div>
                <div class="modal-body">
                    <div id="quickGuideHtmldv">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript" src="../Scripts/XeroWebApp/Home.js"></script>
    <script type="text/javascript" src="../Scripts/XeroWebApp/Default.js"></script>
</asp:Content>


