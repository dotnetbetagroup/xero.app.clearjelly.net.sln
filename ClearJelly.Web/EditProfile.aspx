﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="ClearJelly.Web.EditProfile" %>

<%@ Import Namespace="System.Activities.Statements" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="col-md-12 gray-bg">
            <div class="login-title pb10">
                <h1>Edit Profile</h1>
            </div>
            <p>
                <asp:Label runat="server" ID="SuccessMessage" Visible="False" Text="Your changes have been saved successfully!" CssClass="successMessage" />
            </p>
            <p>
                <asp:Label runat="server" ID="FailureMessage" Visible="False" Text="Some error occurred. Please try again after some time" style="color:red" />
            </p>

             <p>
                <asp:Label runat="server" ID="Passwordfailure" Visible="False" Text="Your changes have been saved successfully. But some error occured while updating password" style="color:red" />
            </p>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </p>
            <div class="container-fluid mb30">
                <div class="form-horizontal">
                    <div class="clearfix"></div>
                    <div class="mb10">
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                    </div>
                    <div class="registration-subtitle pb10">
                        <h4>Personal Details:</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="control-label col-xs-4">First Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                        CssClass="text-danger" ErrorMessage="First Name is required." Display="None" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="LastName" CssClass="control-label col-xs-4">Last Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                                        CssClass="text-danger" ErrorMessage="Last Name is required." Display="None" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Email" CssClass="control-label col-xs-4">Email Address<span class="mandatory">&nbsp;*</span></asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Email" CssClass="form-control" ReadOnly="True" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                        CssClass="text-danger" ErrorMessage="Email is required." Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                        ControlToValidate="Email" ErrorMessage="Please enter valid email."
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        CssClass="text-danger" Display="None"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Mobile" CssClass="control-label col-xs-4">Mobile</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Mobile" CssClass="form-control" />
                                    <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="Mobile"
                                        CssClass="text-danger" ErrorMessage="Mobile is required." Display="None" />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="control-label col-xs-4">Password</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                     <asp:RegularExpressionValidator ID="Regex4" runat="server" ControlToValidate="Password"
    ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
    ErrorMessage="Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="control-label col-xs-4">Confirm password</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                                        CssClass="text-danger" ErrorMessage="Password and confirm password do not match." Display="None" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="control-label col-xs-4">Address</asp:Label>
                                <div class="col-xs-8">
                                    <asp:TextBox runat="server" ID="Address" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="control-label col-xs-4">Profile picture</asp:Label>
                                <div class="col-xs-8 mt5">
                                    <asp:FileUpload ID="ProfilePicture" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <asp:Panel ID="CompanyDetails" runat="server" Visible="False">
                        <div class="registration-subtitle pb10 pt5">
                            <h4>Company details:</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="CompanyName" CssClass="control-label col-xs-4">Company Name<span class="mandatory">&nbsp;*</span></asp:Label>
                                    <div class="col-xs-8">
                                        <asp:TextBox runat="server" ID="CompanyName" CssClass="form-control" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="CompanyName"
                                            CssClass="text-danger" ErrorMessage="Company Name is required." Display="None" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="CompanyAddress" CssClass="control-label col-xs-4">Company address</asp:Label>
                                    <div class="col-xs-8">
                                        <asp:TextBox runat="server" ID="CompanyAddress" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <%--<div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="PhoneNumber" CssClass="control-label col-xs-3">Phone number</asp:Label>
                                    <div class="col-xs-8">
                                        <asp:TextBox runat="server" ID="PhoneNumber" CssClass="form-control" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="PhoneNumber"
                                        CssClass="text-danger" ErrorMessage="The PhoneNumber field is required." Display="None" />
                                    </div>
                                </div>
                            </div>--%>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Website" CssClass="control-label col-xs-4">Website</asp:Label>
                                    <div class="col-xs-8">
                                        <asp:TextBox runat="server" ID="Website" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-4">
                                    <asp:Button runat="server" Text="Save" OnClick="SaveProfile_Click" CssClass="btn btn-success btn-block" ID="btnSaveProfile" />
                                </div>
                                <div class="col-md-3">
                                    <% if (Request.QueryString["userId"] != null)
                                        {
                                    %>
                                    <asp:HyperLink runat="server" NavigateUrl="~/ManageUsers/Index.aspx" CssClass="btn btn-default btn-block">
                                        Cancel
                                    </asp:HyperLink>
                                    <% }
                                        else
                                        { %>
                                    <asp:HyperLink runat="server" NavigateUrl="~/Home.aspx" CssClass="btn btn-default btn-block">
                                        Cancel
                                    </asp:HyperLink>
                                    <%
                                        } %>
                                </div>
                                <%--<asp:Button runat="server" Text="Cancel" OnClick="CancelChanges_Click" ID="btnCancelChanges"/>--%>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>


