﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeBehind="PlatformChoices.aspx.cs" Inherits="ClearJelly.Web.PlatformChoices" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">


    <div class="home-main">
        <header>
            <nav class="navbar-default">
                <div class="header-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="logo">
                                    <h1><a href="/">
                                        <img  class="logoImgCss"  src="/images/logo.png" alt="" /></a></h1>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12 responsive-menu">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <div class="top-menu-main">
                                        <ul>
                                            <li><a href="/Home.aspx">Home</a></li>
                                            <li><a href="http://www.clearjelly.net/#clear-jelly-overview">About</a></li>
                                            <li><a href="http://clearjelly.net/#/managility">Contact</a></li>
                                            <li><a href="https://managility.freshdesk.com">Support</a></li>
                                            <li><a id="loginButton">Login</a></li>
                                            <li><a runat="server" id="registerButton" class="login-link">Register
                                            </a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-6 header-right-icon-main">
                                <div class="header-right-icon">
                                    <img src="/images/right-xero-icon.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>


        <div class="platform-choices">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <h2>Select your platform:</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 text-left platform-logo">

                        <a class="xero-logo" href="#"  runat="server"  onserverclick="Xero_Click">
                            <img src="../Images/xero-logo.png"  id="ClearJelly" class="img-responsive" alt="" />
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 text-left platform-logo">
                        <a class="saasu-logo" href="#"  runat="server" onserverclick="Saasu_Click">
                            <img  runat="server" src="../Images/Saasu-logo.png"  class="img-responsive" id="Saasu" alt="" />
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 text-left platform-logo">
                        <a class="quickbooks-logo" href="#"  runat="server" onserverclick="Quickbook_Click">
                            <img  runat="server" src="../Images/Quickbooks-logo.png"  class="img-responsive" id="Img1" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                        <ul>
                            <li><a href="/privacypolicy.aspx">Privacy Policy</a></li>
                            <li><a href="/terms-of-use.aspx">Terms of Use</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 pull-right text-right copy-right-text-main">&copy; 2015 Managility Pty Ltd</div>
                </div>
            </div>
        </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/Scripts/jquery-1.11.2.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/Scripts/bootstrap.js"></script>
    <script src="/Scripts/jquery.lighter.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(function () {
            $('a[href*=#]:not([href=#])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>
</asp:Content>

