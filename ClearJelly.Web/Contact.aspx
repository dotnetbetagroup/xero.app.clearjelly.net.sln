﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <address>
        <strong>Support:</strong>   <a href="mailto:info@clearjelly.net">info@clearjelly.net</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@managilgity.com.au">Marketing@managilgity.com.au</a>
    </address>
</asp:Content>