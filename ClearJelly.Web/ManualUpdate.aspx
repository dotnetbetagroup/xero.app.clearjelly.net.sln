﻿<%@ Page EnableEventValidation="false"  Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManualUpdate.aspx.cs" Inherits="ManualUpdate" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  
      <div>
        
        <table style="width: 50%">
            <tr>
                <td style="width: 300px">
                  <asp:DropDownList runat="server" ID="CompanyListDropDown" AutoPostBack="True" OnSelectedIndexChanged="CompanyListDropDown_SelectedIndexChanged"></asp:DropDownList>
                        <br /><br />
                </td>
            </tr>  
            <tr>
                <td style="width: 300px">
                    <asp:Label ID="lblSD" runat="server" Width="220" Text="Start Date:" Visible="true" />
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <asp:TextBox ID="txtSD" runat="server" Width="220" Text="" Visible="true"/>
                     
                </td>
            </tr>
        </table>
<br /><br />
        <div class="col-lg-12">
            <label class="lab-title">Actions and Maintenance</label>
            <div id="divActionLinksList">
                <table id="tblActionLink" class="table table-striped cust-table width100">
                    <thead>
                        <tr>
                            <th>Processes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><asp:LinkButton runat="server" OnClick="cmdFullUpdate">Complete Update</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td><asp:LinkButton runat="server" OnClick="cmdGLUpdate">Update the General Ledger Model</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td><asp:LinkButton runat="server" OnClick="cmdSalesUpdate">Update the Sales Model</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br />
        <br />
        <asp:Label ID="invoiceCreatedLabel" runat="server" />
    </div>
    <p>
    </p>
    <script type="text/javascript" src="../Scripts/XeroWebApp/Default.js"></script>
</asp:Content>
