﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using ClearJelly.Entities;
using ClearJelly.Web.ViewModels.Admin;
using ClearJelly.Web.Common;
using NetFwTypeLib;
using System.Net;
using System.Collections.Specialized;

namespace ClearJelly.Web
{
    public partial class ManageIPRequest : Page
    {
        private static NLog.Logger _logger;
        private readonly ClearJellyEntities _clearJellyContext;

        public ManageIPRequest()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            _clearJellyContext = new ClearJellyEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest Page_Load event started.");
                if (Session["login"] != "valid" && Session["Jelly_user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
                if (Convert.ToBoolean(Session["IsSubscriptionExpired"]))
                {
                    if (Session["IsAdmin"] != null && Convert.ToBoolean(Session["IsAdmin"]))
                    {
                        Response.Redirect("~/ManageSubscription.aspx");
                    }
                }
                if (!IsPostBack)
                {
                    currentIP.Text = GetUserIP();
                    var isExist = IsIpExists(currentIP.Text);
                    if (isExist)
                    {
                        saveButton.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest Page_Load event completed.", ex);
                throw ex;
            }

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static List<UserSafeIPListViewModel> GetUserIPList()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetUserIPList event started.");
                var currentPage = new ManageIPRequest();
                var ipList = currentPage.GetUserSafeIPList();
                return ipList;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetUserIPList event completed.", ex);
                throw ex;
            }
        }

        [WebMethod]
        public static bool SaveIPAddress()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest SaveIPAddress  event started.");
                var currentPage = new ManageIPRequest();
                //var isSuccess = currentPage.saveIpAddress();
                var loggedinUser = HttpContext.Current.Session["Jelly_user"];
                if (loggedinUser == null)
                {
                    throw new Exception("Invalid session  Jelly_user");
                }
                var email = loggedinUser.ToString();
                //var result = currentPage.APIRequestForSafeIP(email);
                var includeAzure = bool.Parse(Common.Common.GetIncludeAzure());
                var azureresult = currentPage.saveIpAddress(includeAzure);

                return azureresult;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest SaveIPAddress event completed.", ex);
                throw ex;
            }
        }

        private List<UserSafeIPListViewModel> GetUserSafeIPList()
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetUserSafeIPList  event started.");
                if (Session["Jelly_user"] == null)
                {
                    throw new Exception("Invalid session Jelly_user");
                }
                var email = Session["Jelly_user"].ToString();
                var currentUser = GetCurrentUser(email);

                var safeIps = _clearJellyContext.UserSafeIPs.Where(a => a.UserId == currentUser.UserId)
                        .Select(x => new UserSafeIPListViewModel
                        {
                            UserId = x.UserId,
                            IpAddress = x.IPAddress
                        }).ToList();
                return safeIps;
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetUserSafeIPList event completed.", ex);
                throw ex;
            }
        }

        private string GetUserIP()
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetUserIP event started.");
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                var IpAddress = context.Request.ServerVariables["REMOTE_ADDR"];
                //if (ipAddress == null || ipAddress.Contains(":1"))
                //{
                //    string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                //    return Dns.GetHostByName(hostName).AddressList[0].ToString();
                //}
                return IpAddress;
                //local only
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetUserIP event completed.", ex);
                throw;
            }
        }

        private User GetCurrentUser(string email)
        {
            try
            {
                _logger.Info((Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest GetCurrentUser event started.");
                return _clearJellyContext.Users.Single(x => x.Email == email && x.IsActive && !x.IsDeleted);
            }
            catch (Exception ex)
            {

                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest GetCurrentUser event completed :" + ex.Message, ex);
                throw;
            }
        }

        private bool IsIpExists(string currentIP)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest IsIpExists event started.");
                var email = Session["Jelly_user"].ToString();
                var currentUser = GetCurrentUser(email);
                var isExist = _clearJellyContext.UserSafeIPs.Any(a => a.UserId == currentUser.UserId && a.IPAddress == currentIP);
                return isExist;
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest IsIpExists event completed :" + ex.Message, ex);
                throw;
            }
        }

        private bool saveIpAddress(bool includeAzure)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest saveIpAddress event started.");

                var currentIP = GetUserIP();
                var isExist = IsIpExists(currentIP);
                if (!isExist)
                {
                    //RegisterInFirewall(currentIP);
                    var email = Session["Jelly_user"].ToString();
                    var currentUser = GetCurrentUser(email);
                    if (includeAzure)
                    {
                        if (Common.Common.CheckDatabaseExistsAsync("Xero_" + currentUser.Company.Name))
                        {
                            var checkIfRecordExists = _clearJellyContext.UserSafeIPs.FirstOrDefault(x => x.UserId == currentUser.UserId);
                            if (checkIfRecordExists == null)
                            {
                                AzureConnection.SetDatabaseFirewallRule(currentUser.Company.Name, currentUser.Company.Name, currentIP);
                            }
                            else
                            {
                                var checkCount = _clearJellyContext.UserSafeIPs.Where(x => x.UserId == currentUser.UserId).Count();
                                var companyName = currentUser.Company.Name + checkCount;
                                AzureConnection.SetDatabaseFirewallRule(currentUser.Company.Name, companyName, currentIP);
                            }
                        }
                    }
                    AddSafeIP(currentIP, currentUser);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest saveIpAddress event completed :" + ex.Message, ex);
                throw;
            }
        }

        private void AddSafeIP(string currentIP, User currentUser)
        {
            try
            {
                //var email = Session["Jelly_user"].ToString();
                //var currentUser = GetCurrentUser(email);
                var safeIpRecord = new UserSafeIP()
                {
                    UserId = currentUser.UserId,
                    IPAddress = currentIP
                };
                _clearJellyContext.UserSafeIPs.Add(safeIpRecord);
                _clearJellyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest AddSafeIP event completed :" + ex.Message, ex);
                throw;
            }

        }

        //private void RegisterInFirewall(string ip)
        //{
        //    try
        //    {

        //        var ruleName = "Clearjelly Safe IP List";
        //        var isRuleExist = IsFirewallRuleExists(ruleName);

        //        if (!isRuleExist)
        //        {
        //            AddFirewallRule(ip, ruleName);
        //        }
        //        else
        //        {
        //            UpdateFirewallRule(ruleName, ip);
        //        }
        //        //NetFwTypeLib.INetFwRule2 firewallRule2 = fwPolicy2.Rules.Item("PC Block Bad IP Addresses");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //private bool IsFirewallRuleExists(string ruleName)
        //{
        //    try
        //    {
        //        Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
        //        Microsoft.TeamFoundation.Build.Common.INetFwPolicy2 fwPolicy2 = (Microsoft.TeamFoundation.Build.Common.INetFwPolicy2)Activator.CreateInstance(tNetFwPolicy2);
        //        var getRules = fwPolicy2.Rules.OfType<Microsoft.TeamFoundation.Build.Common.INetFwRule>().ToList();
        //        var isRuleExits = getRules.Any(a => a.Name == ruleName);
        //        return isRuleExits;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest IsFirewallRuleExists event completed :" + ex.Message, ex);
        //        throw;
        //    }
        //}

        //private void AddFirewallRule(string IPAddress, string RuleName)
        //{
        //    try
        //    {
        //        Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
        //        Microsoft.TeamFoundation.Build.Common.INetFwPolicy2 fwPolicy2 = (Microsoft.TeamFoundation.Build.Common.INetFwPolicy2)Activator.CreateInstance(tNetFwPolicy2);

        //        var currentProfiles = fwPolicy2.CurrentProfileTypes;

        //        //INetFwRule2
        //        Microsoft.TeamFoundation.Build.Common.INetFwRule firewallRule = (Microsoft.TeamFoundation.Build.Common.INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));

        //        firewallRule.Name = RuleName;
        //        firewallRule.Description = "Block Nasty Incoming Connections from IP Address.";
        //        firewallRule.Action = Microsoft.TeamFoundation.Build.Common.NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
        //        firewallRule.Direction = Microsoft.TeamFoundation.Build.Common.NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
        //        firewallRule.Enabled = true;
        //        firewallRule.InterfaceTypes = "All";
        //        firewallRule.RemoteAddresses = IPAddress;
        //        firewallRule.Protocol = 6; // TCP
        //                                   //firewallRule.LocalPorts = "1433,80,3389,443"; //sql,iis,rpc
        //        firewallRule.LocalPorts = "1433";//sql
        //        firewallRule.Profiles = currentProfiles;
        //        Microsoft.TeamFoundation.Build.Common.INetFwPolicy2 firewallPolicy = (Microsoft.TeamFoundation.Build.Common.INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
        //        firewallPolicy.Rules.Add(firewallRule);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest AddFirewallRule event completed :" + ex.Message, ex);
        //        throw;
        //    }

        //}

        //private void UpdateFirewallRule(string ruleName, string newAdress)
        //{
        //    try
        //    {
        //        Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
        //        Microsoft.TeamFoundation.Build.Common.INetFwPolicy2 fwPolicy2 = (Microsoft.TeamFoundation.Build.Common.INetFwPolicy2)Activator.CreateInstance(tNetFwPolicy2);
        //        Microsoft.TeamFoundation.Build.Common.INetFwRule firewallRule = fwPolicy2.Rules.Item(ruleName);
        //        if (!firewallRule.RemoteAddresses.Contains(newAdress))
        //        {
        //            firewallRule.RemoteAddresses += "," + newAdress;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest UpdateFirewallRule event completed :" + ex.Message, ex);
        //        throw;
        //    }

        //}

        private bool APIRequestForSafeIP(string email)
        {
            try
            {
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest APIRequestForSafeIP event started.");
                var userIP = GetUserIP();
                string url = "http://localhost:90/api/ManageSafeIP/SaveIPAddress";
                string myParameters = "loggedinUser=" + email + "&IP=" + userIP;
                _logger.Info((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Request URL:" + url);
                using (var client = new WebClient())
                {
                    //client.UploadData
                    //var apiResponse = client.DownloadString(url);
                    //client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    byte[] response = client.UploadValues(url, new NameValueCollection()
                                                                    {
                                                                       { "loggedinUser", email },
                                                                       { "IP", userIP }
                                                                   });

                    string result = System.Text.Encoding.UTF8.GetString(response);
                    return Convert.ToBoolean(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error((HttpContext.Current.Session["Jelly_user"] ?? "") + " ~Execution of ManageIPRequest APIRequestForSafeIP event completed.", ex);
                throw;
            }
        }

        //private void RemoveFirewall(IPAddress ipAddress, string ruleName)
        //{
        //    try
        //    {
        //        Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
        //        Microsoft.TeamFoundation.Build.Common.INetFwPolicy2 fwPolicy2 = (Microsoft.TeamFoundation.Build.Common.INetFwPolicy2)Activator.CreateInstance(tNetFwPolicy2);
        //        Microsoft.TeamFoundation.Build.Common.INetFwRule firewallRule = fwPolicy2.Rules.Item(ruleName);
        //        fwPolicy2.Rules.Remove(ruleName);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error((Session["Jelly_user"] ?? "") + "Execution of ManageIPRequest RemoveFirewall event completed :" + ex.Message, ex);
        //        throw;
        //    }
        //}
    }
}
