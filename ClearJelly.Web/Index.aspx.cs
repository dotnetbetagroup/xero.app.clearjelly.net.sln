﻿using ClearJelly.Entities;
using ClearJelly.ViewModels.Account;
using ClearJelly.XeroApp;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClearJelly.Web
{
    public partial class Index : System.Web.UI.Page
    {
        private readonly ClearJellyEntities _clearJellyContext;

        public Index()
        {

            _clearJellyContext = new ClearJellyEntities();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User != null && User.Identity.Name != "")
            {
                Response.Redirect("~/Home.aspx");
            }
        }

        public UserAndSubscriptionViewModel UserSubscriptionDetails(string userName, string password)
        {
            try
            {
                using (var dbContext = _clearJellyContext)
                {
                    UserAndSubscriptionViewModel userSubscriptions = new UserAndSubscriptionViewModel();
                    if (userName != null && password != null)
                    {

                        var userDetails = dbContext.Users.FirstOrDefault(x => x.Email == userName && x.Password == password);


                        if (userDetails != null)
                        {
                            var currentDate = DateTime.Now.Date;
                            var companySubscription = dbContext.CompanySubscriptions.
                                Where(x => x.CompanyId == userDetails.CompanyId
                                && x.IsActive == true
                                && (!x.EndDate.HasValue || x.EndDate.Value >= currentDate)
                            ).ToList();

                            foreach (var item in companySubscription)
                            {
                                ActiveSubscriptions actSub = new ActiveSubscriptions();
                                actSub.SubscriptionName = item.SubscriptionType.TypeName;
                                actSub.EndDate = item.EndDate.HasValue ? item.EndDate.Value.ToShortDateString() : null;
                                userSubscriptions.ActiveSubscriptions.Add(actSub);
                            }
                            var serverName = Common.Common.GetAzureServerName();
                            userSubscriptions.UserName = userDetails.Email + "@" + serverName;
                            // userSubscriptions.UserName = userDetails.Email;
                            userSubscriptions.DatabaseName = "AP_" + userDetails.Company.Name;
                            userSubscriptions.Password = EncryptDecrypt.DecryptString(password);
                            userSubscriptions.ServerName = serverName + ".database.windows.net";
                        }
                    }
                    return userSubscriptions;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}