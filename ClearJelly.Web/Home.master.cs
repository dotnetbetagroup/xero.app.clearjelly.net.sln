﻿using System;

namespace ClearJelly.Web
{
    public partial class HomeMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Jelly_Admin_user"] != null)
            {
                Response.Redirect("~/Admin/Index", false);
            }
        }
    }
}
