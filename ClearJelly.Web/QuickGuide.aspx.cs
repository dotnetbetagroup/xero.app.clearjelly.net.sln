﻿using ClearJelly.Entities;
using ClearJelly.Entities.Enums;
using ClearJelly.QBApp;
using System;
using System.Linq;

namespace ClearJelly.Web
{
    public partial class QuickGuide : System.Web.UI.Page
    {
        private static readonly ClearJellyEntities _clearJellyContext = new ClearJellyEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var dbcontext = new ClearJellyEntities())
            {
                var template = dbcontext.EmailTemplates.FirstOrDefault(a => a.EmailTemplateId == (int)EmailTemplates.quickGuide);
                addVideo.InnerHtml = template.TemplateContent;
            }
        }
    }
}