﻿$(function () {
    $("#MainContent_txtSD").datepicker({ dateFormat: 'dd/M/yy' });
    $("#MainContent_txtSD").attr('readonly', 'readonly');
    $("#clearSD").on("click", function () {
        $("#MainContent_txtSD").val('');
    });
});