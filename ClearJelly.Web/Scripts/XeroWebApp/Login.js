﻿
$(document).ready(function () {
    $(".left-side-main").hide();
    $('#loginForm').parents('.container').parent(".col-md-main-content").addClass('loginParent');
    $('#loginForm').parents('.container').parent(".col-md-main-content").siblings(".navbar").addClass('loginHead');
    
});

//api_key: 75vznq38rc4j2h linkedin for server
var linkedIn = {
    onLinkedInLoad: function () {
        // Setup an event listener to make an API call once auth is complete
        IN.Event.on(IN, "auth", linkedIn.getProfileData);
    },
    onSuccess: function (data) {
        var redirectUrl = getParameterByName('RedirectUrl');
        var linkedinData = {
            "EmailAddress": data.emailAddress,
            "FirstName": data.firstName,
            "LastName": data.lastName,
            "PictureUrl": data.publicProfileUrl,
            "Location": data.location.name,
            "UserId": data.id,
            "RedirectUrl": redirectUrl
        };
        $.ajax({
            url: "Login.aspx/LinkedinLoginCallback",
            contentType: "application/json; charset=utf-8",
            method: "POST",
            data: JSON.stringify({ "linkedinData": linkedinData }),
            success: function (data) {
                var returnUrl = data.d;
                window.location.href = returnUrl;
            },
            error: function () {
                alert("Something went wrong, please try again");
                location.reload();
            }
        });
    },
    onError: function (error) {
        // Handle an error response from the API call
        console.log(error);
    },
    getProfileData: function () {
        // Use the API call wrapper to request the member's basic profile data
        //IN.API.Raw("/people/~:(id,first-name,last-name,headline,picture-url,email-address)").result(linkedIn.onSuccess).error(linkedIn.onError);
        IN.API.Raw("/people/~:(id,first-name,last-name,headline,picture-url,email-address,industry,location:(name),public-profile-url,date-of-birth)").result(linkedIn.onSuccess).error(linkedIn.onError);
        //IN.API.Raw("firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "email-address", "educations", "date-of-birth").result(linkedIn.onSuccess).error(linkedIn.onError);
    },
    init: function () {
    }
};

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();
    }
}

window.fbAsyncInit = function () {
    FB.init({
        appId: facebookAppId, //'1631833673698395', // local host
        //appId: '1456754504636948', // demo : http://demo.zealousys.com/civi_crm/
        cookie: true,  // enable cookies to allow the server to access
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.4' // use version 2.2
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.
};




// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    FB.api('/me', { fields: 'email,last_name,first_name,name' }, function (data) {
        console.log(data);
        var redirectUrl = getParameterByName('RedirectUrl');
        var facebookData = {
            "Name": data.name,
            "Id": data.id,
            "Email": data.email,
            "FirstName": data.first_name,
            "LastName": data.last_name,
            "RedirectUrl": redirectUrl
        };
        $.ajax({
            url: "Login.aspx/FBLoginCallback",
            contentType: "application/json; charset=utf-8",
            method: "POST",
            data: JSON.stringify({ "facebookData": facebookData }),
            success: function (data) {
                var returnUrl = data.d;
                window.location.href = returnUrl;
            },
            error: function () {
                alert("Something went wrong, please try again");
                location.reload();
            }
        });
    });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}