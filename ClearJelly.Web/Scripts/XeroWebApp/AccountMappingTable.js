﻿$(document).ready(function () {
    
    $("#divLoading").show();
    bindCustomUploadGrid();
    $("#successDiv").hide()
    $("#errorDiv").hide()
});


function bindCustomUploadGrid() {

    $.ajax({
        url: 'AccountMapping.aspx/GetCustomUploadList',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#divLoading").hide();

            CreateCustomUploadGrid(data);
            setTimeout(function () {
                $("#MainContent_InfoPanel").hide();
                $("#MainContent_FailPanel").hide();
            }, 5000);
            if ($('#AccountMappingGrid').DataTable().data().any()) {
                $("#ClearAllButton").show()
            } else {
                $("#ClearAllButton").hide()
            }
        },
        error: function () {
            $("#divLoading").hide();
            setTimeout(function () {
                $(".alert").hide();
            }, 5000);
            $(".alert-danger").show();

        }
    });
}


function CreateCustomUploadGrid(data) {
    var strHtml = '';
    strHtml += '<table id=AccountMappingGrid class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Org Name</th>';
    strHtml += '<th>Account Code</th>';
    strHtml += '<th>Account Name</th>';
    strHtml += '<th>Mapping 1 </th>';
    strHtml += '<th>Mapping 2 </th>';
    strHtml += '<th>Mapping 3</th>';
    strHtml += '<th>Mapping 4</th>';
    strHtml += '<th>Mapping 5</th>';
    strHtml += '<th>Mapping 6</th>';
    strHtml += '<th>Mapping 7</th>';
    strHtml += '<th>Mapping 8</th>';
    strHtml += '<th>Mapping 9</th>';
    strHtml += '<th>Mapping 10</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {

        strHtml += '<tr>';
        strHtml += '<td>' + value.OrgName + '</td>';
        strHtml += '<td>' + value.Code + '</td>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.Mapping1 + '</td>';
        strHtml += '<td>' + value.Mapping2 + '</td>';
        strHtml += '<td>' + value.Mapping3 + '</td>';
        strHtml += '<td>' + value.Mapping4 + '</td>';
        strHtml += '<td>' + value.Mapping5 + '</td>';
        strHtml += '<td>' + value.Mapping6 + '</td>';
        strHtml += '<td>' + value.Mapping7 + '</td>';
        strHtml += '<td>' + value.Mapping8 + '</td>';
        strHtml += '<td>' + value.Mapping9 + '</td>';
        strHtml += '<td>' + value.Mapping10 + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#UploadPayrolll").html(strHtml);
    strHtml = '';
    $("#AccountMappingGrid").dataTable({
        "aoColumns": [
        null,
        null,
         null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
       null

       
        ]
    }
    );
}

$(document).on("click", "#ClearAllButton", function () {
    
    bootbox.confirm("Are you sure you want to delete all the records ?", function (result) {
        if (result) {
            DeleteAllRecords();
        }
    });
});


$("input[type=file]").change(function () {
    $("#MainContent_btnUpload").show();
});



function DeleteAllRecords() {
    $("#divLoading").show();
    $.ajax({
        url: 'AccountMapping.aspx/ClearAllData',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $(".alert").hide();
            $("#successDiv").hide()
            $("#errorDiv").hide()
            if (data.d) {
                bindCustomUploadGrid();
                $("#SuccessLabel").text("Records deleted successfully.");
                $("#successDiv").show()
                setTimeout(function () {
                    $("#successDiv").hide()
                }, 5000);
                $("#MainContent_InfoPanel").hide()
                $("#errorDiv").hide()

            } else {
                setTimeout(function () {
                    $("#errorDiv").show()
                }, 5000);
                $("#MainContent_FailPanel").hide()
                $("#successDiv").hide()
            }
            $("#divLoading").hide();
        },
        error: function () {
            $("#successDiv").hide()
            $("#errorDiv").hide()
            $(".alert").hide();
            setTimeout(function () {
                $(".alert-danger").show();
            }, 5000);

            $("#divLoading").hide();
        }
    });
}


function ValidateFileUpload(Source, args) {

    var fuData = document.getElementById("MainContent_ExcelFileUpload");
    var FileUploadPath = fuData.value;

    if (FileUploadPath == '') {
        args.IsValid = false;
    }
    else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        if (Extension == "xls" || Extension == "xlsx") {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
}
