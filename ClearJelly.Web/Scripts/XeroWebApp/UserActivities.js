﻿
$(document).ready(function () {
    $(".left-side-box ul li").removeClass("active");
    $("#useractivities").addClass("active");
    $('#IsOnlyActiveSubscription').val(false);
    bindUser(0);
   
});

$("#MainContent_FilterDropDown").change(function () {
    var filterId = $(this).val();
    bindUser(filterId);
});


$("#MainContent_CategoryDropDown").change(function () {
    var categoryId = $(this).val();
    bindUser(filterId);
});

function bindUser(filterId) {
    $.ajax({
        url: 'UserActivities.aspx/GetUserList?filterId=' + filterId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateUsersGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}


function bindUserSubscription(userId) {
    $('#subscriptionPopup').modal('show');
    $("#userSubscriptiondv").html("");
    $.ajax({
        url: 'UserActivities.aspx/GetUserSubscriptionList?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateSubscriptionGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}




function CreateUsersGrid(data) {
    
    var strHtml = '';
    strHtml += '<table id=userTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Name</th>';
    strHtml += '<th>Company</th>';
    strHtml += '<th>Email</th>';
    strHtml += '<th>Registration Date</th>';
    strHtml += '<th>Admin</th>';
    strHtml += '<th>Active</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {

        strHtml += '<tr>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.CompanyName + '</td>';
        strHtml += '<td>' + value.Email + '</td>';
        strHtml += '<td>' + value.RegistrationDate + '</td>';
        if (value.IsAdmin) {
            strHtml += '<td>Yes</td>';
        }
        else {
            strHtml += '<td>No</td>';
        }

        if (value.IsActive) {
            strHtml += '<td>Yes</td>';
        }
        else {
            strHtml += '<td>No</td>';
        }
        strHtml += '<td class="NolinkBorder">';
        //suspended user
        if (!value.IsActive && !value.IsSuspended) {
            strHtml += '<a title="Activate User" id="view_' + value.UserId + '"  data-email=' + value.Email + ' class="handPointer activate"><img src="/images/user_active.png" title="Activate user"></a>';
        }
        //unblock user
        if (value.IsBlocked) {
            strHtml += '<a title="Unblock User" id="view_' + value.UserId + '"  data-email=' + value.Email + ' class="handPointer blockedUser"><img src="/images/user_unblock.png" title="Unblock user"></a>';
        }

        if (value.IsAdmin) {
            //suspend & Reactive
            if (value.IsSuspended) {
                strHtml += '<a title="ReActivate" id="view_' + value.UserId + '"  data-email=' + value.Email + ' class="handPointer reactivate"><img src="/images/user_reactive.png" title="Re-Activate user"></a>';
                strHtml += '<a title="Collect Outstanding Balance" id="view_' + value.UserId + '"  data-email=' + value.Email + ' class="handPointer collectoutstanding"><img src="/images/user_reactive.png" title="Collect Outstanding Balance"></a>';
            }
            else { strHtml += '<a title="Suspend" id="view_' + value.UserId + '" class="suspendUser handPointer"><img src="/images/user_suspend.png" title="Suspend user"></a>'; }
            //Delete
            { strHtml += '<a title="Delete" id="view_' + value.UserId + '"  data-email=' + value.Email + ' class="DeleteUser handPointer"><img title="Delete user" src="/images/user_delete.png"></a>'; }
            //view all subscription
            strHtml += '<a title="View All Subscription" id="view_' + value.UserId + '" class="viewUser handPointer"><img src="/images/user_viewall.png" title="View All Subscription of user"></a>';
            if (value.IsStartedAsTrail) {
                strHtml += '<a title="Extend Subscription" id="view_' + value.UserId + '"  data-email=' + value.Email + '  class="extendSubscription handPointer"><img src="/images/ExtendTrialPeriod.png" title="Extend Subscription"></a>';
            }
            if (value.PaidVersion) {
                strHtml += '<a title="Paid Subscription" id="view_' + value.UserId + '"  data-email=' + value.Email + '  class="handPointer"><img src="/images/paid_user.png" title="Paid Version"></a>';
            }
            //strHtml += '<a title="Impersonate User" id="view_' + value.UserId + '"  data-email=' + value.Email + '  class="handPointer impersonateUser"><img src="/images/user_reactive.png" title="Impersonate User"></a>';
        }

        strHtml += '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#userdv").html(strHtml);
    strHtml = '';
    var $tableSel = $('#userTbl');
    $tableSel.dataTable({
        "iDisplayLength": 50,
        "aoColumns": [
        { "width": "20%" },
        null,
        { "width": "30%" },
        {
            "width": "10%","sType" : "date-uk", "render": function (data) {
                
                if (data == null) {
                    return "No Report";
                }
                return moment(data).format(mommentDateFormat);
            }
        },
        { "width": "10%" },
        { "width": "10%" },
        { "width": "15%", "bSortable": false, "bSearchable": false }
        ],
        "order": [[1, 'asc'], [3, 'desc']],
    });

}




function ActivateUser(userId) {
    $("#divLoading").show();
    $.ajax({
        url: 'UserActivities.aspx/ActivateUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $('#successMsg').html("User Activated successfully.");
                $(".alert-success").show();
                rebindUserGrid();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
            $("#divLoading").hide();
        },
        error: function () {
            $("#divLoading").hide();
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

function UnblockUser(userId) {
    $.ajax({
        url: 'UserActivities.aspx/UnblockUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $('#successMsg').html("User Unblocked successfully.");
                $(".alert-success").show();
                rebindUserGrid();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

$(document).on("click", ".impersonateUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();

    $.ajax({
        url: 'UserActivities.aspx/ImpersonateUser?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

          
        },
        error: function () {

        }
    });

})


$(document).on("click", ".extendSubscription", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    $.ajax({
        url: 'UserActivities.aspx/GetUserSubscriptionList?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            $('#extendSubscriptionPopup').modal('show');
            $("#MainContent_numberOfDays").val("");
            $("#ExtendDaysValidation").hide();
            $("#StatusValidation").hide();
            $("#extendUpdateDiv").show();
            $("#saveDiv").show();
            CreateSubscriptionGrid(data);
            $("#MainContent_numberOfDays").val(30);

            if ($("#ActiveStatus").text() == "false") {
                $("#StatusValidation").show();
                $("#extendUpdateDiv").hide();
                $("#saveDiv").hide();
            } else {
                $("#StatusValidation").hide();
                $("#extendUpdateDiv").show();
                $("#saveDiv").show();
            }
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });


    $(document).off("click", "#btnExtendSave");
    $(document).on("click", "#btnExtendSave", function () {
        if ($("#MainContent_numberOfDays").val() == "") {
            $("#ExtendDaysValidation").show();
            return false;
        }
        var daystoExtend = $("#MainContent_numberOfDays").val();
        ExtendSubscription(userId, daystoExtend);
    });
});


function CreateSubscriptionGrid(data) {

    var strHtml = '';
    strHtml += '<table id=subscriptionTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Subscription</th>';
    strHtml += '<th>Additional Users</th>';
    strHtml += '<th>No.Of Packs</th>';
    strHtml += '<th>Started as Trial</th>';
    strHtml += '<th>Yearly</th>';
    strHtml += '<th>Start Date</th>';
    strHtml += '<th>End Date</th>';
    strHtml += '<th>Active</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {

        strHtml += '<tr>';

        strHtml += '<td>' + value.SubscriptionType + '</td>';
        strHtml += '<td>' + value.NoOfUsers + '</td>';
        strHtml += '<td>' + value.NoOfPacks + '</td>';
        if (value.StartedAsTrial) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }
        if (value.IsYearly) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }
        strHtml += '<td>' + value.StartDate + '</td>';
        strHtml += '<td>' + value.EndDate + '</td>';
        if (value.IsActive) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }

        strHtml += '</tr>';
        //user can have only one trial subscription
        if (value.StartedAsTrial) {
            if (value.StartedAsTrial && value.IsActive) {
                $("#ActiveStatus").text("true");
            } else {
                $("#ActiveStatus").text("false");
            }
        }
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#userSubscriptiondv").html(strHtml);
    $("#userSubscriptiondvs").html(strHtml);

    strHtml = '';
    $("#subscriptionTbl").dataTable({
        "aoColumns": [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
        ],
        "order": [[5, 'desc']],
    }
    );
}

$(document).on("click", ".viewUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    bindUserSubscription(userId);
});

$(document).on("click", ".suspendUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    bindSuspendUser(userId);
});

$(document).on("click", ".blockedUser", function () {
    var elementId = this.id;
    var email = $(this).data('email');
    bootbox.confirm("Are you sure you want to unblock the user " + email + " ?", function (result) {
        if (result) {
            var userId = elementId.split("_").pop();
            UnblockUser(userId);
        }
    });

});

function ExtendSubscription(userId, daystoExtend) {

    $.ajax({
        url: 'UserActivities.aspx/ExtendSubscriptionUser',
        type: "Post",
        data: JSON.stringify({ userId: userId, daystoExtend: daystoExtend }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            if (data.d) {
                $('#extendSubscriptionPopup').modal('hide');
                $('#successMsg').html("Trial subscription extended successfully.");
                $(".alert-success").show();
            }
            else {
                $('#extendSubscriptionPopup').modal('hide');
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $('#extendSubscriptionPopup').modal('hide');
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}



function ReActivateUser(userId) {

    $.ajax({
        url: 'UserActivities.aspx/ReActivateUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $('#successMsg').html("User Activated successfully.");
                $(".alert-success").show();
                rebindUserGrid();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

$(document).on("click", ".reactivate", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    var email = $(this).data('email');
    bootbox.confirm("Are you sure you want to Re-Activate " + email + "? ", function (result) {
        if (result) {
            ReActivateUser(userId);
        }
    });
});



$(document).on("click", ".collectoutstanding", function () {

    var elementId = this.id;
    var userId = elementId.split("_").pop();
    var email = $(this).data('email');

    $.ajax({
        url: 'UserActivities.aspx/GetOutstandingDetails',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateSuspendedUserSubscriptionGrid(data.d, userId);
        },
        error: function () {

        }
    });
});


function CreateSuspendedUserSubscriptionGrid(data, userId) {

    var strHtml = '';
    strHtml += '<table id=suspendedsubscriptionTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Subscription</th>';
    strHtml += '<th>RecurringProfileId</th>';
    strHtml += '<th>OutStanding Balance</th>';
    strHtml += '<th>Collect</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.lstUserSubscriptionDetailsViewModel, function (index, value) {

        strHtml += '<tr>';
        strHtml += '<td>' + value.SubscriptionName + '</td>';
        strHtml += '<td>' + value.RecurringProfileId + '</td>';
        strHtml += '<td>' + value.OutStandingBalance.OutStandingBalance + " " + value.OutStandingBalance.CurrencyCode + '</td>';
        strHtml += '<td class="NolinkBorder">';

        if (parseFloat(value.OutStandingBalance.OutStandingBalance) > 0) {
            strHtml += '<a title="Collect Outstandanding Balance" id="view_' + value.RecurringProfileId + "_" + userId + '"  data-email=' + value.Email + ' class="handPointer outstandingBalance"><img src="/images/user_unblock.png" title="Collect Outstandanding Balance"></a>';
        }
        strHtml += '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $(".alert").hide();
    $('#SuspendedsubscriptionPopup').modal('show');
    $("#suspendeduserSubscriptiondv").html("");
    $("#suspendeduserSubscriptiondv").html(strHtml);

    strHtml = '';
    $("#suspendedsubscriptionTbl").dataTable({
        "aoColumns": [
        null,
        null,
        null,
        null
        ],
        "order": [[2, 'desc']],
    }
    );
}


$(document).on("click", ".outstandingBalance", function () {

    var elementId = this.id;
    var recurringProfileId = elementId.split("_")[1];
    var userId = elementId.split("_")[2];
    var email = $(this).data('email');

    $.ajax({
        url: 'UserActivities.aspx/CollectOutstandingBalance',
        type: "Post",
        data: JSON.stringify({ recurringProfileId: recurringProfileId }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            if (data.d.ACK == "Failure") {
                $(".alert").hide();
                $('#suspendFailureMsg').html(data.d.LongMessage);
                $("#SuspendedsubscriptionPopup .alert-danger").show()
                setTimeout(function () {
                    $("#SuspendedsubscriptionPopup .alert-danger").hide()
                }, 5000);
                //window.setTimeout(ShowAlertInPopup(), 2000);
            } else {

                ReActivateUser(userId)
                $(".alert").hide();
                $('#suspendsuccessMsg').html("Balance has been collected successfully and user is also reactivated.");
                $("#SuspendedsubscriptionPopup .alert-success").show()
                setTimeout(function () {
                    $("#SuspendedsubscriptionPopup .alert-success").hide()
                }, 5000);
            }

        },
        error: function (e) {

            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
});


$(document).on("click", ".DeleteUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    var email = $(this).data('email');
    bootbox.confirm("Are you sure you want to delete " + email + "? ", function (result) {
        if (result) {
            DeleteUser(userId);
        }
    });
});
$(document).on("click", ".activate", function () {
    var elementId = this.id;
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    var email = $(this).data('email');
    bootbox.confirm("Are you sure you want to Activate " + email + "? ", function (result) {
        if (result) {
            ActivateUser(userId);
        }
    });
});

function DeleteUser(userId) {
    $("#divLoading").show();
    $.ajax({
        url: 'UserActivities.aspx/DeleteUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#divLoading").hide();
            if (data.d) {
                $(".alert").hide();
                $('#successMsg').html("User Deleted successfully.");
                $(".alert-success").show();
                rebindUserGrid();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $("#divLoading").hide();
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}


function bindSuspendUser(userId) {
    $('#suspendUserPopup').modal('show');
    hideValidationMsgs();
    $.ajax({
        url: 'UserActivities.aspx/GetUserSuspendDetails?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            bindSuspendedData(data);
            validation();
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function hideValidationMsgs() {
    $(".validationMsgs").each(function () {
        if (!$(this).hasClass("display-none")) {
            $(this).addClass("display-none");
        }
    });
}

function bindSuspendedData(data) {
    var suspendData = data.d;
    $("#MainContent_suspendedDate").val(suspendData.SuspendDate);
    $("#MainContent_suspendReason").val("");
    $("#haveAnyAdditionaluser").val(suspendData.AnyAdditionalUser);
    if (suspendData.AnyAdditionalUser) {
        $("#associateUserNote").show();
    }
    else { $("#associateUserNote").hide(); }

    $("#suspendUserId").val(suspendData.UserId);
}



function validation() {
    $('#btnSave').unbind('click');
    $('#btnSave').click(function (e) {
        var isError = false;
        var reasonVal = $('#MainContent_suspendReason').val();
        var suspendUserId = $("#suspendUserId").val();
        if (reasonVal == null || reasonVal == "") {
            $("#suspendReasonMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }

        if (!isError) {
            var model = {
                'UserId': suspendUserId,
                'SuspendReason': reasonVal,
            };
            suspendUser(model);
        }
    });

    $('#MainContent_suspendReason').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#suspendReasonMsg").removeClass("display-none");
        }
        else { $("#suspendReasonMsg").addClass("display-none"); }
    });
}
function rebindUserGrid() {
    $("#MainContent_FilterDropDown").change();
}


function suspendUser(model) {
    $.ajax({
        url: 'UserActivities.aspx/SuspendUser',
        type: "Post",
        data: JSON.stringify({ model: model }),
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#suspendUserPopup').modal('hide');
            if (data.d) {
                $(".alert").hide();
                $('#successMsg').html("User suspended successfully.");
                $(".alert-success").show();
                rebindUserGrid();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
            $('#suspendUserPopup').modal('hide');
        }
    });
}