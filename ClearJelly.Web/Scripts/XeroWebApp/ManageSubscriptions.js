﻿
$(document).ready(function () {
    $(".left-side-box ul li").removeClass("active");
    $("#manageSubscriptions").addClass("active");
    bindSubscriptions();
    AddSubscriptionType();
    allowNumeric();
    allowTwoDecimal();
});

function allowTwoDecimal() {
    $(".twoDecimal").change(function () {
        if ($(this).val() != null && $(this).val() != "") {
            var val = parseFloat($(this).val()).toFixed(2);
            $(this).val(val);
        }
    });
}



function bindSubscriptions() {
    $.ajax({
        url: 'ManageSubscriptions.aspx/GetSubscriptionList',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateSubscriptionTypesGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function CreateSubscriptionTypesGrid(data) {
    var strHtml = '';
    strHtml += '<table id=subscriptionTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Name</th>';
    strHtml += '<th>Description</th>';
    strHtml += '<th>Monthly Fee</th>';
    strHtml += '<th>Yearly Fee</th>';
    strHtml += '<th>Additional User Fee</th>';
    strHtml += '<th>Entities</th>';
    strHtml += '<th>Subscribed</th>';
    strHtml += '<th>Active</th>';

    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.TypeName + '</td>';
        strHtml += '<td>' + value.Description + '</td>';
        strHtml += '<td>' + value.MonthlyFee.toFixed(2) + '</td>';
        strHtml += '<td>' + value.YearlyContractFee.toFixed(2) + '</td>';
        strHtml += '<td>' + value.AdditionalUserFee.toFixed(2) + '</td>';
        strHtml += '<td>' + value.Entities + '</td>';
        strHtml += '<td>' + value.SubscribedCompanies + '</td>';
        if (value.IsActive) {
            strHtml += '<td>Yes</td>';
        }
        else {
            strHtml += '<td>No</td>';
        }

        strHtml += '<td> ' +
           '<a id="edit_' + value.SubscriptionTypeId + '" class="editSubscription handPointer">Edit</a>' + ' | ' +
           '<a id="delete_' + value.SubscriptionTypeId + '" data-subscribed =' + value.SubscribedCompanies + ' class="deleteSubscription handPointer">Delete</a>' +
           '</td>';

        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#manageSubscriptiondv").html(strHtml);
    strHtml = '';
    var $tableSel = $('#subscriptionTbl');
    $tableSel.dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
             { "bSortable": false, "bSearchable": false }
        ],
        //"order": [[1, 'asc'], [3, 'desc']],
    });
}

function AddSubscriptionType() {
    $('#btnAddNew').click(function () {
        $('#Popup').modal('show');
        $('#poupheading').html("Add Subscription");
        $("#successMsg").html("Subscription added successfully.");
        hideValidationMsgs();
        ClearData();
        validation();
    });
}

function ClearData() {
    $('#MainContent_Name').val("");
    $("#MainContent_Description").val("");
    $('#MainContent_MonthlyFee').val("");
    $('#MainContent_YearlyFee').val("");
    $('#MainContent_AdditionalUserFee').val("");
    $('#MainContent_Entities').val("");
    $("#subscriptionTypeId").val("");
}

function hideValidationMsgs() {
    $(".validationMsgs").each(function () {
        if (!$(this).hasClass("display-none")) {
            $(this).addClass("display-none");
        }
    });
}

function bindUserSubscription(userId) {
    $('#subscriptionPopup').modal('show');
    $("#userSubscriptiondv").html("");
    $.ajax({
        url: 'UserSubscriptions.aspx/GetUserSubscriptionList?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateSubscriptionGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function updateSampleLink(model) {
    $.ajax({
        url: 'ManageSubscriptions.aspx/SaveSubscription',
        type: "Post",
        data: JSON.stringify({ model: model }),
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#Popup').modal('hide');
            if (data.d) {
                $(".alert").hide();

                $(".alert-success").show();
                bindSubscriptions();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
            $('#Popup').modal('hide');
        }
    }
    );
}

function validation() {
    $('#btnUpdate').unbind('click');
    $('#btnUpdate').click(function (e) {
        var isError = false;
        var subscriptionTypeId = $('#subscriptionTypeId').val();
        var selectedNameVal = $('#MainContent_Name').val();
        if (selectedNameVal == null || selectedNameVal == "") {
            $("#nameMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }
        var selectedMonthlyVal = $('#MainContent_MonthlyFee').val();
        if (selectedMonthlyVal == null || selectedMonthlyVal == "") {
            $("#monthlyMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }
        var selectedYearlyVal = $('#MainContent_YearlyFee').val();
        if (selectedYearlyVal == null || selectedYearlyVal == "") {
            $("#yearlyMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }
        var selectedAdditionalUserVal = $('#MainContent_AdditionalUserFee').val();
        if (selectedAdditionalUserVal == null || selectedAdditionalUserVal == "") {
            $("#additionalMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }
        var selectedEntitiesVal = $('#MainContent_Entities').val();
        if (selectedEntitiesVal == null || selectedEntitiesVal == "") {
            $("#entitiesMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }

        var selectedDescVal = $('#MainContent_Description').val();

        if (!isError) {
            var model = {
                SubscriptionTypeId: subscriptionTypeId,
                TypeName: selectedNameVal,
                Description: selectedDescVal,
                MonthlyFee: selectedMonthlyVal,
                AdditionalUserFee: selectedAdditionalUserVal,
                YearlyContractFee: selectedYearlyVal,
                Entities: selectedEntitiesVal,
                IsActive: $("#IsActive:checked").length == 1 ? true : false
            };
            updateSampleLink(model);
        }
    });

    $('#MainContent_Name').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#nameMsg").removeClass("display-none");
        }
        else { $("#nameMsg").addClass("display-none"); }
    });

    $('#MainContent_MonthlyFee').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#monthlyMsg").removeClass("display-none");
        }
        else { $("#monthlyMsg").addClass("display-none"); }
    });

    $('#MainContent_YearlyFee').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#yearlyMsg").removeClass("display-none");
        }
        else { $("#yearlyMsg").addClass("display-none"); }
    });

    $('#MainContent_AdditionalUserFee').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#additionalMsg").removeClass("display-none");
        }
        else { $("#additionalMsg").addClass("display-none"); }
    });

    $('#MainContent_Entities').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#entitiesMsg").removeClass("display-none");
        }
        else { $("#entitiesMsg").addClass("display-none"); }
    });


}

function EditSubscription(subscriptionTypeId) {
    $('#Popup').modal('show');
    $("#successMsg").html("Subscription updated successfully.");
    $('#poupheading').html("Update Subscription");
    hideValidationMsgs();
    $.ajax({
        url: 'ManageSubscriptions.aspx/GetEditSubscriptionData?subscriptionTypeId=' + subscriptionTypeId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            bindEditData(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

$(document).on("click", ".editSubscription", function () {
    var elementId = this.id;
    var subscriptionId = elementId.split("_").pop();
    EditSubscription(subscriptionId);
});

$(document).on("click", ".deleteSubscription", function () {
    var elementId = this.id;
    var sampleLinkId = elementId.split("_").pop();
    var subscribed = $(this).data("subscribed");
    if (subscribed == 0) {
        bootbox.confirm("Are you sure you want to delete? ", function (result) {
            if (result) {
                deleteSampleLink(sampleLinkId);
            }
        });
    }
    else {
        bootbox.alert("The Subscription has other associated company(s).");
    }
});

function bindEditData(data) {
    $("#subscriptionTypeId").val(data.d.SubscriptionTypeId);
    $('#MainContent_Name').val(data.d.TypeName);
    $('#MainContent_Description').val(data.d.Description);
    $('#MainContent_MonthlyFee').val(data.d.MonthlyFee.toFixed(2));
    $('#MainContent_YearlyFee').val(data.d.YearlyContractFee.toFixed(2));
    $('#MainContent_AdditionalUserFee').val(data.d.AdditionalUserFee.toFixed(2));
    $('#MainContent_Entities').val(data.d.Entities);
    if (data.d.IsActive) {
        $("#IsActive").prop('checked', true);
    }
    else {
        $("#IsActive").prop('checked', false);
    }
    validation();
}


function deleteSampleLink(subscriptionTypeId) {
    $.ajax({
        url: 'ManageSubscriptions.aspx/Delete',
        type: "Post",
        data: "{ subscriptionTypeId:" + subscriptionTypeId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#Popup').modal('hide');
            if (data.d) {
                $(".alert").hide();
                $("#successMsg").html("Subscription Type deleted successfully.");
                $(".alert-success").show();
                bindSubscriptions();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
            $('#Popup').modal('hide');
        }
    });
}


