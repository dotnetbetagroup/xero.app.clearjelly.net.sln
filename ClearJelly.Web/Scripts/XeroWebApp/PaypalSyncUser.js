﻿
$(document).ready(function () {
    
    $(".left-side-box ul li").removeClass("active");
    $("#Paypal").addClass("active");
   
    bindPaidTrialGrid();
});


function bindPaidTrialGrid() {
    $('#MainContent_ActiveFilter').val("0")
    $('#MainContent_PaidTrialFilter').val("0")
    $.ajax({
        url: 'PaypalSyncData.aspx/GetPaidTrialList',
        type: "GET",
        contentType: "application/json; charset=utf-8", 
        success: function (data) {
            
           
            CreatePaidOrTrailUser(data);
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}



$(document).on("click", ".syncDataButton", function () {
    $("#divLoading").show();
    
    $.ajax({
        url: "PaypalSyncData.aspx/btnSyncPaypal_Click",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#divLoading").hide();
            bindPaidTrialGrid();
           
        },
        error: function () {
            $("#divLoading").hide();
        }
    });
});


function CreatePaidOrTrailUser(data) {
    
    var strHtml = '';
    strHtml += '<table id=PaodOrTrialTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Email </th>';
    strHtml += '<th> Is Paid</th>';
    strHtml += '<th> Subscription Name</th>';
    strHtml += '<th> Last Payment Date</th>';
    strHtml += '<th>No Of Cycles</th>';
    strHtml += '<th>Status</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {

        strHtml += '<tr>';
        strHtml += '<td>' + value.Email +'</td>';
        strHtml += '<td>' + value.IsPaid + '</td>';
        strHtml += '<td>' + value.SubscriptionName + '</td>';
        strHtml += '<td>' + value.LastPaymentDate + '</td>';
        strHtml += '<td>' + value.NoOfCycles + '</td>';
        strHtml += '<td>' + value.Status + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#PaidOrTrialDiv").html(strHtml);
    strHtml = '';
    $("#PaodOrTrialTbl").dataTable({
        "aoColumns": [
        null,
        null,
        null,
        null,
         null,
        { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}


$("#MainContent_ActiveFilter").change(function () {
    var filterId = $(this).val();
    var paidTrial = $("#MainContent_PaidTrialFilter").val();
    bindUser(filterId, paidTrial);
});

$("#MainContent_PaidTrialFilter").change(function () {
    var filterId = $("#MainContent_ActiveFilter").val()
    var paidTrial = $(this).val();
    bindUser(filterId ,paidTrial);
});


function bindUser(filterId, paidTrial) {
    $.ajax({
        url: 'PaypalSyncData.aspx/GetUserList?filterId=' + filterId + "&paidTrial=" + paidTrial,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            CreatePaidOrTrailUser(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}