﻿$(document).ready(function () {
    if (!isPostBack) {
        $("#MainContent_btnRegister").prop("disabled", true);
        $("#MainContent_btnRegisterClientSide").prop("disabled", true);
        $("#MainContent_btnBuy").prop("disabled", true);
        $('#MainContent_chkboxTerms').attr('checked', false);
        $("#MainContent_HdnCountryId").val($('#MainContent_CountryDropDown :selected').val());
        $("#MainContent_HdnQuantity").val("1");
        $("#rdTrial").prop('checked', true);
    }
    $("#subscriptionPlanDv").hide();
    if (isPostBack) {
        UpdatePreviousSelection();
    }
    UpdatePaymentAmounts();
    //GetSubscriptionPlans();
});

$(document).on("change", "#MainContent_chkboxTerms", function () {
    if ($(this).is(":checked")) {
        $("#MainContent_btnRegister").prop("disabled", false);
        $("#MainContent_btnBuy").prop("disabled", false);
        $("#MainContent_btnBuy").prop("disabled", false);
        $("#MainContent_btnRegisterClientSide").prop("disabled", false);
        
    } else {
        $("#MainContent_btnRegister").prop("disabled", true);
        $("#MainContent_btnBuy").prop("disabled", true);
        $("#MainContent_btnRegisterClientSide").prop("disabled", true);
    }
});

$(document).on("change", "#MainContent_CountryDropDown", function () {
    if (!isNaN($('#MainContent_CountryDropDown :selected').val())) {
        $("#MainContent_HdnCountryId").val($("#MainContent_CountryDropDown").val());
        UpdatePaymentAmounts();
    } else {
        $("#MainContent_HdnCountryId").val("0");
        ResetAmounts();
    }
});



$("#MainContent_CompanyName").focusout(function () {

var companyName = $(this).val();
if (companyName != "") {
    var obj = { companyName: companyName }
    $.ajax({
        url: "Register.aspx/companyExists",
        type: "POST",
        data: JSON.stringify(obj),
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            if (data.d) {
                $("#MainContent_btnRegister").prop("disabled", true);
                $("#MainContent_chkboxTerms").prop("disabled", true)
                $("#CompanyExistValidation").removeClass("display-none");
            } else {
                $("#MainContent_btnRegister").prop("disabled", false);
                $("#MainContent_chkboxTerms").prop("disabled", false)
                $("#CompanyExistValidation").addClass("display-none");
            }
        }
    });
}
});




$(document).on("click", ".monthlyRd,.yearlyRd", function () {
    if (!isNaN($('#MainContent_CountryDropDown :selected').val())) {
        var subscriptionTypeId = $(this).attr("data-subscriptiontypeid");
        $("#MainContent_HdnSubscriptionTypeId").val(subscriptionTypeId);
        var quantity = $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val();
        $("#MainContent_HdnQuantity").val(quantity);
        var gstRate = $('#MainContent_CountryDropDown :selected').attr("data-gstrate");
        var basicAmount;
        if ($(this).attr("class").indexOf("monthlyRd") != -1) {
            $("#MainContent_HdnSubscriptionMonths").val("1");
            basicAmount = $('.monthlyFeeLbl[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
        } else {
            $("#MainContent_HdnSubscriptionMonths").val("12");
            basicAmount = $('.yearlyContractPrice[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
        }
        SetAmounts(basicAmount, quantity, gstRate);
    }
});



//$(document).on("click", ".monthlyRd", function () {
//    if (!isNaN($('#MainContent_CountryDropDown :selected').val())) {
//        $("#MainContent_HdnSubscriptionMonths").val("1");
//        var subscriptionTypeId = $(this).attr("data-subscriptiontypeid");
//        $("#MainContent_HdnSubscriptionTypeId").val(subscriptionTypeId);
//        var basicAmount = $('.monthlyFeeLbl[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
//        var quantity = $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val();
//        var gstRate = $('#MainContent_CountryDropDown :selected').attr("data-gstrate");
//        SetAmounts(basicAmount, quantity, gstRate);
//    }
//});

//$(document).on("click", ".yearlyRd", function () {
//    if (!isNaN($('#MainContent_CountryDropDown :selected').val())) {
//        $("#MainContent_HdnSubscriptionMonths").val("12");
//        var subscriptionTypeId = $(this).attr("data-subscriptiontypeid");
//        $("#MainContent_HdnSubscriptionTypeId").val(subscriptionTypeId);
//        var basicAmount = $('.yearlyContractPrice[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
//        var quantity = $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val();
//        var gstRate = $('#MainContent_CountryDropDown :selected').attr("data-gstrate");
//        SetAmounts(basicAmount, quantity, gstRate);
//    }
//});

$(document).on("blur", ".quantityTxt", function () {
    var thisVal = $(this).val();
    if (thisVal == "" || thisVal == null || thisVal == "0") {
        $(this).val(1);
    }
    if ($(this).data("subscriptiontypeid") == $("#MainContent_HdnSubscriptionTypeId").val()) {
        $("#MainContent_HdnQuantity").val($(this).val());
        UpdatePaymentAmounts();
    }
});

function SetAmounts(basicAmount, quantity, gstRate) {
    var grossAmount =  Math.round((basicAmount * quantity) *100)/100;
    var gstAmount = Math.round(grossAmount * gstRate) / 100;
    var netAmount = Math.round((grossAmount + gstAmount) * 100) / 100;
    $("#grossAmtLbl").text("$ " + grossAmount);
    $("#gstRateLbl").text(gstRate + "%");
    $("#gstAmtLbl").text("$ " + gstAmount);
    $("#netAmtLbl").text("$ " + netAmount);
    $("#MainContent_HdnNetAmount").val(netAmount);

    $("#MainContent_HdnTotal").val(grossAmount);
    $("#MainContent_HdnGST").val(gstAmount);
}

function UpdatePaymentAmounts() {
    var selectedPlan;
    selectedPlan = $(".monthlyRd:checked,.yearlyRd:checked");
    selectedPlan.click();
}

function UpdatePreviousSelection() {
    var subscriptionTypeId = $("#MainContent_HdnSubscriptionTypeId").val();
    $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val($("#MainContent_HdnQuantity").val());
    if ($("#MainContent_HdnSubscriptionMonths").val() == "12") {
        $('.yearlyRd[data-subscriptiontypeid="' + subscriptionTypeId + '"]').prop("checked", true);
    } else {
        $('.monthlyRd[data-subscriptiontypeid="' + subscriptionTypeId + '"]').prop("checked", true);
    }
}

function ResetAmounts() {
    $("#grossAmtLbl").text("");
    $("#gstRateLbl").text("");
    $("#gstAmtLbl").text("");
    $("#netAmtLbl").text("");
}


//api_key: 75vznq38rc4j2h linkedin for server
var linkedIn = {
    onLinkedInLoad: function () {
        // Setup an event listener to make an API call once auth is complete
        IN.Event.on(IN, "auth", linkedIn.getProfileData);
    },
    onSuccess: function (data) {
        var linkedinData = {
            "EmailAddress": data.emailAddress,
            "FirstName": data.firstName,
            "LastName": data.lastName,
            "PictureUrl": data.publicProfileUrl,
            "Location": data.location.name,
            "UserId": data.id
        };
        $.ajax({
            url: "Login.aspx/LinkedinLoginCallback",
            contentType: "application/json; charset=utf-8",
            method: "POST",
            data: JSON.stringify({ "linkedinData": linkedinData }),
            success: function (data) {
                var returnUrl = data.d;
                window.location.href = returnUrl;
            },
            error: function () {
                alert("Something went wrong, please try again");
                location.reload();
            }
        });
    },
    onError: function (error) {
        // Handle an error response from the API call
        console.log(error);
    },
    getProfileData: function () {
        // Use the API call wrapper to request the member's basic profile data
        //IN.API.Raw("/people/~:(id,first-name,last-name,headline,picture-url,email-address)").result(linkedIn.onSuccess).error(linkedIn.onError);
        IN.API.Raw("/people/~:(id,first-name,last-name,headline,picture-url,email-address,industry,location:(name),public-profile-url,date-of-birth)").result(linkedIn.onSuccess).error(linkedIn.onError);
        //IN.API.Raw("firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "email-address", "educations", "date-of-birth").result(linkedIn.onSuccess).error(linkedIn.onError);
    },
    init: function () {
    }
};

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();
    }
}

window.fbAsyncInit = function () {
    FB.init({
        appId: facebookAppId, //'1631833673698395', // local host
        //appId: '1456754504636948', // demo : http://demo.zealousys.com/civi_crm/
        cookie: true,  // enable cookies to allow the server to access
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.4' // use version 2.2
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.
};




// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    FB.api('/me', { fields: 'email,last_name,first_name,name' }, function (data) {
        console.log(data);
        var facebookData = {
            "Name": data.name,
            "Id": data.id,
            "Email": data.Email,
            "FirstName": data.first_name,
            "LastName": data.last_name,
        };
        $.ajax({
            url: "Login.aspx/FBLoginCallback",
            contentType: "application/json; charset=utf-8",
            method: "POST",
            data: JSON.stringify({ "facebookData": facebookData }),
            success: function (data) {
                var returnUrl = data.d;
                window.location.href = returnUrl;
            },
            error: function () {
                alert("Something went wrong, please try again");
                location.reload();
            }
        });
    });
}

$(document).on("change", "input[type='radio'][name='BuyOption']", function () {
    if (this.value == 'FreeTrial') {
        DisplayTrialHtml();
        ChangeCSSForTrial();
    } else {
        DisplayBuyHtml();
         ChangeCSSForBuy();
    }
});

function DisplayTrialHtml() {
    $("#subscriptionPlanDv").hide();
    $("#dvBtnRegister").show();
    $("#dvbtnBuy").hide();
}

function DisplayBuyHtml() {
    $("#subscriptionPlanDv").show();
    $("#dvBtnRegister").hide();
    $("#dvbtnBuy").show();
}

function ChangeCSSForTrial() {
    $("#dvTerms").removeClass("pull-right autowidth");
    $("#dvTerms").addClass("col-xs-12");
    $('[data-lblblank="ForSubscription"]').show();
    $("#dvBtnRow").removeClass("pull-right");
    $("#dvBtnRow").addClass("col-md-6 col-xs-12");
    $("#dvBtns").addClass("col-xs-12").addClass("col-xs-8");
    $("#dvBtnRegister").removeClass("col-md-6");
    $("#dvBtnRegister").addClass("col-md-6");
    $("#dvbtnBuy").removeClass("col-md-6");
    $("#dvbtnBuy").addClass("col-md-6");
    $("#dvbtnCancel").removeClass("col-md-6");
    $("#dvbtnCancel").addClass("col-md-6");
}

function ChangeCSSForBuy() {
    $("#dvTerms").addClass("pull-right autowidth");
    $("#dvTerms").removeClass("col-xs-12");
    $('[data-lblblank="ForSubscription"]').hide();
    $("#dvBtnRow").addClass(" pull-right");
    $("#dvBtnRow").removeClass("col-md-6 col-xs-12");
    //$("#dvBtns").removeClass("col-xs-12");
    $("#dvBtns").removeClass("col-xs-9").removeClass("col-xs-8").addClass("col-xs-12");

    $("#dvBtnRegister").addClass("col-md-6");
    $("#dvBtnRegister").removeClass("col-md-6");
    $("#dvbtnBuy").addClass("col-md-6");
    $("#dvbtnBuy").removeClass("col-md-6");
    $("#dvbtnCancel").addClass("col-md-6");
    $("#dvbtnCancel").removeClass("col-md-6");
}