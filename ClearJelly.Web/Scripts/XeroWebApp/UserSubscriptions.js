﻿
$(document).ready(function () {
    $(".left-side-box ul li").removeClass("active");
    $("#userSubscriptions").addClass("active");
    $('#IsOnlyActiveSubscription').val(false);
    bindUser();

    /* Custom filtering function which will search data in column four between two values */
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
           
            var isOnlyActiveSubscription = $('#IsOnlyActiveSubscription').val();
            var hasActiveSubscription = data[5]; // use data for Has Active Subscription

            if (isOnlyActiveSubscription == "false" || (isOnlyActiveSubscription && hasActiveSubscription == "Yes")) {
                return true;
            }
            return false;
        }
    );
});





function bindUser() {
    $.ajax({
        url: 'UserSubscriptions.aspx/GetUserList',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            CreateUsersGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}


function bindUserSubscription(userId) {
    $('#subscriptionPopup').modal('show');
    $("#userSubscriptiondv").html("");
    $.ajax({
        url: 'UserSubscriptions.aspx/GetUserSubscriptionList?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateSubscriptionGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function ActivateUser(userId) {
    $.ajax({
        url: 'ActivateUser.aspx/ActivateUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $(".alert-success").show();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

function CreateUsersGrid(data) {
    var strHtml = '';
    strHtml += '<table id=userTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Name</th>';
    strHtml += '<th>Company</th>';
    strHtml += '<th>Email</th>';
    strHtml += '<th>Admin</th>';
    strHtml += '<th>Active</th>';
    strHtml += '<th>Active Subscription</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.CompanyName + '</td>';
        strHtml += '<td>' + value.Email + '</td>';
        if (value.IsAdmin) {
            strHtml += '<td>Yes</td>';
        }
        else {
            strHtml += '<td>No</td>';
        }

        if (value.IsActive) {
            strHtml += '<td>Yes</td>';
        }
        else {
            strHtml += '<td>No</td>';
        }
        if (value.HasAnyActiveSubscription) {
            strHtml += '<td>Yes</td>';
        }
        else {
            strHtml += '<td>No</td>';
        }

        if (value.IsAdmin) {
            strHtml += '<td> <a id="view_' + value.UserId + '" class="viewUser handPointer">View Subscriptions</a></td>';
        }
        else {
            strHtml += '<td></td>';
        }
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#userdv").html(strHtml);
    strHtml = '';
    var $tableSel = $('#userTbl');
    $tableSel.dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
             { "bSortable": false, "bSearchable": false }
        ],
        "order": [[1, 'asc'], [3, 'desc']],
    });

    $("#chkOnlyActive").change(function () {
        if ($("#chkOnlyActive:checked").length > 0) {
            $("#IsOnlyActiveSubscription").val(true);
            $tableSel.dataTable().fnDraw(); // Manually
        }
        else {
            $("#IsOnlyActiveSubscription").val(false);
            $tableSel.dataTable().fnDraw(); // Manually
        }
    });


    $("#displayOnlyActive").click(function () {

    });
    $("#displayAll").click(function () {

    });

}



function CreateSubscriptionGrid(data) {
    var strHtml = '';
    strHtml += '<table id=subscriptionTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Subscription</th>';
    strHtml += '<th>Additional Users</th>';
    strHtml += '<th>No.Of Packs</th>';
    strHtml += '<th>Started as Trial</th>';
    strHtml += '<th>Yearly</th>';
    strHtml += '<th>Start Date</th>';
    strHtml += '<th>End Date</th>';
    strHtml += '<th>Active</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';

        strHtml += '<td>' + value.SubscriptionType + '</td>';
        strHtml += '<td>' + value.NoOfUsers + '</td>';
        strHtml += '<td>' + value.NoOfPacks + '</td>';
        if (value.StartedAsTrial) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }
        if (value.IsYearly) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }
        strHtml += '<td>' + value.StartDate + '</td>';
        strHtml += '<td>' + value.EndDate + '</td>';
        if (value.IsActive) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }

        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#userSubscriptiondv").html(strHtml);
    strHtml = '';
    $("#subscriptionTbl").dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        "order": [[5, 'desc']],
    }
    );
}

$(document).on("click", ".viewUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    bindUserSubscription(userId);
});

