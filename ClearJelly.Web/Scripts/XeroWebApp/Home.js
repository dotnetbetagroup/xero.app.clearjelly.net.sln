﻿var isflag = false;
var processCookie = false;
$(document).ready(function () {
    $("#validation").hide();
    $(".loaderSpan").hide();
    processCookie = Cookies.get("UpdateModelProcess");
    if (processCookie === "running") {

        SetQbProcess();
        Cookies.remove('UpdateModelProcess');
    }

    processCookie = Cookies.get("ABCRunning");
    if (processCookie === "running") {

        ABCStart();
        Cookies.remove('ABCRunning');
    }

    if (processCookie === "updating") {

        ABCUpdate();
        Cookies.remove('ABCRunning');
    }


    var company = Cookies.get("selectedCompany");
    function ABCStart() {
        debugger;
        var club = $('#MainContent_ABCClubsList option:selected').val()
        $("#MainContent_MessageLabel2").text("The model process is running.");
        $(".loaderSpan").show();
        $.ajax({
            url: "Home.aspx/InitABC?club=" + club,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (data) => {
                debugger;
                if (data.d) {
                    abcProcessEnded();
                }
                else {
                    $('#MainContent_MessageLabel2').text = 'Somethig went wrong while we were loading your data';
                    $('.loaderSpan').hide();
                }
            },
            error: () => {
                debugger;
            }
        });
        Cookies.remove('abcclub');
    }

    function ABCUpdate() {
        debugger;
        var club = $('#MainContent_ABCClubsList option:selected').val()
        $("#MainContent_MessageLabel2").text("Your model is updating, please wait.");
        $(".loaderSpan").show();
        $.ajax({
            url: "Home.aspx/UpdateABC?club=" + club,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (data) => {
                debugger;
                if (data.d) {
                    abcProcessEnded();
                }
                else {
                    $('#MainContent_MessageLabel2').text = 'Somethig went wrong while we were loading your data';
                    $('.loaderSpan').hide();
                }
            },
            error: () => {
                debugger;
            }
        });
        //Cookies.remove('abcclub');
    }

    function abcProcessEnded() {
        var spanEle = document.getElementById('MainContent_MessageLabel2');
        spanEle.innerText = "Process ended, an email will be send to you";
        $('.loaderSpan').hide();
    }

    /* global setting */
    var datepickersOpt = {
        dateFormat: 'dd/mm/yy',
        //minDate: 0
    }
    $("#MainContent_companyName").text($("#MainContent_CompanyQickBookListDropDown option:selected").text());
    Cookies.set('selectedCompany', $("#MainContent_CompanyQickBookListDropDown option:selected").text());
    $('#MainContent_CompanyQickBookListDropDown').on('change', function () {
        var company = $("#MainContent_CompanyQickBookListDropDown option:selected").text();
        Cookies.set('selectedCompany', company);
        $("#MainContent_companyName").text(company);
    })
    $('#MainContent_btnUpdate').on('click', function () {
        var company = $("#MainContent_CompanyQickBookListDropDown option:selected").text();
        Cookies.set('selectedCompany', company);
    })

    $("#MainContent_txtSD").datepicker($.extend({
        onSelect: function () {

            var minDate = $(this).datepicker('getDate');
            //var setMaxDate = minDate.getDate();
            //minDate.setDate(minDate.getDate() + 2); //add two days
            var EndDate = $(this).datepicker('getDate');
            EndDate.setMonth(EndDate.getMonth() + 6);
            if (minDate.getDate() == 31 && minDate.getMonth() == 11) {
                EndDate.setDate(EndDate.getDate() - 1)
            }

            var StartDate = $(this).datepicker('getDate');
            $("#MainContent_txtED").datepicker("option", "maxDate", EndDate);
            //$("#MainContent_txtED").datepicker("option", "minDate", StartDate);

        }
    }, datepickersOpt));

    $("#MainContent_txtED").datepicker($.extend({
        onSelect: function () {

            var maxDate = $(this).datepicker('getDate');
            //maxDate.setDate(maxDate.getDate() - 2);
            $("#MainContent_txtSD").datepicker("option", "maxDate", maxDate);
            var minDate = $(this).datepicker('getDate');
            minDate.setMonth(minDate.getMonth() - 6);
            $("#MainContent_txtSD").datepicker("option", "minDate", minDate);
        }
    }, datepickersOpt));


    $("#MainContent_TextBox1").datepicker($.extend({
        onSelect: function () {

            var minDate = $(this).datepicker('getDate');
            //var setMaxDate = minDate.getDate();
            //minDate.setDate(minDate.getDate() + 2); //add two days
            var EndDate = $(this).datepicker('getDate');
            EndDate.setMonth(EndDate.getMonth() + 1);
            if (minDate.getDate() == 31 && minDate.getMonth() == 11) {
                EndDate.setDate(EndDate.getDate() - 1)
            }

            var StartDate = $(this).datepicker('getDate');
            $("#MainContent_TextBox1").datepicker("option", "maxDate", EndDate);
            //$("#MainContent_txtED").datepicker("option", "minDate", StartDate);

        }
    }, datepickersOpt));

    $("#MainContent_TextBox2").datepicker($.extend({
        onSelect: function () {

            var maxDate = $(this).datepicker('getDate');
            //maxDate.setDate(maxDate.getDate() - 2);
            $("#MainContent_TextBox2").datepicker("option", "maxDate", maxDate);
            var minDate = $(this).datepicker('getDate');
            minDate.setMonth(minDate.getMonth() - 1);
            $("#MainContent_TextBox2").datepicker("option", "minDate", minDate);
        }
    }, datepickersOpt));

    $.ajax({
        "url": "Home.aspx/GetLinksDashboard",
        "type": "POST",
        "contentType": "application/json; charset=utf-8",

        "dataType": "json",
        "success": function (data) {
            isflag = true;
            var tableData = data.d;
            $.each(tableData, function (index, value) {
                if (value.Name == "Web Browser (Jedox Web)") {
                    $("#webbrowserlink").text(value.Subject);
                    $("#webbrowserlink").attr("href", value.Subject);
                }
                else if (value.Name == "Download Power Bi Client") {
                    $("#DownloadpowerBiclient").attr("href", value.Subject)
                }
                else {
                    $("#sampleDashboardBI").attr("href", value.Subject)
                }
            });
        }
    });



    $("#tblActionLink").dataTable({
        "responsive": true,
        "bFilter": false,
        "lengthChange": false,
        "processing": true,
        "paging": false,
        "bSort": false
        //"sDom": '<"top">rt<"bottom"flp><"clear">'
    });
    $.ajax({
        "url": "Home.aspx/GetSampleLinks",
        "type": "POST",
        "contentType": "application/json; charset=utf-8",
        "dataType": "json",
        "success": function (data) {
            var tableData = data.d;
            $.each(tableData, function (index, value) {
                value.DownloadLink = '<a class="glyphicon gray-icon glyphicon-download-alt" aria-hidden="true" target="_blank" href="' + value.DownloadLink + '"></a>';
            });
            $('#tblDownloadLink').dataTable({
                "aaData": tableData,
                "responsive": true,
                "bFilter": false,
                "lengthChange": false,
                "processing": true,
                "aoColumns": [
                    {
                        "mData": "SortOrder",
                        "visible": false,
                    },
                    {
                        "mData": "Title",
                        "width": "90%"
                    },
                    {
                        "mData": "DownloadLink",
                        "width": "10%",
                        "bSearchable": false,
                        "bSortable": false
                    }
                ],
                "order": [[0, 'asc']]
            });
        }
    });

    $('[class*=hideLoad]').hide()
});

$(document).on("click", "#quickGuide", function () {
    var pathname = window.location.host;
    var url = window.location.protocol + "//" + pathname + "/QuickGuide.aspx";
    window.open(url, "_self");
});


$(document).on("click", "#btnChange", function () {
    window.location.href = "../ManageSubscription.aspx";
});


$(document).on("click", "#addSafeIP", function () {
    window.location.href = "../ManageIPRequest";
});

function checkDate(sender, args) {

    if (sender._selectedDate < new Date()) {
        alert("You cannot select a day earlier than today!");
        sender._selectedDate = new Date();
        // set the date back to the current date
        sender._textbox.set_Value(sender._selectedDate.format(sender._format))
    }
}


$(document).on("click", "#MainContent_ImagetxtSD", function () {
    $("#MainContent_txtSD").datepicker("show");
});

$(document).on("click", "#MainContent_ImagetxtED", function () {

    $("#MainContent_txtED").datepicker("show");
});

$(document).on("click", "#MainContent_Image1", function () {

    $("#MainContent_TextBox1").datepicker("show");
});

$(document).on("click", "#MainContent_Image2", function () {

    $("#MainContent_TextBox2").datepicker("show");
});

function updateFlag() {
    $.ajax({
        "url": "Home.aspx/ChangeIsMessageShownFlag",
        "type": "POST",
        "contentType": "application/json; charset=utf-8",
        "dataType": "json",
        "success": function (data) {
        }
    });
}

function ProcessEnded(data) {
    var spanEle = document.getElementById('MainContent_MessageLabel2');
    spanEle.innerText = data;
    $('.loaderSpan').hide();
}

function SetQbProcess() {
    $("#MainContent_MessageLabel2").text("The model process is running.");
    $(".loaderSpan").show();
    $.ajax({
        "url": "Home.aspx/InitQB",
        "type": "GET",
        "contentType": "application/json; charset=utf-8",
        "dataType": "json",
        "async": "true",
        "success": function (data) {

            //if (data.d.Result) {
            ProcessEnded(data.d.Result);
            //}
            //else {
            //    $('#MainContent_MessageLabel2').text = 'Somethig went wrong while we were loading your data';
            //    $('.loaderSpan').hide();
            //}
        }
    });
}

// This function will be called from server side while updating model and creating model
function SetMessageLabel() {

    $.ajax({
        "url": "Home.aspx/IsPRocessRunning",
        "type": "GET",
        "contentType": "application/json; charset=utf-8",
        "dataType": "json",
        "success": function (data) {

            if (!data.d) {
                $.ajax({
                    "url": "Home.aspx/IsMessageNotShown",
                    "type": "GET",
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "success": function (data) {
                        if (data.d != "") {
                            updateFlag();
                            $(".loaderSpan").hide();
                            $("#MainContent_MessageLabel").text(data.d);
                            //update flag

                        }
                    }
                });
                clearInterval(interval);
            }
            else {
                $("#MainContent_MessageLabel").text("The xero model process is running.");
                $(".loaderSpan").show();
            }
        }
    });
}