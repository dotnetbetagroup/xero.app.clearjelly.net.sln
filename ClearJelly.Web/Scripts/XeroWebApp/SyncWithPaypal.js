﻿$(document).ready(function () {

    $(".left-side-box ul li").removeClass("active");
    $("#Paypal").addClass("active");
    bindPaypalSubscriptionGrid();
});

function bindPaypalSubscriptionGrid() {
  
    $.ajax({
        url: 'SyncWithPaypal.aspx/GetPaypalSubscriptionList',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            if (data.d.length > 0) {
                $("#lastUpdatedDate").text(data.d[0].LastUpdatedDate);
            }
            CreateSubscriptionHistoryGrid(data);
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}


function CreateSubscriptionHistoryGrid(data) {
    var strHtml = '';
    strHtml += '<table id=SUbscriptionHistoryTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Recurring Id </th>';
    strHtml += '<th>Company Name </th>';
    strHtml += '<th>Email </th>';
    strHtml += '<th>No of cycles</th>';
    strHtml += '<th>Amount</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
      
        strHtml += '<tr>';
        strHtml += '<td>' + '<a href="#" class="ShowReccuringProlile" id="Reccuring_' + value.RecurringProfileId + '">' + value.RecurringProfileId + ' </a> </td>';
        strHtml += '<td>' + value.CompanyName + '</td>';
        strHtml += '<td>' + value.UserName + '</td>';
        strHtml += '<td>' + value.NoOfCycles + '</td>';
        strHtml += '<td>' + value.Amount + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#paypalSubscription").html(strHtml);
    strHtml = '';
    $("#SUbscriptionHistoryTbl").dataTable({
        "aoColumns": [
        null,
        null,
        null,
        null,
     
        { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}

$(document).on("click", ".syncDataButton", function () {
    $("#divLoading").show();
    
    $.ajax({
        url: "SyncWithPaypal.aspx/btnSyncPaypal_Click",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            $("#divLoading").hide();
            bindPaypalSubscriptionGrid();
            //if (data.d != null) {
            //    CreateRecuuringProfileGrid(data.d)
            //}
            //$('#ReccuringProfilePopup').modal('show');
        },
        error: function () {
        $("#divLoading").hide();
    }
    });
});



$(document).on("click", ".ShowReccuringProlile", function () {
    
    var elementId = this.id;
    var orgName = elementId.split("_")[1];
    var obj = { recurringProfileId: orgName }
    $.ajax({
        url: "SyncWithPaypal.aspx/ShowReccuringProlileList",
        type: "POST",
        data: JSON.stringify(obj),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateRecuuringProfileGrid(data.d)
            $('#ReccuringProfilePopup').modal('show');
        },
        error: function () {
            
    }
    });

});


function CreateRecuuringProfileGrid(data) {
    
    var strHtml = '';
    strHtml += '<table id=RecurringHistoryTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Recurring Id </th>';
    //strHtml += '<th>Last Payment on</th>';
    strHtml += '<th>Amount</th>';
    strHtml += '<th>Status</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data, function (index, value) {
      
        strHtml += '<tr>';
        strHtml += '<td>' + '<a href="#" class="ShowReccuringProlile" id="Reccuring_' + value.RecurringProfileId + '">' + value.RecurringProfileId + ' </a> </td>';
        //strHtml += '<td>' + value.LastPaymentDate + '</td>';
        strHtml += '<td>' + value.LastPaymentAmount + '</td>';
        strHtml += '<td>' + value.Status + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#HistoryRecurringProfile").html(strHtml);
    strHtml = '';
    $("#RecurringHistoryTbl").dataTable({
        "aoColumns": [
        null,
        //null,
        null,
       
        { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}
