﻿$(document).ready(function () {
    $('.monthlyRd:first').prop("checked", true);
    $(".quantityTxt").val('1')
    GetActiveSubscriptions();
    UpdatePaymentAmounts();
});

$(document).on("click", ".monthlyRd,.yearlyRd", function () {
    var subscriptionTypeId = $(this).attr("data-subscriptiontypeid");
    $("#MainContent_HdnSubscriptionTypeId").val(subscriptionTypeId);
    var quantity = $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val();
    $("#MainContent_HdnQuantity").val(quantity);
    var additionalUserPrice = $('.additionalUserPrice[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
    var gstRate = $('#MainContent_HdnGSTRate').val();
    var additionalUser = $("#MainContent_HdnAdditionalUser").val();
    var basicAmount;
    if ($(this).attr("class").indexOf("monthlyRd") != -1) {
        $("#MainContent_HdnSubscriptionMonths").val("1");
        basicAmount = $('.monthlyFeeLbl[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
    } else {
        $("#MainContent_HdnSubscriptionMonths").val("12");
        basicAmount = $('.yearlyContractPrice[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
    }
    SetAmounts(basicAmount, quantity, gstRate, additionalUser, additionalUserPrice);
});

//$(document).on("click", ".yearlyRd", function () {
//    $("#MainContent_HdnSubscriptionMonths").val("12");
//    var subscriptionTypeId = $(this).attr("data-subscriptiontypeid");
//    $("#MainContent_HdnSubscriptionTypeId").val(subscriptionTypeId);
//    var basicAmount = $('.yearlyContractPrice[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
//    var quantity = $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val();
//    var additionalUserAmount = $('.additionalUserPrice[data-subscriptiontypeid="' + subscriptionTypeId + '"]').text();
//    var gstRate = $('#MainContent_HdnGSTRate').val();
//    var additionalUser = $("#MainContent_HdnAdditionalUser").val();
//    SetAmounts(basicAmount, quantity, gstRate);
//});

$(document).on("blur", ".quantityTxt", function () {
    var thisVal = $(this).val();
    if (thisVal == "" || thisVal == null || thisVal == "0") {
        $(this).val(1);
    }
    if ($(this).data("subscriptiontypeid") == $("#MainContent_HdnSubscriptionTypeId").val()) {
        $("#MainContent_HdnQuantity").val($(this).val());
        UpdatePaymentAmounts();
    }
});

function SetAmounts(basicAmount, quantity, gstRate, additionalUser, additionalUserPrice) {
    var subscriptionAmount = Math.round((basicAmount * quantity) * 100) / 100;
    //var additionalUserAmount = additionalUser * additionalUserPrice;
    //var grossAmount = subscriptionAmount + additionalUserAmount;
    var grossAmount = subscriptionAmount;
    var gstAmount = Math.round(grossAmount * gstRate) / 100;
    var netAmount = Math.round((grossAmount + gstAmount) * 100) / 100;
    $("#subscriptionAmtLbl").text("$ " + subscriptionAmount);
    //$("#addtionalUserLbl").text(additionalUser);
    //$("#additionalUserAmtLbl").text("$ " + additionalUserAmount);
    $("#grossAmtLbl").text("$ " + grossAmount);
    $("#gstRateLbl").text(gstRate + "%");
    $("#gstAmtLbl").text("$ " + gstAmount);
    $("#netAmtLbl").text("$ " + netAmount);
    $("#MainContent_HdnNetAmount").val(netAmount);

    $("#MainContent_HdnTotal").val(grossAmount);
    $("#MainContent_HdnGST").val(gstAmount);
}

function UpdatePaymentAmounts() {
    var selectedPlan;
    selectedPlan = $(".monthlyRd:checked,.yearlyRd:checked");
    selectedPlan.click();
}

function UpdatePreviousSelection() {

    var subscriptionTypeId = $("#MainContent_HdnSubscriptionTypeId").val();
    $('.quantityTxt[data-subscriptiontypeid="' + subscriptionTypeId + '"]').val($("#MainContent_HdnQuantity").val());
    if ($("#MainContent_HdnSubscriptionMonths").val() == "12") {

        if ($('.yearlyRd[data-subscriptiontypeid="' + subscriptionTypeId + '"]').length == 0) {
            $('.yearlyRd:first').prop("checked", true);
        }
        $('.yearlyRd[data-subscriptiontypeid="' + subscriptionTypeId + '"]').prop("checked", true);
    } else {
        $('.monthlyRd[data-subscriptiontypeid="' + subscriptionTypeId + '"]').prop("checked", true);
        if ($('.monthlyRd[data-subscriptiontypeid="' + subscriptionTypeId + '"]').length == 0) {
            $('.monthlyRd:first').prop("checked", true);
        }
    }
}


function GetActiveSubscriptions() {
    $.ajax({
        url: 'ManageSubscription.aspx/GetActiveSubscriptions',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateActiveSubscrptionsGrid(data);
            //DeleteUser();
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function CreateActiveSubscrptionsGrid(data) {
    var strHtml = '';
    strHtml += '<table id=activeSubscrptionsTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Subscription</th>';
    strHtml += '<th>Start Date</th>';
    strHtml += '<th>No. of Pack</th>';
    strHtml += '<th>Additional Users</th>';
    strHtml += '<th>Yearly</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.TypeName + '</td>';
        strHtml += '<td>' + value.StartDate + '</td>';
        strHtml += '<td>' + value.Quantity + '</td>';
        strHtml += '<td>' + value.AdditionalUsers + '</td>';
        if (value.IsYearly)
        { strHtml += '<td>Yes</td>'; }
        else { strHtml += '<td>No</td>'; }

        strHtml += '<td> <a id="edit_' + value.CompanySubscriptionId + '" class="cancelSubscription pointerCursor">Cancel</a></td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#activeSubscriptiondv").html(strHtml);
    strHtml = '';

    $("#activeSubscrptionsTbl").dataTable({
        "aoColumns": [
           null,
           null,
           null,
           null,
           null,
           { "bSortable": false, "bSearchable": false }
        ],
        "order": [[1, 'desc']]
    });
}


$(document).on("click", ".cancelSubscription", function () {
    var elementId = this.id;
    bootbox.confirm("Are you sure you want to cancel this subscription ? ", function (result) {
        if (result) {
            var companySubscriptionId = elementId.split("_").pop();
            CancelSubscription(companySubscriptionId);
        }
    });

});


function CancelSubscription(companySubscriptionId) {
    $.ajax({
        url: 'ManageSubscription.aspx/CancelSubscription',
        type: "Post",
        data: "{ companySubscriptionId:" + companySubscriptionId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $(".alert-success").show();
                GetActiveSubscriptions();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}