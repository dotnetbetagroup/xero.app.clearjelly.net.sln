﻿$(document).ready(function () {
    
    $(".left-side-box ul li").removeClass("active");
    $("#ModelProcess").addClass("active");
    bindModelProcessGrid();
    //$("#searchModelProces").prop("disabled", true);
    var datepickersOpt = {
        dateFormat: 'dd/mm/yy',
        //minDate: 0
    }


    $("#MainContent_txtStartDate").datepicker($.extend({
        onSelect: function () {
            
            var minDate = $(this).datepicker('getDate');
            //var setMaxDate = minDate.getDate();
            //minDate.setDate(minDate.getDate() + 2); //add two days
            var EndDate = $(this).datepicker('getDate');
            EndDate.setMonth(EndDate.getMonth() + 6);
            if (minDate.getDate() == 31 && minDate.getMonth() == 11) {
                EndDate.setDate(EndDate.getDate() - 1)
            }

            var StartDate = $(this).datepicker('getDate');
            $("#MainContent_txtEndDate").datepicker("option", "maxDate", EndDate);
            //$("#MainContent_txtED").datepicker("option", "minDate", StartDate);


            //$("#searchModelProces").prop("disabled", false)


        }
    }, datepickersOpt)).keyup(function (e) {
        if ($(this).val() == "")
            
        $("#MainContent_txtStartDate").datepicker("option", "minDate", null);


    });

    $("#MainContent_txtEndDate").datepicker($.extend({
        onSelect: function () {
            
            var maxDate = $(this).datepicker('getDate');
            //maxDate.setDate(maxDate.getDate() - 2);
            $("#MainContent_txtStartDate").datepicker("option", "maxDate", maxDate);
            var minDate = $(this).datepicker('getDate');
            minDate.setMonth(minDate.getMonth() - 6);
            $("#MainContent_txtStartDate").datepicker("option", "minDate", minDate);

        }
    }, datepickersOpt)).keyup(function (e) {
        if ($(this).val() == "")
            
        $("#MainContent_txtEndDate").datepicker("option", "maxDate", null);


    });

});

function bindModelProcessGrid() {

    $.ajax({
        url: 'ModelProcessDetails.aspx/GetModelProcessList',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            $("#MainContent_OrgDropDown").empty();

            $("#MainContent_OrgDropDown").prepend("<option value='0' selected='selected'> Select All</option>");
            CreateModelProcessGrid(data.d.lstModelProcessVm, "modelProcessTable");
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

$(document).on("click", "#imgStartDate", function () {
    $("#MainContent_txtStartDate").datepicker("show");
});

$(document).on("click", "#imgEndDate", function () {
    $("#MainContent_txtEndDate").datepicker("show");
});

function CreateModelProcessGrid(data, tableName) {
    var strHtml = '';
    strHtml += '<table id="' + tableName + '" class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>User </th>';
    strHtml += '<th>Organisation Name </th>';
    strHtml += '<th>Process Type</th>';
    strHtml += '<th>Status </th>';
    strHtml += '<th>Created Date </th>';
    strHtml += '<th>From Date </th>';
    strHtml += '<th>To Date </th>';

    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data, function (index, value) {
        
        strHtml += '<tr>';
        strHtml += '<td>' + value.UserName + '</td>';
        strHtml += '<td>' + '<a href="#" class="ShowModelProcessDetails" id="modelProcess_' + value.ModelProcessId + '">' + value.OrganizationName + ' </a> </td>';
        strHtml += '<td>' + value.ProcessType + '</td>';
        strHtml += '<td>' + value.Status + '</td>';
        strHtml += '<td>' + value.CreatedDate + '</td>';
        strHtml += '<td>' + value.FromDate + '</td>';
        strHtml += '<td>' + value.ToDate + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    if (tableName == "modelProcessTable") {
        $("#ModelProcessDiv").html(strHtml);
    } else {
        $("#modelProcessInner").html(strHtml);
    }
    strHtml = '';
    $("#" + tableName).dataTable({
        "aoColumns": [
       { "width": "15%" },
         { "width": "20%" },
        { "width": "15%" },

      { "width": "10%" },
         { "width": "15%" },
         { "width": "15%" },
       { "width": "15%" },

        ]
    }
    );
}


$(document).on("click", ".ShowModelProcessDetails", function () {
    
    var elementId = this.id;
    var modelProcessId = elementId.split("_")[1];
    var obj = { modelProcessId: modelProcessId }
    $.ajax({
        url: "ModelProcessDetails.aspx/ShowModelProcessList",
        type: "POST",
        data: JSON.stringify(obj),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateModelProcessGrid(data.d, "modelProcessInnerTable")
            $('#modelProcessInnerTable').DataTable().column(0).visible(false)
            $('#ModelProcessPopup').modal('show');
        }
    });

});


$(document).on("change", "#MainContent_FilterDropDown", function () {
    
    var userId = $(this).val();
    var startDate = $("#MainContent_txtStartDate").val()
    var endDate = $("#MainContent_txtEndDate").val()
    if (startDate == "") {
        startDate = null;
    }
    if (endDate == "") {
        endDate = null;
    }

    //bindUser(userId);
    bindOrgDropDown(userId);
});

function bindOrgDropDown(userId) {
    
    $.ajax({
        url: 'ModelProcessDetails.aspx/BindOrganizationDropdown?userId=' + userId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            var Result = data.d.lst_Xero_Org;

            $("#MainContent_OrgDropDown").empty();

            $("#MainContent_OrgDropDown").prepend("<option value = '0' selected='selected'> Select All</option>");

            if (Result != null) {
                if (Result.length > 0) {

                    for (var i = 0; i < Result.length; i++) {
                        
                        $("#MainContent_OrgDropDown").append("<option value='" + Result[i].CompanyID + "_" + Result[i].OrgName + "'>" +
                        Result[i].OrgName + "</option>");
                    }

                }
            }
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}



$(document).on("change", "#MainContent_OrgDropDown", function () {
    
    var value = $(this).val();
    var organisationName = null;
    if (value != "") {
        organisationName = value.split("_")[1]
    }
    var userId = $("#MainContent_FilterDropDown").val();

    //bindUserGrid(userId, organisationName, null, null);
});



function bindUserGrid(userId, orgName, startDate, endDate) {
    

    $.ajax({
        url: 'ModelProcessDetails.aspx/GetModelProcessFilterListPost',
        type: "POST",
        data: JSON.stringify({ userId: userId, orgName: orgName, startDate: startDate, endDate: endDate }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            CreateModelProcessGrid(data.d.lstModelProcessVm, "modelProcessTable");
            if (startDate == null) {
                $("#MainContent_txtStartDate").val("");
                $("#MainContent_txtEndDate").val("");
            }
        },
        error: function () {

        }
    });
}


$(document).on("click", "#searchModelProces", function () {
    
    var userDDl = $("#MainContent_FilterDropDown").val();
    if (userDDl != 0) {
        var orgDetail;
        if ($("#MainContent_OrgDropDown").val() == 0 || $("#MainContent_OrgDropDown").val() == null) {
            orgDetail = null;
        } else {
            orgDetail = $("#MainContent_OrgDropDown").val().split("_")[1]
        }
        var startDate = $("#MainContent_txtStartDate").val()
        var endDate = $("#MainContent_txtEndDate").val()
        if (startDate == "") {
            startDate = null;

        }
        if (endDate == "") {
            endDate = null;
        }
        bindUserGrid(userDDl, orgDetail, startDate, endDate)

    } else {
        var startDate = $("#MainContent_txtStartDate").val()
        var endDate = $("#MainContent_txtEndDate").val()
        if (startDate == "") {
            startDate = null;

        }
        if (endDate == "") {
            endDate = null;
        }
        bindUserGrid(0, null, startDate, endDate)
    }
});


$("#MainContent_txtStartDate,#MainContent_txtEndDate ").focusout(function (e) {
    
    var startDate = $("#MainContent_txtStartDate").val();
    var endDate = $("#MainContent_txtEndDate").val();
    //if ($("#MainContent_FilterDropDown") != 0) {
    //    if (startDate == "") {
    //        $("#searchModelProces").prop("disabled", true)
    //    } else {
    //        $("#searchModelProces").prop("disabled", false)
    //    }
    //} else {
    //    $("#searchModelProces").prop("disabled", false)
    //}
});


$("#ClearButton").click(function () {
    $("select").val(0);
    $('#MainContent_OrgDropDown').find('option:not(:first)').remove();
    $("#MainContent_txtStartDate").val("")
    $("#MainContent_txtEndDate").val("")
    $("#searchModelProces").trigger("click");
})