﻿
$(document).ready(function () {
    bindUserIP();
});

function bindUserIP() {
    $.ajax({
        url: 'ManageIpRequest.aspx/GetUserIPList',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            CreateUsersGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function CreateUsersGrid(data) {
    var strHtml = '';
    strHtml += '<table id=useripTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>IP Address</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.IpAddress + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#userIpListdv").html(strHtml);
    strHtml = '';
    $("#useripTbl").dataTable({
        "aoColumns": [
            null,
        ]
    });
}


function SaveIp() {
    $("#divLoading").show();
    $.ajax({
        url: 'ManageIPRequest.aspx/SaveIPAddress',
        type: "Post",
        data: "{ }",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
          
            $("#divLoading").hide();
            if (data.d) {
                $("#SuccessMsg").html("IP address saved successfully.");
                $(".save").hide();
                $(".alert").hide();
                $(".alert-success").show();
                bindUserIP();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $("#divLoading").hide();
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}


$(document).on("click", ".save", function () {
    var elementId = this.id;
    bootbox.confirm("Are you sure you want to save this IP address?", function (result) {
        if (result) {
            SaveIp();
        }
    });
});