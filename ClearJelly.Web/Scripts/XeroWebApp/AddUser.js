﻿
$(document).ready(function () {
   
    GetActiveSubscriptions();
    $("#profile.collapseHeading").trigger('click');
});


function GetActiveSubscriptions() {
    $.ajax({
        url: '/ManageSubscription.aspx/GetActiveSubscriptions',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateActiveSubscrptionsGrid(data);
            //DeleteUser();
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function subscriptionChangeEvent() {
   
    //set first subscription default selected.
    $(".selectedSubscription:first").prop("checked", true);
    var subscriptionId = $(".selectedSubscription:first").attr("Id");
    $("#MainContent_HdnSubscription").val(subscriptionId);

    $(".selectedSubscription").change(function () {
       
        var subscriptionId = $(this).attr("Id");
        $("#MainContent_HdnSubscription").val(subscriptionId);
        getAdditionalUserFeeDetails(subscriptionId);
    });

    getAdditionalUserFeeDetails(subscriptionId);
}


function getAdditionalUserFeeDetails(subscriptionId) {
   
    $.ajax({
        url: 'AddUser.aspx/GetAdditionalUserFeeDetails?subscriptionId=' + subscriptionId,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           
            $("#MainContent_AddUserFee").val('$ ' + data.d.AdditionUserPrice.toFixed(2));
            $("#MainContent_AddGSTFee").val('$ ' + data.d.AdditionUserGst.toFixed(2));
            $("#MainContent_AddTotalFee").val('$ ' + data.d.AdditionalUserGrossAmount.toFixed(2));
             
            $("#MainContent_SubscriptionFee").val('$ ' + data.d.SubscriptionGrossAmount.toFixed(2));
            $("#MainContent_SubscriptionGSTFee").val('$ ' + data.d.SubsctiptionGstOrderTotal.toFixed(2));
            $("#MainContent_SubscriptionTotalFee").val('$ ' + data.d.SubsctiptionOrderTotal.toFixed(2));
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

function CreateActiveSubscrptionsGrid(data) {
    var strHtml = '';
    strHtml += '<table id=activeSubscrptionsTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Select</th>';
    strHtml += '<th>Subscription</th>';
    strHtml += '<th>Start Date</th>';
    strHtml += '<th>No. of Pack</th>';
    strHtml += '<th>Additional Users</th>';
    strHtml += '<th>Yearly</th>';

    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td><input type="radio" name="subscriptionSelection" id= ' + value.CompanySubscriptionId + ' class="selectedSubscription" /></td>';
        strHtml += '<td>' + value.TypeName + '</td>';
        strHtml += '<td>' + value.StartDate + '</td>';
        strHtml += '<td>' + value.Quantity + '</td>';
        strHtml += '<td>' + value.AdditionalUsers + '</td>';
        if (value.IsYearly)
        { strHtml += '<td>Yes</td>'; }
        else { strHtml += '<td>No</td>'; }
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#activeSubscriptiondv").html(strHtml);
    
    strHtml = '';

    $("#activeSubscrptionsTbl").dataTable({
        "aoColumns": [
            { "bSortable": false, "bSearchable": false },
           null,
           null,
           null,
           null,
           null
        ],
        "order": [[2, 'desc']]
    });
    subscriptionChangeEvent();
}


var selectIds = $('#profile,#profile2');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    })
});