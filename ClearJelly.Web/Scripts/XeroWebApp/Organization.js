﻿$(document).ready(function () {
    bindOrganizationGrid();
});

function bindOrganizationGrid() {

    $.ajax({
        url: 'Organization.aspx/GetUserOrganisations',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            CreateOrganizationGrid(data);
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

function CreateOrganizationGrid(data) {
    var strHtml = '';
    strHtml += '<table id=organizationTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>OrgName</th>';
    strHtml += '<th>OrgShortCode</th>';
    //strHtml += '<th>IsActive</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {

        strHtml += '<tr>';
        strHtml += '<td>' + value.OrgName + '</td>';
        strHtml += '<td>' + value.OrgShortCode + '</td>';
        //strHtml += '<td>' + value.isActive + '</td>';
        strHtml += '<td> <a href="#" class="DeleteOrganization" id="' + value.OrgShortCode + "_" + value.CompanyID + "_" + value.OrgName + '" >Delete</a></td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#manageOrganizationdv").html(strHtml);
    strHtml = '';
    $("#organizationTbl").dataTable({
        "aoColumns": [
           null,
            null,
             { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}


$(document).on("click", ".DeleteOrganization", function () {
    var elementId = this.id;
    var orgName = elementId.split("_")[2];
    var obj = { orgName: orgName }
    $.ajax({
        url: "Home.aspx/IsDeletedOrgProcessRunning",
        type: "POST",
        data: JSON.stringify(obj),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (!data.d) {

                var orgCode = elementId.split("_")[0];
                var companyId = elementId.split("_")[1];
                bootbox.confirm('Are you sure you want to delete this Organization ?', function (result) {
                    if (result) {
                        DeleteOrganization(orgCode, companyId);
                    }
                });
            }
            else {
                bootbox.alert("Delete is not allowed because your model process is running please try after completing the model process.");
            }
        }
    });

});


function DeleteOrganization(orgCode, companyId) {
    var obj = { orgCode: orgCode, companyId: companyId }
    $.ajax({
        url: 'Organization.aspx/DeleteOrganization',
        type: "Post",
        data: JSON.stringify(obj),
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            bindOrganizationGrid();
            $(".alert").hide();
            if (data.d) {
                $(".alert-success").show();
                $("#successMsg").text("Organization deleted successfully")
            } else {
                $(".alert").hide();
                $(".alert-danger").show();
            }

        },
        error: function (data) {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}