﻿$(document).ready(function () {
    
    $("#divLoading").show();
    bindCustomUploadGrid();
    $("#successDiv").hide()
    $("#errorDiv").hide()
  

});


function bindCustomUploadGrid() {
   
    $.ajax({
        url: 'UploadPayroll.aspx/GetCustomUploadList',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#divLoading").hide();
           
            CreateCustomUploadGrid(data);
            setTimeout(function () {
                $("#MainContent_InfoPanel").hide();
                $("#MainContent_FailPanel").hide();
            }, 5000);
            if ($('#CustomUploadGrid').DataTable().data().any()) {
                $("#ClearAllButton").show()
            } else {
                $("#ClearAllButton").hide()
            }
        },
        error: function () {
            $("#divLoading").hide();
            setTimeout(function () {
            $(".alert").hide();
            }, 5000);
                $(".alert-danger").show();
          
        }
    });
}


window.onload = function () {
    var inputElement = document.getElementById("MainContent_ExcelFileUpload");
    inputElement.onclick = function (event) {
        $("#MainContent_CustomValidator1").css("visibility", "hidden")
    }
}

function ValidateFileUpload(Source, args) {
    
    var fuData = document.getElementById("MainContent_ExcelFileUpload");
    var FileUploadPath = fuData.value;

    if (FileUploadPath == '') {
        args.IsValid = false;
    }
    else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        if (Extension == "xls" || Extension == "xlsx") {
            args.IsValid = true; 
        }
        else {
            args.IsValid = false;
        }
    }
}


$(document).on("click", "#ClearAllButton", function () {
  
    bootbox.confirm("Are you sure you want to delete all the records ?", function (result) {
        if (result) {
            DeleteAllRecords();
        }
    });
});




$("input[type=file]").change(function () {
    $("#MainContent_btnUpload").show();
});


function DeleteAllRecords() {
    $("#divLoading").show();
    $.ajax({
        url: 'UploadPayroll.aspx/ClearAllData',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            $(".alert").hide();
            $("#successDiv").hide()
            $("#errorDiv").hide()
            if (data.d) {
                bindCustomUploadGrid();
                $("#SuccessLabel").text("Records deleted successfully.");
                $("#successDiv").show()
                setTimeout(function () {
                    $("#successDiv").hide()
                }, 5000);
                $("#MainContent_InfoPanel").hide()
                $("#errorDiv").hide()

            } else {
                setTimeout(function () {
                    $("#errorDiv").show()
                }, 5000);
                $("#MainContent_FailPanel").hide()
                $("#successDiv").hide()
            }
            $("#divLoading").hide();
        },
        error: function () {
            $("#successDiv").hide()
            $("#errorDiv").hide()
            $(".alert").hide();
            setTimeout(function () {
                $(".alert-danger").show();
            }, 5000);

            $("#divLoading").hide();
        }
    });
}



function CreateCustomUploadGrid(data) {
    var strHtml = '';
    strHtml += '<table id=CustomUploadGrid class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Date</th>';
    strHtml += '<th>Pay Item Type </th>';
    strHtml += '<th>Employee </th>';
    strHtml += '<th>Pay Item</th>';
    strHtml += '<th>Employee Group</th>';
    strHtml += '<th>Amount</th>';
    strHtml += '<th>Org Name</th>';
  
    strHtml += '<th>Tracking Code 1</th>';
    strHtml += '<th>Tracking code 2</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        
        strHtml += '<tr>';
        strHtml += '<td>' + value.DisplayDate + '</td>';
        strHtml += '<td>' + value.PayItemType + '</td>';
        strHtml += '<td>' + value.Employee + '</td>';
        strHtml += '<td>' + value.PayItem + '</td>';
        strHtml += '<td>' + value.EmployeeGroup + '</td>';
        strHtml += '<td>' + '$ '+ value.Amount + '</td>';
        strHtml += '<td>' + value.OrgName + '</td>';
    
        strHtml += '<td>' + value.TrackingCode1 + '</td>';
        strHtml += '<td>' + value.TrackingCode2 + '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#UploadCustomData").html(strHtml);
    strHtml = '';
    $("#CustomUploadGrid").dataTable({
        "aoColumns": [
      { "width": "10%" },
        null,
        null,
        null,
        null,
        { "width": "10%" },
      
        null,
        null,
        { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}