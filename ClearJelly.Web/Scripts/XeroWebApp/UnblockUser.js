﻿
$(document).ready(function () {
    bindUser();
    $(".left-side-box ul li").removeClass("active");
    $("#unblockUser").addClass("active");
});

function bindUser() {
    $.ajax({
        url: 'UnblockUser.aspx/GetUserList',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            CreateUsersGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function UnblockUser(userId) {
    $.ajax({
        url: 'UnblockUser.aspx/UnblockUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $(".alert-success").show();
                bindUser();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

function CreateUsersGrid(data) {
    var strHtml = '';
    strHtml += '<table id=UnblockUserTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Name</th>';
    strHtml += '<th>Email</th>';
    strHtml += '<th>Block time</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.Email + '</td>';
        strHtml += '<td>' + value.blockTime + '</td>';
        if (value.isBlock) {
            strHtml += '<td></td>';
        }
        else {
            strHtml += '<td> <a id="edit_' + value.UserId + '" data-email=' + value.Email + ' class="blockedUser">Unblock</a></td>';
        }
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#unblockUserdv").html(strHtml);
    strHtml = '';
    $("#UnblockUserTbl").dataTable({
        "order": [[2, 'desc']],
        "aoColumns": [
            null,
            null,
            null,
             { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}

//var date = new Date(parseInt(jsonDate.substr(6)));
//var formattedDate = date.format("dd-MM-yyyy");

$(document).on("click", ".blockedUser", function () {
    var elementId = this.id;
    var email = $(this).data('email');
    bootbox.confirm("Are you sure you want to unblock the user " + email + " ?", function (result) {
        if (result) {
            var userId = elementId.split("_").pop();
            UnblockUser(userId);
        }
    });

});
