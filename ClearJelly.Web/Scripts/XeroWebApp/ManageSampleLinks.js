﻿
$(document).ready(function () {
    $(".left-side-box ul li").removeClass("active");
    $("#sampleLink").addClass("active");
    bindSampleLinks();
    AddSampleLink();

});

function bindSampleLinks() {
    $.ajax({
        url: 'ManageSampleLinks.aspx/GetSampleLinks',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateSampleLinksGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function hideValidationMsgs() {
    $(".validationMsgs").each(function () {
        if (!$(this).hasClass("display-none")) {
            $(this).addClass("display-none");
        }
    });
}
function EditSampleLink(sampleLinkId) {
    $('#EditSampleLinkPopup').modal('show');
    $("#successMsg").html("Sample Link updated successfully.");
    $('#poupheading').html("Update Sample Link");
    hideValidationMsgs();
    $.ajax({
        url: 'ManageSampleLinks.aspx/GetEditSampleLink?sampleLinkId=' + sampleLinkId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            bindEditData(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}
function bindEditData(data) {
    $('#MainContent_Title').val(data.d.Title);
    $('#MainContent_Downloadlink').val(data.d.DownloadLink);
    $("#sampleLinkId").val(data.d.SampleLinkId);
    validation();
}
function validation() {
    $('#btnUpdate').unbind('click');
    $('#btnUpdate').click(function (e) {
        var isError = false;
        var sampleLinkId = $('#sampleLinkId').val();
        var titleVal = $('#MainContent_Title').val();
        if (titleVal == null || titleVal == "") {
            $("#titleMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }

        var downloadVal = $('#MainContent_Downloadlink').val();
        if (downloadVal == null || downloadVal == "") {
            $("#downloadMsg").removeClass("display-none");
            isError = true;
            e.preventDefault();
        }

        if (!isError) {
            var model = {
                'SampleLinkId': sampleLinkId,
                'DownloadLink': downloadVal,
                'Title': titleVal
            };

            updateSampleLink(model);
        }
    });

    $('#MainContent_Downloadlink').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#downloadMsg").removeClass("display-none");
        }
        else { $("#downloadMsg").addClass("display-none"); }
    });

    $('#MainContent_Title').blur(function () {
        var thisVal = $(this).val();
        if (thisVal == null || thisVal == "") {
            $("#titleMsg").removeClass("display-none");
        }
        else { $("#titleMsg").addClass("display-none"); }
    });

}

function updateSampleLink(model) {
    $.ajax({
        url: 'ManageSampleLinks.aspx/Update',
        type: "Post",
        data: JSON.stringify({ model: model }),
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#EditSampleLinkPopup').modal('hide');
            if (data.d) {
                $(".alert").hide();

                $(".alert-success").show();
                bindSampleLinks();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
            $('#EditSampleLinkPopup').modal('hide');
        }
    });
}

function AddSampleLink() {
    $('#btnAddNew').click(function () {
        $('#EditSampleLinkPopup').modal('show');
        $('#poupheading').html("Add Sample Link");
        $("#successMsg").html("Sample Link added successfully.");
        hideValidationMsgs();
        ClearData();
        validation();
    });

}

function ClearData() {
    $('#MainContent_Title').val("");
    $('#MainContent_Downloadlink').val("");
    $("#sampleLinkId").val("");
}


function CreateSampleLinksGrid(data) {
    var strHtml = '';
    strHtml += '<table id=sampleLinkTbl class="display table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Order</th>';
    strHtml += '<th>Title</th>';
    strHtml += '<th>Download Link</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr class="moveCursor" id=' + value.SampleLinkId + '>';
        strHtml += '<td>' + value.SortOrder + '</td>';
        strHtml += '<td>' + value.Title + '</td>';
        strHtml += '<td>' + value.DownloadLink + '</td>';
        strHtml += '<td> ' +
            '<a id="edit_' + value.SampleLinkId + '" class="editSampleLink handPointer">Edit</a>' + ' | ' +
            '<a id="delete_' + value.SampleLinkId + '" class="deleteSampleLink handPointer">Delete</a>' +
            '</td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#bindDatadv").html(strHtml);
    strHtml = '';
    $('#sampleLinkTbl').dataTable().rowReordering({
        sURL: "ManageSampleLinks.aspx/UpdateSortOrder",
        type: 'POST',
    });
}

$(document).on("click", ".editSampleLink", function () {
    var elementId = this.id;
    var sampleLinkId = elementId.split("_").pop();
    EditSampleLink(sampleLinkId);
});

$(document).on("click", ".deleteSampleLink", function () {
    var elementId = this.id;
    var sampleLinkId = elementId.split("_").pop();

    bootbox.confirm("Are you sure you want to delete? ", function (result) {
        if (result) {
            deleteSampleLink(sampleLinkId);
        }
    });
});

function deleteSampleLink(sampleLinkId) {
    $.ajax({
        url: 'ManageSampleLinks.aspx/Delete',
        type: "Post",
        data: "{ sampleLinkId:" + sampleLinkId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#EditSampleLinkPopup').modal('hide');
            if (data.d) {
                $(".alert").hide();
                $("#successMsg").html("Sample Link deleted successfully.");
                $(".alert-success").show();
                bindSampleLinks();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
            $('#EditSampleLinkPopup').modal('hide');
        }
    });
}


