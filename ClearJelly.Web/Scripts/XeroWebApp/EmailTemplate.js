﻿
$(document).ready(function () {

    $(".left-side-box ul li").removeClass("active");
    $("#emailTempates").addClass("active");
    $("#MainContent_CategoryDropDown").change();
});

$("#MainContent_CategoryDropDown").change(function () {
    var categoryId = $(this).val();
    bindEmailTemplate(categoryId);
});


function bindEmailTemplate(categoryId) {
    $.ajax({
        url: 'EmailTemplate.aspx/GetEmailTemplateList?categoryId=' + categoryId,
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateUsersGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}


function CreateUsersGrid(data) {
    var strHtml = '';
    strHtml += '<table id=Tbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Name</th>';
    strHtml += '<th>Category</th>';
    strHtml += '<th>Subject</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.Category + '</td>';
        strHtml += '<td>' + value.Subject + '</td>';
        if (value.CategoryId != 6) {
            strHtml += '<td> <a id="edit_' + value.EmailTemplateId + '" data-EmailTemplateId=' + value.EmailTemplateId + ' class="editTemplate">Edit</a></td>';
        } else {
            strHtml += '<td> <a id="edit_' + value.EmailTemplateId + '_' + value.Subject +'_'+ value.Name + '" data-EmailTemplateId=' + value.EmailTemplateId + ' class="editTemplateinline">Edit</a></td>';
        }
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#dv").html(strHtml);
    strHtml = '';
    $("#Tbl").dataTable({
        // "order": [[2, 'desc']],
        "aoColumns": [
            null,
            null,
            null,
          { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}

$(document).on("click", ".editTemplate", function () {
    var elementId = this.id;
    var templateId = elementId.split("_").pop();
    window.location.href = '../Admin/EditTemplate.aspx?templateId=' + templateId;
});


$(document).on("click", ".editTemplateinline", function () {
   
    var elementId = this.id;
    var templateId = elementId.split("_")[1];
    var link = elementId.split("_")[2];
    var title = elementId.split("_")[3];
    $('#sampleLinkId').val(templateId)
    $('#EditLinkPopup').modal('show');
    $("#MainContent_Title").val(title);
    $("#MainContent_Downloadlink").val(link);

    //window.location.href = '../Admin/EditTemplate.aspx?templateId=' + templateId;
});


$('#btnUpdate').click(function (e) {
   
    var isError = false;
    var templateId = $('#sampleLinkId').val();
    var downloadVal = $('#MainContent_Downloadlink').val();
    if (downloadVal == null || downloadVal == "") {
        $("#downloadMsg").removeClass("display-none");
        isError = true;
        e.preventDefault();
    }

    if (!isError) {
        updateLink(downloadVal, templateId);
    }
});

function updateLink(downloadVal, templateId) {
   
    $.ajax({
        url: 'EmailTemplate.aspx/Update',
        type: "Post",
        data: JSON.stringify({ templateId: templateId, subject: downloadVal }),
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
          
            bindEmailTemplate(0);
            $("#MainContent_CategoryDropDown").change();
            $('#EditLinkPopup').modal('hide');
            if (data.d) {
                $(".alert").hide();
                $(".alert-success").show();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
            $('#EditSampleLinkPopup').modal('hide');
        }
    });
}