﻿$(function () {
    $("#MainContent_txtSD").datepicker({ format: 'dd/mm/yyyy' });
    $("#MainContent_txtSD").attr('readonly', 'readonly');
    $("#clearSD").on("click", function () {
        $("#MainContent_txtSD").val('');
    });
    $("#MainContent_txtED").datepicker({ format: 'dd/mm/yyyy' });
    $("#MainContent_txtED").attr('readonly', 'readonly');
    $("#clearSD").on("click", function () {
        $("#MainContent_txtED").val('');
    });
});