﻿var _userId;
$(document).ready(function () {
    $.ajax({
        url: 'Index.aspx/GetUserList',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            CreateUsersGrid(data);
            DeleteUser();
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
});


function CreateUsersGrid(data) {
    var strHtml = '';
    strHtml += '<table id=manageUserTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Email</th>';
    strHtml += '<th>First Name</th>';
    strHtml += '<th>Last Name</th>';
    strHtml += '<th>Mobile</th>';
    strHtml += '<th>Active</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.Email + '</td>';
        strHtml += '<td>' + value.FirstName + '</td>';
        strHtml += '<td>' + value.LastName + '</td>';
        strHtml += '<td>' + value.Mobile + '</td>';
        if (value.IsActive) { strHtml += '<td>Yes</td>'; } else { strHtml += '<td>No</td>'; }
        strHtml += '<td> <a id="edit_' + value.UserId + '" class="editUser">Edit</a>';
        strHtml += '&nbsp;<a id="delete_' + value.UserId + '" class="deleteUser" href="">Delete</a></td>';
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#manageuserdv").html(strHtml);
    strHtml = '';
    $("#manageUserTbl").dataTable({
        "aoColumns": [
           null,
            null,
            null,
            null,
            null,
             { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}

$(document).on("click", ".editUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    window.location.href = '../EditProfile.ASPX?userId=' + userId;
});

$(document).on("click", ".deleteUser", function () {
    var elementId = this.id;
    _userId = elementId.split("_").pop();
});

$(document).on("click", "#addUser", function () {
    window.location.href = "./AddUser.aspx";
});

function DeleteUser() {
    var obj = {};
    obj.userId = _userId;
    $(".deleteUser").confirm({
        title: "Delete user confirmation",
        text: "Are you sure you want delete the user?",
        confirm: function () {
            $.ajax({
                url: 'Index.aspx/DeleteUser',
                type: "POST",
                data: JSON.stringify({ userId: _userId }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d) {
                        $("#MainContent_LblSuccess").text("User has been removed successfully!");
                        $("#MainContent_LblFailure").text("");
                        $.ajax({
                            url: 'Index.aspx/GetUserList',
                            type: "GET",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (userdata) {
                                CreateUsersGrid(userdata);
                            },
                            error: function () {
                            }
                        });
                    } else {
                        $("#MainContent_LblSuccess").text("");
                        $("#MainContent_LblFailure").text("Something went wrong, please try again!");
                    }
                },
                error: function () {
                    $("#MainContent_LblSuccess").text("");
                    $("#MainContent_LblFailure").text("Something went wrong, please try again!");
                }
            });
        },
        cancel: function () {
            _userId = "";
        },
        confirmButton: "Yes",
        cancelButton: "No"
    });
}