﻿
$(document).ready(function () {
    $(".left-side-box ul li").removeClass("active");
    $("#activateUser").addClass("active");
    bindUser();
});

function bindUser() {
    $.ajax({
        url: 'ActivateUser.aspx/GetUserList',
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            
            CreateUsersGrid(data);
        },
        error: function () {
            $("#MainContent_LblSuccess").text("");
            $("#MainContent_LblFailure").text("Something went wrong, please try again!");
        }
    });
}

function ActivateUser(userId) {
    $.ajax({
        url: 'ActivateUser.aspx/ActivateUser',
        type: "Post",
        data: "{ userId:" + userId + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d) {
                $(".alert").hide();
                $(".alert-success").show();
                bindUser();
            }
            else {
                $(".alert").hide();
                $(".alert-danger").show();
            }
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}

function CreateUsersGrid(data) {
    var strHtml = '';
    strHtml += '<table id=activateUserTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Name</th>';
    strHtml += '<th>Email</th>';
    strHtml += '<th>Is Active</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
        strHtml += '<tr>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.Email + '</td>';
        if (value.isActive) {
            strHtml += '<td>Yes</td>';
            strHtml += '<td></td>';
        }
        else {
            strHtml += '<td>No</td>';
            strHtml += '<td> <a id="edit_' + value.UserId + '" class="activateUser">Activate</a></td>';

        }
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#activteuserdv").html(strHtml);
    strHtml = '';
    $("#activateUserTbl").dataTable({
        "aoColumns": [
            null,
            null,
            null,
             { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}

$(document).on("click", ".activateUser", function () {
    var elementId = this.id;
    var userId = elementId.split("_").pop();
    bootbox.confirm("Are you sure you want to Activate user? ", function (result) {
        if (result) {
            ActivateUser(userId);
        }
    });

    
});
