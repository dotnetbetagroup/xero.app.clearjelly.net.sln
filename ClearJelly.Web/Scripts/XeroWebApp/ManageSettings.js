﻿$(document).ready(function () {
   
    $(".left-side-box ul li").removeClass("active");
    $("#settingsAdmin").addClass("active");
    bindAppSettingGrid();
});

function bindAppSettingGrid() {
    $.ajax({
        url: 'ManageSetting.aspx/GetSettingList',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           
            CreateAppSettingGrid(data);
        },
        error: function () {
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}


function CreateAppSettingGrid(data) {
    var strHtml = '';
    strHtml += '<table id=manageAppsettingTbl class="table table-striped cust-table">';
    strHtml += '<thead>';
    strHtml += '<tr>';
    strHtml += '<th>Key </th>';
    strHtml += '<th>Value</th>';
    strHtml += '<th>Description</th>';
    strHtml += '<th>Action</th>';
    strHtml += '</tr>';
    strHtml += '</thead>';
    strHtml += '<tbody>';
    $.each(data.d, function (index, value) {
       
        strHtml += '<tr>';
        strHtml += '<td>' + value.Name + '</td>';
        strHtml += '<td>' + value.Value + '</td>';
        strHtml += '<td>' + value.Description + '</td>';
        if (value.Value.toLowerCase().trim() == "true" || value.Value.toLowerCase().trim() == "false") {
            if (value.Value.toLowerCase().trim() == "true") {
                strHtml += '<td> <a href="#" class="EditSettingBool" id="Edit_' + value.SettingId + "_" + "false" + '" > False </a></td>';
            } else {
                strHtml += '<td> <a href="#" class="EditSettingBool" id="Edit_' + value.SettingId + "_" + "true" + '" >True</a></td>';
            }
        } else {
            strHtml += '<td> <a  href="#" class="EditSettingText" id="Edit_' + value.SettingId + "_" + value.Value + '">Edit</a></td>';
        }
        strHtml += '</tr>';
    });
    strHtml += '</tbody>';
    strHtml += '</table>';
    $("#manageAppsettingdv").html(strHtml);
    strHtml = '';
    $("#manageAppsettingTbl").dataTable({
        "aoColumns": [
           null,
            null,
            null,
             { "bSortable": false, "bSearchable": false }
        ]
    }
    );
}

$(document).on("click", ".EditSettingBool", function () {
   
    var ChangeToOnOffBool = "";
    var NoteText = "";
    var elementId = this.id;
    var keyId = elementId.split("_")[1];
    var keyValue = elementId.split("_")[2];
    if (keyValue == "true") {
        ChangeToOnOffBool = "on"
        NoteText = "<br/><br/><b>Note</b>: If you turn on the maintenance mode then users will be redirected to maintenance page "
    } else {
        ChangeToOnOffBool = "off"
    }
    bootbox.confirm('Are you sure you want to turn' + " " + ChangeToOnOffBool + " " + 'maintenance  mode? ' + " " + NoteText + " ", function (result) {
        if (result) {
            UpdateSettingValue(keyId, keyValue);
        }
    });
});

$(document).on("click", ".EditSettingText", function () {
   
    var elementId = this.id;
    var keyId = elementId.split("_")[1];
    var keyValue = elementId.split("_")[2];
    $('#AppSettingPopup').modal('show');
    $("#AppKeyId").val(keyId);
    $("#MainContent_Value").val(keyValue);
});


$('#btnUpdate').click(function (e) {
   
    if ($("#MainContent_Value").val() != "" && $("#MainContent_Value").val() != null) {
        var keyId = $("#AppKeyId").val();
        var keyValue = $("#MainContent_Value").val();
        UpdateSettingValue(keyId, keyValue)
    }
    else {
        $("#titleMsg").show();
    }
});


function UpdateSettingValue(key, keyValue) {
    var obj = { key: key, keyValue: keyValue }
    $.ajax({
        url: 'ManageSetting.aspx/UpdateSettingValue',
        type: "Post",
        data: JSON.stringify(obj),
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           
            $('#AppSettingPopup').modal('hide');
            bindAppSettingGrid();
            $(".alert").hide();
            if (data.d) {
                $(".alert-success").show();
                $("#successMsg").text("Setting value updated successfully")
            } else {
                $(".alert").hide();
                $(".alert-danger").show();
            }

        },
        error: function (data) {
            $('#AppSettingPopup').modal('hide');
            $(".alert").hide();
            $(".alert-danger").show();
        }
    });
}


