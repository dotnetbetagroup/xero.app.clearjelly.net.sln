using System.Web.Routing;
using ClearJelly.Web.Resolvers;
using Microsoft.AspNet.FriendlyUrls;
using Microsoft.AspNet.FriendlyUrls.Resolvers;

namespace ClearJelly.Web
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            //settings.AutoRedirectMode = RedirectMode.Permanent;
            settings.AutoRedirectMode = RedirectMode.Off;
            routes.EnableFriendlyUrls(settings, new IFriendlyUrlResolver[] {new CustomWebFormsFriendlyUrlResolver()});
            routes.MapPageRoute(routeName: "Admin", routeUrl: "Admin", physicalFile: "~/Admin/Index.aspx");
        }
    }
}
