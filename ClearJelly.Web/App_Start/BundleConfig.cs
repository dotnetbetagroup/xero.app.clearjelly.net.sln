﻿using System.Web.Optimization;

namespace ClearJelly.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
                            "~/Scripts/WebForms/WebForms.js",
                            "~/Scripts/WebForms/WebUIValidation.js",
                            "~/Scripts/WebForms/MenuStandards.js",
                            "~/Scripts/WebForms/Focus.js",
                            "~/Scripts/WebForms/GridView.js",
                            "~/Scripts/WebForms/DetailsView.js",
                            "~/Scripts/WebForms/TreeView.js",
                            "~/Scripts/WebForms/WebParts.js"));

            // Order is very important for these files to work, they have explicit dependencies
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            // Use the Development version of Modernizr to develop with and learn from. Then, when you’re
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-2.8.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                          "~/Scripts/bootstrap.js",
                          "~/Scripts/bootbox.js",
                          "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/respond").Include(
                    "~/Scripts/respond.min.js",
                    "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                    "~/Scripts/XeroWebApp/common.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryDataTable").Include(
            "~/Scripts/jquery.dataTables.js",
             "~/Scripts/jquery.dataTables.rowReordering.js",
            "~/Scripts/jquery.dataTables.rowGrouping.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryConfirm").Include(
            "~/Scripts/jquery.confirm.min.js"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapcss").Include(
                "~/Content/bootstrap.css"
                , "~/Content/bootstrap-datepicker3.css"
                ));

            bundles.Add(new StyleBundle("~/Content/DataTablecss").Include(
                      "~/Content/jquery.dataTables.css",
                      "~/Content/jquery.dataTables_themeroller.css"));

            //ScriptManager.ScriptResourceMapping.AddDefinition(
            //    "respond",
            //    new ScriptResourceDefinition
            //    {
            //        Path = "~/Scripts/respond.min.js",
            //        DebugPath = "~/Scripts/respond.js",
            //    });

            //ScriptManager.ScriptResourceMapping.AddDefinition(
            //  "common",
            //  new ScriptResourceDefinition
            //  {
            //      Path = "~/Scripts/XeroWebApp/common.js"
            //  });
        }
    }
}